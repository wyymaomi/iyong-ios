//
//  MainTabBarController+AccountInfo.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MainTabBarController+AccountInfo.h"
#import "UserInfoViewController.h"
#import "MyAccountViewController.h"
#import "OrderStatusListViewController.h"
#import "SettingViewController.h"
#import "MyBusinessCircleViewController.h"

@interface MainTabBarController ()<AccountInfoTableViewDelegate>

@end

@implementation MainTabBarController (AccountInfo)

#pragma mark - 点击侧滑帐户菜单

- (void)onClickAccount
{
//    self.nextAction = @selector(onClickAccount);
//    self.object1 = nil;
//    self.object2 = nil;
//    
//    if ([self needLogin]) {
//        return;
//    }
    
    self.isAccountInfoShow = YES;
    self.accountInfoView.isNeedAnimation = YES;
    [self.accountInfoView show];
    self.accountInfoView.accountTableView.tableViewDelegate = self;
    
//    [self doOrderCountQuery];
//    [self doGetWaitPassedPartner];
}

#pragma mark - AccountInfoTableView Delegate method

-(void)didSelectRow:(NSInteger)row;
{
    self.isAccountInfoShow = YES;
    self.accountInfoView.isNeedAnimation = NO;
    
    if (row == AccountInfoTableRowAvatar) {
        [self.accountInfoView dismiss];
        [self viewPage:@"UserInfoViewController" animated:YES title:@"我的帐户"];
    }
    
    if (row == AccountInfoTableRowMyFavorite) {
        [self.accountInfoView dismiss];
//        [self viewPage:@"]
    }
    
//    if (row == AccountInfoTableRowMyAccount) {
//        [self.accountInfoView dismiss];
//        [self viewPage:@"MyAccountViewController" animated:YES title:@"我的帐户"];
//    }
//    if (row == AccountInfoTableRowMyTravel) {
//        [self.accountInfoView dismiss];
//        [self viewPage:@"OrderStatusListViewController" animated:YES title:@"我的行程"];
//    }
    if (row == AccountInfoTableRowSettings) {
        [self.accountInfoView dismiss];
        [self viewPage:@"SettingViewController" animated:YES title:@"设置"];
    }
//    if (row == AccountInfoTableRowMyInquiry) {
//        [self.accountInfoView dismiss];
//        [self viewPage:@"MyBusinessCircleViewController" animated:YES title:@"我的账户"];
//    }
    self.nextAction = nil;
}

- (void)onGotoPage:(NSString*)vcPage title:(NSString *)title
{
    [self.accountInfoView dismiss];
    self.isAccountInfoShow = YES;
    self.accountInfoView.isNeedAnimation = NO;
    
    [self viewPage:vcPage animated:YES title:title];
    self.nextAction = nil;
    
}

#pragma mark - 转向帐户页面

- (void)gotoAccountInfo
{
    self.isAccountInfoShow = NO;
//    self.nextAction = @selector(gotoAccountInfo);
//    self.object1 = nil;
//    self.object2 = nil;
    
//    if ([self needLogin]) {
//        return;
//    }
    
//    UINavigationController *navigationController = [[UINavigationController alloc] init];
//    BaseViewController *vc = [[cls alloc]init];
//    vc.title = title;
//    vc.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:vc animated:animated];
    
    
    
//    UINavigationController *navCtrl=[[UINavigationController alloc] initWithRootViewController:vc];
    
    [self viewPage:@"UserInfoViewController" animated:YES title:@""];
    self.nextAction = nil;
    
}

- (void)viewPage:(NSString*)vcName
{
    
}

- (void)viewPage:(NSString*)vcName animated:(BOOL)animated title:(NSString*)title
{
    if (IsStrEmpty(vcName)) {
        return;
    }
    Class cls = NSClassFromString(vcName);
    if (!cls) {
        return;
    }
    BaseViewController *vc = [[cls alloc]init];
    vc.title = title;
    vc.hidesBottomBarWhenPushed = YES;
    vc.enter_type = ENTER_TYPE_PRESENT;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.modalPresentationStyle = UIModalPresentationCurrentContext;
//    vc.modalPresentationStyle = Pre
//    UINavi gationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
//    [self.meNavigation pushViewController:vc animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navigationController animated:YES completion:nil];
    
//    [self.meNavigation pushViewController:navigationController animated:YES];
//    self.meNavigation pushViewController:navigationCon animated:<#(BOOL)#>
    
//    [self.navigationController pushViewController:vc animated:animated];
}




@end
