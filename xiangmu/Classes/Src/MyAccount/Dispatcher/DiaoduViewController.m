//
//  DiaoduViewController.m
//  xiangmu
//
//  Created by David kim on 16/3/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DiaoduViewController.h"
#import "DiaoduyuanCollectionViewCell.h"
#import "AddDiaoViewController.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "MyUtil.h"
#import "EditDiaoduViewController.h"
#import "HttpConstants.h"
#import "UserManager.h"
#import "MJRefresh.h"

@interface DiaoduViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArray;
    NSString *uid;
}

@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation DiaoduViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"调度员管理";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"添加" style:UIBarButtonItemStyleDone target:self action:@selector(gotoAdd)];
    
    dataArray=[NSMutableArray array];
//    self.view.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
//    self.tabBarController.tabBar.hidden=YES;
//    [self createMyNav];
   
    [self.orderTabelView reloadData];
    [self setupHeaderView];
  //  [self setupFooterView];
    [self.orderTabelView.mj_header beginRefreshing];
    // Do any additional setup after loading the view.
}
-(UITableView *)orderTabelView
{
    if (_orderTabelView == nil) {
        self.orderTabelView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, ViewHeight) style:UITableViewStylePlain];
        self.orderTabelView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
        self.orderTabelView.scrollEnabled=YES;
        self.orderTabelView.showsVerticalScrollIndicator=NO;
        self.orderTabelView.delegate=self;
        self.orderTabelView.dataSource=self;
        self.orderTabelView.tableFooterView=[[UIView alloc]init];
        [self.view addSubview:_orderTabelView];
    }
    return _orderTabelView;
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(AddDiaoduyuan:) name:@"AddDiaoduyuan" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(editDiaoduyuan:) name:@"editDiaoduyuan" object:nil];
//    
//    [self.orderTabelView reloadData];
//    [self setupHeaderView];
//    [self.orderTabelView.mj_header beginRefreshing];
//}
-(void)diaodu
{
    [self.orderTabelView reloadData];
    [self setupHeaderView];
    [self.orderTabelView.mj_header beginRefreshing];
}
-(void)editdiaodu
{
    [self.orderTabelView reloadData];
    [self setupHeaderView];
    [self.orderTabelView.mj_header beginRefreshing];
}

-(void)setupHeaderView
{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefreshing)];
    self.orderTabelView.mj_header = header;
}
-(void)setupFooterView
{
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(LoadMoreData)];
    footer.triggerAutomaticallyRefreshPercent = 1;
    footer.automaticallyHidden=YES;
    self.orderTabelView.mj_footer = footer;
}
-(void)LoadMoreData
{
    
}
-(void)headerRefreshing
{
    self.nextAction = @selector(headerRefreshing);
    self.object1 = nil;
    self.object2 = nil;
    
    // 汽车信息获取
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Diaodu_List_API];
    NSString *parmas = [NSString stringWithFormat:@"time=%@", @""];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        StrongSelf
        NSLog(@"response success");
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
             [self resetAction];
            
            dataArray  = [responseData valueForKey:@"data"];
            [strongSelf.orderTabelView reloadData];
            self.orderTabelView.mj_footer.hidden = self.orderTabelView.frame.size.height < ViewHeight ? YES : NO;
            [self.orderTabelView.mj_header endRefreshing];
        }
        
        NSLog(@"%@&&&&&&&&&&",dataArray);
        NSLog(@"result = %@", responseData);
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        [self.orderTabelView.mj_footer endRefreshing];
    }];
    
}

-(void)gotoAdd
{
    AddDiaoViewController *addVC=[[AddDiaoViewController alloc]init];
    addVC.delegate = self;
    [self.navigationController pushViewController:addVC animated:YES];
}


#pragma mark---UITableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    DiaoduyuanCollectionViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[DiaoduyuanCollectionViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    cell.name.text=[dataArray[indexPath.section] valueForKey:@"nickname"];
    cell.phoneLabel.text=[dataArray[indexPath.section] valueForKey:@"username"];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DiaoduyuanCollectionViewCell *cell=(DiaoduyuanCollectionViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    EditDiaoduViewController *editDiaoDuVC=[[EditDiaoduViewController alloc]init];
    editDiaoDuVC.delegate = self;
    editDiaoDuVC.name=cell.name.text;
    editDiaoDuVC.phone=cell.phoneLabel.text;
    [self.navigationController pushViewController:editDiaoDuVC animated:YES];
}
//设置分区头视图
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, 10)];
    headerView.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

//删除操作
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //获取到相应cell
    DiaoduyuanCollectionViewCell *DiaoduCell=(DiaoduyuanCollectionViewCell *)[self.orderTabelView cellForRowAtIndexPath:indexPath];
    
    if (DiaoduCell.editingStyle==UITableViewCellEditingStyleDelete)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"确定删除" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
        [alert show];
        uid=[dataArray[indexPath.section] valueForKey:@"id"];
        //  }
        
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        NSLog(@"确定");
        
        // 汽车信息获取
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Diaodu_Delete_API];
        
        NSString *parmas = [NSString stringWithFormat:@"userId=%@", uid];
//        NSLog(@"%@&&&&&&&&&&",uid);
        
        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
            
            NSLog(@"response success");
            [self setupHeaderView];
            [self.orderTabelView.mj_header beginRefreshing];
            [self.orderTabelView reloadData];
            NSLog(@"result = %@", responseData);
            
        } andFailure:^(NSString *errorDesc) {
            
            NSLog(@"response failure");
                  return;
        }];
        
    }
    
    else
    {
        NSLog(@"取消");
      
        
    }

}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *title=[MyUtil createLabelFrame:CGRectMake(0, 0, 20, 30) title:@"删除" font:[UIFont systemFontOfSize:8] textAlignment:NSTextAlignmentCenter numberOfLines:0 textColor:[UIColor whiteColor]];
    return title.text;
}


@end
