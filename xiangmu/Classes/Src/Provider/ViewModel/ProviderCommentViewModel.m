//
//  ProviderCommentViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderCommentViewModel.h"
#import "ProviderCommentModel.h"

@implementation ProviderCommentViewModel

- (void)setCommentModel:(ProviderCommentModel *)commentModel
{
    _commentModel = commentModel;
    [self setMomentsBodyFrames];
    [self setCellHeight];
}

- (void)setServiceModel:(ProviderCommentModel *)serviceModel
{
    _commentModel = serviceModel;
    //    _moment = moment;
    //    //计算主体Frame
    [self setMomentsBodyFrames];
    //    //计算工具条Frame
    //    [self setMomentsToolBarFrames];
    //    //计算CellHeight
    [self setCellHeight];
}

//计算Code圈主体Frame
- (void)setMomentsBodyFrames{
    
    // 公司基本信息
    //    CGFloat companyX = 0;
    //    CGFloat companyY = 0;
    //    CGFloat companyWidth = ViewWidth;
    //    CGFloat companyHeight = 50;
    //    self.bodyCompanyFrame = CGRectMake(companyX, companyY, companyWidth, companyHeight);
    
    //头像
//    CGFloat iconX = circleCellMargin;
//    CGFloat iconY = circleCellMargin;
//    CGFloat iconW = circleCelliconWH;
//    CGFloat iconH = circleCelliconWH;
    self.bodyIconFrame = CGRectMake(24, 20, 40, 40);
    
    // 昵称
    _nameFont = FONT(15);
//    CGSize nameSize = [self.commentModel.nickname textSize:CGSizeMake(ViewWidth-CGRectGetMaxX(self.bodyIconFrame)-10-55, MAXFLOAT) font:_nameFont];
    self.bodyNameFrame = CGRectMake(CGRectGetMaxX(self.bodyIconFrame)+10, CGRectGetMinY(self.bodyIconFrame), ViewWidth-CGRectGetMaxX(self.bodyIconFrame)-10-55, 20);
    
    
    // 时间
//    CGSize nameSize ＝
//    CGSize
    self.timeFont = FONT(13);
    self.bodyTimeFrame = CGRectMake(ViewWidth-50, self.bodyNameFrame.origin.y, 50, 20);
    
    
    // 内容
    _contentFont = FONT(13);
    CGSize contentSize = [self.commentModel.content textSize:CGSizeMake(self.bodyNameFrame.size.width, MAXFLOAT) font:_contentFont];
    self.bodyTextFrame = CGRectMake(self.bodyNameFrame.origin.x,
                                    CGRectGetMaxY(self.bodyNameFrame)+2.5,
                                    self.bodyNameFrame.size.width,
                                    contentSize.height);
    
    
    
    
    //昵称
//    CGFloat nameX = circleCelliconWH + circleCellMargin * 2;
//    CGFloat nameY = iconY;
//    //    CGFloat nameW = 120;
//    CGSize nameSize = [self.commentModel.commenterCompanyId sizeWithAttributes:circleCellNameattributes];
//    self.bodyNameFrame = (CGRect){{nameX,nameY},{nameSize.width, iconH/2}};
    
    // 公司名称
//    CGFloat timeX = nameX;
//    CGFloat timeY =  CGRectGetMaxY(self.bodyNameFrame) + 5;
//    CGSize timeSize = [self.commentModel.createTime boundingRectWithSize:CGSizeMake(ViewWidth-timeX-circleCellMargin, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:circleCellTimeattributes context:nil].size;
//    self.bodyTimeFrame = (CGRect){{timeX,timeY},timeSize};
    
    // 如果公司名称为空 昵称垂直居中
    //    if (IsStrEmpty(self.serviceModel.companyName)) {
    //        self.bodyNameFrame = (CGRect){{nameX, nameY}, {nameSize.width, iconH}};
    //    }
    
    //正文
//    CGFloat textX = circleCellMargin;
//    CGFloat textY = CGRectGetMaxY(self.bodyIconFrame) + 5;
//    if (IsStrEmpty(self.commentModel.content)) {
//        self.bodyTextFrame = (CGRect){{textX, CGRectGetMaxY(self.bodyIconFrame) + 1},{0,0}};
//    }
//    else {
//        CGFloat textW = circleCellWidth - circleCellMargin * 2;
//        CGSize textSize = [self.commentModel.content boundingRectWithSize:CGSizeMake(textW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:circleCellTextattributes context:nil].size;
//        self.bodyTextFrame = (CGRect){{textX,textY},textSize};
//    }
    
    
    
    //图片 (判断是否有图片)
    //    if ([self.serviceModel hasPhoto]) {
    //        CGFloat photosX = 15;
    //        CGFloat photosY = CGRectGetMaxY(self.bodyTextFrame) + 5;
    //        CGFloat photosW = (ViewWidth -  15 * 2);
    //        CGFloat photosH = 60;
    //        self.bodyPhotoFrame = CGRectMake(photosX, photosY, photosW, photosH);
    //        //主体frame
    //        CGFloat bodyH = CGRectGetMaxY(self.bodyPhotoFrame) + circleCellMargin;
    //        self.momentsBodyFrame = CGRectMake(0, 0, circleCellWidth, bodyH);
    //    }else {
    //主体frame
//    CGFloat bodyH = CGRectGetMaxY(self.bodyTextFrame) + 5;
    
    CGFloat bodyHeight;
    if (CGRectGetMaxY(self.bodyIconFrame) > CGRectGetMaxY(self.bodyTextFrame)) {
        bodyHeight = CGRectGetMaxY(self.bodyIconFrame)+10;
    }
    else {
        bodyHeight = CGRectGetMaxY(self.bodyTextFrame)+10;
    }
    self.momentsBodyFrame = CGRectMake(0, 0, ViewWidth, bodyHeight);
    //    }
}

//计算Code圈工具条Frame
//- (void)setMomentsToolBarFrames
//{
//    //工具条frame
//    CGFloat toolBarX = 0;
//    CGFloat toolBarY = CGRectGetMaxY(self.momentsBodyFrame)+1;
//    CGFloat toolBarW = ViewWidth;
//    CGFloat toolBarH = circleCellToolBarHeight;
//    self.momentsToolBarFrame = CGRectMake(toolBarX, toolBarY, toolBarW, toolBarH);
//
//    // 通话
//    CGFloat telX = 0;
//    CGFloat telY = 0;
//    CGFloat telW = toolBarW/3;
//    CGFloat telH = circleCellToolBarHeight;
//    self.toolTelFrame = CGRectMake(telX, telY, telW, telH);
//
//    //收藏
//    CGFloat commentX = telX + telW + 1;
//    CGFloat commentY = 0;
//    CGFloat commentW = telW;
//    CGFloat commentH = telH;
//    self.toolFavoriteFrame = CGRectMake(commentX, commentY, commentW, commentH);
//
//    //点赞
//    CGFloat likeX = commentX + commentW + 1;
//    CGFloat likeY = 0;
//    CGFloat likeW = toolBarW / 3;
//    CGFloat likeH = circleCellToolBarHeight;
//    self.toolLikeFrame = CGRectMake(likeX, likeY, likeW, likeH);
//}

- (void)setCellHeight{
    
    self.cellHeight = CGRectGetMaxY(self.momentsBodyFrame);
    //    self.cellHeight = CGRectGetMaxY(self.momentsToolBarFrame);
}

@end
