//
//  BankcardListView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BancardListViewDelegate <NSObject>

-(void)onBankcardListSelect:(NSInteger)selectedRow;

-(void)onAddBankcard;

@end

@interface BankcardListView : UIView<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataList;
@property (assign, nonatomic) NSInteger selectedRow;

@property (weak, nonatomic) id<BancardListViewDelegate> delegate;

@end
