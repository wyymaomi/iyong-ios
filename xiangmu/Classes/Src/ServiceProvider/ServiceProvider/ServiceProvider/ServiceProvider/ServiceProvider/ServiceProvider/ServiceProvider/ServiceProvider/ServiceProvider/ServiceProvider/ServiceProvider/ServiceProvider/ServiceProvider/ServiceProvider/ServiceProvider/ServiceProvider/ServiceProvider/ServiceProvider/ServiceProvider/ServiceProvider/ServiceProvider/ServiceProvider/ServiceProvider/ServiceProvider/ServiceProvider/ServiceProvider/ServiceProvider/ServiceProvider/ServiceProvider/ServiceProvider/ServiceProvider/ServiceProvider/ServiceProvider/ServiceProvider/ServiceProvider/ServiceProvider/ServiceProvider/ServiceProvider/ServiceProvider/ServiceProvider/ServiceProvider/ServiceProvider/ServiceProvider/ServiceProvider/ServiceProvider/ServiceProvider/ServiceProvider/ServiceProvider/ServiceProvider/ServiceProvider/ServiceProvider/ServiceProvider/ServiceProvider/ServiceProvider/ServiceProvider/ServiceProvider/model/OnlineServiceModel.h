//
//  OnlineServiceModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol OnlineServiceModel <NSObject>

@end

@interface OnlineServiceModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *business;// 经营描述(业务范围)
@property (nonatomic, strong) NSString<Optional> *img1;
@property (nonatomic, strong) NSString<Optional> *img2;
@property (nonatomic, strong) NSString<Optional> *img3;
@property (nonatomic, strong) NSString<Optional> *img4;
@property (nonatomic, strong) NSString<Optional> *thumbsQuantity;// 点赞数量
@property (nonatomic, strong) NSNumber<Optional> *thumbsTime;// 最后一次点赞时间
@property (nonatomic, strong) NSString<Optional> *companyName;// 公司名
@property (nonatomic, strong) NSString<Optional> *nickname;// 昵称
@property (nonatomic, strong) NSNumber<Optional> *companyScor;// 公司评分
@property (nonatomic, strong) NSString<Optional> *logoUrl;// 公司LOGO
@property (nonatomic, strong) NSString<Optional> *mobile;// 手机号码
@property (nonatomic, strong) NSNumber<Optional> *collection;//是否已收藏
@property (nonatomic, strong) NSString<Optional> *companyId; // 公司ID
@property (nonatomic, strong) NSString<Optional> *collectionTime;
@property (nonatomic, strong) NSNumber<Optional> *thumbsUp;// 是否已经点过赞

- (BOOL)hasPhoto;

@end

@interface OnlineServiceResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel, Optional> *data;

@end


