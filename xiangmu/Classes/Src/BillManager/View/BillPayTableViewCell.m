//
//  BillPayTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BillPayTableViewCell.h"

@implementation BillPayTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath
{
    static NSString *ID = @"BillPayTableViewCell";
    BillPayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.checkButton.tag = indexPath.row;
        cell.infoView.tag = indexPath.row;
    }
    return cell;
}

+ (CGFloat)getCellHeight
{
    return 172;
}

-(void)initData:(BillRepayModel*)data
{
    self.upCompanyLabel.text = IsStrEmpty(data.upCompanyName)?@"自派":data.upCompanyName;
    self.payDateLabel.text = [data.repaymentTime getDateTime:@"MM月dd日"];
    self.payMoney.text = [NSString stringWithFormat:@"还款金额：¥%ld", (long)[data.price integerValue]];
    [self.payMoney setTextColor:UIColorFromRGB(0xD51618) text:[NSString stringWithFormat:@"¥%ld", (long)[data.price integerValue]]];
    
//    self.payMoney.frame = CGRectMake(ViewWidth/2, self.payMoney.top, ViewWidth/2-15,/*self.payMoney.size.width,*/ self.payMoney.size.height);
    
    self.startTime.text = [data.carTime getDateTime];
    self.startLocation.text = [StringUtil getSafeString:data.upAddress];
    self.endLocation.text = [StringUtil getSafeString:data.downAddress];
    self.customerLabel.text = [NSString stringWithFormat:@"%@ %@", [StringUtil getSafeString:data.customerName], [StringUtil getSafeString:data.customerMobile]];
    
    [self.infoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doSelectCell:)]];
}

- (IBAction)onButtonClick:(id)sender {
    UIButton *button = (UIButton*)sender;
    self.checkButton.selected = !self.checkButton.selected;
    if (self.delegate && [self.delegate respondsToSelector:@selector(onCheckButton:checked:)]) {
        [self.delegate onCheckButton:button.tag checked:self.checkButton.selected];
    }
}

-(void)doSelectCell:(UIView*)view
{
    self.infoView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
//        [weakSelf.delegate onSelectRowAtIndex:self.infoView.tag];
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(onSelectRowAtIndex:)]) {
            [weakSelf.delegate onSelectRowAtIndex:weakSelf.infoView.tag];
        }
        weakSelf.infoView.backgroundColor = [UIColor whiteColor];
    });
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
