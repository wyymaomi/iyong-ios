//
//  AddCarDetailViewController.m
//  xiangmu
//
//  Created by David kim on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AddCarDetailViewController.h"
#import "AddCarViewController.h"
#import "MyUtil.h"
#import "EditCarViewController.h"

@interface AddCarDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@end

@implementation AddCarDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    [self createMyNav];
    [self createInterface];
    
   // [self.view addSubview:nameLabel];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    _dateStr=[formatter stringFromDate:[NSDate date]];
}
-(void)createMyNav
{
//    UILabel *nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 80, 40)];
//    nameLabel.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
//    nameLabel.font=[UIFont systemFontOfSize:13];
//    nameLabel.text=self.name;
    
    UILabel *titleLabel=[self addNavTitle:CGRectMake(0, 0, 335, 49) title:self.name];
    titleLabel.textColor=[UIColor whiteColor];
    
    UIButton *backBtn=[self addNavBtn:CGRectMake(0, 0, 9, 15) title:nil target:self action:@selector(gotoBack) isLeft:YES];
    [backBtn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    
    UIButton *saveBtn=[self addNavBtn:CGRectMake(0, 0, 40, 20) title:@"保存" target:self action:@selector(gotoSave) isLeft:NO];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.translucent=NO;
}
-(void)createInterface
{
    if (self.section==0) {
        if (self.row==1||self.row==2||self.row==3||self.row==6) {
            
            UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(0, 10, ViewWidth, 40)];
            textField.clearButtonMode=UITextFieldViewModeAlways;
            textField.font=[UIFont systemFontOfSize:13];
            textField.textAlignment=NSTextAlignmentCenter;
            textField.tag=600+self.row;
            
            if (self.row==6) {
                textField.keyboardType=UIKeyboardTypeNumberPad;
                textField.placeholder=@"请填手机号";
            }
            else if (self.row==1)
            {
                textField.placeholder=@"请填写车";
                
            }
            else if (self.row==2)
            {
                textField.placeholder=@"请填车牌号";
                
            }
            else if (self.row==3)
            {
                textField.placeholder=@"请填姓名";
                
            }
            
        //    if (self.type==SearchTypeEdit) {
                if ([self.rightname isEqualToString:@"请填写车"]||[self.rightname isEqualToString:@"请填写车牌号"]||[self.rightname isEqualToString:@"请填姓名"]||[self.rightname isEqualToString:@"请填写手机号"])
                {
                textField.placeholder=self.rightname;
                }
                else
                {
                    textField.text=self.rightname;
                }
//            }
//            
//            else if (self.type==SearchTypeAdd)
//            {
//                textField.placeholder=self.rightname;
//            }
            textField.backgroundColor=[UIColor whiteColor];
            [self.view addSubview:textField];

        }
        else if(self.row==4)
        {
            self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, 300) style:UITableViewStylePlain];
            self.tableView.delegate=self;
            self.tableView.dataSource=self;
            [self.view addSubview:self.tableView];
        }
        else if(self.row==5)
        {
            self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, 100) style:UITableViewStylePlain];
            self.tableView.backgroundColor=[UIColor whiteColor];
            self.tableView.delegate=self;
            self.tableView.dataSource=self;
            [self.view addSubview:self.tableView];
        }
    }
    else
    {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(10, 60, ViewWidth-20, ViewHeight/3)];
        view.tag=900;
        view.backgroundColor=[UIColor whiteColor];
        [self.view addSubview:view];
        
        UIDatePicker *datePicker=[[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, ViewWidth-20, ViewHeight/3)];
        datePicker.backgroundColor=[UIColor whiteColor];
        //日期模式
        [datePicker setDatePickerMode:UIDatePickerModeDate];
        NSLocale *local=[[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"];
        datePicker.locale=local;
        //定义最小日期
        NSDateFormatter *formatter_minDate=[[NSDateFormatter alloc]init];
        [formatter_minDate setDateFormat:@"yyyy-MM-dd"];
        //最小日期是今天
       // NSDate *minDate=[NSDate date];
        formatter_minDate=nil;
        // [formatter_minDate release];
        
      //  [datePicker setMinimumDate:minDate];
        [datePicker addTarget:self action:@selector(dateValueChanged:) forControlEvents:UIControlEventValueChanged];
        [view addSubview:datePicker];
        
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}
-(void)viewWillLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}
-(void)dateValueChanged:(UIDatePicker *)sender
{
    // UITextField *text5=[self.view viewWithTag:104];
    UIDatePicker *datePicker=(UIDatePicker *)sender;
    NSDate *date=datePicker.date;
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    _dateStr=[formatter stringFromDate:date];
    NSLog(@"%@",_dateStr);
    
    //    NSString *lastDate=[(NSString *)date substringToIndex:10];
    //NSLog(@"%@",lastDate);
    formatter=nil;
    // [formatter release];
    // text5.text=[formatter stringFromDate:date];
}
-(void)gotoSave
{
    if (self.section==0) {
        if (self.row==1||self.row==2||self.row==3)
        {
            
            UITextField *textfield=[self.view viewWithTag:600+self.row];
            if ([_delegate respondsToSelector:@selector(getRow:WithTitle:)]) {
                [_delegate getRow:self.row WithTitle:textfield.text];
            }
        
            [self.navigationController popViewControllerAnimated:YES];
        }
          else  if (self.row == 6) {
              UITextField *textfield=[self.view viewWithTag:600+self.row];
                NSString *searchText = textfield.text;
                NSError *error = NULL;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^1[3|4|5|7|8][0-9]\\d{8}$" options:NSRegularExpressionCaseInsensitive error:&error];
                NSTextCheckingResult *result = [regex firstMatchInString:searchText options:0 range:NSMakeRange(0, [searchText length])];
                if (result) {
                    NSLog(@"手机号正确");
                    if ([_delegate respondsToSelector:@selector(getRow:WithTitle:)]) {
                        [_delegate getRow:self.row WithTitle:textfield.text];
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    NSLog(@"手机号错误");
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入11位手机号"preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    [alertView addAction:alertAction];
                    [self presentViewController:alertView animated:YES completion:nil];
                }
                
            }
            
    }
    else
    {
        if ([_delegate respondsToSelector:@selector(KnowRow:WithDate:)]) {
            [_delegate KnowRow:self.row WithDate:_dateStr];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)gotoBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark-------UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.row==4) {
        return 6;
    }
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
        if (self.row==4)
        {
            NSArray *nameArray=@[@"50后",@"60后",@"70后",@"80后",@"90后",@"00后"];
            cell.textLabel.text=nameArray[indexPath.row];
            cell.textLabel.font=[UIFont systemFontOfSize:14];
            cell.textLabel.frame=CGRectMake(0, 0, ViewWidth, 50);
            cell.textLabel.textAlignment=NSTextAlignmentCenter;
            cell.textLabel.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        }
        else if(self.row==5)
        {
        NSArray *nameArray=@[@"男",@"女"];
        cell.textLabel.text=nameArray[indexPath.row];
        cell.textLabel.frame=CGRectMake(0, 0, ViewWidth, 50);
        cell.textLabel.font=[UIFont systemFontOfSize:14];
        cell.textLabel.textAlignment=NSTextAlignmentCenter;
        cell.textLabel.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell=(UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (self.row==4) {
//         [[NSNotificationCenter defaultCenter]postNotificationName:@"addSuccess" object:cell.textLabel.text];
        if([_delegate respondsToSelector:@selector(ChangeRow:WithTitle:)])
        {
            [_delegate ChangeRow:indexPath.row WithTitle:cell.textLabel.text];
        }
    }
    else
    {
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"ageSuccess" object:cell.textLabel.text];
        if([_delegate respondsToSelector:@selector(AgeRow:WithTitle:)])
        {
            [_delegate AgeRow:indexPath.row WithTitle:cell.textLabel.text];
        }
       
    }
        [self.navigationController popViewControllerAnimated:YES];
    
}

@end
