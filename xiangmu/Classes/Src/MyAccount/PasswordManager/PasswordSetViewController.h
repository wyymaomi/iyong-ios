//
//  MimaViewController.h
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
typedef NS_ENUM(NSInteger,MimaType)
{
    MimaTypeLoad=10,
    MimaTypeShoushi,
    MimaTypeJiaoyi,
};
@interface PasswordSetViewController : LFNavController
@property (nonatomic,assign)MimaType type;
@property (nonatomic, strong) UITextField *oldPwdTextField;
@property (nonatomic, strong) UITextField *pwdTextField;
@property (nonatomic, strong) UITextField *rePwdTextField;
@property (nonatomic, strong) NSString *nextViewController;
@end
