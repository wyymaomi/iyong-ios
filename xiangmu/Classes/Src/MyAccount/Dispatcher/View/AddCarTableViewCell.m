//
//  AddCarTableViewCell.m
//  xiangmu
//
//  Created by David kim on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AddCarTableViewCell.h"
#import "MyUtil.h"
#define ViewWidth [[UIScreen mainScreen]bounds].size.width
@implementation AddCarTableViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *leftImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-15, 18, 23/3, 42/3)];
        leftImageView.image = [UIImage imageNamed:@"icon_disclosure"];
        [self addSubview:leftImageView];
        
        self.rightLabel=[MyUtil createLabelFrame:CGRectMake(ViewWidth-155, 10,130, 30) title:nil font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentRight numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
        [self addSubview:self.rightLabel];
    }
    return  self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
