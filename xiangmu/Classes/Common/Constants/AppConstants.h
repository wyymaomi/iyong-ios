//
//  AppConstants.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h

//#define NAVBAR_BGCOLOR

#define REQUEST_TIME_OUT    60      // 请求超时时间
#define LOGIN_OVER_TIME     10*60  // 登录超时时间

#define MSG_DECRYPT_FAILURE @"decrypt failed!"

#define KEY_SESSION     @"key_session"      // session
#define KEY_TOKEN       @"key_token"        // token
#define KEY_DES_KEY     @"key_des_key"      // DES 密钥
#define KEY_COMPANY_ID  @"key_company_id"   // company ID
#define KEY_USER_NAME   @"key_account"      // 账号
#define KEY_SHA_BODY    @"key_sha_body"     // sha加密的body
#define KEY_RSA_BODY    @"key_rsa_body"     // RSA加密的body
#define KEY_GESTURE_PWD @"key_gesture_pwd"  // 手势密码

#define kGesturePwdSetSuccessNotification @"kGesturePwdSetSuccessNotification" //手势密码设置成功后退出之前的登录界面
#define kRegisterSuccessNotification @"kRegisterSuccessNotification" // 注册成功后通知登录界面
#define kUnauthorizeNotification @"Unauthorize_Notification" // 未授权通知
#define kLoginSuccessNotification @"kLoginSuccessNotification"
#define kOnLoginOvertimeNotification @"kEnterbackgroundNotification" // 进入后台通知

#endif /* AppConstants_h */
