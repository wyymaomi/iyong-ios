//
//  CommentDetailViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ExperienceDetailViewController.h"
#import "DiscussTopTableViewCell.h"
#import "UserExperienceTableViewCell.h"
#import "PullRefreshTableView.h"
#import "ExperienceCommentModel.h"
#import "ExperienceCommentTableViewCell.h"
#import "UserExpericeModel.h"
#import "CommentPopupVIew.h"
#import "ExperienceDetailViewController+NetworkRequest.h"
#import <UShareUI/UShareUI.h>
#import "ShareUtil.h"

#define DialogViewHeight  ViewHeight*0.3

#define EXPERIENCE_DETAIL_PATH @"/iyong-web/share/provider.html"

@interface ExperienceDetailViewController ()<UITableViewDelegate, UITableViewDataSource, CommentPopupViewDelegate,UserExperienceCellDelegate>

@property (nonatomic, strong) MLSDKScene *scene;
@property (nonatomic, strong) NSString *mobid;

@property (nonatomic, strong) UIView *topbarView;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *shareButton;


@end

@implementation ExperienceDetailViewController

+ (NSString *)MLSDKPath
{
    return EXPERIENCE_DETAIL_PATH;
}

- (instancetype)initWithMobLinkScene:(MLSDKScene *)scene
{
    if (self = [super init])
    {
        self.scene = scene;
    }
    return self;
}

-(UIImageView*)shareImageView
{
    if (!_shareImageView) {
        _shareImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _shareImageView.hidden = YES;
    }
    return _shareImageView;
}

- (UIView*)topbarView
{
    if (!_topbarView) {
        _topbarView = [UIView new];
        _topbarView.backgroundColor = [UIColor darkGrayColor];
        _topbarView.frame = CGRectMake(0, 0, ViewWidth, 70*Scale);
//        _topbarView.alpha = 0.5;
        _topbarView.backgroundColor = ColorFromRGBWithAlpha(0x030b1e, 0.4);//UIColorFromRGB(0x40030b1e);
        
        [_topbarView addSubview:self.backButton];
        [_topbarView addSubview:self.shareButton];
        
    }
    return _topbarView;
}

- (UIButton*)backButton
{
    if (_backButton == nil) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.frame = CGRectMake(12*Scale, 35*Scale, 12*Scale, 21*Scale);
        [_backButton setBackgroundImage:[UIImage imageNamed:@"icon_back"] forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(pressedBackButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

-(UIButton*)shareButton
{
    if (!_shareButton) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareButton.frame = CGRectMake(ViewWidth-34*Scale, 35*Scale, 21*Scale, 21*Scale);
        [_shareButton setBackgroundImage:[UIImage imageNamed:@"icon_share2"] forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(onClickShareButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}

- (CommentPopupView*)commentPopupView
{
    if (_commentPopupView == nil) {
        _commentPopupView = [CommentPopupView new];
        _commentPopupView.delegate = self;
    }
    return _commentPopupView;
}

-(UIButton*)commentButton
{
    if (_commentButton == nil) {
        _commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _commentButton.frame = CGRectMake(0, 0, ViewWidth, 50);
        _commentButton.titleLabel.font = FONT(13);
        [_commentButton setImage:[UIImage imageNamed:@"icon_btn_comment"] forState:UIControlStateNormal];
        _commentButton.backgroundColor = [UIColor whiteColor];
        [_commentButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_commentButton addTarget:self action:@selector(onClickDiscussButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commentButton;
}

-(NSMutableArray*)followCommentList
{
    if (_followCommentList == nil) {
        _followCommentList = [NSMutableArray array];
    }
    return _followCommentList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view addSubview:self.pullRefreshTableView];
//    self.pullRefreshTableView.autoresizingMask = UITableViewA
    self.pullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.pullRefreshTableView.frame = CGRectMake(0, 0, ViewWidth, self.view.frame.size.height-50);
    [self.view addSubview:self.commentButton];
    
    UIBarButtonItem *shareButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_share2"] style:UIBarButtonItemStyleDone target:self action:@selector(onClickShareButton:)];
    [self.navigationItem setRightBarButtonItems:@[/*favoriteButtonItem,*/shareButtonItem]];
    
    [self.view addSubview:self.topbarView];
    
    [self fetchExperienceDetail];
    
}


- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.commentButton.top = self.view.height - 50;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self fetchFollowCommentList:@""];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
}

- (void)onClickFavoriteButton:(id)sender
{
    [self addLikeNumber];
}

- (void)onClickShareButton:(id)sender
{
    
    WeakSelf
    NSDictionary *params = @{@"experienceId":self.experienceViewModel.serviceModel.id};
    // 根据路径、来源以及自定义参数构造scene
    MLSDKScene *scene = [[MLSDKScene alloc] initWithMLSDKPath:EXPERIENCE_DETAIL_PATH source:@"iYONG-Experience" params:params];
    [MobLink getMobId:scene result:^(NSString *mobid) {
        if (mobid != nil) {
            NSString *shareUrl = [NSString stringWithFormat:@"%@%@", EXPERIENCE_DETAIL_SHARE_URL, weakSelf.experienceViewModel.serviceModel.id];
            NSString *title = @"【iYONG】始于品质 衷于价格 –尽在iYONG出行";
            DLog(@"shareUrl = %@", shareUrl);
            [ShareUtil startShare:shareUrl title:title desc:weakSelf.experienceViewModel.serviceModel.content icon:weakSelf.shareImageView.image webpageUrl:shareUrl];
        }
    }];
    
}

- (void)onClickLikeButton:(id)sender;
{
    self.nextAction = @selector(onClickLikeButton:);
    self.object1 = sender;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    [self addLikeNumber];
}

- (void)onHeaderRefresh
{
    [self saveNextAction:@selector(onHeaderRefresh) object1:nil object2:nil];
    
    [self fetchExperienceDetail];
    
    [self fetchFollowCommentList:@""];
}

-(void)onFooterRefresh
{
    
    if (self.followCommentList.count == 0) {
        [self fetchFollowCommentList:@""];
        return;
    }
    
    ExperienceCommentViewModel *commentViewModel = [self.followCommentList lastObject];
    if (commentViewModel.commentModel) {
        [self fetchFollowCommentList:commentViewModel.commentModel.createTime];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - 点击评论功能

- (IBAction)onClickDiscussButton:(id)sender {
    
    // 判断用户是否已经登录
    if ([self needLogin]) {
        return;
    }
    
    [self.commentPopupView addComment];
    
}

- (void)onGetText:(NSString *)text
{
    [self uploadComment:text];
}

//- (void)showTextInputAlertView {
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"请输入评论" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
//    UITextField *txtName = [alert textFieldAtIndex:0];
//    txtName.placeholder = @"请输入名称";
//    [alert show];
//}
//
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (buttonIndex == 1) {
//        UITextField *txt = [alertView textFieldAtIndex:0];
//        //获取txt内容即可
//        
//        [self uploadComment:txt.text];
//        
//    }
//    
//    
//}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return TableViewSectionNumber;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == CommentCellSectionAdvertisement) {
        return 1;
    }
    if (section == CommentCellSectionComment) {
        return 1;
    }
    if (section == CommentCellSectionFollowComment) {
        return self.followCommentList.count;
    }
    return 1;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == CommentCellSectionAdvertisement) {
        return ViewWidth;
    }
    if (indexPath.section == CommentCellSectionComment) {
        return self.experienceViewModel.cellHeight;
    }
    if (indexPath.section == CommentCellSectionFollowComment) {
        ExperienceCommentViewModel *viewModel = self.followCommentList[indexPath.row];
        return viewModel.cellHeight;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == CommentCellSectionFollowComment) {
        return 40;
    }
    return 0.00001f;
}

//- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    if (section == CommentCellSectionFollowComment) {
//        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(12, 0, ViewWidth-24, 30)];
//        
//        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, ViewWidth, 30)];
//        titleLabel.text = @"评论区";
//        titleLabel.font = FONT(13);
//        titleLabel.textColor = UIColorFromRGB(0x333333);
//        titleLabel.backgroundColor = [UIColor clearColor];
//        [view addSubview:titleLabel];
//        
//        return view;
//        
//    }
//    
//    return nil;
//}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == CommentCellSectionFollowComment) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(12, 0, ViewWidth-24, 30)];
        view.backgroundColor = [UIColor whiteColor];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(24, 15, 2, 17)];
        bgView.backgroundColor = NAVBAR_BGCOLOR;
        [view addSubview:bgView];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, bgView.top-1, ViewWidth, view.height)];
        titleLabel.font = FONT(17);
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = @"用户评论";
        titleLabel.backgroundColor = [UIColor clearColor];
        [titleLabel sizeToFit];
        [view addSubview:titleLabel];
        
        //        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, ViewWidth, 30)];
        //        titleLabel.text = @"用户评论";
        //        titleLabel.font = FONT(13);
        //        titleLabel.textColor = UIColorFromRGB(0x333333);
        //        titleLabel.backgroundColor = [UIColor clearColor];
        //        [view addSubview:titleLabel];
        
        return view;
        
    }
    
    return [UIView new];
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 广告条
    if (indexPath.section == CommentCellSectionAdvertisement) {
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%lu%lu", (long)indexPath.section, (long)indexPath.row];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            _bannerView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, ViewWidth)];
            _bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
            //            _bannerView.delegate = self;
            _bannerView.placeholderImage = DefaultPlaceholderImage;
            _bannerView.currentPageDotColor =  NAVBAR_BGCOLOR;
//            _bannerView.currentPageDotColor = [UIColor whiteColor];
            _bannerView.displayType = SDCycleScrollViewDisplayTypeExperience;
            _bannerView.downloadFromAliyunOSS = YES;
            [cell.contentView addSubview:_bannerView];
            
        }
        _bannerView.aliyunOSSImageArray = self.experienceViewModel.serviceModel.imgs;
        
        return cell;
    }
    // 用户评论
    if (indexPath.section == CommentCellSectionComment) {
        UserExperienceTableViewCell *cell = [UserExperienceTableViewCell momentsTableViewCellWithTableView:tableView];
        [cell setViewModel:self.experienceViewModel];
        cell.cellDelegate = self;
        
        NSString *thumbupImageName = [self.experienceViewModel.serviceModel.thumbsUp boolValue]?@"icon_thumup_highlighted":@"icon_thumbup";
        [cell.bodyView.likeButton setImage:[UIImage imageNamed:thumbupImageName] forState:UIControlStateNormal];
        
        self.avatarIconImage = cell.bodyView.iconView.image;
        
        return cell;
    }
    // 用户发表的评论
    if (indexPath.section == CommentCellSectionFollowComment) {
        ExperienceCommentViewModel *viewModel = self.followCommentList[indexPath.row];
        ExperienceCommentTableViewCell *cell = [ExperienceCommentTableViewCell momentsTableViewCellWithTableView:tableView];
        [cell setViewModel:viewModel];
        
        return cell;
    }
    
    return nil;
}


@end
