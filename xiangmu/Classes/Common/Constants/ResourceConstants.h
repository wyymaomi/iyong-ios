//
//  ResourceConstants.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef ResourceConstants_h
#define ResourceConstants_h

#pragma mark - plist
#define ORDER_STATUS_HEADER_PLIST @"OrderStatusHeader.plist"
#define ORDER_STATUS_CELL_PLIST     @"OrderStatusCell.plist"
#define HOME_CELL_PLIST @"HomeCell.plist"
#define CODE_MSG_PLIST @"CodeMsg.plist"

#pragma mark - STRING
#define STR_OK @"确定"
#define MSG_FAILED @"操作失败，请重试"
#define MSG_DECRYPT_FAILURE @"解密失败"
#define MSG_LOADING @"加载中，请稍候..."
#define MSG_REGISTER_SUCCESS @"注册成功"
#define MSG_REGISTER_FAILURE @"注册失败"
#define MSG_IS_REGISTERING @"正在注册，请稍候..."
#define MSG_IS_LOGING @"正在登录，请稍候..."
#define MSG_ERROR_INVALID_MOBILE @"手机号码格式有误，请重新输入"
#define MSG_ERROR_INVALID_REF_MOBILE @"推荐人手机号输入格式有误"
#define MSG_MOBILE_NULL @"请输入手机号"
#define MSG_VERIFY_CODE_NULL @"请输入验证码"
#define MSG_NEW_PWD_NULL @"请输入新密码"
#define MSG_RE_NEW_PWD_NULL @"请输入确认新密码"
#define MSG_TWICE_PWD_NOT_SAME @"两次输入密码不同，请重新输入"
#define MSG_GET_VERIFY_CODING @"正在获取验证码，请稍候..."
#define MSG_GET_VERIFY_FAILURE @"获取验证码失败，请重试"
#define MSG_RESET_PWDING @"正在重设密码，请稍候..."
#define MSG_RESET_PWD_SUCCESS @"重设密码成功，请返回"
#define MSG_RESET_PWD_FAILURE @"重设密码失败，请重试"
#define MSG_SET_PAY_PWD_SUCCESS @"设置支付成功，请返回"
#define MSG_PAY_PWD_NULL @"请输入支付密码"
#define MSG_INVALID_PAY_PWD @"请输入6位数字支付密码"
#define MSG_SET_PAY_PWD_SUCCESS @"设置支付密码成功"
#define MSG_SET_PAY_PWD_FAILURE @"设置支付密码失败"
#define MSG_UPDATE_LOGIN_PWD @"正在重设登录密码，请稍候..."
#define MSG_UPDATE_LOGIN_PWD_SUCCESS @"重设登录密码成功，请返回"
#define MSG_UPDATE_LOGIN_PWD_FAILURE @"重设登录密码失败，请返回"
#define MSG_SET_PAY_PWDING @"正在设置支付密码，请稍候..."
#define MSG_LOGIN_SUCCESS @"登录成功"
#define MSG_LOGIN_FAILURE @"登录失败"
#define MSG_LOGIN_TIP @"请先登录"
#define MSG_GET_CAR_LIST_SUCCESS @"获取汽车信息成功"
#define MSG_GET_CAR_LIST_FAILURE @"获取汽车信息失败"
#define MSG_SAVE_ORDER_SUCCESS @"保存订单成功，请返回"
#define MSG_SAVE_ORDER_FAILURE @"保存订单失败"
#define MSG_DISPATCH_ORDER_FAILURE @"派单失败"
#define MSG_NEW_ORDER @"正在添加订单，请稍候..."
#define MSG_DISPATCH_ORDER @"正在派单，请稍候..."
#define MSG_ORDER_DETAIL @"正在查询订单，请稍候..."

#define MSG_ENDTIME_MUST_BE_MORE_THAN_STARTTIME @"结束日期必须大于开始日期，请重新输入"
#define MSG_PLS_ENTER_LESS_THANK_SIX_MOTNHS_TIME_INTERVALS @"请输入六个月以内日期间隔"
#define ALERT_MSG_CARTIME_NULL @"请选择用车时间"
#define ALERT_MSG_START_LOCATION_NULL @"请输入出发地点"
#define ALERT_MSG_CUSTOMER_NULL @"请输入用车人"
#define ALERT_MSG_CUSTOMER_MOBILE_NULL @"请输入用车人电话"
#define ALERT_MSG_ROUGH_ITINERARY_NULL @"请输入大致行程"
#define ALERT_MSG_ORDER_REMARK_NULL @"请输入备注"
#define ALERT_MSG_DISPATCH_PRICE_NULL @"请输入派单价格"
#define ALERT_MSG_CHOOSE_DISPATCH_OBJECT @"请选择派单单位"
#define ALERT_MSG_CHOOSE_VECHILE @"请选择车辆"
#define ALERT_MSG_LOCATION_NULL @"请输入到达地点"
#define ALERT_MSG_DISTANCE_NULL @"请输入路程距离"
#define ALERT_MSG_ENDTIME_NULL @"请输入结束时间"
#define ALERT_MSG_REMARK_NULL @"请输入备注"
#define ALERT_MSG_SETTLEMENT_PRICE_NULL @"请输入结算价格"
#define ALERT_MSG_RATE_ID_NULL @"请选择付款方式"
#define STR_ORDER_REMARK @"请保持车辆清洁，正装、备水、纸巾、雨伞，路桥费停车费垫付，不签不收"
#define ALERT_MSG_CONTENT_LIMIT @"内容限制在30字以内！"
#define ALERT_MSG_NO_CONTENT @"没有输入，请重新填写"
#define MSG_SEND_SMS_SUCCESS @"发送短信成功"
#define MSG_SEND_SMS_FAILURE @"发送短信失败"


#endif /* ResourceConstants_h */
