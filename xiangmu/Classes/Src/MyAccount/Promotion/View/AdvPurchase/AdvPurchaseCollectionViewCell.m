//
//  AdvPurchaseCollectionViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/4/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPurchaseCollectionViewCell.h"

@implementation AdvPurchaseCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0x4c4c4c);
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 38*Scale, self.width, 16*Scale)];
        _titleLabel.textAlignment = UITextAlignmentCenter;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = BOLD_FONT(16*Scale);
        [self addSubview:_titleLabel];
        
        _purchaseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _purchaseButton.backgroundColor = UIColorFromRGB(0xe96e32);
        [_purchaseButton setTitle:@"点击购买" forState:UIControlStateNormal];
        [_purchaseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _purchaseButton.titleLabel.font = BOLD_FONT(14*Scale);
        _purchaseButton.frame = CGRectMake(27.5*Scale, _titleLabel.bottom+6*Scale, (self.frame.size.width-55*Scale)/2, 30*Scale);
        _purchaseButton.cornerRadius = 5;
        [self addSubview:_purchaseButton];
        
    }
 
    return self;
    
}

- (void)layoutSubviews
{
    
    [super layoutSubviews];
    
    
    self.titleLabel.frame = CGRectMake(0, 38*Scale, self.width, 16*Scale);
    
    self.purchaseButton.frame = CGRectMake(27*Scale, self.titleLabel.bottom+6*Scale, (self.width-54*Scale), 30*Scale);
        
    
}



@end
