//
//  AdPurchaseOrder.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

//    NSDictionary *dict = @{@"date": self.currentSelectDate,
//                           @"timeSegmentText": timeSegmentText,
//                           @"adId": adId,
//                           @"price": price};

@interface AdPurchaseOrder : JSONModel

@property (nonatomic, strong) NSString<Optional> *date;
@property (nonatomic, strong) NSString<Optional> *timeSegment;
@property (nonatomic, strong) NSString<Optional> *adId;
@property (nonatomic, strong) NSNumber<Optional> *price;
@property (nonatomic, strong) NSNumber<Optional> *timeIndex;

-(NSString*)displayDate;

@end
