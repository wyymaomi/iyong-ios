//
//  ProviderDetailViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderDetailViewModel.h"
#import "ProviderDetailModel.h"

@implementation ProviderDetailViewModel

-(void)initCompanyInfoFrame
{
    if (!_detailModel) {
        return;
    }
    
//    _detailModel = detailModel;
    
    _companyInfoSectionHeight = 70;
    
    [self initCompanyNameFrame];
    
    [self initToolBarFrame];
    
//    _company
}

-(void)initCompanyNameFrame;
{
    _companyNameFrame = CGRectMake(24, 14, ViewWidth-64-5, 20);
    _avatarImageFrame = CGRectMake(ViewWidth-24-40, (_companyInfoSectionHeight-40)/2, 40,40);
}

-(void)initToolBarFrame
{
    self.browsesFont = FONT(14);
    self.commentsFont = FONT(14);
    self.likesFont = FONT(14);
    
    CGFloat positionY = CGRectGetMaxY(_companyNameFrame) + 11;
    
    // 浏览量
    _browseNumImageFrame = CGRectMake(24, positionY, 17, 13);
    CGFloat browseTextWidth = [self.detailModel.browseQuantity textSize:CGSizeMake(ViewWidth, MAXFLOAT) font:self.browsesFont].width;
    CGFloat toolBrowseTextWdith = browseTextWidth<20?20:browseTextWidth;
//    CGFloat toolBrowseTextWidth = browseTextSize.width<20?20:browseTextSize.width;
    _browseNumTextFrame = CGRectMake(CGRectGetMaxX(_browseNumImageFrame)+5, positionY, toolBrowseTextWdith, 15);
    _commentNumImageFrame = CGRectMake(CGRectGetMaxX(_browseNumTextFrame)+10, positionY, 15, 15);
    CGFloat commentTextWidth = [self.detailModel.commentQuantity textSize:CGSizeMake(ViewWidth, MAXFLOAT) font:self.commentsFont].width;
    CGFloat toolCommentTextWidth = commentTextWidth<20?20:commentTextWidth;
    _commentNumTextFrame = CGRectMake(CGRectGetMaxX(_commentNumImageFrame)+5, positionY, toolCommentTextWidth, 15);
    _thumbUpNumImageFrame = CGRectMake(CGRectGetMaxX(_commentNumTextFrame)+10, positionY, 15, 15);
    CGFloat thumbUpTextWidth = [self.detailModel.thumbsQuantity textSize:CGSizeMake(ViewWidth, MAXFLOAT) font:self.likesFont].width;
    CGFloat toolThumbUpTextWidth = thumbUpTextWidth<20?20:thumbUpTextWidth;
    _thumbUpNumTextFrame = CGRectMake(CGRectGetMaxX(_thumbUpNumImageFrame)+5, positionY, toolThumbUpTextWidth, 15);
    
    
}

- (void)setDetailModel:(ProviderDetailModel *)detailModel
{
    _detailModel = detailModel;
    
    //    self.companyDescriptionSectionHeight =
    
    //    CGSize *size = [detailModel.blurb textSize:CGSizeMake(ViewWidth-48, 10000) font:FONT(15)];
    //    if (IsStrEmpty(detailModel.blurb)) {
    //        self.companyDescriptionSectionHeight = 15 + 30 +
    //    }
//    _descriptionText = _detailModel.blurb;
//    if (_detailModel.blurb.length == 0) {
//        _descriptionText = @"该公司暂无简介";
//    }
    //    self.companyDescriptionSectionHeight = 15 + 30 + 45;
    //    if (detailModel.blurb.length > 0) {
//    CGSize size = [[detailModel displayDescriptionText] textSize:CGSizeMake(ViewWidth-48, 10000) font:FONT(15)];
    


}

-(void)initCompanyDescription
{
    if (!_detailModel) {
        return;
    }
    
    [self initTitleFrame];
    
    [self initDescriptionFrame];
    
    _companyDescriptionSectionHeight = CGRectGetMaxY(self.bodyFrame) + 15;
}

- (void)initTitleFrame
{
    self.titleFrame = CGRectMake(0, 15, ViewWidth, 18);
}

- (void)initDescriptionFrame
{
    CGSize size = [[_detailModel displayDescriptionText] textSize:CGSizeMake(ViewWidth-27, 10000) font:FONT(15)];
    self.bodyFrame = CGRectMake(27, CGRectGetMaxY(self.titleFrame)+15, ViewWidth-27*2, size.height);
}

//- (void)initViewModel:(ProviderDetailModel*)detailModel
//{
//    _detailModel = detailModel;
//    
////    self.companyDescriptionSectionHeight =
//    
////    CGSize *size = [detailModel.blurb textSize:CGSizeMake(ViewWidth-48, 10000) font:FONT(15)];
////    if (IsStrEmpty(detailModel.blurb)) {
////        self.companyDescriptionSectionHeight = 15 + 30 +
////    }
//    _descriptionText = _detailModel.blurb;
//    if (_detailModel.blurb.length == 0) {
//        _descriptionText = @"该公司暂无简介";
//    }
////    self.companyDescriptionSectionHeight = 15 + 30 + 45;
////    if (detailModel.blurb.length > 0) {
//        CGSize size = [_descriptionText textSize:CGSizeMake(ViewWidth-48, 10000) font:FONT(15)];
//        self.companyDescriptionSectionHeight = size.height + 30 + 45;
////    }
//    
//    
//}

@end
