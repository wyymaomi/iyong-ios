//
//  NewOrder3ViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NewOrderViewController.h"
#import <YYAlert/YYAlertView.h>
#import "UIButton+Boostrap.h"
#import "BaseOrderTableViewCell.h"
#import "CustomActionSheetDatePicker.h"
#import "ActionSheetPicker.h"
#import "OrderManagerViewController.h"
#import "TextInputHelperViewController.h"
#import "LocationSearchViewController.h"

#import "OrderBaseInfoResponseModel.h"
#import "DispatchPriceResponseModel.h"
#import "NewOrderHttpMessage.h"
#import "UserManager.h"
#import "CarModel.h"
#import "UserHelper.h"
#import "OrderViewModel.h"

#import "InputHelperViewController.h"
#import "AppConfig.h"

@interface NewOrderViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, InputHelperDelegate, LocationSearchDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *dispatchButton;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UIView *updateView;
@property (weak, nonatomic) IBOutlet UIView *saveSubmitView;

@property (weak, nonatomic) IBOutlet UILabel *carTimeLabel;// 用车日期
@property (weak, nonatomic) IBOutlet UITextField *startLocationTextField;// 出发地点
@property (weak, nonatomic) IBOutlet UITextField *customNameTextField;// 用车人
@property (weak, nonatomic) IBOutlet UITextField *telephoneTextField;// 电话
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;// 接单价格
@property (weak, nonatomic) IBOutlet UITextView *itineraryTextField;// 大致行程
@property (weak, nonatomic) IBOutlet UITextView *memoTextField;// 备注

@property (strong, nonatomic) OrderBaseInfoResponseModel *orderBaseInfoModel;
@property (strong, nonatomic) DispatchPriceResponseModel *dispatchPriceModel;
@property (strong, nonatomic) NewOrderHttpMessage *orderHttpMessage;


@end

@implementation NewOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    if (self.mode == 0) {
        self.title = @"新建订单";
        self.saveSubmitView.hidden = NO;
        self.updateView.hidden = YES;
        self.orderId = @"";
    }
    else if (self.mode == 1) {
        self.title = @"编辑订单";
        self.saveSubmitView.hidden = YES;
        self.updateView.hidden = NO;

        [self initOrderContent];
    }
    [self initViews];
    
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAuthNotification) name:kUnauthorizeNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLoginSuccess) name:kLoginSuccessNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLoginSuccess) name:kGesturePwdSetSuccessNotification object:nil];
//}

-(void)initOrderContent
{
    
    NSDate *carDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([self.orderBaseModel.carTime doubleValue] / 1000.0f)];
    
    self.orderHttpMessage.carTime = [carDate stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.orderHttpMessage.location = self.orderBaseModel.location.trim;
    self.orderHttpMessage.customerName = self.orderBaseModel.customerName.trim;
    self.orderHttpMessage.customerMobile = self.orderBaseModel.customerMobile.trim;
    self.orderHttpMessage.itinerary = self.orderBaseModel.itinerary.trim;
    if ([self.orderBaseModel.price integerValue] > 0) {
        self.orderHttpMessage.price = self.orderBaseModel.price;
    }
//    self.orderHttpMessage.price = self.orderBaseModel.price.trim;
    self.orderHttpMessage.remark = self.orderBaseModel.remark.trim;
    self.orderHttpMessage.model = self.orderBaseModel.model.trim;
    
    [self.tableView reloadData];
    
}


-(NewOrderHttpMessage*)orderHttpMessage
{
    if (_orderHttpMessage == nil) {
        _orderHttpMessage = [NewOrderHttpMessage new];
//        _orderHttpMessage.areaCode = [AppConfig currentConfig].defaultCityCode;
    }
    return _orderHttpMessage;
}


- (void)initViews
{
    [self.saveButton blueStyle];
    [self.dispatchButton blueStyle];
    self.saveButton.tag = 1;
    self.dispatchButton.tag = 2;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
}

-(void)initData
{
//    if (self.bidModel && self.enquireModel) {
//        self.enquireModel
//    }
}

#pragma mark - UITableView Delegate Method
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width,10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }
    if (section == 1) {
        return 2;
    }
    if (section == 2) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BaseOrderTableViewCell *cell = [BaseOrderTableViewCell cellWithTableView:tableView];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"用车时间";
            cell.textField.placeholder = @"请选择用车时间";
            cell.textField.tag = OrderTextFieldCarTime;
            cell.textField.text = self.orderHttpMessage.carTime.trim;
            cell.textField.enabled = NO;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"车       型";
            cell.textField.placeholder = @"请输入需求车型";
            cell.textField.tag = OrderTextFieldCarModel;
            cell.textField.text = self.orderHttpMessage.model.trim;
            cell.textField.enabled = NO;
        }
        if (indexPath.row == 2) {
            cell.titleLabel.text = @"出发地点";
            cell.textField.placeholder = @"请输入出发地点";
            cell.textField.tag = OrderTextFieldStartLocation;
            cell.textField.text = self.orderHttpMessage.location.trim;
            cell.textField.enabled = NO;
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"用  车  人";
            cell.textField.placeholder = @"请输入用车人信息";
            cell.textField.tag = OrderTextFieldCustomerName;
            cell.textField.text = self.orderHttpMessage.customerName.trim;
            cell.textField.enabled = NO;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"电       话";
            cell.textField.placeholder = @"请输入用车电话";
            cell.textField.tag = OrderTextFieldCustomerMobile;
            cell.textField.text = self.orderHttpMessage.customerMobile.trim;
            cell.textField.enabled = NO;
        }
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"大致行程";
            cell.textField.placeholder = @"请输入大致行程";
            cell.textField.tag = OrderTextFieldItinerary;
            cell.textField.text = self.orderHttpMessage.itinerary.trim;
            cell.textField.enabled = NO;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"备       注";
            cell.textField.placeholder = @"请输入备注";
            cell.textField.tag = OrderTextFieldRemark;
            cell.textField.text = self.orderHttpMessage.remark.trim;
            cell.textField.enabled = NO;
        }
    }
    if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"接单价格";
            cell.textField.placeholder = @"请输入接单价格";
            cell.textField.tag = OrderTextFieldOriginPrice;
            cell.textField.text = self.orderHttpMessage.price.trim;
            cell.textField.enabled = NO;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        [self setCarDate:cell.textField];
        return;
    }
    else if (indexPath.section == 2  /*|| (indexPath.section == 0 && indexPath.row == 2)*/) {
        BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell != nil) {
            TextInputHelperViewController *viewController = [[TextInputHelperViewController alloc] init];
            viewController.tag = cell.textField.tag;
            viewController.delegate = self;
            viewController.title = cell.titleLabel.text;
            viewController.text = cell.textField.text;
            [self.navigationController pushViewController:viewController animated:YES];
            return;
        }
        
    }
    else if (indexPath.section == 0 && indexPath.row == 2) {
        BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell != nil) {
            LocationSearchViewController *viewController = [[LocationSearchViewController alloc] init];
//            viewController.delegate = self;
//            LocationSearchViewController *vc = [[LocationSearchViewController alloc] init];
            
            viewController.title = cell.titleLabel.text;
            
//            if (indexPath.row == 0) {
            
                WeakSelf
                viewController.selectLocationHandle = ^(NSString *text, NSString *cityId) {
                    weakSelf.orderHttpMessage.location = text;
                    weakSelf.orderHttpMessage.areaCode = cityId;
                    [weakSelf.tableView reloadData];
                };
                
//            }

            [self.navigationController pushViewController:viewController animated:YES];
            
            return;
        }
    }
    else {
        BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell != nil) {
            InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
            viewController.title = cell.titleLabel.text;
            viewController.text = cell.textField.text.trim;
            viewController.tag = cell.textField.tag;
            viewController.delegate = self;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    } 
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

#pragma mark - InputHelperDelegate method
-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
{
    if (text.trim.length > 0) {
        if (tag == OrderTextFieldStartLocation) {
            self.orderHttpMessage.location = text;
        }
        if (tag == OrderTextFieldCustomerName) {
            self.orderHttpMessage.customerName = text;
        }
        if (tag == OrderTextFieldCustomerMobile) {
            self.orderHttpMessage.customerMobile = text;
        }
        if (tag == OrderTextFieldItinerary) {
            self.orderHttpMessage.itinerary = text;
        }
        if (tag == OrderTextFieldRemark) {
            self.orderHttpMessage.remark = text;
        }
        if (tag == OrderTextFieldOriginPrice) {
            self.orderHttpMessage.price = text;
        }
        if (tag == OrderTextFieldCarModel) {
            self.orderHttpMessage.model = text;
        }
        [self.tableView reloadData];
        
    }
}

#pragma mark - Location Delegate method

//- (void)didSelectLocation:(NSString*)location;
//{
//    self.orderHttpMessage.location = location;
//    
//    [self.tableView reloadData];
//}

#pragma mark - UITextField Delegate method

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
//    if (textField.tag == OrderTextFieldCarTime) {
//        [self setCarDate];
//        return NO;
//    }
    
    return YES;
}

-(void)textFieldDidChange:(UITextField*)textField
{
    if (textField.text.trim.length > 0) {
        if (textField.tag == OrderTextFieldStartLocation) {
            self.orderHttpMessage.location = textField.text.trim;
        }
        if (textField.tag == OrderTextFieldCustomerName) {
            self.orderHttpMessage.customerName = textField.text.trim;
        }
        if (textField.tag == OrderTextFieldCustomerMobile) {
            self.orderHttpMessage.customerMobile = textField.text.trim;
        }
        if (textField.tag == OrderTextFieldItinerary) {
            self.orderHttpMessage.itinerary = textField.text.trim;
        }
        if (textField.tag == OrderTextFieldRemark) {
            self.orderHttpMessage.remark = textField.text.trim;
        }
        if (textField.tag == OrderTextFieldOriginPrice) {
            self.orderHttpMessage.price = textField.text.trim;
        }

    }
}

#pragma mark - business logic
- (void)setCarDate:(id)sender
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@"请选择日期和时间"
                                     doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
                                         weakSelf.orderHttpMessage.carTime = selectDateTime;
                                         [weakSelf.tableView reloadData];
                                     } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                         
                                     } origin:sender];
    
    
}

#pragma mark - 编辑订单
- (IBAction)onUpdateOrder:(id)sender {
    
    
    
//    if (IsStrEmpty(self.orderHttpMessage.carTime.trim)) {
//        [self showAlertView:ALERT_MSG_CARTIME_NULL];
//        return;
//    }
//    if (IsStrEmpty(self.orderHttpMessage.location.trim)) {
//        [self showAlertView:ALERT_MSG_START_LOCATION_NULL];
//        return;
//    }
//    if (IsStrEmpty(self.orderHttpMessage.itinerary.trim)) {
//        [self showAlertView:ALERT_MSG_ROUGH_ITINERARY_NULL];
//        return;
//    }
//    if (IsStrEmpty(self.orderHttpMessage.customerName.trim)) {
//        [self showAlertView:ALERT_MSG_CUSTOMER_NULL];
//        return;
//    }
    
    NSString *errorMsg;
    
    if (![OrderViewModel validateNewOrder:self.orderHttpMessage error:&errorMsg]) {
        [YYAlertView showAlertView:@"" message:errorMsg cancleButtonTitle:@"确定" okButtonTitle:nil completeBlock:nil];
        return;
    }
    
    if (![UserHelper validatePhoneNum:self.orderHttpMessage.customerMobile.trim error:&errorMsg]) {
        [self showAlertView:errorMsg];
        return;
    }
    


    
    
#if 1
    
    self.nextAction = @selector(onUpdateOrder:);
    self.object1 = sender;
    self.object2 = nil;
    
    NSString *params = [NSString stringWithFormat:@"%@&id=%@", self.orderHttpMessage.description, self.orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_UPDATE_ORDER];
    
    WeakSelf
    
//    [self showHUDIndicatorViewAtCenter:MSG_UPDATE_OREDER];
    
    [self showHUDIndicatorViewAtCenter:@"正在更新订单信息，请稍候"];
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            if ([responseData[@"data"] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *data = responseData[@"data"];
                if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(onUpdateOrderInfo:)]) {
                    [strongSelf.delegate onUpdateOrderInfo:data];
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
            }
        }
        else {
            [strongSelf showAlertView:MSG_DISPATCH_ORDER_FAILURE];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
#endif
    

}



#pragma mark - 派单
- (IBAction)doDispatchOrder:(id)sender {
    
    NSString *errorMsg;
    
    if (![OrderViewModel validateNewOrder:self.orderHttpMessage error:&errorMsg]) {
        [YYAlertView showAlertView:@"" message:errorMsg cancleButtonTitle:@"确定" okButtonTitle:nil completeBlock:nil];
        return;
    }
    
    if (![UserHelper validatePhoneNum:self.orderHttpMessage.customerMobile.trim error:&errorMsg]) {
        [self showAlertView:errorMsg];
        return;
    }
    
    if (IsStrEmpty(self.orderHttpMessage.itinerary.trim)) {
        [self showAlertView:ALERT_MSG_ROUGH_ITINERARY_NULL];
        return;
    }
//    if (IsStrEmpty(self.orderHttpMessage.price.trim)) {
//        [self showAlertView:ALERT_MSG_DISPATCH_PRICE_NULL];
//        return;
//    }
    
//    [self resetAction];
//    if ([self needLogin]) {
//
//        return;
//    }
    
//    self.nextAction = @selector(doDispatchOrder:);
//    self.object1 = sender;
//    self.object2 = nil;
    

    
    [self dispatchOrder];
//    [self checkAccountBalance];
 

}

- (void)dispatchOrder
{
    
    self.nextAction = @selector(dispatchOrder);
    self.object1 = nil;
    self.object2 = nil;
    
#if 1
    NSString *params = self.orderHttpMessage.description;
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_SAVE_API];
    
    WeakSelf
    
    [self showHUDIndicatorViewAtCenter:MSG_NEW_ORDER];
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *array = responseData[@"data"];
            NSInteger orderStatus;
            
            if (!IsArrEmpty(array)) {
                
                for (NSDictionary *dict in array) {
                    
                    NSUInteger status = [dict[@"status"] integerValue];
                    NSDictionary *vo = dict[@"vo"];
                    
                    if (status == 1) {
                        strongSelf.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusNew;
                    }
                    if (status == 2) {
                        strongSelf.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusDispatch;
                    }
                }
                
            }
            
            if (strongSelf.source_from == SOURCE_FROM_COMPANY_COMMUNITY) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"发布订单成功，请返回" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                    
                }];
                
            }
            else {
                OrderManagerViewController *vc = [[OrderManagerViewController alloc] init];
                vc.title = @"订单";
                vc.source_from = SOURCE_FROM_NEW_ORDER;
                vc.orderStatus = orderStatus;
                vc.orderBaseInfoModel = strongSelf.orderBaseInfoModel;
                vc.dispatchPriceModel = strongSelf.dispatchPriceModel;
                [strongSelf.navigationController pushViewController:vc animated:YES];
            }
            
            
            
        }
        else {
            [strongSelf showAlertView:MSG_DISPATCH_ORDER_FAILURE];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
#endif

}

#pragma mark - 保存订单
- (IBAction)doSaveOrder:(id)sender {
    
    if (IsStrEmpty(self.orderHttpMessage.carTime.trim)) {
        [self showAlertView:ALERT_MSG_CARTIME_NULL];
        return;
    }
    if (IsStrEmpty(self.orderHttpMessage.location.trim)) {
        [self showAlertView:ALERT_MSG_START_LOCATION_NULL];
        return;
    }
    if (IsStrEmpty(self.orderHttpMessage.itinerary.trim)) {
        [self showAlertView:ALERT_MSG_ROUGH_ITINERARY_NULL];
        return;
    }
    if (IsStrEmpty(self.orderHttpMessage.customerName.trim)) {
        [self showAlertView:ALERT_MSG_CUSTOMER_NULL];
        return;
    }
    //    if (IsStrEmpty(self.orderHttpMessage.customerMobile.trim)) {
    //        [self showAlertView:ALERT_MSG_CUSTOMER_MOBILE_NULL];
    //        return;
    //    }
    
    NSString *errorMsg;
    if (![UserHelper validatePhoneNum:self.orderHttpMessage.customerMobile.trim error:&errorMsg]) {
        [self showAlertView:errorMsg];
        return;
    }
    
    if (IsStrEmpty(self.orderHttpMessage.itinerary.trim)) {
        [self showAlertView:ALERT_MSG_ROUGH_ITINERARY_NULL];
        return;
    }
    //    if (IsStrEmpty(self.orderHttpMessage.price.trim)) {
    //        [self showAlertView:ALERT_MSG_DISPATCH_PRICE_NULL];
    //        return;
    //    }
    
    [self resetAction];
    if ([self needLogin]) {
        self.nextAction = @selector(doSaveOrder:);
        self.object1 = sender;
        self.object2 = nil;
        return;
    }

    
#if 1
    NSString *params = self.orderHttpMessage.description;
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_SAVE_API];
    
    WeakSelf
    
    [self showHUDIndicatorViewAtCenter:MSG_NEW_ORDER];
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *array = responseData[@"data"];
            NSInteger orderStatus;
            
            if (!IsArrEmpty(array)) {
                
                for (NSDictionary *dict in array) {
                    
                    NSUInteger status = [dict[@"status"] integerValue];
                    NSDictionary *vo = dict[@"vo"];
                    
                    if (status == 1) {
                        strongSelf.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusNew;
                    }
                    if (status == 2) {
                        strongSelf.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusDispatch;
                    }
                }
                
            }
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:MSG_SAVE_ORDER_SUCCESS delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
            
        }
        else {
            [strongSelf showAlertView:MSG_DISPATCH_ORDER_FAILURE];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
#endif
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
