//
//  HomeCollectionView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/15.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HomeCollectionView.h"
#import "AWCollectionViewDialLayout.h"
#import "CommunityHelper.h"
#import "HomeCollectionViewCell.h"

@interface HomeCollectionView ()<UICollectionViewDataSource, UICollectionViewDelegate>
{

    CGFloat cell_height;
    int type;
}

@property (nonatomic, strong) NSArray *items;

@end

static NSString *cellId = @"HomeCollectionCellId";

@implementation HomeCollectionView

-(NSArray*)items
{
    if (!_items) {
        CommunityHelper *helper = [CommunityHelper new];
        _items = helper.vechileServiceList;
    }
    return _items;
}

#pragma mark - life cycle

-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    
    if (self) {
    
        type = 0;
        

        
//        dialLayout = [[AWCollectionViewDialLayout alloc] initWithRadius:radius andAngularSpacing:angularSpacing andCellSize:CGSizeMake(cell_width, cell_height) andAlignment:WHEELALIGNMENTCENTER andItemHeight:cell_height andXOffset:xOffset];
//        [dialLayout setShouldSnap:YES];
//        [self setCollectionViewLayout:dialLayout];
//        dialLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        [self registerClass:[HomeCollectionViewCell class] forCellWithReuseIdentifier:cellId];
        
//        [self registerNib:[UINib nibWithNibName:@"dialCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellId];
//        [self registerClass:[FirstCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        
        self.delegate=self;
        self.dataSource=self;
        self.scrollEnabled=NO;
        
    }
    
    return self;
}

-(void)switchExample{
//    type = (int)exampleSwitch.selectedSegmentIndex;
    CGFloat radius = 0 ,angularSpacing  = 0, xOffset = 0;
    
//    if(type == 0){
//        [dialLayout setCellSize:CGSizeMake(340, 100)];
//        [dialLayout setWheelType:WHEELALIGNMENTLEFT];
//        [dialLayout setShouldFlip:NO];
    
        radius = 300;
        angularSpacing = 18;
        xOffset = 70;
//    }else if(type == 1){
//        [dialLayout setCellSize:CGSizeMake(260, 50)];
//        [dialLayout setWheelType:WHEELALIGNMENTCENTER];
//        [dialLayout setShouldFlip:YES];
//        
//        radius = 320;
//        angularSpacing = 5;
//        xOffset = 124;
//        
//        
//        
//    }
    
//    [radiusLabel setText:[NSString stringWithFormat:@"Radius: %i", (int)radius]];
//    radiusSlider.value = radius/1000;
//    [dialLayout setDialRadius:radius];
    
//    [angularSpacingLabel setText:[NSString stringWithFormat:@"Angular spacing: %i", (int)angularSpacing]];
//    angularSpacingSlider.value = angularSpacing / 90;
//    [dialLayout setAngularSpacing:angularSpacing];
    
//    [xOffsetLabel setText:[NSString stringWithFormat:@"X offset: %i", (int)xOffset]];
//    xOffsetSlider.value = xOffset/320;
//    [dialLayout setXOffset:xOffset];
    
    
    [self reloadData];
    
}

#pragma mark - UICollectionView Delegate method

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.items.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    UICollectionViewCell *cell;
    HomeCollectionViewCell *cell;
//    if(type == 0){
        cell = [cv dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
//    }else{
//        cell = [cv dequeueReusableCellWithReuseIdentifier:cellId2 forIndexPath:indexPath];
//    }
    
    NSDictionary *item = [self.items objectAtIndex:indexPath.item];
    
    cell.textLabel.text = item[@"title"];
    cell.iconImageView.image = [UIImage imageNamed:item[@"icon"]];
    
    
    return cell;
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *item = [self.items objectAtIndex:indexPath.item];
    NSString *link = [item valueForKey:@"url"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
}



- (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    return hexInt;
}

-(UIColor*)colorFromHex:(NSString*)hexString{
    unsigned int hexint = [self intFromHexString:hexString];
    
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:1];
    
    return color;
}

//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}





@end
