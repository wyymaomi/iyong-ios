//
//  VechileCertificationViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"


typedef NS_ENUM(NSInteger, SectionTag)
{
    SectionTagDriverLicenseNumber = 0,
//    SectionTagDriverLicenseGetDate,
//    SectionTagDriverLicenseValidateDate,
    SectionTagVechileLicenseGetDate,
//    SectionTagVechileLicenseValidateDate,
    SectionTagIDType
//    SectionTagIDNumber,
//    SectionTagIDValidateDate
    
};

typedef NS_ENUM(NSInteger, ImageViewTag) {
    ImageViewTagDriverLicense = 0,
    ImageViewTagVechileLicense,
    ImageViewTagIdcard,
    ImageViewTagIdcardBack
};

@interface VechileCertificationViewController : BaseViewController

@property (nonatomic, strong) NSString *carId;

@property (nonatomic, assign) BOOL bEditMode; // 是否编辑模式

@end
