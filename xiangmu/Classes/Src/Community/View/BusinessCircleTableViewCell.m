//
//  BusinessCircleTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BusinessCircleTableViewCell.h"
#import "DateUtil.h"

@implementation BusinessCircleTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        
        [self.contentView addSubview:self.countDownView];
        [self.contentView addSubview:self.userInfoView];
        [self.contentView addSubview:self.startTimeView];
        [self.contentView addSubview:self.endTimeView];
        [self.contentView addSubview:self.startLocationView];
        [self.contentView addSubview:self.endLocationView];
        [self.contentView addSubview:self.vechileModelView];
        [self.contentView addSubview:self.vechileNumberView];
        [self.contentView addSubview:self.roughItineraryView];
        [self.contentView addSubview:self.fullTextButton];
        [self.contentView addSubview:self.thirdPayLabel];
        [self.contentView addSubview:self.elapseTimeLabel];
        [self.contentView addSubview:self.statusButton];
        [self.contentView addSubview:self.publishPriceButton];
        
        self.roughItineraryView.hidden = YES;
        self.thirdPayLabel.hidden = YES;
  
    }

    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self adjustBaseInfoPosition];
    
    [self adjustCollapsePosition];

}

-(void)adjustBaseInfoPosition
{
    self.countDownView.top = 15;
    self.userInfoView.top = self.countDownView.bottom+2;
    self.startTimeView.top = self.userInfoView.bottom+2;
    self.endTimeView.top = self.startTimeView.bottom;
    self.startLocationView.top = self.endTimeView.bottom;
    self.endLocationView.top = self.startLocationView.bottom;
    self.vechileModelView.top = self.endLocationView.bottom;
    self.vechileNumberView.top = self.vechileModelView.bottom;
    
    self.roughItineraryView.top = self.vechileNumberView.bottom;
    self.thirdPayLabel.top = self.roughItineraryView.bottom;
    self.fullTextButton.top = self.vechileNumberView.bottom;
    self.elapseTimeLabel.top = self.vechileModelView.top + 3 * kLineHeight;
    self.publishPriceButton.bottom = self.elapseTimeLabel.bottom-5;
}

- (void)adjustCollapsePosition
{
    
    if (self.enquireModel == nil) {
        return;
    }
    
    BOOL isOpen = [self.enquireModel.isOpen boolValue];
    if (isOpen) {
        self.roughItineraryView.hidden = NO;
        self.thirdPayLabel.hidden = NO;
        
        CGSize labelSize = [self.enquireModel.itinerary textSize:CGSizeMake(ViewWidth-kTextLabelLeft-kLineRightInset, 10000) font:self.roughItineraryLabel.font];
        self.roughItineraryView.height = labelSize.height + kLineHeight;
        [self.roughItineraryLabel sizeToFit];
        self.roughItineraryLabel.top = 2;
        
        if (labelSize.height < kLineHeight) {
            self.thirdPayLabel.top = self.roughItineraryView.top + kLineHeight;
            self.fullTextButton.top = self.roughItineraryView.top + kLineHeight*2;
        }
        else {
            self.thirdPayLabel.top = self.roughItineraryView.top + labelSize.height + 5;
            self.fullTextButton.top = self.roughItineraryView.top + self.roughItineraryLabel.height + kLineHeight;
        }
        
        [self.fullTextButton setTitle:@"收起" forState:UIControlStateNormal];
        self.elapseTimeLabel.top = self.fullTextButton.top + kLineHeight + 5;
        self.publishPriceButton.bottom = self.elapseTimeLabel.bottom-5;
    }
    else {
        self.roughItineraryView.hidden = YES;
        self.thirdPayLabel.hidden = YES;
        
        [self.fullTextButton setTitle:@"展开" forState:UIControlStateNormal];
        
        self.roughItineraryView.hidden = YES;
        self.fullTextButton.top = self.vechileNumberView.bottom;
        self.elapseTimeLabel.top = self.fullTextButton.top + kLineHeight + 5;
        self.publishPriceButton.bottom = self.elapseTimeLabel.bottom-5;
        
    }
}

+(CGFloat)getCellHeight:(EnquireModel*)data
{
    CGFloat height = 258;
    
    if (![data.isOpen boolValue]) {
        
        return height;
    
    }
    
    if (IsStrEmpty(data.itinerary)) {
        
        height = height + kLineHeight * 2;
        
        return height;
        
    }
    
    CGSize labelSize = [data.itinerary textSize:CGSizeMake(ViewWidth-kTextLabelLeft-kLineRightInset, MAXFLOAT) font:FONT(13)];
    
    height = height + labelSize.height + 10 + kLineHeight;
    
    return height;
    
}

-(void)initData:(EnquireModel*)data
{
    
    self.enquireModel = data;
    
    self.nicknameLabel.text = [StringUtil getSafeString:data.nickname.trim];
    self.startDateTimeLabel.text = [DateUtil formatDateTime:data.startTime dateFomatter:@"yyyy-MM-dd HH:mm"];
    self.endDateTimeLabel.text = [DateUtil formatDateTime:data.endTime dateFomatter:@"yyyy-MM-dd HH:mm"];
    self.startLocationLabel.text = [StringUtil getSafeString:data.startPlace];
    self.endLocationLabel.text = data.endPlace;
    self.vechileModelLabel.text = data.model;
    self.vechileNumberLabel.text = data.quantity;
    self.elapseTimeLabel.text = [DateUtil timerAgo:data.createTime];
    self.roughItineraryLabel.text = data.itinerary;
    if ([_enquireModel.advance boolValue]) {
        self.thirdPayLabel.text = @"需要第三方包含垫付费用";
    }
    else {
        self.thirdPayLabel.text = @"不需要第三方包含垫付费用";
    }
    
    NSTimeInterval inquireEndTime = [data.createTime doubleValue] + [data.releaseTime doubleValue] * 60 * 1000;
    BOOL isEnd = [data checkIsEnd];
    if (isEnd) {
        [self.statusButton grayStyle];
//        [self.statusButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xE5C338)] forState:UIControlStateNormal];
        [self.statusButton setTitle:@"已结束" forState:UIControlStateNormal];
        self.hourElapseTimeLabel.text = @"00";
        self.minuteElapseTimeLabel.text = @"00";
        self.secondElapseTimeLabel.text = @"00";
    }
    else {
        [self.statusButton yellowStyle];
//        [self.statusButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x215a83)] forState:UIControlStateNormal];
        [self.statusButton setTitle:@"询价中..." forState:UIControlStateNormal];
        
        NSString *hour;
        NSString *minute;
        NSString *second;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)(inquireEndTime/1000)];
        [date timeLeft:[NSDate date] hour:&hour minute:&minute second:&second];
        
        self.hourElapseTimeLabel.text = hour;
        self.minuteElapseTimeLabel.text = minute;
        self.secondElapseTimeLabel.text = second;
        
    }
    
    [self setNeedsLayout];
    
//    self.isOpen = [data.isOpen boolValue];
    
//    [self setSubviewsFrame:data];
    
    
}

#pragma mark - getter and setter

- (UIView*)startTimeView
{
    if (_startTimeView == nil) {
        _startTimeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, kLineHeight)];
        
        // 图片
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 2, 14, 19)];
        iconImageView.image = [UIImage imageNamed:@"icon_car_time"];
        [_startTimeView addSubview:iconImageView];
        
        // label
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(39, 0, 73, kLineHeight)];
        label.font = FONT(13);
        label.backgroundColor = [UIColor clearColor];
        label.text = @"出发时间：";
        label.textColor = UIColorFromRGB(0x808080);
        [_startTimeView addSubview:label];
        
        UILabel *textLabel = [UILabel new];
        textLabel.frame = CGRectMake(kTextLabelLeft, 0, ViewWidth-kTextLabelLeft-kLineRightInset, kLineHeight);
        textLabel.font = FONT(13);
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.textColor = UIColorFromRGB(0x555555);
        [_startTimeView addSubview:textLabel];
        _startDateTimeLabel = textLabel;
        
    }
    return _startTimeView;
}

- (UIView*)endTimeView
{
    if (!_endTimeView) {
        
        _endTimeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, kLineHeight)];
        
        // 图片
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 2, 14, 19)];
        iconImageView.image = [UIImage imageNamed:@"icon_end_time"];
        [_endTimeView addSubview:iconImageView];
        
        // label
        UILabel *label = [UILabel new];
        label.frame = CGRectMake(39, 0, 73, kLineHeight);
        label.font = FONT(13);
        label.text = @"结束时间：";
        label.textColor = UIColorFromRGB(0x808080);
        label.backgroundColor = [UIColor clearColor];
        [_endTimeView addSubview:label];
        
        // 结束时间
        UILabel *textLabel = [UILabel new];
        textLabel.frame = CGRectMake(kTextLabelLeft, 0, ViewWidth-kTextLabelLeft-kLineRightInset, kLineHeight);
        textLabel.font = FONT(13);
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.textColor = UIColorFromRGB(0x555555);
        [_endTimeView addSubview:textLabel];
        _endDateTimeLabel = textLabel;
        
    }
    return _endTimeView;
}

- (UIView*)startLocationView
{
    if (_startLocationView == nil) {
        
        _startLocationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, kLineHeight)];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 2, 14, 19)];
        iconImageView.image = [UIImage imageNamed:@"icon_start"];
        [_startLocationView addSubview:iconImageView];
        
        // label
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(39, 0, 73, kLineHeight)];
        label.font = FONT(13);
        label.text = @"出发地点：";
        label.textColor = UIColorFromRGB(0x808080);
        label.backgroundColor = [UIColor clearColor];
        [_startLocationView addSubview:label];
        
        // 出发地点
        UILabel *textLabel = [UILabel new];
        textLabel.frame = CGRectMake(kTextLabelLeft, 0, ViewWidth-kTextLabelLeft-kLineRightInset, kLineHeight);
        textLabel.font = FONT(13);
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.textColor = UIColorFromRGB(0x555555);
        [_startLocationView addSubview:textLabel];
        _startLocationLabel = textLabel;
        
    }
    
    return _startLocationView;
}

-(UIView*)endLocationView
{
    if (_endLocationView == nil) {
        
        _endLocationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, kLineHeight)];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 2, 14, 19)];
        iconImageView.image = [UIImage imageNamed:@"icon_arrive"];
        [_endLocationView addSubview:iconImageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(39, 0, 73, kLineHeight)];
        label.font = FONT(13);
        label.text = @"结束地点：";
        label.textColor = UIColorFromRGB(0x808080);
        label.backgroundColor = [UIColor clearColor];
        [_endLocationView addSubview:label];
        
        // 结束地点
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTextLabelLeft, 0, ViewWidth-kTextLabelLeft-kLineRightInset, kLineHeight)];
        textLabel.font = FONT(13);
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.textColor = UIColorFromRGB(0x555555);
        [_endLocationView addSubview:textLabel];
        _endLocationLabel = textLabel;
        
    }
    return _endLocationView;
}

- (UIView*)vechileModelView
{
    if (_vechileModelView == nil) {
        
        _vechileModelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, kLineHeight)];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 0, 16, 24)];
        iconImageView.image = [UIImage imageNamed:@"icon_car_model"];
        [_vechileModelView addSubview:iconImageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(39, 0, 73, kLineHeight)];
        label.font = FONT(13);
        label.text = @"车       型:";
        label.textColor = UIColorFromRGB(0x808080);
        label.backgroundColor = [UIColor clearColor];
        [_vechileModelView addSubview:label];
        
        // 车型
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTextLabelLeft, 0, ViewWidth-kTextLabelLeft-kLineRightInset, kLineHeight)];
        textLabel.font = FONT(13);
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.textColor = UIColorFromRGB(0x555555);
        [_vechileModelView addSubview:textLabel];
        _vechileModelLabel = textLabel;
        
        
    }
    return _vechileModelView;
}

- (UIView*)vechileNumberView
{
    if (_vechileNumberView == nil) {
        
        _vechileNumberView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, kLineHeight)];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 2, 12, 15)];
        iconImageView.image = [UIImage imageNamed:@"icon_bid_vechile_number"];
        [_vechileNumberView addSubview:iconImageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(39, 0, 73, kLineHeight)];
        label.text = @"数       量:";
        label.textColor = [UIColor darkTextColor];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = UIColorFromRGB(0x808080);
        label.font = FONT(13);
        [_vechileNumberView addSubview:label];
        
        // 数量
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTextLabelLeft, 0, ViewWidth-kTextLabelLeft-kLineRightInset, kLineHeight)];
        textLabel.font = FONT(13);
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.textColor = UIColorFromRGB(0x555555);
        [_vechileNumberView addSubview:textLabel];
        _vechileNumberLabel = textLabel;
        
    }
    
    return _vechileNumberView;
}

-(UIView*)roughItineraryView
{
    if (_roughItineraryView == nil) {
        
        _roughItineraryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, kLineHeight)];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 2, 15, 18)];
        iconImageView.image = [UIImage imageNamed:@"icon_itinerary"];
        [_roughItineraryView addSubview:iconImageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(39, 0, 73, kLineHeight)];
        label.text = @"大致行程：";
        label.textColor = UIColorFromRGB(0x808080);
        label.backgroundColor = [UIColor clearColor];
        label.font = FONT(13);
        [_roughItineraryView addSubview:label];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTextLabelLeft, 0, ViewWidth-kTextLabelLeft-kLineRightInset, kLineHeight)];
        textLabel.font = FONT(13);
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.textColor = UIColorFromRGB(0x555555);
        textLabel.numberOfLines = 0;
        textLabel.lineBreakMode = NSLineBreakByCharWrapping;
        [_roughItineraryView addSubview:textLabel];
        _roughItineraryLabel = textLabel;
        
    }
    
    return _roughItineraryView;
}

-(UILabel*)thirdPayLabel
{
    if (_thirdPayLabel == nil) {
        
        _thirdPayLabel = [[UILabel alloc] initWithFrame:CGRectMake(39, 0, ViewWidth-39, kLineHeight)];
        _thirdPayLabel.textColor = UIColorFromRGB(0x808080);
        _thirdPayLabel.font = FONT(13);
        _thirdPayLabel.backgroundColor = [UIColor clearColor];
        
    }
    
    return _thirdPayLabel;
}

- (UIButton*)fullTextButton
{
    if (_fullTextButton == nil) {
        
        _fullTextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_fullTextButton setTitle:@"全文" forState:UIControlStateNormal];
        _fullTextButton.titleLabel.font = FONT(13);
        [_fullTextButton setTitleColor:UIColorFromRGB(0x2A528D) forState:UIControlStateNormal];
        _fullTextButton.backgroundColor = [UIColor clearColor];
        _fullTextButton.frame = CGRectMake(0, 0, 69, 30);
    }
    return _fullTextButton;
}

-(UILabel*)elapseTimeLabel
{
    if (_elapseTimeLabel == nil) {
        
        _elapseTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, ViewWidth, kLineHeight)];
        _elapseTimeLabel.textColor = UIColorFromRGB(0x808080);
        _elapseTimeLabel.font = FONT(13);
        _elapseTimeLabel.backgroundColor = [UIColor clearColor];
    }
    return _elapseTimeLabel;
}

- (UIButton*)statusButton
{
    if (_statusButton == nil) {
        
        NSInteger buttonWidth = 63;
        NSInteger buttonHeight = 25;
        _statusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _statusButton.frame = CGRectMake(ViewWidth-kLineRightInset-buttonWidth, 50, buttonWidth, buttonHeight);
        _statusButton.titleLabel.font = FONT(13);
        
    }
    return _statusButton;
}

- (UIView*)countDownView
{
    if (_countDownView == nil) {
        
        NSInteger subviewWidth = 205;
        NSInteger subviewHeight = 23;
        
        _countDownView = [[UIView alloc] initWithFrame:CGRectMake(ViewWidth-subviewWidth-kLineRightInset, 10, ViewWidth, subviewHeight)];
        
        UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, subviewWidth, subviewHeight)];
        bgImageView.image = [UIImage imageNamed:@"img_count_down"];
        [_countDownView addSubview:bgImageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 111, subviewHeight)];
        label.textColor = [UIColor whiteColor];
        label.text = @"离询价时间结束";
        label.font = FONT(13);
        label.textAlignment = UITextAlignmentRight;
        [_countDownView addSubview:label];
        
        UILabel *hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(127, 0, 20, subviewHeight)];
        hourLabel.textColor = [UIColor whiteColor];
        hourLabel.font = FONT(13);
        [_countDownView addSubview:hourLabel];
        _hourElapseTimeLabel = hourLabel;
        
        UILabel *minuteLabel = [[UILabel alloc] initWithFrame:CGRectMake(155, 0, 20, subviewHeight)];
        minuteLabel.textColor = [UIColor whiteColor];
        minuteLabel.font = FONT(13);
        [_countDownView addSubview:minuteLabel];
        _minuteElapseTimeLabel = minuteLabel;
        
        UILabel *secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(183, 0, 20, subviewHeight)];
        secondLabel.textColor = [UIColor whiteColor];
        secondLabel.font = FONT(13);
        [_countDownView addSubview:secondLabel];
        _secondElapseTimeLabel = secondLabel;
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(145, 0, 20, subviewHeight)];
        label1.text = @":";
        label1.textColor = [UIColor whiteColor];
        label1.textAlignment = UITextAlignmentLeft;
        label1.font = FONT(13);
        [_countDownView addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(174, 0, 20, subviewHeight)];
        label2.text = @":";
        label2.textColor = [UIColor whiteColor];
        label2.textAlignment = UITextAlignmentLeft;
        label2.font = FONT(13);
        [_countDownView addSubview:label2];
        
    }
    
    return _countDownView;
}

- (UIButton*)publishPriceButton
{
    
    if (_publishPriceButton == nil) {
        
        NSInteger buttonWidth = 48;
        NSInteger buttonHeight = 48;
        
        _publishPriceButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        _publishPriceButton.frame = CGRectMake(ViewWidth-kLineRightInset-buttonWidth, 0, buttonWidth, buttonHeight);
        [_publishPriceButton setTitle:@"报价" forState:UIControlStateNormal];
        [_publishPriceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _publishPriceButton.cornerRadius = buttonWidth/2;
        _publishPriceButton.backgroundColor = UIColorFromRGB(0x22a02b);
//        [_publishPriceButton setBackgroundImage:[UIImage imageNamed:@"icon_more"] forState:UIControlStateNormal];
        
    }
    
    return _publishPriceButton;
    
}

- (UIView*)userInfoView
{
    if (!_userInfoView) {
        _userInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, ViewWidth, 40)];
        
        UIImageView *avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 2, 36, 36)];
        avatarImageView.image = [UIImage imageNamed:@"icon_avatar"];
        avatarImageView.cornerRadius = 18;
        [_userInfoView addSubview:avatarImageView];
        _avatarImageView = avatarImageView;
        
        UILabel *nickNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(avatarImageView.right+12, 0, _userInfoView.width-avatarImageView.right-12-65, _userInfoView.height)];
        nickNameLabel.textAlignment = UITextAlignmentLeft;
        nickNameLabel.textColor = UIColorFromRGB(0x907A6A);
        nickNameLabel.font = FONT(13);
        _nicknameLabel.backgroundColor = [UIColor clearColor];
        [_userInfoView addSubview:nickNameLabel];
        _nicknameLabel = nickNameLabel;
        
    }
    return _userInfoView;
}



@end
