//
//  UIImageView+Download.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UIImageView+Download.h"
#import "NSData+ImageContentType.h"
#import "NetworkManager.h"
#import "AliyunOSSManager.h"
#import "ImageUtil.h"
#import "YYImageDownloader.h"

static char imageURLKey;
static char loadOperationKey;

@implementation UIImageView (Download)

- (void)downloadAdvImage:(NSString*)imageUrl
        placeholderImage:(UIImage*)placeholderImage
                 success:(SuccessBlock)success
              andFailure:(FailureBlock)failure
{
    if (IsStrEmpty(imageUrl)) {
        return;
    }
    
    // 如果本地已经缓存图片
    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
    
    NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    
    NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
    
    if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
        
        self.image = [UIImage imageWithData:imageData];
        
        if (success) {
            success(imageData);
        }
        
        return;
    }
    else {
        
        if (placeholderImage != nil) {
            
            __weak __typeof(self)wself = self;
            
            dispatch_main_async_safe(^{
                
                wself.image = placeholderImage;
                
            });
        }
        
    }
    
    // 如果本地没有缓存图片 则保存到本地
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, DONWLOAD_IMAGE_API_ENCRYPT ];
    
    NSString *params = [NSString stringWithFormat:@"url=%@", imageUrl];
    
    __weak __typeof(self)wself = self;
    
    [[NetworkManager sharedInstance] downloadImageData:url requestMethod:POST params:params success:^(id responseData) {
        
        if (!wself) return;
        
        if ([responseData isKindOfClass:[NSData class]]) {
            
            if (!responseData) {
                return;
            }
            
            NSData *imageData = responseData;
            
            // 判断是否为jpg或者png格式图片
            NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
            
            if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                
                [[FileCache defaultCache] saveData:imageData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                
                //                dispatch_main_async_safe(^{
                
                wself.image = [UIImage imageWithData:imageData];
                
                //                    [self setNeedsLayout];
                
                //                });
                
                if (success) {
                    
                    success(imageData);
                }
                
            }
            else {
                
                if (placeholderImage != nil) {
                    
                    //                    dispatch_main_async_safe(^{
                    
                    wself.image = placeholderImage;
                    
                    //                    });
                    
                }
                
            }
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (placeholderImage != nil) {
            
            //            dispatch_main_async_safe(^{
            
            wself.image = placeholderImage;
            
            //            });
            
        }
        
        if (failure) {
            
            failure(errorDesc);
            
        }
        
    }];
}

- (NSMutableDictionary *)operationDictionary {
    NSMutableDictionary *operations = objc_getAssociatedObject(self, &loadOperationKey);
    if (operations) {
        return operations;
    }
    operations = [NSMutableDictionary dictionary];
    objc_setAssociatedObject(self, &loadOperationKey, operations, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    return operations;
}

//- (void)cancelCurrentImageLoad
//{
//    // Cancel in progress downloader from queue
//    NSMutableDictionary *operationDictionary = [self operationDictionary];
//    id operations = [operationDictionary objectForKey:key];
//    if (operations) {
//        if ([operations isKindOfClass:[NSArray class]]) {
////            for (id <SDWebImageOperation> operation in operations) {
////                if (operation) {
////                    [operation cancel];
////                }
////            }
//        } else if ([operations conformsToProtocol:@protocol(SDWebImageOperation)]){
////            [(id<SDWebImageOperation>) operations cancel];
//        }
//        [operationDictionary removeObjectForKey:key];
//    }
//}
//- (void)downloadBanner

- (void)downloadImageFromAliyunOSS:(NSString*)imageUrl
                       isThumbnail:(BOOL)isThumbnail
                  placeholderImage:(UIImage*)placeholderImage
                           success:(SuccessBlock)success
                        andFailure:(FailureBlock)failure
{
//#if 1
    
//    self.image = nil;
    
//    [self sd_cancelCurrentImageLoad];
//    objc_setAssociatedObject(self, &imageURLKey, imageUrl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    if (placeholderImage) {
        dispatch_main_async_safe(^{
            self.image = placeholderImage;
        })
        
    }
    
    if (IsStrEmpty(imageUrl)) {
        return;
    }

#if 1
    __weak __typeof(self)wself = self;
    [[YYImageDownloader sharedDownloader] downloadImageWithImageUrl:imageUrl isThumbnail:isThumbnail completed:^(NSData *imageData, NSError *error) {
        
        if (imageData != nil && imageData.length > 0) {
            
            UIImage *image = [UIImage imageWithData:imageData];
//            UIImage *image = [[UIImage alloc] initWithData:imageData];
            
            //            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            dispatch_main_sync_safe(^{
//                DLog(@"显示图片：imageUrl=%@", imageUrl);
//                wself.image = [UIImage imageWithData:imageData];
//                wself.image = [UIImage sdImage]
                wself.image = image;
                [wself setNeedsLayout];
                //                [wself setNeedsLayout];
                if (success) {
                    success(image);
                }
            });
        }
        else {
            dispatch_main_async_safe(^{
                if (failure) {
                    failure(nil);
                }
            });
        }
    }];
    return;
#endif
    
}

- (void)downloadImage:(NSString*)imageUrl
     placeholderImage:(UIImage*)placeholderImage
              success:(SuccessBlock)success
           andFailure:(FailureBlock)failure;
{
    [self downloadImageFromAliyunOSS:imageUrl isThumbnail:NO placeholderImage:placeholderImage success:^(id responseData) {
        
        if ([responseData isKindOfClass:[UIImage class]]) {
            if (success) {
                success(responseData);
            }
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        if (failure) {
            failure(nil);
        }
        
    }];
    
#if 0
    
    if (placeholderImage) {
        dispatch_main_async_safe(^{
            self.image = placeholderImage;
        })
        
    }
    
    if (IsStrEmpty(imageUrl) || [imageUrl contains:@"null"]) {
        return;
    }
    
//    self.image = placeholderImage;
//    self.image = nil;
//    self.image = placeholderImage;

#if 1
    // 如果本地已经缓存图片
    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
    
    NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    
    NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
    
    if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
        
        WeakSelf
        
        dispatch_main_async_safe(^{
            
            weakSelf.image = [UIImage imageWithData:imageData];
            
            [weakSelf setNeedsLayout];
            
            if (success) {
                success(imageData);
            }
            
        });
        
        
        return;
    }
    else {
        
        if (placeholderImage != nil) {
            
            
            WeakSelf
            
            dispatch_main_async_safe(^{
                
                weakSelf.image = placeholderImage;//[UIImage imageWithData:imageData];
                
                [weakSelf setNeedsLayout];
                
                if (success) {
                    success(imageData);
                }
                
            });
            
        }
        
    }
    
    
    
    // 如果本地没有缓存图片 则保存到本地
    //    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, DONWLOAD_IMAGE_API_ENCRYPT];
    
    //    NSString *params = [NSString stringWithFormat:@"url=%@", imageUrl];
    
    __weak __typeof(self)wself = self;
    
    [[NetworkManager sharedInstance] downloadImageData:imageUrl requestMethod:POST success:^(id responseData) {
        //    [[NetworkManager sharedInstance] downloadImageData:url requestMethod:POST params:params success:^(id responseData) {
        
        if (!wself) return;
        
        if ([responseData isKindOfClass:[NSData class]]) {
            
            if (!responseData) {
                return;
            }
            
            NSData *imageData = responseData;
            
            // 判断是否为jpg或者png格式图片
            NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
            
            if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                
                [[FileCache defaultCache] saveData:imageData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                
                dispatch_main_async_safe(^{
                    
                    wself.image = [UIImage imageWithData:imageData];
                    
                    [wself setNeedsLayout];
                    
                    if (success) {
                        
                        success(imageData);
                    }
                    
                });
                
                
                
            }
            else {
                
                if (placeholderImage != nil) {
                    
                    dispatch_main_async_safe(^{
                        
                        wself.image = placeholderImage;
                        
                    });
                    
                }
                
            }
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (placeholderImage != nil) {
            
            dispatch_main_async_safe(^{
                
                wself.image = placeholderImage;
                
            });
            
        }
        
        if (failure) {
            
            failure(errorDesc);
            
        }
        
    }];
    
#endif
    
#endif
    
    
}

//-(void)downloadAvatarImageView:(NSString*)imageUrl
//              placeholderImage:(UIImage*)placeholderImage
//                       success:(SuccessBlock)success
//                    andFailure:(FailureBlock)failure
//{
//    
//    if (placeholderImage) {
//        self.image = placeholderImage;
//    }
//    
//    __weak __typeof(self)wself = self;
//    
//    [[YYImageDownloader sharedDownloader] downloadImageWithAvatarUrl:imageUrl isThumbnail:YES completed:^(NSData *imageData, NSError *error) {
//        
//        if (imageData != nil) {
//            
//            dispatch_main_async_safe(^{
//                wself.image = [UIImage imageWithData:imageData];
//                [wself setNeedsLayout];
//                //                [wself setNeedsLayout];
//                if (success) {
//                    success(imageData);
//                }
//            });
//            
//        }
//        else {
//            
//            dispatch_main_async_safe(^{
//                wself.image = placeholderImage;
//                [wself setNeedsLayout];
//                //                [wself setNeedsLayout];
//                if (failure) {
//                    failure(nil);
//                }
//            });
//            
//        }
//        
//    }];
//    
//    
//}

@end
