//
//  AdvSumHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/13.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface AdvSumHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *areaCode;// 地区代码
@property (nonatomic, assign) NSUInteger position;// 广告版位1到10数字
@property (nonatomic, assign) NSUInteger type; //广告类型：1banner广告；2在线广告；3区域广告
@property (nonatomic, strong) NSString *startTime; // 开始时间：yyyy-MM-dd
@property (nonatomic, strong) NSString *endTime; // 结束时间：yyyy-MM-dd

@end
