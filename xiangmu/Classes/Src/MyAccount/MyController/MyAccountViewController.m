//
//  MyAccountViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyAccountViewController.h"
#import "MyAccountTableView.h"
#import "CreditLineViewController.h"
#import "BillPayViewController.h"
#import "AccountBalanceViewController.h"
#import "TradeRecordViewController.h"
//#import "BaseWebViewController.h"
#import "BBAlertView.h"

@interface MyAccountViewController ()<MyAccountTableViewDelegate>

@property (nonatomic, strong) MyAccountTableView *myAccountTableView;
@property (nonatomic, strong) NSString *financeUrl;


@end

@implementation MyAccountViewController



#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"我的账户";
    
    [self.view addSubview:self.myAccountTableView];
    
//    [self getFinanceUrl];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self getFinanceInfo];
    
//    [self getFI]
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getFinanceUrl
{
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(GET_FINANCE_URL_API) requestMethod:POST params:nil success:^(id responseData) {
        
        StrongSelf
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSString *data = responseData[@"data"];
            
            strongSelf.financeUrl = data;
            
        }
        else {

        }
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

-(void)getFinanceInfo
{
    self.nextAction = @selector(getFinanceInfo);
    self.object1 = nil;
    self.object2 = nil;
    
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_FINANCE_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            strongSelf.myAccountTableView.creditLine = data[@"creditLine"];
            strongSelf.myAccountTableView.creditAmount = data[@"creditAmount"];
            strongSelf.myAccountTableView.accountAmount = data[@"accountAmount"];
        }
        else {
            strongSelf.myAccountTableView.creditLine = nil;
            strongSelf.myAccountTableView.creditAmount = nil;
            strongSelf.myAccountTableView.accountAmount = nil;
        }
        [strongSelf.myAccountTableView reloadData];
        
    } andFailure:^(NSString *errorDesc) {
        [weakSelf resetAction];
        DLog(@"response failure");
    }];
    
}

#pragma mark - MyAccountTableView Delegate method

- (void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath;
{
    NSArray *sectionList = self.myAccountTableView.cellList[indexPath.section];
    NSDictionary *cellDict = sectionList[indexPath.row];
    if (IsStrEmpty(cellDict[@"vc"])) {
        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleLogin Title:nil message:@"该系统暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }
    else if ([cellDict[@"vc"] startsWith:@"http://"]) {
        NSString *url = !IsStrEmpty(self.financeUrl) ? self.financeUrl : cellDict[@"vc"];
        [self gotoWebpage:url];
        
//        BaseWebViewController *viewController = [[BaseWebViewController alloc] init];
//        if (!IsStrEmpty(self.financeUrl)) {
//            viewController.url = self.financeUrl;
//        }
//        else {
//            viewController.url = cellDict[@"vc"];
//        }
////        viewController.url = cellDict[@"vc"];
//        viewController.enter_type = ENTER_TYPE_PUSH;
//        viewController.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:viewController animated:YES];
    }
    else {
        [self gotoPage:cellDict[@"vc"] animated:YES title:cellDict[@"title"]];
    }
    
}


-(void)viewDidLayoutSubviews
{
    if ([self.myAccountTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.myAccountTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.myAccountTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.myAccountTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

#pragma mark -

- (MyAccountTableView*)myAccountTableView
{
    if (!_myAccountTableView) {
        
        _myAccountTableView = [[MyAccountTableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
        
        _myAccountTableView.accountDelegate = self;
        
    }
    
    return _myAccountTableView;
    
}



@end
