//
//  PartnerModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol PartnerModel <NSObject>
@end



@interface PartnerModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *logoUrl;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *companyName;
@property (nonatomic, strong) NSNumber<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *mobile;
@property (nonatomic, strong) NSString<Optional> *nickname;
@property (nonatomic, strong) NSString<Optional> *partnerCompanyId;
@property (nonatomic, strong) NSString<Optional> *updateTime;
@property (nonatomic, strong) NSString<Optional> *valid;
@property (nonatomic, strong) NSData<Optional> *imgData;// 图像数据

@end

@interface PartnerResponseModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSArray<PartnerModel, Optional> *data;
@end


@protocol ApplyPartnerModel <NSObject>
@end


@interface ApplyPartnerModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *dstCompanyId;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *srcCompanyId;
@property (nonatomic, strong) NSString<Optional> *srcCompanyName;
@property (nonatomic, strong) NSString<Optional> *srcMobile;
@property (nonatomic, strong) NSString<Optional> *srcNickname;

@end
@interface ApplyPartnerResponseModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSArray<ApplyPartnerModel, Optional> *data;

@end
