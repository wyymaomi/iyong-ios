//
//  SearchModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol LocationSearchModel <NSObject>
@end

@interface LocationSearchModel : JSONModel

// 百度返回接口数据
@property (nonatomic, strong) NSString<Optional> *name; // poi名称
@property (nonatomic, strong) NSDictionary<Optional> *location;//poi经纬度坐标 "lat":39.915174,"lng":116.403875
@property (nonatomic, strong) NSString<Optional> *uid;// poi的唯一标示，ID
@property (nonatomic, strong) NSString<Optional> *city;// 城市
@property (nonatomic, strong) NSString<Optional> *district;// 区县

// 腾讯返回接口数据
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *title;
@property (nonatomic, strong) NSString<Optional> *address;
@property (nonatomic, strong) NSNumber<Optional> *time;
//@property (nonatomic, strong) NSString<Optional> *tel;
//@property (nonatomic, strong) NSString<Optional> *category;
//@property (nonatomic, strong) NSNumber<Optional> *type;
//@property (nonatomic, strong) NSNumber<Optional> *_distance;
@property (nonatomic, strong) NSString<Optional> *areaCode; // 城市代码

@end

@interface LocationSearchResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSString<Optional> *message;
@property (nonatomic, strong) NSNumber<Optional> *count;
@property (nonatomic, strong) NSArray<LocationSearchModel, Optional> *result;
@property (nonatomic, strong) NSArray<LocationSearchModel, Optional> *data;
//@property (nonatomic, strong) NSArray<LocationSearchModel, Optional> *result;
@property (nonatomic, strong) NSString<Optional> *request_id;

@end
