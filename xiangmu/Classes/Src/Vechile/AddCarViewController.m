//
//  AddCarViewController.m
//  xiangmu
//
//  Created by David kim on 16/3/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AddCarViewController.h"
#import "MyUtil.h"
#import "NetworkManager.h"
#import "AFNetworking.h"
#import "OrderViewController.h"
#import <UIKit/UIKit.h>
#import "UserManager.h"
#import "HttpConstants.h"
#import "AddCarTableViewCell.h"
#import "AddCarDetailViewController.h"
#import "LoginViewController.h"
#import "GTMBase64.h"
#import "ImageUtil.h"

@interface AddCarViewController ()<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    NSString *dateStr;
    NSIndexPath *sIndexPath;
}
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *button;
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation AddCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    dateStr=[formatter stringFromDate:[NSDate date]];
    
    [self createMyNav];
    [self.view addSubview:self.tableView];
}

-(void)AgeRow:(NSInteger)row WithTitle:(NSString *)title
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:5 inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=title;
}
-(void)ChangeRow:(NSInteger)row WithTitle:(NSString *)title
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:4 inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=title;
}
-(void)getRow:(NSInteger)row WithTitle:(NSString *)title
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
     cell.rightLabel.text=title;
    if (title.length == 0) {
        if (row == 1) {
            cell.rightLabel.text = @"请填写车";
        }
        else if (row==2)
        {
            cell.rightLabel.text=@"请填写车牌号";
        }
        else if (row==3)
        {
            cell.rightLabel.text=@"请填姓名";
        }
        else if (row==6)
        {
            cell.rightLabel.text=@"请填写手机号";
        }
    }
   
}
-(void)ChangeRow:(NSInteger)row WithSex:(NSString *)sex
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=sex;
}
-(void)KnowRow:(NSInteger)row WithDate:(NSString *)date
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:1];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=date;
}
-(void)createMyNav
{
    self.titleLabel=[self addNavTitle:CGRectMake(0, 0, 335, 49) title:@"添加"];
    self.titleLabel.textColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:35.0f/255.0f green:70.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.translucent=NO;
    UIButton *btn=[self addNavBtn:CGRectMake(0, 0, 9, 15) title:nil target:self action:@selector(gotoBack) isLeft:YES];
    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    self.button=[self addNavBtn:CGRectMake(0, 0, 40, 20) title:@"保存" target:self action:@selector(gotoSave) isLeft:NO];
    [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
-(UITableView *)tableView
{
    if (_tableView == nil) {
        self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, ViewHeight) style:UITableViewStyleGrouped];
        self.tableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        self.tableView.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
        self.tableView.scrollEnabled=YES;
        self.tableView.delegate=self;
        self.tableView.dataSource=self;
        self.tableView.showsVerticalScrollIndicator=NO;
        self.tableView.tableFooterView=[[UIView alloc]init];
    }
    return _tableView;
}

-(void)gotoBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)gotoSave
{
    
    self.nextAction = @selector(gotoSave);
    self.object1 = nil;
    self.object2 = nil;
    
    for (int i=1; i<7; i++)
    {
        sIndexPath=[NSIndexPath indexPathForRow:i inSection:0];
    }
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:sIndexPath];
    if ([cell.rightLabel.text isEqualToString:@"请填写车"]||[cell.rightLabel.text isEqualToString:@"请填写车牌号"]||[cell.rightLabel.text isEqualToString:@"请填姓名"]||[cell.rightLabel.text isEqualToString:@"请填写手机号"])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"必填信息没有添加完整" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
//        NSLog(@"%@",cell.rightLabel.text);
        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:1 inSection:0];
        AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath1];
        
        
        NSIndexPath *indexPath2=[NSIndexPath indexPathForRow:2 inSection:0];
        AddCarTableViewCell *cell1=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath2];
        
        
        NSIndexPath *indexPath3=[NSIndexPath indexPathForRow:3 inSection:0];
        AddCarTableViewCell *cell2=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath3];
        
        
        NSIndexPath *indexPath8=[NSIndexPath indexPathForRow:4 inSection:0];
        AddCarTableViewCell *cell8=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath8];
        
        NSIndexPath *indexPath7=[NSIndexPath indexPathForRow:5 inSection:0];
        AddCarTableViewCell *cell7=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath7];
        if ([cell7.rightLabel.text isEqualToString:@"男"]) {
            cell7.rightLabel.text=@"1";
        }
        else
        {
            cell7.rightLabel.text=@"0";
        }
        
        NSIndexPath *indexPath4=[NSIndexPath indexPathForRow:6 inSection:0];
        AddCarTableViewCell *cell3=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath4];
        
        NSIndexPath *indexPath5=[NSIndexPath indexPathForRow:0 inSection:1];
        AddCarTableViewCell *cell4=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath5];
        
        if ([cell4.rightLabel.text isEqualToString:@"选择日期"]) {
            cell4.rightLabel.text=@" ";
        }
        NSIndexPath *indexPath6=[NSIndexPath indexPathForRow:1 inSection:1];
        AddCarTableViewCell *cell5=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath6];
        if ([cell5.rightLabel.text isEqualToString:@"选择日期"]) {
            cell5.rightLabel.text=@" ";
        }

    
        // 先登录 登录成功 再获取车辆信息
        
        
        // 汽车信息获取
       
//        NSString *parmas = @"model=bgbg&number=Vfbbg&driver=Efrgf&age=90后&sex=1&mobile=13245678909&inspectionDate=2016-06-03&insuranceDate=2016-06-03";
    
//        NSData *data =
    NSData *data = UIImageJPEGRepresentation(avatarImg.image, 0.5);
    NSString *fileData  = [[NSString alloc] initWithData:[GTMBase64 encodeData:data] encoding:NSUTF8StringEncoding];
    DLog(@"data.length = %d", data.length);
    DLog(@"fileData.length = %d", fileData.length);
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_SAVE_API];
        NSString *params = [NSString stringWithFormat:@"fileContent=%@&model=%@&number=%@&driver=%@&age=%@&sex=%ld&mobile=%@&inspectionDate=%@&insuranceDate=%@", fileData, cell.rightLabel.text,cell1.rightLabel.text ,cell2.rightLabel.text,cell8.rightLabel.text,(long)[cell7.rightLabel.text integerValue],cell3.rightLabel.text,cell4.rightLabel.text,cell5.rightLabel.text];
    
//    NSString *params = [NSString stringWithFormat:@"%@&fileContent=%@", @"model=i320&number=沪A51319&driver=老张2&age=70后&sex=1&mobile=13888888888&inspectionDate=2016-11-12&insuranceDate=2016-11-12", fileData];
        [self hideHUDIndicatorViewAtCenter];
    [self showHUDIndicatorViewAtCenter:@"正在保存车辆，请稍候"];
    WEAKSELF
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"添加车辆成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
        else {
            [self showAlertView:[self getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
    }];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}




//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 7;
    }
    else
    {
        return 2;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return 80;
        }
        return 50;
    }
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section==0) {
        return 20;
    }
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    AddCarTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    cell.backgroundColor=[UIColor whiteColor];
    if (cell==nil) {
        cell=[[AddCarTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    if (indexPath.section==0) {
        NSArray *nameArray=@[@"头   像",@"车   型",@"车   牌",@"姓   名",@"年   龄",@"性   别",@"电   话"];
        NSArray *rightArray=@[@"",@"请填写车",@"请填写车牌号",@"请填姓名",@"60后",@"男",@"请填写手机号"];
        cell.textLabel.textColor=[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
        cell.textLabel.font=[UIFont systemFontOfSize:13];
        cell.textLabel.text=nameArray[indexPath.row];
        if (indexPath.row==0) {
            avatarImg=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-150, 10, 100, 60)];
            [cell addSubview:avatarImg];
        }
        else
        {
            cell.rightLabel.text=rightArray[indexPath.row];
        }
        
    }
    
    else
    {
        NSArray *nameArray=@[@"年检日期",@"保险日期"];
        cell.textLabel.textColor=[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
        cell.textLabel.font=[UIFont systemFontOfSize:13];
        cell.textLabel.text=nameArray[indexPath.row];
        cell.rightLabel.text=@"选择日期";
    }
    //显示最右边的箭头
   // cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
//-(void)showAlert:(UITapGestureRecognizer *)tap
//{
//    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"提示" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
//    UIAlertAction *fromPhotoAction=[UIAlertAction actionWithTitle:@"从手机相册中选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        UIImagePickerController *imagePicker=[[UIImagePickerController alloc]init];
//        imagePicker.delegate=self;
//        imagePicker.allowsEditing=YES;
//        imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
//        [self presentViewController:imagePicker animated:YES completion:nil];
//    }];
//    UIAlertAction *fromCameraAction=[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        UIImagePickerController *imagePicker=[[UIImagePickerController alloc]init];
//        imagePicker.delegate=self;
//        imagePicker.allowsEditing=NO;
//        imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
//        [self presentViewController:imagePicker animated:YES completion:nil];
//        
//    }];
//    [alertController addAction:cancelAction];
//    [alertController addAction:fromCameraAction];
//    [alertController addAction:fromPhotoAction];
//    [self presentViewController:alertController animated:YES completion:nil];
//    
//    
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0&&indexPath.row==0) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:@"取消"
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:@"拍照", @"从相册选择", nil];
        [actionSheet showInView:self.view];
    }
    else
    {
        AddCarTableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
        AddCarDetailViewController *Addcardetail=[[AddCarDetailViewController alloc]init];
        Addcardetail.name=[NSString stringWithFormat:@"%@",cell.textLabel.text];
        Addcardetail.type=SearchTypeAdd;
        Addcardetail.rightname=cell.rightLabel.text;
        NSLog(@"%@",Addcardetail.name);
        Addcardetail.delegate=self;
        Addcardetail.section=indexPath.section;
        Addcardetail.row=indexPath.row;
        [self.navigationController pushViewController:Addcardetail animated:YES];
    }
}

#pragma mark - UIActionSheet delegate method

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        
    }
    switch (buttonIndex) {
        case 0:
            [self takePhoto];
            break;
        case 1:
            [self LocalPhoto];
            break;
        default:
            break;
    }
}

//开始拍照
-(void)takePhoto
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.delegate = self;
        //设置拍照后的图片可被编辑
        cameraPicker.allowsEditing = YES;
        cameraPicker.sourceType = sourceType;
        [self presentViewController:cameraPicker animated:YES completion:nil];
    }else
    {
        NSLog(@"模拟其中无法打开照相机,请在真机中使用");
    }
}

//打开本地相册
-(void)LocalPhoto
{
    UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
    
    photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    photoPicker.delegate = self;
    //设置选择后的图片可被编辑
    photoPicker.allowsEditing = YES;
    [self presentViewController:photoPicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllDelegate method

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]) {
        UIImage* image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        CGSize imageSize = CGSizeMake(100, 100);
        avatarImg.image = [ImageUtil imageWithImage:image scaledToSize:imageSize];
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"您取消了选择图片");
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//选好图片之后
//-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
//{
//    //在选择之后,先退出图片选择
//    [picker dismissViewControllerAnimated:YES completion:nil];
//    
//    UIImage *image =[info objectForKey:UIImagePickerControllerOriginalImage];
//    [self saveImage:image withName:@"帅帅的车"];
//    NSString *fullpath1 =[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/"] stringByAppendingPathComponent:@"帅帅的车"];
//    [avatarImg setImage:[[UIImage alloc]initWithContentsOfFile:fullpath1]];
//    
//    NSUserDefaults *ud1 =[NSUserDefaults standardUserDefaults];
//    [ud1 setObject:fullpath1 forKey:@"imagePath"];
//    [ud1 synchronize];

    //    //这里的token是要和个人相关联的
    //    NSString *a2=(NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)token, (CFStringRef)@"!*'();:@&=+$,/?%#[]", (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    //
    //    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    NSString *s1=[NSString stringWithFormat:@"http://mgjt.hzgames.cn:8080/horse_tvpartner/account2/avatar?token=%@",a2];
    //    [manager POST:s1 parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    //     {
    //         [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 1)name:@"avatar" fileName:@"image" mimeType:@"image/jpeg"];
    //
    //     }success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //         NSLog(@"Success: %@", responseObject);
    //     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //         NSLog(@"Error: %@", error);
    //     }];
    
//    [self saveImage:image withName:@"currentImage.png"];
//    NSString *fullpath=[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"]stringByAppendingPathComponent:@"currentImage.png"];
//    [avatarImg setImage:[[UIImage alloc]initWithContentsOfFile:fullpath]];
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    [ud setObject:fullpath forKey:@"imagePath"];
//    [ud synchronize];
//    NSLog(@"图片路径是：%@",fullpath);
//}

//存储图片
//- (void) saveImage:(UIImage *)currentImage withName:(NSString *)imageName
//{
//    NSData *imageData =UIImageJPEGRepresentation(currentImage, 0.5);
//    //获取沙盒目录
//    NSString *fullPath =[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"] stringByAppendingPathComponent:imageName];
//    //将图片写入文件
//    [imageData writeToFile:fullPath atomically:YES];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
