//
//  CommentView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/28.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CommentPopupViewDelegate <NSObject>

- (void)onGetText:(NSString*)text;

@end

@interface CommentPopupView : UIView

@property (nonatomic, weak) id<CommentPopupViewDelegate> delegate;

- (void)addComment;

@end
