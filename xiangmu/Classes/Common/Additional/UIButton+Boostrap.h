//
//  UIButton+HighlitedImage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Boostrap)

-(void)bootstrapStyle;

-(void)blueStyle;
-(void)whiteStyle;
-(void)alertStyle;
-(void)darkRedStyle;
- (void)yellowStyle;
- (void)grayStyle;
- (void)greenStyle;

- (void)okStyle;
- (void)cancelStyle;

- (UIImage *) buttonImageFromColor:(UIColor *)color;

@end
