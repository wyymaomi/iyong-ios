//
//  RedpackDAO.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RedpackDAO.h"

@implementation RedpackDAO

-(NSString*)getRedpackMoney
{
    __block NSString *redpackMoney = @"";
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        
        NSString *mobile = [UserManager sharedInstance].userModel.username;
        
//        NSString *createTime = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
        
        NSString *sql = [NSString stringWithFormat:@"select * from red_pack where mobile = ?"];
        
        FMResultSet *rs = [db executeQuery:sql, mobile];
        
        if (!rs) {
            [rs close];
        }
        
        while ([rs next]) {
            
            redpackMoney = [rs stringForColumn:@"amount"];
            
        }
        
    }];
    
    return redpackMoney;
}


-(NSString*)getTodayRedpackMoney
{
//    __block NSInteger rowCount = 0;
    
    __block NSString *redpackMoney = @"";
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        
        NSString *mobile = [UserManager sharedInstance].userModel.username;
        
        NSString *createTime = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
        
        NSString *sql = [NSString stringWithFormat:@"select * from red_pack where createTime = ? AND mobile = ?"];
        
        FMResultSet *rs = [db executeQuery:sql, createTime, mobile];
        
        if (!rs) {
            [rs close];
        }
        
        while ([rs next]) {
            
            redpackMoney = [rs stringForColumn:@"amount"];
        
        }
        
    }];
    
    return redpackMoney;

}

#pragma mark - 判断该账号是否领取过红包

-(BOOL)isExistRedpack:(NSInteger)flag
{
    __block NSInteger rowCount = 0;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        
        NSString *mobile = [UserManager sharedInstance].userModel.username;
        
        NSString *sql = @"select count(*) from red_pack where mobile = ? AND flag = ?";
        
        FMResultSet *rs = [db executeQuery:sql, mobile, [NSNumber numberWithInteger:flag]];
        
        if (!rs) {
            [rs close];
            return;
        }
        
        if ([rs next]) {
            
            rowCount = [rs intForColumnIndex:0];
            
            [rs close];
            
        }
        
        
    }];

    
    if (rowCount > 0) {
        return YES;
    }
    
    return NO;
}

/**
 *   是否有已入账的红包
 */
-(BOOL)isExistTodayRedpack:(NSInteger)flag
{
    
    __block NSInteger rowCount = 0;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        
        NSString *mobile = [UserManager sharedInstance].userModel.username;
        
        NSString *createTime = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
        
//        NSString *sql = @"select count(*) from red_pack where mobile = ? AND createTime = ? AND flag = ?";
        
        NSString *sql = @"select count(*) from red_pack where mobile = ? AND createTime = ? AND flag = ?";
        
        FMResultSet *rs = [db executeQuery:sql, mobile, createTime, [NSNumber numberWithInteger:flag]];
        
        if (!rs) {
            [rs close];
            return;
        }
        
        if ([rs next]) {
            
            rowCount = [rs intForColumnIndex:0];
            
            [rs close];

        }
        
        
    }];
    
    if (rowCount > 0) {
        return YES;
    }
    
    return NO;
}

-(BOOL)updateRedpackStatus:(NSInteger)flag
{
    __block BOOL isSuccess = NO;
    
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        NSString *createTime = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
        
        NSString *mobile = [UserManager sharedInstance].userModel.username;
        
        isSuccess = [db executeUpdate:@"UPDATE red_pack SET flag = ? WHERE mobile = ? AND createTime = ?", [NSNumber numberWithInteger:flag], mobile, createTime];
        
    }];
    
    return isSuccess;
}

/**
 *  红包领取后
 */
-(BOOL)insertRedpack:(NSString*)amount flag:(NSInteger)flag
{
    
    __block BOOL isSuccess = NO;
    
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        NSString *sql = @"replace into red_pack(amount, createTime, mobile, flag) values(?,?,?,?)";
        
        NSString *mobile = [UserManager sharedInstance].userModel.username;
        
        NSString *createTime = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
        
        isSuccess = [db executeUpdate:sql, amount, createTime, mobile, [NSNumber numberWithInteger:flag]];
        
    }];
    
    return isSuccess;
}


@end
