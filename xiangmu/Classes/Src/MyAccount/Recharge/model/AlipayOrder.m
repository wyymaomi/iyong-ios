//
//  Order.m
//  AliSDKDemo
//
//  Created by 方彬 on 07/25/16.
//
//

#import "AlipayOrder.h"
#import "DataSigner.h"
#import "NSDataEx.h"

@implementation BizContent

- (NSString *)description {
    
    NSMutableDictionary *tmpDict = [NSMutableDictionary new];
    // NOTE: 增加不变部分数据
    [tmpDict addEntriesFromDictionary:@{@"subject":_subject?:@"",
                                        @"out_trade_no":_out_trade_no?:@"",
                                        @"total_amount":_total_amount?:@"",
                                        @"seller_id":_seller_id?:@"",
                                        @"product_code":_product_code?:@"QUICK_MSECURITY_PAY"}];
    
    // NOTE: 增加可变部分数据
    if (_body.length > 0) {
        [tmpDict setObject:_body forKey:@"body"];
    }
    
    if (_timeout_express.length > 0) {
        [tmpDict setObject:_timeout_express forKey:@"timeout_express"];
    }
    
    if (_enable_pay_channels.length > 0) {
        [tmpDict setObject:_enable_pay_channels forKey:@"enable_pay_channels"];
    }
    
    // NOTE: 转变得到json string
    NSData* tmpData = [NSJSONSerialization dataWithJSONObject:tmpDict options:0 error:nil];
    NSString* tmpStr = [[NSString alloc]initWithData:tmpData encoding:NSUTF8StringEncoding];
    return tmpStr;
}

@end


@implementation Order

- (NSString *)orderInfoEncoded:(BOOL)bEncoded {
    
    if (_app_id.length <= 0) {
        return nil;
    }
    
    // NOTE: 增加不变部分数据
    NSMutableDictionary *tmpDict = [NSMutableDictionary new];
    [tmpDict addEntriesFromDictionary:@{@"app_id":_app_id,
                                        @"method":_method?:@"alipay.trade.app.pay",
                                        @"charset":_charset?:@"utf-8",
                                        @"timestamp":_timestamp?:@"",
                                        @"version":_version?:@"1.0",
                                        @"biz_content":_biz_content.description?:@"",
                                        @"sign_type":_sign_type?:@"RSA"}];
    
    
    // NOTE: 增加可变部分数据
    if (_format.length > 0) {
        [tmpDict setObject:_format forKey:@"format"];
    }
    
    if (_return_url.length > 0) {
        [tmpDict setObject:_return_url forKey:@"return_url"];
    }
    
    if (_notify_url.length > 0) {
        [tmpDict setObject:_notify_url forKey:@"notify_url"];
    }
    
    if (_app_auth_token.length > 0) {
        [tmpDict setObject:_app_auth_token forKey:@"app_auth_token"];
    }
    
    // NOTE: 排序，得出最终请求字串
    NSArray* sortedKeyArray = [[tmpDict allKeys] sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    NSMutableArray *tmpArray = [NSMutableArray new];
    for (NSString* key in sortedKeyArray) {
        NSString* orderItem = [self orderItemWithKey:key andValue:[tmpDict objectForKey:key] encoded:bEncoded];
        if (orderItem.length > 0) {
            [tmpArray addObject:orderItem];
        }
    }
    return [tmpArray componentsJoinedByString:@"&"];
}

- (NSString*)orderItemWithKey:(NSString*)key andValue:(NSString*)value encoded:(BOOL)bEncoded
{
    if (key.length > 0 && value.length > 0) {
        if (bEncoded) {
            value = [self encodeValue:value];
        }
        return [NSString stringWithFormat:@"%@=%@", key, value];
    }
    return nil;
}

- (NSString*)encodeValue:(NSString*)value
{
    NSString* encodedValue = value;
    if (value.length > 0) {
        encodedValue = (__bridge_transfer  NSString*)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)value, NULL, (__bridge CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 );
    }
    return encodedValue;
}

@end

@implementation AlipayOrder


- (id)initWithDictionary:(NSDictionary*)dict;
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSString*)getAppScheme
{
    NSString *appScheme = @"com.chinaiyong.iyong";
#ifdef TEST
    appScheme = @"com.chinaiyong.iyongtest";
#endif
    
    return appScheme;
}

- (NSString*)getAppId;
{
    return @"2016120503866964";
}

- (NSString*)getAppPrivateKey;
{
    return @"MIICeQIBADANBgkqhkiG9w0BAQEFAASCAmMwggJfAgEAAoGBALn3xnT9UgAYC4ZkgAlRi/b+yF8DoubGRY05wDNYQO8/KcGv9e4W6mafso2Vtwsq11P36P6WHHno2yq4ngUQJxfJcMSfx9p4KLaaB3hJd4U7d84cjfnvoTz0fwLVApO5mGXZKYGk9zHSge0O4aQAkOkTfXvIkNtQMzE+hqRlng+fAgMBAAECgYEAoGx9N7i7fzc+4e1agi9llWnOwiAp6/hIUX7rLZ/JTFnplJtNhNkh1euXpQntAGYxoM7woIYwBH56ak0ei/GWNjSqpfOAtpLeRU2YjeAb5FpC/fal/4dL5yi95B60VGiFbCxFKATH1mg39yKN2SBqbgCLt2plNYpfk9g0/MKnQuECQQD2RpTCtPOmMSv9t7R6RfvwVHi7HdpfKeClfSx0jzdca86Z/44HHIxW2eNWUFTbME/ne97scfTfbJQgdLHyY29xAkEAwU+WOy8s2H4lHxFPbGGASCE3xJ01n80bwt1axe8UMv/lB0oQaDEAlJmzP6uNJP1rpVOF39XjZRxZXjQMgb8IDwJBAJIQ7q5E0LXjOCo1SM+0rmauUyT34crLdbfxc+p1/unL7y+bSMui/QGOHa3V215dWyb6aw9lz0ysg3Z0gA45N6ECQQCFZ1IKt+yf3rXpcb240rR/koT053T5 Ocfdu3qIPAxZgaThWehmruJyqns8BjyaeVTPPAuYQ/fwcBbwXsT+yQFbAkEAkA2rQsvqydcaCS6S1bhl7zvYBv5XiLgCiaasXP5o8ZLlAFsyuW6bbAV3WALGNI+r3dJbmqKU1WV+YLR1a5pirA==";
}

- (NSString*)generateOrderString:(NSDictionary*)dict;
{
    
    NSString *tradeNo = dict[kTradeNo];
    NSString *amount = dict[kRechargeAmount];
    NSString *timestamp = dict[kPayTime];
    
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    Order* order = [Order new];
    
    // NOTE: app_id设置
    order.app_id = [self getAppId];
    
    // NOTE: 支付接口名称
    order.method = @"alipay.trade.app.pay";
    
    // NOTE: 参数编码格式
    order.charset = @"utf-8";
    
    // NOTE: 当前时间点
//    NSDateFormatter* formatter = [NSDateFormatter new];
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    order.timestamp = timestamp;//[formatter stringFromDate:[NSDate date]];
    
    // NOTE: 支付版本
    order.version = @"1.0";
    
    // NOTE: sign_type设置
    order.sign_type = @"RSA";
    
    // NOTE: 商品数据
    order.biz_content = [BizContent new];
    order.biz_content.body = @"我是测试数据";
    order.biz_content.subject = @"1";
    order.biz_content.out_trade_no = tradeNo;//[self generateTradeNO]; //订单ID（由商家自行制定）
    order.biz_content.timeout_express = @"30m"; //超时时间设置
    order.biz_content.total_amount = [NSString stringWithFormat:@"%.2f", /*0.01*/[amount doubleValue]]; //商品价格
    order.biz_content.enable_pay_channels = @"balance,moneyFund,debitCardExpress";
    
    //将商品信息拼接成字符串
    NSString *orderInfo = [order orderInfoEncoded:NO];
    NSString *orderInfoEncoded = [order orderInfoEncoded:YES];
    DLog(@"orderSpec = %@",orderInfo);
    
    // NOTE: 获取私钥并将商户信息签名，外部商户的加签过程请务必放在服务端，防止公私钥数据泄露；
    //       需要遵循RSA签名规范，并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner([self getAppPrivateKey]);
    NSString *signedString = [signer signString:orderInfo];
    DLog(@"signedString = %@", signedString);
    
    // NOTE: 如果加签成功，则继续执行支付
    if (signedString != nil) {
//        //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
//        NSString *appScheme = @"com.chinaiyong.iyong";
//#ifdef TEST
//        appScheme = @"com.chinaiyong.iyongtest";
//#endif
        
        // NOTE: 将签名成功字符串格式化为订单字符串,请严格按照该格式
        NSString *orderString = [NSString stringWithFormat:@"%@&sign=%@",
                                 orderInfoEncoded, signedString];
        
        DLog(@"orderString = %@", orderString);
        
        
    return orderString;
    }
    
    return nil;
}

@end
