//
//  AdPayViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdPayViewController.h"
#import "AdPayViewController+Logic.h"
//#import "RechargeTableViewCell.h"
//#import "RechargetypeTableViewCell.h"
//#import "SDKManager.h"
//#import "AlipayOrder.h"
//#import "SDKRegisterService.h"
//#import "SDKPayService.h"
//#import "Bankcard.h"
//#import "BBAlertView.h"
//#import "BankcardViewController.h"
//#import "UpmpPay.h"
//#import <IPSUPMPSDK/IPSUpmpViewController.h>
//#import "RechargeDetailViewController.h"
//#import "PayUtil.h"
#import "AdvPayTableViewCell.h"
#import "AdvAccountPayTableViewCell.h"

@interface AdPayViewController ()

@property (strong, nonatomic) UIButton *okButton;
//@property (nonatomic, strong) UILabel *messageLabel;// 提示文字
//@property (nonatomic, strong) UITextField *transAmountTextField;//交易金额输入框
@property (nonatomic, strong) NSArray *rechargeTypeList;
//@property (nonatomic, assign) AdPayType payType;

//@property (nonatomic, strong) BBAlertView *bankcardAlertView;
//@property (nonatomic, strong) BBAlertView *paymentAlertView;
//@property (nonatomic, strong) Bankcard *bankcard;

@end

@implementation AdPayViewController

-(AdPurchaseHttpMessage*)httpMessage
{
    if (!_httpMessage) {
        _httpMessage = [[AdPurchaseHttpMessage alloc] init];
        _httpMessage.payMode = [NSNumber numberWithInteger:AdPayTypePromotionAccount];
    }
    return _httpMessage;
}

- (NSArray*)rechargeTypeList
{
    if (!_rechargeTypeList) {
        _rechargeTypeList = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RechrageType.plist" ofType:nil]];
    }
    
    return _rechargeTypeList;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"支付";
    self.payDelegate = self;
    
    [self.view addSubview:self.groupTableView];
    
    [self getFinanceInfo];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openProviderInfoPage) name:kAdvPurchaseSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openProviderInfoPage) name:kAdvPurchaseFailureNotification object:nil];
    
//    self.httpMessage.price = @"100";
//    self.httpMessage.adMainIds = @"8a9996925b662fdb015b6662e67404f1";
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"限额说明" style:UIBarButtonItemStyleDone target:self action:@selector(gotoLimitAmount)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)openProviderInfoPage
{
    [self gotoPage:@"ProviderInfoViewController"];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.mode == PayModeAdv) {
        if (section == [tableView numberOfSections]-1) {
            //        return self.rechargeTypeList.count;
            return 2;
        }
        return 2;
        //    return 1;
    }
    else {
        if (section == [tableView numberOfSections]-1) {
            return 2;
        }
        return 1;
    }
    

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        return 250;
    }
    return 10;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 250)];
        UIButton *payButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [payButton setBackgroundColor:[UIColor redColor]];
        [payButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_pay"] forState:UIControlStateNormal];
        [payButton addTarget:self action:@selector(doAdPurchase) forControlEvents:UIControlEventTouchUpInside];
        payButton.frame = CGRectMake(58, 160, ViewWidth-58*2, 57.5*Scale);
        [view addSubview:payButton];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(25, 10, ViewWidth-25, 16)];
        label.text = @"需付金额：";
        label.font = FONT(16);
        label.textColor = UIColorFromRGB(0xce5a5a);
        [view addSubview:label];
        
        UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, ViewWidth-15, 16)];
//        moneyLabel.text = [NSString stringWithFormat:@"%.2f元", self.httpMessage.]
        moneyLabel.textColor = UIColorFromRGB(0xce5a5a);
        moneyLabel.text = [NSString stringWithFormat:@"%.2f元", [self.httpMessage.price floatValue]];
        moneyLabel.font = FONT(16);
        moneyLabel.textAlignment = UITextAlignmentRight;
        [view addSubview:moneyLabel];
        
        return view;
    }
    
    return [UIView new];

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSInteger adPayType = [self.httpMessage.payMode integerValue];
    
    if (indexPath.section == 0) {
        
        AdvAccountPayTableViewCell *cell = [AdvAccountPayTableViewCell cellWithTableView:tableView indexPath:indexPath];
        
        if (indexPath.row == AdPayRowPromotionAccount) {
            cell.radioButton.selected = adPayType == AdPayTypePromotionAccount ? YES : NO;
            cell.amountLabel.text = [NSString stringWithFormat:@"%.1f元", [self.promotionAccountMoney floatValue]];
        }
        if (indexPath.row == AdPayRowMyAccount) {
            cell.radioButton.selected = adPayType == AdPayTypeMyAccount ? YES : NO;
            cell.amountLabel.text = [NSString stringWithFormat:@"%.1f元", [self.myAccountMoney floatValue]];
        }
        cell.radioButton.tag = indexPath.section * 1000 + indexPath.row;
        [cell.radioButton addTarget:self action:@selector(onClickRadioButton:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    if (indexPath.section == [tableView numberOfSections]-1) {
        
        AdvPayTableViewCell *cell = [AdvPayTableViewCell cellWithTableView:tableView indexPath:indexPath];

        if (indexPath.row == AdThirdPayRowAlipay) {
            cell.radioButton.selected = adPayType == AdPayTypeAlipay ? YES : NO;
        }
        if (indexPath.row == AdThirdPayRowWechat) {
            cell.radioButton.selected = adPayType == AdPayTypeWechat ? YES : NO;
        }
        
        cell.radioButton.tag = indexPath.section * 1000 + indexPath.row;
        [cell.radioButton addTarget:self action:@selector(onClickRadioButton:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
//        self.payType = indexPath.row;
        [self.groupTableView reloadData];
        if (indexPath.row == AdPayRowPromotionAccount) {
//            self.payType = AdPayTypePromotionAccount;
            self.httpMessage.payMode = [NSNumber numberWithInteger:AdPayTypePromotionAccount];
        }
        if (indexPath.row == AdPayRowMyAccount) {
//            self.payType = AdPayTypeMyAccount;
            self.httpMessage.payMode = [NSNumber numberWithInteger:AdPayTypeMyAccount];
        }
    }
    if (indexPath.section == [tableView numberOfSections] -1) {
//        self.payType = indexPath.row+2;
        [self.groupTableView reloadData];
        if (indexPath.row == AdThirdPayRowAlipay) {
//            self.payType = AdPayTypeAlipay;
            self.httpMessage.payMode = [NSNumber numberWithInteger:AdPayTypeAlipay];
        }
        else if (indexPath.row == AdThirdPayRowWechat) {
//            self.payType = AdPayTypeWechat;
            self.httpMessage.payMode = [NSNumber numberWithInteger:AdPayTypeWechat];
        }
    }
    
}

- (void)onClickRadioButton:(UIButton*)button
{
    NSInteger tag = button.tag;
    NSInteger section = tag/1000;
    NSInteger row = tag%1000;
//    self tableView
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    [self tableView:self.groupTableView didSelectRowAtIndexPath:indexPath];
}


@end
