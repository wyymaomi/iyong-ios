//
//  AdvPurchaseDetailViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPurchaseDetailViewController.h"
#import "AdvPurchaseInfoTableViewCell.h"
#import "AdPurchaseCalendarTableViewCell.h"
#import "AdPurchaseTimeSelectionTableViewCell.h"
#import "AdvPurchaseOrderTableViewCell.h"
#import "AdvPurchaseDetailViewController+NetworkRequest.h"
#import "HeaderView.h"
#import "DateUtil.h"
#import "AdPurchaseHttpMessage.h"
#import "AdPayViewController.h"
#import "BannerPurchaseInfoTableViewCell.h"
#import "BaseViewController+navigation.h"

@interface AdvPurchaseDetailViewController ()<AdPurchaseCalendarTableViewCellDelegate,AdPurchaseTimeSelectionTableViewCellDelegate>
@property (nonatomic, strong) UIButton *payButton;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *helpButton;
@end

@implementation AdvPurchaseDetailViewController

-(UIButton*)helpButton
{
    if (!_helpButton) {
        _helpButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _helpButton.frame = CGRectMake(ViewWidth-50*Scale, 13*Scale, 50*Scale, 16*Scale);
        [_helpButton setTitle:@"说明" forState:UIControlStateNormal];
        [_helpButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_helpButton addTarget:self action:@selector(gotoBrowsePage) forControlEvents:UIControlEventTouchUpInside];
    }
    return _helpButton;
}

- (UIButton*)closeButton
{
    if (_closeButton == nil) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_close"] forState:UIControlStateNormal];
        _closeButton.frame = CGRectMake(15*Scale, 13*Scale, 16*Scale, 16*Scale);
        [_closeButton addTarget:self action:@selector(pressedBackButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

//-(NSMutableArray*)advOrderList
//{
//    if (_advOrderList) {
//        _advOrderList = [NSMutableArray array];
//    }
//    return _advOrderList;
//}

-(AdvPurchaseViewModel*)viewModel
{
    if (!_viewModel) {
        _viewModel = [[AdvPurchaseViewModel alloc] init];
    }
    return _viewModel;
}

-(UIButton*)payButton;
{
    if (!_payButton) {
        _payButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_payButton setImage:[UIImage imageNamed:@"btn_pay"] forState:UIControlStateNormal];
        [_payButton addTarget:self action:@selector(gotoPay) forControlEvents:UIControlEventTouchUpInside];
    }
    return _payButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableViewRowNums = 2;
    
    [self.view addSubview:self.groupTableView];
    self.groupTableView.backgroundColor = [UIColor whiteColor];
    self.groupTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:self.closeButton];
    
    if (self.advType == AdvTypeLocalArea || self.advType == AdvTypeOnlineProvider) {
        self.groupTableView.frame = CGRectMake(0, 42*Scale, ViewWidth, self.view.height-42*Scale);
    }
    
//    [self.view addSubview:self.helpButton];
    
    AdvSumHttpMessage *httpMessage = [AdvSumHttpMessage new];
    httpMessage.type = self.advType;
    httpMessage.position = self.position;
    httpMessage.areaCode = self.advType == AdvTypeLocalArea ? [UserManager sharedInstance].userModel.areaCode : @"";
    httpMessage.startTime = [DateUtil getCurrentDate];
    httpMessage.endTime = @"";
    [self fetchAdvSumList:httpMessage];
    
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
//    self.groupTableView.frame = CGRectMake(0, 0, ViewWidth, self.view.height-80);
    
}

-(void)pressedBackButton
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleLogin Title:nil message:@"是否取消当前选项" customView:nil delegate:nil cancelButtonTitle:STR_CANCEL otherButtonTitles:STR_OK];
    [alertView setConfirmBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - openBrowserPage

- (void)gotoBrowsePage
{
    NSString *url = @"http://iyong-public.oss-cn-shanghai.aliyuncs.com/iyong/ad/explain/rule.html";
    [self openWebPage:url];
}

#pragma mark - 

-(void)onSelectMonth:(NSDate *)date
{
    AdvSumHttpMessage *httpMessage = [AdvSumHttpMessage new];
    httpMessage.type = self.advType;
    httpMessage.position = self.position;
    httpMessage.areaCode = self.advType == AdvTypeLocalArea ? [UserManager sharedInstance].userModel.areaCode : @"";
    httpMessage.startTime = [date stringWithDateFormat:@"yyyy-MM-dd"];//[DateUtil getCurrentDate];
    httpMessage.endTime = @"";
    DLog(@"httpMessage.description = %@", httpMessage.description);
    [self fetchAdvSumList:httpMessage];
    
}

#pragma mark - 选择日期

- (void)onClickDate:(NSString *)dateStr
{
//    [self fetchAd]
    self.viewModel.currentSelectDate = dateStr;
    [self.viewModel.advOrderList removeAllObjects];
    [self fetchAdvDetailList:dateStr];
}

// 取消选择日期
- (void)onDeselectDate:(NSString*)dateStr;
{
    self.viewModel.currentSelectDate = nil;
    
    
    
    // 删除已选时间列表中该日期的所有选项
//    WeakSelf
//    [self.viewModel.advOrderList enumerateObjectsUsingBlock:^(AdPurchaseOrder * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        
//        if ([obj.date isEqualToString:dateStr]) {
//            [weakSelf.viewModel.advOrderList removeObject:obj];
//        }
//    }];
    
//    NSMutableArray *deleteItems = [NSMutableArray array];
//    NSInteger count = self.viewModel.advOrderList.count;
//    for (NSInteger i = 0; i < count; i++) {
//        AdPurchaseOrder *order = self.viewModel.advOrderList[i];
//        if ([order.date isEqualToString:dateStr]) {
////            deleteItems addObject:[NSNumber numberWithInt]
//            [self.viewModel.advOrderList removeObject:order];
//            [self.groupTableView reloadData];
//        }
//    }
//    for (NSInteger i = 0; i < count; i++) {
//        AdPurchaseOrder *order = self.viewModel.advOrderList[i];
//        if ([order.date isEqualToString:dateStr]) {
//            //            deleteItems addObject:[NSNumber numberWithInt]
//            [self.viewModel.advOrderList removeObject:order];
//            [self.groupTableView reloadData];
//        }
//    }
    
    
//    self.groupTableView
//    [self.groupTableView reloadData];
    
}

#pragma mark - 选择时间段

-(BOOL)shouldSelectTimeSegmentView;
{
    NSInteger orderCount = self.viewModel.advOrderList.count;
    if (orderCount >= [self.viewModel.countLeft integerValue]) {
        //        [MsgToolBox showToast:@"最多选择5个"];
//        NSString *msgText = [NSString stringWithFormat:@"当天当前类型当前版位剩余可购买广告数%ld个，请重新选择", [self.viewModel.countLeft integerValue]];
        //        [MsgToolBox showToast:msgText];
        
        [MsgToolBox showAlert:@"提示" content:kMsgAdvPurchaseOutofLimit];
        return NO;
    }
    
    return YES;
}
//- (void)onClickButton:(NSString*)timeSegmentText adId:(NSString*)adId price:(NSNumber*)price isSelected:(BOOL)isSelected;
//- (void)onClickButton:(NSInteger)index timeSegmentText:(NSString*)timeSegmentText adId:(NSString*)adId price:(NSNumber*)price isSelected:(BOOL)isSelected;
- (void)onClickButton:(NSInteger)index timeSegmentText:(NSString*)timeSegmentText adId:(NSString*)adId price:(NSNumber*)price endTime:(double)endTime isSelected:(BOOL)isSelected;
{
    if (IsStrEmpty(self.viewModel.currentSelectDate)) {
        [MsgToolBox showToast:@"请先选择日期"];
        return;
    }
    
//    double currentTimeStamp = [[NSDate getTimeStamp] doubleValue];
//    if (isSelected && currentTimeStamp > endTime) {
//        [MsgToolBox showToast:@"选择的时间段已过期，请重新选择"];
//        return;
//    }
    
    NSDictionary *dict = @{@"date": self.viewModel.currentSelectDate,
                           @"timeSegment": timeSegmentText,
                           @"adId": adId,
                           @"price": price,
                           @"timeIndex":[NSNumber numberWithInteger:index]};
    
    NSError *error;
    AdPurchaseOrder *order = [[AdPurchaseOrder alloc] initWithDictionary:dict error:&error];
    if (!error) {
        if (isSelected) {
            __block NSInteger count = 0;
            [self.viewModel.advOrderList enumerateObjectsUsingBlock:^(AdPurchaseOrder * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.adId isEqualToString:order.adId]) {
                    count++;
                }
            }];
            if (count == 0) {
                [self.viewModel.advOrderList addObject:order];
            }
        }
        else {
            [self.viewModel.advOrderList enumerateObjectsUsingBlock:^(AdPurchaseOrder * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.adId isEqualToString:order.adId]) {
                    [self.viewModel.advOrderList removeObject:obj];
                }
            }];
        }
        if (self.viewModel.advOrderList.count > 0) {
            self.tableViewRowNums = 4;
        }
        else {
            self.tableViewRowNums = 3;
        }
        
        [self.groupTableView reloadData];
    }
    
    
    
}

- (void)onDeleteItem:(id)sender
{
    UIButton *button = (UIButton*)sender;
    NSInteger index = button.tag;
    [self.viewModel.advOrderList removeObjectAtIndex:index];
    [self.groupTableView reloadData];
}

#pragma mark - 进入支付界面

- (void)gotoPay
{
    
    NSInteger count = self.viewModel.advOrderList.count;
    if (count == 0) {
        [MsgToolBox showToast:@"至少选择一个时间段"];
        return;
    }
    if (count > [self.viewModel.countLeft integerValue]) {
//        [MsgToolBox showToast:@"最多选择5个"];
//        NSString *msgText = [NSString stringWithFormat:@"当天当前类型当前版位剩余可购买广告数%ld个，请重新选择", [self.viewModel.countLeft integerValue]];
//        [MsgToolBox showToast:msgText];
        [MsgToolBox showAlert:@"提示" content:kMsgAdvPurchaseOutofLimit];
        return;
    }
 
    self.nextAction = @selector(gotoPay);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    [self resetAction];
    
    __block NSInteger totalPrice=0;
    __block NSMutableArray *adIdList = [NSMutableArray new];
    
    [self.viewModel.advOrderList enumerateObjectsUsingBlock:^(AdPurchaseOrder * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        totalPrice += [obj.price longValue];
        [adIdList addObject:obj.adId];
    }];
    
    AdPurchaseHttpMessage *httpMessage = [AdPurchaseHttpMessage new];
    httpMessage.price = [NSNumber numberWithInteger:totalPrice];
    httpMessage.adMainIds = [adIdList componentsJoinedByString:@","];
    httpMessage.payMode = [NSNumber numberWithInteger:AdPayTypePromotionAccount];
    
    AdPayViewController *viewController = [[AdPayViewController alloc] init];
    viewController.httpMessage = httpMessage;
    [self.navigationController pushViewController:viewController animated:YES];
    
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    return 1;
//    return 4;
    return self.tableViewRowNums;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return 4;
    if (section == AdvPurchaseDetailRowSelection) {
//        return 10;
        return self.viewModel.advOrderList.count;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == AdvPurchaseDetailRowTItle) {
        return 140;
    }
    if (indexPath.section == AdvPurchaseDetailRowCalendar) {
        return [AdPurchaseCalendarTableViewCell getCellHeight];
    }
    if (indexPath.section == AdvPurchaseDetailRowTime) {
//        return 350;
//        return 306;
        return [AdPurchaseTimeSelectionTableViewCell getCellHeight];
    }
    if (indexPath.section == AdvPurchaseDetailRowSelection) {
        return [AdvPurchaseOrderTableViewCell getCellHeight];
    }
    
    return 0.0f;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section > AdvPurchaseDetailRowTItle) {
        return 45;
    }
    
    return 0.0001f;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section > AdvPurchaseDetailRowTItle) {
        //        return [HeaderView customView];
        HeaderView *headerView = [HeaderView customView];
        if (section == AdvPurchaseDetailRowCalendar) {
            headerView.titleLabel.text = @"选择日期";
        }
        else if (section == AdvPurchaseDetailRowSelection) {
            headerView.titleLabel.text = @"已选时间";
        }
        else if (section == AdvPurchaseDetailRowTItle) {
            headerView.titleLabel.text = @"选择时间";
        }
        else if (section == AdvPurchaseDetailRowTime) {
            headerView.titleLabel.text = @"选择时间";
        }
        return headerView;
    }
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.viewModel.advOrderList.count > 0 && section == [self numberOfSectionsInTableView:tableView]-1) {
        return 200;
    }
    return 0.00001f;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (self.viewModel.advOrderList.count > 0 && section == [self numberOfSectionsInTableView:tableView]-1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 200)];
        [view addSubview:self.payButton];
        self.payButton.frame = CGRectMake(60*Scale, 70, ViewWidth-120*Scale, 52);
        return view;
    }
    return [UIView new];
//    return [UIView new];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%lu%lu", (long)indexPath.section, (long)indexPath.row];
    // 广告条
    if (indexPath.section == AdvPurchaseDetailRowTItle) {
        
        static NSString *CellIdentifier = @"TItleCellID";
        
        if (self.advType == AdvTypeBanner) {
            BannerPurchaseInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([BannerPurchaseInfoTableViewCell class]) owner:nil options:nil] lastObject];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.positionTextLabel.text = [NSString stringWithFormat:@"第%ld版位", (long)_position];
            }
            return cell;
        }
        else {
            AdvPurchaseInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (!cell) {
                cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdvPurchaseInfoTableViewCell class]) owner:nil options:nil] lastObject];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                //            cell = [[AdvPurchaseInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell.positionTItleLabel.text = [NSString stringWithFormat:@"第%ld版位", (long)_position];
                cell.selectTextLabel.text = [ProviderUtil displayAdvTitle:self.advType];
            }
            
            return cell;
        }
        
        
        
    }
    if (indexPath.section == AdvPurchaseDetailRowCalendar) {
        
        static NSString *CellIdentifier = @"CalendarIdentifier";
        
        AdPurchaseCalendarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[AdPurchaseCalendarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.cellDelegate = self;
            
        }
        
        if (self.viewModel.saleAdvSumList.count > 0) {
            cell.viewModel = self.viewModel;
        }
        
        
        return cell;
    }
    if (indexPath.section == AdvPurchaseDetailRowTime) {
        
        static NSString *CellIdentifier = @"TimeCellID";
        
        AdPurchaseTimeSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[AdPurchaseTimeSelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.cellDelegate = self;
        }
        
        cell.viewModel = self.viewModel;
        
        return cell;
    }
    if (indexPath.section == AdvPurchaseDetailRowSelection) {
        
        static NSString *CellIdentifier = @"SelectionID";
        
        AdvPurchaseOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdvPurchaseOrderTableViewCell class]) owner:nil options:nil] lastObject];
        }
        
        AdPurchaseOrder *data = self.viewModel.advOrderList[indexPath.row];
        
        cell.order = data;
        
        cell.deleteButton.tag = indexPath.row;
        
        [cell.deleteButton addTarget:self action:@selector(onDeleteItem:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
//    if (indexPath.section == AdvPurchaseDetailRowPay) {
    
//        static NSString *CellIdentifier
        
//    }
//    else {
//        
//        static NSString *CellIdentifier = @"CellIdentifier";
//        
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        
//        if (!cell) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//        }
//        
//        return cell;
//    }
    
    return nil;
    
    
}



@end
