//
//  PFNavigationController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/9.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PFNavigationController;
@protocol PFNavigationControllerDelegate <NSObject>
@optional
- (void)pfNavigationController:(PFNavigationController *)pfNavigationController willPopViewControllerAnimated:(BOOL)animated;
@end

@interface PFNavigationController : UINavigationController

@property (nonatomic, weak) id<PFNavigationControllerDelegate> PFDelegate;

@end
