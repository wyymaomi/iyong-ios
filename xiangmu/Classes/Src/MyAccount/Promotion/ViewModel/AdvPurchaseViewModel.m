//
//  AdvPurchaseViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPurchaseViewModel.h"

@implementation AdvPurchaseViewModel

-(NSMutableArray*)saleAdvSumList
{
    if (!_saleAdvSumList) {
        _saleAdvSumList = [NSMutableArray new];
    }
    return _saleAdvSumList;
}


-(NSMutableArray*)saleAdvDetailList
{
    if (!_saleAdvDetailList) {
        _saleAdvDetailList = [NSMutableArray new];
    }
    return _saleAdvDetailList;
}

-(NSMutableArray*)advOrderList
{
    if (!_advOrderList) {
        _advOrderList = [NSMutableArray new];
    }
    return _advOrderList;
}

-(NSMutableDictionary*)advSumDictionary
{
    if (!_advSumDictionary) {
        _advSumDictionary = [NSMutableDictionary new];
    }
    return _advSumDictionary;
}

@end
