//
//  GuideViewControllerViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface GuideViewController : BaseViewController
{
    // 创建页码控制器
    UIPageControl *pageControl;
    // 判断是否是第一次进入应用
    BOOL flag;
}
@end
