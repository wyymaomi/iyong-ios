//
//  UserDefaults.m
//  lottery
//
//  Created by WYY on 14-4-24.
//
//

#import "UserDefaults.h"

@implementation UserDefaults
//单例,检测userdefaluts是否存在
+ (instancetype)sharedInstance
{
    static UserDefaults *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[UserDefaults alloc] init];
    });
    return instance;
}

- (BOOL)hasObjectForKey:(id)key
{
	id value = [[NSUserDefaults standardUserDefaults] objectForKey:key];
	return value ? YES : NO;
}

- (id)objectForKey:(NSString *)key
{
	if ( nil == key )
		return nil;
	
	id value = [[NSUserDefaults standardUserDefaults] objectForKey:key];
	return value;
}

- (void)setObject:(id)value forKey:(NSString *)key
{
	if ( nil == key || nil == value )
		return;
    
    if ((NSNull *)value == [NSNull null]) {
        [self removeObjectForKey:key];
        return;
    }
    
	[[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)removeObjectForKey:(NSString *)key
{
	if ( nil == key )
		return;
	
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)removeAllObjects
{
	[NSUserDefaults resetStandardUserDefaults];
}

- (id)objectForKeyedSubscript:(id)key
{
	return [self objectForKey:key];
}

- (void)setObject:(id)obj forKeyedSubscript:(id)key
{
	if ( obj )
	{
		[self setObject:obj forKey:key];
	}
	else
	{
		[self removeObjectForKey:key];
	}
}

@end
