//
//  AvatarViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AvatarViewController.h"
#import "CameraPhotoService.h"
#import "AvatarViewController+NetworkRequest.h"

@interface AvatarViewController ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,CameraPhotoServiceDelegate>

@property (nonatomic, strong) CameraPhotoService *cameraPhotoService;

@end

@implementation AvatarViewController

- (CameraPhotoService*)cameraPhotoService
{
    if (!_cameraPhotoService) {
        _cameraPhotoService = [[CameraPhotoService alloc] initWithViewController:self type:CameraPhotoTypeSmall];
//        _cameraPhotoService.type = CameraPhotoTypeAvatar;
        _cameraPhotoService.delegate = self;
    }
    return _cameraPhotoService;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"头像";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveUserInfo)];
    [self.avatarTableView reloadData];
//    self.view.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 95*Scale)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    _avatarImageView=[[UIImageView alloc]initWithFrame:CGRectMake((ViewWidth-60*Scale)/2, (bgView.height-60*Scale)/2, 60*Scale, 60*Scale)];
    if (self.avatarImageData != nil && self.avatarImageData.length > 0) {
        _avatarImageView.image = [UIImage imageWithData:self.avatarImageData];
        _avatarImageView.cornerRadius = _avatarImageView.frame.size.width/2;
    }
    else {
        _avatarImageView.image = [UIImage imageNamed:@"icon_avatar"];
    }

    [bgView addSubview:_avatarImageView];
    
    self.avatarTableView.top = bgView.bottom + 2.5*Scale;
}


//-(void)saveAvatar
//{
////    [self update];
//}

-(UITableView *)avatarTableView
{
    if (_avatarTableView == nil) {
        _avatarTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 140, ViewWidth, ViewHeight-140) style:UITableViewStylePlain];
        _avatarTableView.backgroundColor = [UIColor clearColor];
        _avatarTableView.tableFooterView = [UIView new];
//        _avatarTableView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
        _avatarTableView.tableFooterView=[[UIView alloc]init];
        _avatarTableView.delegate=self;
        _avatarTableView.dataSource=self;
        _avatarTableView.bounces=YES;
        _avatarTableView.scrollEnabled=NO;
        _avatarTableView.showsVerticalScrollIndicator=NO;
        _avatarTableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_avatarTableView];
    }
    return _avatarTableView;
}
//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([_avatarTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_avatarTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([_avatarTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_avatarTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}
#pragma UITableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    NSArray *nameArray=@[@"从相册选一张",@"拍一张照片"];
    cell.textLabel.text=nameArray[indexPath.row];
//    cell.textLabel.textColor=[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
//    cell.textLabel.font=[UIFont systemFontOfSize:13];
    cell.textLabel.textColor = DefaultTableRowTitleTextColor;
    cell.textLabel.font = DefaultTableRowTitleFont;
    
//    UIImageView *rightImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-15, 18, 23/3, 42/3)];
//    rightImageView.image = [UIImage imageNamed:@"icon_disclosure"];
//    [cell.contentView addSubview:rightImageView];
    //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return 50;
    return DefaultTableRowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.avatarTableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        [self.cameraPhotoService LocalPhoto];
    }
    else {
        [self.cameraPhotoService takePhoto];
    }
}

- (void)onGetImageData:(NSData*)imageData tag:(NSInteger)tag;
{
    self.avatarImageData = imageData;
    self.avatarImageView.image = [UIImage imageWithData:self.avatarImageData];
    self.changeAvatarImage = YES;
    DLog(@"data.size = %lu", (unsigned long)imageData.length);
}

@end
