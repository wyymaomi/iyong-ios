//
//  AppDelegate+Location.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AppDelegate+Location.h"
#import "LocationService.h"

@implementation AppDelegate (Location)

#pragma mark - 定位

- (void)_startLocation;
{
    // 定位
    [AppConfig currentConfig].defaultCityName = DEFAULT_CITY_NAME;
    [AppConfig currentConfig].defaultCityCode = DEFAULT_CITY_CODE;
    
    [AppConfig currentConfig].gpsCityName = DEFAULT_CITY_NAME;
    [AppConfig currentConfig].gpsCityCode = DEFAULT_CITY_CODE;
    
    [LocationService sharedInstance].delegate = self;
    [[LocationService sharedInstance] startLocation];
}

#pragma mark - Location Service Delegate methods

- (void)onLocateCityServiceUnable;
{
    [UIRoute openSystemSetting:kMsgOpenSystemLocation];
}

- (void)onLocateCityUserDenied;
{
    [UIRoute openSystemSetting:kMsgOpenSystemLocation];
}

- (void)onLocateCitySuccess:(NSString*)city;
{
    if (IsStrEmpty(city)) {
        return;
    }
    
    NSString *cityCode = [MyUtil getCityCodeFromCityName:city];
    if (!IsStrEmpty(cityCode)) {
        [AppConfig currentConfig].defaultCityName = city;
        [AppConfig currentConfig].defaultCityCode = cityCode;
        
        [AppConfig currentConfig].gpsCityName = city;
        [AppConfig currentConfig].gpsCityCode = cityCode;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kOnGpsLocationNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kDisplayAdvNotification object:nil];
    [LocationService saveCityToConfig:city];
}

- (void)onLocateCityFail;
{
    [MsgToolBox showAlert:@"提示" content:@"定位城市失败，请重试!"];
}

- (void)onLocateCityAddressFail;
{
    [MsgToolBox showAlert:@"提示" content:@"获取地址失败，请重试!"];
}



@end
