//
//  NSDictionary+Additional.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NSDictionary+Additional.h"

@implementation NSDictionary (Additional)

+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        @autoreleasepool {
            [objc_getClass("__NSDictionaryM") swizzleMethod:@selector(setObject:forKey:) swizzledSelector:@selector(mutableSetObject:forKey:)];
        }
    });
}

- (void)mutableSetObject:(id)obj forKey:(NSString *)key{
    if (obj && key) {
        [self mutableSetObject:obj forKey:key];
    }
}

@end
