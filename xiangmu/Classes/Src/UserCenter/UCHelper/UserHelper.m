//
//  UCHelper.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UserHelper.h"

@implementation UserHelper


+ (BOOL)validateUserName:(NSString *)userName error:(NSString **)errorMsg
{
    if (userName == nil || userName.length != 11) {
        *errorMsg = @"请输入11位手机号";
        return NO;
    }
    
//    NSString *mobileNoRegex = @"1[0-9]{10,10}";
//    NSPredicate *mobileNoTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNoRegex];
//    BOOL validateOK = [mobileNoTest evaluateWithObject:phoneNum];
//    
//    if (!validateOK) {
//        *errorMsg = @"手机号格式错误";
//        return NO;
//    }
    
    return YES;
}

+(BOOL)validLoginPwd:(NSString*)codeString error:(NSString**)errorMsg
{
    if (IsStrEmpty(codeString)) {
        *errorMsg = @"请输入密码";
        return NO;
    }
    
    if (codeString.length < 6 || codeString.length > 20) {
        *errorMsg = @"请输入6-20位密码";
        return NO;
    }
    
    return YES;
}


+ (BOOL)validatePhoneNum:(NSString *)phoneNum error:(NSString **)errorMsg
{
    if (phoneNum == nil || [phoneNum isEqualToString:@""]) {
        *errorMsg = @"请输入手机号";
        return NO;
    }
    
    if (phoneNum.length != 11) {
        *errorMsg = @"请输入11位手机号";
        return NO;
    }
    
    NSString *mobileNoRegex = @"1[0-9]{10,10}";
    NSPredicate *mobileNoTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNoRegex];
    BOOL validateOK = [mobileNoTest evaluateWithObject:phoneNum];
    
    if (!validateOK) {
        *errorMsg = @"手机号格式错误";
        return NO;
    }
    
    return YES;
}

+ (BOOL)validateRefPhoneNum:(NSString*)phoneNum error:(NSString**)errorMsg;
{
    if (phoneNum == nil) {
        *errorMsg = @"请输入推荐人手机号";
        return NO;
    }
    
    if (phoneNum.length != 11) {
        *errorMsg = @"请输入11位推荐人手机号";
        return NO;
    }
    
    NSString *mobileNoRegex = @"1[0-9]{10,10}";
    NSPredicate *mobileNoTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNoRegex];
    BOOL validateOK = [mobileNoTest evaluateWithObject:phoneNum];
    
    if (!validateOK) {
        *errorMsg = @"推荐人手机号格式错误";
        return NO;
    }
    
    return YES;
}

+(BOOL)validateTelephone:(NSString*)codeString error:(NSString**)errorMsg
{
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    NSPredicate *phoneNoTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    BOOL validateOK = [phoneNoTest evaluateWithObject:codeString];
    
    if (!validateOK) {
        *errorMsg = @"输入的联系电话格式错误";
        return NO;
    }
    
    return YES;
}

+ (BOOL)validatePassWord:(NSString *)codeString error:(NSString**)errorMsg
{
    DLog(@"password = %@", codeString);
    
    if (IsStrEmpty(codeString)) {
        *errorMsg = @"请输入密码";
        return NO;
    }
    
    if (codeString.length < 6 || codeString.length > 16) {
        *errorMsg = @"请输入6-16位密码";
        return NO;
    }
    
//    NSString *searchText = @"rangeOfString";
    NSRange range = [codeString rangeOfString:@"[A-Za-z]+" options:NSRegularExpressionSearch];
    if (range.location == NSNotFound) {
        *errorMsg = @"密码至少有一位字母，请重新输入";
        return NO;
    }
    
//    NSString *		regex = @"(^[A-Za-z0-9]{6,20}$)";
//    NSString*       regex = @"(^[A-Za-z]{6,20}$)";
//    NSString *      regex = @"[A-Za-z]";
//    NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
//    BOOL validateOK = [pred evaluateWithObject:codeString];
//    
//    if (!validateOK) {
//        *errorMsg = @"密码至少有一位字母，请重新输入";
//        *errorMsg = @"请输入6-20位包含数字字母密码";
//        return NO;
//    }
    
    
    return YES;
    
}

+ (BOOL)validateCodeString:(NSString *)codeString error:(NSString **)errorMsg
{
    if(IsStrEmpty(codeString))
    {
        *errorMsg = @"请输入验证码";
        return NO;
    }
    
    if (codeString.length != 6) {
        *errorMsg = @"请输入6位验证码";
        return NO;
    }
    
//    NSString *verifyCodeRegex = [NSString stringWithFormat:@"([a-z,A-Z,0-9]+)"];
//    NSPredicate *verifyCodeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", verifyCodeRegex];
//    if ([verifyCodeTest evaluateWithObject:codeString]==NO) {
//        *errorMsg = L(@"Please_input_correct_VerifyNum");
//        return NO;
//    }
    
    NSString *regex= [NSString stringWithFormat:@"^\\d{6}$"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if ([predicate evaluateWithObject:codeString]==NO) {
        *errorMsg = @"请输入6位数字验证码";//L(@"Please_input_correct_VerifyNum");
        return NO;
    }
    
    return YES;
}

+ (void)cancelTimer
{
    if (_timer != nil) {
        dispatch_source_cancel(_timer);
        _timer = nil;
    }
}

+ (void)verificationCode:(void(^)())blockYes blockNo:(void(^)(id time))blockNo {
    
    if (_timer != nil) {
        dispatch_source_cancel(_timer);
    }

    
    __block int timeout = 59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                blockYes();
            });
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
//                NSLog(@"____%@",strTime);
                blockNo(strTime);
                
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

#pragma mark - 支付密码验证
+ (BOOL)verifyPaypwd:(NSString*)payPwd error:(NSString**)errorMsg;
{
    if(IsStrEmpty(payPwd))
    {
        *errorMsg = MSG_PAY_PWD_NULL;
        return NO;
    }
    
    if (payPwd.length != 6) {
        *errorMsg = MSG_INVALID_PAY_PWD;
        return NO;
    }
    
    NSString *regex= [NSString stringWithFormat:@"^\\d{6}$"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if ([predicate evaluateWithObject:payPwd]==NO) {
        *errorMsg = MSG_INVALID_PAY_PWD;//L(@"Please_input_correct_VerifyNum");
        return NO;
    }
    
    return YES;
}

+(BOOL)validateTwicePwd:(NSString*)pwd1 pwd2:(NSString*)pwd2 error:(NSString**)errorMsg;
{
    if (![pwd1 isEqualToString:pwd2]) {
        *errorMsg = @"两次输入密码不同，请重新输入";
        return NO;
    }
    return YES;
}

+ (BOOL)validateCompanyName:(NSString*)companyName error:(NSString**)errorMsg;
{
    if (IsStrEmpty(companyName)) {
        *errorMsg = @"请输入公司名称";
        return NO;
    }
    
    return YES;
}

+ (BOOL)validateNickName:(NSString*)nickName error:(NSString**)errorMsg;
{
    if (IsStrEmpty(nickName)) {
        *errorMsg = @"请输入昵称";
        return NO;
    }
    
    return YES;
}

+ (BOOL)validateIdcard:(NSString*)idcardStr error:(NSString**)errorMsg;
{
    if (IsStrEmpty(idcardStr)) {
        *errorMsg = @"请输入身份证号";
        return NO;
    }
    
    if (![StringUtil isIdentityCardNo:idcardStr]) {
        *errorMsg = @"请输入正确的身份证号";
        return NO;
    }
    
    return YES;
}

+ (BOOL)validateBankcardNo:(NSString*)bankcardNo error:(NSString**)errorMsg;
{
    if (IsStrEmpty(bankcardNo)) {
        *errorMsg = @"请输入银行卡号";
        return NO;
    }
    
    return YES;
}


+ (BOOL)validateEmail:(NSString *)codeString error:(NSString **)errorMsg
{
    if (IsStrEmpty(codeString)) {
        *errorMsg = @"请输入您的邮箱地址";
        return NO;
    }
    
    NSString *verifyCodeRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *verifyCodeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", verifyCodeRegex];
    if ([verifyCodeTest evaluateWithObject:codeString]==NO) {
        *errorMsg = @"请输入正确格式的邮箱地址";
        return NO;
    }
    
    return YES;
}




@end
