//
//  UIImage+ImageDecoder.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageDecoder)

+ (nullable UIImage *)decodedImageWithImage:(nullable UIImage *)image;

+ (nullable UIImage *)decodedAndScaledDownImageWithImage:(nullable UIImage *)image;

@end
