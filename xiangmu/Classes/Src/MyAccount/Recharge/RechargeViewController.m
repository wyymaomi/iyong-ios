//
//  RechargeViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RechargeViewController.h"
#import <IPSUPMPSDK/IPSUpmpViewController.h>
#import "Bankcard.h"
#import "TPKeyboardAvoidingTableView.h"
#import "RechargeTableViewCell.h"
#import "PaymentView.h"
#import "InputPaypwdView.h"
#import "BBAlertView.h"
#import "BankcardViewController.h"
#import "RechargeDetailViewController.h"
#import "UpmpPay.h"

@interface RechargeViewController ()<UITableViewDelegate, UITableViewDataSource, BancardListViewDelegate, IPSUPMPDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (strong, nonatomic) UIButton *okButton;
@property (nonatomic, strong) UITextField *transAmountTextField;//交易金额输入框
@property (nonatomic, strong) BBAlertView *paymentAlertView;
@property (nonatomic, strong) BBAlertView *bankcardAlertView;
@property (nonatomic, strong) Bankcard *bankcard;
//@property (nonatomic, strong) NSString *bankcardNumber;// 银行卡号
@property (nonatomic, strong) NSString *transAmount;// 金额
@property (nonatomic, assign) double drawMoneyRate; // 提现费率
@property (nonatomic, assign) double drawMoneyFee; // 提现费用
@property (nonatomic, assign) double drawRealMoney; // 实际提现金额 扣除费率后的提现金额

@end

@implementation RechargeViewController

-(BBAlertView*)bankcardAlertView
{
    if (!_bankcardAlertView) {
        _bankcardAlertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleBankcard Title:nil message:nil customView:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
        _bankcardAlertView.bankcardListView.delegate = self;
    }
    return _bankcardAlertView;
}

-(BBAlertView*)paymentAlertView
{
    if (!_paymentAlertView) {
        _paymentAlertView = [[BBAlertView alloc] initWithStyle: BBAlertViewStylePayment Title:nil message:nil customView:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    }
    return _paymentAlertView;
}

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = (self.type == 0)?@"充值":@"提现";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    if (_type == 1) {
        [self registKVO];
        self.drawMoneyRate = 3;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_type == 1) {
//        [self getDrawMoenyRate];
    }
    
}

-(void)dealloc
{
    [self.paymentAlertView.paymentView.payInputView.pwdTextField resignFirstResponder];
    if (_type == 1) {
        [self unRegistKVO];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 

#pragma mark -
#pragma mark KVO

- (void)registKVO
{
    [self addObserver:self
           forKeyPath:@"transAmount"
              options:NSKeyValueObservingOptionNew
              context:NULL];
}

- (void)unRegistKVO
{
    [self removeObserver:self
              forKeyPath:@"transAmount"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"transAmount"])
    {
        double transAmount = [_transAmount doubleValue];
        if (transAmount == 0) {
            self.drawMoneyFee = 0;
            self.drawRealMoney = 0;
        }
        else {
            self.drawMoneyFee = ceilf(transAmount * self.drawMoneyRate / 100.f);
            self.drawRealMoney = transAmount - self.drawMoneyFee;
        }
    }
}

#pragma mark -

-(void)onAddBankcard;
{
    [self.bankcardAlertView dismiss];
    BankcardViewController *viewController = [[BankcardViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)showBankcardListView
{
    [self getBankcardList];
}

-(void)getBankcardList
{
    self.nextAction = @selector(getBankcardList);
    self.object1 = nil;
    self.object2 = nil;
    
    [self showHUDIndicatorViewAtCenter:@"正在查询银行卡列表，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] getBankcardList:^(id responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        BankcardResponseData *bankcardResponseData = [[BankcardResponseData alloc] initWithDictionary:responseData error:nil];
        if ([bankcardResponseData.code integerValue] == STATUS_OK) {
            NSArray *dataList = bankcardResponseData.data;
            if (dataList.count > 0 ) {
                strongSelf.bankcardAlertView.bankcardListView.dataList = bankcardResponseData.data;
                [strongSelf.bankcardAlertView.bankcardListView.tableView reloadData];
                [strongSelf.bankcardAlertView show];
            }
            else {
//                WeakSelf
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定银行卡" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        BankcardViewController *viewController = [[BankcardViewController alloc] init];
                        [strongSelf.navigationController pushViewController:viewController animated:YES];
                    }
                }];
            }

        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[bankcardResponseData.code integerValue]]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

-(void)onBankcardListSelect:(NSInteger)selectedRow;
{
    if (self.bankcardAlertView.bankcardListView.dataList.count > 0) {
        self.bankcard = self.bankcardAlertView.bankcardListView.dataList[selectedRow];
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:0 inSection:1];
        RechargeTableViewCell *cell2 = [self.tableView cellForRowAtIndexPath:indexPath2];
        [cell2.valueTextField becomeFirstResponder];
        
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        RechargeTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.valueTextField.text = [NSString stringWithFormat:@"%@(%@)", self.bankcard.bankName, [self.bankcard.number substringFromIndex:self.bankcard.number.length-4]];
        [self.bankcardAlertView dismiss];
    }

}

- (IBAction)doRecharge:(id)sender {
    
    // 判断是否选择了银行卡
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    RechargeTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
//    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:0 inSection:1];
//    RechargeTableViewCell *cell2 = [self.tableView cellForRowAtIndexPath:indexPath2];
//    [cell2.valueTextField resignFirstResponder];
    [self.transAmountTextField resignFirstResponder];
    
    if (IsStrEmpty(cell.valueTextField.text.trim)) {
        [self showAlertView:@"请选择银行卡"];
        return;
    }
    
    self.transAmount = [NSString stringWithFormat:@"%.2f", [self.transAmountTextField.text.trim doubleValue]];
    if (IsStrEmpty(self.transAmount) || [self.transAmount doubleValue] <= 0) {
        NSString *alertMsg = (self.type == 0)?@"请输入充值金额":@"请输入提现金额";
        WeakSelf
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:alertMsg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                [weakSelf.transAmountTextField becomeFirstResponder];
            }
        }];
        return;
    }
    
    if ([self.transAmount integerValue] < 100) {
        [MsgToolBox showToast:@"充值金额必须大于100元"];
        return;
    }
    
    if ([self.transAmount integerValue] % 100 != 0) {
        if (self.type == 0) {
            [MsgToolBox showToast:@"充值金额必须是100的整数倍"];
            return;
        }
        else {
            [MsgToolBox showToast:@"提现金额必须是100的整数倍"];
            return;
        }
        return;
    }
    // 判断提现金额是否大于1
    
    // 判断提现金额是否大于账户余额
    if ((self.type == 1) && ([self.transAmount doubleValue] > [self.accountMoney doubleValue])) {
        [self showAlertView:@"可提现金额大于账户余额,请重新输入"];
        return;
    }
    
    self.paymentAlertView.paymentView.titleLabel.text = (self.type == 0) ? @"充值" : @"提现";
    self.paymentAlertView.paymentView.moneyLabel.text = [NSString stringWithFormat:@"¥ %@", self.transAmount];
    self.paymentAlertView.paymentView.rateMoneyLabel.hidden = YES;
    WeakSelf
    self.paymentAlertView.paymentView.payInputView.clickBlock = ^(){
        [weakSelf.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
    };
    self.paymentAlertView.paymentView.payInputView.completeHandle = ^(NSString *payPassword) {
        [weakSelf checkPayPwd:payPassword];
    };
    [self.paymentAlertView show];
    [self.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
    
}

- (IBAction)doDrawMoney:(id)sender
{
    // 判断是否选择了银行卡
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    RechargeTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    [self.transAmountTextField resignFirstResponder];
    
    if (IsStrEmpty(cell.valueTextField.text.trim)) {
        [self showAlertView:@"请选择银行卡"];
        return;
    }
    
    self.transAmount = [NSString stringWithFormat:@"%.2f", [self.transAmountTextField.text.trim doubleValue]];
    if (IsStrEmpty(self.transAmount) || [self.transAmount doubleValue] < 100.0f) {
        NSString *alertMsg = @"提现金额必须大于100元";
        WeakSelf
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:alertMsg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                [weakSelf.transAmountTextField becomeFirstResponder];
            }
        }];
        return;
    }
    
    if ([self.transAmount integerValue] < 100) {
        [MsgToolBox showToast:@"提现金额必须大于100元"];
        return;
    }
    
    if ([self.transAmount integerValue] % 100 != 0) {
//        if (self.type == 0) {
//            [MsgToolBox showToast:@"提现金额必须是100的整数倍"];
//            return;
//        }
//        else {
            [MsgToolBox showToast:@"提现金额必须是100的整数倍"];
            return;
//        }
//        return;
    }
    
    // 判断提现金额是否大于账户余额
    if ((self.type == 1) && ([self.transAmount doubleValue] > [self.accountMoney doubleValue])) {
        [self showAlertView:@"可提现金额大于账户余额,请重新输入"];
        return;
    }
    
    self.paymentAlertView.paymentView.titleLabel.text = (self.type == 0) ? @"充值" : @"提现";
    self.paymentAlertView.paymentView.moneyLabel.text = [NSString stringWithFormat:@"¥ %@", self.transAmount];
    
    self.paymentAlertView.paymentView.rateMoneyLabel.hidden = (_type == 0)? YES : NO;
    self.paymentAlertView.paymentView.rateMoneyLabel.text = [NSString stringWithFormat:@"扣除手续费%.2f元", self.drawMoneyFee];
    WeakSelf
    self.paymentAlertView.paymentView.payInputView.clickBlock = ^(){
        [weakSelf.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
    };
    self.paymentAlertView.paymentView.payInputView.completeHandle = ^(NSString *payPassword) {
        [weakSelf checkPayPwd:payPassword];
    };
    [self.paymentAlertView show];
    [self.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
}

-(void)doWithdrawMoney
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, WITHDRAW_API];
    NSString *params = [NSString stringWithFormat:@"price=%.f&realPrice=%.f&bankcardId=%@", [self.transAmount doubleValue], self.drawRealMoney,self.bankcard.id];
    
    [self showHUDIndicatorViewAtCenter:@"正在申请提现，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf resetAction];
        
        NSInteger code = [responseData[@"code"] integerValue];
        if (code == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"提现申请成功，提现金额将在24小时内到账" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf.transAmountTextField resignFirstResponder];
                    [weakSelf.paymentAlertView.paymentView.payInputView.pwdTextField resignFirstResponder];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];

        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code]];
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
        
    }];

    
}

#pragma mark - 获取提现费率

- (void)getDrawMoenyRate
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, WITHDRAW_RATE_API];
//    NSString *params = [NSString stringWithFormat:@"price=%.f&bankcardId=%@", [self.transAmount doubleValue], self.bankcard.id];
    NSString *params = @"";
    
//    [self showHUDIndicatorViewAtCenter:@"正在申请提现，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
//        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf resetAction];
        
        NSInteger code = [responseData[@"code"] integerValue];
        
        if (code == STATUS_OK) {
            
            weakSelf.drawMoneyRate = [responseData[@"data"] doubleValue];
            
            [weakSelf.tableView reloadData];
            
//            weakSelf.drawMoneyRate = responseData[@"data"];
            
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code]];
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
        
    }];
}



-(void)checkPayPwd:(NSString*)inputPwd
{
    self.nextAction = @selector(checkPayPwd:);
    self.object1 = inputPwd;
    self.object2 = nil;
    
    [self.paymentAlertView.paymentView.payInputView resetInputView];
    [self.paymentAlertView.paymentView.payInputView.pwdTextField resignFirstResponder];
    [self.paymentAlertView dismiss];
    [self showHUDIndicatorViewAtCenter:@"正在校验支付密码，请稍候"];
    
    WeakSelf
    [[NetworkManager sharedInstance] verifyPaypassword:inputPwd success:^(NSDictionary *responseData) {
//        StrongSelf
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                if (weakSelf.type == 0) {
                    [weakSelf getUpmpBillNo];
                }
                else {
                    [weakSelf doWithdrawMoney];
                }
            }
            else if([data integerValue] == 0){
                // 校验支付密码失败
//                [MsgToolBox showToast:@"支付密码输入校验失败，请重新输入"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"支付密码输入校验失败，请重新输入" delegate:self cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        [weakSelf.paymentAlertView show];
                        [weakSelf.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
                        [weakSelf.paymentAlertView.paymentView.payInputView resetInputView];
                    }
                    
                }];
            }
        }
        else {
            // 校验支付密码失败
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[self getErrorMsg:code_status] delegate:self cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [weakSelf.paymentAlertView show];
                    [weakSelf.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
                    [weakSelf.paymentAlertView.paymentView.payInputView resetInputView];
                }
                
            }];
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

#pragma mark - 获取环讯支付订单号

-(void)getUpmpBillNo;
{
    NSDictionary *dict = @{kRechargeAmount:self.transAmountTextField.text.trim,
                           kBankcardNo:self.bankcard.number};
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, RECHARGE_BILL_NO_API];
    UpmppayOrder *payOrder = [UpmppayOrder new];
    UpmpPay *upmpPay = [payOrder getUpmpData:@"" tranAmt:dict[kRechargeAmount] bankcardNo:dict[kBankcardNo]];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:upmpPay.description success:^(id responseData) {
        StrongSelf
        NSDictionary *result = (NSDictionary*)responseData;
        NSInteger code = [result[@"code"] integerValue];
        NSDictionary *data = result[@"data"];
        
        if (code == STATUS_OK) {
            NSString *billNo = data[@"orderCode"];
            NSString *notifyUrl = data[@"notifyUrl"];
            if (!IsStrEmpty(billNo) && !IsStrEmpty(notifyUrl)) {
                upmpPay.merBillNo = billNo;
                upmpPay.merNoticeUrl = notifyUrl;
                [strongSelf upmpPay:upmpPay];
            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf showAlertView:errorDesc];
    }];
}

-(void)upmpPay:(UpmpPay*)upmpPay
{
    UpmppayOrder *payOrder = [UpmppayOrder new];
    NSString *requestDES;
    NSString *signString = [payOrder generateSignString:upmpPay merRequestDes:&requestDES];
    [IPSUpmpViewController IPSStartPayWithMerCode:UPMP_MER_CODE bankCard:self.bankcard.number sign:signString merRequestInfo:requestDES delegate:self viewController:self];
}

#pragma mark - Upmp delegate method
-(void)orderCompletedStatus:(NSString*)status
                    merCode:(NSString*)merCode
                  merBillNo:(NSString*)merBillNo
                  orderDesc:(NSString*)orderDesc
                    tranAmt:(NSString*)tranAmt;
{
    if ([status isEqualToString:@"000000"]) {
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, RECHARGE_CALLBACK_API];
        NSString *params = [NSString stringWithFormat:@"status=%@&merCode=%@&merBillNo=%@&tranAmt=%@", status, merCode, merBillNo, tranAmt];
        WeakSelf
        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
            StrongSelf
            
            NSInteger code_status = [responseData[@"code"] integerValue];
            
            if (code_status == STATUS_OK) {

                [strongSelf.navigationController popViewControllerAnimated:YES];
                
//                strongSelf.navigationController popToViewController:<#(nonnull UIViewController *)#> animated:<#(BOOL)#>

//                NSArray *vcArray = self.navigationController.viewControllers;
//                
//                for(UIViewController *vc in vcArray)
//                {
//                    if ([vc isKindOfClass:[AccountBalanceViewController class]])
//                    {
//                        [self.navigationController popToViewController:vc animated:YES];
//                    }
//                }
                
//                RechargeDetailViewController *viewController = [[RechargeDetailViewController alloc] init];
//                NSIndexPath *bankcardIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//                RechargeTableViewCell *bankcardCell = [self.tableView cellForRowAtIndexPath:bankcardIndexPath];
//                viewController.bankcardInfo = bankcardCell.valueTextField.text.trim;
////                NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:1 inSection:1];
//                viewController.transAmount = strongSelf.transAmountTextField.text.trim;//[NSString stringWithFormat:@"¥%@", self.transAmount];
//                [strongSelf.navigationController pushViewController:viewController animated:YES];
            }
            else if (code_status ==STATUS_FAIL) {
                NSString *msg = responseData[@"data"];
                if (IsStrEmpty(msg)) {
                    msg = @"充值失败，请重试";
                }
                [strongSelf showAlertView:msg];
            }
            else {
                [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
            }
            
        } andFailure:^(NSString *errorDesc) {
            
            StrongSelf
            [strongSelf showAlertView:errorDesc];
            
        }];
    }
    
}


#pragma mark - 充值

//-(UpmpPay*)getUpmpData:(NSString*)billNo tranAmt:(NSString*)tranAmt bankcardNo:(NSString*)bankcardNo
//{
//    NSDate *date = [NSDate date];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"yyyyMMddHHmmss";
//    NSString *dateStr = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:date]];
//    
//    UpmpPay *upmpPay = [[UpmpPay alloc] init];
//    upmpPay.merCode = UPMP_MER_CODE;
//    upmpPay.accCode = UPMP_ACC_CODE;
//    upmpPay.merBillNo = billNo;
//    upmpPay.tranAmt = tranAmt;
//    upmpPay.ordPerVal = @"1";
//    upmpPay.requestTime = dateStr;
//    upmpPay.orderDesc = @"recharge";
//    upmpPay.bankCard = bankcardNo;
////    upmpPay.merNoticeUrl = UPMP_NOTICE_URL;
//    upmpPay.ccyCode = @"156";
//    upmpPay.prdCode = @"2301";
//    
//    return upmpPay;
//}
//
//-(void)getBillNo
//{
//    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, RECHARGE_BILL_NO_API];
//    
//    UpmpPay *upmpPay = [self getUpmpData:nil tranAmt:self.transAmount bankcardNo:self.bankcard.number];
//    
//    WeakSelf
//    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:upmpPay.description success:^(id responseData) {
//        StrongSelf
//        NSDictionary *result = (NSDictionary*)responseData;
//        NSInteger code = [result[@"code"] integerValue];
//        if (code == STATUS_OK) {
//            NSString *billNo = result[@"data"];
//            NSString *noticeUrl = result[@"notifyUrl"];
//            if (!IsStrEmpty(billNo) && !IsStrEmpty(noticeUrl)) {
//                upmpPay.merBillNo = billNo;
//                upmpPay.merNoticeUrl = noticeUrl;
//                [strongSelf upmpPay:upmpPay];
//                
//            }
//        }
//        else {
//            [strongSelf showAlertView:[strongSelf getErrorMsg:code]];
//        }
//    } andFailure:^(NSString *errorDesc) {
//        StrongSelf
//        [strongSelf showAlertView:errorDesc];
//    }];
//}

//-(void)upmpPay:(UpmpPay*)upmpPay
//{
//    NSString* bankCardStr = [IPSUpmpViewController TripleDES:self.bankcard.number encryptOrDecrypt:kCCEncrypt key:UPMP_DES_KEY desIv:UPMP_DES_IV];
//    
//    NSMutableDictionary* merRequestDict = [[NSMutableDictionary alloc]init];
//    [merRequestDict setValue:upmpPay.accCode forKey:@"accCode"];
//    [merRequestDict setValue:upmpPay.merBillNo forKey:@"merBillNo"];
//    [merRequestDict setValue:upmpPay.ccyCode forKey:@"ccyCode"];
//    [merRequestDict setValue:upmpPay.prdCode forKey:@"prdCode"];
//    [merRequestDict setValue:upmpPay.tranAmt forKey:@"tranAmt"];
//    [merRequestDict setValue:upmpPay.requestTime forKey:@"requestTime"];
//    [merRequestDict setValue:upmpPay.ordPerVal forKey:@"ordPerVal"];
//    [merRequestDict setValue:upmpPay.merNoticeUrl forKey:@"merNoticeUrl"];
//    [merRequestDict setValue:upmpPay.orderDesc forKey:@"orderDesc"];
//    [merRequestDict setValue:bankCardStr forKey:@"bankCard"];
//    
//    DLog(@"merRequestDict:%@",merRequestDict);
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:merRequestDict options:NSJSONWritingPrettyPrinted error:&error];
//    NSString *requestJason =[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    DLog(@"requestJason=%@", requestJason);
//    NSString* merRequestDes = [IPSUpmpViewController TripleDES:requestJason encryptOrDecrypt:kCCEncrypt key:UPMP_DES_KEY desIv:UPMP_DES_IV];
//    DLog(@"加密数据:%@",merRequestDes);
//    
//    NSString* merRequest = [NSString stringWithFormat:@"%@%@%@",UPMP_MER_CODE,merRequestDes,UPMP_MD5_CERT];
//    DLog(@"md5字段：%@",merRequest);
//    
//    NSString* merRequestMd5 = [IPSUpmpViewController md5:merRequest];
//    DLog(@"md5:%@",merRequestMd5);
//    
//    [IPSUpmpViewController IPSStartPayWithMerCode:UPMP_MER_CODE bankCard:self.bankcard.number sign:merRequestMd5 merRequestInfo:merRequestDes delegate:self viewController:self];
//}

#pragma mark - Upmp delegate method
//-(void)orderCompletedStatus:(NSString*)status
//                    merCode:(NSString*)merCode
//                  merBillNo:(NSString*)merBillNo
//                  orderDesc:(NSString*)orderDesc
//                    tranAmt:(NSString*)tranAmt;
//{
//    if ([status isEqualToString:@"000000"]) {
//        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, RECHARGE_CALLBACK_API];
//        NSString *params = [NSString stringWithFormat:@"status=%@&merCode=%@&merBillNo=%@&tranAmt=%@", status, merCode, merBillNo, tranAmt];
//        WeakSelf
//        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
//            StrongSelf
//            
//            NSInteger code_status = [responseData[@"code"] integerValue];
//            
//            if (code_status == STATUS_OK) {
//                
//                RechargeDetailViewController *viewController = [[RechargeDetailViewController alloc] init];
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//                RechargeTableViewCell *cell = [strongSelf.tableView cellForRowAtIndexPath:indexPath];
//                viewController.bankcardInfo = cell.valueTextField.text.trim;
//                viewController.transAmount = [NSString stringWithFormat:@"¥%@", self.transAmount];
//                [strongSelf.navigationController pushViewController:viewController animated:YES];
//            }
//            else if (code_status ==STATUS_FAIL) {
//                NSString *msg = responseData[@"data"];
//                if (IsStrEmpty(msg)) {
//                    msg = @"充值失败，请重试";
//                }
//                [strongSelf showAlertView:msg];
//            }
//            else {
//                [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
//            }
//            
//        } andFailure:^(NSString *errorDesc) {
//            
//            StrongSelf
//            [strongSelf showAlertView:errorDesc];
//            
//        }];
//    }
//
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RechargeTableViewCell *cell = [RechargeTableViewCell cellWithTableView:tableView indexPath:indexPath];
    if (indexPath.section == 1) {
        self.transAmountTextField = cell.valueTextField;
        cell.valueTextField.placeholder = (self.type == 0) ? @"请输入充值金额" : [NSString stringWithFormat:@"本次可提现金额:%@元", self.accountMoney];
        cell.valueTextField.delegate = self;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.00001f;
    }
    return 500;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        UIView *view = [[UIView alloc] init];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, self.view.frame.size.width, 20)];
        label.text = (self.type == 0)?@"充值金额24小时内到账，充值金额必须是100元的整数倍":@"提现金额24小时内到账，提现金额必须是100元的整数倍";
        label.textColor = [UIColor lightGrayColor];
        label.font = FONT(12);
        [view addSubview:label];
        
        NSString *buttonTitle = (self.type==0)?@"确认充值":@"确认提现";
        [self.okButton setTitle:buttonTitle forState:UIControlStateNormal];
        self.okButton.frame = CGRectMake(20, label.bottom + 10, ViewWidth-40, 40);
        [self.okButton blueStyle];
        [view addSubview:self.okButton];
        
        if (_type) {
            
            NSMutableString *mutableString = [NSMutableString new];
            [mutableString appendString:@"温馨提示\r\n"];
            [mutableString appendString:@"1.每周一至周日9:00-21:00的提款申请当天处理，21:00以后的提款申请延至第二天处理(法定假日处理时间另行公示);\r\n"];
            [mutableString appendString:@"2.提现申请提交后24小时内可到账，若超过3个工作日未到账，请与本站客服联系；\r\n"];
            [mutableString appendString:@"3.每日提款申请最多为5次\r\n"];
            [mutableString appendFormat:@"4.提现需要收取%.2f％手续费，手续费向上取为整数，如手续费1.4元，实际收取向上取整为2元", self.drawMoneyRate];
            
            CGSize titleSize = [mutableString boundingRectWithSize:CGSizeMake(ViewWidth-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size;
            
            
            UILabel *label = [UILabel new];
            
            label.frame = CGRectMake(15, self.okButton.bottom + 20, ViewWidth-30, titleSize.height);
            label.numberOfLines = 0;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            label.text = mutableString;
            label.font = FONT(14);
            label.textColor = [UIColor darkGrayColor];
            [view addSubview:label];
        }

        return view;
    }
    return [[UIView alloc] init];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        [self showBankcardListView];
    }
}


#pragma mark - UITextFieldDelegate


//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
//{
//    NSString *str = textField.text.trim;
//    if ([str startsWith:@"0"]) {
//        return NO;
//    }
//    return YES;
//}

//#define myDotNumbers @"0123456789"
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //输入字符限制
    if ((range.location == 0) && ([string isEqualToString:@"0"]))
    {
        return NO;
    }
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:myDotNumbers]invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if (filtered.length == 0) {
        //支持删除键
        return [string isEqualToString:@""];
    }
    if (textField.text.length == 0) {
        return ![string isEqualToString:@"."];
    }
    //第一位为0，只能输入.
    else if (textField.text.length == 1){
        if ([textField.text isEqualToString:@"0"]) {
            return [string isEqualToString:@"."];
//            return NO;
        }
    }
    else{//只能输入一个.
        if ([textField.text rangeOfString:@"."].length) {
            if ([string isEqualToString:@"."]) {
                return NO;
            }
            //两位小数
            NSArray *ary =  [textField.text componentsSeparatedByString:@"."];
            if (ary.count == 2) {
                if ([ary[1] length] == 2) {
                    return NO;
                }
            }
        }
    }
    
    return YES;
}


- (UIButton*)okButton
{
    if (!_okButton) {
        _okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (self.type == 0) {
            [_okButton addTarget:self action:@selector(doRecharge:) forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            [_okButton addTarget:self action:@selector(doDrawMoney:) forControlEvents:UIControlEventTouchUpInside];
        }
        
//        _okButton.frame = CGRectMake(20, 20, ViewWidth-40, 40);
        [_okButton blueStyle];
    }
    return _okButton;
}




@end
