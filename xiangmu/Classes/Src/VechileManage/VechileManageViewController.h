//
//  VechileManageViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger, BindStatus)
{
    BindStatusInProgress=1,
    BindStatusFinish,
    BindStatusReject
};

@interface VechileManageViewController : BaseViewController

@property (nonatomic, assign) BindStatus bindStatus;

@end
