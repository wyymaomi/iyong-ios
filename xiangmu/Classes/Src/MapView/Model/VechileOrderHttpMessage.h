//
//  VechileOrderHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

//typedef NS_ENUM(NSUInteger, VechielType)
//{
//    VechileTypeComfortable = 1,
//    VechileTypeCommercial,
//    VechielTypeLuxure,
//};

#define DEFAULT_CAR_TYPE 4

typedef NS_ENUM(NSUInteger, CarMode)
{
    CarModeImmediate=1,// 立即用车
    CarModeBooking=2 // 预约用车
};

typedef NS_ENUM(NSUInteger, RouteType)
{
    RouteTypeDistanceFist=1,// 距离最短
    RouteTypeTimeFirst,// 时间最短
    RouteTypeAvoidTrafficJams // 避免拥堵
};

@interface VechileOrderHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *orderId; // 订单号
@property (nonatomic, assign) NSInteger useCarMode; // 用车类型
@property (nonatomic, strong) NSString *carDateTime;
@property (nonatomic, assign) NSInteger carType; //用车类型
@property (nonatomic, assign) NSInteger carUnitPrice; // 用车价格
@property (nonatomic, assign) NSInteger carStartPrice; // 起步价
@property (nonatomic, assign) NSInteger carStartDistance; // 起步公里
@property (nonatomic, assign) NSInteger carRouteType; // 路线规划方案
@property (nonatomic, assign) CLLocationCoordinate2D startCoordiante;// 起始地点经纬度
@property (nonatomic, strong) NSString *startLocation;// 上车地点
@property (nonatomic, strong) NSString *startAddress; // 上车地址
@property (nonatomic, strong) NSString *startCity;// 上车城市
@property (nonatomic, strong) NSString *startCityCode; // 上车城市编码
@property (nonatomic, assign) CLLocationCoordinate2D endCoordiante; // 结束地点经纬度
@property (nonatomic, strong) NSString *endLocation;// 结束地点
@property (nonatomic, strong) NSString *endAddress; // 下车地址
@property (nonatomic, strong) NSString *endCity;// 结束城市
@property (nonatomic, strong) NSString *endCityCode; // 结束城市地区编码
@property (nonatomic, assign) CGFloat distance;// 驾车距离
@property (nonatomic, assign) CGFloat carPrice;
@property (nonatomic, strong) NSString *driverMobile;// 司机手机号
@property (nonatomic, strong) NSString *driverName;
@property (nonatomic, strong) NSString *carModel;// 车型
@property (nonatomic, strong) NSString *carNumber; // 车牌号
@property (nonatomic, strong) NSString *companyName; // 公司名称
@property (nonatomic, assign) double receiveCutoffTime;// 司机接单截止时间
@property (nonatomic, assign) double receiveOrderTime;//司机实际接单时间 以秒为单位
@property (nonatomic, assign) double receiveOrderTimeInMillions;// 以毫秒为单位
@property (nonatomic, strong) NSString *telephone;// 用车人电话

@end
