//
//  UIViewController+Addition.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Addition)

-(BOOL)openCompanyModePage;
//-(void)openCompanyModePage;
//-(void)openCompanyModePage;

- (void)showHUDIndicatorViewAtCenter:(NSString*)indiTitle;
- (void)hideHUDIndicatorViewAtCenter;
- (void)showAlertView:(NSString*)msg;

- (void)showLoginAlertView;
- (void)showLoginAlertViewWithMsg:(NSString*)msg;
- (void)presentLogin;
- (void)presentAccountLogin;
- (void)presentGestureSetting:(NSInteger)source_from;
- (void)presentGestureLogin;

- (void)dismissToRootViewController:(BOOL)flag completion: (void (^ __nullable)(void))completion;

- (void)gotoWebpage:(NSString*)url;

-(void)openWebView:(NSString*)url;

-(void)gotoRealnameCertificationPage;

@end

NS_ASSUME_NONNULL_END
