//
//  CustomActionSheetDatePicker.h
//
//  Created by 湛思科技 on 16/4/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AbstractActionSheetPicker.h"

@class CustomActionSheetDatePicker;

typedef void(^CustomActionDateDoneBlock)(CustomActionSheetDatePicker *picker, NSString *selectDateTime);
typedef void(^CustomActionDateCancelBlock)(CustomActionSheetDatePicker *picker);

@interface CustomActionSheetDatePicker : AbstractActionSheetPicker<UIPickerViewDelegate, UIPickerViewDataSource>

+(instancetype)showPickerWithTitle:(NSString*)title
                         doneBlock:(CustomActionDateDoneBlock)doneBlock
                       cancelBlock:(CustomActionDateCancelBlock)cancelBlockOrNil
                            origin:(id)origin;

@property (nonatomic, copy) CustomActionDateDoneBlock onActionSheetDone;
@property (nonatomic, copy) CustomActionDateCancelBlock onActionSheetCancel;

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *year;
@property (strong, nonatomic) NSString *day;
@property (strong, nonatomic) NSString *hour;
@property (strong, nonatomic) NSString *minute;

@property (strong, nonatomic) NSString *resultDate;

@property (strong, nonatomic) NSDate *startDate;


@end
