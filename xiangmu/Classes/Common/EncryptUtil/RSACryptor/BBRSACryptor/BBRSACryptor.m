//
//  BBRSACryptor.m
//  BBRSACryptor-ios
//
//  Created by liukun on 14-3-21.
//  Copyright (c) 2014年 liukun. All rights reserved.
//

#import "BBRSACryptor.h"
#import "GTMBase64.h"


#define DocumentsDir [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define OpenSSLRSAKeyDir [DocumentsDir stringByAppendingPathComponent:@".openssl_rsa"]
#define OpenSSLClientRSAPublicKeyFile [OpenSSLRSAKeyDir stringByAppendingPathComponent:@"client.publicKey.pem"]
#define OpenSSLClientRSAPrivateKeyFile [OpenSSLRSAKeyDir stringByAppendingPathComponent:@"client.privateKey.pem"]
#define OpenSSLServerRSAPublicKeyFile [OpenSSLRSAKeyDir stringByAppendingPathComponent:@"server.publicKey.pem"]
#define OpenSSLServerRSAPrivateKeyFile [OpenSSLRSAKeyDir stringByAppendingPathComponent:@"server.privateKey.pem"]

@implementation BBRSACryptor

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        // mkdir for key dir
        NSFileManager *fm = [NSFileManager defaultManager];
        if (![fm fileExistsAtPath:OpenSSLRSAKeyDir])
        {
            [fm createDirectoryAtPath:OpenSSLRSAKeyDir withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
    return self;
}

-(void)dealloc
{
    RSA_free(_rsaPrivate);
    RSA_free(_rsaPublic);
    RSA_free(_serverRsaPublic);
    RSA_free(_serverRsaPrivate);
}

- (BOOL)generateClientRSAKeyPairWithKeySize:(int)keySize
{
    if (NULL != _rsa)
    {
        RSA_free(_rsa);
        _rsa = NULL;
    }
    _rsa = RSA_generate_key(keySize,RSA_F4,NULL,NULL);
    assert(_rsa != NULL);
    
    const char *publicKeyFileName = [OpenSSLClientRSAPublicKeyFile cStringUsingEncoding:NSASCIIStringEncoding];
    const char *privateKeyFileName = [OpenSSLClientRSAPrivateKeyFile cStringUsingEncoding:NSASCIIStringEncoding];

    //写入私钥和公钥
    RSA_blinding_on(_rsa, NULL);
    
//    BIO *priBio = BIO_new_file(privateKeyFileName, "w");
//    PEM_write_bio_RSAPrivateKey(priBio, _rsa, NULL, NULL, 0, NULL, NULL);
//    PEM_write_bio_PKCS8PrivateKey(priBio, NULL, NULL, NULL, 0, NULL, NULL);
//    PEM_write_bio_PKCS8PrivateKey(priBio,_rsa,NULL,NULL,0,NULL,NULL);
    
    BIO *pubBio = BIO_new_file(publicKeyFileName, "w");
    PEM_write_bio_RSAPublicKey(pubBio, _rsa);
    

    FILE *priWtire =NULL;
    priWtire =fopen(privateKeyFileName,"wb");
    EVP_PKEY *pkey =NULL;
    pkey =EVP_PKEY_new();
    EVP_PKEY_assign_RSA(pkey, _rsa);
    PEM_write_PKCS8PrivateKey(priWtire, pkey,NULL,NULL,0,0,NULL);
    fclose(priWtire);
    

    
    
//    BIO_free_all(priBio);
    BIO_free_all(pubBio);

    //分别获取公钥和私钥
    _rsaPrivate = RSAPrivateKey_dup(_rsa);
    assert(_rsaPrivate != NULL);
    
    _rsaPublic = RSAPublicKey_dup(_rsa);
    assert(_rsaPublic != NULL);
    
    if (_rsa && _rsaPublic && _rsaPrivate)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

/**
 *   导入服务器公钥
 */
-(BOOL)importServerRSAPublicKeyBase64:(NSString*)publicKey
{
    //格式化公钥
    @autoreleasepool {
        NSMutableString *result = [NSMutableString string];
        [result appendString:@"-----BEGIN PUBLIC KEY-----\n"];
        int count = 0;
        for (int i = 0; i < [publicKey length]; ++i) {
            
            unichar c = [publicKey characterAtIndex:i];
            if (c == '\n' || c == '\r') {
                continue;
            }
            [result appendFormat:@"%c", c];
            if (++count == 64) {
                [result appendString:@"\n"];
                count = 0;
            }
        }
        [result appendString:@"\n-----END PUBLIC KEY-----"];
        [result writeToFile:OpenSSLServerRSAPublicKeyFile
                 atomically:YES
                   encoding:NSASCIIStringEncoding
                      error:NULL];
        //    DLog(@" ")
        //    DLog(@"%@", result);
    }

    
    FILE *publicKeyFile;
    const char *publicKeyFileName = [OpenSSLServerRSAPublicKeyFile cStringUsingEncoding:NSASCIIStringEncoding];
    publicKeyFile = fopen(publicKeyFileName,"rb");
    if (NULL != publicKeyFile)
    {
        BIO *bpubkey = NULL;
        bpubkey = BIO_new(BIO_s_file());
        BIO_read_filename(bpubkey, publicKeyFileName);
        
        _serverRsaPublic = PEM_read_bio_RSA_PUBKEY(bpubkey, NULL, NULL, NULL);
        assert(_serverRsaPublic != NULL);
        BIO_free_all(bpubkey);
    }
    fclose(publicKeyFile);
    
    return YES;
}

- (BOOL)importServerRSAPrivateKeyBase64:(NSString *)privateKey
{
    //格式化私钥
    const char *pstr = [privateKey UTF8String];
    int len = (int)[privateKey length];
    @autoreleasepool {
        NSMutableString *result = [NSMutableString string];
        [result appendString:@"-----BEGIN RSA PRIVATE KEY-----\n"];
        int index = 0;
        int count = 0;
        while (index < len) {
            char ch = pstr[index];
            if (ch == '\r' || ch == '\n') {
                ++index;
                continue;
            }
            [result appendFormat:@"%c", ch];
            if (++count == 64)
            {
                [result appendString:@"\n"];
                count = 0;
            }
            index++;
        }
        [result appendString:@"\n-----END RSA PRIVATE KEY-----"];
        [result writeToFile:OpenSSLServerRSAPrivateKeyFile
                 atomically:YES
                   encoding:NSASCIIStringEncoding
                      error:NULL];
    }

    
    FILE *privateKeyFile;
    const char *privateKeyFileName = [OpenSSLServerRSAPrivateKeyFile cStringUsingEncoding:NSASCIIStringEncoding];
    privateKeyFile = fopen(privateKeyFileName,"rb");
    if (NULL != privateKeyFile)
    {
        BIO *bpubkey = NULL;
        bpubkey = BIO_new(BIO_s_file());
        BIO_read_filename(bpubkey, privateKeyFileName);
        
        _serverRsaPrivate = PEM_read_bio_RSAPrivateKey(bpubkey, NULL, NULL, NULL);
        assert(_serverRsaPrivate != NULL);
        BIO_free_all(bpubkey);
    }
    fclose(privateKeyFile);
//    free((void*)privateKeyFileName);
    
    return YES;
}

- (BOOL)importClientRSAPublicKeyBase64:(NSString *)publicKey
{
    //格式化公钥
    @autoreleasepool {
        NSMutableString *result = [NSMutableString string];
        [result appendString:@"-----BEGIN PUBLIC KEY-----\n"];
        int count = 0;
        for (int i = 0; i < [publicKey length]; ++i) {
            
            unichar c = [publicKey characterAtIndex:i];
            if (c == '\n' || c == '\r') {
                continue;
            }
            [result appendFormat:@"%c", c];
            if (++count == 64) {
                [result appendString:@"\n"];
                count = 0;
            }
        }
        [result appendString:@"\n-----END PUBLIC KEY-----"];
        [result writeToFile:OpenSSLClientRSAPublicKeyFile
                 atomically:YES
                   encoding:NSASCIIStringEncoding
                      error:NULL];
    }

    
    FILE *publicKeyFile;
    const char *publicKeyFileName = [OpenSSLClientRSAPublicKeyFile cStringUsingEncoding:NSASCIIStringEncoding];
    publicKeyFile = fopen(publicKeyFileName,"rb");
    if (NULL != publicKeyFile)
    {
        BIO *bpubkey = NULL;
        bpubkey = BIO_new(BIO_s_file());
        BIO_read_filename(bpubkey, publicKeyFileName);
        
        _rsaPublic = PEM_read_bio_RSA_PUBKEY(bpubkey, NULL, NULL, NULL);
        assert(_rsaPublic != NULL);
        BIO_free_all(bpubkey);
//        BIO_free(bpubkey);
    }
    fclose(publicKeyFile);
//    free((void*)publicKeyFileName);
    
    return YES;
}

- (BOOL)importClientRSAPrivateKeyBase64:(NSString *)privateKey
{
    //格式化私钥
    const char *pstr = [privateKey UTF8String];
    int len = (int)[privateKey length];
    @autoreleasepool {
        NSMutableString *result = [NSMutableString string];
        [result appendString:@"-----BEGIN RSA PRIVATE KEY-----\n"];
        int index = 0;
        int count = 0;
        while (index < len) {
            char ch = pstr[index];
            if (ch == '\r' || ch == '\n') {
                ++index;
                continue;
            }
            [result appendFormat:@"%c", ch];
            if (++count == 64)
            {
                [result appendString:@"\n"];
                count = 0;
            }
            index++;
        }
        [result appendString:@"\n-----END RSA PRIVATE KEY-----"];
        [result writeToFile:OpenSSLClientRSAPrivateKeyFile
                 atomically:YES
                   encoding:NSASCIIStringEncoding
                      error:NULL];
        
        //        DLog(@"%@", result);
    }

    
    FILE *privateKeyFile;
    const char *privateKeyFileName = [OpenSSLClientRSAPrivateKeyFile UTF8String];
//    const char *privateKeyFileName = [OpenSSLRSAPrivateKeyFile cStringUsingEncoding:NSASCIIStringEncoding];
    privateKeyFile = fopen(privateKeyFileName,"rb");
    if (NULL != privateKeyFile)
    {
        BIO *bpubkey = NULL;
        bpubkey = BIO_new(BIO_s_file());
        BIO_read_filename(bpubkey, privateKeyFileName);
        
        _rsaPrivate = PEM_read_bio_RSAPrivateKey(bpubkey, NULL, NULL, NULL);
        assert(_rsaPrivate != NULL);
        BIO_free_all(bpubkey);
//        BIO_free(bpubkey);
    }
    fclose(privateKeyFile);
//    free((void*)privateKeyFileName);
    return YES;
}

-(NSString*)base64EncodeServerPublicKey
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:OpenSSLServerRSAPublicKeyFile])
    {
        NSString *str = [NSString stringWithContentsOfFile:OpenSSLServerRSAPublicKeyFile
                                                  encoding:NSUTF8StringEncoding error:nil];
        NSString *string = [[str componentsSeparatedByString:@"-----"] objectAtIndex:2];
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        return string;
    }
    return nil;
}

- (NSString*)base64EncodeServerPrivateKey;
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:OpenSSLServerRSAPrivateKeyFile])
    {
        NSString *str = [NSString stringWithContentsOfFile:OpenSSLServerRSAPrivateKeyFile
                                                  encoding:NSUTF8StringEncoding error:nil];
        NSString *string = [[str componentsSeparatedByString:@"-----"] objectAtIndex:2];
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        return string;
    }
    return nil;
}

- (NSString *)base64EncodedPublicKey
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:OpenSSLClientRSAPublicKeyFile])
    {
        NSString *str = [NSString stringWithContentsOfFile:OpenSSLClientRSAPublicKeyFile
                                                  encoding:NSUTF8StringEncoding error:nil];
//        DLog(@"客户端公钥 = %@", str);
        NSString *string = [[str componentsSeparatedByString:@"-----"] objectAtIndex:2];
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        
//        NSLog(@"客户端公钥 = %@", string);
        return string;
    }
    return nil;
}

- (NSString *)base64EncodedPrivateKey
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:OpenSSLClientRSAPrivateKeyFile])
    {
        @autoreleasepool {
            NSString *str = [NSString stringWithContentsOfFile:OpenSSLClientRSAPrivateKeyFile
                                                      encoding:NSUTF8StringEncoding error:nil];
            //        DLog(@"客户端私钥 = %@", str);
            NSString *string = [[str componentsSeparatedByString:@"-----"] objectAtIndex:2];
            string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@"\r" withString:@""];
            
            //        NSLog(@"客户端私钥 = %@", string);
            
            return string;
        }

    }
    return nil;
}

#pragma mark - 用服务器端公钥加密

/**
 *   用服务器端公钥加密
 */
-(NSData*)encryptWithServerPublicKeyUsingPadding:(RSA_PADDING_TYPE)padding plainData:(NSData*)plainData
{
//    NSLog(@"plainData = %@", [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding]);
    
    NSAssert(_serverRsaPublic != NULL, @"You should import server public key first");
    
    if ([plainData length])
    {
        int len = (int)[plainData length];
        unsigned char *plainBuffer = (unsigned char *)[plainData bytes];
        
        //result len
        int clen = RSA_size(_serverRsaPublic);
        unsigned char *cipherBuffer = calloc(clen, sizeof(unsigned char));
        
        RSA_public_encrypt(len,plainBuffer,cipherBuffer, _serverRsaPublic,  padding);
        
        NSData *cipherData = [[NSData alloc] initWithBytes:cipherBuffer length:clen];
        
        free(cipherBuffer);
        
        plainBuffer = NULL;
        
        return cipherData;
    }
    
    return nil;
}

-(NSData*)encryptWithServerPrivateKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                        plainData:(NSData*)plainData;
{
    return nil;
}


/**
 *   用客户端公钥加密
 */
- (NSData *)encryptWithPublicKeyUsingPadding:(RSA_PADDING_TYPE)padding plainData:(NSData *)plainData
{
    
    NSAssert(_rsaPublic != NULL, @"You should import public key first");
    
    if ([plainData length])
    {
        int len = (int)[plainData length];
        unsigned char *plainBuffer = (unsigned char *)[plainData bytes];
        
        //result len
        int clen = RSA_size(_rsaPublic);
        unsigned char *cipherBuffer = calloc(clen, sizeof(unsigned char));
        
        RSA_public_encrypt(len,plainBuffer,cipherBuffer, _rsaPublic,  padding);
        
        NSData *cipherData = [[NSData alloc] initWithBytes:cipherBuffer length:clen];
        
        free(cipherBuffer);
        
        plainBuffer = NULL;
        
        return cipherData;
    }
    
    return nil;
}

/**
 *   用客户端私钥加密
 */
- (NSData *)encryptWithPrivateKeyUsingPadding:(RSA_PADDING_TYPE)padding plainData:(NSData *)plainData
{
    NSAssert(_rsaPrivate != NULL, @"You should import private key first");
    
    if ([plainData length])
    {
        int len = (int)[plainData length];
        unsigned char *plainBuffer = (unsigned char *)[plainData bytes];
        
        //result len
        int clen = RSA_size(_rsaPrivate);
        unsigned char *cipherBuffer = calloc(clen, sizeof(unsigned char));
        
        RSA_private_encrypt(len,plainBuffer,cipherBuffer, _rsaPrivate,  padding);
        
        NSData *cipherData = [[NSData alloc] initWithBytes:cipherBuffer length:clen];
        
        free(cipherBuffer);
        
        plainBuffer = NULL;
        
        return cipherData;
    }
    
    return nil;
}

/**
 *   用服务器端私钥解密
 */
- (NSData *)decryptWithServerPrivateKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                         cipherData:(NSData *)cipherData;
{
    NSAssert(_serverRsaPrivate != NULL, @"You should import server private key first");
    
    if ([cipherData length])
    {
        int len = (int)[cipherData length];
        unsigned char *cipherBuffer = (unsigned char *)[cipherData bytes];
        
        //result len
        int mlen = RSA_size(_serverRsaPrivate);
        unsigned char *plainBuffer = calloc(mlen, sizeof(unsigned char));
        
        RSA_private_decrypt(len, cipherBuffer, plainBuffer, _serverRsaPrivate, padding);
        
        NSData *plainData = [[NSData alloc] initWithBytes:plainBuffer length:mlen];
        
        free(plainBuffer);
        
        cipherBuffer = NULL;
        
        return plainData;
    }
    
    return nil;
}

/**
 *   用服务器端公钥解密
 */
- (NSData *)decryptWithServerPublicKeyUsingPadding:(RSA_PADDING_TYPE)padding cipherData:(NSData *)cipherData
{
    NSAssert(_serverRsaPublic != NULL, @"You should import server public key first");
    
    if ([cipherData length])
    {
        int len = (int)[cipherData length];
        unsigned char *cipherBuffer = (unsigned char *)[cipherData bytes];
        
        //result len
        int mlen = RSA_size(_serverRsaPublic);
        unsigned char *plainBuffer = calloc(mlen, sizeof(unsigned char));
        
        RSA_public_decrypt(len, cipherBuffer, plainBuffer, _serverRsaPublic, padding);
        
        NSData *plainData = [[NSData alloc] initWithBytes:plainBuffer length:mlen];
        
        free(plainBuffer);
        
        cipherBuffer = NULL;
        
        return plainData;
    }
    
    return nil;
}

/**
 *   用客户端私钥解密
 */
- (NSData *)decryptWithPrivateKeyUsingPadding:(RSA_PADDING_TYPE)padding cipherData:(NSData *)cipherData
{
    NSAssert(_rsaPrivate != NULL, @"You should import private key first");
    
    if ([cipherData length])
    {
        int len = (int)[cipherData length];
        unsigned char *cipherBuffer = (unsigned char *)[cipherData bytes];
        
        //result len
        int mlen = RSA_size(_rsaPrivate);
        unsigned char *plainBuffer = calloc(mlen, sizeof(unsigned char));
        
        RSA_private_decrypt(len, cipherBuffer, plainBuffer, _rsaPrivate, padding);
        
        NSData *plainData = [[NSData alloc] initWithBytes:plainBuffer length:mlen];
        
        free(plainBuffer);
        
        cipherBuffer = NULL;
        
        return plainData;
    }
    
    return nil;
}


/**
 *   用客户端公钥解密
 */
- (NSData *)decryptWithPublicKeyUsingPadding:(RSA_PADDING_TYPE)padding cipherData:(NSData *)cipherData
{
    NSAssert(_rsaPublic != NULL, @"You should import public key first");
    
    if ([cipherData length])
    {
        int len = (int)[cipherData length];
        unsigned char *cipherBuffer = (unsigned char *)[cipherData bytes];
        
        //result len
        int mlen = RSA_size(_rsaPublic);
        unsigned char *plainBuffer = calloc(mlen, sizeof(unsigned char));
        
        RSA_public_decrypt(len, cipherBuffer, plainBuffer, _rsaPublic, padding);
        
        NSData *plainData = [[NSData alloc] initWithBytes:plainBuffer length:mlen];
        
        free(plainBuffer);
        
        cipherBuffer = NULL;
        
        return plainData;
    }
    
    return nil;
}

@end
