//
//  BusinessCircleModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol BusinessCircleModel <NSObject>
@end

@protocol Comment <NSObject>
@end

@interface Comment :JSONModel

@property (nonatomic, strong) NSString<Optional> *companyName;
@property (nonatomic, strong) NSString<Optional> *price;

@end

@interface BusinessCircleModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *companyName;
@property (nonatomic, strong) NSString<Optional> *carDateTime;
@property (nonatomic, strong) NSString<Optional> *carModel;
@property (nonatomic, strong) NSString<Optional> *startLocation;
@property (nonatomic, strong) NSString<Optional> *roughTravel;
@property (nonatomic, strong) NSString<Optional> *createTime;
@property (nonatomic, strong) NSArray<Comment, Optional> *comments;


@end

@interface BusinessCircleModelList : JSONModel

@property (nonatomic, strong) NSArray<BusinessCircleModel, Optional> *data;

@end
