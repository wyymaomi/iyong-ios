//
//  ProviderDetailTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderDetailTableViewCell.h"

@implementation ProviderDetailTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"ProviderDetailTableViewCell";
    ProviderDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[ProviderDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
//        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}
//- (instancetype) initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    static NSString *CellID = @"ProviderDetailTableViewCell";
//    ProviderDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID];
//    if (!cell) {
//        cell = [[ProviderDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellID];
////        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.backgroundColor = [UIColor whiteColor];
//        
//        [self setupViews];
//        
//    }
//    
//    return cell;
//}

- (void)setViewModel:(ProviderDetailViewModel *)viewModel
{
    if (viewModel == nil) {
        return;
    }
    
    _viewModel = viewModel;
    
    [viewModel initCompanyInfoFrame];
    
    ProviderDetailModel *data = _viewModel.detailModel;
    
//    [self.avatarImageView downloadImage:data.logoUrl placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        //            weakcell.avatarImageView.cornerRadius = weakcell.avatarImageView.width/2;
//        //            weakSelf.avatarIconImage = weakcell.avatarImageView.image;
//    } andFailure:^(NSString *errorDesc) {
//        //            weakSelf.avatarIconImage = AvatarPlaceholderImage;
//    }];
    
//    [self.avatarImageView downloadImageFromAliyunOSS:data.logoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//        
//    }];
    
    [self.avatarImageView yy_setImageWithObjectKey:data.logoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
        
    }];
    
    self.companyLabel.text = data.companyName;
    self.browserNumberLabel.text = data.browseQuantity;
    self.commentNumberLabel.text = data.commentQuantity;
    self.likeNumberLabel.text = data.thumbsQuantity;
    
    NSString *thumbupImageName = [data.thumbsUp boolValue]?@"icon_thumup_highlighted":@"icon_thumbup";
    [self.likeButton setImage:[UIImage imageNamed:thumbupImageName] forState:UIControlStateNormal];

    
    self.companyLabel.frame = viewModel.companyNameFrame;
    
    self.avatarImageView.frame = viewModel.avatarImageFrame;
    
    self.browserNumberImageView.frame = viewModel.browseNumImageFrame;
    
    self.browserNumberLabel.frame = viewModel.browseNumTextFrame;
    
    self.commentNumberImageView.frame = viewModel.commentNumImageFrame;
    
    self.commentNumberLabel.frame = viewModel.commentNumTextFrame;
    
    self.likeButton.frame = viewModel.thumbUpNumImageFrame;
    
    self.likeNumberLabel.frame = viewModel.thumbUpNumTextFrame;
    
    
    self.browserNumberLabel.font = viewModel.browsesFont;
    
    self.commentNumberLabel.font = viewModel.commentsFont;
    
    self.likeNumberLabel.font = viewModel.likesFont;
    
    
//    self.companyDescriptionLabel.frame = _viewModel.bodyFrame;
    
//    self.companyDescriptionLabel.text = [_viewModel.detailModel displayDescriptionText];
    
    //    self.lineView.frame = CGRectMake(12, viewModel.companyDescriptionSectionHeight-SEPARATOR_LINE_HEIGHT, ViewWidth-24, SEPARATOR_LINE_HEIGHT);
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.lineView.frame = CGRectMake(12, self.height-SEPARATOR_LINE_HEIGHT, ViewWidth-24, SEPARATOR_LINE_HEIGHT);
    
//    _companyLabel.frame = CGRectMake(24, 14, ViewWidth-64-24, 20);
//    _avatarImageView.frame = CGRectMake(ViewWidth-24-40, (self.height-40)/2, 40, 40);
//    _browserNumberImageView.frame = CGRectMake(24, _companyLabel.bottom+11, 17, 13);
//    _browserNumberLabel.frame = CGRectMake(_browserNumberImageView.right+5, _companyLabel.bottom+10, 27, 13);
//    _commentNumberImageView.frame = CGRectMake(_browserNumberLabel.right + 5, _companyLabel.bottom+10, 15, 15);
//    _commentNumberLabel.frame = CGRectMake(_commentNumberImageView.right+5, _companyLabel.bottom+10, 27, 13);
////    _likeNumberImageView.frame = CGRectMake(_commentNumberLabel.right+5, _companyLabel.bottom+10, 15, 15);
//    _likeButton.frame = CGRectMake(_commentNumberLabel.right+5, _companyLabel.bottom+10, 15, 15);
//    _likeNumberLabel.frame = CGRectMake(_likeButton.right+5, _companyLabel.bottom+10, 30, 13);
//    UIView *lineView
    
//    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(24, self.height-0.5, ViewWidth-48, 0.5)];
//    line.tag = 1000;
//    line.backgroundColor = UIColorFromRGB(0xEBEBEB);
//    [self.contentView addSubview:line];
}


- (void)setupViews
{
    _companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(24, 14, ViewWidth-64-24, 20)];
    _companyLabel.textColor = UIColorFromRGB(0x333333);
    _companyLabel.font = FONT(17);
    _companyLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_companyLabel];
    
    _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ViewWidth-24-40, 0, 40, 40)];
    _avatarImageView.image = AvatarPlaceholderImage;
    _avatarImageView.cornerRadius = _avatarImageView.width/2;
    [self.contentView addSubview:_avatarImageView];
    
    _browserNumberImageView = [[UIImageView alloc] init];
    _browserNumberImageView.image = [UIImage imageNamed:@"icon_eye"];
    [self.contentView addSubview:_browserNumberImageView];
    
    _browserNumberLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _browserNumberLabel.textColor = UIColorFromRGB(0x9a9a9a);
    _browserNumberLabel.font = FONT(13);
    _browserNumberLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_browserNumberLabel];
    
    _commentNumberImageView = [[UIImageView alloc] init];
    _commentNumberImageView.image = [UIImage imageNamed:@"icon_comment"];
    [self.contentView addSubview:_commentNumberImageView];
    
    _commentNumberLabel = [UILabel new];
    _commentNumberLabel.textColor = UIColorFromRGB(0x9a9a9a);
    _commentNumberLabel.font = FONT(13);
    [self.contentView addSubview:_commentNumberLabel];
    
//    _likeNumberImageView = [UIImageView new];
//    _likeNumberImageView.image = [UIImage imageNamed:@"icon_thumbup"];
//    [self.contentView addSubview:_likeNumberImageView];
    _likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_likeButton setImage:[UIImage imageNamed:@"icon_thumbup"] forState:UIControlStateNormal];
    [self.contentView addSubview:_likeButton];
    
    _likeNumberLabel = [UILabel new];
    _likeNumberLabel.textColor = UIColorFromRGB(0x9a9a9a);
    _likeNumberLabel.font = FONT(13);
    _likeNumberLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_likeNumberLabel];
    

    _lineView = [UIView new];
    _lineView.backgroundColor = UIColorFromRGB(0xEBEBEB);
    [self.contentView addSubview:_lineView];
    
}

//- (UILabel*)companyLabel
//{
//    if (!_companyLabel) {
//        _companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, ViewWidth-24, 30)];
//        _companyLabel.font = FONT(13);
//        _companyLabel.textColor = [UIColor blackColor];
//        _companyLabel.backgroundColor = [UIColor clearColor];
//    }
//    return _companyLabel;
//}



+(CGFloat)getCellHeight
{
    return 70;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
