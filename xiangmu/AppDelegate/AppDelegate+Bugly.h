//
//  AppDelegate+Bugly.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Bugly)<BuglyDelegate>

- (void)setupBugly;

@end
