//
//  WeiOrderCollectionViewCell.m
//  xiangmu
//
//  Created by David kim on 16/4/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "WeiOrderCollectionViewCell.h"

@implementation WeiOrderCollectionViewCell
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        NSArray *imageArray=@[@"时间",@"机场",@"下车地点"];
        NSArray *nameArray=@[@"用车日期:",@"出发地点:",@"到达地点:"];
        for (int i=0; i<3; i++)
        {
            UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(20, 5+25*i, 20, 20)];
            imageView.image=[UIImage imageNamed:imageArray[i]];
            [self addSubview:imageView];
            
            UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(45, 5+25*i, 120, 20)];
            titleLabel.font=[UIFont systemFontOfSize:14];
            titleLabel.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
            titleLabel.text=nameArray[i];
            [self addSubview:titleLabel];
        }
        
        self.dateLabel.frame=CGRectMake(170, 30, 120, 20);
        [self addSubview:self.dateLabel];
        
        self.jichangLabel.frame=CGRectMake(170, 55, 120, 20);
        [self addSubview:self.jichangLabel];
        
        self.daodaLabel.frame=CGRectMake(170, 80, 120, 20);
        [self addSubview:self.daodaLabel];
        
        UIImageView *rightimageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-60, 20, 20, 30)];
        rightimageView.image=[UIImage imageNamed:@"蓝色的箭头"];
        [self addSubview:rightimageView];
    }
    return self;
}

@end
