//
//  InquireView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "InquireView.h"

@implementation InquireView

- (id)init {
    
    NSArray* array;
    
    //    UINib *nib = [UINib nibWithNibName:@"MyHeader" bundle:nil];
    //    array = [nib instantiateWithOwner:nil
    //                              options:nil];
    
    array = [[NSBundle mainBundle] loadNibNamed:@"InquireView"
                                          owner:nil
                                        options:nil];
    
    self = (InquireView*)[array lastObject];
    
    if(self) {
        /**
         * 需要修改 AutoresizingMask, 不然可能因為大小的關係跑掉.
         */
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
