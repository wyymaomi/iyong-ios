//
//  NSString+Encrypt.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NSString+Encrypt.h"
#import "GTMBase64.h"

#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>


@implementation NSString (Encrypt)

#pragma mark - 散列函数
- (NSString *) sha1
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (unsigned int)data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i=0; i<CC_SHA1_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

#pragma mark - 32位MD5加密方式
-(NSString *) md5
{
    const char *cStr = [self UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest );
    
    return [self stringFromBytes:digest length:CC_MD5_DIGEST_LENGTH];
}

#pragma mark - DES加密
-(NSString *) desEncrypt:(NSString *)key
{
    if (self.length == 0) {
        return @"";
    }
    
    if (key == nil || key.length == 0) {
        return @"";
    }
    
    NSString *ciphertext = nil;
    
    NSData *textData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSInteger dataLength = [textData length];
    
//    unsigned char buffer[1024];
//    memset(buffer, 0, sizeof(char));
    
    size_t  dataInLength = [textData length];
    uint8_t *dataOut = NULL;
//    size_t dataOutMoved = 0;
    size_t    dataOutAvailable = (dataInLength + kCCBlockSizeDES) & ~(kCCBlockSizeDES - 1);
    dataOut = malloc( dataOutAvailable * sizeof(uint8_t));
    memset((void *)dataOut, 0x0, dataOutAvailable);//将已开辟内存空间buffer的首 1 个字节的值设为值 0
    
    
    Byte iv[] = {1,2,3,4,5,6,7,8};
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,
                                          kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding|kCCOptionECBMode,
                                          [key UTF8String],
                                          kCCKeySizeDES,
                                          iv,
                                          [textData bytes],
                                          dataLength,
//                                          buffer,
                                          (void *)dataOut,
                                          /*1024,*/dataOutAvailable,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        NSData *data = [NSData dataWithBytes:(const void *)dataOut length:(NSInteger)numBytesEncrypted];
        ciphertext = [[NSString alloc] initWithData:[GTMBase64 encodeData:data] encoding:NSUTF8StringEncoding];
    }
    free(dataOut);
    return ciphertext;
}

#pragma mark - DES解密
-(NSString *) decryptUseDES:(NSString*)key
{
    if (self.length == 0) {
        return @"";
    }
    
    if (key == nil || key.length == 0) {
        return @"";
    }
    
    NSData* cipherData = [GTMBase64 decodeString:self];
    unsigned char buffer[cipherData.length];
    memset(buffer, 0, sizeof(char));
    size_t numBytesDecrypted = 0;
    Byte iv[] = {1,2,3,4,5,6,7,8};
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt,
                                          kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding|kCCOptionECBMode,
                                          [key UTF8String],
                                          kCCKeySizeDES,
                                          iv,
                                          [cipherData bytes],
                                          [cipherData length],
                                          buffer,
                                          cipherData.length,
                                          &numBytesDecrypted);
    NSString* plainText = nil;
    if (cryptStatus == kCCSuccess) {
        NSData* data = [NSData dataWithBytes:buffer length:(NSUInteger)numBytesDecrypted];
        plainText = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    return plainText;
}

#pragma mark - 助手方法
/**
 *  返回二进制 Bytes 流的字符串表示形式
 *
 *  @param bytes  二进制 Bytes 数组
 *  @param length 数组长度
 *
 *  @return 字符串表示形式
 */
- (NSString *)stringFromBytes:(unsigned char *)bytes length:(int)length {
    NSMutableString *strM = [NSMutableString string];
    
    for (int i = 0; i < length; i++) {
        [strM appendFormat:@"%02x", bytes[i]];
    }
    
    return [strM copy];
}



@end
