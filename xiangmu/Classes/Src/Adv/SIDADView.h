//
//  SIDADView.h
//  SIDAdView
//
//  Created by XU JUNJIE on 13/7/15.
//  Copyright (c) 2015 ISNC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AdvDelegate <NSObject>

- (void)onClickAdv:(NSInteger)index;
- (void)onDismiss;

@end

@class AdvModel;

@interface SIDADView : UIView

@property (nonatomic, weak) id<AdvDelegate> delegate;

- (void)closeBtnClickEventHandle;

- (void)dismiss;

-(void)showAdv:(NSArray<AdvModel*>*)advList;

-(void)showFirstMaskView;

//- (void)show:(NSArray*)images borderColor:(UIColor*)color;

- (void)showWithFaceInfo: (NSDictionary *)info advertisementImage: (UIImage *)image borderColor: (UIColor *)color;

-(void)show;

//不带NavigationBar呈现方法：
- (void)showInView:(UIView *)view withFaceInfo: (NSDictionary *)info advertisementImage: (UIImage *)image borderColor: (UIColor *)color;

@end
