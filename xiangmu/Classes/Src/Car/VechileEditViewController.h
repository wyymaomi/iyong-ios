//
//  VechileEditViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "CarModel.h"
#import "VechileHttpMessage.h"

@class VechileEditViewModel;

typedef NS_ENUM(NSUInteger, VechileEditRow) {
    VechileEditRowAvartar,
    VechileEditRowModel,
    VechileEditRowNumber,
    VechileEditRowName,
    VechileEditRowAge,
    VechileEditRowSex,
    VechileEditRowMobile
};

typedef NS_ENUM(NSUInteger, VechileEditMode) {
    VechileEditModeAdd = 0,
    VechileEditModeEdit
};

@interface VechileEditViewController : BaseViewController

@property (nonatomic, assign) VechileEditMode editType; // 0: 添加车辆 1:编辑车辆
//@property (nonatomic, strong) CarModel *vechileModel;

@property (nonatomic, strong) VechileEditViewModel *vechileViewModel;
@property (nonatomic, strong) NSData *imageData;//


@end
