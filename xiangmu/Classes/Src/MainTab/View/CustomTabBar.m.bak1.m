//
//  CustomTabBar.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CustomTabBar.h"


#define LBMagin 10

@interface CustomTabBar ()



@end

@implementation CustomTabBar

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        
        //        self.backgroundColor = [UIColor whiteColor];
        [self setShadowImage:[UIImage imageWithColor:[UIColor clearColor]]];
        
        UIImageView *halfCircle = [[UIImageView alloc] init];
        halfCircle.image = [UIImage imageNamed:@"icon_semi_circle"];
        [self addSubview:halfCircle];
        self.semiCircleImageView = halfCircle;
        
        UIButton *plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [plusBtn setBackgroundImage:[UIImage imageNamed:@"icon_tab_community"] forState:UIControlStateNormal];
        [plusBtn setBackgroundImage:[UIImage imageNamed:@"icon_tab_community_sel"] forState:UIControlStateHighlighted];
        //        [plusBtn setBackgroundImage:[UIImage imageNamed:@"icon_tab_community_sel"] forState:UIControlStateSelected];
        self.centerButton = plusBtn;
        [plusBtn addTarget:self action:@selector(plusBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:plusBtn];
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"社区";
        label.font = [UIFont fontWithName:@"Helvetica" size:12.0f];
        [label sizeToFit];
        label.textColor = [UIColor whiteColor];
        [self addSubview:label];
        self.titleLabel = label;
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    
    self.semiCircleImageView.centerX = self.centerX;
    self.semiCircleImageView.centerY = self.height * 0.5 - 3 * LBMagin ;//self.height * 0.5 - 2 * LBMagin;
    DLog(@"self.semiCircleImageView.centerY = %f", self.semiCircleImageView.centerY);
    self.semiCircleImageView.size = CGSizeMake(self.semiCircleImageView.image.size.width, self.semiCircleImageView.image.size.height);
    
    self.centerButton.centerX = self.centerX;
    self.centerButton.centerY = self.height * 0.5 - 1.5 * LBMagin ;
    self.centerButton.size = CGSizeMake(self.centerButton.currentBackgroundImage.size.width/3, self.centerButton.currentBackgroundImage.size.height/3);
    DLog(@"self.plusBtn.centerY = %f", self.centerButton.centerY);
    
    self.titleLabel.centerX = self.centerButton.centerX;
    self.titleLabel.centerY = CGRectGetMaxY(self.centerButton.frame) + LBMagin ;
    
    
    
    int btnIndex = 0;
    Class class = NSClassFromString(@"UITabBarButton");
    
//    for (UIView *btn in self.subviews) {
//        
//        if ([btn isKindOfClass:class]) {
//            
//            btn.width = self.width / 5;
//            
//            btn.left = btn.width * btnIndex;
//            
//            btnIndex++;
//            
//            if (btnIndex == 2) {
//                btnIndex++;
//            }
//            
//        }
//    }
    
    [self bringSubviewToFront:self.centerButton];
}


- (void)plusBtnDidClick
{
    //    self.titleLabel.textColor = [UIColor yellowColor];
    //    self.centerButton.selected = YES;
    
    if ([self.delegate respondsToSelector:@selector(tabBarCenterButtonClick:)]) {
        [self.delegate tabBarCenterButtonClick:self];
    }
    
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    if (self.isHidden == NO) {
        
        CGPoint newP = [self convertPoint:point toView:self.centerButton];
        
        if ( [self.centerButton pointInside:newP withEvent:event]) {
            
            return self.centerButton;
            
        }else{
            
            return [super hitTest:point withEvent:event];
        }
    }
    
    else {
        
        return [super hitTest:point withEvent:event];
        
    }
    
}

@end
