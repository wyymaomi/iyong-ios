//
//  SalesDispatchTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SalesDispatchTableViewCell.h"

@implementation SalesDispatchTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath
{
    static NSString *ID = @"SalesDispatchTableViewCellID";
    SalesDispatchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;

        if (indexPath.section == 0) {
            cell.titleLabel.text = @"外派";
        }
        else {
            cell.titleLabel.text = @"自派";
        }
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)drawRect:(CGRect)rect {
//    
//    CGContextRefcontext =UIGraphicsGetCurrentContext();
//    
//    CGContextSetFillColorWithColor(context, [UIColorclearColor].CGColor);
//    
//    CGContextFillRect(context, rect);
//    
//    //上分割线，
//    
//    //CGContextSetStrokeColorWithColor(context, COLORWHITE.CGColor);
//    
//    //CGContextStrokeRect(context, CGRectMake(5, -1, rect.size.width - 10, 1));
//    
//    //下分割线
//    
//    CGContextSetStrokeColorWithColor(context,COLORSEPLINE.CGColor);
//    
//    CGContextStrokeRect(context,CGRectMake(0, rect.size.height-0.5, rect.size.width,1));
//    
//}


@end
