//
//  InvoiceAmountTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceAmountTableViewCell.h"

@implementation InvoiceAmountTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"InvoiceAmountTableViewCellID";
    InvoiceAmountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.amountTextField.delegate = self;
    }
    return cell;
}

+(CGFloat)getCellHeight
{
    return 52;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onMoneyValueChanged:(id)sender {
    
    if (self.amountDelegate && [self.amountDelegate respondsToSelector:@selector(onInvoiceAmountChanged:tag:)]) {
        [self.amountDelegate onInvoiceAmountChanged:[self.amountTextField.text floatValue] tag:self.tag];
    }
}


- (IBAction)onTimesTextFieldDidChanged:(id)sender {
    NSInteger times = [self.timesTextField.text integerValue];
//    self.textField.text = [NSString stringWithFormat:@"%ld", times];
    if (times <= 0)
    {
        times  = 1;
        self.timesTextField.text = [NSString stringWithFormat:@"%ld", (long)times];
//        self.textField.text = [NSString stringWithFormat:@"%ld", (long)times];
    }
    if (self.amountDelegate && [self.amountDelegate respondsToSelector:@selector(onTimesValueChanged:tag:)]) {
        [self.amountDelegate onTimesValueChanged:times tag:self.tag];
    }
}

- (IBAction)onPlusBtnClick:(id)sender {
    NSInteger times = [self.timesTextField.text integerValue];
    times++;
    self.timesTextField.text = [NSString stringWithFormat:@"%ld", (long)times];
    if (self.amountDelegate && [self.amountDelegate respondsToSelector:@selector(onTimesValueChanged:tag:)]) {
        [self.amountDelegate onTimesValueChanged:times tag:self.tag];
    }
}

- (IBAction)onReductionBtnClick:(id)sender {
    NSInteger times = [self.timesTextField.text integerValue];
    times--;
    // 如果为第一行
    if (self.tag == 0) {
        if (times == 0) {
            times = 1;
        }
    }
    
    self.timesTextField.text = [NSString stringWithFormat:@"%ld", times];

    
    if (self.amountDelegate && [self.amountDelegate respondsToSelector:@selector(onTimesValueChanged:tag:)]) {
        [self.amountDelegate onTimesValueChanged:times tag:self.tag];
    }
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //输入字符限制
    if ((range.location == 0) && ([string isEqualToString:@"0"]))
    {
        return NO;
    }
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:myDotNumbers]invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if (filtered.length == 0) {
        //支持删除键
        return [string isEqualToString:@""];
    }
    if (textField.text.length == 0) {
        return ![string isEqualToString:@"."];
    }
    //第一位为0，只能输入.
    else if (textField.text.length == 1){
        if ([textField.text isEqualToString:@"0"]) {
            return [string isEqualToString:@"."];
            //            return NO;
        }
    }
    else{//只能输入一个.
        if ([textField.text rangeOfString:@"."].length) {
            if ([string isEqualToString:@"."]) {
                return NO;
            }
            //两位小数
            NSArray *ary =  [textField.text componentsSeparatedByString:@"."];
            if (ary.count == 2) {
                if ([ary[1] length] == 2) {
                    return NO;
                }
            }
        }
    }
    
    return YES;
}

@end
