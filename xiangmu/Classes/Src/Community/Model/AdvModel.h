//
//  AdvModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol AdvModel <NSObject>

@end

@interface AdvModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *imageUrl;
@property (nonatomic, strong) NSString<Optional> *linkUrl;
@property (nonatomic, strong) NSNumber<Optional> *sort;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSNumber<Optional> *updateTime;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSNumber<Optional> *special;//是否特殊活动(是=true/1,否=false/0)
@property (nonatomic, strong) NSNumber<Optional> *specialType; //特殊活动类型(1=红包)

-(BOOL)isRedpack;

@end

@interface AdvResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<AdvModel,Optional> *data;

@end
