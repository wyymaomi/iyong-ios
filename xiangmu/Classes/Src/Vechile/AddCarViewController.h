//
//  AddCarViewController.h
//  xiangmu
//
//  Created by David kim on 16/3/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "AddCarDetailViewController.h"

@protocol Shuaxin <NSObject>

-(void)gotoShuaxin;

@end

@interface AddCarViewController : LFNavController<Get>
{
    //头像
    UIImageView *avatarImg;
}
@property (nonatomic,weak) id<Shuaxin>delegate;
@end
