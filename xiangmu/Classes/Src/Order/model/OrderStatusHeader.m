//
//  OrderStatusHeader.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderStatusHeader.h"

@implementation OrderStatusHeader

-(id)initWithDict:(NSDictionary*)dict
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

@end
