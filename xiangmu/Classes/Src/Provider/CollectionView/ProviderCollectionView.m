//
//  ProviderCollectionView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderCollectionView.h"
#import "ProviderCollectionViewCell.h"

@implementation ProviderCollectionView

- (NSMutableArray*)dataList
{
    if (!_dataList) {
        _dataList = [NSMutableArray array];
    }
    return _dataList;
}

-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    
    if (self) {
        
        [self initViews];
        
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self initViews];
        
    }
    
    return self;
}

- (void)initViews
{
    
    [self registerNib:[UINib nibWithNibName:@"ProviderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProviderCollectionViewCell"];
    //    [self registerClass:[HomepageCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    self.delegate=self;
    self.dataSource=self;
    self.scrollEnabled=NO;

}

#pragma mark - UICollectionView Delegate method

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(15, 15, 15, 15);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProviderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProviderCollectionViewCell" forIndexPath:indexPath];
    
    OnlineServiceModel *data = self.dataList[indexPath.row];
    
    NSString *imgName = data.imgs[0];
    
    // 下载图片
//    [cell.imageView downloadImageFromAliyunOSS:imgName isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:^(id responseData) {
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//    }];
    
    [cell.imageView yy_setImageWithObjectKey:imgName placeholder:DefaultPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
        
    }];
    
    // 下载头像
//    [cell.avatarImageView downloadImage:data.logoUrl placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//    }];
    
//    [cell.avatarImageView downloadImageFromAliyunOSS:data.logoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:nil andFailure:nil];
    
    [cell.avatarImageView yy_setImageWithObjectKey:data.logoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
        
    }];

    cell.companyLabel.text = data.companyName;
    
    [cell.starView setStarsImages:[data.companyScor floatValue]];
    
    cell.commentNumberLabel.text = [NSString stringWithFormat:@"%@条评论", data.commentQuantity];//data.commentQuantity;
    
    return cell;
    
}

////cell点击变色
//-(BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
///cell点击变色
//-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger row = indexPath.row;
    
    if (_customDelegate && [_customDelegate respondsToSelector:@selector(onSelectItem:)]) {
        [_customDelegate onSelectItem:row];
    }
    
    //    NSDictionary *cellDict = self.subviewCellList[indexPath.row];
    //    NSString *viewController = cellDict[@"vc"];
    //
    //    if ([viewController isEqualToString:@""]) {
    //        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该系统暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    //        [alertView show];
    //    }
    //    else {
    //        if (_customDelegate && [_customDelegate respondsToSelector:@selector(onGotoPage:title:)]) {
    //            [_customDelegate onGotoPage:viewController title:cellDict[@"text"]];
    //        }
    //    }
    
}






@end
