//
//  CompanyCertifcationHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface CompanyCertifcationHttpMessage : BaseHttpMessage

//@property (nonatomic, strong) NSString *fileContent;
@property (nonatomic, strong) NSString *businessLicenceUrl;

@property (nonatomic, strong) NSString *address;  // 企业经营地址
@property (nonatomic, strong) NSString *areaName; // 地区名称
@property (nonatomic, strong) NSString *areaCode; // 城市编码
@property (nonatomic, strong) NSString *name;     // 公司名称

@end
