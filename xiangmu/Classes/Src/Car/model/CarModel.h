//
//  CarResponseModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol CarModel
@end

@interface CarListModel : JSONModel

@property (nonatomic, assign) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<CarModel> *data;

@end

@interface CarModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *driver;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *inspectionDate;
@property (nonatomic, strong) NSNumber<Optional> *insuranceDate;
@property (nonatomic, strong) NSString<Optional> *mobile;
@property (nonatomic, strong) NSString<Optional> *model;
@property (nonatomic, strong) NSString<Optional> *number;
@property (nonatomic, strong) NSNumber<Optional> *updateTime;
@property (nonatomic, strong) NSString<Optional> *userId;
@property (nonatomic, strong) NSString<Optional> *age;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSNumber<Optional> *sex;
@property (nonatomic, strong) NSString<Optional> *imgUrl;
@property (nonatomic, strong) NSData<Optional> *imgData;// 图像数据
@property (nonatomic, strong) NSNumber<Optional> *valid;// 删除标记
@property (nonatomic, strong) NSNumber<Optional> *certificationStatus; // 认证状态

@end
