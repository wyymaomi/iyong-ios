//
//  GeneralAskHeaderTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "GeneralAskHeaderTableViewCell.h"

@implementation GeneralAskHeaderTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"GeneralAskHeaderTableViewCellID";
    GeneralAskHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.rechargeView.borderColor = NAVBAR_BGCOLOR;
        cell.withDrawView.borderColor = NAVBAR_BGCOLOR;
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
