//
//  BindMailboxViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@protocol MailboxDelegate <NSObject>

-(void)onBindMailboxSuccess:(NSString*)emailBox;

@end

@interface BindMailboxViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (nonatomic, strong) NSString *emailbox;

@property (weak, nonatomic) id<MailboxDelegate> delegate;

@end
