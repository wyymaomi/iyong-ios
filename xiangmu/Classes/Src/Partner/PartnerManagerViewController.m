//
//  FirstViewController.m
//  load
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PartnerManagerViewController.h"
#import "PartnerTableViewCell.h"
#import "AddpartnerViewController.h"
//#import "MyPartnerViewController.h"
#import "PartnerWaitPassedViewController.h"
#import "MyPartnerListViewController.h"
#import "AppDelegate.h"

@interface PartnerManagerViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
}
//标题控件
@property(nonatomic,strong)UILabel *titleLabel;

@end

@implementation PartnerManagerViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self doGetWaitPassedPartner];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"合作伙伴";
    [self createTableView];
    // Do any additional setup after loading the view.
}

-(void)createTableView
{
    _tableView=[[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.scrollEnabled=NO;
//    _tableView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    [self.view addSubview:_tableView];
}

-(void)doGetWaitPassedPartner
{
    // 待通过合作伙伴
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Partner_WaitPass_COUNT_API];
    
//    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
//        StrongSelf
        NSInteger code = [responseData[@"code"] integerValue];
        if (code == STATUS_OK) {
            NSInteger dataCount = [responseData[@"data"] integerValue];
            AppDelegate *appDelegate = APP_DELEGATE;
            appDelegate.applyPartnerBadgeNumber = dataCount;
            [_tableView reloadData];
//            strongSelf.applyPartnerBadgeNumber = dataCount;
        }
//        [[NSNotificationCenter defaultCenter] postNotificationName:kOnGetWaitPasswdPartnerNotification object:nil];
    } andFailure:^(NSString *errorDesc) {
        
    }];
}

//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

//行将显示的时候调用
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

#pragma  mark ---UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    PartnerTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[PartnerTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    NSArray *nameArray=@[@"添加合作伙伴",@"我的合作伙伴",@"合作伙伴待通过"];
    if (indexPath.row==0) {
        cell.title.text=nameArray[0];
    }
    if (indexPath.row==1) {
        cell.title.text=nameArray[1];
    }
    if (indexPath.row==2) {
        cell.title.text=nameArray[2];
        AppDelegate *appDelegate = APP_DELEGATE;
        NSString *badgeNumberStr = [NSString stringWithFormat:@"%ld", (long)appDelegate.applyPartnerBadgeNumber];
        [cell showBadgeValue:badgeNumberStr];
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0) {
        AddpartnerViewController *addpartnerVC=[[AddpartnerViewController alloc]init];
        [self.navigationController pushViewController:addpartnerVC animated:YES];
    }
    else if (indexPath.row==1)
    {
        MyPartnerListViewController *vc = [[MyPartnerListViewController alloc] init];
        vc.source_from = SOURCE_FROM_MY_PARTNER;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        PartnerWaitPassedViewController *daitgVC=[[PartnerWaitPassedViewController alloc]init];
        [self.navigationController pushViewController:daitgVC animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
