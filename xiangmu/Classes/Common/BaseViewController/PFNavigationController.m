//
//  PFNavigationController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/9.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PFNavigationController.h"

@interface PFNavigationController ()

@end

@implementation PFNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    
    UIViewController *returnController = [super popViewControllerAnimated:animated];
    
    // update navigationBar style
    if ([self.PFDelegate respondsToSelector:@selector(pfNavigationController:willPopViewControllerAnimated:)]) {
        [self.PFDelegate pfNavigationController:self willPopViewControllerAnimated:animated];
    }
    
    return returnController;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
