//
//  PostContentTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"
#import "UIPlaceHolderTextView.h"

#define TextViewMaxLength 500

@interface PostContentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *textView;

//@property (nonatomic, strong) UIPlaceHolderTextView *textView;

//@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *textView;

//@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *msgLabel;

@property (nonatomic, strong) HeaderView *headerView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

@end
