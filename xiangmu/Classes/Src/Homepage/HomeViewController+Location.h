//
//  HomeViewController+Location.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController (Location)<GYZChooseCityDelegate>

- (void)didSelectCity:(id)sender;

@end
