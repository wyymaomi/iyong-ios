//
//  AgreepayHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface AgreepayHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *dstPrice;

@end
