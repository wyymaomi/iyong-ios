//
//  VechileArrangeTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VechileArrangeResponseModel.h"
#import "HyperlinksButton.h"

@interface VechileArrangeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *vechileImageView;

@property (weak, nonatomic) IBOutlet UIView *vechileInfoView;
@property (weak, nonatomic) IBOutlet UIView *carIdView;
@property (weak, nonatomic) IBOutlet UIView *driverContactView;
@property (weak, nonatomic) IBOutlet UILabel *carNumberLabel;// 车牌号码
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;// 司机
@property (weak, nonatomic) IBOutlet HyperlinksButton *driverMobileButton;

//@property (weak, nonatomic) IBOutlet UILabel *driverMobileLabel;// 司机手机号码
//@property (weak, nonatomic) IBOutlet UILabel *carModel;
@property (weak, nonatomic) IBOutlet UILabel *carNumberLbl;
@property (weak, nonatomic) IBOutlet UIButton *addCarButton;// 添加车辆按钮
@property (weak, nonatomic) IBOutlet UIButton *changeButton;// 变更啊扭
@property (weak, nonatomic) IBOutlet UIView *carSelView;// 车辆选择视图
@property (weak, nonatomic) IBOutlet UIView *carDetailView;
@property (weak, nonatomic) IBOutlet UIButton *smsButton;// 短信
@property (weak, nonatomic) IBOutlet UIButton *wechatButton;// 微信
@property (weak, nonatomic) IBOutlet UIButton *telephoneButton;// 电话
@property (weak, nonatomic) IBOutlet UIButton *resendButton;// 重新发送短信按钮

+ (instancetype)cellWithTableView:(UITableView *)tableView;
-(void)initData:(VechileArrangeResponseModel*)model;
+(CGFloat)getHeight:(VechileArrangeResponseModel*)model;

@end
