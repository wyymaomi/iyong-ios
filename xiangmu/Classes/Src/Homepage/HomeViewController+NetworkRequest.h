//
//  HomeViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController (NetworkRequest)

- (void)fetchBannerAdvList;

- (void)fetchRecommendList;


- (void)fetchServiceProviderByPageIndex:(NSInteger)pageIndex areaCode:(NSString*)areaCode requestType:(NSUInteger)requestType;
//- (void)fetchServiceProviderByPageIndex:(NSInteger)pageIndex areaCode:(NSString*)areaCode;

- (void)fetchExperienceList;

- (void)fetchServiceProviderByType:(NSUInteger)pageIndex areaCode:(NSString*)areaCode businessType:(NSUInteger)businessType;

@end
