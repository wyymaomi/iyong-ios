//
//  CommunityCateogryTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunityCateogryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *redDotImageView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
