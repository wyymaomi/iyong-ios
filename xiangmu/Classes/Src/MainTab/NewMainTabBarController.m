//
//  NewMainTabBarController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/11.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewMainTabBarController.h"
#import "MapViewController.h"
#import "NewCustomTabBar.h"
#import "AppDelegate.h"

@interface NewMainTabBarController ()<UITabBarControllerDelegate>

@end

@implementation NewMainTabBarController

//- (NSArray*)tabConfigArray
//{
//    if (!_tabConfigArray) {
//        _tabConfigArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TabConfig.plist" ofType:nil]];
//    }
//    
//    return _tabConfigArray;
//}

-(id)init
{
    self = [super init];
    
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLogout) name:kOnLogoutNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLoginSuccess) name:kLoginSuccessNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUnreadMessageCount) name:@"setupUnreadMessageCount" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetCompanyCertificationedNotification) name:kOnCompanyCertificationedNotification object:nil];
        
        //    [self createControllers];
        
        [self createControllers];
        
        [self initTabBar];
        
        
        self.delegate = self;
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    DLog(@"");
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLogout) name:kOnLogoutNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLoginSuccess) name:kLoginSuccessNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUnreadMessageCount) name:@"setupUnreadMessageCount" object:nil];
//    
//    //    [self createControllers];
//    
//    [self createControllers];
//    
//    [self initTabBar];
//    
//    
//    self.delegate = self;
    
    // Do any additional setup after loading the view.
    
    //    [self initIntroduceView];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

// 状态栏是否隐藏
- (BOOL)prefersStatusBarHidden
{
    return NO;
    //    return self.isStatusBarHidden;
}

-(void)createControllers
{
    
    for (NSUInteger i = 0; i < self.tabConfigArray.count; i++) {
        NSDictionary *dict = self.tabConfigArray[i];
        Class cls = NSClassFromString(dict[@"viewController"]);
        BaseViewController *viewController = [[cls alloc] init];
//        viewController.title = dict[@"name"];
        viewController.source_from = SOURCE_FROM_TAB_CONTROL;
        viewController.hasLeftBackButton = NO;
//        viewController.image = [[UIImage imageNamed:dict[@"icon"]]]
        viewController.tabBarItem.image = [[UIImage imageNamed:dict[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        viewController.tabBarItem.selectedImage = [[UIImage imageNamed:dict[@"icon_focused"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        viewController.tabBarItem.title = dict[@"name"];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        [self addChildViewController:navigationController];
      
        
//        if (i==1) {
////            viewController.title = viewController.tabBarItem.title;
//        }
        if (i == 2) {
//            viewController.title = viewController.tabBarItem.title;
            self.companySettleNavigation = navigationController;
        }
        else if (i == 3) {
            self.meNavigation = navigationController;
        }
//        else if (i == 2) {
//            self.mapNavigation = navigationController;
//        }
        
        //
#if 0
        if (i == 2) {
            self.companySettleNavigation = navigationController;
        }
        else if (i == 3) {
            self.meNavigation = navigationController;
        }
#endif
        
    }

//    NSArray *ctrlArray=@[@"ServiceProviderRootViewController",
//                         @"InquireViewController",
//                         /*@"CommunityListViewController",@"CommunityCategoryViewController",*/
//                         /*@"BillManageViewController",*/@"BusinessCircleListViewController",
//                         @"MyViewController"];
//    ////////////////////
//    NSArray *nameArray=@[@"首页",@"询价",/*@"发布",*/@"报价",@"我"];
//    NSArray *imageArray=@[@"icon_tab_home",@"icon_tab_enquiry",/*@"icon_tab_plus",*/ @"icon_tab_offer",@"icon_tab_me"];
//    NSArray *selectedArray=@[@"icon_tab_home_focused",@"icon_tab_enquiry_focused",/*@"icon_tab_plus",*/@"icon_tab_offer_focused",@"icon_tab_me_focused"];
    
    //    NSMutableArray *navArray=[[NSMutableArray alloc]init];
//    for (int i=0; i<ctrlArray.count; i++) {
//        
//        Class cls=NSClassFromString(ctrlArray[i]);
//        
//        BaseViewController *vc=[[cls alloc]init];
//        vc.source_from = SOURCE_FROM_TAB_CONTROL;
//        vc.hasLeftBackButton = NO;
//        vc.tabBarItem.image=[[UIImage imageNamed:imageArray[i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        vc.tabBarItem.selectedImage=[[UIImage imageNamed:selectedArray[i]]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        vc.tabBarController.tabBar.barTintColor = NAVBAR_BGCOLOR;
//        vc.tabBarItem.title = nameArray[i];
//        //        [vc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(2, -5)];
//        
//        UINavigationController *navCtrl=[[UINavigationController alloc] initWithRootViewController:vc];
//        
//        //        if (i == 0) {
//        //            _homeNavigation = navCtrl;
//        //        }
//        //        if (i == 1) {
//        //            _orderNavigation = navCtrl;
//        //
//        //        }
//        //        if (i == 2) {
//        //            _communityNavigation = navCtrl;
//        ////            vc.tabBarItem.image = nil;
//        ////            vc.tabBarItem.selectedImage = nil;
//        //        }
//        //        if (i == 3) {
//        //            _billNavigation = navCtrl;
//        //        }
//        //        if (i == 4) {
//        //            _meNavigation = navCtrl;
//        //        }
//        
//        if (i == 3){
//            _meNavigation = navCtrl;
//        }
//        
//        [self addChildViewController:navCtrl];
//    }
}

-(void)initTabBar
{
    
//    NewCustomTabBar *tabbar = [[NewCustomTabBar alloc] init];
    CustomTabBar *tabbar = [[CustomTabBar alloc] init];
    [self setValue:tabbar forKey:@"tabBar"];
    
//    CustomTabBar *tabbar = [[CustomTabBar alloc] init];
//    tabbar.delegate = self;
//    [self setValue:tabbar forKeyPath:@"tabBar"];
    
    //    CustomTabBar *customTabBar = [[CustomTabBar alloc] init];
    //    customTabBar.frame = self.tabBar.bounds;
    //    customTabBar.delegate = self;
    //    self.tabBar addSub
    
    UITabBar *tabBar = [self tabBar];
    self.selectedIndex = 0;
    //    [self tabBarController:self shouldSelectViewController:_communityNavigation];
    
    //    [tabBar setShadowImage:[UIImage imageNamed:@"img_tab_bar"]];
    
    //    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, tabBar.frame.size.height)];
    //    bgView.backgroundColor = [UIColor whiteColor];//NAVBAR_BGCOLOR;
    //    [tabBar insertSubview:bgView atIndex:0];
    
    //设置tabbar的颜色
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x333333), NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica" size:12.0f],NSFontAttributeName,nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x09a892), NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica" size:12.0f],NSFontAttributeName,nil] forState:UIControlStateSelected];
    
    //通过这两个参数来调整badge位置
    [tabBar setTabIconWidth:33.33];
    //    [tabBar setTabIconWidth:self.tabBar.frame.size.width/5-10];
    [tabBar setBadgeTop:9];
    
}

-(void)onGetCompanyCertificationedNotification
{
    self.selectedIndex = 2;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    // 设置社区按钮和文字为默认状态
//    CustomTabBar *tabBar = (CustomTabBar*)[self tabBar];
//    
//    if (viewController == _homeNavigation) {
//        tabBar.centerButton.selected = NO;
//        tabBar.titleLabel.textColor = UIColorFromRGB(0x333333);
//        return YES;
//    }
    
//    if (viewController == self.) {
//        <#statements#>
//    }
    
//    if (viewController == self.mapNavigation) {
//        self.tabIndex = 2;
//        if (![[UserManager sharedInstance] isLogin]) {
//            [self showLoginAlertView];
//            return NO;
//        }
//    }
    
    if (viewController == self.companySettleNavigation) {
        
        self.tabIndex = 0;
        if (![[UserManager sharedInstance] isLogin]) {
            [self showLoginAlertView];
            return NO;
        }
        
//        BOOL isRealnameCertification = [UserManager sharedInstance].userModel.isRealnameCertified;
//        BOOL isCompanyCertification = [UserManager sharedInstance].userModel.isCompanyCertified;
        
//        [self openCompanyModePage];
        
//        if (!isRealnameCertification || !isCompanyCertification) {
//            return NO;
//        }
        
        return [self openCompanyModePage];
        
        
        
        
        
//        return NO;
        
    }
    
    else if (viewController == self.meNavigation) {
        
        self.tabIndex = 0;
        
        AppDelegate *appDelegate = APP_DELEGATE;
        
        if (![[UserManager sharedInstance] isLogin]) {
            [self showLoginAlertView];
            return NO;
        }
        
        if ([appDelegate isLoginOvertime]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
            return YES;
        }
        
    }
    
    
//    AppDelegate *appDelegate = APP_DELEGATE;
//    
//    if (![[UserManager sharedInstance] isLogin]) {
//        [self showLoginAlertView];
//        return NO;
//    }
//    
//    if ([appDelegate isLoginOvertime]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
//        return YES;
//    }
//    
//    
//    if (viewController == _communityNavigation) {
//        return NO;
//    }
//    
//    if (viewController == _homeNavigation || viewController == _orderNavigation || viewController == _orderNavigation || viewController == _meNavigation) {
//        //        tabBar.centerButton.isSelected = NO;
//        tabBar.centerButton.selected = NO;
//        tabBar.titleLabel.textColor = UIColorFromRGB(0x333333);
//    }
    
    return YES;
}

-(NSArray*)viewControllers;
{
    return self.navigationController.viewControllers;
}

-(void)backFromLogin
{
    self.selectedIndex = 0;
}


- (void)onLogout
{
    [self.tabBar setBadgeStyle:kCustomBadgeStyleNone value:0 atIndex:1];
}


-(void)onGestureLoginSuccess;
{
    self.selectedIndex = self.tabIndex;
}

- (void)onLoginSuccess
{
    self.selectedIndex = self.tabIndex;
}



-(BOOL)canLogin
{
    //当应用进入后台时间超过10分钟，重新进入弹出登录界面
    AppDelegate *appDelegate = APP_DELEGATE;
    
    if (![[UserManager sharedInstance] isLogin]) {
        [self showLoginAlertView];
        return NO;
    }
    if ([appDelegate isLoginOvertime]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CustomTabBarDelegate
-(void)tabBarCenterButtonClick:(CustomTabBar*)tabBar;
{
#if 0
    if ([self canLogin]) {
        
        PublishViewController *publishViewController = [[PublishViewController alloc] init];
        [self presentViewController:publishViewController animated:YES completion:nil];
        
    }
#endif
    
    if ([self canLogin]) {
        
        MapViewController *viewController = [[MapViewController alloc] init];
        viewController.enter_type = ENTER_TYPE_PRESENT;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        [self presentViewController:navigationController animated:YES completion:nil];
        
        
        //        PublishViewController *viewController = [[PublishViewController alloc] init];
        //        [self presentViewController:viewController animated:YES completion:nil];
        
        
//        PostExperienceViewController *viewController = [[PostExperienceViewController alloc] init];
//        viewController.type = TableTypeUserExperience;
//        viewController.enter_type = ENTER_TYPE_PRESENT;
//        viewController.title = @"发布体验";
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
//        //        self.navigationController pushViewController:navigation animated:<#(BOOL)#>
//        [self presentViewController:navigationController animated:YES completion:nil];
        
    }
    
    return;
    
    //    if ([self canLogin]) {
    //
    //        CommunityCategoryViewController *viewController = [[CommunityCategoryViewController alloc] init];
    //        viewController.enter_type = ENTER_TYPE_PRESENT;
    //
    ////        CommunityListViewController *viewController = [[CommunityListViewController alloc] init];
    ////        viewController.enter_type = ENTER_TYPE_PRESENT;
    //        UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:viewController];
    //        [self presentViewController:navVc animated:YES completion:nil];
    //
    //    }
    
    self.selectedIndex = 1;
    //    if ([self canLogin]) {
    
    //    [self tabBarController:self shouldSelectViewController:_communityNavigation];
    //        }
    
    
    //    [self presentViewController:_communityNavigation animated:YES completion:nil];
    
    //    LBpostViewController *plusVC = [[LBpostViewController alloc] init];
    //    plusVC.view.backgroundColor = [self randomColor];
    //
    //    LBNavigationController *navVc = [[LBNavigationController alloc] initWithRootViewController:plusVC];
    //
    //    [self presentViewController:navVc animated:YES completion:nil];
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
