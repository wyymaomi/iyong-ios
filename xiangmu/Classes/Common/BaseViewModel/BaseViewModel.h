//
//  BaseViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HUDshowMessageDelegate<NSObject>
@optional
//加载框控制
- (void)showMessage:(NSString *)message WithCode:(NSString *)code;
- (void)hideHUD;

//刷新控件控制
//- (void)addDefaultFooter;
//- (void)HideFooter;
//- (void)endRefresh;
@end

@interface BaseViewModel : NSObject

@end
