//
//  BusinessHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface BusinessHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *business;// 经营范围
@property (nonatomic, strong) NSString *blurb; // 公司简介
@property (nonatomic, strong) NSString *imgs; // 上传图片数组，用逗号连接
//@property (nonatomic, strong) NSString *img1;
//@property (nonatomic, strong) NSString *img2;
//@property (nonatomic, strong) NSString *img3;
//@property (nonatomic, strong) NSString *img4;
//@property (nonatomic, strong) NSString *img_1_content;
//@property (nonatomic, strong) NSString *img_2_content;
//@property (nonatomic, strong) NSString *img_3_content;
//@property (nonatomic, strong) NSString *img_4_content;

@end

@interface ExperienceHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *content;
//@property (nonatomic, strong) NSString *img1;
//@property (nonatomic, strong) NSString *img2;
//@property (nonatomic, strong) NSString *img3;
//@property (nonatomic, strong) NSString *img4;
//@property (nonatomic, strong) NSString *img_1_content;
//@property (nonatomic, strong) NSString *img_2_content;
//@property (nonatomic, strong) NSString *img_3_content;
//@property (nonatomic, strong) NSString *img_4_content;
@property (nonatomic, strong) NSString *areaCode;
@property (nonatomic, strong) NSString *imgs; // 图片数组，用逗号连接

@end
