//
//  AddBankcardTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBankcardTableViewCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
