//
//  RecommendViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, RecommendType)
{
    RecommendTypeServiceProvider = 100,
    RecommendTypeUserExperience = 200
};

@interface RecommendViewController : BaseViewController

@end
