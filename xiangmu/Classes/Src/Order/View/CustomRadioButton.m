//
//  CustomRadioButton.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CustomRadioButton.h"

@implementation CustomRadioButton

//-(id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//
//}


//+ (instancetype)customView
//{
//    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
//}

-(void)awakeFromNib
{
    self.isSelected = NO;
//    [self.clickButton addTarget:self action:@selector(onClickBtn:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setIsSelected:(BOOL)isSelected
{
    if (isSelected){
        self.radioButtonImage.image = [UIImage imageNamed:@"icon_radio_btn_sel"];
        self.textLabel.textColor = [UIColor redColor];
        self.backgroundColor = UIColorFromRGB(0xEFCB32);
    }
    else {
        self.radioButtonImage.image = [UIImage imageNamed:@"icon_radio_btn_unsel"];
        self.textLabel.textColor = [UIColor blackColor];
        self.backgroundColor = UIColorFromRGB(0xF3E28F);
    }
}


//- (IBAction)onClickBtn:(id)sender {
////    self.isSelected = !self.isSelected;
//    if (_isSelected) {
//        _isSelected = NO;
//        self.radioButtonImage.image = [UIImage imageNamed:@"icon_radio_btn_unsel"];
//        self.textLabel.textColor = [UIColor blackColor];
//    }
//    else {
//        _isSelected = YES;
//        self.radioButtonImage.image = [UIImage imageNamed:@"icon_radio_btn_sel"];
//        self.textLabel.textColor = [UIColor redColor];
//    }
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
