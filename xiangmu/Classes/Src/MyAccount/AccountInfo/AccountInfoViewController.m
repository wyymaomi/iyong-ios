//
//  AccountInfoViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AccountInfoViewController.h"
#import "AccountAvartarTableViewCell.h"

@interface AccountInfoViewController ()

@end

@implementation AccountInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view addSubview:self.plainTableView];
    self.plainTableView.width = ViewWidth/2;
    self.plainTableView.scrollEnabled = NO;
    self.plainTableView.delegate = self;
    self.plainTableView.dataSource = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 148;
    }
    return 44;
    
//    EnquireModel *item = self.dataList[indexPath.row - 1];
//    return [MyBusinessCircleTableViewCell getCellHeight:item];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        static NSString *AccountInfoAvatarCellID =  @"AccountInfoAvatarCellID";

        AccountAvartarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AccountInfoAvatarCellID];
        if (!cell) {
            cell = [[AccountAvartarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AccountInfoAvatarCellID];
        }
//        CommunityHeaderTableViewCell *cell = [CommunityHeaderTableViewCell cellWithTableView:tableView];
//        
//        [cell initData];
//        
        return cell;
        
    }
    else {
        
//        static NSString *cellIdentifier = @"MyEnquireTableViewCell";
        
//        MyBusinessCircleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//        
//        if (!cell) {
//            cell = [[MyBusinessCircleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//        }
//        EnquireModel *item = self.dataList[indexPath.row - 1];
//        [cell initData:item];
//        cell.delegate = self;
//        cell.statusButton.tag = indexPath.row - 1;
//        [cell.statusButton addTarget:self action:@selector(completeInquire:) forControlEvents:UIControlEventTouchUpInside];
//        cell.fullTextButton.tag = indexPath.row - 1;
//        [cell.fullTextButton addTarget:self action:@selector(onFullText:) forControlEvents:UIControlEventTouchUpInside];
        
//        return cell;
        
    }
    
    return nil;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}


@end
