//
//  MsgView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MsgView.h"
#import "CustomSegment.h"


@interface MsgView ()

@property (nonatomic, strong) IBOutlet CustomSegment *customSegment;

@end

@implementation MsgView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end


@interface TTMessage ()

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) MsgView *msgView;

@end

@implementation TTMessage


-(UIView*)backgroundView
{
    if (_backgroundView == nil) {
        _backgroundView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backgroundView.backgroundColor = ColorFromRGBWithAlpha(0x0, 0.5);
    }
    return _backgroundView;
}

-(MsgView*)msgView
{
    if (_msgView == nil) {
        _msgView = [MsgView customView];
        CGRect mainRect = [UIScreen mainScreen].bounds;
        NSInteger height = _msgView.frame.size.height + 100;
        _msgView.frame = CGRectMake(10, (mainRect.size.height - height) / 2, mainRect.size.width - 2 * 10, _msgView.frame.size.height  + 100);
        _msgView.customSegment.items = @[@"复制", @"关闭"];
    }
    return _msgView;
}

-(void)showMessageViewWithTitle:(NSString*)title
             orderBaseInfoModel:(OrderBaseInfoResponseModel*)orderBaseInfoModel
            vechileArrangeModel:(VechileArrangeResponseModel*)vechileArrangeModel
              cancelButtonTitle:(NSString*)cancelButtonTitle
               otherButtonTitle:(NSString*)otherButtonTitle
                       onCancel:(BBBasicBlock)canceled
                        onOther:(BBBasicBlock)other;
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self.backgroundView];
    [self.backgroundView addSubview:self.msgView];
    self.msgView.customSegment.delegate = self;
    NSDate *carDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([orderBaseInfoModel.carTime doubleValue] / 1000.0f)];
    self.msgView.carDateTimeLabel.text = [carDate stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.msgView.startLocationLabel.text = orderBaseInfoModel.location.trim;
    self.msgView.carUserNameLabel.text = orderBaseInfoModel.customerName.trim;
    self.msgView.mobileLabel.text = orderBaseInfoModel.customerMobile.trim;
    self.msgView.roughTravelLabel.text = orderBaseInfoModel.itinerary.trim;
    self.msgView.remarkLabel.text = orderBaseInfoModel.remark.trim;
    self.msgView.vechileArrangeLabel.text = [NSString stringWithFormat:@"%@ %@ %@", vechileArrangeModel.number.trim, vechileArrangeModel.driver.trim, vechileArrangeModel.mobile.trim];
}

-(void)showMessageViewWithTitle:(OrderCopyResponseModel*)orderCopyModel
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self.backgroundView];
    [self.backgroundView addSubview:self.msgView];
    self.msgView.customSegment.delegate = self;
    NSDate *carDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([orderCopyModel.carTime doubleValue] / 1000.0f)];
    self.msgView.carDateTimeLabel.text = [carDate stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.msgView.startLocationLabel.text = orderCopyModel.location.trim;
    self.msgView.carUserNameLabel.text = orderCopyModel.customerName.trim;
    self.msgView.mobileLabel.text = orderCopyModel.customerMobile.trim;
    self.msgView.roughTravelLabel.text = orderCopyModel.itinerary.trim;
    self.msgView.remarkLabel.text = orderCopyModel.remark.trim;
    CarInfo *carInfo = orderCopyModel.carInfo;
    self.msgView.vechileArrangeLabel.text = [NSString stringWithFormat:@"%@ %@ %@", carInfo.number.trim, carInfo.driver.trim, carInfo.mobile.trim];
//    self.msgView.vechileArrangeLabel.text = [NSString stringWithFormat:@"%@ %@ %@", vechileArrangeModel.number.trim, vechileArrangeModel.driver.trim, vechileArrangeModel.mobile.trim];
}

- (void)didSelectAtIndex:(NSInteger)index;
{
    if (index == 1) {
//        if (_cancelBlock) {
//            _cancelBlock();
//        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(onClickCancelButton)]) {
            [self.delegate onClickCancelButton];
        }
        [self.backgroundView removeFromSuperview];
    }
    else if (index == 0) {
//        if (_confirmBlock) {
//            _confirmBlock();
//        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(onClickOKButton)]) {
            [self.delegate onClickOKButton];
        }
        [self.backgroundView removeFromSuperview];
    }
}

@end

