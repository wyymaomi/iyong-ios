//
//  SalesCarFilterViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "CarModel.h"

@protocol SalesCarFilterDelegate <NSObject>

-(void)onGetCarData:(BOOL)isOuterDispatch carData:(CarModel*)carData;

@end

@interface SalesCarFilterViewController : BaseViewController

@property (nonatomic, weak) id<SalesCarFilterDelegate> delegate;

@end
