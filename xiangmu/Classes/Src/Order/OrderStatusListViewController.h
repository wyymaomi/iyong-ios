//
//  OrderStatusListViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseTableViewController.h"

typedef NS_ENUM(NSUInteger, OrderStatusRow) {
    OrderStatusRowUntreated = 0,    // 未处理
    OrderStatusRowStroke,           // 行程中
    OrderStatusRowWaitBargin,       // 待结算
    OrderStatusRowUnpaid,           // 未支付
    OrderStatusRowCompleted         // 已完成
};

@interface OrderStatusListViewController : BaseTableViewController

@end
