//
//  DAO.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DAO.h"
#import "DatabaseManager.h"
#import "AppConfig.h"

@implementation DAO

- (id)init{
    self = [super init];
    
    if(self)
    {
        self.databaseQueue = [DatabaseManager currentManager].databaseQueue;
        if (self.databaseQueue) {
            [DAO createTablesNeeded];
        }
    }
    
    return self;
}


- (FMDatabaseQueue *)databaseQueue
{
    if (![[DatabaseManager currentManager] isDatabaseOpened]) {
        [[DatabaseManager currentManager] openDataBase];
        self.databaseQueue = [DatabaseManager currentManager].databaseQueue;
    }
    return _databaseQueue;
}

+ (void)createTablesNeeded
{
//    @autoreleasepool {
        FMDatabaseQueue *databaseQueue = [DatabaseManager currentManager].databaseQueue;
        
        __block BOOL isSuccess = NO;
        [databaseQueue inTransaction:^(FMDatabase *database, BOOL *rollBack){
            
            NSString *dataBaseVersion = [AppConfig currentConfig].databaseVersion;
            
            if (IsStrEmpty(dataBaseVersion) || ![dataBaseVersion isEqualToString:DATABASE_VERSION]) {
                
                NSString *sql1 = [NSString stringWithFormat:@"DROP TABLE info_car"];
                NSString *sql2 = [NSString stringWithFormat:@"DROP TABLE info_partner"];
                NSString *sql3 = [NSString stringWithFormat:@"DROP TABLE search_history"];
                NSString *sql4 = [NSString stringWithFormat:@"DROP TABLE info_adv"];
                NSString *sql5 = [NSString stringWithFormat:@"DROP TABLE red_pack"];
                
                isSuccess = [database executeUpdate:sql1];
                isSuccess = [database executeUpdate:sql2];
                isSuccess = [database executeUpdate:sql3];
                isSuccess = [database executeUpdate:sql4];
                isSuccess = [database executeUpdate:sql5];
                
            }
            
            NSString *sql1 = @"CREATE TABLE IF NOT EXISTS info_car(id TEXT PRIMARY KEY NOT NULL, companyId TEXT NOT NULL, driver TEXT NOT NULL,  model TEXT NOT NULL, number TEXT NOT NULL, imgUrl TEXT, inspectionDate LONG, insuranceDate LONG, mobile TEXT NOT NULL, sex TEXT NOT NULL, age TEXT NOT NULL, updateTime LONG, certificationStatus INTEGER)";
            
            NSString *sql2 = @"CREATE TABLE IF NOT EXISTS info_partner(ID INTEGER PRIMARY KEY AUTOINCREMENT, companyId TEXT NOT NULL, companyName TEXT NOT NULL, createTime LONG NOT NULL, mobile TEXT NOT NULL, nickname TEXT NOT NULL, partnerCompanyId TEXT NOT NULL, partnerId TEXT NOT NULL, logoUrl TEXT, updateTime LONG)";
            
            NSString *sql3 = @"CREATE TABLE IF NOT EXISTS search_history(id TEXT PRIMARY KEY NOT NULL, title TEXT, address TEXT, time LONG, cityName TEXT, latitude FLOAT, longitude FLOAT)";
            
            NSString *sql4 = @"CREATE TABLE IF NOT EXISTS info_adv(id TEXT PRIMARY KEY NOT NULL, imageUrl TEXT, linkUrl TEXT, sort INTEGER, type INTEGER, updateTime LONG)";
            
            NSString *sql5 = @"CREATE TABLE IF NOT EXISTS red_pack(id INTEGER PRIMARY KEY AUTOINCREMENT, createTime TEXT, amount TEXT, mobile TEXT, flag INTEGER)";
            
            isSuccess = [database executeUpdate:sql1];
            isSuccess = [database executeUpdate:sql2];
            isSuccess = [database executeUpdate:sql3];
            isSuccess = [database executeUpdate:sql4];
            isSuccess = [database executeUpdate:sql5];
            
            if (isSuccess) {
                [AppConfig currentConfig].databaseVersion = DATABASE_VERSION;
            }
            
        }];
        
//    } 
}

@end
