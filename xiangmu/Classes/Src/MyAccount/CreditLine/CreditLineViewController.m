//
//  CreditLineViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CreditLineViewController.h"
#import "ApplyForCreditLineViewController.h"

@interface CreditLineViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *curCreditLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UIButton *companyApplyButton;
@property (weak, nonatomic) IBOutlet UIButton *personApplyButton;

@end

@implementation CreditLineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"信用额度";
    
    [self.companyApplyButton blueStyle];
    [self.personApplyButton whiteStyle];
    self.avatarImageView.cornerRadius = self.avatarImageView.width/2;
    
    [self initData];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getAccountInfo];
    [self getFinanceInfo];
}

-(void)getFinanceInfo
{
    self.nextAction = @selector(getFinanceInfo);
    self.object1 = nil;
    self.object2 = nil;
    
//    if (![[UserManager sharedInstance] isLogin]) {
//        return;
//    }
    
    if ([self needLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_FINANCE_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            NSString *creditLine = data[@"creditLine"];
            strongSelf.curCreditLineLabel.text = [NSString stringWithFormat:@"当前额度 ¥%@",creditLine];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        NSLog(@"response failure");
        //        return;
    }];
    
}

-(void)getAccountInfo
{
    self.nextAction = @selector(getAccountInfo);
    self.object1 = nil;
    self.object2 = nil;
    
//    if (![[UserManager sharedInstance] isLogin]) {
//        return;
//    }
    
    if ([self needLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, My_Detail_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        NSLog(@"response success");
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if(code_status == STATUS_OK) {
            
        }
        NSDictionary *userData  = responseData[@"data"];
        [UserManager sharedInstance].userModel = [[UserModel alloc] initWithDictionary:userData error:nil];
//        strongSelf.level = [userData[@"level"] integerValue];
        
//        [_tableView reloadData];
        
        // 如果logoUrl不空则下载图片
        NSString *logoUrl = userData[@"logoUrl"];
//        if (!IsStrEmpty(logoUrl)) {
//            [strongSelf downloadAvartarImage:logoUrl];
//        }
        [strongSelf.avatarImageView yy_setImageWithObjectKey:logoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
            
        } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
            
        }];
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        StrongSelf
        [strongSelf resetAction];
        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"网络连接错误，请重试" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
        //        [alert show];
        //        return;
        
    }];
    
}

-(void)initData
{
    self.curCreditLineLabel.text = @"";
    [self.creditLine integerValue];
    self.mobileLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.username];
    self.companyLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.companyName];
    
}

-(void)downloadAvartarImage:(NSString*)imgUrl
{
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            strongSelf.avatarImageView.image = [UIImage imageWithData:imageData];
            strongSelf.avatarImageView.cornerRadius = self.avatarImageView.frame.size.width/2;
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        strongSelf.avatarImageView.image = [UIImage imageNamed:@"icon_avatar"];
        strongSelf.avatarImageView.cornerRadius = 0;
        
    }];
}

- (IBAction)onClickButton:(id)sender {
    UIButton *button = (UIButton*)sender;
    
    ApplyForCreditLineViewController *viewController = [[ApplyForCreditLineViewController alloc] init];
    if (button == self.companyApplyButton) {
        viewController.type = 0;
        viewController.title = self.companyApplyButton.titleLabel.text;
    }
    else if(button == self.personApplyButton){
        viewController.type = 1;
        viewController.title = self.personApplyButton.titleLabel.text;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
