//
//  Config.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/11.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

#define  dCarLastUpdateStr       NSStringFromSelector(@selector(carLastUpdateStr))
#define  dAdvLastUpdateStr       NSStringFromSelector(@selector(advLastUpdateStr))
#define  dPartnerLastUpdateStr   NSStringFromSelector(@selector(partnerLastUpdateStr))

#define  dDatabaseVersion        NSStringFromSelector(@selector(databaseVersion))

#define  dIsFirstLocateCity      NSStringFromSelector(@selector(isFirstLocateCity))
#define  dLocationCity           NSStringFromSelector(@selector(locationCity))

#define  dBusinessCircleNumber   NSStringFromSelector(@selector(businessCircleNumber))

#define  dIsEquireShowIntro      NSStringFromSelector(@selector(IsEnquireShowIntro))

#define  dDefaultCityName        NSStringFromSelector(@selector(defaultCityName))
#define  dDefaultCityCode        NSStringFromSelector(@selector(defaultCityCode))

#define  dServiceType            NSStringFromSelector(@selector(serviceType)) // 用车服务类型

#define dDeviceId                NSStringFromSelector(@selector(deviceId)) // 阿里云推送deviceId

#define dGpsCityName             NSStringFromSelector(@selector(gpsCityName)) // GPS定位城市
#define dGpsCityCode             NSStringFromSelector(@selector(gpsCityCode)) // GPS定位城市编码

#define  dSoundOn        NSStringFromSelector(@selector(isSoundOn))



@interface AppConfig : NSObject

+(AppConfig *)currentConfig;

@property (nonatomic, strong) NSUserDefaults             *defaults;

@property (nonatomic, strong) NSString *carLastUpdateStr;// 车辆最近更新时间
@property (nonatomic, strong) NSString *partnerLastUpdateStr;// 合作伙伴最近更新时间
@property (nonatomic, strong) NSString *databaseVersion;// 数据库版本号
@property (nonatomic, strong) NSNumber *isFirstLocateCity;
@property (nonatomic, strong) NSString *locationCity;
@property (nonatomic, strong) NSNumber *businessCircleNumber;// 业务圈收到的消息数量

@property (nonatomic, strong) NSNumber *IsEnquireShowIntro;//

@property (nonatomic, strong) NSString *advLastUpdateStr; // 广告上次更新时间

@property (nonatomic, strong) NSString *defaultCityName; // 城市名称
@property (nonatomic, strong) NSString *defaultCityCode; // 城市编码

@property (nonatomic, strong) NSString *gpsCityName; // GPS定位城市
@property (nonatomic, strong) NSString *gpsCityCode; // GPS定位城市编码

@property (nonatomic, strong) NSNumber *serviceType;

@property (nonatomic, strong) NSString *deviceId;

@property (nonatomic, strong) NSNumber    *isSoundOn;

@end
