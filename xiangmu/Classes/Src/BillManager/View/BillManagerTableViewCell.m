//
//  BillManagerTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/11.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BillManagerTableViewCell.h"

@implementation BillManagerTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"BillManagerTableViewCell";
    BillManagerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.backgroundColor = [UIColor whiteColor];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
//        cell.orderInfoView.tag = indexPath.section;
        
        //        cell.tag = indexPath.row;
        //        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectRow:)];
        //        cell.orderInfoView
    }
    return cell;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
