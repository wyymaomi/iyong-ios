//
//  InputHelperViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@protocol InputHelperDelegate<NSObject>

-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;

@end

@interface InputHelperViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *textFieldBackgroundView;

@property (weak, nonatomic) IBOutlet UIView *addContactView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) NSString *text;
@property (nonatomic, assign) NSInteger tag;
@property (weak, nonatomic) id<InputHelperDelegate> delegate;

@property (nonatomic, copy) void (^completeHandle)(NSInteger tag, NSString *text);

@end
