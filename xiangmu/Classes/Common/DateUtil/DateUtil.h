//
//  DateUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtil : NSObject

+(BOOL)isLessThanSixMonths:(NSString*)dateStr1
                  dateStr2:(NSString*)dateStr2
                     error:(NSString **)errorMsg;

+ (NSString*)getYearStr:(NSDate*)date;

+ (NSString*)getMonthStr:(NSDate*)date;

+ (NSDate*)getTenDayAfterDateTime:(NSDate*)date;
//+(NSDate*)getTenDayAfterDateTime:(NSString*)dateTime;

+ (NSString*)formatDateTime:(id)dateTime dateFomatter:(NSString*)dateFormatter;
+ (NSString*)timerAgo:(id)dateTime;
+ (NSString*)timerLeft:(id)dateTime;

+ (NSString *)getCurrentDate;

+ (NSInteger)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;

@end
