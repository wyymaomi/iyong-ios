//
//  CommentToolBarView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserExpericenViewModel.h"
#import "UserExpericeModel.h"

@interface CommentToolBarView : UIView

@property (nonatomic, strong) UIView *likeView;
@property (nonatomic, strong) UIImageView *likeImageView;
@property (nonatomic, strong) UILabel *likeNumsLabel;

@property (nonatomic, strong) UIView *favoriteView;
@property (nonatomic, strong) UIImageView *favoriteImageView;
@property (nonatomic, strong) UILabel *favoriteLabel;

@property (nonatomic, strong) UIView *telephoneView;
@property (nonatomic, strong) UIButton *telephoneButton;

@property (nonatomic, strong) UserExpericenViewModel *viewModel;

@property (nonatomic, strong) UIView *shareView;
@property (nonatomic, strong) UIImageView *shareImageView;
@property (nonatomic, strong) UILabel *shareLabel;


@end
