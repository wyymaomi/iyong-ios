//
//  UserExpericeModel.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "UserExpericeModel.h"

@implementation UserExpericeModel

- (BOOL)hasPhoto;
{
    if (!IsStrEmpty([StringUtil getSafeString:self.img1]) ||
        !IsStrEmpty([StringUtil getSafeString:self.img2]) ||
        !IsStrEmpty([StringUtil getSafeString:self.img3]) ||
        !IsStrEmpty([StringUtil getSafeString:self.img4])) {
        return YES;
    }
    
    return NO;
}

- (NSString*)dispalyTime;
{
    return [_createTime getDateTime:@"MM.dd"];
}



@end

@implementation UserExperienceListModel


@end

@implementation UserExpericeResponseData


@end
