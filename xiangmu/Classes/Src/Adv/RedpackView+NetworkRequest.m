//
//  RedpackView+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RedpackView+NetworkRequest.h"

@implementation RedpackView (NetworkRequest)

-(void)getRedPackage;
{
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(GET_RED_PACK_API) requestMethod:POST params:nil success:^(id responseData) {
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

-(void)saveRedPackage:(NSString*)redPackage;
{
    NSString *params = [NSString stringWithFormat:@"amount"];
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(SAVE_RED_PACK_API) requestMethod:POST params:params success:^(id responseData) {
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

@end
