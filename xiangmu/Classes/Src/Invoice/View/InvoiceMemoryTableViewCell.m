//
//  InvoiceMemoryTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceMemoryTableViewCell.h"

@implementation InvoiceMemoryTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"InvoiceMemoryTableViewCellIdentifier";
    InvoiceMemoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
//        cell.memoTextView.placeholder = @"特殊事项说明";
//        cell.memoTextView.placeholderColor = [UIColor lightGrayColor];
        
        XXTextView *textView = [[XXTextView alloc] initWithFrame:CGRectMake(16, 30, ViewWidth-32, 100)];
        textView.xx_placeholder = @"特殊事项说明";
        [cell.contentView addSubview:textView];
        cell.memoTextView = textView;
    }
    return cell;
}

+(CGFloat)getCellHeight
{
    return 174;
}

-(void)layoutSubviews
{
//    self.memoTextView.placeholder = @"特殊事项说明";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
