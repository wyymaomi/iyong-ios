//
//  InvoiceSettingEditViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/30.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceSettingEditViewController.h"
//#import "InvoiceInfoTableViewCell.h"
#import "InvoiceInfoSettingTableViewCell.h"
#import "ActionSheetCityPicker.h"
#import "ContactInfoTableViewCell.h"
#import "InvoiceMemoryTableViewCell.h"
#import "InvoiceManager.h"

@interface InvoiceSettingEditViewController ()<UITextViewDelegate>

@property (nonatomic, assign) BOOL isOpen;

@end

@implementation InvoiceSettingEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"开票信息";
    
    self.isOpen = YES;
    
    [self.view addSubview:self.tpkeyboardAvoidingGroupTableView];
    self.tpkeyboardAvoidingGroupTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)onClickUpdownArrow
{
    self.isOpen = !self.isOpen;
    
    [self.tpkeyboardAvoidingGroupTableView reloadData];
    
}

#pragma  mark - 城市选择

//-(UIView *)hideKeyboard
//{
//    for ( UIView *childView in self.tpkeyboardAvoidingGroupTableView.subviews ) {
//        if ( [childView respondsToSelector:@selector(isFirstResponder)] && [childView isFirstResponder] ) return childView;
//        UIView *result = [self TPKeyboardAvoiding_findFirstResponderBeneathView:childView];
//        if ( result ) return result;
//    }
//}

-(void)selectCity
{
    DLog(@"doSelectBankCity");
    WeakSelf

    
     [[self.tpkeyboardAvoidingGroupTableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.tpkeyboardAvoidingGroupTableView] resignFirstResponder];
    
    ActionSheetCityPicker *cityPicker = [[ActionSheetCityPicker alloc] initWithTitle:@"" level:AreainfoLevelDistrict doneBlock:^(ActionSheetCityPicker *picker, AreaInfoModel *province, AreaInfoModel *city, AreaInfoModel *district) {
        
        NSMutableString *areaName = [NSMutableString string];
        if ([province.name isEqualToString:@"北京"] ||
            [province.name isEqualToString:@"上海"] ||
            [province.name isEqualToString:@"重庆"] ||
            [province.name isEqualToString:@"天津"]) {
            
            [areaName appendFormat:@"%@%@", city.name, district.name];
        }
        else {
            [areaName appendFormat:@"%@%@%@", province.name, city.name, district.name];
        }
        
        weakSelf.invoiceSettingHttpMessage.areaCode = district.id;
        weakSelf.invoiceSettingHttpMessage.areaName = areaName;
        [weakSelf.tpkeyboardAvoidingGroupTableView reloadData];
        
    } cancelBlock:^(ActionSheetCityPicker *picker) {
        
        
    } origin:self.view];
    
    [cityPicker showActionSheetPicker];
    
}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
    //    return self.cellTitleList.count;
    //    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else if (section == 1){
        return 1;
    }
    return 1;
    //    NSArray *list = self.cellList[section];
    //    return list.count;
    //    return self.cellTitleList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        if (self.isOpen) {
            return [InvoiceInfoSettingTableViewCell getCellHeight];
        }
        else {
            return [InvoiceInfoSettingTableViewCell getCloseStatusCellHeight];
        }
    }
    else if (indexPath.section == 1 && indexPath.row == 0) {
        return [ContactInfoTableViewCell getCellHeight];
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        return [InvoiceMemoryTableViewCell getCellHeight];
    }
    return 0.00001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //    return 10;
    if (section == 0) {
        return 10;
    }
    
    return 0.00001f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView]-1) {
        return 77;
    }
    return 10;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView]-1) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 77)];
        
        UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [okButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xe06430)] forState:UIControlStateNormal];
        okButton.frame = CGRectMake(10, 23, ViewWidth-20, 45);
        okButton.titleLabel.font = FONT(16);
        [okButton setTitle:@"保存" forState:UIControlStateNormal];
        [okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        okButton.cornerRadius = 5;
        [okButton addTarget:self action:@selector(onClickSaveButton:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:okButton];
        
        return view;
    }
    
    return [UIView new];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        InvoiceInfoSettingTableViewCell *cell = [InvoiceInfoSettingTableViewCell cellWithTableView:tableView];
        
        cell.invoiceIdentifierView.hidden = !self.isOpen;
        cell.registAddressView.hidden = !self.isOpen;
        cell.bankNameView.hidden = !self.isOpen;
        cell.bankcardNumberView.hidden = !self.isOpen;
        
        UIImage *updownButtonImage = self.isOpen?[UIImage imageNamed:@"btn_down_arrow"]:[UIImage imageNamed:@"btn_up_arrow"];
        [cell.updownButton setImage:updownButtonImage forState:UIControlStateNormal];
        
        [cell.updownButton addTarget:self action:@selector(onClickUpdownArrow) forControlEvents:UIControlEventTouchUpInside];
        [cell.companyInvoiceView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickUpdownArrow)]];
        
        cell.invoiceTitleTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.header];
        cell.taxpayerNumberTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.taxpayerNumber];
        cell.registAddressTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.registerAddress];
        cell.bankNameTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.openingBank];
        cell.bankAccountTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.bankAccount];
        
        [cell.invoiceTitleTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.taxpayerNumberTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.registAddressTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.bankNameTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.bankAccountTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        return cell;
        
    }
    else if (indexPath.section == 1) {
        
        ContactInfoTableViewCell *cell = [ContactInfoTableViewCell cellWithTableView:tableView];
        
//        [cell.areaView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCity)]];
        
        cell.contactPhoneTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.tel];
        
        cell.contactNameTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.contact];
        
        cell.contactAreaTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.areaName];
        
        cell.addressDetailTextField.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.address];
        
        [cell.areaView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCity)]];
        
        [cell.contactNameTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.contactPhoneTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.addressDetailTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        return cell;
        
    }
    else if (indexPath.section == 2) {
        
        InvoiceMemoryTableViewCell *cell = [InvoiceMemoryTableViewCell cellWithTableView:tableView];
        
        
        cell.memoTextView.text = [StringUtil getSafeString:self.invoiceSettingHttpMessage.remark];
        
        cell.memoTextView.delegate = self;
        
        return cell;
        
    }
    
    return nil;
    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //
    //    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
    //        [self.customDelegate didSelectRowAtIndexPath:indexPath];
    //    }
    
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - 

-(void)onClickSaveButton:(id)sender
{
    NSString *errorMsg;
    BOOL isVerifyed = [InvoiceManager validateInvoiceSettinginfo:self.invoiceSettingHttpMessage error:&errorMsg];
    if (!isVerifyed) {
        [MsgToolBox showToast:errorMsg];
        return;
    }
    
    if (self.editMode == InvoiceEditModeAddNew) {
        [self addNewInvoiceSetting];
    }
    else {
        [self updateInvoiceSetting];
    }
    
}

#pragma mark - 增加发票信息设置

-(void)addNewInvoiceSetting
{
//    NSString *params = self.
    
    [self showHUDIndicatorViewAtCenter:@"正在保存发票设置信息，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(ADD_NEW_INVOICE_SETTING_API) requestMethod:POST params:self.invoiceSettingHttpMessage.description success:^(id responseData) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code = [responseData[@"code"] integerValue];
        
        if (code == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"添加发票设置成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf pressedBackButton];
                }
            }];
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code)];
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        [MsgToolBox showToast:errorDesc];
        
    }];
}

#pragma mark - 更新开票设置信息

-(void)updateInvoiceSetting
{
    //    NSString *params = self.
    
    [self showHUDIndicatorViewAtCenter:@"正在保存发票设置信息，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(UPDATE_INVOICE_SETTING_API) requestMethod:POST params:self.invoiceSettingHttpMessage.description success:^(id responseData) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code = [responseData[@"code"] integerValue];
        
        if (code == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"更新发票设置成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf pressedBackButton];
                }
            }];
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code)];
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        [MsgToolBox showToast:errorDesc];
        
    }];
}


#pragma mark - initialize

-(InvoiceSettingHttpMessage*)invoiceSettingHttpMessage
{
    if (!_invoiceSettingHttpMessage) {
        _invoiceSettingHttpMessage = [InvoiceSettingHttpMessage new];
    }
    return _invoiceSettingHttpMessage;
}

#pragma mark -

- (void)onInvoiceTextChanged:(id)sender {
    
    if ([sender isKindOfClass:[UITextField class]]) {
        UITextField *textField = sender;
        
        NSInteger tag = textField.tag;
        
        NSString *text = textField.text.trim;
        
        switch (tag) {
            case InvoiceTextFieldTypeTitle:
                self.invoiceSettingHttpMessage.header = text;
                break;
            case InvoiceTextFieldTypeTaxpayerNumber:
                self.invoiceSettingHttpMessage.taxpayerNumber = text;
                break;
            case InvoiceTextFieldTypeRegistAddress:
                self.invoiceSettingHttpMessage.registerAddress = text;
                break;
            case InvoiceTextFieldTypeBankName:
                self.invoiceSettingHttpMessage.openingBank = text;
                break;
            case InvoiceTextFieldTypeBankAccount:
                self.invoiceSettingHttpMessage.bankAccount = text;
                break;
            case InvoiceTextFieldTypeContactPhoneNumber:
                self.invoiceSettingHttpMessage.tel = text;
                break;
            case InvoiceTextFieldTypeDetailAddress:
                self.invoiceSettingHttpMessage.address = text;
                break;
            case InvoiceTextFieldTypeContacterName:
                self.invoiceSettingHttpMessage.contact = text;
                break;
                
            default:
                break;
        }
    }
    
    
}

- (void)textViewDidChange:(UITextView *)textView;
{
    self.invoiceSettingHttpMessage.remark = textView.text.trim;
}


@end
