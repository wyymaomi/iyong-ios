//
//  GenerateOrderHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface GenerateOrderHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *enquiryId;
@property (nonatomic, strong) NSString *carTime;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *itinerary;
@property (nonatomic, strong) NSString *customerName;
@property (nonatomic, strong) NSString *customerMobile;
//@property (nonatomic, strong) NSString *customerName;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, strong) NSString *dstCompanyId;
@property (nonatomic, strong) NSString *dstPrice;
@property (nonatomic, strong) NSString *areaCode;

@end
