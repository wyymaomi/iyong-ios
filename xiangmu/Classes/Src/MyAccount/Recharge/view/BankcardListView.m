//
//  BankcardListView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BankcardListView.h"
#import "BankcardPaymentTableViewCell.h"
#import "AddBankcardNumberTableViewCell.h"
#import "Bankcard.h"

@implementation BankcardListView

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.selectedRow = -1;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.dataList.count;
    if (section == 0) {
        return 1;
    }
    return self.dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
//        BankcardPaymentTableViewCell *cell = [BankcardPaymentTableViewCell cellWithTableView:tableView indexPath:indexPath];
//        cell.checkImageView.hidden = YES;
        
        AddBankcardNumberTableViewCell *cell = [AddBankcardNumberTableViewCell cellWithTableView:tableView indexPath:indexPath];
        
        return cell;
    }
    else {
    
        BankcardPaymentTableViewCell *cell = [BankcardPaymentTableViewCell cellWithTableView:tableView indexPath:indexPath];
        Bankcard *bankCard = self.dataList[indexPath.row];
        cell.bankcardLabel.text = [NSString stringWithFormat:@"%@(%@)", bankCard.bankName, [bankCard.number substringFromIndex:bankCard.number.length-4]];
        cell.checkImageView.hidden = self.selectedRow == indexPath.row ? NO : YES;
        return cell;
        
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(onAddBankcard)]) {
            [self.delegate onAddBankcard];
        }
        
    }
    else {
    
        self.selectedRow = indexPath.row;
        //    BankcardPaymentTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        //    self.titleLabel.text = cell.bankcardLabel.text;
        [tableView reloadData];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(onBankcardListSelect:)]) {
            [self.delegate onBankcardListSelect:self.selectedRow];
        }
        
    }
    

    
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}


@end
