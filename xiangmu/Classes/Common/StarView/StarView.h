//
//  StarView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kStarSizeWidth 10
#define kStarSizeHeight 10
#define kStarFrameWith 12

@interface StarView : UIView

@property (nonatomic, strong) UIImageView *star1;
@property (nonatomic, strong) UIImageView *star2;
@property (nonatomic, strong) UIImageView *star3;
@property (nonatomic, strong) UIImageView *star4;
@property (nonatomic, strong) UIImageView *star5;


- (void)setStarsImages:(CGFloat)starFloat;

@end
