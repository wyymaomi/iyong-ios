//
//  RegisterViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController (NetworkRequest)

-(void)doRegister;

-(void)doLogin:(NSString*)userName password:(NSString*)paswordPlain;

-(void)doGetVerifyCode;

- (void)doRegisterThirdparty;

@end
