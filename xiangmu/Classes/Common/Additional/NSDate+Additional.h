//
//  NSDate+Additional.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SECOND	(1)
#define MINUTE	(60 * SECOND)
#define HOUR	(60 * MINUTE)
#define DAY		(24 * HOUR)
#define MONTH	(30 * DAY)
#define YEAR	(12 * MONTH)

#define NSMondayTitle @"Monday"
#define NSTuesdayTitle @"Tuesday"
#define NSWednesdayTitle @"Wednesday"
#define NSThursdayTitle @"Thursday"
#define NSFridayTitle @"Friday"
#define NSSaturdayTitle @"Saturday"
#define NSSundayTitle @"Sunday"

static NSString *kNSDateHelperFormatFullDateWithTime    = @"MMM d, yyyy h:mm a";
static NSString *kNSDateHelperFormatFullDate            = @"MMM d, yyyy";
static NSString *kNSDateHelperFormatShortDateWithTime   = @"MMM d h:mm a";
static NSString *kNSDateHelperFormatShortDate           = @"MMM d";
static NSString *kNSDateHelperFormatWeekday             = @"EEEE";
static NSString *kNSDateHelperFormatWeekdayWithTime     = @"EEEE h:mm a";
static NSString *kNSDateHelperFormatTime                = @"h:mm a";
static NSString *kNSDateHelperFormatTimeWithPrefix      = @"'at' h:mm a";
static NSString *kNSDateHelperFormatSQLDate             = @"yyyy-MM-dd";
static NSString *kNSDateHelperFormatSQLTime             = @"HH:mm:ss";
static NSString *kNSDateHelperFormatSQLDateWithTime     = @"yyyy-MM-dd HH:mm:ss";
static NSString *kNSDateHelperFormatSQLDateWithMonth    = @"MM月dd日 HH:mm";

@interface NSDate (Category)

@property (nonatomic, readonly) NSInteger	year;
@property (nonatomic, readonly) NSInteger	month;
@property (nonatomic, readonly) NSInteger	day;
@property (nonatomic, readonly) NSInteger	hour;
@property (nonatomic, readonly) NSInteger	minute;
@property (nonatomic, readonly) NSInteger	second;
@property (nonatomic, readonly) NSInteger	weekday;
@property (nonatomic, readonly) NSString    *weekdayString;

@property (nonatomic, readonly) NSString    *hourString;
@property (nonatomic, readonly) NSString    *minuteString;

//@property (nonatomic, strong) NSDateFormatter *dateFormatter;

+ (void)initializeStatics;

+ (NSCalendar *)sharedCalendar;
+ (NSDateFormatter *)sharedDateFormatter;

- (NSString *)stringWithDateFormat:(NSString *)format;
- (NSString *)timeAgo;
- (NSString *)timeLeft;
- (NSString*)timeLeft:(NSDate*)date;
- (void)timeLeft:(NSDate*)date hour:(NSString**)hour minute:(NSString**)minute second:(NSString**)second;

+(NSTimeInterval)unixTimestamp;
+ (NSTimeInterval)timeStamp;
+ (NSString*)getTimeStamp;

+ (NSString *)getDateAccordingTime:(NSString *)aTime formatStyle:(NSString *)formate;
+ (NSDate *)dateFromString:(NSString *)dateString;
+ (NSDate*)dateFromString:(NSString*)dateString dateFormat:(NSString*)dateFormat;
+ (NSDate *)now;
+ (NSString*)nowString;

+ (NSInteger)dateCompare:(NSDate*)firstDate secondDate:(NSDate*)secondDate;

-(BOOL)isPreviousMonth:(NSDate*)compareDate;
-(BOOL)isNextMonth:(NSDate*)compareDate;


@end
