//
//  PayUtil.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PayUtil.h"

@implementation PayUtil

+(BOOL)validatePayInfo:(NSString*)amount bankcardNo:(NSString*)bankcardNo payType:(NSInteger)payType error:(NSString**)errorMsg;
{
    if (IsStrEmpty(amount)) {
        *errorMsg = @"请输入充值金额";
        return NO;
    }
    
    if (payType == RechargeTypeIPS) {
        if (IsStrEmpty(bankcardNo)) {
            *errorMsg = @"请选择银行卡";
            return NO;
        }
    }
    
    return YES;
}

@end
