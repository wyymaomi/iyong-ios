//
//  NSDate+Category.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NSDate+Additional.h"



@implementation NSDate (Category)

static NSCalendar *_calendar = nil;
static NSDateFormatter *_displayFormatter = nil;

//-(NSDateFormatter*)dateFormatter {
//    if (!self.dateFormatter) {
//        self.dateFormatter = [[NSDateFormatter alloc] init];
//    }
//    return self.dateFormatter;
//}

+ (void)initializeStatics {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        @autoreleasepool {
            if (_calendar == nil) {
#if __has_feature(objc_arc)
                _calendar = [NSCalendar currentCalendar];
#else
                _calendar = [[NSCalendar currentCalendar] retain];
#endif
            }
            if (_displayFormatter == nil) {
                _displayFormatter = [[NSDateFormatter alloc] init];
            }
        }
    });
}

+ (NSCalendar *)sharedCalendar {
    [self initializeStatics];
    return _calendar;
}

+ (NSDateFormatter *)sharedDateFormatter {
    [self initializeStatics];
    return _displayFormatter;
}

- (NSInteger)year
{
    return [[[self class] sharedCalendar] components:NSYearCalendarUnit
                                           fromDate:self].year;
}

- (NSInteger)month
{
    return [[[self class] sharedCalendar] components:NSMonthCalendarUnit
                                           fromDate:self].month;
}

- (NSInteger)day
{
    return [[[self class] sharedCalendar] components:NSDayCalendarUnit
                                           fromDate:self].day;
}

- (NSInteger)hour
{
    return [[[self class] sharedCalendar] components:NSHourCalendarUnit
                                           fromDate:self].hour;
}

- (NSInteger)minute
{
    return [[[self class] sharedCalendar] components:NSMinuteCalendarUnit
                                           fromDate:self].minute;
}

- (NSInteger)second
{
    return [[[self class] sharedCalendar] components:NSSecondCalendarUnit
                                           fromDate:self].second;
}

- (NSString*)hourString
{
    if (self.hour >= 10) {
        return [NSString stringWithFormat:@"%ld", (long)self.hour];
    }
    else {
        return [NSString stringWithFormat:@"0%ld", (long)self.hour];
    }
    return @"";
}

- (NSString*)minuteString
{
    if (self.minute >= 10) {
        return [NSString stringWithFormat:@"%ld", (long)self.minute];
    }
    else {
        return [NSString stringWithFormat:@"0%ld", (long)self.minute];
    }
    return @"";
}

- (NSInteger)weekday
{
    return [[[self class] sharedCalendar] components:NSWeekdayCalendarUnit
                                           fromDate:self].weekday;
}

- (NSString*)weekdayString
{
    NSUInteger weekday = [self weekday];
    switch (weekday) {
        case 1:
            return @"星期日";
            //            return NSLocalizedString(NSSundayTitle, @"星期日");
            
        case 2:
            return @"星期一";
            //            return NSLocalizedString(NSMondayTitle, @"星期一");
            
        case 3:
            return @"星期二";
            //            return NSLocalizedString(NSTuesdayTitle, @"星期二");
            
        case 4:
            return @"星期三";
            //            return NSLocalizedString(NSWednesdayTitle, @"星期三");
            
        case 5:
            return @"星期四";
            //            return NSLocalizedString(NSThursdayTitle, @"星期四");
            
        case 6:
            return @"星期五";
            //            return NSLocalizedString(NSFridayTitle, @"星期五");
            
        case 7:
            return @"星期六";
            //            return NSLocalizedString(NSSaturdayTitle, @"星期六");
            
        default:
            return nil;
    }
    
    return @"";
}

+ (NSDate *)dateFromString:(NSString *)dateString
{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    NSDateFormatter *dateFormatter = [[self class] sharedDateFormatter];
    [dateFormatter setDateFormat:kNSDateHelperFormatSQLDateWithTime];
    return [dateFormatter dateFromString:dateString];
}

+ (NSDate*)dateFromString:(NSString*)dateString dateFormat:(NSString*)dateFormat
{
    NSDateFormatter *dateFormatter = [[self class] sharedDateFormatter];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter dateFromString:dateString];
}

- (NSString *)stringWithDateFormat:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[self class] sharedDateFormatter];
//    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:self];
}

- (NSString *)timeAgo
{
    NSTimeInterval delta = [[NSDate date] timeIntervalSinceDate:self];
    
    if (delta < 1 * MINUTE)
    {
        return @"刚刚";
    }
    else if (delta < 2 * MINUTE)
    {
        return @"1分钟前";
    }
    else if (delta < 45 * MINUTE)
    {
        int minutes = floor((double)delta/MINUTE);
        return [NSString stringWithFormat:@"%d分钟前", minutes];
    }
    else if (delta < 90 * MINUTE)
    {
        return @"1小时前";
    }
    else if (delta < 24 * HOUR)
    {
        int hours = floor((double)delta/HOUR);
        return [NSString stringWithFormat:@"%d小时前", hours];
    }
    else if (delta < 48 * HOUR)
    {
        return @"昨天";
    }
    else if (delta < 30 * DAY)
    {
        int days = floor((double)delta/DAY);
        return [NSString stringWithFormat:@"%d天前", days];
    }
    else if (delta < 12 * MONTH)
    {
        int months = floor((double)delta/MONTH);
        return months <= 1 ? @"1个月前" : [NSString stringWithFormat:@"%d个月前", months];
    }
    
    int years = floor((double)delta/MONTH/12.0);
    return years <= 1 ? @"1年前" : [NSString stringWithFormat:@"%d年前", years];
}

- (void)timeLeft:(NSDate*)date hour:(NSString**)hour minute:(NSString**)minute second:(NSString**)second
{
    long int delta = lround( [self timeIntervalSinceDate:date] );
    
//    NSMutableString * result = [NSMutableString string];
    
    if ( delta >= YEAR )
    {
        NSInteger years = ( delta / YEAR );
//        [result appendFormat:@"%ld年", (long)years];
        delta -= years * YEAR ;
    }
    
    if ( delta >= MONTH )
    {
        NSInteger months = ( delta / MONTH );
//        [result appendFormat:@"%ld月", (long)months];
        delta -= months * MONTH ;
    }
    
    if ( delta >= DAY )
    {
        NSInteger days = ( delta / DAY );
//        [result appendFormat:@"%ld天", (long)days];
        delta -= days * DAY ;
    }
    
    if ( delta >= HOUR )
    {
        NSInteger hours = ( delta / HOUR );
//        [result appendFormat:@"%ld小时", (long)hours];
        if (hours < 10) {
            *hour = [NSString stringWithFormat:@"0%ld", (long)hours];
        }
        else {
            *hour = [NSString stringWithFormat:@"%ld", (long)hours];
        }
        delta -= hours * HOUR ;
       
    }
    else {
        *hour = @"00";
    }
    
    if ( delta >= MINUTE )
    {
        NSInteger minutes = ( delta / MINUTE );
//        [result appendFormat:@"%ld分", (long)minutes];
        if (minutes < 10){
            *minute = [NSString stringWithFormat:@"0%ld", (long)minutes];
        }
        else {
            *minute = [NSString stringWithFormat:@"%ld", (long)minutes];
        }

        delta -= minutes * MINUTE ;
    }
    else {
        *minute = @"00";
    }
    
    NSInteger seconds = ( delta / SECOND );
    if (seconds < 10){
        *second = [NSString stringWithFormat:@"0%ld", (long)seconds];
    }
    else {
        *second = [NSString stringWithFormat:@"%ld", (long)seconds];
    }
    
//    *second = [NSString stringWithFormat:@"%ld", (long) seconds];
//    [result appendFormat:@"%ld秒", (long)seconds];
    
//    return result;

}

-(NSString*)timeLeft:(NSDate*)date
{
    long int delta = lround( [self timeIntervalSinceDate:date] );
    
    NSMutableString * result = [NSMutableString string];
    
    if ( delta >= YEAR )
    {
        NSInteger years = ( delta / YEAR );
        [result appendFormat:@"%ld年", (long)years];
        delta -= years * YEAR ;
    }
    
    if ( delta >= MONTH )
    {
        NSInteger months = ( delta / MONTH );
        [result appendFormat:@"%ld月", (long)months];
        delta -= months * MONTH ;
    }
    
    if ( delta >= DAY )
    {
        NSInteger days = ( delta / DAY );
        [result appendFormat:@"%ld天", (long)days];
        delta -= days * DAY ;
    }
    
    if ( delta >= HOUR )
    {
        NSInteger hours = ( delta / HOUR );
        [result appendFormat:@"%ld小时", (long)hours];
        delta -= hours * HOUR ;
    }
    
    if ( delta >= MINUTE )
    {
        NSInteger minutes = ( delta / MINUTE );
        [result appendFormat:@"%ld分", (long)minutes];
        delta -= minutes * MINUTE ;
    }
    
    NSInteger seconds = ( delta / SECOND );
    [result appendFormat:@"%ld秒", (long)seconds];
    
    return result;
    
}

- (NSString *)timeLeft
{
    long int delta = lround( [self timeIntervalSinceDate:[NSDate date]] );
    
    NSMutableString * result = [NSMutableString string];
    
    if ( delta >= YEAR )
    {
        NSInteger years = ( delta / YEAR );
        [result appendFormat:@"%ld年", (long)years];
        delta -= years * YEAR ;
    }
    
    if ( delta >= MONTH )
    {
        NSInteger months = ( delta / MONTH );
        [result appendFormat:@"%ld月", (long)months];
        delta -= months * MONTH ;
    }
    
    if ( delta >= DAY )
    {
        NSInteger days = ( delta / DAY );
        [result appendFormat:@"%ld天", (long)days];
        delta -= days * DAY ;
    }
    
    if ( delta >= HOUR )
    {
        NSInteger hours = ( delta / HOUR );
        [result appendFormat:@"%ld小时", (long)hours];
        delta -= hours * HOUR ;
    }
    
    if ( delta >= MINUTE )
    {
        NSInteger minutes = ( delta / MINUTE );
        [result appendFormat:@"%ld分", (long)minutes];
        delta -= minutes * MINUTE ;
    }
    
    NSInteger seconds = ( delta / SECOND );
    [result appendFormat:@"%ld秒", (long)seconds];
    
    return result;
}


/**
 根据时间戳获取时间字符串

 @param aTime
 @param formate
 @return
 */
+ (NSString *)getDateAccordingTime:(NSString *)aTime formatStyle:(NSString *)formate
{
    
    NSDate *nowDate = [NSDate dateWithTimeIntervalSince1970:[aTime doubleValue]/1000];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:formate];
    return[formatter stringFromDate:nowDate];
}

+(NSTimeInterval)unixTimestamp
{
    return [[NSDate date] timeIntervalSince1970];
}

+ (NSTimeInterval)timeStamp
{
    return [[NSDate date] timeIntervalSince1970]*1000;
}

+(NSString*)getTimeStamp
{
    return [NSString stringWithFormat:@"%.f", [[NSDate date] timeIntervalSince1970] * 1000];
}

+ (NSDate *)now
{
    return [NSDate date];
}

+ (NSString*)nowString
{
    NSDate *date = [NSDate date];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    NSDateFormatter *dateFormatter = [[self class] sharedDateFormatter];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:date];
}

+(NSInteger)dateCompare:(NSDate*)firstDate secondDate:(NSDate*)secondDate;
{
    switch ([firstDate compare:secondDate]) {
        case NSOrderedSame:
            return 0;
            break;
        case NSOrderedAscending:
            return -1;
        case NSOrderedDescending:
            return 1;
            
        default:
            break;
    }
    
    return 0;
}

// 判断是否当前月份的前月
-(BOOL)isPreviousMonth:(NSDate*)compareDate;
{
    NSInteger currentDateYear = self.year;
    NSInteger compareDateYear = compareDate.year;
    NSInteger currentDateMonth = self.month;
    NSInteger compareDateMonth = compareDate.month;
    if (compareDateYear >= currentDateYear) {
        if (compareDateMonth >= currentDateMonth) {
            return NO;
        }
    }
    
    return YES;
}

-(BOOL)isNextMonth:(NSDate*)compareDate;
{
    return YES;
}

@end
