//
//  UserManager.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UserManager.h"
#import <CloudPushSDK/CloudPushSDK.h>
#import "AFNetworking.h"
#import "EncryptUtil.h"
#import "HttpConstants.h"
#import "NetworkManager.h"
#import "LogConstants.h"
#import "AppConstants.h"
#import "NSString+Encrypt.h"
#import "UserDefaults.h"
#import "AppDelegate.h"
#import "AppConfig.h"
#import "SystemInfo.h"
#import "SFHFKeychainUtils.h"
#import "ChatDemoHelper.h"
#import <AdSupport/ASIdentifierManager.h>
#import "JPUSHService.h"


@implementation UserManager

+ (instancetype)sharedInstance
{
    static UserManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[UserManager alloc] init];
    });
    return instance;
}

+(NSString*)systemInfo
{
//    deviceId 移动端推送识别号
//    versionCode iYONG版本号
//    osType 系统类型(1=IOS，2=android)
//    osVersionCode 移动端设备系统版本
//    clientModel 移动端设备型号
//    NSString *deviceId = [CloudPushSDK getDeviceId];
//    if (IsStrEmpty(deviceId)) {
//        deviceId = [AppConfig currentConfig].deviceId;
//    }
//    NSString *deviceId = [JPUSHService registrationID];
//    if (IsStrEmpty(deviceId)) {
//        deviceId = [AppConfig currentConfig].deviceId;
//    }
    NSString *deviceId = [AppConfig currentConfig].deviceId;
    
    NSString *uuid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    return [NSString stringWithFormat:@"deviceId=%@&versionCode=%@&osType=1&osVersionCode=%@&clientModel=%@&uuid=%@",
            [StringUtil getSafeString:deviceId],
            [SystemInfo appShortVersion],
            [[SystemInfo OSVersion] replaceAll:@" " with:@""],
            [[SystemInfo deviceVersion] replaceAll:@" " with:@""],
            uuid];
}


- (void)getAccountInfo:(void(^)(NSInteger code_status))block
{
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(My_Detail_API) requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
//        NSLog(@"response success");
        NSInteger code_status = [responseData[@"code"] integerValue];
        if(code_status == STATUS_OK) {
            
            UserModel *userInfo = [[UserModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            strongSelf.userModel = userInfo;
            

            
        }
        
        if (block) {
            block(code_status);
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        if (block) {
            block(NETWORK_FAILED);
        }
        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"网络连接错误，请重试" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
        //        [alert show];
        //        return;
        
    }];
}

- (void)loginWithUsername:(NSString *)username password:(NSString *)password
{
    
//    EMError *error = [[EMClient sharedClient] registerWithUsername:username password:password];
//    if (error==nil) {
//        NSLog(@"注册成功");
//        
//        EMError *error = [[EMClient sharedClient] loginWithUsername:username password:password];
//        if (!error) {
//            NSLog(@"环信登陆成功");
//            [self saveLastLoginUsername];
//            NSString *from = [[EMClient sharedClient] currentUsername];
//            DLog(@"from = %@", from);
//            DLog(@"isLogin = %d", [[EMClient sharedClient] isLoggedIn]);
//        }
//        
//    }
//    else {
//        EMError *error = [[EMClient sharedClient] loginWithUsername:username password:password];
//        if (!error) {
//            NSLog(@"环信登陆成功");
//            [self saveLastLoginUsername];
//            NSString *from = [[EMClient sharedClient] currentUsername];
//            DLog(@"from = %@", from);
//            DLog(@"isLogin = %d", [[EMClient sharedClient] isLoggedIn]);
//        }
//    }
//    
//
//    
//    return;
//    [self showHudInView:self.view hint:NSLocalizedString(@"login.ongoing", @"Is Login...")];
    //异步登陆账号
    __weak typeof(self) weakself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        EMError *error = [[EMClient sharedClient] loginWithUsername:username password:password];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakself hideHud];
            if (!error) {
                //设置是否自动登录
                DLog(@"登陆环信服务器成功");
                [[EMClient sharedClient].options setIsAutoLogin:NO];
                
                
                //获取数据库中数据
//                [MBProgressHUD showHUDAddedTo:weakself.view animated:YES];
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [[EMClient sharedClient] migrateDatabaseToLatestSDK];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [[ChatDemoHelper shareHelper] asyncGroupFromServer];
                        [[ChatDemoHelper shareHelper] asyncConversationFromDB];
//                        [[ChatDemoHelper shareHelper] asyncPushOptions];
//                        [MBProgressHUD hideAllHUDsForView:weakself.view animated:YES];
                        //发送自动登陆状态通知
                        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@([[EMClient sharedClient] isLoggedIn])];
                        
                        //保存最近一次登录用户名
                        [weakself saveLastLoginUsername];
                        
//                        [[EMClient sharedClient] setApnsNickname:[UserManager sharedInstance].userModel.displayChatName];
                
                        NSString *from = [[EMClient sharedClient] currentUsername];
                        
//                        - (void)savePushOptions
//                        {
                            //    BOOL isUpdate = NO;
                            //    EMPushOptions *options = [[EMClient sharedClient] pushOptions];
                            //    if (_pushDisplayStyle != options.displayStyle) {
                            //        options.displayStyle = _pushDisplayStyle;
                            //        isUpdate = YES;
                            //    }
                            //
                            //    if (_nickName && _nickName.length > 0 && ![_nickName isEqualToString:options.displayName])
                            //    {
                            //        options.displayName = _nickName;
                            //        isUpdate = YES;
                            //    }
                            //    if (options.noDisturbingStartH != _noDisturbingStart || options.noDisturbingEndH != _noDisturbingEnd){
                            //        isUpdate = YES;
                            //        options.noDisturbStatus = _noDisturbingStatus;
                            //        options.noDisturbingStartH = _noDisturbingStart;
                            //        options.noDisturbingEndH = _noDisturbingEnd;
                            //    }
                            
                            EMPushOptions *options = [[EMClient sharedClient] pushOptions];
                            options.displayStyle = EMPushDisplayStyleMessageSummary;
//                            options.displayName = [[UserManager sharedInstance].userModel displayChatName];
                        
                            //    options.displayStyle =EMPushOptions *options = [[EMClient sharedClient] pushOptions];
                            //    options.displayStyle == EMPushDisplayStyleMessageSummary;// 显示消息内容
                            // options.displayStyle == EMPushDisplayStyleSimpleBanner // 显示“您有一条新消息”
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                    EMError *error = [[EMClient sharedClient] updatePushOptionsToServer]; // 更新配置到服务器，该方法为同步方法，如果需要，请放到单独线程
                    if(!error) {
                        DLog(@"updateEMPushOptions success");
#if DEBUG
                        //                                [MsgToolBox showToast:@"设置消息类型成功"];
#endif
                        // 成功
                    }else {
                        DLog(@"updateEMPushOptions failure");
#if DEBUG
                        //                                [MsgToolBox showToast:@"设置消息类型失败"];
#endif
                        // 失败
                    }
                    
//                });

                            
                            
                            //    __weak typeof(self) weakself = self;
                            //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            //        EMError *error = nil;
                            //        if (isUpdate) {
                            //            error = [[EMClient sharedClient] updatePushOptionsToServer];
                            //        }
                            //        dispatch_async(dispatch_get_main_queue(), ^{
                            //            if (!error) {
                            //                [weakself.navigationController popViewControllerAnimated:YES];
                            //            } else {
                            //                [weakself showHint:[NSString stringWithFormat:@"保存失败-error:%@",error.errorDescription]];
                            //            }
                            //        });
                            //    });
//                        }
                        
                        DLog(@"isLogin = %d", [[EMClient sharedClient] isLoggedIn]);
                        
                        DLog(@"currentUserName=%@", from);

//                    });
//                });
            } else {
#if DEBUG
                DLog(@"登陆环信服务器失败");
                switch (error.code)
                {
                    case EMErrorUserNotFound:
                        TTAlertNoTitle(NSLocalizedString(@"error.usernotExist", @"User not exist!"));
                        break;
                    case EMErrorNetworkUnavailable:
                        TTAlertNoTitle(NSLocalizedString(@"error.connectNetworkFail", @"No network connection!"));
                        break;
                    case EMErrorServerNotReachable:
                        TTAlertNoTitle(NSLocalizedString(@"error.connectServerFail", @"Connect to the server failed!"));
                        break;
                    case EMErrorUserAuthenticationFailed:
                        TTAlertNoTitle(error.errorDescription);
                        break;
                    case EMErrorServerTimeout:
                        TTAlertNoTitle(NSLocalizedString(@"error.connectServerTimeout", @"Connect to the server timed out!"));
                        break;
                    case EMErrorServerServingForbidden:
                        TTAlertNoTitle(NSLocalizedString(@"servingIsBanned", @"Serving is banned"));
                        break;
                    default:
                        TTAlertNoTitle(NSLocalizedString(@"login.fail", @"Login failure"));
                        break;
                }
#endif
            }
//        });
    });
}

#pragma  mark - private
- (void)saveLastLoginUsername
{
    NSString *username = [[EMClient sharedClient] currentUsername];
    if (username && username.length > 0) {
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:username forKey:[NSString stringWithFormat:@"em_lastLogin_username"]];
        [ud synchronize];
    }
}


#pragma mark - 查询环信账户体系中账户
- (void)doQueryHXAccount
{
    
//    [self loginWithUsername:@"wyy" password:@"wyy"];
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, HX_USER_INFO_API];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary * responseData) {
        
//        NSDictionary *result = responseData;
        
//        DLog(@"doQueryHXAccount: result = %@", responseData);
        
        NSInteger code = [responseData[@"code"] integerValue];
        
        if (code == STATUS_OK) {
            
            NSDictionary *data = responseData[@"data"];
            
            NSString *password = data[@"password"];
            
            NSString *username = data[@"username"];
            
            if (username.length > 0 && password.length > 0) {
                
//                BOOL isAutoLogin = [EMClient sharedClient].options.isAutoLogin;
                
//                if (!isAutoLogin) {
                    [weakSelf loginWithUsername:username password:password];
//                }  
                
            }
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        DLog(@"doQueryHXAccount: errorDesc = %@", errorDesc);
        
    }];
    
}

#pragma mark - 第三方注册

- (void)doThirdPartyRegister:(NSString*)registerParam
                      mobile:(NSString*)mobile
                thirdPartyId:(NSUInteger)thirdPartyId
                     success:(void(^)(NSInteger code_status))withSuccess
                     failure:(void(^)(NSInteger code_status))withFailure;
{
    NSString *desKey = [EncryptUtil generateDesKey];
    NSAssert(desKey != nil, @"desKey cannot be null");
    
    NSString *params = [NSString stringWithFormat:@"%@&%@", registerParam, [UserManager systemInfo]];
//    NSDictionary *encryptParams = [EncryptUtil encryptRegisterBody:loginParam param2:params desKey:desKey];
    
    NSDictionary *encryptParams = [EncryptUtil encryptLoginRequest:params desKey:desKey];
    NSAssert(encryptParams != nil, @"encryptParams cannot be null");
    
    
    NSString *url;
    if (thirdPartyId == ThirdpartyIdWechat) {
        url = [NSString stringWithFormat:@"%@/%@",HTTP_BASE_URL, LOGIN_REGISTER_WECHAT_API];
    }
    else if (thirdPartyId == ThirdpartyIdQQ) {
        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_REGISTER_QQ_API];
    }
    else if (thirdPartyId == ThirdpartyIdWeibo) {
        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_REGISTER_WEIBO_API];
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    __weak __typeof(self)weakSelf = self;
    
    [manager POST:url
       parameters:encryptParams
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              __strong __typeof(self)strongSelf = weakSelf;
              // 返回加密报文
              NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              
              // 解密返回的报文
              NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
              
              NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
              
              id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
              
              DLog(@"result = %@", result);
              
              if ([result isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *resultDict = result;
                  NSString *code = resultDict[@"code"];
                  
                  if (IsStrEmpty(code)) {
                      return;
                  }
                  
                  if ([code integerValue] == STATUS_OK ) {
                      
                      // 存储sessionId，token
                      NSDictionary *data = resultDict[@"data"];
                      
                      strongSelf.thirdPartyId = [NSString stringWithFormat:@"%ld", thirdPartyId];
                      strongSelf.thirdPartyLoginParams = registerParam;
                      
                      strongSelf.token = data[@"token"];
                      strongSelf.desKey = desKey;
                      
                      UserModel *userData = [[UserModel alloc] initWithDictionary:data error:nil];
                      strongSelf.userModel = userData;
                      
                      if (withSuccess) {
                          withSuccess(STATUS_OK);
                      }
                  }
                  else {
                      if (withFailure) {
                          withFailure([code integerValue]);
                      }
                  }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"Error: %@", error);
              if (withFailure) {
                  withFailure(STATUS_FAIL);
              }
              
          }];

    
}


#pragma mark - 注册操作
-(void)doRegister:(NSString*)params
         userName:(NSString*)userName
         password:(NSString*)password
          success:(void(^)(NSInteger code_status))withSuccess
          failure:(void(^)(NSInteger code_status))withFailure;
{
    NSString *desKey = [EncryptUtil generateDesKey];
    NSAssert(desKey != nil, @"desKey cannot be null");
    
    NSString *loginParam = [NSString stringWithFormat:@"username=%@&password=%@&%@", userName, password, [UserManager systemInfo]];
    
    NSDictionary *encryptParams = [EncryptUtil encryptRegisterBody:loginParam param2:params desKey:desKey];
    
    if (loginParam == nil || [loginParam isEqualToString:@""]) {
        return;
    }
    
    if (encryptParams == nil) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Register_API];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    __weak __typeof(self)weakSelf = self;
    
    [manager POST:url
       parameters:encryptParams
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              __strong __typeof(self)strongSelf = weakSelf;
              // 返回加密报文
              NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              
              // 解密返回的报文
              NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
              
              NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
              
              id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
              
              DLog(@"result = %@", result);
              
              if ([result isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *resultDict = result;
                  NSString *code = resultDict[@"code"];
                  
                  if (IsStrEmpty(code)) {
                      return;
                  }
                  
                  if ([code integerValue] == STATUS_OK ) {
                      
                      // 保存用户名和密码
//                      [SFHFKeychainUtils storeUsername:kLoginUserNameKey andPassword:userName forServiceName:kKeychainServiceNameSuffix updateExisting:YES error:nil];
//                      [SFHFKeychainUtils storeUsername:kLoginPasswdKey andPassword:password forServiceName:kKeychainServiceNameSuffix updateExisting:YES error:nil];
                      
                      strongSelf.storeUserName = userName;
                      strongSelf.storePassword = password;
                      
                      // 存储sessionId，token
                      NSDictionary *data = resultDict[@"data"];
                      strongSelf.token = data[@"token"];
                      strongSelf.desKey = desKey;
                      
//                      strongSelf.loginShaBody = [loginParam sha1];
//                      strongSelf.loginRsaBody = [EncryptUtil getRSALoginParams:loginParam];
                      
                      UserModel *userData = [[UserModel alloc] initWithDictionary:data error:nil];
                      strongSelf.userModel = userData;
                      
                      if (withSuccess) {
                          withSuccess(STATUS_OK);
                      }
                  }
                  else {
                      if (withFailure) {
                          withFailure([code integerValue]);
                      }
                  }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"Error: %@", error);
              if (withFailure) {
                  withFailure(STATUS_FAIL);
              }
              
          }];
}

#pragma mark - 登录操作
-(void)doAutoLogin:(void(^)(NSInteger code_status))withSuccess
           failure:(void(^)(NSInteger code_status))withFailure;
{
//    DLog(@"url = %@", [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_API]);
    DLog(@"openUDID = %@", [SystemInfo deviceUUID]);
    
    NSString *desKey = [EncryptUtil generateDesKey];
    NSString *params;
    NSDictionary *encryptParams;
    NSString *url;
    NSString *token = [StringUtil getSafeString:[UserManager sharedInstance].token];
//    NSString *uuid = [StringUtil getSafeString:[SystemInfo deviceUUID]];
    NSString *uuid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    // 判断上次是否第三方登录
    NSString *thirdPartyLogin = self.thirdPartyId;
    if (IsStrEmpty(thirdPartyLogin)) {
        NSString *userName;
        NSString *passwordPlain;
        
        userName = self.storeUserName;
        passwordPlain = self.storePassword;
        
        if (IsStrEmpty(userName.trim) || IsStrEmpty(passwordPlain.trim)) {
            return;
        }
        params = [NSString stringWithFormat:@"username=%@&password=%@&%@&token=%@&uuid=%@",
                  userName, passwordPlain, [UserManager systemInfo], token, uuid];
        
        encryptParams = [EncryptUtil encryptLoginRequest:params desKey:desKey];
        
        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_API];
    }
    else {
        params = [NSString stringWithFormat:@"%@&%@&token=%@&uuid=%@",
                  self.thirdPartyLoginParams, [UserManager systemInfo], token, uuid];
        encryptParams = [EncryptUtil encryptLoginRequest:params desKey:desKey];
        NSInteger thirdPartyId = [thirdPartyLogin integerValue];
        if (thirdPartyId == ThirdpartyIdWechat) {
            url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_WEHAT_API];
        }
        else if (thirdPartyId == ThirdpartyIdQQ){
            url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_QQ_API];
        }
        else if (thirdPartyId == ThirdpartyIdWeibo){
            url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_WEIBO_API];
        }
    }
    
    if (IsNilOrNull(encryptParams)) {
        return;
    }

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [manager.requestSerializer setValue:[UserManager sharedInstance].token forHTTPHeaderField:@"token"];
    __weak __typeof(self)weakSelf = self;
    [manager POST:url
       parameters:encryptParams
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              __strong __typeof(self)strongSelf = weakSelf;
              // 返回加密报文
              if (![responseObject isKindOfClass:[NSData class]]) {
                  return;
              }
              
              NSData *responseData = responseObject;
              if (responseData.length == 0) {
                  return;
              }
              
              NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              if (IsStrEmpty(responseCipher)) {
                  return;
              }
              
              // 解密返回的报文
              NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
              if (IsStrEmpty(decryptResponse)) {
                  return;
              }
              
              NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
              if (IsNilOrNull(decryptData)) {
                  return;
              }
              
              id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
              
              DLog(@"result = %@", result);
              
              if ([result isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *resultDict = result;
                  
                  NSString *code = resultDict[@"code"];
                  
                  if (IsStrEmpty(code)) {
                      return;
                  }
                  
                  if ([code integerValue] == STATUS_OK ) {
                      // 存储sessionId，token
                      NSDictionary *data = resultDict[@"data"];
                      
                      strongSelf.token = data[@"token"];
                      strongSelf.desKey = desKey;
                      strongSelf.userModel = [[UserModel alloc] initWithDictionary:data error:nil];
                      
//                      strongSelf.thirdPartyId = nil;
//                      strongSelf.thirdPartyLoginParams = nil;
                      
                      // 登录成功后重新计算时间
                      AppDelegate *appDelegate = APP_DELEGATE;
                      [appDelegate initResignTime];
                      
                      [strongSelf doQueryHXAccount];
                      
                      if (withSuccess) {
                          withSuccess(STATUS_OK);
                      }
                  }
                  else {
                      if (withFailure) {
                          withFailure([code integerValue]);
                      }
                  }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"Error: %@", error);

              if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
                  [[UserManager sharedInstance] doLogout];
                  if (withFailure) {
                      withFailure(NETWORK_FAILED);
                  }
//                  [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
                  return;
              }
              else {
                  if (withFailure) {
                      withFailure(NETWORK_FAILED);
                  }
              }
              
          }];
}


#pragma mark - 第三方登录
- (void)doThirdPartyLogin:(NSString*)loginParams
             thirdPartyId:(NSUInteger)thirdPartyId
                success:(void(^)(NSInteger code_status))withSuccess
                failure:(void(^)(NSInteger code_status))withFailure;
{
    NSString *desKey = [EncryptUtil generateDesKey];
    
    NSAssert(desKey != nil, @"desKey cannot be null");
    
    DLog(@"desKey = %@", desKey);
    
    //    NSString *app_version = [SystemInfo appVersion];
    NSString *url;
    if (thirdPartyId == ThirdpartyIdWechat) {
        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_WEHAT_API];
    }
    else if (thirdPartyId == ThirdpartyIdQQ){
        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_QQ_API];
    }
    else if (thirdPartyId == ThirdpartyIdWeibo){
        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_WEIBO_API];
    }
    DLog(@"url = %@", url);
    
    NSString *params ;//= [NSString stringWithFormat:@"username=%@&password=%@&deviceId=%@&versionCode=%@&osType=1", userName, passwordPlain, [StringUtil getSafeString:[CloudPushSDK getDeviceId]], [SystemInfo appShortVersion]];
    
//    NSString *deviceId = [CloudPushSDK getDeviceId];
//    if (IsStrEmpty(deviceId)) {
//        deviceId = [AppConfig currentConfig].deviceId;
//    }
    NSString *deviceId = [AppConfig currentConfig].deviceId;
    
    params = [NSString stringWithFormat:@"%@&%@",loginParams, [UserManager systemInfo]];

    NSDictionary *encryptParams = [EncryptUtil encryptLoginRequest:params desKey:desKey];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    __weak __typeof(self)weakSelf = self;
    [manager POST:url
       parameters:encryptParams
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              __strong __typeof(self)strongSelf = weakSelf;
              // 返回加密报文
              if (![responseObject isKindOfClass:[NSData class]]) {
                  return;
              }
              
              NSData *responseData = responseObject;
              if (responseData.length == 0) {
                  return;
              }
              
              NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              if (IsStrEmpty(responseCipher)) {
                  return;
              }
              
              // 解密返回的报文
              NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
              if (IsStrEmpty(decryptResponse)) {
                  return;
              }
              
              NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
              if (IsNilOrNull(decryptData)) {
                  return;
              }
              
              id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
              
              DLog(@"result = %@", result);
              
              if ([result isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *resultDict = result;
                  
                  NSString *code = resultDict[@"code"];
                  
                  if (IsStrEmpty(code)) {
                      return;
                  }
                  
                  if ([code integerValue] == STATUS_OK ) {
                      
                      // 保存用户名和密码
//                      strongSelf.storeUserName = userName;
//                      strongSelf.storePassword = passwordPlain;
                      
                      // 存储sessionId，token
                      NSDictionary *data = resultDict[@"data"];
                      
//                      strongSelf.storeUserName = mobile;
//                      strongSelf.
                      
                      // 保存第三方登录信息
                      strongSelf.thirdPartyLoginParams = loginParams;
                      strongSelf.thirdPartyId = [NSString stringWithFormat:@"%ld", (unsigned long)thirdPartyId];
                      
                      strongSelf.token = data[@"token"];
                      //                      strongSelf.companyId = data[@"companyId"];
                      strongSelf.desKey = desKey;
                      //                      strongSelf.loginShaBody = [params sha1];
                      //                      strongSelf.loginRsaBody = [EncryptUtil getRSALoginParams:params];
                      strongSelf.userModel = [[UserModel alloc] initWithDictionary:data error:nil];
                      
                      // 登录成功后重新计算时间
                      AppDelegate *appDelegate = APP_DELEGATE;
                      [appDelegate initResignTime];
                      
                      [strongSelf doQueryHXAccount];
                      
                      if (withSuccess) {
                          withSuccess(STATUS_OK);
                      }
                  }
                  else {
                      if (withFailure) {
                          withFailure([code integerValue]);
                      }
                  }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"Error: %@", error);
              //              if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
              //                  [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
              //                  return;
              //              }
              //              else {
              if (withFailure) {
                  withFailure(NETWORK_FAILED);
              }
              //              }
              
          }];
}


#pragma mark - 登录操作
-(void)doLogin:(NSString*)userName
      password:(NSString*)passwordPlain
       success:(void(^)(NSInteger code_status))withSuccess
       failure:(void(^)(NSInteger code_status))withFailure;
{
    NSString *desKey = [EncryptUtil generateDesKey];
    
    NSAssert(desKey != nil, @"desKey cannot be null");
    
    DLog(@"desKey = %@", desKey);
    
//    NSString *app_version = [SystemInfo appVersion];
    
    NSString *params ;
    
//    NSString *deviceId = [CloudPushSDK getDeviceId];
//    if (IsStrEmpty(deviceId)) {
    NSString *deviceId = [AppConfig currentConfig].deviceId;
//    }
    NSString *uuid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    params = [NSString stringWithFormat:@"username=%@&password=%@&deviceId=%@&versionCode=%@&osType=1&clientModel=%@&osVersionCode=%@&uuid=%@", userName, passwordPlain, [StringUtil getSafeString:deviceId], [SystemInfo appShortVersion], [[SystemInfo deviceVersion] replaceAll:@" " with:@""], [[SystemInfo OSVersion] replaceAll:@" " with:@""], uuid];
    NSDictionary *encryptParams = [EncryptUtil encryptLoginRequest:params desKey:desKey];
//    DLog(@"encryptParams = %@", encryptParams);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [manager.requestSerializer setValue:[UserManager sharedInstance].token forHTTPHeaderField:@"token"];
    __weak __typeof(self)weakSelf = self;
    [manager POST:[NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_API]
       parameters:encryptParams
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              __strong __typeof(self)strongSelf = weakSelf;
              // 返回加密报文
              if (![responseObject isKindOfClass:[NSData class]]) {
                  return;
              }
              
              NSData *responseData = responseObject;
              if (responseData.length == 0) {
                  return;
              }
              
              NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              if (IsStrEmpty(responseCipher)) {
                  return;
              }
              
              // 解密返回的报文
              NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
              if (IsStrEmpty(decryptResponse)) {
                  return;
              }
              
              NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
              if (IsNilOrNull(decryptData)) {
                  return;
              }
              
              id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
              
              DLog(@"result = %@", result);
              
              if ([result isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *resultDict = result;
                  
                  NSString *code = resultDict[@"code"];
                  
                  if (IsStrEmpty(code)) {
                      return;
                  }
                  
                  if ([code integerValue] == STATUS_OK ) {
                      
                      
                      
                      // 保存用户名和密码
                      strongSelf.storeUserName = userName;
                      strongSelf.storePassword = passwordPlain;
                      
                      strongSelf.thirdPartyId = nil;
                      
                      // 存储sessionId，token
                      NSDictionary *data = resultDict[@"data"];

                      strongSelf.token = data[@"token"];
//                      strongSelf.companyId = data[@"companyId"];
                      strongSelf.desKey = desKey;
//                      strongSelf.loginShaBody = [params sha1];
//                      strongSelf.loginRsaBody = [EncryptUtil getRSALoginParams:params];
                      strongSelf.userModel = [[UserModel alloc] initWithDictionary:data error:nil];
                      
                      // 登录成功后重新计算时间
                      AppDelegate *appDelegate = APP_DELEGATE;
                      [appDelegate initResignTime];
                      
                      [strongSelf doQueryHXAccount];
                      
                      if (withSuccess) {
                          withSuccess(STATUS_OK);
                      }
                  }
                  else {
                      if (withFailure) {
                          withFailure([code integerValue]);
                      }
                  }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"Error: %@", error);
//              if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
//                  [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
//                  return;
//              }
//              else {
                  if (withFailure) {
                      withFailure(NETWORK_FAILED);
                  }
//              }
              
          }];
    
}

#pragma mark - 获取注册验证码 加密方式同登陆操作

- (void)doGetRegisterVerifyCode:(NSString*)mobile
                        success:(void(^)(NSInteger code_status))withSuccess
                        failure:(void(^)(NSInteger code_status))withFailure;
{
    DLog(@"iphone UDID = %@", [SystemInfo deviceUUID]);
    
    NSString *desKey = [EncryptUtil generateDesKey];
    NSString *params = [NSString stringWithFormat:@"mobile=%@&osType=1&udid=%@", mobile, [SystemInfo deviceUUID]];
    NSDictionary *encryptParams = [EncryptUtil encryptLoginRequest:params desKey:desKey];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    __weak __typeof(self)weakSelf = self;
    [manager POST:[NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, REGISTER_VERIFY_CODE_API]
       parameters:encryptParams
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              __strong __typeof(self)strongSelf = weakSelf;
              // 返回加密报文
              NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              
              // 解密返回的报文
              NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
              
              NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
              
              id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
              
              DLog(@"result = %@", result);
              
              if ([result isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *resultDict = result;
                  NSString *code = resultDict[@"code"];
                  
                  if (IsStrEmpty(code)) {
                      return;
                  }
                  
                  if ([code integerValue] == STATUS_OK ) {
                      // 存储sessionId，token
//                      NSDictionary *data = resultDict[@"data"];
//                      strongSelf.token = data[@"token"];
//                      strongSelf.desKey = desKey;
//                      strongSelf.companyId = data[@"companyId"];
                      
                      if (withSuccess) {
                          withSuccess(STATUS_OK);
                      }
                  }
                  else {
                      if (withFailure) {
                          withFailure([code integerValue]);
                      }
                  }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"Error: %@", error);
              //              if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
              //                  [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
              //                  return;
              //              }
              //              else {
              if (withFailure) {
                  withFailure(NETWORK_FAILED);
              }
              //              }
              
          }];

}


#pragma mark - 获取验证码
-(void)doGetVerifyCode:(NSString*)mobile
          success:(void(^)(NSInteger code_status))withSuccess
          failure:(void(^)(NSInteger code_status))withFailure;
{
    NSString *desKey = [EncryptUtil generateDesKey];
    NSString *params = [NSString stringWithFormat:@"mobile=%@&osType=1&udid=%@", mobile, [SystemInfo deviceUUID]];
    NSDictionary *encryptParams = [EncryptUtil encryptLoginRequest:params desKey:desKey];
//    DLog(@"encryptParams = %@", encryptParams);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    __weak __typeof(self)weakSelf = self;
    [manager POST:[NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, VERIFY_CODE_API]
       parameters:encryptParams
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              __strong __typeof(self)strongSelf = weakSelf;
              // 返回加密报文
              NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              
              // 解密返回的报文
              NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
              
              NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
              
              id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
              
              DLog(@"result = %@", result);
              
              if ([result isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *resultDict = result;
                  NSString *code = resultDict[@"code"];
                  
                  if (IsStrEmpty(code)) {
                      return;
                  }
                  
                  if ([code integerValue] == STATUS_OK ) {
                      // 存储sessionId，token
                      NSDictionary *data = resultDict[@"data"];
                      strongSelf.token = data[@"token"];
                      strongSelf.desKey = desKey;
//                      strongSelf.companyId = data[@"companyId"];
                      
                      if (withSuccess) {
                          withSuccess(STATUS_OK);
                      }
                  }
                  else {
                      if (withFailure) {
                          withFailure([code integerValue]);
                      }
                  }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"Error: %@", error);
//              if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
//                  [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
//                  return;
//              }
//              else {
                  if (withFailure) {
                      withFailure(NETWORK_FAILED);
                  }
//              }
              
          }];
}

#pragma mark - 重设密码
-(void)doResetPwd:(NSString*)params
               success:(void(^)(NSInteger code_status))withSuccess
               failure:(void(^)(NSInteger code_status))withFailure;
{
    NSString *desKey = [EncryptUtil generateDesKey];
    
    NSDictionary *encryptParams = [EncryptUtil encryptLoginRequest:params desKey:desKey];
//    DLog(@"encryptParams = %@", encryptParams);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    __weak __typeof(self)weakSelf = self;
    [manager POST:[NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, RESET_PWD_API]
       parameters:encryptParams
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              __strong __typeof(self)strongSelf = weakSelf;
              // 返回加密报文
              NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              
              // 解密返回的报文
              NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
              
              NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
              
              id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
              
              DLog(@"result = %@", result);
              
              if ([result isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *resultDict = result;
                  NSString *code = resultDict[@"code"];
                  
                  if (IsStrEmpty(code)) {
                      return;
                  }
                  
                  if ([code integerValue] == STATUS_OK ) {
                      // 存储sessionId，token
                      NSDictionary *data = resultDict[@"data"];
                      strongSelf.token = data[@"token"];
                      strongSelf.desKey = desKey;
                      
                      
                      if (withSuccess) {
                          withSuccess(STATUS_OK);
                      }
                  }
                  else {
                      if (withSuccess) {
                          withSuccess([code integerValue]);
                      }
                  }
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"Error: %@", error);
              if (withFailure) {
                  withFailure(NETWORK_FAILED);
              }
              
          }];
}

#pragma mark - 判断是否登录
-(BOOL)isLogin
{
    if (self.token != nil && self.token.length > 0) {
        return YES;
    }
    return NO;
}

-(BOOL)isGesturePwdLogin
{
    if (!IsStrEmpty(self.userModel.username) && !IsStrEmpty(self.storeUserName)  && !IsStrEmpty(self.storePassword) && [self gesturePasswordIsExit]) {
        return YES;
    }
    return NO;
}

-(void)isGesturePwdLogin:(void(^)(NSInteger hasSetGesturePwd))withBlock
{
    if ([self gesturePasswordIsExit]) {
        if (withBlock) {
            withBlock(1);
        }
    }
    else {
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, HAS_GESTURE_PWD_API];
        //    WeakSelf
        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary *responseData) {
            if (![responseData isKindOfClass:[NSDictionary class]]) {
                return;
            }
            
            id codeStatus = responseData[@"code"];
            if (IsNilOrNull(codeStatus)) {
                if (withBlock) {
                    withBlock(0);
                }
                return;
            }
            
            NSInteger code = [responseData[@"code"] integerValue];
            if (code == STATUS_OK) {
                NSString *data = responseData[@"data"];
                if ([data integerValue] == 1) {
                    if (withBlock) {
                        withBlock(1);
                    }
                }
                else {
                    if (withBlock) {
                        withBlock(0);
                    }
                }
            }
            else {
                if (withBlock) {
                    withBlock(0);
                }
            }
        } andFailure:^(NSString *errorDesc) {
            if (withBlock) {
                withBlock(0);
            }
        }];
    }

}

-(BOOL)gesturePasswordIsExit
{
    return !IsStrEmpty([[UserDefaults sharedInstance] objectForKey:KEY_GESTURE_PWD]);
}

#pragma mark - 保存token session DES密钥
-(void)setToken:(NSString *)token
{
    [[UserDefaults sharedInstance] setObject:token forKey:KEY_TOKEN];
}

-(NSString*)token
{
    return [[UserDefaults sharedInstance] objectForKey:KEY_TOKEN];
}

-(void)setDesKey:(NSString *)desKey
{
    [[UserDefaults sharedInstance] setObject:desKey forKey:KEY_DES_KEY];
}



-(NSString*)desKey
{
    return [[UserDefaults sharedInstance] objectForKey:KEY_DES_KEY];
}

- (void)setStoreUserName:(NSString *)storeUserName
{
    if (IsStrEmpty(storeUserName)) {
        return;
    }
    
    [SFHFKeychainUtils storeUsername:kLoginUserNameKey andPassword:storeUserName forServiceName:kKeychainServiceNameSuffix updateExisting:YES error:nil];
}

- (NSString*)storeUserName
{
    NSString *userName = [SFHFKeychainUtils getPasswordForUsername:kLoginUserNameKey andServiceName:kKeychainServiceNameSuffix error:nil];
    return userName;
}

- (void)setStorePassword:(NSString *)storePassword
{
    if (IsStrEmpty(storePassword)) {
        return;
    }
    
    [SFHFKeychainUtils storeUsername:kLoginPasswdKey andPassword:storePassword forServiceName:kKeychainServiceNameSuffix updateExisting:YES error:nil];
    
}

- (NSString*)storePassword
{
    NSString *passwordPlain = [SFHFKeychainUtils getPasswordForUsername:kLoginPasswdKey andServiceName:kKeychainServiceNameSuffix error:nil];
    return passwordPlain;
}

#pragma mark - 第三方登录信息

- (void)setThirdPartyId:(NSString *)thirdPartyId
{
    if (IsStrEmpty(thirdPartyId)) {
        [[UserDefaults sharedInstance] removeObjectForKey:KEY_THIRDPARTY_ID];
    }
    else {
        [[UserDefaults sharedInstance] setObject:thirdPartyId forKey:KEY_THIRDPARTY_ID];
    }
}

- (NSString*)thirdPartyId
{
    return [[UserDefaults sharedInstance] objectForKey:KEY_THIRDPARTY_ID];
}

- (void)setThirdPartyLoginParams:(NSString *)thirdPartyLoginParams
{
    if (thirdPartyLoginParams == nil) {
        return;
    }
    
    [SFHFKeychainUtils storeUsername:kLoginUserNameKey andPassword:thirdPartyLoginParams forServiceName:kKeychainServiceNameSuffix updateExisting:YES error:nil];
}


-(NSString*)thirdPartyLoginParams
{
    NSString *userName = [SFHFKeychainUtils getPasswordForUsername:kLoginUserNameKey andServiceName:kKeychainServiceNameSuffix error:nil];
    return userName;
}


#pragma mark - 取出token session DES密钥

-(BOOL)isBindMailbox
{
    return !IsStrEmpty(self.userModel.email);
//    return !IsStrEmpty(self.mailBox);
}

#pragma mark - UserModel
-(void)setUserModel:(UserModel *)userModel
{
    [[UserDefaults sharedInstance] setObject:userModel.toJSONData forKey:KEY_USER_MODEL];
}

-(UserModel*)userModel;
{
    NSData *data = [[UserDefaults sharedInstance] objectForKey:KEY_USER_MODEL];
    UserModel *userModel = [[UserModel alloc] initWithData:data error:nil];
    return userModel;
}

#pragma mark - 区分是否自派外派
-(BOOL)isSelfDispatch:(NSString*)companyId;
{
    return [self.userModel.companyId isEqualToString:companyId];
//    return [self.companyId isEqualToString:companyId];
}


#pragma mark - 退出登录
-(void)doLogout;
{
    [[UserDefaults sharedInstance] removeObjectForKey:KEY_TOKEN];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_SESSION];
    [[UserDefaults sharedInstance] removeObjectForKey:KEY_DES_KEY];
    [[AppConfig currentConfig] setCarLastUpdateStr:nil];
    [[AppConfig currentConfig] setPartnerLastUpdateStr:nil];
    
    EMError *error = [[EMClient sharedClient] logout:YES];
    if (!error) {
//#if DEBUG
//        [MsgToolBox showToast:@"退出环信成功"];
//#endif
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kOnLogoutNotification object:nil];
    
//    [[Config currentCofnig] removeObject]
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_COMPANY_ID];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_GESTURE_PWD];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_USER_NAME];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_USER_PWD];

}

-(void)doRemoveGesturePwd
{
//    [self doLogout];
    [[UserDefaults sharedInstance] removeObjectForKey:KEY_GESTURE_PWD];
}

-(void)doRemoveAll
{
    [[UserDefaults sharedInstance] removeObjectForKey:KEY_TOKEN];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_SESSION];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_COMPANY_ID];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_GESTURE_PWD];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_USER_NAME];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_SHA_BODY];
//    [[UserDefaults sharedInstance] removeObjectForKey:KEY_RSA_BODY];
}

-(NSInteger)userLevel
{
    return [self.userModel.level integerValue];
}


@end
