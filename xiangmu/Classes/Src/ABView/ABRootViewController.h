//
//  ABRootViewController.h
//  Nav
//
//  Created by zhuo li on 11-5-10.
//  Copyright 2011 chiefree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h> 
#import <AddressBookUI/AddressBookUI.h> 
#import <UIKit/UIKit.h>
//#import "CallViewController.h"

@protocol ABRootViewControllerDelegate <NSObject>

-(void)onGetContactPhone:(NSString*)phone;

@end

@interface ABRootViewController : ABPeoplePickerNavigationController <ABPeoplePickerNavigationControllerDelegate>

@property (nonatomic, assign) id<ABRootViewControllerDelegate> controllerDelegate;

@end

