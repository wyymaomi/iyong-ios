//
//  ProviderDetailModel.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>



@interface ProviderDetailModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *blurb;
//@property (nonatomic, strong) NSString<Optional> *business;
@property (nonatomic, strong) NSString<Optional> *collectionQuantity;
@property (nonatomic, strong) NSNumber<Optional> *collection;
@property (nonatomic, strong) NSString<Optional> *thumbsQuantity;
@property (nonatomic, strong) NSNumber<Optional> *thumbsUp;
@property (nonatomic, strong) NSNumber<Optional> *thumbsTime;
@property (nonatomic, strong) NSNumber<Optional> *companyScor;
@property (nonatomic, strong) NSString<Optional> *browseQuantity;
@property (nonatomic, strong) NSString<Optional> *commentQuantity;
@property (nonatomic, strong) NSString<Optional> *companyName;
@property (nonatomic, strong) NSString<Optional> *logoUrl;
@property (nonatomic, strong) NSString<Optional> *mobile;
@property (nonatomic, strong) NSArray<Optional> *imgs;
@property (nonatomic, strong) NSArray<Optional> *business;// 经营范围

- (NSString*)displayDescriptionText;

@end
