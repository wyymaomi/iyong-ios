//
//  MyAccountTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AccountInfoTableView.h"
//#import "CommunityHeaderTableViewCell.h"
#import "AccountHeaderTableViewCell.h"
#import "MeTableViewCell.h"
//#import "CommunityCateogryTableViewCell.h"
//#import "OrderStatusTableViewCell.h"
#import "AppConfig.h"

@interface AccountInfoTableView ()

@property (nonatomic, strong) NSArray *cellList;
@property (nonatomic, strong) UIImageView *reddotImageView;

@end

@implementation AccountInfoTableView

- (NSArray*)cellList
{
    if (_cellList == nil) {
//        _cellList = @[@{@"title":@"我的询价", @"icon":@"icon_my_inquiry"},
//                      @{@"title":@"我的收藏", @"icon":@"icon_gray_star"},
//                      @{@"title":@"我的行程", @"icon":@"icon_my_travel"},
//                      @{@"title":@"我的行程", @"icon":@"icon_my_travel"},
//                      @{@"title":@"设置", @"icon":@"icon_my_settings"},
//                      @{@"title":@"消息", @"icon":@"icon_message"}];
//        _cellList = @[@{@"title": @"我的收藏", @"icon":@"icon_my_favorite"},
//                      @{@"title": @"我的账户", @"icon":@"icon_my_account"},
//                      @{@"title": @"我的设置", @"icon": ]
    }
    return _cellList;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    [super setup];
    
    self.backgroundColor = [UIColor whiteColor];
    self.alpha = 1;
    
    self.scrollEnabled = YES;
    
    self.delegate = self;
    self.dataSource = self;
    
    self.tableFooterView = [UIView new];
    self.rowHeight = 44;
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return 5;
//    return 7;
//    return 8;
    return 9;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
//        return 154;
        return [AccountHeaderTableViewCell getCellHeight];
    }
    return 55*Scale;
}
                      
    

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == AccountInfoTableRowAvatar) {
        
        AccountHeaderTableViewCell *cell = [AccountHeaderTableViewCell cellWithTableView:tableView];
        [cell.editUserInfoButton addTarget:self action:@selector(onClickEditUserInfoButton) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    
    
//    if (indexPath.row == AccountInfoTableRowMyInquiry) {
//        
//        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
//        
//        cell.titleLabel.text = @"询价";
//        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_inquiry"];
//        
//        
////        CommunityCateogryTableViewCell *cell = [CommunityCateogryTableViewCell cellWithTableView:tableView];
////        cell.backgroundColor = [UIColor clearColor];
////        cell.titleLabel.text = @"我的询价";
////        cell.titleLabel.font = FONT(14);
////        cell.titleLabel.textColor = [UIColor darkTextColor];
////        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_inquiry"];
////        
////        if (indexPath.section == 0 && indexPath.row == 0) {
//            NSInteger msgCount = [[AppConfig currentConfig].businessCircleNumber integerValue];
//            if (msgCount > 0) {
//                cell.reddotImageView.hidden = NO;
//            }
//            else {
//                cell.reddotImageView.hidden = YES;
//            }  
////        }
//        
//        return cell;
//        
//    }
    
//    if (indexPath.row == AccountInfoTableRowMyTravel) {
//        
//        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
//        
//        cell.titleLabel.text = @"行程";
//        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_travel"];
//        
////        OrderStatusTableViewCell *cell = [OrderStatusTableViewCell cellWithTableView:tableView];
////        cell.backgroundColor = [UIColor clearColor];
////        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_travel"];
////        cell.titleLabel.text = @"我的行程";
////        cell.titleLabel.font = FONT(14);
////        cell.titleLabel.textColor = [UIColor darkTextColor];
////        NSString *badgeNumStr = [NSString stringWithFormat:@"%ld", (long)self.orderCount];
////        if (IsStrEmpty(badgeNumStr)|| [badgeNumStr isEqualToString:@"0"]) {
////            cell.badgeButton.hidden = YES;
////            cell.badgeButton.alpha = 0;
////        }
////        else {
////            [cell.badgeButton setTitle:badgeNumStr forState:UIControlStateNormal];
////            cell.badgeButton.hidden = NO;
////        }
//        
//        return cell;
//        
//    }
    
    if (indexPath.row == AccountInfoTableRowSettings) {
        
        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
        
        cell.titleLabel.text = @"我的设置";
        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_settings"];
        
//        OrderStatusTableViewCell *cell = [OrderStatusTableViewCell cellWithTableView:tableView];
//        cell.backgroundColor = [UIColor clearColor];
//        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_settings"];
//        cell.titleLabel.text = @"设置";
//        cell.titleLabel.font = FONT(14);
//        cell.titleLabel.textColor = [UIColor darkTextColor];
//        cell.badgeButton.hidden = YES;
        
        return cell;
    }
    
    if (indexPath.row == AccountInfoTableRowMyAccount) {
        
        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
        
        cell.titleLabel.text = @"我的账户";
        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_account"];
        
//        OrderStatusTableViewCell *cell = [OrderStatusTableViewCell cellWithTableView:tableView];
//        cell.backgroundColor = [UIColor clearColor];
//        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_account"];
//        cell.titleLabel.text = @"我的帐户";
//        cell.titleLabel.font = FONT(14);
//        cell.titleLabel.textColor = [UIColor darkTextColor];
//        cell.badgeButton.hidden = YES;
        
        return cell;
        
    }
    
    if (indexPath.row == AccountInfoTableRowMyFavorite) {
        
        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
        
        cell.titleLabel.text = @"我的收藏";
        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_favorite"];
        
//        CommunityCateogryTableViewCell *cell = [CommunityCateogryTableViewCell cellWithTableView:tableView];
//        cell.backgroundColor = [UIColor clearColor];
//        cell.titleLabel.text = @"我的收藏";
//        cell.titleLabel.font = FONT(14);
//        cell.titleLabel.textColor = [UIColor darkTextColor];
//        cell.iconImageView.image = [UIImage imageNamed:@"icon_gray_star"];
//        cell.redDotImageView.hidden = YES;
        
        //        if (indexPath.section == 0 && indexPath.row == 0) {
//        NSInteger msgCount = [[AppConfig currentConfig].businessCircleNumber integerValue];
//        if (msgCount > 0) {
//            cell.redDotImageView.hidden = NO;
//        }
//        else {
//            cell.redDotImageView.hidden = YES;
//        }
        //        }
        
        return cell;
        
        
    }
    
    if (indexPath.row == AccountInfoTableRowMessage) {
        
        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
        
        self.reddotImageView = cell.reddotImageView;
        
        cell.titleLabel.text = @"我的消息";
        
        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_message"];
//        cell.iconImageView.userInteractionEnabled = YES;
//        cell.reddotImageView.hidden = self.messageCount > 0 ? NO : YES;
        
        if (self.messageCount > 0) {
            cell.reddotImageView.hidden = NO;
        }
        else {
            cell.reddotImageView.hidden = YES;
        }
        
        return cell;
        
    }
    
    if (indexPath.row == AccountInfoTableRowFinance) {
        
        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
        
        cell.titleLabel.text = @"金融服务";
        
        cell.iconImageView.image = [UIImage imageNamed:@"icon_finance"];
        
        return cell;
        
    }
    
    if (indexPath.row == AccountInfoTableRowCompanyMode) {
        
        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
        
        cell.titleLabel.text = @"切换企业模式";
        
        cell.iconImageView.image = [UIImage imageNamed:@"icon_switch_company"];
        
        return cell;
    }
    
    if (indexPath.row == AccountInfoTableRowMyTravel) {
        
        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
        
        cell.titleLabel.text = @"我的行程";
        
        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_car_order"];
        
//        cell.iconImageView.
        return cell;
    }
    
    if (indexPath.row == AccountInfoTableRowContactService) {
        
        MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
        
        cell.titleLabel.text = @"联系客服";
        
        cell.iconImageView.image = [UIImage imageNamed:@"icon_contact_service"];
        
        return cell;
        
        
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_tableViewDelegate && [_tableViewDelegate respondsToSelector:@selector(didSelectRow:)]) {
        [_tableViewDelegate didSelectRow:indexPath.row];
    }
}

-(void)onClickEditUserInfoButton
{
    if (_tableViewDelegate && [_tableViewDelegate respondsToSelector:@selector(didSelectRow:)]) {
        [_tableViewDelegate didSelectRow:0];
    }
}

//-(void)onClick

@end
