//
//  IdleShareTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "IdleShareTableViewCell.h"

@implementation IdleShareTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    
    static NSString *ID =  @"IdleShareTableViewCell";
    IdleShareTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    return cell;
}

+ (CGFloat)getCellHeight
{
    return 274;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
