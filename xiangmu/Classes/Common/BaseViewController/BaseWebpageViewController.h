//
//  BaseWebpageViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/13.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseWebpageViewController : BaseViewController

@property (nonatomic, strong) NSString *url;

@end
