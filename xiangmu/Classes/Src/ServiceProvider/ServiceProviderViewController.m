//
//  ServiceProviderViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ServiceProviderViewController.h"
#import "BusinessHttpMessage.h"
#import "Additional.h"
#import "CameraPhotoService.h"
#import "GTMBase64.h"
#import "MsgToolBox.h"
#import "DefineConstants.h"
#import "ResourceConstants.h"
#import "BusinessInfoModel.h"
#import "AliyunOSSManager.h"
#import "AppConfig.h"

@interface ServiceProviderViewController ()<CameraPhotoServiceDelegate, UITextViewDelegate>

@property (nonatomic, strong) BusinessHttpMessage *businessHttpMessage;
@property (nonatomic, strong) ExperienceHttpMessage *experienceHttpMessage;
@property (nonatomic, strong) CameraPhotoService *cameraPhotoService;
//@property (nonatomic, strong) NSMutableArray *downloadImageDataArray;// 下载的数据
@property (nonatomic, strong) NSMutableArray *imageDataArray;
//@property (nonatomic, strong) NSMutableDictionary *uploadImageDict;

@end

@implementation ServiceProviderViewController

//- (NSMutableDictionary*)uploadImageDict
//{
//    if (_uploadImageDict == nil) {
//        _uploadImageDict = [NSMutableDictionary new];
//    }
//    return _uploadImageDict;
//}

- (NSMutableArray*)imageDataArray
{
    if (_imageDataArray == nil) {
        _imageDataArray = [NSMutableArray arrayWithCapacity:4];
    }
    return _imageDataArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    self.title = @"服务商认证";
    
    if (_type == PublishTypeBusiness) {
        self.textView.placeholder = @"请输入经营范围";
    }
    else if (_type == PublishTypeUserExperience) {
        self.textView.placeholder = @"请输入用户体验";
    }

    [self.okButton blueStyle];
    [self.okButton setTitle:@"保存/发布" forState:UIControlStateNormal];
    [self.okButton addTarget:self action:@selector(onClickOKButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.imageView1.tag = ImageViewIndex1;
    self.imageView2.tag = ImageViewIndex2;
    self.imageView3.tag = ImageViewIndex3;
    self.imageView4.tag = ImageViewIndex4;
    
    [self.imageView1 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.imageView2 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.imageView3 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.imageView4 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    
    if (_type == PublishTypeBusiness) {
        [self getBusinessInfo];
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 点击图片

- (void)onClickImageView:(id)sender
{
    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer *)sender;
    UIImageView *imageView = (UIImageView*)[singleTap view];
    
    [self.cameraPhotoService show:imageView.tag];
    
}

#pragma mark - CameraPhotoServiceDelegate

- (void)onGetImageData:(NSData*)imageData tag:(NSInteger)tag;
{
    UIImageView *imageView = [self.view viewWithTag:tag];
    imageView.image = [UIImage imageWithData:imageData];

//    [self.uploadImageDict setObject:imageData forKey:[NSNumber numberWithInteger:tag]];
//    self.imageDataDict[@tag] = imageData;
    
    NSInteger index = tag / 1000 - 1;
    self.imageDataArray[index] = imageData;
}

- (void)getBusinessInfo
{
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SERVICER_GET_API];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary * responseData) {
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        NSDictionary *data = responseData[@"data"];
        
        if (code_status == STATUS_OK) {
            
            BusinessInfoModel *businessInfoModel = [[BusinessInfoModel alloc] initWithDictionary:data error:nil];
            
            weakSelf.textView.text = businessInfoModel.business;
            
//            if (businessInfoModel.img1.trim.length > 0) {
//                [weakSelf.imageView1 downloadImageFromAliyunOSS:businessInfoModel.img1 isThumbnail:NO placeholderImage:VechilePlaceholderImage success:^(id responseData) {
//                    weakSelf.imageDataArray[0] = responseData;
//                } andFailure:^(NSString *errorDesc) {
//  
//                }];
//            }
//            
//            if (businessInfoModel.img2.trim.length > 0) {
//                [weakSelf.imageView2 downloadImageFromAliyunOSS:businessInfoModel.img2 isThumbnail:NO placeholderImage:VechilePlaceholderImage success:^(id responseData) {
//                    weakSelf.imageDataArray[1] = responseData;
//                } andFailure:^(NSString *errorDesc) {
//                    
//                }];
//            }
//            
//            if (businessInfoModel.img3.trim.length > 0) {
//                [weakSelf.imageView3 downloadImageFromAliyunOSS:businessInfoModel.img3 isThumbnail:NO placeholderImage:VechilePlaceholderImage success:^(id responseData) {
//                    weakSelf.imageDataArray[2] = responseData;
//                } andFailure:^(NSString *errorDesc) {
//                    
//                }];
//            }
//            
//            if (businessInfoModel.img4.trim.length > 0) {
//                [weakSelf.imageView4 downloadImageFromAliyunOSS:businessInfoModel.img4 isThumbnail:NO placeholderImage:VechilePlaceholderImage success:^(id responseData) {
//                    weakSelf.imageDataArray[3] = responseData;
//                } andFailure:^(NSString *errorDesc) {
//                    
//                }];
//            }
            
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
    
    
}

- (void)onClickOKButton:(id)sender
{
    if (_type == PublishTypeUserExperience) {
        self.experienceHttpMessage.content = self.textView.text.trim;
        if (self.experienceHttpMessage.content.length == 0) {
            [MsgToolBox showToast:@"请填写用户体验"];
            return;
        }
    }
    else if (_type == PublishTypeBusiness) {
        self.businessHttpMessage.business = self.textView.text.trim;
    }
    
    
//    if (self.businessHttpMessage.business.length == 0) {
//        [MsgToolBox showToast:@"请填写经营范围"];
//        return;
//    }
    
    self.nextAction = @selector(onClickOKButton:);
    self.object1 = nil;
    self.object2 = nil;
    
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    
    if (self.type == PublishTypeBusiness) {
        [self publishCompanyBusiness];
    }
    else {
        [self publishUserExperience];
    }
    
    
}

//- (NSMutableArray*)imageDataArray
//{
//    NSMutableArray *array = [NSMutableArray new];
//    [array addObject:IsNilOrNull(self.uploadImageDict[@1000])?[NSData new]:self.uploadImageDict[@1000]];
//    [array addObject:IsNilOrNull(self.uploadImageDict[@2000])?[NSData new]:self.uploadImageDict[@2000]];
//    [array addObject:IsNilOrNull(self.uploadImageDict[@3000])?[NSData new]:self.uploadImageDict[@3000]];
//    [array addObject:IsNilOrNull(self.uploadImageDict[@4000])?[NSData new]:self.uploadImageDict[@4000]];
//    
//    return array;
//}

- (void)publishUserExperience
{
    
//    self.experienceHttpMessage
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_SAVE_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在上传中"];
    
    WeakSelf
    
    if (self.imageDataArray.count == 0) {
        
        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:self.experienceHttpMessage.description success:^(id responseData) {
            
            StrongSelf
            
            [strongSelf hideHUDIndicatorViewAtCenter];
            [strongSelf resetAction];
            
            NSInteger code_status = [responseData[@"code"] integerValue];
            
            if (code_status == STATUS_OK) {
                
                [YYAlertView showAlertView:@"" message:@"发布用户体验成功，请返回" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                    
                }];
            }
            else {
                [MsgToolBox showToast:getErrorMsg(code_status)];
            }
            
        } andFailure:^(NSString *errorDesc) {
            
            [weakSelf resetAction];
            [weakSelf hideHUDIndicatorViewAtCenter];
            
        }];
        return;
    }
    
//    NSMutableArray *imageDataArray = [self imageDataArray];
    [[AliyunOSSManager sharedInstance] uploadImages:self.imageDataArray isAsync:YES folderName:kAliyunOSSPublichImagePath([UserManager sharedInstance].userModel.companyId) complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        if (state == UploadImageSuccess) {
            
            StrongSelf
            
//            NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_SAVE_API];
            
//            strongSelf.experienceHttpMessage.img1 = [StringUtil getSafeString:names[0]];
//            strongSelf.experienceHttpMessage.img2 = [StringUtil getSafeString:names[1]];
//            strongSelf.experienceHttpMessage.img3 = [StringUtil getSafeString:names[2]];
//            strongSelf.experienceHttpMessage.img4 = [StringUtil getSafeString:names[3]];
            
            [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:strongSelf.experienceHttpMessage.description success:^(id responseData) {
                
                [strongSelf hideHUDIndicatorViewAtCenter];
                [strongSelf resetAction];
                
                NSInteger code_status = [responseData[@"code"] integerValue];
                
                if (code_status == STATUS_OK) {
                    
                    [YYAlertView showAlertView:@"" message:@"发布用户体验成功，请返回" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                        if (buttonIndex == 0) {
                            [strongSelf.navigationController popViewControllerAnimated:YES];
                        }
                        
                    }];
                }
                else {
                    [MsgToolBox showToast:getErrorMsg(code_status)];
                }
                
            } andFailure:^(NSString *errorDesc) {
                
                [weakSelf resetAction];
                [weakSelf hideHUDIndicatorViewAtCenter];
                
            }];
        }
        
    }];
    
    
}

- (void)publishCompanyBusiness
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SERVICER_SAVE_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在上传中"];
    
    WeakSelf
    
    if (self.imageDataArray.count == 0) {
        
        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:self.businessHttpMessage.description success:^(id responseData) {
            
            StrongSelf
            
            [strongSelf hideHUDIndicatorViewAtCenter];
            [strongSelf resetAction];
            
            NSInteger code_status = [responseData[@"code"] integerValue];
            
            if (code_status == STATUS_OK) {
                
                [YYAlertView showAlertView:@"" message:@"发布公司经营范围成功，请返回" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                }];
                
            }
            else {
                [MsgToolBox showToast:getErrorMsg(code_status)];
            }
            
        } andFailure:^(NSString *errorDesc) {
            
            [weakSelf resetAction];
            [weakSelf hideHUDIndicatorViewAtCenter];
            
        }];
        
        return;
    }
    
    [[AliyunOSSManager sharedInstance] uploadImages:self.imageDataArray isAsync:YES folderName:kAliyunOSSPublichImagePath([UserManager sharedInstance].userModel.companyId) complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        if (state == UploadImageSuccess) {
            
            StrongSelf
            
//            strongSelf.businessHttpMessage.img1 = [StringUtil getSafeString:names[0]];
//            strongSelf.businessHttpMessage.img2 = [StringUtil getSafeString:names[1]];
//            strongSelf.businessHttpMessage.img3 = [StringUtil getSafeString:names[2]];
//            strongSelf.businessHttpMessage.img4 = [StringUtil getSafeString:names[3]];
            
            [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:strongSelf.businessHttpMessage.description success:^(id responseData) {
                
                [strongSelf hideHUDIndicatorViewAtCenter];
                [strongSelf resetAction];
                
                NSInteger code_status = [responseData[@"code"] integerValue];
                
                if (code_status == STATUS_OK) {
                    
                    [YYAlertView showAlertView:@"" message:@"发布公司经营范围成功，请返回" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                        if (buttonIndex == 0) {
                            [strongSelf.navigationController popViewControllerAnimated:YES];
                        }
                        
                    }];
                }
                else {
                    [MsgToolBox showToast:getErrorMsg(code_status)];
                }
                
            } andFailure:^(NSString *errorDesc) {
                
                [weakSelf resetAction];
                [weakSelf hideHUDIndicatorViewAtCenter];
                
            }];
        }
        
    }];
    
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location >= 500) {
        [MsgToolBox showToast:ALERT_MSG_BUSINESS_CONTENT_LIMIT];
        return NO;
    }
    
    return YES;
    
}

#pragma mark - getter and setter

- (BusinessHttpMessage*)businessHttpMessage
{
    if (_businessHttpMessage == nil) {
        _businessHttpMessage = [BusinessHttpMessage new];
    }
    return _businessHttpMessage;
}

- (ExperienceHttpMessage*)experienceHttpMessage
{
    if (!_experienceHttpMessage) {
        _experienceHttpMessage = [ExperienceHttpMessage new];
        _experienceHttpMessage.areaCode = [AppConfig currentConfig].gpsCityCode;
    }
    return _experienceHttpMessage;
}

-  (CameraPhotoService*)cameraPhotoService
{
    if (_cameraPhotoService == nil) {
        _cameraPhotoService = [[CameraPhotoService alloc] initWithViewController:self.navigationController type:CameraPhotoLarge];
        _cameraPhotoService.delegate = self;
    }
    return _cameraPhotoService;
}



@end
