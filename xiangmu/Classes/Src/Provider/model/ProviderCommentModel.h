//
//  ProviderCommentModel.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol ProviderCommentModel <NSObject>

@end

@interface ProviderCommentModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *topicId;// 评论ID 公司ID
@property (nonatomic, strong) NSString<Optional> *commenterCompanyId;// 评论者公司ID
@property (nonatomic, strong) NSString<Optional> *commenterLogoUrl;// 评论者头像
@property (nonatomic, strong) NSString<Optional> *content; // 评论内容
@property (nonatomic, strong) NSString<Optional> *createTime; // 评论时间
@property (nonatomic, strong) NSString<Optional> *nickname;

- (NSString*)displayTime;

@end

@interface ProviderCommentResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<ProviderCommentModel, Optional> *data;

@end
