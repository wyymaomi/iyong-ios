//
//  AdPurchaseDetailTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdPurchaseDetailTableView : UITableView<UITableViewDelegate, UITableViewDataSource>

@end
