//
//  OrderCountModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@interface OrderCountModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *arrangeCar;
@property (nonatomic, strong) NSNumber<Optional> *companyId;
@property (nonatomic, strong) NSNumber<Optional> *completed;
@property (nonatomic, strong) NSNumber<Optional> *fillWaybill;
@property (nonatomic, strong) NSNumber<Optional> *notBargain;
@property (nonatomic, strong) NSNumber<Optional> *stroke;
@property (nonatomic, strong) NSNumber<Optional> *unpaid;
@property (nonatomic, strong) NSNumber<Optional> *untreated;

@end
