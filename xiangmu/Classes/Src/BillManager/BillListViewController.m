//
//  BillListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BillListViewController.h"
#import "PullRefreshTableView.h"
#import "BillTableViewCell.h"
#import "BillHeaderView.h"
#import "OrderManagerViewController.h"
#import "MJRefresh.h"

@interface BillListViewController ()<UITableViewDelegate,UITableViewDataSource,  PullRefreshTableViewDelegate, BillTableViewCellDelegate, EmptyDataDelegate>

//@property (nonatomic, strong) PullRefreshTableView *tableView;

@end

@implementation BillListViewController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view addSubview:self.pullRefreshTableView];
    self.pullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.emptyDataDelegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.pullRefreshTableView beginHeaderRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PullRefreshDelegate method

-(void)onHeaderRefresh;
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, _url];
    NSString *params = @"time=";
    self.pullRefreshTableView.footerHidden = YES;
    self.loadStatus = LoadStatusLoading;
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        strongSelf.loadStatus = LoadStatusFinish;
        
        BillResponseData *billResponseData = [[BillResponseData alloc] initWithDictionary:responseData error:nil];
        
        if ([billResponseData.code integerValue] == STATUS_OK) {
            
            if (!IsArrEmpty(billResponseData.data)) {
                strongSelf.dataList  = billResponseData.data;
                strongSelf.pullRefreshTableView.footerHidden = NO;
                [strongSelf.pullRefreshTableView reloadData];
            }

        }
        else {
            [strongSelf showAlertView:[self getErrorMsg:[billResponseData.code integerValue]]];
        }
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf resetAction];
        strongSelf.loadStatus = LoadStatusNetworkError;
    }];
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        return;
    }
    
    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    BillOrderModel *billModel = self.dataList.lastObject;
    NSString *params = [NSString stringWithFormat:@"time=%@", [billModel.createTime stringValue]];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, _url];
    
    self.pullRefreshTableView.footerHidden = YES;
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        BillResponseData *billResponseData = [[BillResponseData alloc] initWithDictionary:responseData error:nil];
        if ([billResponseData.code integerValue] == STATUS_OK) {
            if (!IsArrEmpty(billResponseData.data)) {
                [strongSelf.dataList addObjectsFromArray:billResponseData.data];
                [strongSelf.pullRefreshTableView reloadData];
            }
        }
        else {
            [strongSelf showAlertView:[self getErrorMsg:[billResponseData.code integerValue]]];
        }
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf resetAction];
    }];
}

- (NSString*)emptyTitleText
{
    return @"";
}

- (NSString*)emptyDetailText;
{
    return @"暂时没有该类账单数据";
}


#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BillTableViewCell getCellHeight];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 41;
//}

//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
//{
//    BillHeaderView *headerView = [BillHeaderView customView];
//    BillOrderModel *billModel = self.dataList[section];
//    headerView.statusLabel.text = self.title;
//    headerView.moenyLabel.text = [NSString stringWithFormat:@"%@金额：%@", self.title,  billModel.price];
//    return headerView;
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BillTableViewCell *cell = [BillTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.delegate = self;
    BillOrderModel *billModel = self.dataList[indexPath.section];
    [cell initData:billModel type:self.title];
//    cell.orderStatusLabel.text = self.title
    return cell;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    [self gotoOrderDetailPage:indexPath.section];
//    
//}



//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

-(void)onSelectRowAtIndex:(NSUInteger)rowIndex;
{
    [self gotoOrderDetailPage:rowIndex];
}

-(void)gotoOrderDetailPage:(NSInteger)index
{
    BillOrderModel *billModel = self.dataList[index];
    NSString *params = [NSString stringWithFormat:@"orderId=%@", billModel.orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_DETAIL_API];
    
    [self showHUDIndicatorViewAtCenter:MSG_ORDER_DETAIL];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        id code = responseData[@"code"];
        if (IsNilOrNull(code)) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *array = responseData[@"data"];
            
            if (!IsArrEmpty(array)) {
                
                NSInteger orderStatus = 0;
                OrderManagerViewController *vc = [[OrderManagerViewController alloc] init];
                
                for (NSDictionary *dict in array) {
                    
                    NSUInteger status = [dict[@"status"] integerValue];
                    NSDictionary *vo = dict[@"vo"];
                    if (status == 1) {
                        vc.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusNew;
                    }
                    if (status == 2) {
                        vc.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusDispatch;
                    }
                    if (status == 3) {
                        vc.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusVechileArrange;
                    }
                    if (status == 4) {
                        vc.wayBillModel = [[WayBillResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusWayBill;
                    }
                    if (status == 5) {
                        vc.barginAndPayModel = [[BarginAndPayResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusBarginAndPay;
                    }
                }
                
                vc.source_from = SOURCE_FROM_ORDER_LIST;
                vc.orderStatus = orderStatus;
                vc.orderMode = OrderModeDetail;
                vc.orderId = billModel.orderId;
                vc.title = @"结算支付";
                vc.hidesBottomBarWhenPushed = YES;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
