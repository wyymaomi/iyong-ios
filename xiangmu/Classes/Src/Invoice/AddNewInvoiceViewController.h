//
//  AddNewInvoiceViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "InvoiceinfoHttpMessage.h"



@interface AddNewInvoiceViewController : BaseViewController

@property (nonatomic, strong) InvoiceinfoHttpMessage *invoiceinfoHttpMessage;

@end
