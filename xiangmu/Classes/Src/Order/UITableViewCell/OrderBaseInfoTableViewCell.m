//
//  OrderBaseInfoTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderBaseInfoTableViewCell.h"
#import "CustomActionSheetDatePicker.h"

@implementation OrderBaseInfoTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"CellIdentifier";
    OrderBaseInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0xDEDEDE);
        cell.startLocationTextField.tag = OrderTextFieldStartLocation;
        cell.itineraryTextView.tag = OrderTextFieldItinerary;
        cell.memoTextView.tag = OrderTextFieldRemark;
        cell.telephoneButton.lineColor = UIColorFromRGB(0x00A04E);
    }
    return cell;
}

-(void)setDateViewStatus:(NSInteger)status
{
    switch (status) {
        case OrderInfoTypeNew:
            self.carDateLabel.userInteractionEnabled = YES;
            self.carDateView.borderWidth = 1.0f;
            self.arrowImageView.hidden = NO;
            break;
        case OrderInfoTypeBrowsableAndEditable:
            self.carDateLabel.userInteractionEnabled = NO;
            self.carDateView.borderWidth = 0.0f;
            self.arrowImageView.hidden = YES;
            break;
        case OrderInfoTypeBrowsableAndNoEditable:
            self.carDateLabel.userInteractionEnabled = NO;
            self.carDateView.borderWidth = 0.0f;
            self.arrowImageView.hidden = YES;
            break;
        default:
            break;
    }
}

-(void)initData:(OrderBaseInfoResponseModel*)model
{
    if (IsNilOrNull(model)) {
        return;
    }
//    self.upCompanyLabel.text = IsStrEmpty(model.upCompanyName)?@"自派":model.upCompanyName;
    if ([[UserManager sharedInstance] isSelfDispatch:model.companyId]) {
        self.upCompanyLabel.text = @"自派";
    }
    else {
        self.upCompanyLabel.text = model.upCompanyName;
    }
    self.orderIdLabel.text = [StringUtil getSafeString:model.code];
    self.orderIdLabel.hidden = NO;
    
    NSDate *carDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([model.carTime doubleValue] / 1000.0f)];
    
//    self.carDateLabel.text = [NSDate nowString];

    self.carModelLabel.text = model.model;
    self.startLocationTextField.text = model.location;
    self.customerTextField.text = model.customerName;
//    self.customerMobileTextField.text = model.customerMobile;
//    self.telephoneButton.titleLabel.text = model.customerMobile;
    [self.telephoneButton setTitle:model.customerMobile forState:UIControlStateNormal];
    self.itineraryTextView.text = model.itinerary;
    self.memoTextView.text = model.remark;
    
    [self setDateViewStatus:[model.type integerValue]];
    switch ([model.type integerValue]) {
        case OrderInfoTypeNew:
//            self.editButton.hidden = YES;
            
//            self.carDateLabel.text = model.editCarTime;
            
//            self.customerTextField.enabled = YES;
//            self.customerTextField.borderStyle = UITextBorderStyleLine;
//            self.customerTextField.borderWidth = 1;
            
//            self.customerMobileTextField.enabled = YES;
//            self.customerMobileTextField.borderStyle = UITextBorderStyleLine;
//            self.customerMobileTextField.borderWidth = 1;
            
//            self.startLocationTextField.enabled = YES;
//            self.startLocationTextField.borderStyle = UITextBorderStyleLine;
//            self.startLocationTextField.borderWidth = 1;
            
//            self.itineraryTextView.editable = YES;
//            self.memoTextView.editable = YES;
            break;
        case OrderInfoTypeBrowsableAndEditable:
            self.editButton.hidden = NO;
            self.editButton.enabled = YES;
            
            self.carDateLabel.text = [carDate stringWithDateFormat:@"yyyy-MM-dd HH:mm"];
            
            self.customerTextField.enabled = NO;
            self.customerTextField.borderStyle = UITextBorderStyleNone;
            self.customerTextField.borderWidth = 0;
            
//            self.customerMobileTextField.enabled = NO;
//            self.customerMobileTextField.borderStyle = UITextBorderStyleNone;
//            self.customerMobileTextField.borderWidth = 0;
//            self.telephoneButton.titleLabel.text
            
            self.startLocationTextField.enabled = NO;
            self.startLocationTextField.borderStyle = UITextBorderStyleNone;
            self.startLocationTextField.borderWidth = 0;
            
            self.itineraryTextView.editable = NO;
            self.memoTextView.editable = NO;
            self.itineraryTextView.borderWidth = 0;
            self.memoTextView.borderWidth = 0;
            break;
        case OrderInfoTypeBrowsableAndNoEditable:
            self.editButton.hidden = YES;
            
            self.carDateLabel.text = [carDate stringWithDateFormat:@"yyyy-MM-dd HH:mm"];
            
            self.customerTextField.enabled = NO;
            self.customerTextField.borderStyle = UITextBorderStyleNone;
            self.customerTextField.borderWidth = 0;
            
//            self.customerMobileTextField.enabled = NO;
//            self.customerMobileTextField.borderStyle = UITextBorderStyleNone;
//            self.customerMobileTextField.borderWidth = 0;
            
            self.startLocationTextField.enabled = NO;
            self.startLocationTextField.borderStyle = UITextBorderStyleNone;
            self.startLocationTextField.borderWidth = 0;
            
            self.itineraryTextView.editable = NO;
            self.memoTextView.editable = NO;
            self.itineraryTextView.borderWidth = 0;
            self.memoTextView.borderWidth = 0;
            break;
        default:
            break;
    }
    
}

//- (void)setCarDateEnabled

-(void)doSelectCarDate
{
 
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
