//
//  DispatchOrderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DispatchPriceResponseModel.h"

@protocol DispatchOrderTableViewCellDelegate<NSObject>

-(void)doGetDispatchCompany;
-(void)onGetDispatchPrice;

- (void)onDispatchRollbackBtnClick;
- (void)onAgreeRollback;

@end

@interface DispatchOrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *srcPriceTextField;//接单价格TextField
@property (weak, nonatomic) IBOutlet UITextField *editDispatchPriceTextField;// 发单价格TextField
@property (weak, nonatomic) IBOutlet UITextField *editDispatchObjectTextField;// 派单对象TextField

@property (weak, nonatomic) IBOutlet UIView *editDispatchObjectView;// 派单对象视图
@property (weak, nonatomic) IBOutlet UIView *receivePriceView; // 接单价格视图
@property (weak, nonatomic) IBOutlet UIView *editDstPriceView;// 发单价格视图

@property (weak, nonatomic) IBOutlet UILabel *srcPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dstPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dispatchCompanyLabel;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIView *dispatchPriceView;// 发单价格视图

@property (weak, nonatomic) id<DispatchOrderTableViewCellDelegate> delegate;

@property (strong, nonatomic) DispatchPriceResponseModel *model;

- (void)initData:(DispatchPriceResponseModel*)dispatchPriceModel;
//+ (instancetype)cellWithTableView:(UITableView *)tableView;
+ (instancetype)cellWithTableView:(UITableView *)tableView dispatchPriceModel:(DispatchPriceResponseModel*)dispatchPriceModel;
+ (NSUInteger)getHeight:(DispatchPriceResponseModel*)dispatchPriceMode;

@end
