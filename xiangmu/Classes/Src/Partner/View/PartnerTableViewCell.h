//
//  PartnerTableViewCell.h
//  xiangmu
//
//  Created by David kim on 16/3/24.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartnerTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *disclosureImageView;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UIButton *badgeValueBtn;

-(void)showBadgeValue:(NSString*)number;

@end
