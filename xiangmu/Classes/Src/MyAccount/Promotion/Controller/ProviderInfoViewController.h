//
//  ProviderInfoViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "ProviderInfoViewModel.h"

//#import "ProviderInfoViewController+NetworkRequest.h"

typedef NS_ENUM(NSUInteger,ProviderInfoSection)
{
    ProviderInfoSectionBalance=0,
    ProviderInfoSectionTodayDetail,
    ProviderInfoSectionRank,
    ProviderInfoSectionAdvList
};


@interface ProviderInfoViewController : BaseViewController

- (void)initData:(ProviderOrderModel*)order;

@property (nonatomic, strong) ProviderInfoViewModel *viewModel;

@property (nonatomic, strong) NSNumber *currentPomotionAccountMoney;
@property (nonatomic, strong) NSNumber *promotionAccountMoney;
@property (nonatomic, strong) NSNumber *myAccountMoney;

@end
