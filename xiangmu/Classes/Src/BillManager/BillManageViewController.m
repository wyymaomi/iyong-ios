//
//  BillManageViewController.m
//  xiangmu
//
//  Created by David kim on 16/3/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BillManageViewController.h"
#import "BillManagerTableViewCell.h"
#import "BillPayViewController.h"
//#import "PayViewController.h"
#import "BillPayViewController.h"
#import "BillListViewController.h"
#import "BBAlertView.h"



@interface BillManageViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) NSArray *urlList;
@property (nonatomic, strong) NSArray *cellList;

@end

@implementation BillManageViewController


- (NSArray*)cellList
{
    if (!_cellList) {
        
        _cellList = @[@[@{@"title":@"已收款", @"icon":@"icon_bill_receive"},
                        @{@"title":@"未收款", @"icon":@"icon_bill_unreceive"}],
                        @[@{@"title":@"已付款", @"icon":@"icon_bill_pay"},
                          @{@"title":@"未付款", @"icon":@"icon_bill_unpay"}],
                        @[@{@"title":@"信用还款", @"icon":@"icon_bill_credit_repay"}]];
    }
    return _cellList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
//    self.title = @"账单";
//    if (self.source_from == SOURCE_FROM_HOME) {
//        self.title = @"账单管理";
//    }
//    else if(self.source_from == SOURCE_FROM_TAB_CONTROL){
//        self.title = @"账单";
//    }
    
    [self.view addSubview:self.groupTableView];
//    [self.groupTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:BillManagerCellId];

//    [self createTableView];
}


-(NSArray*)urlList
{
    if (_urlList == nil) {
        _urlList = @[BILL_LIST_COLLECTED_API,
                     BILL_LIST_UNCOLLECTED_API,
                     BILL_LIST_PAID_API,
                     BILL_LIST_UNPAID_API];
    }
    return _urlList;
}

//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([self.groupTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.groupTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.groupTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.groupTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

#pragma mark---UITableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.cellList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = self.cellList[section];
    return array.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
//    return [BillManagerTableViewCell getCellHeight];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if (section==0) {
//        return 0.1;
//    }
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0001f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BillManagerTableViewCell *cell = [BillManagerTableViewCell cellWithTableView:tableView indexPath:indexPath];
    
    NSDictionary *dict = self.cellList[indexPath.section][indexPath.row];
    
    cell.titleLabel.text = dict[@"title"];
    cell.iconImageView.image = [UIImage imageNamed:dict[@"icon"]];

    
    return cell;
}
//反选
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    self.nextAction = @selector(tableView:didSelectRowAtIndexPath:);
//    self.object1 = tableView;
//    self.object2 = indexPath;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BillManagerTableViewCell *cell=(BillManagerTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section==2) {
        
        BillPayViewController *viewController = [[BillPayViewController alloc] init];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    else
    {
        NSInteger index = indexPath.section * 2 + indexPath.row;
        BillListViewController *viewController = [[BillListViewController alloc] init];
        viewController.url = self.urlList[index];
        viewController.title = cell.titleLabel.text;
        if (self.source_from == SOURCE_FROM_TAB_CONTROL) {
            viewController.hidesBottomBarWhenPushed = YES;
        }
        [self.navigationController pushViewController:viewController animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
