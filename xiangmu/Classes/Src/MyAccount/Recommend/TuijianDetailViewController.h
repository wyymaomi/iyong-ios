//
//  TuijianDetailViewController.h
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"

@protocol Tuijian <NSObject>

-(void)tuijian;

@end
@interface TuijianDetailViewController : LFNavController
@property (nonatomic,weak) id <Tuijian> delegate;
@end
