//
//  AdPurchaseViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/4/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdPurchaseViewController.h"
#import "AdvPurchaseTableViewCell.h"
#import "AdvPurchaseDetailViewController.h"


@interface AdPurchaseViewController ()<AdvPurchaseCollectionViewDelegate>

@end

@implementation AdPurchaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"推广专栏";
    
    [self setNavigationBar];
    [self.view addSubview:self.plainTableView];
    self.plainTableView.delegate = self;
    self.plainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.headerPullRefreshTableView.pullRefreshDelegate = self;
    
//    [self.headerPullRefreshTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - openBrowserPage

- (void)gotoBrowsePage
{
    NSString *url = @"http://iyong-public.oss-cn-shanghai.aliyuncs.com/iyong/ad/explain/rule.html";
    [self openWebPage:url];
}

#pragma mark - 设置导航栏

- (void)setNavigationBar
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"说明" style:UIBarButtonItemStyleDone target:self action:@selector(gotoBrowsePage)];
}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
//    return TABLE_ROW_COUNT;
    //    return self.tableViewRows;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == AdvPurchaseRowBanner) {
        return 175*Scale;
    }
    
    
    return 220*Scale;
    
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%lu%lu", (long)indexPath.section, (long)indexPath.row];
    // 广告条
    if (indexPath.row == AdvPurchaseRowBanner) {
        
        static NSString *CellIdentifier = @"BannerCellID";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = UIColorFromRGB(0x333333);
            
            _bannerView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 175*Scale)];
            //            _bannerView.autoScrollTimeInterval = 3.8;
            _bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
            _bannerView.delegate = self;
            _bannerView.displayType = SDCycleScrollViewDisplayTypeBanner;
            _bannerView.currentPageDotColor =  NAVBAR_BGCOLOR;//UIColorFromRGB(0x09a892);//[UIColor whiteColor];
            _bannerView.downloadFromAliyunOSS = NO;
//            _bannerView.placeholderImage = [UIImage imageNamed:@"img_placeholder"];
            [cell.contentView addSubview:_bannerView];
            
        }
        
        _bannerView.imageURLStringsGroup = @[@"img_banner1", @"img_banner2", @"img_banner3", @"img_banner4"];
        
        
//        NSMutableArray *advImageList = [NSMutableArray new];
//        for (NSUInteger i = 0; i < 4; i++) {
//            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, <#CGFloat width#>, <#CGFloat height#>)]
//        }
        
//        NSMutableArray *advImageList = [NSMutableArray new];
//        
//        for (AdvModel *advModel in self.viewModel.advList) {
//            [advImageList addObject:advModel.imageUrl];
//        }
//        
//        _bannerView.aliyunOSSImageArray = advImageList;
        
        return cell;
        
    }
    else {
//    if (indexPath.row == AdvPurchaseRowOnlienProvider) {
        
        static NSString *CellIdentifier = @"AdvPurchaseTableViewCell";
        
        AdvPurchaseTableViewCell *cell = (AdvPurchaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[AdvPurchaseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.collectionView.customDelegate = self;
            cell.collectionView.tag = indexPath.row;
        }
        
        if (indexPath.row == AdvPurchaseRowOnlienProvider) {
//            cell.headerView.titleLabel.text = @"在线商户";
            cell.headerView.titleLabel.text = @"全国商户";
        }
        if (indexPath.row == AdvPurchaseRowLocalArea) {
            cell.headerView.titleLabel.text = @"所属专区";
        }
        
        
        
        return cell;
        
    }
    
    return nil;
    
    
}

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index;
{
    
    WeakSelf
    [self hasUnFinishedOrder:^(NSInteger resultStatus) {
        StrongSelf
        if (resultStatus == 0) {
            AdvPurchaseDetailViewController *viewController = [[AdvPurchaseDetailViewController alloc]init];
            viewController.position = index+1;
            viewController.advType = AdvTypeBanner;
            [strongSelf.navigationController pushViewController:viewController animated:YES];
        }
        else {
            [strongSelf handleUnFinishedOrder];
        }
        
    }];
    
}

- (void)onClickItem:(NSInteger)row tag:(NSInteger)tag
{
    WeakSelf
    [self hasUnFinishedOrder:^(NSInteger resultStatus) {
        StrongSelf
        if (resultStatus == 0) {
            AdvPurchaseDetailViewController *viewControler = [[AdvPurchaseDetailViewController alloc] init];
            viewControler.position = row+1;
            if (tag == AdvPurchaseRowOnlienProvider) {
                viewControler.advType = AdvTypeOnlineProvider;
                
            }
            else if (tag == AdvPurchaseRowLocalArea) {
                viewControler.advType = AdvTypeLocalArea;
            }
            [strongSelf.navigationController pushViewController:viewControler animated:YES];
        }
        else {
            [strongSelf handleUnFinishedOrder];
        }
        
    }];
    
}

-(void)handleUnFinishedOrder
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:kMsgHasUnpayOrder delegate:nil cancelButtonTitle:STR_CANCEL otherButtonTitles:STR_OK, nil];
    WeakSelf
    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 1) {
             [weakSelf gotoPage:@"ProviderInfoViewController"];
        }
    }];
}

-(void)hasUnFinishedOrder:(void (^)(NSInteger resultStatus))completeBlock
{
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_HAS_UNFINISH_ORDER_API) requestMethod:POST params:nil success:^(id responseData) {
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSInteger result = [responseData[@"data"] integerValue];
            
            if (completeBlock) {
                completeBlock(result);
            }
        }
        else {
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
        
    }];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
