//
//  SalesQueryViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SalesFilterListViewController.h"
#import "PullRefreshTableView.h"
#import "BillTableViewCell.h"
#import "ActionSheetDatePicker.h"
#import "CarTableViewController.h"
//#import "CarModel.h"
#import "SalesModel.h"
#import "DateUtil.h"
#import "OrderManagerViewController.h"
#import "BindMailboxViewController.h"
#import "BillHeaderView.h"
#import "BBAlertView.h"


@interface SalesFilterListViewController ()<UITableViewDelegate, UITableViewDataSource, PullRefreshTableViewDelegate, CarTableViewControllDelegate, MailboxDelegate, BillTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *startTimeView;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *endTimeView;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *selectCarView;
@property (weak, nonatomic) IBOutlet UILabel *selectCarLabel;
//@property (weak, nonatomic) IBOutlet PullRefreshTableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *endOrderCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMoenyLabel;
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *filterTableView;

//@property (nonatomic, strong) NSArray *dataList;
//@property (nonatomic, strong) CarModel *carModel;
//@property (nonatomic, strong) NSString *carId;

@end

@implementation SalesFilterListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"营业查询";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"导出" style:UIBarButtonItemStyleDone target:self action:@selector(export)];
    
    self.filterTableView.delegate = self;
    self.filterTableView.dataSource = self;
    self.filterTableView.backgroundColor = [UIColor clearColor];
    
    self.startTimeLabel.text = self.startDateTime;
    self.endTimeLabel.text = self.endDateTime;
    if (_isAll) {
        self.selectCarLabel.text = @"全部";
    }
    else {
        if (self.isOuterDispatch) {
            self.selectCarLabel.text = @"外派";
        }
        else {
            if (self.carData == nil) {
                self.selectCarLabel.text = @"自派";
            }
            else {
                self.selectCarLabel.text = self.carData.number;
            }
        }
    }
    
    self.endOrderCountLabel.text = @"";
    self.totalMoenyLabel.text = @"";
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshData];
//    [self refreshData:NO];
//    [self refreshSummaryData:@0];
}

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    

}

//-(NSArray*)dataList
//{
//    if (IsNilOrNull(_dataList)) {
//        _dataList = [NSArray new];
//    }
//    return _dataList;
//}

-(void)refreshData//:(BOOL)isDefault
{
    self.nextAction = @selector(refreshData);
    self.object1 = nil;//[NSNumber numberWithBool:isDefault];
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SALES_LIST_API];
    
    NSString *startTime = self.startDateTime;
    NSString *endTime = self.endDateTime;
    
    NSString *params;
    if (_isAll) {
        params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=", startTime, endTime];
    }
    else {
        if (self.isOuterDispatch) {
            params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=1", startTime, endTime];
        }
        else {
            if (self.carData == nil) {
                params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=0", startTime, endTime];
            }
            else {
                params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=0&carId=%@", startTime, endTime, self.carData.id];
            }
        }
    }
    
    
    WeakSelf
    [self showHUDIndicatorViewAtCenter:@"正在查询，请稍候..."];
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        SalesResponseData *salesResponseData = [[SalesResponseData alloc] initWithDictionary:responseData error:nil];
        if ([salesResponseData.code integerValue] == STATUS_OK) {
            strongSelf.dataList = salesResponseData.data;
            [strongSelf.filterTableView reloadData];
            [strongSelf refreshSummaryData:@0];
        }
        else {
            [strongSelf showAlertView:[self getErrorMsg:[salesResponseData.code integerValue]]];
        }
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        //        [strongSelf.pullRefreshTableView endHeaderRefresh];
    }];
}

#pragma mark - 刷新汇总数据
-(void)refreshSummaryData:(NSNumber*)isDefault
{
    //当开始结束时间都不为空才呈现统计数据 否则无需调统计接口
    if (IsStrEmpty(self.startTimeLabel.text) || IsStrEmpty(self.endTimeLabel.text)) {
        return;
    }
    
    self.nextAction = @selector(refreshSummaryData:);
    self.object1 = isDefault;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SALES_SUMMARY_API];
    NSString *startTime = self.startDateTime;
    NSString *endTime = self.endDateTime;
    
    NSString *params;
    if (_isAll) {
        params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=", startTime, endTime];
    }
    else {
        if (self.isOuterDispatch) {
            params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=1", startTime, endTime];
        }
        else {
            if (self.carData == nil) {
                params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=0", startTime, endTime];
            }
            else {
                params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=0&carId=%@", startTime, endTime, self.carData.id];
            }
        }
    }
    
    
    WeakSelf
    [self showHUDIndicatorViewAtCenter:@"正在查询，请稍候..."];
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            SalesSummaryModel *summaryModel = [[SalesSummaryModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            strongSelf.endOrderCountLabel.text = summaryModel.count;
            strongSelf.totalMoenyLabel.text = [NSString stringWithFormat:@"¥%@",summaryModel.totalPrice];
        }
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
//        [strongSelf.tableView endHeaderRefresh];
    }];
}

-(void)export
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault1 Title:nil message:@"你确定导出数据？" customView:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定"];
    WeakSelf
    [alertView setConfirmBlock:^{
//        [weakSelf checkMailbox];
        if ([[UserManager sharedInstance] isBindMailbox]) {
            [weakSelf startExportData:[UserManager sharedInstance].userModel.email];
        }
        else {
            [weakSelf checkMailbox];
        }
    }];
    [alertView show];
}

-(void)checkMailbox
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请先绑定邮箱" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
        
        if (buttonIndex == 0) {
            BindMailboxViewController *viewController = [[BindMailboxViewController alloc] init];
            viewController.delegate = self;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        
    }];
                              
}

#pragma mark - Bind email delegate method

-(void)onBindMailboxSuccess:(NSString*)emailBox;
{
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
//    MyTableViewCell *cell = (MyTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
//    cell.right.text = emailBox;
    [self startExportData:emailBox];
    
}

-(void)startExportData:(NSString*)emailBox
{
    self.nextAction = @selector(startExportData:);
    self.object1 = emailBox;
    self.object2 = nil;
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SEND_REPORT_API];
    NSString *startTime = self.startDateTime;
    NSString *endTime = self.endDateTime;
    
    NSString *params;
    if (_isAll) {
        params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=", startTime, endTime];
    }
    else {
        if (self.isOuterDispatch) {
            params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=1", startTime, endTime];
        }
        else {
            if (self.carData == nil) {
                params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=0", startTime, endTime];
            }
            else {
                params = [NSString stringWithFormat:@"startTime=%@&endTime=%@&time=&isExpatriate=0&carId=%@", startTime, endTime, self.carData.id];
            }
        }
    }
    params = [params stringByAppendingFormat:@"&email=%@", emailBox];
    
    [self showHUDIndicatorViewAtCenter:@"正在导出数据，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        
        //        NSLog(@"response success");
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            [strongSelf showCheckDialog];
        }
        else {
            [strongSelf showAlertView:[self getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        //        NSLog(@"response failure");
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

-(void)showCheckDialog
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleCheck Title:nil message:@"导出文件将在24小时之内发送到您邮箱，请注意查收" customView:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alertView show];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alertView dismiss];
    });

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
//    return self.dataList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BillTableViewCell getCellHeight];
//    return 110;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 41;
//}
//
//
//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
//{
//
//    BillHeaderView *headerView = [BillHeaderView customView];
//    SalesModel *model = self.dataList[section];
//    headerView.statusLabel.text = @"已完成";
//    headerView.moenyLabel.text = [NSString stringWithFormat:@"已结算金额：%@", model.price];
//    return headerView;
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BillTableViewCell *cell = [BillTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.delegate = self;
    SalesModel *model = self.dataList[indexPath.row];
    [cell initSalesData:model];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.nextAction = @selector(tableView:didSelectRowAtIndexPath:);
    self.object1 = tableView;
    self.object2 = indexPath;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self gotoOrderDetailPage:indexPath.section];
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

-(void)onSelectRowAtIndex:(NSUInteger)rowIndex;
{
    [self gotoOrderDetailPage:rowIndex];
}

-(void)gotoOrderDetailPage:(NSInteger)index
{
//    OrderListItem *listItem = self.orderList[indexPath.row];
    SalesModel *model = self.dataList[index];
    NSString *params = [NSString stringWithFormat:@"orderId=%@", model.orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_DETAIL_API];
    
    [self showHUDIndicatorViewAtCenter:MSG_ORDER_DETAIL];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        id code = responseData[@"code"];
        if (IsNilOrNull(code)) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *array = responseData[@"data"];
            
            if (!IsArrEmpty(array)) {
                
                NSInteger orderStatus = 0;
                OrderManagerViewController *vc = [[OrderManagerViewController alloc] init];
                
                for (NSDictionary *dict in array) {
                    
                    NSUInteger status = [dict[@"status"] integerValue];
                    NSDictionary *vo = dict[@"vo"];
                    if (status == 1) {
                        vc.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusNew;
                    }
                    if (status == 2) {
                        vc.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusDispatch;
                    }
                    if (status == 3) {
                        vc.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusVechileArrange;
                    }
                    if (status == 4) {
                        vc.wayBillModel = [[WayBillResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusWayBill;
                    }
                    if (status == 5) {
                        vc.barginAndPayModel = [[BarginAndPayResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusBarginAndPay;
                    }
                }
                
                vc.source_from = SOURCE_FROM_ORDER_LIST;
                vc.orderStatus = orderStatus;
                vc.orderMode = OrderModeDetail;
                vc.orderId = model.orderId;
                vc.title = @"结算支付";
                vc.hidesBottomBarWhenPushed = YES;
                [strongSelf.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}


@end
