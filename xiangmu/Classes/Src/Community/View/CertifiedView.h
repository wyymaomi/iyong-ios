//
//  CertifiedView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CertifiedView : UIView

@property (strong, nonatomic) UIImageView *personImageView;
@property (strong, nonatomic) UIImageView *companyImageView;
@property (strong, nonatomic) UIImageView *vechileImageView;

//@property (weak, nonatomic) IBOutlet UIImageView *personImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *companyImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *vechileImageView;

- (void)initData:(BOOL)isRealnameCertified isCompanyCertified:(BOOL)isCompanyCertified isVechileCertified:(BOOL)isVechileCertified;

@end
