//
//  NewsListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewsListViewController.h"
#import "NewsListViewController+NetworkRequest.h"
#import "VechileExperienceTableViewCell.h"
#import "NewsTableViewCell.h"
#import "NewsAdvTableViewCell.h"
//#import "BaseWebViewController.h"
//#import "BaseWebpageViewController.h"

@interface NewsListViewController ()

@end

@implementation NewsListViewController

- (PageUtil*)commonPage
{
    if (_commonPage == nil) {
        _commonPage = [PageUtil new];
        _commonPage.pageIndex = 1;
    }
    return _commonPage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DLog(@"NewsListViewController viewDidLoad");
    // Do any additional setup after loading the view.
    [self.view addSubview:self.pullRefreshTableView];
    //    self.title = @"体验";
    [self.navigationItem setTitle:@"资讯"];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(gotoPublish)];
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@sele
    //                                              ]
    
    [self.pullRefreshTableView beginHeaderRefresh];
    
//    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    //#if DEBUG
    //    [self.view addSubview:self.fpsLabel];
    //    self.fpsLabel.top = self.view.height-30*Scale;
    //#endif
    
    //    self.pullRefreshTableView.separatorStyle = UITable;
    
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(onPublish)];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.plainTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.plainTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.plainTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.plainTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    //    self.fpsLabel.top = self.view.frame.size.height-22;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x333333)] forBarMetrics:UIBarMetricsDefault];
    
    
    
    //    self.statusView.frame = CGRectMake(0, 0, ViewWidth, 50);
    //    [self.statusView initWithTitles:self.titleArray NormalColor:[UIColor blackColor] SelectedColor:[UIColor blackColor] LineColor:[UIColor blueColor]];
    //
    self.pullRefreshTableView.frame = CGRectMake(0, 0, ViewWidth, self.view.frame.size.height);
    
    //    [self.pullRefreshTableView beginHeaderRefresh];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forBarMetrics:UIBarMetricsDefault];
}

//- (UIStatusBarStyle)preferredStatusBarStyle {
//    return UIStatusBarStyleLightContent;
//}
//
//// 状态栏是否隐藏
//- (BOOL)prefersStatusBarHidden
//{
//    return NO;
//    //    return self.isStatusBarHidden;
//}

// 状态栏隐藏动画
//- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
//{
//    return UIStatusBarAnimationSlide;
//}

// 状态栏是否隐藏
//- (BOOL)prefersStatusBarHidden
//{
//    return self.isStatusBarHidden;
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PullRefresh delegate method

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;

    self.commonPage.pageIndex = 1;
    
    [self fetchNewsListByPageIndex:self.commonPage.pageIndex newsId:nil];
    
//    [self fetchNewsListByPageIndex:self.commonPage.pageIndex];
    
//    [self fetchNewsListByCreateTime:nil];
    
    //    [self refreshData:nil time:nil isHeaderRefresh:YES];
//    self.commonPage.pageIndex = 1;
//    [self fetchExperienceListByPageIndex:self.commonPage.pageIndex];
    
}

-(void)onFooterRefresh
{
    
    if (self.dataList.count == 0) {
        //        [self.pullRefreshTableView endFooterRefresh];
        //        self.pullRefreshTableView onH
        [self onHeaderRefresh];
        return;
    }
    
    if (self.dataList.count > 0) {
        self.nextAction = @selector(onFooterRefresh);
        self.object1 = nil;
        self.object2 = nil;
        
        NewsModel *newsModel = [self.dataList lastObject];
        [self fetchNewsListByPageIndex:self.commonPage.pageIndex newsId:newsModel.id];
        
    }
    
    
}

#pragma mark -

- (void)gotoPublish
{
    
//    if ([self needLogin]) {
//        return;
//    }
//    
//    PostExperienceViewController *viewController = [[PostExperienceViewController alloc] init];
//    viewController.type = TableTypeUserExperience;
//    viewController.enter_type = ENTER_TYPE_PRESENT;
//    //    self.navigationController
//    //    [self.navigationController pushViewController:viewController animated:YES];
//    //    viewController.enter_type = ENTER_TYPE_PRESENT;
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
//    [self presentViewController:navigationController animated:YES completion:nil];
//    
//    //    PublishViewController *viewController = [[PublishViewController alloc] init];
//    //    viewController.
//    //    self gotoPage:@"PublishView"
//    //    PublishViewController *publishViewController = [[PublishViewController alloc] init];
//    //    [self presentViewController:publishViewController animated:YES completion:nil];
}

#pragma mark -

//-(void)refreshData:(NSString*)quantity time:(NSString*)time isHeaderRefresh:(BOOL)isHeaderRefresh
//{
//
////    if ([self.title isEqualToString:@"我的收藏"]) {
////        [self fetchMyFavoriteList:quantity time:time isHeaderRefresh:isHeaderRefresh];
////    }
////    else {
//        [self fetchServiceList:quantity time:time isHeaderRefresh:isHeaderRefresh];
////    }
//
//}



#pragma  mark - 按点赞数量排序获取数据


#if 0
- (void)fetchServiceList:(NSString*)quantity time:(NSString*)time isHeaderRefresh:(BOOL)isHeaderRefresh
{
    NSDictionary *params = @{/*@"quantity":[StringUtil getSafeString:quantity],*/
                             @"areaCode":[AppConfig currentConfig].gpsCityCode,
                             @"time":[StringUtil getSafeString:time],
                             @"token":[StringUtil getSafeString:[UserManager sharedInstance].token]};
    
    NSString *url;
    url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_LIST_API];
    //    if ([self.title isEqualToString:@"我的收藏"]) {
    //        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, MY_FAVORITE_SERVICER_API];
    //    }
    
    if (isHeaderRefresh) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        if (code_status == STATUS_OK) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
                if (error) {
                    return;
                }
                
                //                if ([responseData.code integerValue] == STATUS_OK) {
                
                for (NSInteger i = 0 ; i < responseData.data.count; i++) {
                    UserExpericeModel *model = responseData.data[i];
                    UserExpericenViewModel *viewModel = [[UserExpericenViewModel alloc] init];
                    viewModel.serviceModel = model;
                    [strongSelf.dataList addObject:viewModel];
                }
                
                
                //                    [strongSelf.pullRefreshTableView reloadData];
                //                }
                //                else {
                //                    [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
                //                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    strongSelf.loadStatus = LoadStatusFinish;
                    [strongSelf.pullRefreshTableView reloadData];
                    //                    strongSelf.pullRefreshTableView.footerHidden = NO;
                });
                
            });
            
            
            //            strongSelf.loadStatus = LoadStatusFinish;
            //
            //            NSError *error;
            //            UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
            //            if (error) {
            //                return;
            //            }
            //
            //            if ([responseData.code integerValue] == STATUS_OK) {
            //
            //                for (NSInteger i = 0 ; i < responseData.data.count; i++) {
            //                    UserExpericeModel *model = responseData.data[i];
            //                    UserExpericenViewModel *viewModel = [[UserExpericenViewModel alloc] init];
            //                    viewModel.serviceModel = model;
            //                    [strongSelf.dataList addObject:viewModel];
            //                }
            //
            //                [strongSelf.pullRefreshTableView reloadData];
            //            }
            //            else {
            //                [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
            //            }
            
        }
        else if (code_status == NETWORK_FAILED) {
            strongSelf.loadStatus = LoadStatusNetworkError;
        }
        else {
            strongSelf.loadStatus = LoadStatusFinish;
            NSError *error;
            UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
            if (error) {
                return;
            }
            [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
        }
        
    }];
}
#endif

#pragma mark - 打电话


#pragma mark - 分享

- (void)onClickShareButton:(id)sender
{
    if ([self needLogin]) {
        return;
    }
    
    //    [UMSocialManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
    //        // 根据获取的platformType确定所选平台进行下一步操作
    //    }];
    
    //    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer*)sender;
    //    UIView *view = [singleTap view];
    //    NSInteger index = view.tag;
    //
    //    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该功能暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    //    [alertView show];
    
    
}

#pragma mark - 评论

- (void)onClickCommentButton:(id)sender
{
    if ([self needLogin]) {
        return;
    }
    
    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer*)sender;
    UIView *view = [singleTap view];
    NSInteger index = view.tag;
    
    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该功能暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];
    
}

#pragma mark - 点赞服务商

- (void)onClickLikeButton:(id)sender
{
    self.nextAction = @selector(onClickLikeButton:);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    UIButton *button = (UIButton*)sender;
    NSInteger index = button.tag;
    DLog(@"index = %ld", (long)index);
    UserExpericenViewModel *viewModel = self.dataList[index];
//    [self addLikeNumber:viewModel];
    
    
    
    //    if ([sender isKindOfClass:[UIButton class]]) {
    
    //    }
    //    else {
    //        UITapGestureRecognizer *singleTap = (UITapGestureRecognizer*)sender;
    //        UIView *view = [singleTap view];
    //        NSInteger index = view.tag;
    //        UILabel *likeLabel = [view viewWithTag:2000];
    //        UIImageView *likeImageView = [view viewWithTag:2001];
    //
    //        UserExpericenViewModel *viewModel = self.dataList[index];
    //
    //        [self addLikeNumber:viewModel likeLabel:likeLabel likeImageView:likeImageView];
    //    }
    
    
    
    
}


#pragma mark - 收藏服务商



#pragma mark - UITableViewCell delegate method


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    DLog(@"-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView");
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    DLog(@"tableView:(UITableView*)tableView numberOfRowsInSection:(NSIndexPath*)indexPath");
//    return 1;
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DLog(@"-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath");
    
    if (self.dataList.count == 0) {
        return 0;
    }
    
    //    UserExpericenViewModel *viewModel = self.dataList[indexPath.section];
    //    return viewModel.cellHeight;
    
    NewsModel *data = self.dataList[indexPath.row];
    
    if ([data.ad boolValue]) {
        return [NewsAdvTableViewCell getCellHeight];
    }
    
    return [NewsTableViewCell getCellHeight];
//    return [VechileExperienceTableViewCell getCellHeight];
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    //    DLog(@"- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section");
//    return 10;
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.5;
//}
//
//- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    //    DLog(@"- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section");
//    return [UIView new];
//}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NewsModel *data = self.dataList[indexPath.row];
    
    
    
    if ([data.ad boolValue]) {
        
        static NSString *CellIdentifier = @"AdvCellIdentifier";
        
        NewsAdvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[NewsAdvTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        
        [cell initData:data];
        
        return cell;
        
    }
    else {
        
        static NSString *CellIdentifier = @"NewsCellIdentifier";
        
        NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[NewsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        [cell initData:data];
        //    cell.titleLabel.text = data.content;
        
            return cell;
        
    }
    
    return nil;
    

    

//    cell.titleLabel.text = data.content;
//    cell.titleLabel.backgroundColor = [UIColor redColor];
//    CGSize textSize = [data.content textSize:CGSizeMake(cell.titleLabel.width, cell.titleLabel.height) font:cell.titleLabel.font];
//    cell.titleLabel.frame = CGRectMake(cell.titleLabel.left, cell.titleLabel.top, textSize.width, textSize.height);
//
//    
//    cell.sourceFromLabel.text = @"汽车之家";
//    
//    if (data.imgs.count >= 1){
//        [cell.imgView yy_setImageWithObjectKey:data.imgs[0] placeholder:DefaultPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//            
//            
//        } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
//            
//            
//        }];
//    }
    
    
    
    
    
    //    VechileExperienceTableViewCell *cell = [[VechileExperienceTableViewCell alloc] initWithStyle:tableView];
    
//    VechileExperienceTableViewCell *cell = [VechileExperienceTableViewCell cellWithTableView:tableView indexPath:indexPath];
//    UserExpericenViewModel *viewModel = self.dataList[indexPath.section];
//    UserExpericeModel *data = viewModel.serviceModel;
//    // 下载头像
//    //    cell.avatarImageView.image = AvatarPlaceholderImage;
//    //    [cell.avatarImageView downloadImage:data.logoUrl placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//    //        cell.avatarImageView.cornerRadius = cell.avatarImageView.width/2;
//    //    } andFailure:^(NSString *errorDesc) {
//    
//    //    }];
//    //    NSString *img1, *img2, *img3;
//    
//    //    [cell.avatarImageView downloadImageFromAliyunOSS:data.logoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:nil andFailure:nil];
//    
//    [cell.avatarImageView yy_setImageWithObjectKey:data.logoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//        
//        
//    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
//        
//        
//    }];
//    
//    cell.imageView1.hidden = YES;
//    cell.imageView2.hidden = YES;
//    cell.imageView3.hidden = YES;
//    if (data.imgs.count >= 1){
//        cell.imageView1.hidden = NO;
//#if DONWLOAD_SWITCH
//        [cell.imageView1 downloadImageFromAliyunOSS:data.imgs[0] isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
//#else
//        [cell.imageView1 yy_setImageWithObjectKey:data.imgs[0] placeholder:DefaultPlaceholderImage imageSize:DefaultImageSize manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//            
//        } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
//            
//        }];
//#endif
//    }
//    if (data.imgs.count >=2) {
//        cell.imageView2.hidden = NO;
//#if DONWLOAD_SWITCH
//        [cell.imageView2 downloadImageFromAliyunOSS:data.imgs[1] isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
//#else
//        [cell.imageView2 yy_setImageWithObjectKey:data.imgs[1] placeholder:DefaultPlaceholderImage imageSize:DefaultImageSize manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//            
//        } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
//            
//        }];
//#endif
//    }
//    if (data.imgs.count >= 3) {
//        cell.imageView3.hidden = NO;
//#if DONWLOAD_SWITCH
//        [cell.imageView3 downloadImageFromAliyunOSS:data.imgs[2] isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
//#else
//        [cell.imageView3 yy_setImageWithObjectKey:data.imgs[2] placeholder:DefaultPlaceholderImage imageSize:DefaultImageSize manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//            
//        } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
//            
//        }];
//#endif
//    }
//    cell.contentLabel.text = data.content;
//    cell.userNameLabel.text = [StringUtil getSafeString:data.nickname];//[StringUtil getSafeString:data.companyName];//data.companyName;
//    
//    cell.thumbupButton.enabled = YES;
//    cell.thumbupButton.tag = indexPath.section;
//    cell.thumbupNumberLabel.text = data.thumbsQuantity;
//    //    [cell.thumbupButton addTarget:self action:@selector(onClickLikeButton:) forControlEvents:UIControlEventTouchUpInside];
//    
//    //    NSString *thumbupImageName = [data.thumbsUp boolValue]?@"icon_thumup_highlighted":@"icon_thumbup_gray";
//    //    [cell.thumbupButton setImage:[UIImage imageNamed:thumbupImageName] forState:UIControlStateNormal];
//    
//    cell.browseNumberLabel.text = data.browseQuantity;
//    cell.commentNumberLabel.text = data.commentQuantity;
    

    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NewsModel *data = self.dataList[indexPath.row];
    
    NSString *url;
    if ([data.linkUrl startsWith:@"http://"] || [data.linkUrl startsWith:@"https://"]) {
        url = data.linkUrl;
    }
    else {
        url = [NSString stringWithFormat:@"http://%@", data.linkUrl];
    }
    
    [self openWebView:url];
//    [self gotoWebpage:url];
    
//    BaseWebpageViewController *viewController = [[BaseWebpageViewController alloc] init];
//    viewController.url = data.linkUrl;
//    viewController.hidesBottomBarWhenPushed = YES;
//    BaseWebViewController *webViewController = [BaseWebViewController new];
//    webViewController.enter_type = ENTER_TYPE_PUSH;
//    webViewController.hidesBottomBarWhenPushed = YES;
//    webViewController.url = data.linkUrl;
//    webViewController.url = @"https://www.baidu.com";
//    [self.navigationController pushViewController:viewController animated:YES];
    
//    UserExpericenViewModel *viewModel = self.dataList[indexPath.section];
//    //    UserExpericeModel *data = viewModel.serviceModel;
//    ExperienceDetailViewController *viewController = [[ExperienceDetailViewController alloc] init];
//    viewController.experienceViewModel = viewModel;
//    viewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:viewController animated:YES];
    
}

//-(void)viewDidLayoutSubviews
//{
//    if ([self.plainTableView respondsToSelector:@selector(setSeparatorInset:)]) {
//        [self.plainTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([self.plainTableView respondsToSelector:@selector(setLayoutMargins:)]) {
//        [self.plainTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

@end
