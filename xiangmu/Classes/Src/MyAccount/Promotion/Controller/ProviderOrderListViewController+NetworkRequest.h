//
//  ProviderOrderListViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderOrderListViewController.h"

@interface ProviderOrderListViewController (NetworkRequest)

- (void)fetchProviderAdOrderListWithDetail:(NSString*)time;

-(void)continuePayOrder:(UIButton*)button;

-(void)cancelOrder:(UIButton*)button;

-(void)deleteOrder:(id)sender;


@end
