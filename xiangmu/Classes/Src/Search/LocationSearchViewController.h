//
//  LocationViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "LocationSearchBar.h"
#import "AssociationalWordDisplayController.h"
#import "iflyMSC/iflyMSC.h"
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

@class LocationSearchModel;

@protocol LocationSearchDelegate <NSObject>

- (void)didSelectLocation:(NSString*)location;

-(void)didSelectAddress:(NSString*)city address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate;

@end

@interface LocationSearchViewController : BaseViewController<LocationSearchBarDelegate, AssociationalWordDisplayDelegate,BMKSuggestionSearchDelegate,IFlyRecognizerViewDelegate>

/**
 *  是否是search状态
 */
@property(nonatomic, assign) BOOL isSearch;

@property (nonatomic, strong) LocationSearchBar *searchBar;
@property (nonatomic, strong) AssociationalWordDisplayController *keywordDisplayController;
@property (nonatomic, weak) id<LocationSearchDelegate> delegate;

@property (nonatomic, copy) void (^selectLocationHandle)(NSString *text, NSString *cityId);

@property (nonatomic, copy) void (^selectAddressHandle)(NSString *location, NSString *address, NSString *cityName,  NSString *areaCode, CLLocationCoordinate2D coordinate);

@property (nonatomic, strong) BMKSuggestionSearch *searcher;

@property (nonatomic, strong) IFlyRecognizerView *iflyRecognizerView;//带界面的识别对象


@end
