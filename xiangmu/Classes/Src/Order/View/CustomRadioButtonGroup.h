//
//  CustomRadioButtonExt.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomRadioButtonGroupDelegate

-(void)onClickButton:(NSInteger)index;

@end

#define ROW_COUNT 2 // 每行显示的个数
#define MARGIN_COLUMN 8 // 每列间距
//#define START_X 10
#define START_Y 5
#define BUTTON_HEIGHT 38
#define BUTTON_WIDTH  145

@interface CustomRadioButtonGroup : UIView

-(id)initWithFrame:(CGRect)frame data:(NSArray*)data;
@property (nonatomic, strong) NSMutableArray *customButtonArray;
@property (nonatomic, weak) id<CustomRadioButtonGroupDelegate> delegate;

@property (nonatomic, strong) UIButton *lastButton;

@end
