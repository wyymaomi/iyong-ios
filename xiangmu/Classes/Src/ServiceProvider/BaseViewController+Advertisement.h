//
//  ServiceProviderRootViewController+Advertisement.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ServiceProviderRootViewController.h"
#import "SIDADView.h"
#import "MaskView.h"

static char const * const kAdView = "kAdView";
static char const * const kAdvList = "kAdvList";
static char const * const kMaskView = "kMaskView";
//static char const * const kGuideView = "kGuideView";
//static char const * const kClickIndex = "kClickIndex";

@interface BaseViewController (Advertisement)<AdvDelegate>

@property (nonatomic, strong) MaskView *maskView;// 遮罩层

@property (nonatomic, strong) SIDADView *adView;// 广告
@property (nonatomic, strong) NSArray *advList; // 广告链接地址

- (void)showAdv:(BOOL)bAll;
- (void)onClickAdvIcon; // 点击广告图标
- (void)showAdvFirstTime;
- (void)onDimiss;

-(void)showMaskView;


@end
