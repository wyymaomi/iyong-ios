//
//  GuideView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideView : UIView

@property (strong, nonatomic) UIButton *okButton;
//@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImageView *imageView;

@end
