//
//  CreditLineApplyUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/11.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CreditHttpMessage.h"

@interface CreditLineApplyUtil : NSObject

+(BOOL)validateCreditLineApply:(CreditHttpMessage*)httpMessage type:(NSUInteger)type errorMsg:(NSString**)errorMsg;

@end
