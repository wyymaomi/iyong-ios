//
//  NewBankTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBankTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bankcardLabel;
@property (weak, nonatomic) IBOutlet UIButton *addBankcardButton;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+(CGFloat)getCellHeight;

@end
