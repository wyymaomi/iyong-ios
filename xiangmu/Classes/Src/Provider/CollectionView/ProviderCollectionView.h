//
//  ProviderCollectionView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "PullRefreshCollectionView.h"
#import "OnlineServiceModel.h"


@protocol ProviderCollectionViewDelegate <NSObject>

- (void)onSelectItem:(NSInteger)row;

@end



@interface ProviderCollectionView : PullRefreshCollectionView<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *dataList;

@property (nonatomic, weak) id<ProviderCollectionViewDelegate> customDelegate;

@end
