//
//  GesturePasswordView.m
//  GesturePassword
//
//  Created by hb on 14-8-23.
//  Copyright (c) 2014年 黑と白の印记. All rights reserved.
//

#import "GesturePasswordView.h"
#import "GesturePasswordButton.h"
#import "TentacleView.h"

@implementation GesturePasswordView {
    NSMutableArray * buttonArray;
    
    CGPoint lineStartPoint;
    CGPoint lineEndPoint;
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = UIColorFromRGB(0xF7F7F7);
        buttonArray = [[NSMutableArray alloc]initWithCapacity:0];
        
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(frame.size.width/2-160, frame.size.height/2-150, 320, 320)];
        for (int i=0; i<9; i++) {
            NSInteger row = i/3;
            NSInteger col = i%3;
            // Button Frame
            
            NSInteger distance = 320/3;
            NSInteger size = distance/1.5;
            NSInteger margin = size/4;
            GesturePasswordButton * gesturePasswordButton = [[GesturePasswordButton alloc]initWithFrame:CGRectMake(col*distance+margin, row*distance, size, size)];
            [gesturePasswordButton setTag:i];
            [view addSubview:gesturePasswordButton];
            [buttonArray addObject:gesturePasswordButton];
        }
        frame.origin.y=0;
        [self addSubview:view];
        
        _tentacleView = [[TentacleView alloc]initWithFrame:view.frame];
        [_tentacleView setButtonArray:buttonArray];
        [_tentacleView setTouchBeginDelegate:self];
        [self addSubview:_tentacleView];
        
        
        
        _state = [[UILabel alloc]initWithFrame:CGRectMake(frame.size.width/2-140, frame.size.height/2-220, 280, 30)];
        _state.text = @"请输入手势密码";
        [_state setTextAlignment:NSTextAlignmentCenter];
        [_state setFont:[UIFont systemFontOfSize:14.f]];
        [self addSubview:_state];
        

//        UIButton *btn=[MyUtil createBtnFrame:CGRectMake(20, CGRectGetMaxY(password.frame)+40, ViewWidth-40, ViewHeight/15) title:@"登  录" bgImageName:nil target:self action:@selector(gotoLoad)];
//        btn.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:150.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
//        btn.layer.cornerRadius=18.0f;
//        [btn setTitle:@"登  录" forState:UIControlStateNormal];
//        [self.scrollview addSubview:btn];
//        self.okButton = [[UIButton alloc] initWithFrame:CGRectMake((frame.size.width - 15) / 2, (frame.size.height - 60), frame.size.width - 15 * 2, 34)];
//        [self.okButton setTitle:@"完成" forState:UIControlStateNormal];
//        self.okButton.backgroundColor = UIColorFromRGB(0x30a6de);
//        self.okButton.layer.cornerRadius = 15.f;
//        [self addSubview:self.okButton];
        
//        imgView = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/2-35, frame.size.width/2-80, 70, 70)];
//        [imgView setBackgroundColor:[UIColor whiteColor]];
//        [imgView.layer setCornerRadius:35];
//        [imgView.layer setBorderColor:[UIColor grayColor].CGColor];
//        [imgView.layer setBorderWidth:3];
//        [self addSubview:imgView];
        
//        _forgetButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width/2-150, frame.size.height/2, 120, 30)];
//        [_forgetButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
//        [_forgetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [_forgetButton setTitle:@"忘记手势密码" forState:UIControlStateNormal];
//        [_forgetButton addTarget:self action:@selector(forget) forControlEvents:UIControlEventTouchDown];
//        [self addSubview:_forgetButton];
        
//        changeButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width/2+30, frame.size.height/2+220, 120, 30)];
//        [changeButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
//        [changeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [changeButton setTitle:@"修改手势密码" forState:UIControlStateNormal];
//        [changeButton addTarget:self action:@selector(change) forControlEvents:UIControlEventTouchDown];
//        [self addSubview:changeButton];
    }
    
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect
{
//    // Drawing code
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
//    CGFloat colors[] =
//    {
//        134 / 255.0, 157 / 255.0, 147 / 255.0, 1.00,
//        3 / 255.0,  3 / 255.0, 37 / 255.0, 1.00,
//    };
//    CGGradientRef gradient = CGGradientCreateWithColorComponents
//    (rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
//    CGColorSpaceRelease(rgb);
//    CGContextDrawLinearGradient(context, gradient,CGPointMake
//                                (0.0,0.0) ,CGPointMake(0.0,self.frame.size.height),
//                                kCGGradientDrawsBeforeStartLocation);
}

- (void)gestureTouchBegin {
//    [self.state setText:@""];
}

-(void)forget{
    if (_gesturePasswordDelegate && [_gesturePasswordDelegate respondsToSelector:@selector(forget)]) {
            [_gesturePasswordDelegate forget];
    }

}

-(void)change{
    if (_gesturePasswordDelegate && [_gesturePasswordDelegate respondsToSelector:@selector(change)]) {
        [_gesturePasswordDelegate change];
    }
}


@end
