//
//  BaseTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PullRefreshTableViewDelegate <NSObject>

-(void)onHeaderRefresh;
@optional
-(void)onFooterRefresh;

@end

@interface PullRefreshTableView : UITableView

@property (nonatomic, assign) BOOL hasHeaderRefresh;
@property (nonatomic, assign) BOOL hasFooterRefresh;
@property (nonatomic, assign) BOOL footerHidden;

@property (nonatomic, weak) id<PullRefreshTableViewDelegate> pullRefreshDelegate;

-(void)beginHeaderRefresh;
-(void)endHeaderRefresh;

-(void)beginFooterRefresh;
-(void)endFooterRefresh;

- (void)setupHeaderView;
- (void)setupFooterView;

@end
