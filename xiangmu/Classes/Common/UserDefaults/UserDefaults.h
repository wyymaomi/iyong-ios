//
//  UserDefaults.h
//  lottery
//
//  Created by WYY on 14-4-24.
//
//

#import <Foundation/Foundation.h>

@interface UserDefaults : NSObject

+ (instancetype)sharedInstance;

- (BOOL)hasObjectForKey:(id)key;

- (id)objectForKey:(id)key;
- (void)setObject:(id)object forKey:(id)key;

- (void)removeObjectForKey:(id)key;
- (void)removeAllObjects;

- (id)objectForKeyedSubscript:(id)key;
- (void)setObject:(id)obj forKeyedSubscript:(id)key;

@end
