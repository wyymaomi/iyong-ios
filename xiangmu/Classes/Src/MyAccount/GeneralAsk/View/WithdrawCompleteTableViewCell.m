//
//  WithdrawCompleteTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/15.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "WithdrawCompleteTableViewCell.h"

@implementation WithdrawCompleteTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"WithdrawRecordInreviewTableViewCell";
    WithdrawCompleteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
