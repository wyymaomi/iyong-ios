//
//  UntreatedOrderTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderItemTableViewCell.h"
#import "OrderListResponseModel.h"

@implementation OrderItemTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"CellIdentifier";
    OrderItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
    }
    return cell;
}

-(void)initData:(OrderListItem*)item
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[item.carTime doubleValue]/1000];
    
    self.carDateLabel.text = [date stringWithDateFormat:@"yyyy-MM-dd HH:mm"];
    
    self.startLocationLabel.text = item.location.trim;
    
    self.arriveLocationLabel.text = item.downAddress.trim;
    

    
    __weak __typeof(self)wself = self;
    
    NSString *logoUrl;
    
    if ([[UserManager sharedInstance] isSelfDispatch:item.companyId]) {
        logoUrl = [UserManager sharedInstance].userModel.logoUrl;
    }
    else {
        logoUrl = item.upCompanyLogoUrl;
    }
    
//    NSString *logoUrl = IsStrEmpty(item.upCompanyName) ? [UserManager sharedInstance].userModel.logoUrl : item.upCompanyLogoUrl;
    
//    [self.avatarImageView downloadImage:logoUrl placeholderImage:[UIImage imageNamed:@"icon_avatar"] success:^(id responseData) {
//        
//        wself.avatarImageView.cornerRadius = wself.avatarImageView.frame.size.width/2;
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//        
//    }];
    
    [self.avatarImageView yy_setImageWithObjectKey:logoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        wself.avatarImageView.cornerRadius = wself.avatarImageView.width/2;
        
    }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
