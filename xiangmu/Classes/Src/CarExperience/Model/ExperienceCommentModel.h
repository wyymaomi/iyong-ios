//
//  ExperienceCommentModel.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol ExperienceCommentModel <NSObject>

@end

@interface ExperienceCommentModel : JSONModel

//commenterCompanyId = 8aae9e12550a0ec101550a9b9fd30066;
//commenterLogoUrl = "8aae9e12550a0ec101550a9b9fd30066/17299f4d-40bb-4105-8a80-87c957de94f8.jpg";
//content = test;
//createTime = 1487930020000;
//nickname = "\U5929\U5802\U732b";
//topicId = 8a9996925982245801598ba0e3d2012d;

@property (nonatomic, strong) NSString<Optional> *topicId;// 评论主题ID
@property (nonatomic, strong) NSString<Optional> *commenterCompanyId;// 评论者公司ID
@property (nonatomic, strong) NSString<Optional> *commenterLogoUrl; // 评论者头像LOGO
@property (nonatomic, strong) NSString<Optional> *content; // 评论内容
@property (nonatomic, strong) NSString<Optional> *createTime; // 评论发表时间
@property (nonatomic, strong) NSString<Optional> *nickname;// 评论者昵称

- (NSString*)displayTime;

@end

@interface ExperienceCommentModelResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<ExperienceCommentModel, Optional> *data;

@end
