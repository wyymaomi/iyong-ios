//
//  AddressTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VechileOrderHttpMessage.h"
#import "CarTypeModel.h"

typedef NS_ENUM(NSUInteger, AddressTableRowType)
{
    AddressTableViewRowTypeTime,
    AddressTableViewRowTypeCarType,
    AddressTableViewRowTypeStartAddress,
    AddressTAbleViewRowTypeEndAddress,
    AddressTAbleViewRowTypeTelephone
};

@protocol AddressTableViewDelegate <NSObject>

-(void)didSelectItemOnTag:(NSUInteger)tag;

@end

@interface AddressTableView : UITableView<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray<CarTypeModel*> *carTypeList;

@property (nonatomic, weak) id<AddressTableViewDelegate> customDelegate;

@property (nonatomic, strong) VechileOrderHttpMessage *vechileOrderHttpMessage;

@property (nonatomic, assign) NSUInteger timeType;

@end
