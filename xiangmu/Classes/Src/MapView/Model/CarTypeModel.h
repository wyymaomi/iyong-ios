//
//  CarTypeModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol RouteModel <NSObject>

@end

@interface RouteModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *useCarMode;
@property (nonatomic, strong) NSNumber<Optional> *route;

@end

@interface CarTypeModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *carType;
@property (nonatomic, strong) NSNumber<Optional> *unitPrice;//单价
@property (nonatomic, strong) NSNumber<Optional> *startingMileage;// 起步里程
@property (nonatomic, strong) NSNumber<Optional> *startingPrice;// 起步价
@property (nonatomic, strong) NSArray<RouteModel, Optional> *carTypeRoutes; //carTypeRoutes 车型模式路线列表（useCarMode 用车模式（1=实时用车,2=预约用车），route 路线(1=最短路程，2=最短时间, 3=躲避拥堵)）


@end
