//
//  AdvPurchaseDetailViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/13.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPurchaseDetailViewController.h"
#import "AdvSumHttpMessage.h"

@interface AdvPurchaseDetailViewController (NetworkRequest)

- (void)fetchAdvSumList:(AdvSumHttpMessage*)httpMessage;

- (void)fetchAdvDetailList:(NSString*)date;

@end
