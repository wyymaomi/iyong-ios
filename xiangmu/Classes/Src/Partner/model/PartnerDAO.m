//
//  PartnerDAO.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PartnerDAO.h"
#import "PartnerModel.h"

@implementation PartnerDAO


- (NSMutableArray*)getAllPartner:(NSString*)companyId;
{
    __block NSMutableArray *array = nil;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString *sql = [NSString stringWithFormat:@"select * from info_partner where companyId = ? ORDER BY updateTime DESC"];
        FMResultSet *rs = [db executeQuery:sql, companyId];
        if (!rs) {
            [rs close];
            return;
        }
        
        array = [NSMutableArray new];
        while ([rs next]) {
            
            PartnerModel *partner = [[PartnerModel alloc] init];
            
            partner.partnerCompanyId = [rs stringForColumn:@"partnerCompanyId"];
            partner.companyName = [rs stringForColumn:@"companyName"];
            partner.createTime = [NSNumber numberWithDouble:[rs longForColumn:@"createTime"]];
            partner.id = [rs stringForColumn:@"partnerId"];
            partner.logoUrl = [rs stringForColumn:@"logoUrl"];
            partner.mobile = [rs stringForColumn:@"mobile"];
            partner.nickname = [rs stringForColumn:@"nickname"];
            partner.updateTime = [rs stringForColumn:@"updateTime"];
            
            [array addObject:partner];
            
        }
        
        
    }];
    return array;
}

- (BOOL)updatePartnerList:(NSArray*)partnerList;
{
    __block BOOL isSuccess = NO;
    
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        for (PartnerModel *partnerModel in partnerList) {
            
            if (partnerModel == nil || IsStrEmpty(partnerModel.id)) {
                continue;
            }
            
            if ([partnerModel.valid boolValue]) {
                
                // 是否存在该条记录
                NSString *querySql = [NSString stringWithFormat:@"select count(*) from info_partner where companyId = ? AND partnerId = ?"];
                FMResultSet *rs = [db executeQuery:querySql, partnerModel.companyId, partnerModel.id];
                // 如果不存在 则增加该条记录
                NSInteger num = 0;
                while ([rs next]) {
                    num = [rs intForColumnIndex:0];
                }
                
                if (num > 0) {
                    NSString *partnerId = partnerModel.id;
                    NSString *companyId = partnerModel.companyId;
                    
                    NSString *sql = [NSString stringWithFormat:@"delete from info_partner where partnerId=? AND companyId=?"];
                    [db executeUpdate:sql, partnerId, companyId];
                }
                
                NSString *sql = @"insert into info_partner(companyId, companyName, partnerId, logoUrl,  mobile, nickname, partnerCompanyId, createTime, updateTime) values(?,?,?,?,?,?,?,?,?)";
                [db executeUpdate:sql, partnerModel.companyId, partnerModel.companyName, partnerModel.id, partnerModel.logoUrl, partnerModel.mobile, partnerModel.nickname, partnerModel.partnerCompanyId, partnerModel.createTime, partnerModel.updateTime];
            }
            else {
                
                NSString *partnerId = partnerModel.id;
                NSString *companyId = partnerModel.companyId;
                
                NSString *sql = [NSString stringWithFormat:@"delete from info_partner where partnerId=? AND companyId=?"];
                [db executeUpdate:sql, partnerId, companyId];
                
            }
            
        }
        
        isSuccess = YES;
        
    }];
    return isSuccess;
}

- (BOOL)deletePartnerById:(NSString*)partnerId companyId:(NSString*)companyId
{
    if (IsStrEmpty(partnerId)) {
        return NO;
    }
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        
        NSString *sql = [NSString stringWithFormat:@"delete from info_partner where partnerId = ? AND companyId = ?"];
        [db executeUpdate:sql, partnerId, companyId];
        
    }];
    
    return YES;
}

@end
