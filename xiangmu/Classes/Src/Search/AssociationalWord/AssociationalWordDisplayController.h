//
//  AssociationalWordDisplayController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchModel.h"

@protocol AssociationalWordDisplayDelegate <NSObject>

@optional
/*!
 @abstract      点击选择某一个联想词的事件传递
 @param         keyword  选择的联想词
 */
-(void)didSelectAssociationalWord:(LocationSearchModel*)data;
- (void)didScrollView;

@end

@interface AssociationalWordDisplayController : NSObject<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *displayTableView;
//@property (nonatomic, assign) CGFloat tableTopPosY; // view顶部的positionY
//@property (nonatomic, assign) CGFloat distanceToTop; // view顶部到屏幕顶端的距离
@property (nonatomic, strong) NSMutableArray *keywordList;
//@property (nonatomic, weak) UIViewController *contentController;
@property (nonatomic, weak) id<AssociationalWordDisplayDelegate> delegate;

/*!
 @abstract      初始化方法
 @param         controller  所在的viewController
 */
//- (id)initWithController:(UIViewController*)controller;


@end
