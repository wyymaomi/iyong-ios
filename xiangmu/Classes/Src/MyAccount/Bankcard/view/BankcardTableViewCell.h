//
//  BankcardTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankcardTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bankTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bankCardNumberLabel;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
