//
//  AliyunOSSManager.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/4.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AliyunOSSiOS/OSSService.h>
#import "NSData+ImageContentType.h"

extern OSSClient *client;

typedef NS_ENUM(NSInteger, UploadImageState) {
    UploadImageFailed   = 0,
    UploadImageSuccess  = 1
};

typedef NS_ENUM(NSInteger, DownloadImageState) {
    DownloadImageFailed = 0,
    DownloadImageSuccess = 1
};



@interface AliyunOSSManager : NSObject

+ (instancetype)sharedInstance;

- (void)setupEnvironment;

- (void)runDemo;

- (void)uploadObjectAsync:(NSString*)fileName imageData:(NSData*)imageData;

- (void)downloadObjectAsync;

- (void)resumableUpload;

- (void)uploadImages:(NSArray<NSData *> *)imageDatas isAsync:(BOOL)isAsync folderName:(NSString*)folderName complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete;

- (void)uploadImageArray:(NSArray<UIImage *>*)images isAsync:(BOOL)isAsync folderName:(NSString*)folderName complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete;

- (void)downloadImage:(NSString*)imageName isThumbnail:(BOOL)isThumbnail complete:(void(^)(DownloadImageState state, NSData *data))complete;

- (void)downloadImageWithSize:(NSString*)imageName size:(CGSize)size complete:(void(^)(DownloadImageState state, NSData *data))complete;

- (void)downloadImageArray:(NSArray<NSString*>*)imageNames
                  complete:(void(^)(NSArray<UIImage*>*images, DownloadImageState state))complete;

-(void)pubObjectACL:(NSString*)objectKey isPrivate:(BOOL)isPrivate;

@end
