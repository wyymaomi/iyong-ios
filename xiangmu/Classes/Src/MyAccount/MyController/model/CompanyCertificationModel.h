//
//  CompanyCertificationModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

//address = test;
//areaCode = 310100;
//areaName = "\U4e0a\U6d77\U4e0a\U6d77\U5e02";
//businessLicenceUrl = "8a9996925b204b29015b28079b6f006b/1471e1d4-6950-4e91-8d2d-a70ab28e25fe.jpg";
//companyId = 8a9996925b204b29015b28079b6f006b;
//id = 8a9996925b9b9ac4015b9ba082a90001;
//name = test;
//status = 4;

@interface CompanyCertificationModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *address;
@property (nonatomic, strong) NSString<Optional> *areaName;
@property (nonatomic, strong) NSString<Optional> *areaCode;
@property (nonatomic, strong) NSString<Optional> *businessLicenceUrl;
//@property (nonatomic, strong) NSString<Optional> *cityCode;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *type;

@end
