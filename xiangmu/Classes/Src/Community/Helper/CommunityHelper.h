//
//  CommunityHelper.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BidHttpMessage.h"
#import "InquireHttpMessage.h"
#import "GYZCity.h"

@interface CommunityHelper : NSObject

+ (BOOL) isValidPublisher:(NSString**)errorMsg; // 是否有发布权限

+ (BOOL) isCertifcationed:(NSString**)errorMsg;// 三项认证是否已经满足

+ (BOOL) isValidBusienssCircleBrowser:(NSString**)errorMsg; // 是否有业务圈浏览权限

+ (BOOL) isValidBusinessCircleBider:(NSString**)errorMsg; // 是否有业务圈报价权限

+ (BOOL) isValidEnquire:(InquireHttpMessage*)httpMessage errorMsg:(NSString**)errorMsg;

+(BOOL)isValidBidContent:(BidHttpMessage*)bidHttpMesage errorMsg:(NSString**)errorMsg;

@property (nonatomic, strong) NSMutableArray *cityDatas;
@property (nonatomic, strong) NSArray *vechileServiceList;// 用车服务类型

- (NSString*)getCityIdFromCityName:(NSString*)cityName;
- (NSString *)getCityNameFromAreacode:(NSString*)areaCode;

- (NSString*)getServicetypeNameFromServiceId:(NSInteger)serviceId;
- (NSArray*)getServicetypeNameList;

@end
