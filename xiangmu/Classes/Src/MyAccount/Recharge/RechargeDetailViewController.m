//
//  RechargeDetailViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RechargeDetailViewController.h"
#import "AccountBalanceViewController.h"

@interface RechargeDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *bankcardInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *rechargeAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UILabel *transTitleLabel;

@end

@implementation RechargeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = @"充值详情";
    self.title = (self.type == 0)?@"充值详情":@"提现详情";
    self.bankcardInfoLabel.text = self.bankcardInfo;
    self.rechargeAmountLabel.text = self.transAmount;
    [self.okButton blueStyle];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doBack:(id)sender {
    NSArray *vcArray = self.navigationController.viewControllers;
    
    for(UIViewController *vc in vcArray)
    {
        if ([vc isKindOfClass:[AccountBalanceViewController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
