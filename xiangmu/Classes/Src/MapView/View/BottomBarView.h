//
//  BottomBarView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BottomBarView : UIView

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
//@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *orderButton;

@property (nonatomic, assign) BOOL isEnabled;  // 是否可以使用

-(void)setEnabled:(BOOL)isEnable;

@end
