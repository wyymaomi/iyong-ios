//
//  InvoiceInfoSettingTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/30.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceInfoSettingTableViewCell.h"

@implementation InvoiceInfoSettingTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"InvoiceInfoSettingTableViewCellID";
    InvoiceInfoSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.invoiceTitleTextField.tag = InvoiceTextFieldTypeTitle;
        cell.taxpayerNumberTextField.tag = InvoiceTextFieldTypeTaxpayerNumber;
        cell.registAddressTextField.tag = InvoiceTextFieldTypeRegistAddress;
        cell.bankNameTextField.tag = InvoiceTextFieldTypeBankName;
        cell.bankAccountTextField.tag = InvoiceTextFieldTypeBankAccount;
    }
    return cell;
}

+(CGFloat)getCellHeight
{
    return 305;
}

+(CGFloat)getCloseStatusCellHeight
{
    return 102;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
