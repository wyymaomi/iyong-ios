//
//  CommunityHeaderView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/9.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityHeaderView.h"

@implementation CommunityHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initData];
}

-(void)initData
{
    self.nickNameLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.nickname] ;
    self.mobileLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.username];
    self.companyLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.companyName];
//    LRWeakSelf(cell)
//    WeakSelf
//    [self.avartarImageView downloadImage:[UserManager sharedInstance].userModel.logoUrl placeholderImage:[UIImage imageNamed:@"icon_avatar"] success:^(id responseData) {
//        weakSelf.avartarImageView.cornerRadius = weakSelf.avartarImageView.frame.size.width/2;
//    } andFailure:^(NSString *errorDesc) {
//        weakSelf.avartarImageView.cornerRadius = 5;
//    }];
    
//    self.avartarImageView downloadImageFromAliyunOSS:[UserManager sharedInstance].userModel isThumbnail:<#(BOOL)#> placeholderImage:<#(UIImage *)#> success:<#^(id responseData)success#> andFailure:<#^(NSString *errorDesc)failure#>
    BOOL isCompanyCertified = [[UserManager sharedInstance].userModel.companyCertification integerValue] == CompanyCertificationStatusFinished;
    BOOL isPersonCertified = [[UserManager sharedInstance].userModel.certification boolValue];
    BOOL isVechileCertified = [[UserManager sharedInstance].userModel.carCertification boolValue];
    [self.certifiedView initData:isPersonCertified isCompanyCertified:isCompanyCertified isVechileCertified:isVechileCertified];
    
    self.starView.width = kStarFrameWith * 5;
    [self.starView setStarsImages:[[UserManager sharedInstance].userModel.companyScor floatValue]];
    
//    [self setNeedsLayout];
    
}

@end
