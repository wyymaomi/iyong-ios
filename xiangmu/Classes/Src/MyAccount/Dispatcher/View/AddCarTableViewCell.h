//
//  AddCarTableViewCell.h
//  xiangmu
//
//  Created by David kim on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCarTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *rightLabel;
@end
