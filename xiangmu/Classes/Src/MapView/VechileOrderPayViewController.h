//
//  VechilrOrderPayViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "AdvPayTableViewCell.h"
#import "VechileOrderHttpMessage.h"
#import "TravelorderPayHttpMessage.h"

typedef NS_ENUM(NSUInteger, AdThirdpayRow)
{
    AdThirdPayRowAlipay,
    AdThirdPayRowWechat
};


typedef NS_ENUM(NSUInteger , AdPayAccountRow)
{
//    AdPayRowPromotionAccount,
    AdPayRowMyAccount
};

@interface VechileOrderPayViewController : BaseViewController

@property (nonatomic, strong) TravelorderPayHttpMessage *payHttpMessage;

@property (nonatomic, strong) NSNumber *myAccountMoney;

@end
