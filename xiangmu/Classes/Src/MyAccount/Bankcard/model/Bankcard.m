//
//  Bankcard.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/4.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "Bankcard.h"

@implementation Bankcard

-(NSString*)bankNameWithCard
{
    return [NSString stringWithFormat:@"%@(%@)", self.bankName, [self.number substringFromIndex:(self.number.length -4)]];
}

@end


@implementation BankcardResponseData

@end