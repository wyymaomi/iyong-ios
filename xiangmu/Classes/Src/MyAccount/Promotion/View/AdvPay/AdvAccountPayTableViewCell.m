//
//  AdvAccountPayTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvAccountPayTableViewCell.h"

@implementation AdvAccountPayTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"AdvAccountPayTableViewCellIdentifier";
    AdvAccountPayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        [cell.radioButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_radio_unselected"] forState:UIControlStateNormal];
        [cell.radioButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_radio_selected"] forState:UIControlStateSelected];
        
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"推广账户";
//            cell.iconImageView.image = [UIImage imageNamed:@"icon_alipay"];
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"我的账户";
//            cell.iconImageView.image = [UIImage imageNamed:@"icon_wechat"];
        }
    }
    return cell;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
