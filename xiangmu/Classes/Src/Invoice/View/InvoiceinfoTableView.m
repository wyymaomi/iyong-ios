//
//  InvoiceinfoTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceinfoTableView.h"
#import "InvoiceinfoTitleTableViewCell.h"
#import "InvoiceCompanyinfoTableViewCell.h"
#import "InvoicinfoContactTableViewCell.h"

@implementation InvoiceinfoTableView

-(void)setup
{
    [super setup];
    
    self.delegate = self;
    self.dataSource = self;
    
    self.tableFooterView = [[UIView alloc] init];
    self.rowHeight = DefaultTableRowHeight;
    
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        return [InvoiceinfoTitleTableViewCell getCellHeight];
    
    }
    else if (indexPath.section == 1 && indexPath.row == 0) {
        
        return [InvoiceCompanyinfoTableViewCell getCellHeight];
        
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        
        return [InvoicinfoContactTableViewCell getCellHeight];
        
    }
    return 0.00001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 24;
    }
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView]-1) {
        return 85;
    }
    return 0.00001f;
    
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.mode == 0 && section == 0) {
        CGFloat viewHeight = [self tableView:tableView heightForHeaderInSection:section];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, viewHeight)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, ViewWidth-24, viewHeight)];
        label.text = @"*请确认开票信息无误，提交后不能修改和取消*";
        label.font = FONT(13);
        label.textColor = UIColorFromRGB(0x666666);
        label.backgroundColor = [UIColor clearColor];
        [view addSubview:label];
        return view;
    }
    return [UIView new];
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ( self.mode == 0 && section == [self numberOfSectionsInTableView:tableView]-1) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 85)];
        
        UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [okButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xe06430)] forState:UIControlStateNormal];
        okButton.frame = CGRectMake(10, 30, ViewWidth-20, 45);
        okButton.titleLabel.font = FONT(16);
        [okButton setTitle:@"确认提交" forState:UIControlStateNormal];
        [okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        okButton.cornerRadius = 5;
        [okButton addTarget:self action:@selector(onClickSubmit:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:okButton];
        
        return view;
    }
    return [UIView new];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        InvoiceinfoTitleTableViewCell *cell = [InvoiceinfoTitleTableViewCell cellWithTableView:tableView];
        
        if (self.invoiceInfoHttpMessage) {
            cell.invoiceTitleLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.header];
            cell.invoiceTypeLabel.text = @"纸质发票";
            cell.invoiceContentLabel.text = @"客运服务";
            cell.invoiceTotalAmountLabel.text = [NSString stringWithFormat:@"%@元", [self.invoiceInfoHttpMessage.totalAmount stringValue]];
            cell.invoiceTotalQuanityLabel.text = [NSString stringWithFormat:@"%@张", [self.invoiceInfoHttpMessage.totalQuantity stringValue]];
        }
        
        return cell;
        
    }
    else if (indexPath.section == 1) {
        
        InvoiceCompanyinfoTableViewCell *cell = [InvoiceCompanyinfoTableViewCell cellWithTableView:tableView];
        
        cell.taxpayerNumberLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.taxpayerNumber];
        cell.registAddressLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.registerAddress];
        cell.bankNameLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.openingBank];
        cell.bankAccountLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.bankAccount];
        
        
        return cell;
        
    }
    else if (indexPath.section == 2) {
        
        InvoicinfoContactTableViewCell *cell = [InvoicinfoContactTableViewCell cellWithTableView:tableView];
        
        cell.contactNameLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.contact];
        cell.contactPhoneLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.tel];
        cell.contactAddressLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.address];
        cell.contactAreaLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.areaName];
        cell.contactRemarkLabel.text = [StringUtil getSafeString:self.invoiceInfoHttpMessage.remark];
        
        return cell;
        
    }
    
    return nil;

    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
//        [self.customDelegate didSelectRowAtIndexPath:indexPath];
//    }
    
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - 提交

-(void)onClickSubmit:(id)sender
{
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(onClickSubmitButton)]) {
        [self.customDelegate onClickSubmitButton];
    }
}


@end
