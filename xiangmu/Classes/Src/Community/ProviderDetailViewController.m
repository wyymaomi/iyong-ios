//
//  ProviderDetailViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderDetailViewController.h"
#import "ProviderHeaderTableViewCell.h"
#import "ProviderDetailTableViewCell.h"
#import "ProviderServiceTypeTableViewCell.h"
#import "ProviderDescriptionTableViewCell.h"
#import "DiscussTopTableViewCell.h"
#import "ProviderDetailViewModel.h"
#import "ProviderDetailModel.h"
#import "ProviderCommentViewModel.h"
#import "ProviderCommentModel.h"
#import "ExperienceCommentTableViewCell.h"
#import "CompanyCommentTableViewCell.h"
#import "CommentPopupView.h"
#import "ProviderDetailViewController+Network.h"
#import <UShareUI/UShareUI.h>
#import "ShareUtil.h"
#import "ChatViewController.h"
#import "EaseUserModel.h"
#import "ChatDemoHelper.h"

#define PROVIDER_DETAIL_PATH @"/iyong-web/share/provider.html"

@interface ProviderDetailViewController ()<UITableViewDelegate, UITableViewDataSource,PullRefreshTableViewDelegate,CommentPopupViewDelegate,SDCycleScrollViewDelegate>

@property (nonatomic, strong) MLSDKScene *scene;
@property (nonatomic, strong) NSString *mobid;

@end

@implementation ProviderDetailViewController

+ (NSString *)MLSDKPath
{
    return PROVIDER_DETAIL_PATH;
}

- (instancetype)initWithMobLinkScene:(MLSDKScene *)scene
{
    if (self = [super init])
    {
        self.scene = scene;
    }
    return self;
}

- (CommentPopupView*)commentPopupView
{
    if (_commentPopupView == nil) {
        _commentPopupView = [CommentPopupView new];
        _commentPopupView.delegate = self;
    }
    return _commentPopupView;
}


- (NSMutableArray*)commentList
{
    if (_commentList == nil) {
        _commentList = [NSMutableArray array];
    }
    return _commentList;
}

- (ProviderDetailViewModel*)viewModel
{
    if (_viewModel == nil) {
        _viewModel = [[ProviderDetailViewModel alloc] init];
    }
    return _viewModel;
}

-(UIImageView*)shareImageView
{
    if (!_shareImageView) {
        _shareImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _shareImageView.hidden = YES;
    }
    return _shareImageView;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.tableHeaderView = [UIView new];
    self.tableView.pullRefreshDelegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIBarButtonItem *shareButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_share2"] style:UIBarButtonItemStyleDone target:self action:@selector(onClickShareButton:)];
    UIBarButtonItem *favoriteButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(onClickFavoriteButton:)];
    
    [self.navigationItem setRightBarButtonItems:@[favoriteButtonItem,shareButtonItem]];
    
    [self.backButton addTarget:self action:@selector(pressedBackButton) forControlEvents:UIControlEventTouchUpInside];
    [self.shareButton addTarget:self action:@selector(onClickShareButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.favoriteButton addTarget:self action:@selector(onClickFavoriteButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bookButton setImage:[UIImage imageNamed:@"icon_btn_comment"] forState:UIControlStateNormal];
    
    [self.commentButton setImage:[UIImage imageNamed:@"chat_unselected"] forState:UIControlStateNormal];
    [self.commentButton setImage:[UIImage imageNamed:@"chat_selected"] forState:UIControlStateHighlighted];


}

- (void)setNavigationView
{
    if (_viewModel) {
        
        UIBarButtonItem *shareButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_share2"] style:UIBarButtonItemStyleDone target:self action:@selector(onClickShareButton:)];
        UIBarButtonItem *favoriteButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_favorite"] style:UIBarButtonItemStyleDone target:self action:@selector(onClickFavoriteButton:)];
        [self.favoriteButton setBackgroundImage:[UIImage imageNamed:@"icon_favorite"] forState:UIControlStateNormal];
        
        if ([_viewModel.detailModel.collection boolValue]) {
            favoriteButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_favorite_focused"] style:UIBarButtonItemStyleDone target:self action:@selector(onClickFavoriteButton:)];
            [self.favoriteButton setBackgroundImage:[UIImage imageNamed:@"icon_favorite_focused"] forState:UIControlStateNormal];
        }
        
        [self.navigationItem setRightBarButtonItems:@[favoriteButtonItem, shareButtonItem]];
        
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.topbarView.backgroundColor = ColorFromRGBWithAlpha(0x030b1e, 0.4);

    self.navigationController.navigationBar.hidden = YES;
    
    self.bookButton.hidden = NO;
    
    [self doQueryProviderDetail];
    
    [self fetchCommentList:@""];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.topbarView.frame = CGRectMake(0, 0, ViewWidth, 70*Scale);
    
    self.backButton.frame = CGRectMake(12*Scale, 35*Scale, 12*Scale, 21*Scale);
    
    self.shareButton.frame = CGRectMake(ViewWidth-76*Scale, 35*Scale, 21*Scale, 21*Scale);
    
    self.favoriteButton.frame = CGRectMake(ViewWidth-34*Scale, 35*Scale, 21*Scale, 21*Scale);
    
//    self.telButton.frame = CGRectMake(0, 0, ViewWidth/2-0.5, 50*Scale);
//    
//    self.commentButton.frame = CGRectMake(ViewWidth/2, 0, ViewWidth/2, 50*Scale);

}

- (void)onHeaderRefresh
{
    [self saveNextAction:@selector(onHeaderRefresh) object1:nil object2:nil];
    
    [self doQueryProviderDetail];
    [self fetchCommentList:@""];
}

- (void)onFooterRefresh
{
    
    if (self.commentList.count == 0) {
//        [self.tableView endFooterRefresh];
        [self fetchCommentList:@""];
        return;
    }
    
    ProviderCommentViewModel *commentViewModel = [self.commentList lastObject];
    if (commentViewModel.commentModel) {
        [self fetchCommentList:commentViewModel.commentModel.createTime];
    }
    
}

#pragma mark - 分享

- (void)onClickShareButton:(id)sender
{
    WeakSelf
    NSDictionary *params = @{@"companyId":self.viewModel.detailModel.companyId};
    // 根据路径、来源以及自定义参数构造scene
    MLSDKScene *scene = [[MLSDKScene alloc] initWithMLSDKPath:PROVIDER_DETAIL_PATH source:@"iYONG-Provider" params:params];
    [MobLink getMobId:scene result:^(NSString *mobid) {
        if (mobid != nil) {
            NSString *shareUrl = [NSString stringWithFormat:@"%@%@", PROVIDER_DETAIL_SHARE_URL, weakSelf.viewModel.detailModel.companyId];
            NSString *title = @"【iYONG】我已入驻iYONG-快来帮我点赞";
            [ShareUtil startShare:shareUrl title:title desc:weakSelf.viewModel.detailModel.blurb icon:weakSelf.shareImageView.image webpageUrl:shareUrl];
        }
    }];
    
}

#pragma mark - 收藏

- (void)onClickFavoriteButton:(id)sender
{
    self.nextAction = @selector(onClickFavoriteButton:);
    self.object1 = sender;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
        // 判断是否自己收藏自己
        if ([[UserManager sharedInstance] isSelfDispatch:self.viewModel.detailModel.companyId]) {
            [MsgToolBox showToast:getErrorMsg(1853)];
//            [MsgToolBox showToast:@"不能自己收藏自己"];
            return;
        }
    
        BOOL isFavorited = [self.viewModel.detailModel.collection boolValue];
    
        if (isFavorited) {
            // 取消收藏
            [self cancelFavorite];
//            [self cancelFavorite:viewModel favoriteView:favoriteView];
        }
        else{
            // 加入收藏
            [self addFavorite];
//            [self addFavorite:viewModel favoriteView:favoriteView];
        }
    
//    [self addFavorite];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 响应通话和预定按钮

- (IBAction)bookButton:(id)sender {
    
//    [self loginWithUsername:@"13761122705" password:@"13761122705"];
    
//    EaseUserModel *model = [[self.dataArray objectAtIndex:(section - 1)] objectAtIndex:row];
//    UIViewController *chatController = nil;
//    chatController = [[ChatViewController alloc] initWithConversationChatter:@"wyy" conversationType:EMConversationTypeChat];
////#endif
//    chatController.title = @"wyy";
////    chatController.title = model.nickname.length > 0 ? model.nickname : model.buddy;
//    [self.navigationController pushViewController:chatController animated:YES];
    
//    self.nextAction = @selector(bookButton:);
//    self.object1 = nil;
//    self.object2 = nil;
    
    // 判断用户是否已经登录
    if ([self needLogin]) {
        return;
    }
    
//    [self resetAction];
    
    NSString *conversationId = self.viewModel.detailModel.companyId;
    ChatViewController *viewController = [[ChatViewController alloc] initWithConversationChatter:conversationId conversationType:EMConversationTypeChat];
    viewController.title = self.viewModel.detailModel.companyName;
    viewController.toNickName = self.viewModel.detailModel.companyName;
    viewController.toAvatar = [StringUtil getSafeString:self.viewModel.detailModel.logoUrl];//self.viewModel.detailModel.logoUrl;
    [self.navigationController pushViewController:viewController animated:YES];
}


- (IBAction)onClickTelButton:(id)sender {
    
//    if (![[UserManager sharedInstance] isLogin]) {
//        [self showLoginAlertViewWithMsg:@"登录且实名认证的用户方可与服务商通话"];
//        return;
//    }
    if ([self needLogin]) {
        return;
    }
    
    // 没有实名认证
//    if (![UserManager sharedInstance].userModel.isRealnameCertified) {
//        [MsgToolBox showAlert:@"" content:@"请先进行实名认证！"];
//        return;
//    }
    
    [self callMobile];
}

//-(void)callMobile
//{
//    NSString *mobile = self.carModel.mobile.trim;
//    if (!IsStrEmpty(mobile)) {
//        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
//                                                                 delegate:self
//                                                        cancelButtonTitle:@"取消"
//                                                   destructiveButtonTitle:nil
//                                                        otherButtonTitles:mobile,nil];
//        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
//        //        actionSheet.tag = ActionSheetTagDriverMobile;
//        [actionSheet showInView:self.view];
//    }
//    
//}

-(void)callMobile
{
    NSString *mobile = self.viewModel.detailModel.mobile;
    if (!IsStrEmpty(mobile)) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"取消"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:mobile,nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
        //        actionSheet.tag = ActionSheetTagDriverMobile;
        [actionSheet showInView:self.view];
    }
    
}

#pragma mark - UIActionSheet Delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0) {
        NSString *mobile = self.viewModel.detailModel.mobile;
        NSString *num = [NSString stringWithFormat:@"tel://%@", mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]];
    }
}



- (IBAction)onClickBookButton:(id)sender {
    
    [self gotoPage:@"NewOrderViewController"];

}

- (IBAction)onClickCommentButton:(id)sender {
    
    // 判断用户是否已经登录
    if ([self needLogin]) {
        return;
    }

    [self.commentPopupView addComment];
    
}

- (void)onGetText:(NSString *)text
{
    [self uploadComment:text];
}

#pragma mark - 点击点赞按钮

- (void)onClickLikeButon:(id)sender
{
    self.nextAction = @selector(onClickLikeButon:);
    self.object1 = sender;
    self.object2 = nil;
    
    
    if ([self needLogin]) {
        return;
    }
    
    [self addLikeNumber];
    
}


#pragma mark - UITableView Delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.00001f;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.0001f;
//}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ProviderSectionNumber;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == ProviderCellCompanyDiscuss) {
        return self.commentList.count;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == ProviderCellAd) {
        return [ProviderHeaderTableViewCell getCellHeight];
    }
    if (indexPath.section == ProviderCellCompanyInfo) {
        return [ProviderDetailTableViewCell getCellHeight];
    }
    if (indexPath.section == ProviderCellCompanyType) {
        return [ProviderServiceTypeTableViewCell getCellHeight];
    }
    if (indexPath.section == ProviderCellCompanyDescription) {
        return self.viewModel.companyDescriptionSectionHeight;
    }
    if (indexPath.section == ProviderCellCompanyDiscuss) {
        
        ProviderCommentViewModel *viewModel = self.commentList[indexPath.row];
        return viewModel.cellHeight;  
        
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == ProviderCellCompanyDiscuss) {
        return 40;
    }
    return 0;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == ProviderCellCompanyDiscuss) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(12, 0, ViewWidth-24, 30)];
        view.backgroundColor = [UIColor whiteColor];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(24, 15, 2, 17)];
        bgView.backgroundColor = NAVBAR_BGCOLOR;
        [view addSubview:bgView];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, bgView.top-1, ViewWidth, view.height)];
        titleLabel.font = FONT(17);
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = @"用户评论";
        titleLabel.backgroundColor = [UIColor clearColor];
        [titleLabel sizeToFit];
        [view addSubview:titleLabel];
        
        return view;
        
    }
    
    return [UIView new];
}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProviderDetailModel *data = self.viewModel.detailModel;
    // 广告条
    if (indexPath.section == 0) {
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%lu%lu", (long)indexPath.section, (long)indexPath.row];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            _bannerView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, [ProviderHeaderTableViewCell getCellHeight])];
            _bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
            _bannerView.delegate = self;
//            _bannerView.displayType =
//            _bannerView.currentPageDotColor = [UIColor whiteColor];
            _bannerView.currentPageDotColor =  NAVBAR_BGCOLOR;
//            _bannerView.currentPageDotColor = [UIColor whiteColor];
            _bannerView.downloadFromAliyunOSS = YES;
            _bannerView.displayType = SDCycleScrollViewDisplayTypeProvider;
//            _bannerView.placeholderImage = [UIImage imageNamed:@"banner_placeholder_image"];
            [cell.contentView addSubview:_bannerView];
            
        }
        
        _bannerView.aliyunOSSImageArray = self.viewModel.detailModel.imgs;
        
        return cell;
    }
    // 公司简介
    if (indexPath.section == ProviderCellCompanyInfo) {
        ProviderDetailTableViewCell *cell = [ProviderDetailTableViewCell cellWithTableView:tableView indexPath:indexPath];
        
        cell.viewModel = self.viewModel;
        
        [cell.likeButton addTarget:self action:@selector(onClickLikeButon:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    // 服务类型
    if (indexPath.section == ProviderCellCompanyType) {
        ProviderServiceTypeTableViewCell *cell = [[ProviderServiceTypeTableViewCell alloc] initWithStyle:tableView style:UITableViewCellStyleDefault reuseIdentifier:@"ProviderServiceTypeTableViewCellId"];
        
        if (data.business.count > 0) {
            
            for (NSInteger i = 0; i < data.business.count; i++){
                
                NSInteger businessType = [data.business[i] integerValue];
                NSArray *subviews = [cell.contentView subviews];
                for (UIImageView *imageView in subviews) {
                    if (imageView.tag == businessType) {
                        [imageView setHighlighted:YES];
                    }
                }
                for (UILabel *label in subviews) {
                    if (label.tag == businessType*1000) {
                        [label setHighlighted:YES];
                    }
                }
                
            }
        }
        
        return cell;
    }
    // 公司详情
    if (indexPath.section == ProviderCellCompanyDescription) {
        ProviderDescriptionTableViewCell *cell = [ProviderDescriptionTableViewCell cellWithTableView:tableView indexPath:indexPath];
        cell.viewModel = self.viewModel;
        
        return cell;
    }
    // 评论
    if (indexPath.section == ProviderCellCompanyDiscuss) {
        
        ProviderCommentViewModel *viewModel = self.commentList[indexPath.row];
        CompanyCommentTableViewCell *cell = [CompanyCommentTableViewCell momentsTableViewCellWithTableView:tableView];
        [cell setViewModel:viewModel];
        
        return cell;
    }
    
    return nil;
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index;
{
//    if (index == 0) {
//        if (_shareImage == nil) {
//            SDCollectionViewCell *collectionViewCell = (SDCollectionViewCell*)[cycleScrollView.mainView cellForItemAtIndexPath:[NSIndexPath indexPathWithIndex:0]];
//            if (collectionViewCell.imageView.image) {
//                _shareImage = collectionViewCell.imageView.image;
//            }
//        }
//    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
////    NSLog(@"offset---scroll:%f",self.myTableView.contentOffset.y);
////    DLog(@"offset====scroll: %f". self)
//    
////    DLog(@"offset ==== scroll: %f", self.tableView.contentOffset.y);
//    UIColor *color=[UIColor redColor];
//    CGFloat offset=scrollView.contentOffset.y;
//    if (offset<0) {
//        self.navigationController.navigationBar.backgroundColor = [color colorWithAlphaComponent:0];
//    }else {
//        CGFloat alpha=1-((64-offset)/64);
//        self.navigationController.navigationBar.backgroundColor=[color colorWithAlphaComponent:alpha];
//    }
//}



@end
