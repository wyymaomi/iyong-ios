//
//  VechilrOrderPayViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileOrderPayViewController.h"
#import "AdPurchaseHttpMessage.h"
#import "AdvAccountPayTableViewCell.h"
#import "VechileOrderPayViewController+Business.h"

@interface VechileOrderPayViewController ()<PayDelegate>



@property (nonatomic, strong) NSArray *rechargeTypeList;

@end

@implementation VechileOrderPayViewController


-(TravelorderPayHttpMessage*)payHttpMessage
{
    if (!_payHttpMessage) {
        _payHttpMessage = [TravelorderPayHttpMessage new];
        _payHttpMessage.payMode = [NSNumber numberWithInteger:AdPayTypeMyAccount];
    }
    return _payHttpMessage;
}

- (NSArray*)rechargeTypeList
{
    if (!_rechargeTypeList) {
        _rechargeTypeList = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RechrageType.plist" ofType:nil]];
    }
    
    return _rechargeTypeList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"支付";
    self.payDelegate = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"支付说明" style:(UIBarButtonItemStylePlain) target:self action:@selector(gotoPriceExplainWebpage)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onThirdPayNotification:) name:kAdvPurchaseSuccessNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onThirdPayFailureNotification:) name:kAdvPurchaseFailureNotification object:nil];
    
    [self.view addSubview:self.groupTableView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self getFinanceInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    if (self.mode == PayModeAdv) {
//        if (section == [tableView numberOfSections]-1) {
//            //        return self.rechargeTypeList.count;
//            return 2;
//        }
//        return 2;
//        //    return 1;
//    }
//    else {
        if (section == [tableView numberOfSections]-1) {
            return 2;
        }
        return 1;
//    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        return 250;
    }
    return 10;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView] - 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 250)];
//        UIButton *payButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        //        [payButton setBackgroundColor:[UIColor redColor]];
//        [payButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_pay"] forState:UIControlStateNormal];
//        [payButton addTarget:self action:@selector(doAdPurchase) forControlEvents:UIControlEventTouchUpInside];
//        payButton.frame = CGRectMake(58, 160, ViewWidth-58*2, 57.5*Scale);
//        [view addSubview:payButton];
        
        UIButton *payButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [payButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [payButton setTitle:@"确认付款" forState:UIControlStateNormal];
        payButton.cornerRadius = 18;
        [payButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateNormal];
        [payButton addTarget:self action:@selector(doAdPurchase) forControlEvents:UIControlEventTouchUpInside];
        payButton.frame = CGRectMake(10*Scale, 70*Scale, ViewWidth-20*Scale, 42);
        payButton.titleLabel.font = FONT(16);
        [view addSubview:payButton];
        
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(25, 10, ViewWidth-25, 16)];
//        label.text = @"支付金额：";
//        label.font = FONT(16);
//        label.textColor = UIColorFromRGB(0xce5a5a);
//        [view addSubview:label];
        
        UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ViewWidth-20, 38*Scale)];
        //        moneyLabel.text = [NSString stringWithFormat:@"%.2f元", self.httpMessage.]
        moneyLabel.textColor = UIColorFromRGB(0xce5a5a);
        CGFloat price = [VechileOrderManager sharedInstance].vechileOrderHttpMessage.carPrice;
        moneyLabel.text = [NSString stringWithFormat:@"支付金额：%.f元", price];
        moneyLabel.font = FONT(16);
        moneyLabel.textAlignment = UITextAlignmentRight;
        [view addSubview:moneyLabel];
        
//        UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10*Scale, payButton.bottom+10, ViewWidth-20*Scale, 21)];
//        infoLabel.text = @"*预约时间前30分钟取消订单将扣除订单金额的10%";
//        infoLabel.textColor = NAVBAR_BGCOLOR;
//        infoLabel.font = FONT(12);
//        infoLabel.backgroundColor = [UIColor clearColor];
////        CGFloat textWidth = [infoLabel.text textSize:CGSizeMake((ViewWidth-20*Scale), 10000) font:infoLabel.font].width;
//        infoLabel.numberOfLines = 0;
//        infoLabel.textAlignment = UITextAlignmentCenter;
////        infoLabel.width = textWidth;
//        [view addSubview:infoLabel];
        
        return view;
    }
    
    return [UIView new];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger adPayType = [self.payHttpMessage.payMode integerValue];
    
    if (indexPath.section == 0) {
        
        AdvAccountPayTableViewCell *cell = [AdvAccountPayTableViewCell cellWithTableView:tableView indexPath:indexPath];
        
//        if (indexPath.row == AdPayRowPromotionAccount) {
//            cell.radioButton.selected = adPayType == AdPayTypePromotionAccount ? YES : NO;
//            cell.amountLabel.text = [NSString stringWithFormat:@"%.1f元", [self.promotionAccountMoney floatValue]];
//        }
        if (indexPath.row == AdPayRowMyAccount) {
            cell.titleLabel.text = @"我的账户";
            cell.radioButton.selected = adPayType == AdPayTypeMyAccount ? YES : NO;
            cell.amountLabel.text = [NSString stringWithFormat:@"%.1f元", [self.myAccountMoney floatValue]];
        }
        cell.radioButton.tag = indexPath.section * 1000 + indexPath.row;
        [cell.radioButton addTarget:self action:@selector(onClickRadioButton:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    if (indexPath.section == [tableView numberOfSections]-1) {
        
        AdvPayTableViewCell *cell = [AdvPayTableViewCell cellWithTableView:tableView indexPath:indexPath];
        
        if (indexPath.row == AdThirdPayRowAlipay) {
            cell.radioButton.selected = adPayType == AdPayTypeAlipay ? YES : NO;
        }
        if (indexPath.row == AdThirdPayRowWechat) {
            cell.radioButton.selected = adPayType == AdPayTypeWechat ? YES : NO;
        }
        
        cell.radioButton.tag = indexPath.section * 1000 + indexPath.row;
        [cell.radioButton addTarget:self action:@selector(onClickRadioButton:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        //        self.payType = indexPath.row;
        [self.groupTableView reloadData];
//        if (indexPath.row == AdPayRowPromotionAccount) {
//            //            self.payType = AdPayTypePromotionAccount;
//            self.httpMessage.payMode = [NSNumber numberWithInteger:AdPayTypePromotionAccount];
//        }
        if (indexPath.row == AdPayRowMyAccount) {
            //            self.payType = AdPayTypeMyAccount;
            self.payHttpMessage.payMode = [NSNumber numberWithInteger:AdPayTypeMyAccount];
        }
    }
    if (indexPath.section == [tableView numberOfSections] -1) {
        //        self.payType = indexPath.row+2;
        [self.groupTableView reloadData];
        if (indexPath.row == AdThirdPayRowAlipay) {
            //            self.payType = AdPayTypeAlipay;
            self.payHttpMessage.payMode = [NSNumber numberWithInteger:AdPayTypeAlipay];
        }
        else if (indexPath.row == AdThirdPayRowWechat) {
            //            self.payType = AdPayTypeWechat;
            self.payHttpMessage.payMode = [NSNumber numberWithInteger:AdPayTypeWechat];
        }
    }
    
}

- (void)onClickRadioButton:(UIButton*)button
{
    NSInteger tag = button.tag;
    NSInteger section = tag/1000;
    NSInteger row = tag%1000;
    //    self tableView
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    [self tableView:self.groupTableView didSelectRowAtIndexPath:indexPath];
}

-(void)gotoPriceExplainWebpage
{
    NSString *url = [NSString stringWithFormat:@"%@%@", RESOURSE_WEB_PAGE, PAY_EXPLAIN_HTML];
    [self openWebView:url];
}


@end
