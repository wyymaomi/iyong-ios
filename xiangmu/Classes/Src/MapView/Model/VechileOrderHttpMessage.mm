//
//  VechileOrderHttpMessage.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileOrderHttpMessage.h"

@implementation VechileOrderHttpMessage

-(id)init
{
    self = [super init];
    if (self) {
        self.carType = DEFAULT_CAR_TYPE;
        self.useCarMode = CarModeImmediate;
    }
    return self;
}

@end
