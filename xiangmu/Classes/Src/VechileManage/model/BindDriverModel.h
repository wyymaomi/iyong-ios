//
//  BindDriverModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

//id 邀请绑定信息ID
//name 姓名
//carNumber 车牌号
//model 车型
//imgUrl 车辆图片地址
//status 绑定状态(1=未处理，2=已接受，3=已拒绝)
//createTime
//curMonthTravel 当月行程总公里数

@interface BindDriverModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *username;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *carNumber;
@property (nonatomic, strong) NSString<Optional> *model;
@property (nonatomic, strong) NSString<Optional> *imgUrl;
@property (nonatomic, strong) NSNumber<Optional> *status;//(1=未处理，2=已接受，3=已拒绝)
@property (nonatomic, strong) NSString<Optional> *createTime;
@property (nonatomic, strong) NSNumber<Optional> *curMonthTravel;
@property (nonatomic, strong) NSString<Optional> *driverId; 

@end
