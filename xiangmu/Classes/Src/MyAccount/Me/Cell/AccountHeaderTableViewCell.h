//
//  AccountHeaderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIButton *editUserInfoButton;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+(CGFloat)getCellHeight;

@end
