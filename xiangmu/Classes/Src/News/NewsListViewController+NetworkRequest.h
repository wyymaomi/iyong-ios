//
//  NewsListViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewsListViewController.h"

@interface NewsListViewController (NetworkRequest)

-(void)fetchNewsListByCreateTime:(NSString*)createTime;

//-(void)fetchNewsListByPageIndex:(NSInteger)pageIndex;

//- (void)fetchExperienceListByPageIndex:(NSInteger)pageIndex;
-(void)fetchNewsListByPageIndex:(NSInteger)pageIndex newsId:(NSString*)newsId;

@end
