//
//  CommunityHeaderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarView.h"
#import "CertifiedView.h"

//@class CertifiedView;
//@class StarView;

@interface CommunityHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet StarView *starView;
@property (weak, nonatomic) IBOutlet CertifiedView *certifiedView;
@property (weak, nonatomic) IBOutlet UIImageView *avartarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
-(void)initData;

@end
