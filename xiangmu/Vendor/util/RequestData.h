//
//  RequestData.h
//  发送验证码
//
//  Created by bww on 15/9/22.
//  Copyright © 2015年 bww. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^MyCallBack)(id obj);

@interface RequestData : NSObject


//验证码请求
+(void)requestVerificationCodeWithDic:(NSDictionary *)dic CallBack:(MyCallBack)callBack;
//注册请求
+(void)requestRegisterWithDic:(NSDictionary *)dic CallBack:(MyCallBack)callBack;


@end
