//
//  MyUtil.h
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface MyUtil : NSObject
+(UILabel *)createLabelFrame:(CGRect)frame title:(NSString *)title font:(UIFont *)font textAlignment:(NSTextAlignment)alignment numberOfLines:(NSInteger)numberOfLines textColor:(UIColor *)textColor;
+(UIButton *)createBtnFrame:(CGRect)frame title:(NSString *)title bgImageName:(NSString *)bgImageName target:(id)target action:(SEL)action;

+(UIImageView *)createImageViewFrame:(CGRect)frame image:(NSString *)image;

+(NSArray*)cityList;

+(NSString*)getCityCodeFromCityName:(NSString*)cityName;

+(NSString*)getCityShortnameFromCityCode:(NSString*)cityCode;

+(NSString*)homeDisplayCityName:(NSString*)cityName;

@end
