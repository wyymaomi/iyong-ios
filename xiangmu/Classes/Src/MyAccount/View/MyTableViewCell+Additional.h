//
//  MyTableViewCell+Additional.h
//  xiangmu
//
//  Created by David kim on 16/9/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyTableViewCell.h"

@interface MyTableViewCell (Additional)

- (void)initLevelData;

- (void)initCertificationData;

- (void)initRealnameCertifcationData;

- (void)initCompanyCertificationData;

- (void)initVechileCertificationData;

- (void)initServiceProviderCertificationData;


@end
