//
//  NSString+Category.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NSString+Category.h"

@implementation NSString (Cateogry)

#define JavaNotFound -1

/**  Java-like method. Returns the char value at the specified index. */
- (unichar) charAt:(NSUInteger)index {
    return [self characterAtIndex:index];
}

/**
 * Java-like method. Compares two strings lexicographically.
 * the value 0 if the argument string is equal to this string;
 * a value less than 0 if this string is lexicographically less than the string argument;
 * and a value greater than 0 if this string is lexicographically greater than the string argument.
 */
- (NSInteger) compareTo:(NSString*) anotherString {
    return [self compare:anotherString];
}

/** Java-like method. Compares two strings lexicographically, ignoring case differences. */
- (NSInteger) compareToIgnoreCase:(NSString*) str {
    return [self compare:str options:NSCaseInsensitiveSearch];
}

/** Java-like method. Returns true if and only if this string contains the specified sequence of char values. */
- (BOOL) contains:(NSString*) str {
    if (str == nil) return NO;
    return ([self rangeOfString:str].location != NSNotFound);
}

- (BOOL) startsWith:(NSString*)prefix {
    if (prefix == nil) {
        return NO;
    }
    return [self hasPrefix:prefix];
}

- (BOOL) endsWith:(NSString*)suffix {
    return [self hasSuffix:suffix];
}

- (BOOL) equals:(NSString*) anotherString {
    return [self isEqualToString:anotherString];
}

- (BOOL) equalsIgnoreCase:(NSString*) anotherString {
    return [[self toLowerCase] equals:[anotherString toLowerCase]];
}

- (NSInteger) indexOfChar:(unichar)ch{
    return [self indexOfChar:ch fromIndex:0];
}

- (NSInteger) indexOfChar:(unichar)ch fromIndex:(NSUInteger)index{
    NSInteger len = self.length;
    for (NSInteger i = index; i < len; ++i) {
        if (ch == [self charAt:i]) {
            return i;
        }
    }
    return JavaNotFound;
}

- (NSInteger) indexOfString:(NSString*)str {
    if (str == nil) {
        return 0;
    }
    NSRange range = [self rangeOfString:str];
    if (range.location == NSNotFound) {
        return JavaNotFound;
    }
    return range.location;
}

- (NSInteger) indexOfString:(NSString*)str fromIndex:(NSUInteger)index {
    NSRange fromRange = NSMakeRange(index, self.length - index);
    NSRange range = [self rangeOfString:str options:NSLiteralSearch range:fromRange];
    if (range.location == NSNotFound) {
        return JavaNotFound;
    }
    return range.location;
}

- (NSInteger) lastIndexOfChar:(unichar)ch {
    NSInteger len = self.length;
    for (NSInteger i = len-1; i >=0; --i) {
        if ([self charAt:i] == ch) {
            return i;
        }
    }
    return JavaNotFound;
}

- (NSInteger) lastIndexOfChar:(unichar)ch fromIndex:(NSUInteger)index {
    NSInteger len = self.length;
    if (index >= len) {
        index = len - 1;
    }
    for (NSInteger i = index; i >= 0; --i) {
        if ([self charAt:i] == ch) {
            return index;
        }
    }
    return JavaNotFound;
}

- (NSInteger) lastIndexOfString:(NSString*)str {
    if (str == nil) {
        return 0;
    }
    NSRange range = [self rangeOfString:str options:NSBackwardsSearch];
    if (range.location == NSNotFound) {
        return JavaNotFound;
    }
    return range.location;
}

- (NSInteger) lastIndexOfString:(NSString*)str fromIndex:(NSUInteger)index {
    NSRange fromRange = NSMakeRange(0, index);
    NSRange range = [self rangeOfString:str options:NSBackwardsSearch range:fromRange];
    if (range.location == NSNotFound) {
        return JavaNotFound;
    }
    return range.location;
}

- (NSString *) substringFromIndex:(NSUInteger)beginIndex toIndex:(NSUInteger)endIndex {
    if (endIndex <= beginIndex) {
        return @"";
    }
    NSRange range = NSMakeRange(beginIndex, endIndex - beginIndex);
    return [self substringWithRange:range];
}

- (NSString *) toLowerCase {
    return [self lowercaseString];
}

- (NSString *) toUpperCase {
    if (!self) {
        return @"";
    }
    return [self uppercaseString];
}

- (NSString *) trim {
    if (!self) {
        return @"";
    }
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *) replaceAll:(NSString*)origin with:(NSString*)replacement {
    if (!self) {
        return @"";
    }
    return [self stringByReplacingOccurrencesOfString:origin withString:replacement];
}

- (NSArray *) split:(NSString*) separator {
    return [self componentsSeparatedByString:separator];
}


-(NSString*)getDateTime/*:(NSTimeInterval)millionTimeInterval;*/
{
    NSTimeInterval timeInterval = [self doubleValue] / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateFormatter stringFromDate:date];
}

// 时间戳转换为日期
-(NSString*)getDateTime:(NSString*)dateFormat
{
    NSTimeInterval timeInterval = [self doubleValue] / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter stringFromDate:date];
}

+(NSString*)getTimeStamp
{
    return [NSString stringWithFormat:@"%.f", [[NSDate date] timeIntervalSince1970] * 1000];
}

- (CGSize)textSize:(CGSize)size font:(UIFont*)font;
{
    if (self.length == 0) {
        return CGSizeMake(0, 0);
    }
    
    return [self boundingRectWithSize:size
                              options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                           attributes:@{NSFontAttributeName:font}
                              context:nil].size;
}

- (NSString *)replaceStringWithAsterisk:(NSInteger)startLocation length:(NSInteger)length
{
    NSString *replaceStr = self;
    for (NSInteger i = 0; i < length; i++) {
        NSRange range = NSMakeRange(startLocation, 1);
        replaceStr = [replaceStr stringByReplacingCharactersInRange:range withString:@"*"];
        startLocation ++;
    }
    return replaceStr;
}

//+ (BOOL)validatePhoneNum:(NSString *)phoneNum error:(NSString **)errorMsg
//{
//    if (phoneNum == nil || [phoneNum isEqualToString:@""]) {
//        *errorMsg = @"请输入手机号";
//        return NO;
//    }
//    
//    if (phoneNum.length != 11) {
//        *errorMsg = @"请输入11位手机号";
//        return NO;
//    }
//    
//    NSString *mobileNoRegex = @"1[0-9]{10,10}";
//    NSPredicate *mobileNoTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNoRegex];
//    BOOL validateOK = [mobileNoTest evaluateWithObject:phoneNum];
//    
//    if (!validateOK) {
//        *errorMsg = @"手机号格式错误";
//        return NO;
//    }
//    
//    return YES;
//}





@end
