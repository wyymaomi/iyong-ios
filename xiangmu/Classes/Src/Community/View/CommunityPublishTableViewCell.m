//
//  CommunityPublishTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityPublishTableViewCell.h"

@implementation CommunityPublishTableViewCell


+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"CommunityPublishTableViewCell";
    CommunityPublishTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;

    }
    return cell;
}





- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
