//
//  VechileUtils.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "VechileUtils.h"

@implementation VechileUtils


+ (BOOL)validateCarId:(NSString*)carId error:(NSString **)errorMsg;
{
    if (carId == nil || carId.length == 0) {
        *errorMsg = @"请输入车牌号";
        return NO;
    }
    
    NSString *carIdRegex = @"^[\u4e00-\u9fa5]{1}[A-Z]{1}[A-Z_0-9]{5}$";
    NSPredicate *carIdPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", carIdRegex];
    BOOL validateOK = [carIdPredicate evaluateWithObject:carId];
    
    if (!validateOK) {
        *errorMsg = @"车牌号格式错误，请重新输入";
        return NO;
    }
    
    return YES;
}

@end
