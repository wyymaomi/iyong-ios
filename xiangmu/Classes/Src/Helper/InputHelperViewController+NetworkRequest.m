//
//  InputHelperViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InputHelperViewController+NetworkRequest.h"

@implementation InputHelperViewController (NetworkRequest)

-(void)updateUserInfo:(NSInteger)tag text:(NSString*)text;
{
    NSString *params;
    if (tag == MyNicknameTextField) {
        params = [NSString stringWithFormat:@"nickname=%@", text];
    }
    
    [self showHUDIndicatorViewAtCenter:@"正在更新昵称，请稍候"];
//    __weak __typeof(self) weakSelf = self;
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(UserInfo_Update_API) requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
//        __strong __typeof(self) strongSelf = weakSelf;
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"更新昵称成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(onTextDidChanged:text:)]) {
                        [strongSelf.delegate onTextDidChanged:self.tag text:self.textField.text.trim];
                        
                    }
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
                
            }];
        }
        else {
            [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [MsgToolBox showAlert:@"" content:errorDesc];
        
    }];
}

@end
