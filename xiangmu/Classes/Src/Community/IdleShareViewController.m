//
//  IdleShareViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "IdleShareViewController.h"
#import "CommunityPublishViewController.h"
#import "IdleShareTableViewCell.h"

@interface IdleShareViewController ()

@end

@implementation IdleShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(publish)];
    [self.view addSubview:self.pullRefreshTableView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 业务方法

- (void)publish
{
    CommunityPublishViewController *viewController = [[CommunityPublishViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
//    return self.dataList.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [IdleShareTableViewCell getCellHeight];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IdleShareTableViewCell *cell = [IdleShareTableViewCell cellWithTableView:tableView indexPath:indexPath];
    
    return cell;

}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//    //    [self gotoOrderDetailPage:indexPath.section];
//
//}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}



@end
