//
//  SDKAlipayServiceImpl.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDKRegisterService.h"
#import "SDKPayService.h"

//#import 
//#import "SDK"

@interface SDKAlipayServiceImpl : NSObject<SDKRegisterService,SDKPayService>

@end
