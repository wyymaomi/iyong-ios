//
//  RechargeLimitionDescriptionViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RechargeLimitionDescriptionViewController.h"
#import "OrderHelpDetailTableViewCell.h"

@interface RechargeLimitionDescriptionViewController ()

@property (nonatomic, strong) NSString *alipayDescription;

@end

@implementation RechargeLimitionDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"限额说明";
    [self.view addSubview:self.groupTableView];
    self.alipayDescription = @"余额的支付限额由帐户认证等级等因素确定，转账信息提交后会确定是否超限。\n银行卡的支付限额是由银行设定，若提示超限，建议更换其他付款方式。";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderHelpDetailTableViewCell *cell = [OrderHelpDetailTableViewCell cellWithTableView:tableView];
    
    if (indexPath.section == RechargeLimitRowAlipay){
        
        cell.titleLabel.text = @"支付宝限额说明";
        cell.detailLabel.text = self.alipayDescription;
        
    }
    
    if (indexPath.section == RechargeLimitRowWepay) {
        cell.detailLabel.text = @"";
        cell.titleLabel.text = @"微信支付限额说明";
        
        UIImageView *imageView = [cell.contentView viewWithTag:100];
        if (!imageView) {
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 60,ViewWidth-10, 400*ViewWidth/320)];
            imageView.image = [UIImage imageNamed:@"img_wechat"];
            imageView.tag = 100;
            [cell.contentView addSubview:imageView];
        }
        
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == RechargeLimitRowAlipay) {
        CGFloat height = [StringUtil heightForString:self.alipayDescription fontSize:14 andWidth:(tableView.frame.size.width-30)]+45+30;
        return height;
    }
    
    return 400 * ViewWidth / 320 + 100;
}


@end
