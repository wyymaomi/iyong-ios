//
//  CustomTabBarViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabbarDelegate.h"

@interface CustomTabBarViewController : UITabBarController<TabbarDelegate>
{
    EMConnectionState _connectionState;
}

@property (nonatomic, strong) UINavigationController *mapNavigation;

@property (nonatomic, strong) UINavigationController *companySettleNavigation;

@property (nonatomic, strong) UINavigationController *meNavigation;

@property (nonatomic, assign) NSInteger tabIndex;

@property (strong, nonatomic) NSDate *lastPlaySoundDate;

@property (nonatomic, strong) NSArray *tabConfigArray;

@end
