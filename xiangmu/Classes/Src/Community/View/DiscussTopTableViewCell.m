//
//  DiscussTopTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "DiscussTopTableViewCell.h"

@implementation DiscussTopTableViewCell

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    static NSString *ID = @"DiscussTopTableViewCell";
    DiscussTopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        
    }
    return cell;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
