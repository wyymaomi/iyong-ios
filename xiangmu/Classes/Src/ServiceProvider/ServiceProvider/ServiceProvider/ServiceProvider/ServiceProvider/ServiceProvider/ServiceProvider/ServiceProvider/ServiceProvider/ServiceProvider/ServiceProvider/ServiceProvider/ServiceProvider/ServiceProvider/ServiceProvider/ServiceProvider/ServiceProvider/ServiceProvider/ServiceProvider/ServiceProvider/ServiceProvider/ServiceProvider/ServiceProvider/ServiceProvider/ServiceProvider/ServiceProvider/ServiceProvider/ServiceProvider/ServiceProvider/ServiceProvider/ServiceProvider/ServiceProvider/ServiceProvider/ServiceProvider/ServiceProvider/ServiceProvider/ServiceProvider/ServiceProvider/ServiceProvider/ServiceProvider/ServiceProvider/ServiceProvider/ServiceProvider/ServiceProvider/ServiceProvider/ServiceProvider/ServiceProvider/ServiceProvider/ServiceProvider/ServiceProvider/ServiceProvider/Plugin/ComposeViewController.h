//
//  ComposeViewController.h
//  ArtSky
//
//  Created by Adam on 16/4/12.
//  Copyright © 2016年 fule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ComposeSentDelegate <NSObject>

- (void) handleComposeSent;

@end

@interface ComposeViewController : BaseViewController

@property (nonatomic, assign) id<ComposeSentDelegate> composeSendDelegate;

//@property (nonatomic, copy) void (^mComposetSentEventBlock)(BOOL flag);

@end
