//
//  ServiceProviderListController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ServiceProviderListController.h"

@interface ServiceProviderListController (NetworkRequest)

- (void)fetchServiceProviderByPageIndex:(NSInteger)pageIndex;

- (void)fetchMyFavoriteList:/*(NSString*)quantity time:*/(NSString*)time;

@end
