//
//  VechileOrderPayViewController+Business.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileOrderPayViewController.h"
#import "TravelorderPayHttpMessage.h"
#import "VechileOrderManager.h"

@interface VechileOrderPayViewController (Business)

-(void)getFinanceInfo;

- (void)doAdPurchase;

-(void)onPaySuccess;

-(void)onThirdPayNotification:(id)sender;

-(void)onThirdPayFailureNotification:(id)sender;

@end
