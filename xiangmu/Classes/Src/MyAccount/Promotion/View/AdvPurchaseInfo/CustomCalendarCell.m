//
//  CustomCalendarCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/13.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CustomCalendarCell.h"


@implementation CustomCalendarCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UILabel *badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width-17.5, 0, 17.5, 12.5)];
        badgeLabel.textColor = [UIColor whiteColor];
        badgeLabel.backgroundColor = [UIColor redColor];
        badgeLabel.cornerRadius = 5;
        badgeLabel.text = @"12";
        badgeLabel.font = FONT(10);
        badgeLabel.textAlignment = UITextAlignmentCenter;
        [self.contentView addSubview:badgeLabel];
        self.badgeLabel = badgeLabel;
    }
    return self;
}

@end
