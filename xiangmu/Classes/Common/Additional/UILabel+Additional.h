//
//  UILabel+Additional.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Additional)

-(void)setTextColor:(UIColor*)textColor range:(NSRange)range;
-(void)setTextColor:(UIColor *)textColor text:(NSString*)text;
-(void)setTextFont:(UIFont*)font text:(NSString*)text;
-(void)setTextCustomAttribute:(UIColor*)textColor font:(UIFont*)font string:(NSString*)string;

- (void)alignTop;

@end
