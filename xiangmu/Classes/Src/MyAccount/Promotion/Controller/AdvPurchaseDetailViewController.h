//
//  AdvPurchaseDetailViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "AdvPurchaseViewModel.h"
#import "ProviderUtil.h"
//#import "AdvPurchaseDetailViewController+NetworkRequest.h"


typedef NS_ENUM(NSUInteger, AdvPurchaseDetailRow)
{
    AdvPurchaseDetailRowTItle,
    AdvPurchaseDetailRowCalendar,
    AdvPurchaseDetailRowTime,
    AdvPurchaseDetailRowSelection,
    AdvPurchaseDetailRowPay
    
};



@interface AdvPurchaseDetailViewController : BaseViewController

@property (nonatomic, strong) AdvPurchaseViewModel *viewModel;

@property (nonatomic, assign) NSInteger position;

@property (nonatomic, assign) AdvType advType;

@property (nonatomic, assign) NSInteger tableViewRowNums;



//@property (nonatomic, strong) NSMutableArray<AdPurchaseOrderInfo*>



@end
