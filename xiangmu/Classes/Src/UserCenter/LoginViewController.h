//
//  ViewController.h
//  xiangmu
//
//  Created by David kim on 16/4/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "RegisterViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "LoginView.h"
#import "QQLoginHttpMessage.h"
#import "WechatLoginHttpMessage.h"
#import "WeiboLoginHttpMessage.h"
#import "ThirdpartyLoginHttpMessage.h"





@interface LoginViewController : BaseViewController<Username>
{
    NSString *paword;
}
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *userNameTextField;
@property (nonatomic, strong) UITextField *userPasswordTextField;
@property (nonatomic, strong) LoginView *loginView;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollview;
//@property (nonatomic, weak) id<LoginViewControllerDelegate> loginDelegate;

@property (nonatomic, strong) UIViewController *previousViewController;// 要返回的界面
@property (nonatomic, assign) SEL previousMethod;// 要执行的方法

//@property (nonatomic, strong) QQLoginHttpMessage *qqLoginHttpMessage;
//@property (nonatomic, strong) WechatLoginHttpMessage *wechatLoginHttpMessage;
//@property (nonatomic, strong) WeiboLoginHttpMessage *weiboLoginHttpMessage;
@property (nonatomic, strong) ThirdpartyLoginHttpMessage *thirdPartyLoginHttpMessage;
@property (nonatomic, assign) NSInteger thirdPartyId;// 第三方登录平台ID

-(void)gotoRegister;

- (void)gotoThirdpartyRegister:(ThirdpartyLoginHttpMessage*)thirdPartyLoginHttpMessage;


@end
