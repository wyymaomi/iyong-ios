//
//  UIPlaceHolderTextView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

//IB_DESIGNABLE
@interface UIPlaceHolderTextView : UITextView

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) UIColor *placeholderColor;
@property (nonatomic, retain) UILabel *placeHolderLabel;
//@property (nonatomic, retain) IBInspectable NSString *placeholder;
//@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@end
