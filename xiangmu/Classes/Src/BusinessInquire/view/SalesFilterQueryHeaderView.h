//
//  SalesFilterQueryHeaderView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesFilterQueryHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIButton *bgButton;


@end
