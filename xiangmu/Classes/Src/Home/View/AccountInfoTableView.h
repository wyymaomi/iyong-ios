//
//  AccountInfoTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableView.h"

typedef NS_ENUM(NSInteger, AccountInfoTableRow) {
    AccountInfoTableRowAvatar = 0,
//    AccountInfoTableRowMyInquiry,
    AccountInfoTableRowMyTravel,
    AccountInfoTableRowMessage,
    AccountInfoTableRowMyFavorite,
    AccountInfoTableRowMyAccount,
    AccountInfoTableRowSettings,
    AccountInfoTableRowFinance,
    AccountInfoTableRowContactService,
    AccountInfoTableRowCompanyMode // 切换企业模式
//    AccountInfoTableRowMyInvoice // 
};

@protocol AccountInfoTableViewDelegate <NSObject>

-(void)didSelectRow:(NSInteger)row;

@end

@interface AccountInfoTableView : BaseTableView<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) NSInteger messageCount;

@property (nonatomic, assign) NSInteger orderCount;

@property (nonatomic, weak) id<AccountInfoTableViewDelegate> tableViewDelegate;


@end
