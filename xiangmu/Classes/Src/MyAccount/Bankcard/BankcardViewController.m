//
//  BankcardViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BankcardViewController.h"
#import "BankcardTableViewCell.h"
#import "AddBankcardTableViewCell.h"
#import "PaypwdInputViewController.h"
#import "PasswordSetViewController.h"
#import "RealnameCertificationViewController.h"
#import "Bankcard.h"

@interface BankcardViewController ()<UITableViewDelegate, UITableViewDataSource>

//@property (nonatomic, strong) NSMutableArray *dataList;
@property (nonatomic, assign) NSInteger rowCount;

@end

@implementation BankcardViewController

//-(NSMutableArray*)dataList
//{
//    if (!_dataList) {
//        _dataList = [NSMutableArray new];
//    }
//    return _dataList;
//}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"银行卡管理";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    //    self.tableView.tableHeaderView = [[UIView alloc] init];
    self.tableView.backgroundColor = UIColorFromRGB(0x383636);
}

//-(void)pressedBackButton
//{
//    [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshBankcardListNotification object:nil];
//    [super pressedBackButton];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 刷新数据
-(void)refreshData
{
    self.nextAction = @selector(refreshData);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, BANKCARD_LIST_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在刷新银行卡列表，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        BankcardResponseData *bankcardResponseData = [[BankcardResponseData alloc] initWithDictionary:responseData error:nil];
        if ([bankcardResponseData.code integerValue] == STATUS_OK) {
            strongSelf.dataList = bankcardResponseData.data;
            [strongSelf.tableView reloadData];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[bankcardResponseData.code integerValue]]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
        
    }];
}

#pragma mark - 删除银行卡

- (void)deleteBankcard:(Bankcard*)bankcard
{
    self.nextAction = @selector(deleteBankcard:);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, DELETE_BANKCARD_API];
    NSString *params = [NSString stringWithFormat:@"banckcardId=%@", bankcard.id];
    
    [self showHUDIndicatorViewAtCenter:@"正在删除银行卡，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            [strongSelf refreshData];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}
#pragma  mark - 判断是否已经设置支付密码

-(void)checkHasPayPwd
{
    
    self.nextAction = @selector(checkHasPayPwd);
    self.object1 = nil;
    self.object2 = nil;
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_HAS_API];
    [self showHUDIndicatorViewAtCenter:@"正在操作中，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                // 已设置支付密码 弹出输入支付密码页面

                    PaypwdInputViewController *viewController = [[PaypwdInputViewController alloc] init];
                    [strongSelf.navigationController pushViewController:viewController animated:YES];
                
//                }
//                else {
//                    RealnameCertificationViewController *viewController = [[RealnameCertificationViewController alloc] init];
//                    [strongSelf.navigationController pushViewController:viewController animated:YES];
//                }
            }
            else if([data integerValue] == 0){
                // 未设置支付密码
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请设置支付密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        PasswordSetViewController *viewController = [[PasswordSetViewController alloc] init];
                        viewController.type = MimaTypeJiaoyi;
                        viewController.nextViewController = @"PaypwdInputViewController";
                        [strongSelf.navigationController pushViewController:viewController animated:YES];
                    }
                }];
            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    self.rowCount = self.dataList.count + 1;
    return self.rowCount;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.rowCount - 1) {
        BankcardTableViewCell *cell = [BankcardTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        if (self.dataList.count > 0) {
            Bankcard *bankcard = self.dataList[indexPath.row];
            cell.bankNameLabel.text = bankcard.bankName;
            cell.bankTypeLabel.text = bankcard.type;
            cell.bankCardNumberLabel.text = [StringUtil encodeBankcardNo:bankcard.number];//bankcard.number;
        }
        
        return cell;
    }
    else {
        AddBankcardTableViewCell *cell = [AddBankcardTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0x5C5c5c);
        cell.contentView.backgroundColor = UIColorFromRGB(0x5C5C5C);
        return cell;
    }
    
    
    return nil;
}

//删除操作
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.rowCount - 1) {
        return YES;
    }
    return NO;
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //获取到相应cell
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        if (indexPath.row < self.rowCount - 1) {
            Bankcard *bankcard = self.dataList[indexPath.row];
            WeakSelf
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否删除" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 1) {
                    [weakSelf deleteBankcard:bankcard];
                }
            }];
        }

        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row < self.rowCount - 1) {
        if (indexPath.row < self.dataList.count - 1) {
            return 116;
        }
        else {
            return 140;
        }
    }
    return 45;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.row == self.rowCount - 1) {
        [self checkHasPayPwd];
    }
}


@end
