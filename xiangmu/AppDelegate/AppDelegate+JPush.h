//
//  AppDelegate+JPush.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AppDelegate.h"
#import "JPUSHService.h"

@interface AppDelegate (JPush)<JPUSHRegisterDelegate>

- (void)setupJPush:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end
