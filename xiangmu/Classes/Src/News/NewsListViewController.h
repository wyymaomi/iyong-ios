//
//  NewsListViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "PageUtil.h"
#import "NewsViewModel.h"
//#import "UserExpericenViewModel.h"

@interface NewsListViewController : BaseViewController

@property (nonatomic, strong) PageUtil *commonPage; // 分页处理

//@property (nonatomic, strong) NewsViewModel *

@end
