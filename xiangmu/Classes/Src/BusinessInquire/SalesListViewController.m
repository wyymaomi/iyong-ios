//
//  SalesListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SalesListViewController.h"
#import "BillHeaderView.h"
#import "BillTableViewCell.h"
#import "PullRefreshTableView.h"
#import "SalesFilterQueryViewController.h"
#import "OrderManagerViewController.h"

@interface SalesListViewController ()<BillTableViewCellDelegate>

@end

@implementation SalesListViewController


#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"营业查询";
    [self.view addSubview:self.pullRefreshTableView];
    self.pullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"magnifier"] style:UIBarButtonItemStyleDone target:self action:@selector(filterQuery)];
    
}

//-(void)awakeFromNib
//{
//    _pullRefreshTableView.hasHeaderRefresh = YES;
//}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.pullRefreshTableView beginHeaderRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)filterQuery
{
    SalesFilterQueryViewController *viewController = [[SalesFilterQueryViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}




#pragma mark - PullDelegate method
-(void)onHeaderRefresh
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SALES_LIST_API];
    NSString *params = @"startTime=&endTime=&carId=&time=";
    
    self.loadStatus = LoadStatusLoading;
    self.pullRefreshTableView.footerHidden = YES;
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        self.loadStatus = LoadStatusFinish;
        
        SalesResponseData *salesResponseData = [[SalesResponseData alloc] initWithDictionary:responseData error:nil];
        if ([salesResponseData.code integerValue] == STATUS_OK) {
            if (!IsArrEmpty(salesResponseData.data)) {
                strongSelf.dataList = salesResponseData.data;
                strongSelf.pullRefreshTableView.footerHidden = NO;
                [strongSelf.pullRefreshTableView reloadData];
            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[salesResponseData.code integerValue]]];
        }
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        StrongSelf
        strongSelf.loadStatus = LoadStatusNetworkError;
//        [strongSelf showAlertView:errorDesc];
        [strongSelf.pullRefreshTableView endHeaderRefresh];
    }];
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        [self.pullRefreshTableView endFooterRefresh];
        return;
    }
    
    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    SalesModel *salesModel = self.dataList.lastObject;
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:salesModel.carTime]];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SALES_LIST_API];
    
//    self.pullRefreshTableView.footerHidden = YES;
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        SalesResponseData *salesResponseData = [[SalesResponseData alloc] initWithDictionary:responseData error:nil];
        if ([salesResponseData.code integerValue] == STATUS_OK) {
            if (!IsArrEmpty(salesResponseData.data)) {
                [strongSelf.dataList addObjectsFromArray:salesResponseData.data];
                [strongSelf.pullRefreshTableView reloadData];
            }
        }
        else {
            [strongSelf showAlertView:[self getErrorMsg:[salesResponseData.code integerValue]]];
        }
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        StrongSelf
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
    }];
}

-(void)gotoOrderDetailPage:(NSInteger)index
{
    //    OrderListItem *listItem = self.orderList[indexPath.row];
    
    SalesModel *model = self.dataList[index];
    NSString *params = [NSString stringWithFormat:@"orderId=%@", model.orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_DETAIL_API];
    
    [self showHUDIndicatorViewAtCenter:MSG_ORDER_DETAIL];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        id code = responseData[@"code"];
        if (IsNilOrNull(code)) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *array = responseData[@"data"];
            
            if (!IsArrEmpty(array)) {
                
                NSInteger orderStatus = 0;
                
                OrderManagerViewController *vc = [[OrderManagerViewController alloc] init];
                
                for (NSDictionary *dict in array) {
                    
                    NSUInteger status = [dict[@"status"] integerValue];
                    NSDictionary *vo = dict[@"vo"];
                    if (status == 1) {
                        vc.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusNew;
                    }
                    if (status == 2) {
                        vc.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusDispatch;
                    }
                    if (status == 3) {
                        vc.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusVechileArrange;
                    }
                    if (status == 4) {
                        vc.wayBillModel = [[WayBillResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusWayBill;
                    }
                    if (status == 5) {
                        vc.barginAndPayModel = [[BarginAndPayResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusBarginAndPay;
                    }
                }
                
                vc.source_from = SOURCE_FROM_ORDER_LIST;
                vc.orderStatus = orderStatus;
                vc.orderMode = OrderModeDetail;
                vc.orderId = model.orderId;
                vc.title = @"结算支付";
                vc.hidesBottomBarWhenPushed = YES;
                [strongSelf.navigationController pushViewController:vc animated:YES];
            }
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

//-(void)filterQuery
//{
//    
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BillTableViewCell getCellHeight];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BillTableViewCell *cell = [BillTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.delegate = self;
    SalesModel *model = self.dataList[indexPath.section];
    [cell initSalesData:model];
    return cell;
}

-(void)onSelectRowAtIndex:(NSUInteger)rowIndex;
{
    [self gotoOrderDetailPage:rowIndex];
}


@end
