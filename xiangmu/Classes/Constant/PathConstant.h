//
//  PathConstant.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef PathConstant_h
#define PathConstant_h

static NSString *_DatabaseDirectory;
static inline NSString* DatabaseDirectory() {
    if(!_DatabaseDirectory) {
        NSString* cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _DatabaseDirectory = [[[cachesDirectory stringByAppendingPathComponent:[[NSProcessInfo processInfo] processName]] stringByAppendingPathComponent:@"Database"] copy];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isDir = YES;
        BOOL isExist = [fileManager fileExistsAtPath:_DatabaseDirectory isDirectory:&isDir];
        if (!isExist)
        {
            [fileManager createDirectoryAtPath:_DatabaseDirectory withIntermediateDirectories:YES attributes:nil error:NULL];
        }
    }
    
    return _DatabaseDirectory;
}

static NSArray *_codeMsgList;
static inline NSArray *codeMsgList() {
    if (_codeMsgList == nil) {
        // 加载plist中的字典数组
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CodeMsg.plist" ofType:nil];
        _codeMsgList = [NSArray arrayWithContentsOfFile:path];
    }
    return _codeMsgList;
}

static NSArray *_homeTypeList;
static inline NSArray *homeTypeList() {
    if (_homeTypeList == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"HomeService.plist" ofType:nil];
        _homeTypeList = [NSArray arrayWithContentsOfFile:path];
    }
    return _homeTypeList;
}

static NSArray *_serviceTypeList;
static inline NSArray *serviceTypeList() {
    if (_serviceTypeList == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"VechileService.plist" ofType:nil];
        _serviceTypeList = [NSArray arrayWithContentsOfFile:path];
    }
    return _serviceTypeList;
}

static NSArray *_carTypeList;
static inline NSArray *carTypeList() {
    if (_carTypeList == nil ){
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CarType.plsit" ofType:nil];
        _carTypeList = [NSArray arrayWithContentsOfFile:path];
    }
    return _carTypeList;
}



#endif /* PathConstant_h */
