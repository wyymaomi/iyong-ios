//
//  UCHelper.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

static dispatch_source_t _timer;
@interface UserHelper : NSObject

+(BOOL)validLoginPwd:(NSString*)codeString error:(NSString**)errorMsg;

// 用户名校验
+ (BOOL)validateUserName:(NSString *)userName error:(NSString **)errorMsg;
//手机校验
+ (BOOL)validatePhoneNum:(NSString *)phoneNum error:(NSString **)errorMsg;
// 推荐人手机号校验
+ (BOOL)validateRefPhoneNum:(NSString*)phoneNum error:(NSString**)errorMsg;
//手机验证码校验
+ (BOOL)validateCodeString:(NSString *)codeString error:(NSString **)errorMsg;
//登录密码校验
+ (BOOL)validatePassWord:(NSString *)codeString error:(NSString **)errorMsg;
//纯数字
//+ (BOOL)isNumText:(NSString *)str;
// 昵称
+ (BOOL)validateNickName:(NSString*)nickName error:(NSString**)errorMsg;
// 验证码
+ (void)verificationCode:(void(^)())blockYes blockNo:(void(^)(id time))blockNo;
// 支付密码校验
+ (BOOL)verifyPaypwd:(NSString*)payPwd error:(NSString**)errorMsg;
+ (void)cancelTimer;
// 验证公司名
+ (BOOL)validateCompanyName:(NSString*)companyName error:(NSString**)errorMsg;
+(BOOL)validateTwicePwd:(NSString*)pwd1 pwd2:(NSString*)pwd2 error:(NSString**)errorMsg;

+ (BOOL)validateIdcard:(NSString*)idcardStr error:(NSString**)errorMsg;
+ (BOOL)validateBankcardNo:(NSString*)bankcardNo error:(NSString**)errorMsg;

+ (BOOL)validateEmail:(NSString *)codeString error:(NSString **)errorMsg;

+(BOOL)validateTelephone:(NSString*)codeString error:(NSString**)errorMsg;



@end
