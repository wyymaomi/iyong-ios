//
//  AdPurchaseHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"



@interface AdPurchaseHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *adMainIds;// 广告编号
@property (nonatomic, strong) NSNumber *price; // 价格
@property (nonatomic, strong) NSNumber *payMode; // 支付方式

@end
