//
//  VechileEditViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "VechileEditViewModel.h"

@implementation VechileEditViewModel

- (CarModel*)vechileModel
{
    if (!_vechileModel) {
        _vechileModel = [CarModel new];
    }
    
    return _vechileModel;
}

//- (void)validate

// 根据车辆认证状态设置按钮文字和背景颜色
- (void)setVechileCertificationStatus:(NSString**)statusText btnBgColor:(UIColor**)btnBgColor;
{
    
//    if (!_vechileModel) {
//        
//        return;
//    }
    
    NSInteger vechileCertificationStatus = [self.vechileModel.certificationStatus integerValue];
//    NSString *statusText;
//    UIColor *btnBgColor;
    
    if (vechileCertificationStatus == CompanyCertificationStatusInit) {
        *statusText = @"未认证";
        *btnBgColor = [UIColor lightGrayColor];
    }
    else if (vechileCertificationStatus == CompanyCertificationStatusInReview) {
        *statusText = @"待认证";
        *btnBgColor = [UIColor lightGrayColor];
    }
    else if (vechileCertificationStatus == CompanyCertificationStatusFinished) {
        *statusText = @"已认证";
        *btnBgColor = UIColorFromRGB(0x30a6df);
    }
    else if (vechileCertificationStatus == CompanyCertificationStatusRejected) {
        *statusText = @"已拒绝";
        *btnBgColor = [UIColor redColor];
    }
    else {
        *statusText = @"未认证";
        *btnBgColor = [UIColor lightGrayColor];
    }
}

// 根据认证状态 车辆认证界面是否可编辑
- (BOOL)isVechileCertCanEdit
{
    if (!_vechileModel) {
        return YES;
    }
    
    NSInteger vechileCertificationStatus = [self.vechileModel.certificationStatus integerValue];
    
    BOOL isEditMode = NO;
    
    if (vechileCertificationStatus == CompanyCertificationStatusInit ||
        vechileCertificationStatus == CompanyCertificationStatusRejected) {
        isEditMode = YES;
    }
    
    return isEditMode;
}



@end
