//
//  AccountInfoView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountInfoTableView.h"
#import "EnterpriseUserCollectionView.h"

@protocol AccountInfoViewDelegate <NSObject>

- (void)onDismiss;
- (void)onGotoPage:(NSString*)vcPage title:(NSString*)title;

@end

@interface AccountInfoView : UIView

@property (nonatomic, strong) AccountInfoTableView *accountTableView;
@property (nonatomic, strong) EnterpriseUserCollectionView *collectionView;

@property (nonatomic, assign) id<AccountInfoViewDelegate> delegate;

@property (nonatomic, assign) BOOL isNeedAnimation;// 是否需要动画效果
@property (nonatomic, assign) BOOL isVisible;

- (void)dismiss;

- (void)show;
//- (void)show:(NSArray*)images borderColor:(UIColor*)color;

-(void)refreshOrderCount:(NSInteger)orderCount;

- (void)refreshPartnerNumber:(NSInteger)partnerNumber;

@end
