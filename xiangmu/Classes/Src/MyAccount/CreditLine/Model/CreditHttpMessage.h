//
//  CreditHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface CreditHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *applicant;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *amountId;
@property (nonatomic, strong) NSString *companyName;
@property (nonatomic, strong) NSNumber *amount;

//-(BOOL)validateCreditApply:(NSString **)errorMsg;

@end
