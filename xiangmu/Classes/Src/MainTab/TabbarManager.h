//
//  TabbarManager.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TabbarDelegate.h"
#import "NewMainTabBarController.h"

@interface TabbarManager : NSObject

-(CustomTabBarViewController*)createTabBarController;
//-(id<TabbarDelegate>)createTabBarController;

@end
