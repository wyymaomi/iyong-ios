//
//  MyViewController.h
//  load
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"


typedef NS_ENUM(NSInteger, MeSection){
    MeSectionOne = 0,
//    MeSectionBusienssCircle,
    MeSectionCertification,
    MeSectionMyAccount,
    MeSectionSetting
};

//typedef NS_ENUM(NSInteger, MeCertificationRow) {
//    MeCertificationRowLevel = 0,                // 等级
//    MeCertificationRowCertificationInfo,    // 认证信息
//    MeCertificationRowRealname,// 实名认证
//    MeCertificationRowCompany,  // 企业认证
//    MeCertificationRowVechile   // 车辆认证
//};


@interface MyViewController : LFNavController
{
    //头像
    UIImageView *avatarImg;
}
@property (nonatomic,strong)UILabel *titleLabel;
@end
