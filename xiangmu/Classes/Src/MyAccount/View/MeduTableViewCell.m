//
//  MeduTableViewCell.m
//  xiangmu
//
//  Created by David kim on 16/3/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MeduTableViewCell.h"
#import "MyUtil.h"

@implementation MeduTableViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.name=[[UILabel alloc]initWithFrame:CGRectMake(20, 10, 100, 30)];
        self.name.textColor=[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
        self.name.textAlignment=NSTextAlignmentJustified;
        self.name.font=[UIFont systemFontOfSize:13];
        [self addSubview:self.name];
        
        
        
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [ super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
