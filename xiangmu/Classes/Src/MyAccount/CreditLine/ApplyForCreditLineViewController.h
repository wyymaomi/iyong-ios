//
//  ApplyForCreditLineViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/15.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger, CreditRowForCompany) {
    CreditRowForCompanyUserName=0,
    CreditRowForCompanyCompanyName,
    CreditRowForCompanyMobile,
    CreditRowForCompanyQuota
};

typedef NS_ENUM(NSUInteger, CreditRowForPerson) {
    CreditRowForPersonUserName=0,
    CreditRowForPersonMobile,
    CreditRowForPersonQuota
};

typedef NS_ENUM(NSUInteger, CreditType) {
    CreditTypeCompany,
    CreditTypePerson
};

@interface ApplyForCreditLineViewController : BaseViewController

@property (nonatomic, assign) CreditType type;// 申请额度类型

@end
