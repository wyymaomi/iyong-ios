//
//  MsgView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSegment.h"
#import "OrderBaseInfoResponseModel.h"
#import "VechileArrangeResponseModel.h"
#import "OrderCopyResponseModel.h"

#if NS_BLOCKS_AVAILABLE
typedef void (^BBBasicBlock)(void);
#endif



@interface MsgView : UIView
@property (weak, nonatomic) IBOutlet UILabel *carDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *startLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *carUserNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *roughTravelLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *vechileArrangeLabel;

@end

@protocol TTMessageDelegate <NSObject>

-(void)onClickOKButton;
-(void)onClickCancelButton;

@end

@interface TTMessage : NSObject<CustomSegmentDelegate>
{
#if NS_BLOCKS_AVAILABLE
    BBBasicBlock    _cancelBlock;
    BBBasicBlock    _confirmBlock;
#endif
}

@property (nonatomic, weak) id<TTMessageDelegate> delegate;


//-(void)showMessageViewWithTitle:(NSString*)title message:(NSString*)message cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitle:(NSString*)otherButtonTitle onCancel:(BBBasicBlock)canceled onOther:(BBBasicBlock)other;

-(void)showMessageViewWithTitle:(NSString*)title
             orderBaseInfoModel:(OrderBaseInfoResponseModel*)orderBaseInfoModel
            vechileArrangeModel:(VechileArrangeResponseModel*)vechileArrangeModel
              cancelButtonTitle:(NSString*)cancelButtonTitle
               otherButtonTitle:(NSString*)otherButtonTitle
                       onCancel:(BBBasicBlock)canceled onOther:(BBBasicBlock)other;

-(void)showMessageViewWithTitle:(OrderCopyResponseModel*)orderCopyModel;


@end
