//
//  ShareUtil.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ShareUtil.h"


@implementation ShareUtil

//+(void)wechatShare:(NSString*)url title:(NSString*)title desc:(NSString*)desc icon:(UIImage*)icon webpageUrl:(NSString*)webpageUrl
//{
//    //创建分享消息对象
//    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
//    
//    //创建网页内容对象
//    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:@"分享标题" descr:@"分享内容描述" thumImage:[UIImage imageNamed:@"icon"]];
//    //设置网页地址
//    shareObject.webpageUrl =@"http://mobile.umeng.com/social";
//    
//    //分享消息对象设置分享内容对象
//    messageObject.shareObject = shareObject;
//    
//    //调用分享接口
//    [[UMSocialManager defaultManager] shareToPlatform:UMSocialPlatformType_WechatSession messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
//        if (error) {
//            NSLog(@"************Share fail with error %@*********",error);
//        }else{
//            NSLog(@"response data is %@",data);
//        }
//    }];
//}

+(void)startShare:(NSString*)url title:(NSString*)title desc:(NSString*)desc icon:(UIImage*)icon webpageUrl:(NSString*)webpageUrl
{
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),
                                               @(UMSocialPlatformType_WechatTimeLine),
                                               @(UMSocialPlatformType_QQ),
                                               @(UMSocialPlatformType_Sina)]];
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        // 根据获取的platformType确定所选平台进行下一步操作
//        NSString *shareUrl = [NSString stringWithFormat:@"%@%@", EXPERIENCE_DETAIL_SHARE_URL, self.experienceViewModel.serviceModel.id];
        DLog(@"shareUrl = %@", url);
//        [self shareWebPageToPlatformType:platformType title:self.experienceViewModel.serviceModel.nickname desc:self.experienceViewModel.serviceModel.content icon:self.avatarIconImage webpageUrl:shareUrl];
        [self shareWebPageToPlatformType:platformType title:title desc:desc icon:icon webpageUrl:webpageUrl];
    }];
}

+(void)startShareInHomepage
{
    
//    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
//    }];
}


+(void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
                            title:(NSString *)title
                             desc:(NSString *)desc
                             icon:(UIImage *)icon
                       webpageUrl:(NSString *)webpageUrl
                     shareSuccess:(void(^)(id data))shareSuccess
                     shareFailure:(void(^)(id data, NSError *error))shareFailure
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:desc thumImage:icon];
    //设置网页地址
    shareObject.webpageUrl = webpageUrl;
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            NSLog(@"************Share fail with error %@*********",error);
            if (shareFailure) {
                shareFailure(data, error);
            }
        }else{
            NSLog(@"response data is %@",data);
            if (shareSuccess) {
                shareSuccess(data);
            }
        }
    }];
    
}


+ (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType title:(NSString*)title desc:(NSString*)desc icon:(UIImage*)icon webpageUrl:(NSString*)webpageUrl
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:desc thumImage:icon];
    //设置网页地址
    shareObject.webpageUrl = webpageUrl;
//    shareObject.webpageUrl = webpageUrl@"http://app.chinaiyong.com:8080/iyong-web/";
//    shareObject.thumbImage =
    
    //    shareObject.webpageUrl =@"http://mobile.umeng.com/social";
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            NSLog(@"************Share fail with error %@*********",error);
        }else{
            NSLog(@"response data is %@",data);
        }
    }];
}

@end
