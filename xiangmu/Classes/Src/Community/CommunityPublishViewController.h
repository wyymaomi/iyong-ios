//
//  CommunityPublishViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface CommunityPublishViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
