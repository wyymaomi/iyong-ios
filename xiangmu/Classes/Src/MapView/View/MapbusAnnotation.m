//
//  MapbusAnnotation.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/11/15.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MapbusAnnotation.h"

@implementation MapbusAnnotation

-(id)init
{
    self = [super init];
    
    if (self) {
        _movingOver = YES;
    }
    return self;
}

-(NSMutableArray*)coordinateList
{
    if (!_coordinateList) {
        _coordinateList = [NSMutableArray new];
    }
    return _coordinateList;
}

-(void)addLocation:(CLLocationCoordinate2D)coordinate;
{
    if (coordinate.latitude == 0 || coordinate.longitude == 0) {
        return;
    }
    
    CLLocation *location = [[CLLocation alloc] initWithCoordinate:coordinate altitude:self.height horizontalAccuracy:self.radius verticalAccuracy:self.radius course:self.direction speed:self.speed timestamp:[NSDate date]];
    [self.coordinateList addObject:location];
}


-(void)startAnimation
{
//    if (self.coordinate.latitude == 0 || self.coordinate.longitude == 0) {
//        return;
//    }
//    
//    CLLocation *location = [[CLLocation alloc] initWithCoordinate:self.coordinate altitude:self.height horizontalAccuracy:self.radius verticalAccuracy:self.radius course:self.direction speed:self.speed timestamp:[NSDate date]];
//    [self.coordinateList addObject:location];

//    if (self.speed <=1) {
//        return;
//    }
    

    
    if (self.coordinateList.count >= 5 && _movingOver) {
        
        
        
        _moveArray = [NSArray arrayWithArray:self.coordinateList];
        
        [self.coordinateList removeAllObjects];
        
        DLog(@"----- moveArrayCount: %ld",_moveArray.count);
        DLog(@"------beging moving -----");
        
        _currentIndex = 0;
        
        _movingOver = NO;
        
//        CLLocation *location = [_moveArray firstObject];
//        self.coordinate = location.coordinate;
        
        [self startMoving];
        
    }
    
}

-(void)startMoving
{
    
//    NSInteger index = _currentIndex % moveArray.count;


    CLLocation *lastLocation = [_moveArray lastObject];
    CLLocation *firstLocation = [_moveArray firstObject];
    
     double distance = [lastLocation distanceFromLocation:firstLocation];
    
    DLog(@"entityName = %@, distance = %.2f", self.entityName, distance);
    
    if (distance <= 100) {
        self.movingOver = YES;
//        self.moveArray = nil;
        return;
    }
    
    CLLocation *newLocation = _moveArray[_currentIndex];
    
    self.annotationView.image = [self.annnationImage zj_imageRotatedByAngle:newLocation.course];
    self.annotationView.bounds = CGRectMake(0, 0, 41/1.5, 34/1.5);
    
    CLLocationCoordinate2D newCoordinate = newLocation.coordinate;
    WeakSelf
    [UIView animateWithDuration:distance/40 animations:^{
        DLog(@"locations.count = %lu", (unsigned long)weakSelf.moveArray.count);
        DLog(@"currentIndex = %lu", (unsigned long)_currentIndex);
        DLog(@"currentCoordinate = %.6f, %.6f", newCoordinate.latitude, newCoordinate.longitude);
        weakSelf.coordinate = newCoordinate;
        weakSelf.currentIndex ++;
    } completion:^(BOOL finished) {
        if (weakSelf.currentIndex == weakSelf.moveArray.count) {
            weakSelf.movingOver = YES;
            weakSelf.moveArray = nil;
        } else {
            [weakSelf startMoving];
        }
    }];
    
}

- (BMKAnnotationView*)getRouteAnnotationView:(BMKMapView *)mapview
{
    
    BMKAnnotationView *view = nil;
    
    view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"mapbus"];
    
    if (view == nil) {
        view = [[BMKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"mapbus"];
        

//        view.frame = CGRectMake(0, 0, 41,34);
        
//        sportAnnotationView.imageView.transform = CGAffineTransformMakeRotation(node.angle);

        
//        view.centerOffset = CGPointMake(0, -)
        
    }
    
    if (view) {
        
        
        //        view.image
        UIImage *image;
        if (self.speed <= 1) {
            image = [UIImage imageNamed:@"icon_mapbus_still"];
        }
        else {
            image = [UIImage imageNamed:@"icon_mapbus"];
        }
        //        UIImage *image = [UIImage imageNamed:@"icon_mapbus"];
        view.image = [image zj_imageRotatedByAngle:self.direction];//[UIImage imageNamed:@"icon_mapbus"];
        
        view.bounds = CGRectMake(0, 0, 41/1.5, 34/1.5);
        
        self.annotationView = view;
        self.annnationImage = image;
        
    }

    
//    switch (_type) {
//        case 0:
//        {
//            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"start_node"];
//            if (view == nil) {
//                view = [[BMKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"start_node"];
//                view.image = [UIImage imageNamed:@"icon_address_up"];
//                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
//            }
//        }
//            break;
//        case 1:
//        {
//            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"end_node"];
//            if (view == nil) {
//                view = [[BMKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"end_node"];
//                view.image = [UIImage imageNamed:@"icon_address_down"];
//                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
//            }
//        }
//            break;
//        case 2:
//        {
//            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"bus_node"];
//            if (view == nil) {
//                view = [[BMKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"bus_node"];
//                view.image = [UIImage imageNamed:@"icon_bus.png"];
//            }
//        }
//            break;
//        case 3:
//        {
//            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"rail_node"];
//            if (view == nil) {
//                view = [[BMKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"rail_node"];
//                view.image = [UIImage imageNamed:@"icon_rail.png"];
//            }
//        }
//            break;
//        case 4:
//        {
//            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"route_node"];
//            if (view == nil) {
//                view = [[BMKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"route_node"];
//            } else {
//                [view setNeedsDisplay];
//            }
//            
//            UIImage* image = [UIImage imageNamed:@"icon_direction.png"];
//            //            view.image = [image imageRotatedByDegrees:_degree];
//        }
//            break;
//        case 5:
//        {
//            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"waypoint_node"];
//            if (view == nil) {
//                view = [[BMKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"waypoint_node"];
//            } else {
//                [view setNeedsDisplay];
//            }
//            
//            UIImage* image = [UIImage imageNamed:@"icon_waypoint.png"];
//            //            view.image = [image imageRotatedByDegrees:_degree];
//        }
//            break;
//            
//        case 6:
//        {
//            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"stairs_node"];
//            if (view == nil) {
//                view = [[BMKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"stairs_node"];
//            }
//            view.image = [UIImage imageNamed:@"icon_stairs.png"];
//        }
//            break;
//        default:
//            break;
//    }
    if (view) {
        view.annotation = self;
        view.canShowCallout = NO;
    }
    return view;
}


@end
