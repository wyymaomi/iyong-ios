//
//  GesturePasswordController.h
//  GesturePassword
//
//  Created by hb on 14-8-23.
//  Copyright (c) 2014年 黑と白の印记. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TentacleView.h"
#import "GesturePasswordView.h"


//@protocol GesturePasswordControllerDelegate <NSObject>
//
//-(void)onGestureLoginSuccess;
//
//@end

@interface GesturePasswordController : BaseViewController <VerificationDelegate,ResetDelegate,GesturePasswordDelegate>

@property (nonatomic, assign) NSInteger source_from;// 从哪个界面进入
//@property (nonatomic, weak) id<GesturePasswordControllerDelegate> loginDelegate;

- (void)clear;

- (BOOL)exist;

@end
