//
//  PaypwdInputViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PaypwdInputViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "PayInputView.h"
#import "BindBankcardViewController.h"
#import "RealnameCertificationViewController.h"
#import "BankcardViewController.h"


@interface PaypwdInputViewController ()

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic, strong) PayInputView *payInputView;
@property (nonatomic, strong) UILabel *msgLabel;

@end

@implementation PaypwdInputViewController

-(PayInputView*)payInputView
{
    if (!_payInputView) {
        _payInputView = [[PayInputView alloc] initWithFrame:CGRectMake(30, 200, ViewWidth - 60, 45)];
        _payInputView.backgroundColor = [UIColor whiteColor];
        _payInputView.borderWidth = 1;
        _payInputView.layer.borderColor =  UIColorFromRGB(0xC9C9C9).CGColor;//[UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1.].CGColor;
        _payInputView.pwdCount = 6;
    }
    return _payInputView;
}

-(UILabel*)msgLabel
{
    if (!_msgLabel) {
        _msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 200-50, ViewWidth, 50)];
        _msgLabel.backgroundColor = [UIColor clearColor];
        _msgLabel.text = @"请输入支付密码，以验证身份";
        _msgLabel.textColor = UIColorFromRGB(0x585858);
        _msgLabel.font = FONT(14);
        _msgLabel.textAlignment = UITextAlignmentCenter;
    }
    return _msgLabel;
}

#pragma mark - life cycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.payInputView.pwdTextField becomeFirstResponder];
    [self.payInputView resetInputView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"银行卡管理";
    [self.scrollView addSubview:self.msgLabel];
    [self.scrollView addSubview:self.payInputView];
    
    WeakSelf
    self.payInputView.completeHandle = ^(NSString* inputPwd){
        [weakSelf checkPayPwd:inputPwd];
    };
    self.payInputView.clickBlock = ^(){
        [weakSelf.payInputView.pwdTextField becomeFirstResponder];
    };

}

-(void)checkPayPwd:(NSString*)inputPwd
{
    self.nextAction = @selector(checkPayPwd:);
    self.object1 = inputPwd;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    [self showHUDIndicatorViewAtCenter:@"正在校验支付密码，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] verifyPaypassword:inputPwd success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                BOOL isIdcardCertificationed = [[UserManager sharedInstance].userModel.certification boolValue];
                if (isIdcardCertificationed) {
                    BindBankcardViewController *viewController = [[BindBankcardViewController alloc] init];
                    viewController.bankcardName = [UserManager sharedInstance].userModel.realName;
                    [strongSelf.navigationController pushViewController:viewController animated:YES];
                }
                else {
                    RealnameCertificationViewController *viewController = [[RealnameCertificationViewController alloc] init];
                    viewController.type = 2;
//                    viewController.delegate = self;
                    [strongSelf.navigationController pushViewController:viewController animated:YES];
                }
            }
            else if([data integerValue] == 0){
                // 校验支付密码失败
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"支付密码输入校验失败，请重新输入" delegate:self cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                   
                    if (buttonIndex == 0) {
                        [strongSelf.payInputView.pwdTextField becomeFirstResponder];
                        [strongSelf.payInputView resetInputView];
                    }
                    
                }];
            }
        }
        else {
            // 校验支付密码失败
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[self getErrorMsg:code_status] delegate:self cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [strongSelf.payInputView.pwdTextField becomeFirstResponder];
                    [strongSelf.payInputView resetInputView];
                }
                
            }];

        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

-(void)pressedBackButton
{
    for (UIViewController *viewController in self.navigationController.viewControllers) {
        if ([viewController isKindOfClass:[BankcardViewController class]]) {
            [self.navigationController popToViewController:viewController animated:YES];
            return;
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
