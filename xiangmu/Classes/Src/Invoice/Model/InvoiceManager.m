//
//  InvoiceManager.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceManager.h"
#import "UserHelper.h"

@implementation InvoiceManager

+ (instancetype)sharedInstance
{
    static InvoiceManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[InvoiceManager alloc] init];
    });
    return instance;
}

-(InvoiceinfoHttpMessage*)invoiceinfoHttpMessage
{
    if (!_invoiceinfoHttpMessage) {
        _invoiceinfoHttpMessage = [InvoiceinfoHttpMessage new];
    }
    return _invoiceinfoHttpMessage;
}

+(BOOL)validateInvoiceInfo:(InvoiceinfoHttpMessage*)invoiceinfo error:(NSString**)errorMsg
{
    // 判断必填项
    if (IsStrEmpty(invoiceinfo.header) ||
        /*
         IsStrEmpty(invoiceSettingInfo.taxpayerNumber) ||
         IsStrEmpty(invoiceSettingInfo.registerAddress) ||
         */
        IsStrEmpty(invoiceinfo.contact) ||
        IsStrEmpty(invoiceinfo.tel) ||
        IsStrEmpty(invoiceinfo.address) ||
        IsStrEmpty(invoiceinfo.areaName)) {
        *errorMsg = @"请输入必填项";
        return NO;
    }
    
    // 判断联系电话是否为手机格式或者电话格式
    BOOL isMobile = [UserHelper validatePhoneNum:invoiceinfo.tel error:errorMsg];
    if (isMobile) {
        return YES;
    }
//    if (!isMobile) {
////        *errorMsg = @"联系电话格式错误";
////        return NO;
//    }

    BOOL isPhoneNumber = [UserHelper validateTelephone:invoiceinfo.tel error:errorMsg];
    if (isPhoneNumber) {
        return YES;
    }
//    if (!isPhoneNumber) {
//        *errorMsg = @"联系电话格式错误";
//        return NO;
//    }
    
    if (!isMobile && !isPhoneNumber) {
        *errorMsg = @"联系电话格式错误";
        return NO;
    }
    // 判断银行账号
    //    BOOL isBankcardVerifyed = [UserHelper validateBankcardNo:invoiceSettingInfo.bankAccount error:errorMsg];
    //    if (!isBankcardVerifyed) {
    //        return NO;
    //    }
    
    
    return YES;
}

+(BOOL)validateInvoiceSettinginfo:(InvoiceSettingHttpMessage*)invoiceSettingInfo error:(NSString**)errorMsg
{
    // 判断必填项
    if (IsStrEmpty(invoiceSettingInfo.header) ||
        /*
        IsStrEmpty(invoiceSettingInfo.taxpayerNumber) ||
        IsStrEmpty(invoiceSettingInfo.registerAddress) ||
         */
        IsStrEmpty(invoiceSettingInfo.contact) ||
        IsStrEmpty(invoiceSettingInfo.tel) ||
        IsStrEmpty(invoiceSettingInfo.address) ||
        IsStrEmpty(invoiceSettingInfo.areaName)) {
        *errorMsg = @"请输入必填项";
        return NO;
    }
    
    // 判断联系电话是否为手机格式或者电话格式
    BOOL isMobile = [UserHelper validatePhoneNum:invoiceSettingInfo.tel error:errorMsg];
//    if (!isMobile) {
//        *errorMsg = @"联系电话格式错误";
//        return NO;
//    }
    BOOL isPhoneNumber = [UserHelper validateTelephone:invoiceSettingInfo.tel error:errorMsg];
//    if (!isPhoneNumber) {
//        *errorMsg = @"联系电话格式错误";
//        return NO;
//    }
    // 判断银行账号
//    BOOL isBankcardVerifyed = [UserHelper validateBankcardNo:invoiceSettingInfo.bankAccount error:errorMsg];
//    if (!isBankcardVerifyed) {
//        return NO;
//    }
    if (isMobile || isPhoneNumber) {
        return YES;
    }
    else {
        *errorMsg = @"联系电话格式错误";
        return NO;
    }
    
    return YES;
}

//+ (BOOL) IsBankCard:(NSString *)cardNumber
//{
//    if(cardNumber.length==0)
//    {
//        return NO;
//    }
//    NSString *digitsOnly = @"";
//    char c;
//    for (int i = 0; i < cardNumber.length; i++)
//    {
//        c = [cardNumber characterAtIndex:i];
//        if (isdigit(c))
//        {
//            digitsOnly =[digitsOnly stringByAppendingFormat:@"%c",c];
//        }
//    }
//    int sum = 0;
//    int digit = 0;
//    int addend = 0;
//    BOOL timesTwo = false;
//    for (NSInteger i = digitsOnly.length - 1; i >= 0; i--)
//    {
//        digit = [digitsOnly characterAtIndex:i] - '0';
//        if (timesTwo)
//        {
//            addend = digit * 2;
//            if (addend > 9) {
//                addend -= 9;
//            }
//        }
//        else {
//            addend = digit;
//        }
//        sum += addend;
//        timesTwo = !timesTwo;
//    }
//    int modulus = sum % 10;
//    return modulus == 0;
//}


//+(BOOL)validate

//+ (BOOL)validatePhoneNum:(NSString *)phoneNum error:(NSString **)errorMsg
//{
//    if (phoneNum == nil || [phoneNum isEqualToString:@""]) {
//        *errorMsg = @"请输入手机号";
//        return NO;
//    }
//    
//    if (phoneNum.length != 11) {
//        *errorMsg = @"请输入11位手机号";
//        return NO;
//    }
//    
//    NSString *mobileNoRegex = @"1[0-9]{10,10}";
//    NSPredicate *mobileNoTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNoRegex];
//    BOOL validateOK = [mobileNoTest evaluateWithObject:phoneNum];
//    
//    if (!validateOK) {
//        *errorMsg = @"手机号格式错误";
//        return NO;
//    }
//    
//    return YES;
//}



@end
