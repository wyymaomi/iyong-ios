//
//  ProviderAdvTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProviderOrderModel.h"

@interface ProviderAdvDetailView : UIView

@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *timeSegmentLabel;
@property (nonatomic, strong) UILabel *priceLabel;

- (void)initData:(NSString*)date timeSegment:(NSString*)timeSegment price:(NSNumber*)price;

@end

@interface ProviderAdvTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *orderDetailView;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@property (weak, nonatomic) IBOutlet UIButton *expireButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelledButton;

- (void)initData:(ProviderOrderModel*)order;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+(CGFloat)getCellHeight;

@end
