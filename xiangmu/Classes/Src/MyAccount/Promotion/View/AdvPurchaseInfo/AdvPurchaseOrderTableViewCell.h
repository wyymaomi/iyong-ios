//
//  AdvPurchaseOrderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvPurchaseViewModel.h"

@interface AdvPurchaseOrderTableViewCell : UITableViewCell

//@property (nonatomic, strong) AdvPurchaseViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeSegmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *deleteIcon;
@property (weak, nonatomic) IBOutlet UIView *leftBarView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

+(CGFloat)getCellHeight;

@property (nonatomic, strong) AdPurchaseOrder *order;

@end
