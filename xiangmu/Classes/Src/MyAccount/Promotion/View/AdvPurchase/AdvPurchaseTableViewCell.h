//
//  AdvPurchaseTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/4/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AdvPurchaseCollectionView.h"

#import "HeaderView.h"

@interface AdvPurchaseTableViewCell : UITableViewCell

@property (nonatomic, strong) HeaderView *headerView;

@property (nonatomic, strong) AdvPurchaseCollectionView *collectionView;

@end
