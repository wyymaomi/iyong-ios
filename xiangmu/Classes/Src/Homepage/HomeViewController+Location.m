//
//  HomeViewController+Location.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomeViewController+Location.h"
#import "MyUtil.h"

#import "AppConfig.h"

@implementation HomeViewController (Location)

- (void)didSelectCity:(id)sender
{
//    self.isAccountInfoShow = NO;
    GYZChooseCityController *cityPickerVC = [[GYZChooseCityController alloc] init];
    [cityPickerVC setDelegate:self];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:cityPickerVC] animated:YES completion:^{
        //
    }];
}

#pragma mark - GYZCityPickerDelegate
- (void) cityPickerController:(GYZChooseCityController *)chooseCityController didSelectCity:(GYZCity *)city
{
    [AppConfig currentConfig].defaultCityName = city.cityName;
    [AppConfig currentConfig].defaultCityCode = city.cityID;
    
    self.gpsCityShortname = [MyUtil getCityShortnameFromCityCode:city.cityID];
    
    [self setNavTitleView];
    
    [self.viewModel.gpsCityList removeAllObjects];
    
    [self fetchAllData];
    //    [self.searchBar.cityButton setTitle:city.cityName forState:UIControlStateNormal];
    //    [LocationService saveCityToConfig:city.cityName];
    //    self.searchBar.searchTextField.text = @"";
    
    [chooseCityController dismissViewControllerAnimated:YES completion:nil];
}

- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController
{
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
