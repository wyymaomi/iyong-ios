//
//  ApiProxy.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Request.h"
#import "AFNetworking.h"

typedef void(^ApiSuccessHandle)(id responseData);
typedef void(^ApiFailHandle)(NSError *error);

@interface ApiProxy : NSObject

+(instancetype)sharedInstance;

@property (nonatomic, strong) AFHTTPRequestOperationManager *requestManager;

- (void)callApiWithParams:(NSString*)url params:(NSDictionary*)params requestType:(RequestType)requestType success:(ApiSuccessHandle)success fail:(ApiFailHandle)fail;



//- (NSInteger)callGetWithParams:(NSDictionary*)params success:



@end
