//
//  ServiceProviderRootViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ServiceProviderRootViewController.h"
#import "ServiceProviderViewController.h"
#import "GYZChooseCityController.h"
#import "YYStatusView.h"
#import "AppConfig.h"
#import "BaseViewController+Advertisement.h"

@interface ServiceProviderRootViewController ()<UIScrollViewDelegate, YYStatusViewDelegate,GYZChooseCityDelegate>

@property (nonatomic, strong) YYStatusView *statusView;

@property (nonatomic, strong) NSArray *titleArray;

//@property (nonatomic, strong) NSMutableArray *childViewControllerArray;

//@property (nonatomic, strong) UIScrollView *titleScrollView;

@property (nonatomic, strong) UIScrollView *contentScrollView;

@property (nonatomic, assign) BOOL isFirstIn;

@property (nonatomic, strong) NSString *cityName;

@end

@implementation ServiceProviderRootViewController

//- (NSMutableArray*)childViewControllerArray
//{
//    if (!_childViewControllerArray) {
//        _childViewControllerArray = [[NSMutableArray alloc] init];
//    }
//    
//    return _childViewControllerArray;
//}

- (NSArray*)titleArray;
{
    if (!_titleArray) {
        _titleArray = @[@"推荐", @"在线服务商", @"用户体验"];
    }
    return _titleArray;
}

- (YYStatusView*)statusView
{
    if (!_statusView) {
        _statusView = [[YYStatusView alloc] init];
        _statusView.delegate = self;
    }
    return _statusView;
}


#pragma  mark - life cycle


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    DLog(@"WYY ServiceProviderRootViewController viewDidLoad");
    
    _isFirstIn = YES;
    
    [self.view addSubview:self.statusView];
    
//    self.title = @"iYONG";
    
    self.cityName = [AppConfig currentConfig].gpsCityName;
    //IsStrEmpty([AppConfig currentConfig].defaultCityName)?DEFAULT_CITY_NAME:[AppConfig currentConfig].defaultCityName;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(onPublish)];
    
    [self setNavTitleView];
    
    // 
    [self setUpAllChildViewController];
    //初始化UIScrollView
    [self setUpScrollView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewServiceProvider) name:@"serviceProviderLoadMoreNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewUserExperience) name:@"experienceLoadMoreNotification" object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_isFirstIn) {
        self.statusView.frame = CGRectMake(0, 0, ViewWidth, 50);
        self.statusView.backgroundColor = [UIColor whiteColor];
        [self.statusView initWithTitles:self.titleArray NormalColor:[UIColor lightGrayColor] SelectedColor:[UIColor blackColor] LineColor:UIColorFromRGB(0x33A6DA)];
        
        //    self.contentScrollView.backgroundColor = [UIColor blueColor];
        self.contentScrollView.frame = CGRectMake(0, self.statusView.bottom, ViewWidth, self.view.height-self.statusView.height);
        self.contentScrollView.contentSize = CGSizeMake(self.titleArray.count * ViewWidth, self.contentScrollView.height);
        
        //    for (BaseViewController *viewController in self.childViewControllers) {
        //        viewController.view.height = self.contentScrollView.height;
        //    }
        
        // 默认显示第0个子控制器
        [self scrollViewDidEndDecelerating:self.contentScrollView];
        
        _isFirstIn = NO;

    }
    
}

//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    
//    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
//    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//
//}

-(void)setNavTitleView
{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor clearColor];
    UIImage *imageForButton = [UIImage imageNamed:@"icon_gps"];
    [button setImage:imageForButton forState:UIControlStateNormal];
    // 设置文字
    NSString *buttonTitleStr = self.cityName;
    [button setTitle:buttonTitleStr forState:UIControlStateNormal];
    button.titleLabel.font = FONT(15);
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    CGSize buttonTitleLabelSize = [buttonTitleStr sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}];
    CGSize buttonImageSize = imageForButton.size;
    button.frame = CGRectMake(0, 0, buttonImageSize.width + buttonTitleLabelSize.width + 20, buttonImageSize.height);
//    button.frame = CGRectMake(0, 0, 100, 100);
    [button setImageEdgeInsets:UIEdgeInsetsMake(0.0, -20, 0.0, 0.0)];
//    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
 
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.width, 30)];
    titleLabel.text = @"iYONG";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = BOLD_FONT(20);
    titleLabel.textAlignment = UITextAlignmentCenter;
    [view addSubview:titleLabel];
    self.navigationItem.titleView = view;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_advertisement"] style:UIBarButtonItemStyleDone target:self action:@selector(onClickAdvIcon)];

    
//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(<#CGFloat x#>, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)]
//#if 0
//    NSString *titleText = [NSString stringWithFormat:@"%@ >", self.cityName];
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 30)];
////    view.backgroundColor = [UIColor yellowColor];
//    self.navigationItem.titleView = view;
//    
////    UIImage *image=[[UIImage imageNamed:@"icon_iyong"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
////    UIImageView *imageView=[[UIImageView alloc]initWithImage:image];
////    imageView.width = 23;
////    imageView.height = 26;
////    imageView.left = view.width/2-35;
////    imageView.top = 2;
////    [view addSubview:imageView];
//    
//    NSString *text = @"iYONG";
//    NSInteger textWidth = [text textSize:CGSizeMake(ViewWidth, 30) font:BOLD_FONT(20)].width;
//    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(view.width/2-textWidth-15, 0, textWidth+10, 30)];
//    label1.text = @"iYONG";
//    label1.textColor = [UIColor whiteColor];
//    label1.font = BOLD_FONT(20);
//    label1.backgroundColor = [UIColor clearColor];
//    label1.textAlignment = UITextAlignmentLeft;
//    [view addSubview:label1];
//    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(view.width/2-5, 0, 5, 30)];
//    label.text = @"|";
//    label.textColor = UIColorFromRGB(0x64685A);//[UIColor darkGrayColor];
//    label.font = FONT(30);
//    label.backgroundColor = [UIColor clearColor];
//    [view addSubview:label];
//    
//    UILabel *cityLabel = [[UILabel alloc] initWithFrame:CGRectMake(view.width/2+8, 0, view.width/2, 30)];
//    cityLabel.text = titleText;
//    cityLabel.font = BOLD_FONT(20);
//    cityLabel.textColor = [UIColor whiteColor];
//    //    cityLabel.backgroundColor = [UIColor blueColor];
//    cityLabel.textAlignment = UITextAlignmentLeft;
//    cityLabel.userInteractionEnabled = YES;
//    [cityLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectCity:)]];
//    [view addSubview:cityLabel];
    
}


-(void)setUpAllChildViewController
{
    
    BaseViewController *recommentViewController = [[NSClassFromString(@"RecommendViewController") alloc] init];
    [self addChildViewController:recommentViewController];
    
    BaseViewController *firstViewController = [[NSClassFromString(@"ServiceProviderListController") alloc]init];
//    [self.childViewControllerArray addObject:vc];
//    firstViewController.view.height = self.view.frame.size.height-50;
    [self addChildViewController:firstViewController];
    
    
//    Class secondCls = NSClassFromString(@"ServiceProviderListController");
    BaseViewController *secondViewController = [[NSClassFromString(@"UserExperienceListViewController") alloc] init];
//    [self.childViewControllerArray addObject:viewController];
    [self addChildViewController:secondViewController];
    
}

-(void)setUpScrollView{
//    NSUInteger count = self.childViewControllers.count;
    //设置标题滚动条
//    self.titleScrollView.contentSize = CGSizeMake(count*labelw, 0);
//    self.titleScrollView.showsHorizontalScrollIndicator = NO;
    self.contentScrollView = [[UIScrollView alloc] init];
    //设置内容滚动条
    self.contentScrollView.contentSize = CGSizeMake(self.titleArray.count * ViewWidth, 0);
    //开启分页
    self.contentScrollView.pagingEnabled = YES;
    //去掉弹簧效果
    self.contentScrollView.bounces = NO;
    //隐藏水平滚动条
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    //设置代理
    self.contentScrollView.delegate = self;
//    self.contentScrollView.scrollEnabled = NO;
    
    [self.view addSubview:self.contentScrollView];
    
//    if (self.childViewControllers.count == 0) {
//        return;
//    }
//    
//    BaseViewController *vc = self.childViewControllers[0];
//    //添加子控制器View
//    //如果控制器已经创建好了  就直接return不再创建了
////    if(vc.isViewLoaded)return;
//    vc.view.frame = CGRectMake(0, 0, ViewWidth, self.contentScrollView.height);
//    [self.contentScrollView addSubview:vc.view];


    
}

#pragma mark - 点击广告

- (void)onClickAdvIcon
{
    self.showAdv = YES;
    [self showAdv:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 查看在线服务商

- (void)viewServiceProvider
{
    [self.statusView changeCurrenIndex:1];
//    [self.statusView changeTag:1];
//    [self.statusView setCurrentIndex:1];
//    [self statusViewSelectIndex:1];
}

#pragma mark - 查看用户体验
- (void)viewUserExperience
{
    [self.statusView changeCurrenIndex:2];
//    [self.statusView changeTag:2];
//    [self.statusView setCurrentIndex:2];
//    self.statusView.buttonArray
//    [self statusViewSelectIndex:2];
}


#pragma mark - 跳转到发布页面

#pragma mark - 跳转到服务商编辑页面

- (void)onPublish
{
    
    self.nextAction = @selector(onPublish);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    // 判断是在线服务还是用户体验
    if (self.statusView.currentIndex == 0) {
        // 服务商权限校验
        if ([[UserManager sharedInstance].userModel isRealnameCertified] && [[UserManager sharedInstance].userModel isCompanyCertified]) {
            [self resetAction];
//            [self gotoPage:@"ServiceProviderViewController" animated:YES title:@"发布服务商"];
            ServiceProviderViewController *viewController = [[ServiceProviderViewController alloc] init];
            viewController.title = @"发布服务商";
            viewController.type = PublishTypeBusiness;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            [self resetAction];
            WeakSelf
            [YYAlertView showAlertView:@"" message:@"请先进行实名认证和企业认证" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [weakSelf gotoPage:@"UserInfoViewController"];
                }
                
            }];
        }
    }
    if (self.statusView.currentIndex == 1) {
        // 发布用户体验
        if ([[UserManager sharedInstance].userModel isRealnameCertified]) {
            [self resetAction];
            ServiceProviderViewController *viewController = [[ServiceProviderViewController alloc] init];
            viewController.title = @"用户体验";
            viewController.type = PublishTypeUserExperience;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            [self resetAction];
            WeakSelf
            [YYAlertView showAlertView:@"" message:@"请先进行实名认证" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [weakSelf gotoPage:@"UserInfoViewController"];
                }
                
            }];
        }
        
    }
    
    
}

#pragma mark - YYStatusViewDelegate method

- (void)statusViewSelectIndex:(NSInteger)index;
{
    
    // 让底部的内容scrollView滚动到对应位置
    CGPoint offset = self.contentScrollView.contentOffset;
    offset.x = index * self.contentScrollView.frame.size.width;
    [self.contentScrollView setContentOffset:offset animated:YES];

}

#pragma mark --UIScrollViewDelegate
//监听scrollView的滚动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGFloat curPage = scrollView.contentOffset.x / scrollView.bounds.size.width;
//    //左边label的角标
//    NSInteger leftIndex = curPage;
//    //右边label的角标
//    NSInteger rightIndex = leftIndex + 1;
//    //获取左边的label
//    UILabel *leftLabel = self.titleLabels[leftIndex];
//    //获取最右边
//    UILabel *rightLabel;
//    if(rightIndex < self.titleArray.count-1){
//        rightLabel = self.titleArray[rightIndex];
//    }
//    //计算缩放比例
//    CGFloat rightScale = curPage - leftIndex;
//    CGFloat leftScale = 1 - rightScale;
//    //缩放
//    leftLabel.transform = CGAffineTransformMakeScale(leftScale*0.3+1, leftScale*0.3+1);
//    rightLabel.transform = CGAffineTransformMakeScale(rightScale*0.3+1, rightScale*0.3+1);
//    leftLabel.textColor = [UIColor colorWithRed:leftScale green:0 blue:0 alpha:1];
//    rightLabel.textColor = [UIColor colorWithRed:rightScale green:0 blue:0 alpha:1];
}

/**
 * scrollView结束了滚动动画以后就会调用这个方法（比如- (void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated;方法执行的动画完毕后）
 */
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    // 一些临时变量
    CGFloat width = scrollView.frame.size.width;
    CGFloat height = scrollView.frame.size.height;
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 当前位置需要显示的控制器的索引
    NSInteger index = offsetX / width;
    
    // 让对应的顶部标题居中显示
//    UILabel *label = self.titleScrollView.subviews[index];
//    CGPoint titleOffset = self.titleScrollView.contentOffset;
//    titleOffset.x = label.center.x - width * 0.5;
//    // 左边超出处理
//    if (titleOffset.x < 0) titleOffset.x = 0;
//    // 右边超出处理
//    CGFloat maxTitleOffsetX = self.titleScrollView.contentSize.width - width;
//    if (titleOffset.x > maxTitleOffsetX) titleOffset.x = maxTitleOffsetX;
//    
//    [self.titleScrollView setContentOffset:titleOffset animated:YES];
    
    
    
    // 取出需要显示的控制器
    UIViewController *willShowVc = self.childViewControllers[index];
    
    // 如果当前位置的位置已经显示过了，就直接返回
    if ([willShowVc isViewLoaded]) return;
    
    // 添加控制器的view到contentScrollView中;
    willShowVc.view.frame = CGRectMake(offsetX, 0, width, height);
    [scrollView addSubview:willShowVc.view];
    
    
}

//滚动完成的时候调用这个方法
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //获取滚动到哪一页
    // 一些临时变量
    CGFloat width = scrollView.frame.size.width;
    CGFloat height = scrollView.frame.size.height;
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 当前位置需要显示的控制器的索引
    NSInteger index = offsetX / width;
    
    // 让对应的顶部标题居中显示
    //    UILabel *label = self.titleScrollView.subviews[index];
    //    CGPoint titleOffset = self.titleScrollView.contentOffset;
    //    titleOffset.x = label.center.x - width * 0.5;
    //    // 左边超出处理
    //    if (titleOffset.x < 0) titleOffset.x = 0;
    //    // 右边超出处理
    //    CGFloat maxTitleOffsetX = self.titleScrollView.contentSize.width - width;
    //    if (titleOffset.x > maxTitleOffsetX) titleOffset.x = maxTitleOffsetX;
    //
    //    [self.titleScrollView setContentOffset:titleOffset animated:YES];
    
//    [self.statusView setCurrentIndex:index];
    
    [self.statusView changeTag:index];
    
    // 取出需要显示的控制器
    UIViewController *willShowVc = self.childViewControllers[index];
    
    // 如果当前位置的位置已经显示过了，就直接返回
    if ([willShowVc isViewLoaded]) return;
    
    // 添加控制器的view到contentScrollView中;
    willShowVc.view.frame = CGRectMake(offsetX, 0, width, height);
    [scrollView addSubview:willShowVc.view];
    
}

#pragma mark - 城市选择

- (void)didSelectCity:(id)sender
{
//    self.isAccountInfoShow = NO;
    GYZChooseCityController *cityPickerVC = [[GYZChooseCityController alloc] init];
    [cityPickerVC setDelegate:self];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:cityPickerVC] animated:YES completion:^{
        //
    }];
}

#pragma mark - GYZCityPickerDelegate
- (void) cityPickerController:(GYZChooseCityController *)chooseCityController didSelectCity:(GYZCity *)city
{
    
//    [AppConfig currentConfig].defaultCityName = city.cityName;
//    [AppConfig currentConfig].defaultCityCode = city.cityID;
    
    [AppConfig currentConfig].gpsCityCode = city.cityID;
    [AppConfig currentConfig].gpsCityName = city.cityName;
    self.cityName = city.cityName;
    
    [self setNavTitleView];
    
    //    [self.searchBar.cityButton setTitle:city.cityName forState:UIControlStateNormal];
    //    [LocationService saveCityToConfig:city.cityName];
    //    self.searchBar.searchTextField.text = @"";
    
    [chooseCityController dismissViewControllerAnimated:YES completion:nil];
}

- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController
{
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}


//-(void)showVc:(NSInteger)index{
//    CGFloat offsetX = index * ViewWidth;
//    //获取对应的子控制器
//    UIViewController *vc = self.childViewControllers[index];
//    //添加子控制器View
//    //如果控制器已经创建好了  就直接return不再创建了
//    if(vc.isViewLoaded)return;
//    vc.view.frame = CGRectMake(offsetX, 0, ViewWidth, self.contentScrollView.height);
//    [self.contentScrollView addSubview:vc.view];
//}


@end
