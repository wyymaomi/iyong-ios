//
//  OrderHeaderSectionView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderHeaderSectionView.h"
#import "OrderStatusHeader.h"

@implementation OrderHeaderSectionView

//+ (instancetype)customView
//{
//    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
//}

-(void)initDataWithHeaderStatus:(NSInteger)section
                   headerStatus:(OrderStatusHeader*)headerStatus
{
    self.isOpen = headerStatus.isOpen;
    self.titleLabel.text = headerStatus.title;
    self.iconImageView.image = [UIImage imageNamed:headerStatus.icon];
    self.tag = section;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(toggleAction:)] ;
    [self addGestureRecognizer:tapGesture];
    self.userInteractionEnabled = YES;
    if (!self.isOpen) {
        self.indicatorImageView.transform = CGAffineTransformRotate(self.indicatorImageView.transform, -M_PI/2);
    }
}

-(IBAction)toggleAction:(id)sender {
    
    self.isOpen = !self.isOpen;
    
    //旋转箭头内容
    if (!self.isOpen) {
        [UIView animateWithDuration:0.25 animations:^{
            self.indicatorImageView.transform = CGAffineTransformRotate(self.indicatorImageView.transform, -M_PI/2);
        }];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
            [self.delegate sectionHeaderView:self sectionClosed:self.tag];
        }
    }
    else {
        [UIView animateWithDuration:0.25 animations:^{
            self.indicatorImageView.transform = CGAffineTransformRotate(self.indicatorImageView.transform, M_PI/2);
        }];
        if (self.delegate && [self.delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
            [self.delegate sectionHeaderView:self sectionOpened:self.tag];
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
