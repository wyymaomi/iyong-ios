//
//  AdvPurchaseInfoTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvPurchaseInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *positionTItleView;
@property (weak, nonatomic) IBOutlet UILabel *selectionTipLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionTItleLabel;

@end
