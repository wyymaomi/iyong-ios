//
//  MeViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/9.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MeViewController.h"
#import "AccountInfoView.h"
#import "PostExperienceViewController.h"
#import "AppDelegate.h"
#import "MainTabBarController.h"
//#import "BaseWebViewController.h"
//#import "UITabBarController+EMConnection.h"
//#import "MainTabBarController+EMConnection.h"

@interface MeViewController ()<AccountInfoViewDelegate,AccountInfoTableViewDelegate>

@property (nonatomic, strong) AccountInfoView *accountInfoView;

@end

@implementation MeViewController

- (AccountInfoView*)accountInfoView
{
    if (!_accountInfoView) {
        _accountInfoView = [[AccountInfoView alloc] initWithFrame:self.view.frame];
        _accountInfoView.delegate = self;
        _accountInfoView.isNeedAnimation = YES;
        _accountInfoView.isVisible = NO;
        _accountInfoView.isBottom = YES;
        _accountInfoView.accountTableView.tableViewDelegate = self;
    }
    return _accountInfoView;
}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [self setDefaultStatusAndNavi];
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    [super viewWillAppear:animated];
    
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [self setupUnreadMessageCount];
    
    [self.accountInfoView.accountTableView reloadData];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    DLog(@"self.view.frame.size.height = %f", self.view.frame.size.height);
    
    self.accountInfoView.frame = CGRectMake(0, 0, ViewWidth, self.view.frame.size.height);
    
    if (self.accountInfoView.isBottom) {
        self.accountInfoView.subBgView.top = self.view.height-50*Scale;
    }
    else {
        self.accountInfoView.subBgView.top = 154;
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.accountInfoView];
//    self.accountInfoView.subBgView.top = self.view.frame.size.height-50*Scale;
//    self.accountInfoView.subBgView.top = self.view.frame.size.height-50*Scale;
    [self.accountInfoView.bottomButton addTarget:self action:@selector(onCollapseButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.title = @"我";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUnreadMessageCount) name:@"refreshMessageCount" object:nil];
    
    [self getFinanceUrl:nil];
    
    
}

-(void)getFinanceUrl:(void(^)(NSInteger code_status))block;
{
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(GET_FINANCE_URL_API) requestMethod:POST params:nil success:^(id responseData) {
        
        StrongSelf
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSString *data = responseData[@"data"];
            
            strongSelf.financeUrl = data;
            
            if (block) {
                block(code_status);
            }
            
        }
        else {
            
            if (block) {
                block(code_status);
            }
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (block) {
            block(NETWORK_FAILED);
        }
        
        
    }];
}

//- (void)

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    
//    [self setupUnreadMessageCount];
//}



//- (void)refreshTableView;
//{
//    [self.accountInfoView.accountTableView reloadData];
//}

- (void)setupUnreadMessageCount
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    
    self.accountInfoView.accountTableView.messageCount = unreadCount;
  
    [self.accountInfoView.accountTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:AccountInfoTableRowMessage inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
}

//- (void)viewDidLayoutSubviews
//{
//    [super viewDidLayoutSubviews];

//    if (!self.accountInfoView.isBottom) {
////        self.subBgView.top = 450*Scale;//self.frame.size.height-50*Scale;
//        self.accountInfoView.subBgView.top = self.view.frame.size.height-50*Scale;
//    }
//    else {
//        self.accountInfoView.subBgView.top = 154;
////        self.subBgView.top = 154;
//    }
//}

- (void)onGotoPage:(NSString*)vcPage title:(NSString*)title;
{
    [self gotoPage:vcPage animated:YES title:title];
}

- (void)openCompanyPublish
{
    PostExperienceViewController *viewController = [[PostExperienceViewController alloc] init];
    viewController.type = TableTypeCompanyBusiness;
    viewController.enter_type = ENTER_TYPE_PRESENT;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)openAdvPurchase
{
    [self gotoPage:@"AdPurchaseViewController"];
}

- (void)openProviderInfo
{
    [self gotoPage:@"ProviderInfoViewController"];
}

-(void)didSelectRow:(NSInteger)row;
{
//    _isAccountInfoShow = YES;
//    self.accountInfoView.isNeedAnimation = NO;
    
    if (row == AccountInfoTableRowAvatar) {
//        [self.accountInfoView dismiss];
        [self gotoPage:@"UserInfoViewController"];
    }
    
    if (row == AccountInfoTableRowMyAccount) {
//        [self.accountInfoView dismiss];
        [self gotoPage:@"MyAccountViewController" animated:YES title:@"我的帐户"];
    }
//    if (row == AccountInfoTableRowMyTravel) {
////        [self.accountInfoView dismiss];
//        [self gotoPage:@"OrderStatusListViewController" animated:YES title:@"我的行程"];
//    }
    if (row == AccountInfoTableRowSettings) {
//        [self.accountInfoView dismiss];
        [self gotoPage:@"SettingViewController"];
    }
//    if (row == AccountInfoTableRowMyInquiry) {
////        [self.accountInfoView dismiss];
//        [self gotoPage:@"MyBusinessCircleViewController"];
//    }
    if (row == AccountInfoTableRowMyFavorite) {
        [self gotoPage:@"ServiceProviderListController" animated:YES title:@"我的收藏"];
    }
    
    if (row == AccountInfoTableRowMessage) {
        [self gotoPage:@"ConversationListController" animated:YES title:@"消息"];
    }
    
    if (row == AccountInfoTableRowFinance) {
        
//        BaseWebViewController *viewController = [[BaseWebViewController alloc] init];
        if (!IsStrEmpty(self.financeUrl)) {
            [self gotoWebpage:self.financeUrl];
//            viewController.url = self.financeUrl;
//            viewController.enter_type = ENTER_TYPE_PUSH;
//            viewController.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:viewController animated:YES];
        }
        else {
            [self getFinanceUrl:^(NSInteger code_status) {
               
                if (code_status == STATUS_OK) {
                    [self gotoWebpage:self.financeUrl];
//                    viewController.url = self.financeUrl;
//                    viewController.enter_type = ENTER_TYPE_PUSH;
//                    viewController.hidesBottomBarWhenPushed = YES;
//                    [self.navigationController pushViewController:viewController animated:YES];
                }
                
            }];
//            [self getFinanceUrl];
//            viewController.url = self.financeUrl;
        }
//        else {
//            viewController.url = cellDict[@"vc"];
//        }
        //        viewController.url = cellDict[@"vc"];

        
    }
    
    if (row == AccountInfoTableRowCompanyMode) {
        [self openCompanyModePage];
//        [self gotoPage:@"CompanyModeViewController" animated:YES title:@"企业模式"];
    }
    
    if (row == AccountInfoTableRowMyTravel) {
        [self gotoPage:@"CarTravelListViewController"];
    }
    
    if (row == AccountInfoTableRowContactService) {
        NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",CONTACT_SERVICE_PHONENUMBER];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }
    
    self.nextAction = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onDismiss;
{
    
}

-(void)openCompanyModePage
{
    // 判断是否实名认证
    // 判断是否企业认证
    BOOL isRealnameCertification = [UserManager sharedInstance].userModel.isRealnameCertified;
    BOOL isCompanyCertification = [UserManager sharedInstance].userModel.isCompanyCertified;
    if (!isRealnameCertification) {
        [self showRealnameCertificationAlertView];
        return;
    }
    if (!isCompanyCertification) {
        
        WeakSelf
        
        [[UserManager sharedInstance] getAccountInfo:^(NSInteger code_status) {
            
            if (code_status == STATUS_OK) {
                
                if ([[UserManager sharedInstance].userModel isCompanyCertified]) {
                    
                    [weakSelf gotoPage:@"CompanyModeViewController"];
                    
                }
                else {
                    
                    [weakSelf showCompanyCertificationAlertView];
                    
                }
            }
            else {
                
                [weakSelf showCompanyCertificationAlertView];
                
            }
        }];
        
    }
    else {
        [self gotoPage:@"CompanyModeViewController"];
    }
    
    
}

-(void)showRealnameCertificationAlertView
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"还未实名认证" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"前往"];
    WeakSelf
    [alertView setConfirmBlock:^{
        [weakSelf gotoRealnameCertificationPage];
//        [weakSelf gotoPage:@"RealnameCertificationViewController"];
    }];
    
    [alertView show];
}

-(void)showCompanyCertificationAlertView
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"还未企业认证" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"前往"];
    [alertView setConfirmBlock:^{
        [self gotoPage:@"CompanyCertificationViewController"];
    }];
    [alertView show];
}



//BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"还未企业认证" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"前往"];
//WeakSelf
//[alertView setConfirmBlock:^{
//    [weakSelf gotoPage:@"CompanyCertificationViewController"];
//}];
//[alertView show];
//return;

- (void)onCollapseButtonClick
{
    
//    WeakSelf
//    if (![[UserManager sharedInstance].userModel isCompanyCertified]){
//        [MsgToolBox showAlert:@"提示" content:@"切换企业模式只有企业认证用户可以操作，请先进行企业认证"];
//        return;
//    }
    
//    self.accountInfoView.isBottom = !self.accountInfoView.isBottom;
    
    
    WeakSelf
    if (self.accountInfoView.subBgView.top > 154) {
        
//        WeakSelf
        
        // 服务商权限校验
        
        if (![[UserManager sharedInstance].userModel isCompanyCertified]) {
            // 如果服务商未通过认证 从后台查询用户信息
            
            [[UserManager sharedInstance] getAccountInfo:^(NSInteger code_status) {
                
                if (code_status == STATUS_OK) {
                    if ([[UserManager sharedInstance].userModel isCompanyCertified]) {
                        [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
                            weakSelf.accountInfoView.subBgView.top = 154;
//                            weakSelf.accountInfoView.isBottom = !weakSelf.accountInfoView.isBottom;
                            
                            return;
                        } completion:^(BOOL finished) {
//                            weakSelf.accountInfoView.isBottom = NO;
                            weakSelf.accountInfoView.isBottom = NO;
                        }];
                    }
                    else {
//                        weakSelf.accountInfoView.isBottom = YES;
                        weakSelf.accountInfoView.isBottom = YES;
                        [MsgToolBox showAlert:@"提示" content:@"切换企业模式只有企业认证用户可以操作，请先进行企业认证"];
                    }
                }
                else {
//                    weakSelf.accountInfoView.isBottom = NO;
                    weakSelf.accountInfoView.isBottom = YES;
                    [MsgToolBox showAlert:@"提示" content:@"切换企业模式只有企业认证用户可以操作，请先进行企业认证"];
                }
            }];
        }
        else {
            
            [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
                weakSelf.accountInfoView.subBgView.top = 154;
//                weakSelf.accountInfoView.isBottom = NO;
            } completion:^(BOOL finished) {
                //        [self.mj_popupViewController viewDidAppear:NO];
                weakSelf.accountInfoView.isBottom = NO;
            }];
        }
        
        
        
    }
    else {
        
        [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            weakSelf.accountInfoView.subBgView.top = self.view.frame.size.height-50*Scale;
//            weakSelf.accountInfoView.isBottom = YES;
//            weakSelf.accountInfoView.isBottom = !weakSelf.accountInfoView.isBottom;
        } completion:^(BOOL finished) {
            weakSelf.accountInfoView.isBottom = YES;
            //        [self.mj_popupViewController viewDidAppear:NO];
        }];
        
    }
    
}



@end
