//
//  BaiduMapTraceViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "BaiduTraceSDK/BaiduTraceSDK.h"

@interface BaiduMapTraceViewController : BaseViewController<BTKTraceDelegate, BTKFenceDelegate, BTKTrackDelegate, BTKEntityDelegate, BTKAnalysisDelegate>


@end
