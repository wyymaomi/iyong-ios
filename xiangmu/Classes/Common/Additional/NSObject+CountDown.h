//
//  NSObject+CountDown.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CountDownBlock)(NSUInteger timer);
typedef void(^CountdownFinishBlock)();

@interface NSObject (CountDown)

/** 定时器 */
@property (nonatomic,strong) dispatch_source_t timer;

/**
 按钮倒计时
 
 @param time 倒计时总时间
 @param countDownBlock 每秒倒计时会执行的block
 @param finishBlock 倒计时完成会执行的block
 */
- (void)countDownTime:(NSUInteger)time countDownBlock:(CountDownBlock)countDownBlock outTimeBlock:(CountdownFinishBlock)finishBlock;

- (void)cancelTimer;

@end
