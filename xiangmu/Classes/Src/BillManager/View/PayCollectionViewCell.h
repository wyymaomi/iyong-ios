//
//  PayCollectionViewCell.h
//  xiangmu
//
//  Created by David kim on 16/3/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong)UILabel *dateLabel;
//@property (nonatomic,strong)UILabel *planeLabel;
//@property (nonatomic,strong)UILabel *avenueLabel;
//@property (nonatomic,strong)UILabel *nameLabel;
//@property (nonatomic,strong)UILabel *phoneLabel;
@property (nonatomic,strong)UILabel *priceLabel;
//@property (nonatomic,strong)UILabel *dayLabel;
@end
