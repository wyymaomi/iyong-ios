//
//  PayHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface PayHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *bargainId;
@property (nonatomic, strong) NSString *rateId;
@property (nonatomic, strong) NSString *payPrice;
@property (nonatomic, strong) NSNumber *payType;

@end
