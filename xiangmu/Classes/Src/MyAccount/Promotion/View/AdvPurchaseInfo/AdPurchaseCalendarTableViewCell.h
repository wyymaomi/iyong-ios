//
//  AdPurchaseCalendarTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
#import "CustomCalendarCell.h"
#import "AdvPurchaseViewModel.h"

@protocol AdPurchaseCalendarTableViewCellDelegate <NSObject>

- (void)onSelectMonth:(NSDate*)date;
- (void)onClickDate:(NSString*)dateStr;
- (void)onDeselectDate:(NSString*)dateStr;

@end

@interface AdPurchaseCalendarTableViewCell : UITableViewCell<FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance>

@property (weak, nonatomic) FSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;

@property (strong, nonatomic) NSCalendar *gregorian;

@property (nonatomic, strong) AdvPurchaseViewModel *viewModel;

@property (nonatomic, weak) id<AdPurchaseCalendarTableViewCellDelegate> cellDelegate;

//- (void)initData:(
//- (void)initData:(

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

+(CGFloat)getCellHeight;

@end
