//
//  CheckUpdateService.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CheckUpdateService.h"
#import "SystemInfo.h"
#import "NetworkManager.h"
#import "MsgToolBox.h"

@implementation CheckUpdateService

- (id)init
{
    if (self = [super init]) {
        self.isForceUpgrade = NO;
    }
    
    return self;
}

-(void)checkVersion
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CHECK_VERSION_API];
    NSDictionary *params = @{@"type":@"1"};
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        if (code_status == STATUS_OK) {
            StrongSelf
//            [strongSelf updateAppVersion:result];
            NSString *app_version = [SystemInfo appVersion];
            DLog(@"app_version = %@", app_version);
            
            NSDictionary *data = result[@"data"];
            NSInteger ver_code = [data[@"versionCode"] integerValue];
            NSInteger update_level = [data[@"upgrade"] integerValue];
            strongSelf.updateInfo = data[@"description"];
            strongSelf.updateUrl = data[@"downloadUrl"];
            
            // 如果服务器端的版本号大于当前的应用版本号，则判断是否需要强制升级
            if (ver_code > [app_version integerValue]) {
                strongSelf.isForceUpgrade = (update_level == 1) ? YES : NO;
                [strongSelf popupUpgradeAlert];
            }
        }
        else if(code_status == STATUS_FAIL){
//            [MsgToolBox showAlert:@"" content:error];
        }
        else {
//            [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
        }
        
    }];
    
    
}

- (void)popupUpgradeAlert;
{
    if (self.isForceUpgrade) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"更新提示" message:[self.updateInfo replaceAll:@" " with:@"\n"]  delegate:self cancelButtonTitle:nil otherButtonTitles:@"升级", nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                //// Yes condition
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.updateUrl]];
            }
        }];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"更新提示" message:[self.updateInfo replaceAll:@" " with:@"\n"] delegate:self cancelButtonTitle:@"以后更新" otherButtonTitles:@"立即更新", nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.updateUrl]];
            }
        }];
    }
}

- (void)updateAppVersion:(NSDictionary *)result
{
    NSString *app_version = [SystemInfo appVersion];
    DLog(@"app_version = %@", app_version);
    
    NSDictionary *data = result[@"data"];
    NSInteger ver_code = [data[@"versionCode"] integerValue];
    NSInteger update_level = [data[@"upgrade"] integerValue];
    self.updateInfo = data[@"description"];
    self.updateUrl = data[@"downloadUrl"];
    
    // 如果服务器端的版本号大于当前的应用版本号，则判断是否需要强制升级
    if (ver_code > [app_version integerValue]) {
        if (update_level == 1) {
            self.isForceUpgrade = YES;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"更新提示" message:self.updateInfo delegate:self cancelButtonTitle:nil otherButtonTitles:@"升级", nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    //// Yes condition
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.updateUrl]];
                }
            }];
        }
        else {
            self.isForceUpgrade = NO;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"更新提示" message:[self.updateInfo replaceAll:@" " with:@"\n"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"更新", nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 1) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.updateUrl]];
                }
            }];
        }
    }
}

@end
