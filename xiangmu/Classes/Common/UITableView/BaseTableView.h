//
//  BaseTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableView : UITableView

@property (nonatomic, assign) NSInteger numberOfSection;
@property (nonatomic, assign) NSInteger numberOfRow;

- (void)setup;

@end
