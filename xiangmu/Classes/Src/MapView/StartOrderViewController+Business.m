//
//  StartOrderViewController+Business.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "StartOrderViewController+Business.h"
#import "AppDelegate.h"

@implementation StartOrderViewController (Business)



//- (void)onOrderReceived:(NSNotification *)notification
//{
//    
//    DLog(@"receive Order has Received Notification");
//    
//    NSDictionary * infoDic = [notification object];
//    
//    self.vechileOrderHttpMessage.orderId = infoDic[@"orderId"];
//    [self stopDriverTimer];
//    [self getOrderDetail];
//}



-(void)getOrderDetail
{
    if (IsStrEmpty(self.vechileOrderHttpMessage.orderId)) {
        return;
    }
    
    NSString *params = [NSString stringWithFormat:@"orderId=%@", self.vechileOrderHttpMessage.orderId];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(GET_TRAVEL_ORDER_DETAIL_API) requestMethod:POST params:params success:^(id responseData) {
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            TravelOrderModel *order = [[TravelOrderModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            
            if ([order.orderStatus integerValue] == CarOrderStatusPendingPay) {

                // 日期
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:[order.startTime doubleValue]/1000.0f];
                weakSelf.vechileOrderHttpMessage.carDateTime = [date stringWithDateFormat:kNSDateHelperFormatSQLDateWithMonth];
                weakSelf.vechileOrderHttpMessage.carType = [order.carType integerValue];
                weakSelf.vechileOrderHttpMessage.startLocation = order.startLocation;
                weakSelf.vechileOrderHttpMessage.endLocation = order.endLocation;
                weakSelf.vechileOrderHttpMessage.carUnitPrice = [order.unitPrice floatValue];
                weakSelf.vechileOrderHttpMessage.distance = [order.mileage floatValue];
                weakSelf.vechileOrderHttpMessage.carPrice = [order.amount floatValue];
                weakSelf.distanceLabel.text = [NSString stringWithFormat:@"行程：%.fkm", weakSelf.vechileOrderHttpMessage.distance];
                weakSelf.priceLabel.text = [NSString stringWithFormat:@"%.f", weakSelf.vechileOrderHttpMessage.carPrice];
                
                weakSelf.vechileOrderHttpMessage.driverMobile = order.driverMobile;
                weakSelf.vechileOrderHttpMessage.carModel = order.carModel;
                weakSelf.vechileOrderHttpMessage.carNumber = order.carNumber;
                weakSelf.vechileOrderHttpMessage.driverName = [order getFormatDriverName];
//                weakSelf.vechileOrderHttpMessage.receiveOrderTime = [order.receiveOrderTime doubleValue];
                weakSelf.vechileOrderHttpMessage.receiveCutoffTime = [order.receiveCutoffTime doubleValue];
                weakSelf.vechileOrderHttpMessage.companyName = order.companyName;
                weakSelf.vechileOrderHttpMessage.receiveOrderTime = [order.receiveOrderTime doubleValue];
                
                weakSelf.receiveOrderStatus = ReceiveOrderStatusComplete;
                [weakSelf stopDriverTimer];
                
                [MsgToolBox showToast:@"请在司机接单后15分钟内支付"];
                
            }
            
        }
        else {
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

-(void)stopDriverTimer;
{
    if (self.driverReceiveTime) {
        [self.driverReceiveTime invalidate];
        self.driverReceiveTime = nil;
    }
}

-(void)onReceiveCancelOrderMessage:(id)sender
{
    AppDelegate *appDelegate = APP_DELEGATE;
    
    UITabBarController*tab = (UITabBarController*)appDelegate.window.rootViewController;
    
    UINavigationController *nvc = tab.selectedViewController;
    
    UIViewController *vc = nvc.visibleViewController;
    
    //防止同一界面多次push
    if ([vc isKindOfClass:[self class]]) {
        
        //        [[NSNotificationCenter defaultCenter] postNotificationName:kGetOrderReceivedNotification object:data];
        
//        StartOrderViewController *viewController = (StartOrderViewController*)vc;
//        [viewController orderReceived:data];
        
        [self stopDriverTimer];
        [self pressedBackButton];
        
        
    }
    

//    [[NSNotificationCenter defaultCenter] postNotificationName:kOnCancelCarOrderSuccessNotification object:nil];
}

@end
