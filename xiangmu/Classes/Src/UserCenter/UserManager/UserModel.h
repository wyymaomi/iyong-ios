//
//  UserModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface UserModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *certification; // 实名认证
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *companyName;
@property (nonatomic, strong) NSString<Optional> *deviceId;
@property (nonatomic, strong) NSString<Optional> *email;
@property (nonatomic, strong) NSNumber<Optional> *empower;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *idNumber;
@property (nonatomic, strong) NSNumber<Optional> *level;
@property (nonatomic, strong) NSString<Optional> *logoUrl;
@property (nonatomic, strong) NSString<Optional> *nickname;
@property (nonatomic, strong) NSString<Optional> *password;
@property (nonatomic, strong) NSString<Optional> *realName;
@property (nonatomic, strong) NSString<Optional> *refCompanyId;
@property (nonatomic, strong) NSString<Optional> *refMobile;
@property (nonatomic, strong) NSString<Optional> *username;
@property (nonatomic, strong) NSNumber<Optional> *valid;
@property (nonatomic, strong) NSNumber<Optional> *companyCertification; // 4种状态
@property (nonatomic, strong) NSString<Optional> *businessLicenceUrl;
@property (nonatomic, strong) NSString<Optional> *companyAddress;
@property (nonatomic, strong) NSNumber<Optional> *carCertification; // 是否车辆认证 0 或 1
@property (nonatomic, strong) NSNumber<Optional> *companyScor;
@property (nonatomic, strong) NSNumber<Optional> *bidPermisson;
@property (nonatomic, strong) NSString<Optional> *areaCode;
@property (nonatomic, strong) NSString<Optional> *areaName;

- (NSString*)displayChatName; // 显示聊天用的昵称：公司名称/昵称/手机号

- (NSString*)displayNickname; // 我的界面显示昵称

- (BOOL)isRealnameCertified; // 是否已经实名认证

- (BOOL)isCompanyCertified; // 是否已经企业认证

@end
