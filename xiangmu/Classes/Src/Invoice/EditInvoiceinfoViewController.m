//
//  EditInvoiceinfoViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "EditInvoiceinfoViewController.h"
#import "InvoiceInfoEditTableViewCell.h"
#import "ContactInfoTableViewCell.h"
#import "InvoiceMemoryTableViewCell.h"
#import "ActionSheetCityPicker.h"
#import "InvoiceInfoViewController.h"
#import "InvoiceSettingListViewController.h"

@interface EditInvoiceinfoViewController ()<UITextViewDelegate>

@property (nonatomic, assign) BOOL isOpen;

@end

@implementation EditInvoiceinfoViewController

-(InvoiceinfoHttpMessage*)invoiceinfoHttpMessage
{
    if (!_invoiceinfoHttpMessage) {
        _invoiceinfoHttpMessage = [InvoiceinfoHttpMessage new];
    }
    return _invoiceinfoHttpMessage;
}

-(NSMutableArray*)amountList
{
    if (!_amountList) {
        _amountList = [NSMutableArray array];
    }
    return _amountList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"发票信息";
    
    self.isOpen = YES;
    
    [self.view addSubview:self.tpkeyboardAvoidingGroupTableView];
    self.tpkeyboardAvoidingGroupTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)onClickUpdownArrow
{
    self.isOpen = !self.isOpen;
    
    [self.tpkeyboardAvoidingGroupTableView reloadData];
    
}

#pragma  mark - 城市选择

-(void)selectCity
{
    DLog(@"doSelectBankCity");
    
    [[self.tpkeyboardAvoidingGroupTableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.tpkeyboardAvoidingGroupTableView] resignFirstResponder];
    
    WeakSelf
    
    ActionSheetCityPicker *cityPicker = [[ActionSheetCityPicker alloc] initWithTitle:@"" level:AreainfoLevelDistrict doneBlock:^(ActionSheetCityPicker *picker, AreaInfoModel *province, AreaInfoModel *city, AreaInfoModel *district) {
        
        NSMutableString *areaName = [NSMutableString string];
        if ([province.name isEqualToString:@"北京"] ||
            [province.name isEqualToString:@"上海"] ||
            [province.name isEqualToString:@"重庆"] ||
            [province.name isEqualToString:@"天津"]) {
            
            [areaName appendFormat:@"%@%@", city.name, district.name];
        }
        else {
            [areaName appendFormat:@"%@%@%@", province.name, city.name, district.name];
        }

        weakSelf.invoiceinfoHttpMessage.areaCode = district.id;
        weakSelf.invoiceinfoHttpMessage.areaName = areaName;
        [weakSelf.tpkeyboardAvoidingGroupTableView reloadData];
        
//        NSString *msg = [NSString stringWithFormat:@"%@%@%@, 地区代码：%@", province.name, city.name, district.name, district.id];
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        [alertView show];
        
    } cancelBlock:^(ActionSheetCityPicker *picker) {
        
        
    } origin:self.view];

    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardWillHideNotification object:nil];
    
//    self.tpkeyboardAvoidingGroupTableView TPKeyboardAvoiding_keyboardWillHide:<#(NSNotification *)#>
    
    [cityPicker showActionSheetPicker];
    
}

#pragma mark -

-(void)selectInvoiceSetting
{
    InvoiceSettingListViewController *viewController = [[InvoiceSettingListViewController alloc] init];
//    viewController.completeHandle(InvoiceSettingHttpMessage *settingHttpMessage){
    
//    };
    viewController.sourceFrom = InvoiceListSourceFromEditInfo;
    
    viewController.completeHandle = ^(InvoiceSettingHttpMessage *settingHttpMessage) {
        
        if (settingHttpMessage) {
            
            self.invoiceinfoHttpMessage.taxpayerNumber = settingHttpMessage.taxpayerNumber;
            self.invoiceinfoHttpMessage.header = settingHttpMessage.header;
            self.invoiceinfoHttpMessage.registerAddress = settingHttpMessage.registerAddress;
            self.invoiceinfoHttpMessage.openingBank = settingHttpMessage.openingBank;
            self.invoiceinfoHttpMessage.bankAccount = settingHttpMessage.bankAccount;
            self.invoiceinfoHttpMessage.contact = settingHttpMessage.contact;
            self.invoiceinfoHttpMessage.tel = settingHttpMessage.tel;
            self.invoiceinfoHttpMessage.areaCode = settingHttpMessage.areaCode;
            self.invoiceinfoHttpMessage.areaName = settingHttpMessage.areaName;
            self.invoiceinfoHttpMessage.address = settingHttpMessage.address;
            self.invoiceinfoHttpMessage.remark = settingHttpMessage.remark;
            
            [self.tpkeyboardAvoidingGroupTableView reloadData];
            
        }
        
    };
    [self.navigationController pushViewController:viewController animated:YES];
//    [self gotoPage:@"InvoiceSettingListViewController"];
}

#pragma makr - 提交

-(void)onSubmit:(id)sender
{
//    NSMutableString *params = [NSMutableString new];
    NSMutableArray *amountParams = [NSMutableArray new];
    [self.amountList enumerateObjectsUsingBlock:^(InvoiceData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        params appendFormat:[NSString stringWithFormat:@"]
        NSString *string = [NSString stringWithFormat:@"invoiceDetails[%ld].amount=%.f&invoiceDetails[%ld].quantity=%ld", idx, obj.amount, idx, (long)obj.times];
        [amountParams addObject:string];
    }];
    InvoiceInfoViewController *viewController = [[InvoiceInfoViewController alloc] init];
    viewController.requestParams = [NSString stringWithFormat:@"%@&%@", self.invoiceinfoHttpMessage.description, [amountParams componentsJoinedByString:@"&"]];
    viewController.invoiceInfoHttpMessage = self.invoiceinfoHttpMessage;
    viewController.mode = InvoiceInfoModeSubmit;
    [self.navigationController pushViewController:viewController animated:YES];
    
//    [self gotoPage:@"InvoiceSettingListViewController"];
//    [self gotoPage:@"InvoiceInfoViewController"];
}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
    //    return self.cellTitleList.count;
//    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else if (section == 1){
        return 1;
    }
    return 1;
    //    NSArray *list = self.cellList[section];
    //    return list.count;
//    return self.cellTitleList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        if (self.isOpen) {
            return [InvoiceInfoEditTableViewCell getCellHeight];
        }
        else {
            return [InvoiceInfoEditTableViewCell getCloseStatusCellHeight];
        }
    }
    else if (indexPath.section == 1 && indexPath.row == 0) {
        return [ContactInfoTableViewCell getCellHeight];
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        return [InvoiceMemoryTableViewCell getCellHeight];
    }
    return 0.00001f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    return 10;
    if (section == 0) {
        return 10;
    }
    
    return 0.00001f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView]-1) {
        return 77;
    }
    return 10;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView]-1) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 77)];
        
        UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [okButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xe06430)] forState:UIControlStateNormal];
        okButton.frame = CGRectMake(10, 23, ViewWidth-20, 45);
        okButton.titleLabel.font = FONT(16);
        [okButton setTitle:@"提交" forState:UIControlStateNormal];
        [okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [okButton addTarget:self action:@selector(onSubmit:) forControlEvents:UIControlEventTouchUpInside];
        okButton.cornerRadius = 5;
        [view addSubview:okButton];
        
        return view;
    }
    
    return [UIView new];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        InvoiceInfoEditTableViewCell *cell = [InvoiceInfoEditTableViewCell cellWithTableView:tableView];
        
        cell.invoiceIdentifierView.hidden = !self.isOpen;
        cell.registAddressView.hidden = !self.isOpen;
        cell.bankNameView.hidden = !self.isOpen;
        cell.bankcardNumberView.hidden = !self.isOpen;
        
        UIImage *updownButtonImage = self.isOpen?[UIImage imageNamed:@"btn_down_arrow"]:[UIImage imageNamed:@"btn_up_arrow"];
        [cell.updownButton setImage:updownButtonImage forState:UIControlStateNormal];
        
        [cell.updownButton addTarget:self action:@selector(onClickUpdownArrow) forControlEvents:UIControlEventTouchUpInside];
        [cell.companyInvoiceView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickUpdownArrow)]];
        
        [cell.selectButton addTarget:self action:@selector(selectInvoiceSetting) forControlEvents:UIControlEventTouchUpInside];
        
        [cell initData:self.invoiceinfoHttpMessage];
        
        [cell.titleTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.taxpayerNumberTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.registAddressTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.bankNameTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];

        [cell.bankAccountTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        return cell;
        
    }
    else if (indexPath.section == 1) {
        
        ContactInfoTableViewCell *cell = [ContactInfoTableViewCell cellWithTableView:tableView];
        
        cell.contactPhoneTextField.text = [StringUtil getSafeString:self.invoiceinfoHttpMessage.tel];
        
        cell.contactNameTextField.text = [StringUtil getSafeString:self.invoiceinfoHttpMessage.contact];
        
        cell.contactAreaTextField.text = [StringUtil getSafeString:self.invoiceinfoHttpMessage.areaName];
        
        cell.addressDetailTextField.text = [StringUtil getSafeString:self.invoiceinfoHttpMessage.address];
        
        [cell.areaView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCity)]];
        
        [cell.contactNameTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.contactPhoneTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell.addressDetailTextField addTarget:self action:@selector(onInvoiceTextChanged:) forControlEvents:UIControlEventEditingChanged];
        
        
        
        return cell;
        
    }
    else if (indexPath.section == 2) {
        
        InvoiceMemoryTableViewCell *cell = [InvoiceMemoryTableViewCell cellWithTableView:tableView];
        
        cell.memoTextView.text = [StringUtil getSafeString:self.invoiceinfoHttpMessage.remark];
        
        cell.memoTextView.delegate = self;
        
        return cell;
        
    }
    
    return nil;
    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
//        [self.customDelegate didSelectRowAtIndexPath:indexPath];
//    }
    
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -

- (void)onInvoiceTextChanged:(id)sender {
    
    if ([sender isKindOfClass:[UITextField class]]) {
        UITextField *textField = sender;
        
        NSInteger tag = textField.tag;
        
        NSString *text = textField.text.trim;
        
        switch (tag) {
            case InvoiceTextFieldTypeTitle:
                self.invoiceinfoHttpMessage.header = text;
                break;
            case InvoiceTextFieldTypeTaxpayerNumber:
                self.invoiceinfoHttpMessage.taxpayerNumber = text;
                break;
            case InvoiceTextFieldTypeRegistAddress:
                self.invoiceinfoHttpMessage.registerAddress = text;
                break;
            case InvoiceTextFieldTypeBankName:
                self.invoiceinfoHttpMessage.openingBank = text;
                break;
            case InvoiceTextFieldTypeBankAccount:
                self.invoiceinfoHttpMessage.bankAccount = text;
                break;
            case InvoiceTextFieldTypeContactPhoneNumber:
                self.invoiceinfoHttpMessage.tel = text;
                break;
            case InvoiceTextFieldTypeDetailAddress:
                self.invoiceinfoHttpMessage.address = text;
                break;
            case InvoiceTextFieldTypeContacterName:
                self.invoiceinfoHttpMessage.contact = text;
                break;
                
            default:
                break;
        }
    }
    

}

- (void)textViewDidChange:(UITextView *)textView;
{
    self.invoiceinfoHttpMessage.remark = textView.text.trim;
}


@end
