//
//  ZhanghuYueViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AccountBalanceViewController.h"
#import "MyUtil.h"
#import "RechargeViewController.h"
#import "GeneralAskViewController.h"

@interface AccountBalanceViewController ()

@property (nonatomic, strong) UILabel *amountLabel;  // 余额
@property (nonatomic, strong) NSString *amount;

@property (nonatomic, strong) UIButton *rechargeButton;
@property (nonatomic, strong) UIButton *drawMoneyButton;

@property (nonatomic, strong) UILabel *rechargeInfoLabel;

@end

@implementation AccountBalanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.mode == 0) {
        self.title = @"账户余额";
    }
    else {
        self.title = @"商户收入";
    }
//    self.title = @"账户余额";
    [self createInterface];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self.view endEditing:YES];
    [self getFinanceInfo];
    
    if (self.mode == 0) {
        [self getRechrageActivityInfo];
    }
}

-(void)getFinanceInfo
{
    self.nextAction = @selector(getFinanceInfo);
    self.object1 = nil;
    self.object2 = nil;
    
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_FINANCE_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            
            // 如果是用户账户余额 显示用户账户余额
            // 如果是商家收入 显示商家收入
            if (strongSelf.mode == 0) {
                NSNumber *amount = data[@"accountAmount"];
                strongSelf.amount = [StringUtil getSafeString:[amount stringValue]];// data[@"accountAmount"];
            }
            else {
                NSNumber *amount = data[@"incomeAmount"];
                strongSelf.amount = [StringUtil getSafeString:[amount stringValue]];
            }
            strongSelf.amountLabel.text = [NSString stringWithFormat:@"¥ %@", strongSelf.amount];
//            strongSelf.amount = data[@"accountAmount"];
//
            //            _creditLine = data[@"creditLine"];
            //            _creditAmount = data[@"creditAmount"];
            //            _accountAmount = data[@"accountAmount"];
        }
//        else {
//            //            _creditLine = nil;
//            //            _creditAmount = nil;
//            //            _accountAmount = nil;
//        }
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
//        return;
    }];
    
}

-(void)getRechrageActivityInfo
{
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(RECHARGE_ACTIVITY_API) requestMethod:POST params:nil success:^(id responseData) {
        
        StrongSelf
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSString *data = responseData[@"data"];
            
            if (IsStrEmpty(data)) {
                strongSelf.rechargeInfoLabel.hidden = YES;
            }
            else {
                strongSelf.rechargeInfoLabel.hidden = NO;
                strongSelf.rechargeInfoLabel.text = data;
            }
            
        }
        
        
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
    
}


-(void)createInterface
{
    
    _rechargeInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, ViewWidth, 16)];
    _rechargeInfoLabel.textColor = [UIColor redColor];
    _rechargeInfoLabel.textAlignment = UITextAlignmentCenter;
    _rechargeInfoLabel.font = FONT(16);
    _rechargeInfoLabel.hidden = YES;
    _rechargeInfoLabel.text = @"";
    [self.view addSubview:_rechargeInfoLabel];
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake((ViewWidth-353/3)/2, 60, 353/3, 334/3)];
    imageView.image=[UIImage imageNamed:@"钻石"];
    [self.view addSubview:imageView];
    
    UILabel *titleLabel=[MyUtil createLabelFrame:CGRectMake(0, 200, ViewWidth, 30) title:@"我的余额" font:[UIFont systemFontOfSize:14] textAlignment:NSTextAlignmentCenter numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
    [self.view addSubview:titleLabel];
    
    
    self.amountLabel=[MyUtil createLabelFrame:CGRectMake(0, 225, ViewWidth, 30) title:@"¥1000" font:[UIFont systemFontOfSize:20] textAlignment:NSTextAlignmentCenter numberOfLines:1 textColor:[UIColor blackColor]];
    self.amountLabel.tag = 10001;
    self.amountLabel.text = self.accountBalance;
    [self.view addSubview:self.amountLabel];
    
    UIButton *btn=[MyUtil createBtnFrame:CGRectMake(20, 280, ViewWidth-40, 40) title:@"充值" bgImageName:nil target:self action:@selector(doRecharge)];
    btn.titleLabel.font=[UIFont systemFontOfSize:16];
    btn.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:150.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
    btn.layer.cornerRadius=18.0f;
    [btn blueStyle];
    [self.view addSubview:btn];
    _rechargeButton = btn;
    
    UIButton *btn1=[MyUtil createBtnFrame:CGRectMake(20, 340, ViewWidth-40, 40) title:@"提现" bgImageName:nil target:self action:@selector(doDrawMoney)];
    btn1.backgroundColor=[UIColor whiteColor];
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn1.titleLabel.font=[UIFont systemFontOfSize:16];
    btn1.layer.cornerRadius=18.0f;
    btn1.borderColor = [UIColor lightGrayColor];
    btn1.borderWidth = 0.5f;
    [self.view addSubview:btn1];
    _drawMoneyButton = btn1;

    
    // 底部文字
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 44 - 20 - 25, self.view.frame.size.width, 20)];
//    label.backgroundColor = [UIColor lightGrayColor];
    label.font = FONT(12);
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = UITextAlignmentCenter;
    label.text = @"本服务由环讯支付提供底层技术支持";
    [self.view addSubview:label];
    label.hidden = YES;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"常见问题" forState:UIControlStateNormal];
    [button setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    button.titleLabel.font = FONT(14);
    button.frame = CGRectMake(0, label.frame.origin.y-25, self.view.frame.size.width, 20);
    [button addTarget:self action:@selector(generalQuestions) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    if (self.mode == 0 ) {
        _rechargeButton.hidden = NO;
        _drawMoneyButton.hidden = YES;
    }
    else {
        _rechargeButton.hidden = YES;
        _drawMoneyButton.hidden = NO;
        _drawMoneyButton.frame = _rechargeButton.frame;
    }
    

    
    
}

#pragma mark - 常见问题
-(void)generalQuestions
{
    if (self.mode == 0) {
        [self openWebView:RECHARGE_EXPLAIN_HTML];
    }
    else if (self.mode == 1) {
        [self openWebView:DRAW_MONEY_EXPLAIN_HTML];
    }
//    return;
//    GeneralAskViewController *viewController = [[GeneralAskViewController alloc] init];
//    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma  mark - 充值
-(void)doRecharge
{
    

    
    [self gotoPage:@"NewRechargeViewController"];
//    [self gotoPage:@"RechargeViewController"];
//    RechargeViewController *viewController = [[RechargeViewController alloc] init];
//    viewController.type = 0;
//    [self.navigationController pushViewController:viewController animated:YES];
    
}

-(void)doDrawMoney
{
    if ([self.amount integerValue] == 0) {
        [MsgToolBox showToast:@"商户收入为0，不能提现"];
        return;
    }
    RechargeViewController *viewController = [[RechargeViewController alloc] init];
    viewController.type = 1;
    viewController.accountMoney = self.amount;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
