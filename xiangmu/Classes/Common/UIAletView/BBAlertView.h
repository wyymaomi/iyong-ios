//
//  BBAlertView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputPaypwdView.h"
#import "BankcardListView.h"
#import "TextInputView.h"
//#import "GuideView1.h"
//#import "GuideViewFirst.h"

@interface NSObject (BBAlert)

- (void)alertCustomDlg:(NSString *)message;

- (void)dismissAllCustomAlerts;

@end

/*!
 @enum      BBAlertViewStyle
 @abstract  alertView的style，通过style来确定UI, 现在只有两种UI
 */
typedef enum {
    BBAlertViewStyleDefault,        // Default
    BBAlertViewStyleDefault1,       // Default1
    BBAlertViewStyleLogin,          // 登录
    BBAlertViewStyleCheck,          // 确认
    BBAlertViewStyleCustomView,     // 可放入自定义的view
    BBAlertViewStyleLogout,          // 退出登录用
    BBAlertViewStylePayment,        // 输入支付密码用
    BBAlertViewStyleBankcard,        // 充值时选择银行卡列表用
    BBAlertViewPublishPrice,    // 发布报价
    BBAlertViewMaskView         // 遮罩层
//    BBAlertView
//    BBAlertViewGuidePage1
}BBAlertViewStyle;

#if NS_BLOCKS_AVAILABLE
typedef void (^BBBasicBlock)(void);
#endif

#define LEFT_BUTTON_BG_COLOR NAVBAR_BGCOLOR
#define TITLE_TEXT_COLOR UIColorFromRGB(0x333333)

@protocol BBAlertViewDelegate;

@interface BBAlertView : UIView
{
@private
    id <BBAlertViewDelegate> __weak _delegate;
    UILabel   *_titleLabel;
    UILabel   *_bodyTextLabel;
    UITextView *_bodyTextView;
    UIView    *_customView;
    UIView    *_contentView;
    UIView    *_backgroundView;
    BOOL    _visible;
    BOOL    _dimBackground;
    UIInterfaceOrientation _orientation;
    BBAlertViewStyle   _style;
#if NS_BLOCKS_AVAILABLE
    BBBasicBlock    _cancelBlock;
    BBBasicBlock    _confirmBlock;
#endif
}

@property (nonatomic, strong) InputPaypwdView *paymentView;

@property (nonatomic, strong) BankcardListView *bankcardListView;

@property (nonatomic, strong) TextInputView *textInputView;

//@property (nonatomic, strong) GuideViewFirst *guideView1;

@property (nonatomic, strong) UIImageView *iconImageView;

/*!
 是否正在显示
 */
@property (nonatomic, readonly, getter=isVisible) BOOL visible;

/*!
 背景是否有渐变背景, 默认YES
 */
@property (nonatomic, assign) BOOL dimBackground;       //是否渐变背景，默认YES

/*!
 背景视图，覆盖全屏的，默认nil
 */
@property (nonatomic, strong) UIView *backgroundView;   //背景view, 可无
@property (nonatomic, assign) BBAlertViewStyle style;

/*!
 在点击确认后,是否需要dismiss, 默认YES
 */
@property (nonatomic, assign) BOOL shouldDismissAfterConfirm;

/*!
 文本对齐方式
 */
@property (nonatomic, assign) UITextAlignment contentAlignment;


@property (nonatomic, weak) id<BBAlertViewDelegate> delegate;

/*!
 @abstract      点击取消按钮的回调
 @discussion    如果你不想用代理的方式来进行回调，可使用该方法
 @param         block  点击取消后执行的程序块
 */
- (void)setCancelBlock:(BBBasicBlock)block;

/*!
 @abstract      点击确定按钮的回调
 @discussion    如果你不想用代理的方式来进行回调，可使用该方法
 @param         block  点击确定后执行的程序块
 */
- (void)setConfirmBlock:(BBBasicBlock)block;

/*!
 @abstract      初始话方法，默认的style：BBAlertViewStyleDefault
 @param         title  标题
 @param         style 样式
 @param         message  内容
 @param         delegate  代理
 @param         cancelButtonTitle  取消按钮title
 @param         otherButtonTitle  其他按钮，如确定
 @result        BBAlertView的对象
 */
- (id)initWithTitle:(NSString *)title
              style:(NSInteger)style
            message:(NSString *)message
           delegate:(id <BBAlertViewDelegate>)delegate
  cancelButtonTitle:(NSString *)cancelButtonTitle 
  otherButtonTitles:(NSString *)otherButtonTitle;

/*!
 @abstract      user this to init with content view
 */
- (id)initWithContentView:(UIView *)contentView;

/*!
 @abstract      初始话方法，默认的style：BBAlertViewStyleDefault
 @param         style  UI类型
 @param         title  标题
 @param         message  内容
 @param         customView 自定义的view,位于内容和button的中间（不常用），一般设为nil
 @param         delegate  代理
 @param         cancelButtonTitle  取消按钮title
 @param         otherButtonTitle  其他按钮，如确定
 @result        BBAlertView的对象
 */
- (id)initWithStyle:(BBAlertViewStyle)style
              Title:(NSString *)title 
            message:(NSString *)message
         customView:(UIView *)customView
           delegate:(id <BBAlertViewDelegate>)delegate
  cancelButtonTitle:(NSString *)cancelButtonTitle 
  otherButtonTitles:(NSString *)otherButtonTitle;

/*!
 @abstract      弹出
 */
- (void)show;


- (void)dismiss;

@end

/*********************************************************************/

@protocol BBAlertViewDelegate <NSObject>

@optional

- (void)alertView:(BBAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex; // before animation and hiding view
- (void)alertView:(BBAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;

- (void)didRotationToInterfaceOrientation:(BOOL)Landscape view:(UIView*)view alertView:(BBAlertView *)aletView;
@end
