//
//  NewsViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewModel.h"
#import "NewsModel.h"

@interface NewsViewModel : BaseViewModel

@property (nonatomic, strong) NSMutableArray<NewsModel*> *dataList;

@end
