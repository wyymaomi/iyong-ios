//
//  YYStatusView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "YYStatusView.h"



@implementation YYStatusView

- (void)initWithTitles:(NSArray *)titleArray NormalColor:(UIColor *)normalColor SelectedColor:(UIColor *)selectedColor LineColor:(UIColor *)lineColor
{
//    if (self = [super init]) {
    
        CGFloat buttonWidth = ViewWidth/titleArray.count;
        
        for (NSInteger i = 0; i < titleArray.count; i++) {
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(buttonWidth * i, 0, buttonWidth, self.frame.size.height);
            [button setTitle:titleArray[i] forState:UIControlStateNormal];
            [button setTitleColor:normalColor forState:UIControlStateNormal];
            [button setTitleColor:selectedColor forState:UIControlStateSelected];
            button.tag = i;
            button.titleLabel.font = FONT(17);
            [button addTarget:self action:@selector(buttonTouchEvent:) forControlEvents:UIControlEventTouchUpInside];
            [self.buttonArray addObject:button];
            [self addSubview:button];
            if (i == 0) {
                button.selected = YES;
            }
            
            self.currentIndex = 0;
            
            self.backgroundColor = UIColorFromRGB(0xF7F7F7);
            
//            self.bottomLineView.frame = CGRectMake(0, self.frame.size.height-2, ViewWidth, 2);
            
            // 线条
            if (lineColor) {
                self.lineView.frame = CGRectMake(0, self.frame.size.height-3, buttonWidth, 3);
                self.lineView.backgroundColor = lineColor;
            }
            
        }
        
        
//    }
    
//    return self;
}

- (void)changeCurrenIndex:(NSInteger)index;
{
    UIButton *button = self.buttonArray[index];
    [self buttonTouchEvent:button];
}

//状态切换
- (void)buttonTouchEvent:(UIButton *)button{
    
    if (button.tag == self.currentIndex) {
        return;
    }
    //代理方法
    if (self.delegate && [self.delegate respondsToSelector:@selector(statusViewSelectIndex:)]) {
        
        [self.delegate statusViewSelectIndex:button.tag];
    }
    if (!_isScroll) {
        [self changeTag:button.tag];
    }
}

#pragma 懒加载
- (NSMutableArray *)buttonArray{
    
    if (!_buttonArray) {
        _buttonArray = [NSMutableArray array];
    }
    
    return _buttonArray;
}
-(void)changeTag:(NSInteger)tag
{
    //选择当前的状态
    self.currentIndex = tag;
    UIButton *button = self.buttonArray[tag];
    button.selected = YES;
    
    //关闭上一个选择状态
    for (int i = 0; i < self.buttonArray.count; i++) {
        if (i != self.currentIndex) {
            
            UIButton *button = self.buttonArray[i];
            button.selected = NO;
        }
    }
    
    //移动横线到对应的状态
    if (self.lineView) {
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = self.lineView.frame;
            float origin = self.frame.size.width/self.buttonArray.count*tag;
            frame.origin.x = origin;
            self.lineView.frame = frame;
        }];
    }
    
    
}
//下面滑动的横线
- (UIView *)lineView{
    
    if (!_lineView) {
        
        _lineView = [[UIView alloc] init];
        [self addSubview:_lineView];
    }
    return _lineView;
}

- (UIView*)bottomLineView
{
    if (!_bottomLineView) {
        _bottomLineView = [UIView new];
        _bottomLineView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:_bottomLineView];
    }
    
    return _bottomLineView;
}


@end
