//
//  UserExperienceTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "UserExperienceTableViewCell.h"
#import "CommentBodyView.h"
#import "CommentToolBarView.h"
#import "UserExpericenViewModel.h"

@implementation UserExperienceTableViewCell

+ (instancetype)momentsTableViewCellWithTableView:(UITableView *)tableView;
{
    static NSString *reuseID = @"cell";
    UserExperienceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[UserExperienceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseID];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xDEDEDE);
        //        self.contentView.backgroundColor = UIColorFromRGB(0xEDEDED);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.bodyView];
//        [self addSubview:self.lineView];
//        [self addSubview:self.toolView];
        
        [self.bodyView.likeButton addTarget:self action:@selector(onClickLikeButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

- (void)onClickLikeButton:(id)sender
{
    if (_cellDelegate && [_cellDelegate respondsToSelector:@selector(onClickLikeButton:)]){
        [_cellDelegate onClickLikeButton:sender];
    }
}

-(UIView*)lineView
{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xEBEBEB);
    }
    return _lineView;
}

//设置cell的frame
//-(void)setFrame:(CGRect)frame{
////    frame.origin.x += circleCellMargin;
////    frame.size.width -= circleCellMargin * 2;
////    [super setFrame:frame];
//    [super setFrame:frame];
//}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (_viewModel) {
        self.bodyView.frame = _viewModel.momentsBodyFrame;
    }
}


- (void)setViewModel:(UserExpericenViewModel *)viewModel
{
    _viewModel = viewModel;
    
    // 设置子控件的frame
    self.bodyView.frame = _viewModel.momentsBodyFrame;
    self.bodyView.viewModel = viewModel;
    
}


- (CommentBodyView*)bodyView
{
    if (_bodyView == nil) {
        _bodyView = [CommentBodyView new];
        _bodyView.cell = self;
        
    }
    
    return _bodyView;
}

@end
