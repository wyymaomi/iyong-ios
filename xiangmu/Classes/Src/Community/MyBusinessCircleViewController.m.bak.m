//
//  MyBusinessCircleViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyBusinessCircleViewController.h"
#import "CommunityHeaderTableViewCell.h"
#import "MyBusinessCircleTableViewCell.h"
#import "NewOrderViewController.h"
#import "NewOrderHttpMessage.h"
#import "DispatchOrderHttpMessage.h"
#import "DateUtil.h"
#import "MsgToolBox.h"
#import "AppDelegate.h"
#import "GenerateOrderHttpMessage.h"
#import "AppConfig.h"
//#import "BusinessCircleTableViewCell.h"

@interface MyBusinessCircleViewController ()<MyBusinessCircleTableViewCellDelegate,UIActionSheetDelegate>

@property (nonatomic, strong) NSString *mobile;

@end

@implementation MyBusinessCircleViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"我的询价";
    // Do any additional setup after loading the view.
    [self.view addSubview:self.pullRefreshTableView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.pullRefreshTableView beginHeaderRefresh];
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    // Force your tableview margins (this may be a bad idea)
    if ([self.pullRefreshTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.pullRefreshTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.pullRefreshTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.pullRefreshTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PullRefresh delegate method

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    [self refreshData:nil isHeaderRefresh:YES];
    
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        [self.pullRefreshTableView endFooterRefresh];
        return;
    }
    
    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    EnquireModel *item = [self.dataList lastObject];
    if (item) {
        [self refreshData:item.createTime isHeaderRefresh:NO];
    }
    
//    CommunityPublishModel *model = [self.dataList lastObject];
//    [self refreshData:model.createTime isHeaderRefresh:NO];
    
}

-(void)refreshData:(NSString*)createTime isHeaderRefresh:(BOOL)isHeaderRefresh
{
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:createTime]];
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_MY_BUSINESS_CIRCLE_API];
    
    if (isHeaderRefresh) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseObject) {
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        EnquireResponse *responseData = [[EnquireResponse alloc] initWithDictionary:responseObject error:nil];
        
        if ([responseData.code integerValue] == STATUS_OK) {
            
            [strongSelf.dataList addObjectsFromArray:responseData.data];
            [strongSelf.pullRefreshTableView reloadData];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[responseData.code integerValue]]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

#pragma mark - 业务逻辑

#pragma mark - 完成询价

- (void)completeInquire:(id)sender
{
    
    self.nextAction = @selector(completeInquire:);
    self.object1 = sender;
    self.object2 = nil;
    
    UIButton *button = (UIButton*)sender;
    
    NSInteger index = button.tag;
    
    EnquireModel *item = self.dataList[index];
    
    BOOL isEnd = [item checkIsEnd];
    
    if (isEnd) {
        
        [MsgToolBox showAlert:@"" content:@"已结束询价"];
        
        item.end = @1;
        
        return;
    }
    
    [self completeEnquire:item popupAlertView:YES];

    
}

- (void)completeEnquire:(EnquireModel*)enquireModel popupAlertView:(BOOL)popupAlertView
{
    WeakSelf
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_END_ENQUIRY_API];
    
    NSString *param = [NSString stringWithFormat:@"enquiryId=%@", enquireModel.id];
    
    [self showHUDIndicatorViewAtCenter:@"正在结束询价，请稍候"];
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            if (popupAlertView) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"结束询价成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    enquireModel.end = @1;
                    [strongSelf.pullRefreshTableView reloadData];
                }];
            }
            else {
                enquireModel.end = @1;
                [strongSelf.pullRefreshTableView reloadData];
            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];

}

#pragma mark - 生成派单

- (void)onGenerateOrder:(BidModel*)bidModel enquireModel:(EnquireModel*)enquireModel;
{
    if ([enquireModel checkIsEnd]) {
        [MsgToolBox showAlert:@"" content:@"已完成询价，请返回"];
        return;
    }
    
    [self checkAccountBalance:bidModel enquireMode:enquireModel];
//    [self generateOrder:bidModel enquireModel:enquireModel];

}

- (void)checkAccountBalance:(BidModel*)bidModel enquireMode:(EnquireModel*)enquireModel;
{
    WeakSelf
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    [[NetworkManager sharedInstance] getFinanceInfo:^(NSDictionary *responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];

            NSInteger creditAmount = [data[@"creditAmount"] integerValue];
            NSInteger accountAmount = [data[@"accountAmount"] integerValue];
            
            
            NSInteger dstPrice = [bidModel.price integerValue];
            // 如果信用余额足够，提示“您的信用余额为XX元，派单后将扣除XX元”
            if (dstPrice <= creditAmount) {
                NSString *alertMsg = [NSString stringWithFormat:@"您的信用余额为%ld元，派单后将扣除%ld元", (long)creditAmount, (long)dstPrice];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:alertMsg delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                   
                    if (buttonIndex == 1) {
                        [strongSelf generateOrder:bidModel enquireModel:enquireModel];
                    }
                    
                }];
            }
            else if (dstPrice <= accountAmount)
            {
                NSString *alertMsg = [NSString stringWithFormat:@"您的帐户余额为%ld元，派单后将扣除%ld元", (long)accountAmount, (long)dstPrice];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:alertMsg delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 1) {
                        [strongSelf generateOrder:bidModel enquireModel:enquireModel];
                    }
                    
                }];

            }
            else {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"账户余额不足，请充值" delegate:nil cancelButtonTitle:@"充值" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        
                        [strongSelf gotoPage:@"RechargeViewController"];
                    }
                    
                }];
                
            }
        }
        
    } andFailue:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}


- (void)generateOrder:(BidModel*)bidModel enquireModel:(EnquireModel*)enquireModel;
{
    self.nextAction = @selector(generateOrder:enquireModel:);
    self.object1 = bidModel;
    self.object2 = enquireModel;
    
    GenerateOrderHttpMessage *httpMessage = [GenerateOrderHttpMessage new];
    httpMessage.enquiryId = enquireModel.id;
    httpMessage.carTime = [DateUtil formatDateTime:enquireModel.startTime dateFomatter:@"yyyy-MM-dd HH:mm"];
    httpMessage.location = enquireModel.startPlace;
    httpMessage.itinerary = enquireModel.itinerary;
    httpMessage.customerName = enquireModel.customerName;
    httpMessage.customerMobile = enquireModel.customerMobile;
    httpMessage.dstPrice = [bidModel.price stringValue];
    httpMessage.model = bidModel.model;
    httpMessage.remark = bidModel.remark;
    httpMessage.dstCompanyId = bidModel.companyId;
    httpMessage.areaCode = enquireModel.areaCode.trim;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_GENERATE_ORDER_API];
    
    WeakSelf
    
    [self showHUDIndicatorViewAtCenter:MSG_NEW_ORDER];
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:httpMessage.description success:^(id responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {

            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"生成订单成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    AppDelegate *appDelegate = APP_DELEGATE;
                    [appDelegate backToOrderList];
                    [strongSelf.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
            
            [strongSelf completeEnquire:enquireModel popupAlertView:NO];
            
        }
        else {
            
            if (code_status == NO_AVAILABEL_MOENY) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"信用余额或账户余额不足，请先充值" delegate:nil cancelButtonTitle:@"充值" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        
                        [strongSelf gotoPage:@"RechargeViewController"];
                    }
                    
                }];
                
            }
            else {
                [MsgToolBox showAlert:@"" content:[strongSelf getErrorMsg:code_status]];
            }
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
        
    }];
    
}



#pragma mark - 派单给公司



#pragma mark - 申请添加好友

- (void)onAddPartner:(NSString*)partnerId;
{
    
    self.nextAction = @selector(onAddPartner:);
    self.object1 = partnerId;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Partner_Apply_API];
    NSString *parmas = [NSString stringWithFormat:@"dstCompanyId=%@", partnerId];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        NSString *number=[responseData valueForKey:@"code"];
        NSLog(@"%@",number);
        NSLog(@"response success");
        if ([number isEqualToString:@"1000"]) {
            [weakSelf showAlertView:@"申请添加伙伴成功"];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:[number integerValue]]];
        }
        
        NSLog(@"result = %@", responseData);
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        
    }];
}

#pragma mark - 打电话

- (void)onMobile:(NSString*)mobile
{
    if (!IsStrEmpty(mobile)) {
        
        self.mobile = mobile;
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"取消"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:mobile, nil];
//        actionSheet.tag = ActionSheetTagCustomerMobile;
        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
        [actionSheet showInView:self.view];
    }
}

#pragma mark - UIActionSheet Delegate method
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (actionSheet.tag == ActionSheetTagCustomerMobile) {
        if (buttonIndex == 0) {
            NSString *num = [NSString stringWithFormat:@"tel://%@", self.mobile];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]];
        }
//    }
//    else if (actionSheet.tag == ActionSheetTagDriverMobile) {
//        if (buttonIndex == 0) {
//            NSString *mobile = self.vechileArrangeModel.mobile.trim;
//            NSString *num = [[NSString alloc] initWithFormat:@"tel://%@",mobile];
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]];
//        }
//    }
    
}


#pragma mark - 全文

- (void)onFullText:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    NSInteger index = button.tag;
    
    EnquireModel *item = self.dataList[index];
    
    BOOL isOpen = [item.isOpen boolValue];
    
    item.isOpen = [NSNumber numberWithBool:!isOpen];
    
    [self.pullRefreshTableView reloadData];
    
    
}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count+1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 175;
    }
    
    EnquireModel *item = self.dataList[indexPath.row - 1];
    return [MyBusinessCircleTableViewCell getCellHeight:item];
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 175;
//}
//
//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
//{
//    
////    TradeRecordHeaderView *headerView = [TradeRecordHeaderView customView];
////    headerView.backgroundColor = NAVBAR_BGCOLOR;
////    headerView.incomeLabel.text = IsNilOrNull(self.income) ? @"" : [self.income stringValue];
////    headerView.outLabel.text = IsNilOrNull(self.expend) ? @"" : [self.expend stringValue];
////    headerView.yearLabel.text = [DateUtil getYearStr:self.date];//[[self.date year] stringValue];
////    headerView.monthLabel.text = [DateUtil getMonthStr:self.date];//[[self.date month] stringValue];
////    //    [headerView.dateView addTap]
////    //    UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showAlert:)];
////    [headerView.dateView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelectDate)]];
////    return headerView;
//}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        CommunityHeaderTableViewCell *cell = [CommunityHeaderTableViewCell cellWithTableView:tableView];
        
        [cell initData];
        
        return cell;
        
    }
    else {
        
        static NSString *cellIdentifier = @"MyEnquireTableViewCell";
        
        MyBusinessCircleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell) {
            cell = [[MyBusinessCircleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        EnquireModel *item = self.dataList[indexPath.row - 1];
        [cell initData:item];
        cell.delegate = self;
        cell.statusButton.tag = indexPath.row - 1;
        [cell.statusButton addTarget:self action:@selector(completeInquire:) forControlEvents:UIControlEventTouchUpInside];
        cell.fullTextButton.tag = indexPath.row - 1;
        [cell.fullTextButton addTarget:self action:@selector(onFullText:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}


@end
