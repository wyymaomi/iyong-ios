//
//  VechileTypeVIew.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileTypeView.h"

@implementation VechileTypeView


-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.vechileLargeView.frame = CGRectMake(0, 0, ViewWidth, 375*Scale);
    
    self.vechileTypeImageView.frame = CGRectMake(35*Scale, 38*Scale, (ViewWidth-70*Scale), 239*Scale);
    
    self.vechileTypeLabel.top = self.vechileTypeImageView.bottom+15*Scale;
    
    self.vechilePriceLabel.top = self.vechileTypeLabel.top + 25;
    
    self.startDistanceLabel.top = self.vechilePriceLabel.top;
    
    self.vechileSmallView.frame = CGRectMake(0, self.vechileLargeView.bottom+24*Scale, ViewWidth, 176*Scale);

    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
