//
//  CommunityCategoryViewController+Guide.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityCategoryViewController.h"
#import "GuideView.h"

//static char const * const kIsReviewing = "isReviewing";
//static char const * const kIsAutoUpdate = "isAutoUpdate";
//static char const * const kUpdateUrl = "updateUrl";
//static char const * const kUpdateInfo = "updateInfo";
//static char const * const kThirdPayModels = "thirdPayModels";

static char const * const kMaskWindow = "kMaskWindow";
static char const * const kGuideViews = "kGuideViews";
static char const * const kGuideView = "kGuideView";
static char const * const kClickIndex = "kClickIndex";

@interface CommunityCategoryViewController (Guide)

- (void)initIntroduceView;

@property (nonatomic, strong) UIWindow *maskWindow;
@property (nonatomic, strong) GuideView *guideView;
@property (nonatomic, assign) NSInteger clickIndex;
//@property (nonatomic, strong) NSMutableArray *guideViews;
//@property (nonatomic, strong) GuideViewFirst *guideFirst;
//@property (nonatomic, strong) GuideViewSecond *guideSecond;
//@property (nonatomic, strong) GuideViewThird *guideThird;
//@property (nonatomic, strong) GuideViewFour *guideFour;

@end
