//
//  AddDiaoDetailViewController.h
//  xiangmu
//
//  Created by David kim on 16/5/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
typedef NS_ENUM(NSInteger,DiaoType)
{
    DiaoTypeAdd=10,
    DiaoTypeEdit,
};
@protocol DiaoDu<NSObject>
-(void)getRow:(NSInteger )row WithTitle:(NSString *)title;
@end
@interface AddDiaoDetailViewController : LFNavController
@property (nonatomic,assign)NSInteger row;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *rightname;
@property (nonatomic,assign)id<DiaoDu>delegate;
@property (nonatomic,assign)DiaoType type;
@end
