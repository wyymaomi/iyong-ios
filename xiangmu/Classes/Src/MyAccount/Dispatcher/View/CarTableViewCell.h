//
//  CarTableViewCell.h
//  xiangmu
//
//  Created by David kim on 16/4/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarTableViewCell : UITableViewCell


//车型
//@property(nonatomic,strong)UILabel *carModelLbl;
//车牌号
@property(nonatomic,strong)UILabel *carNumLbl;
//驾驶员
@property(nonatomic,strong)UILabel *driverLbl;
//电话
@property(nonatomic,strong)UILabel *telLbl;
//图片
@property(nonatomic,strong)UIImageView *carImageView;
//年检
//@property(nonatomic,strong)UILabel *yearLbl;
//保险
//@property(nonatomic,strong)UILabel *safeLbl;

@end
