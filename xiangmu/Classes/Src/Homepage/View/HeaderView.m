//
//  HeaderView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/6.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HeaderView.h"

@implementation HeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.lineView.left = 24;
    self.titleLabel.left = self.lineView.right+5;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
