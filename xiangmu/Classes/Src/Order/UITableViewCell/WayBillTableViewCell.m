//
//  PathInfoTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "WayBillTableViewCell.h"

@implementation WayBillTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView wayBillModel:(WayBillResponseModel*)wayBillModel;
{
    static NSString *ID = @"CellIdentifier";
    WayBillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if ([wayBillModel.type integerValue] == OrderWaybillTypeFirst) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
    }
    else {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] firstObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = UIColorFromRGB(0xDEDEDE);

    return cell;
}

-(void)initData:(WayBillResponseModel*)model;
{

    if (IsNilOrNull(model)) {
        return;
    }
    
    NSDate *carDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([model.endTime doubleValue] / 1000.0f)];
    
    switch ([model.type integerValue]) {

        case OrderWaybillTypeBrowsableAndEditable:
            self.editButton.hidden = YES;
            self.arriveLocationLabel.text = model.location.trim;
            self.distanceLabel.text = model.distance.trim;
            self.remarkLabel.text = model.remark.trim;
            self.endTimeLabel.text = [carDate stringWithDateFormat:@"yyyy-MM-dd HH:ss"];
            break;
        case OrderWaybillTypeBrowsableAndNoEditable:
            self.editButton.hidden = YES;
            self.arriveLocationLabel.text = model.location.trim;
            self.distanceLabel.text = model.distance.trim;
            self.remarkLabel.text = model.remark.trim;
            self.endTimeLabel.text = [carDate stringWithDateFormat:@"yyyy-MM-dd HH:ss"];
            break;
        case OrderWaybillTypeFirst:
            self.editButton.hidden = YES;
            self.editArriveLocationTextField.enabled = NO;
            self.editArriveLocationTextField.text = model.editLocation.trim;
            self.editArriveLocationView.userInteractionEnabled = YES;
            self.editArriveLocationView.backgroundColor = [UIColor whiteColor];
            [self.editArriveLocationView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onGetArriveLocation:)]];
            
        
            self.editEndTimeTextField.enabled = NO;
            self.editEndTimeTextField.text = model.editEndTime.trim;
            self.editEndTimeView.userInteractionEnabled = YES;
            self.editEndTimeView.backgroundColor = [UIColor whiteColor];
            [self.editEndTimeView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onGetEndTime:)]];
            
            self.editDistanceTextField.enabled = NO;
            self.editDistanceTextField.text = model.editDistance.trim;
            self.editDistanceView.userInteractionEnabled = YES;
            self.editDistanceView.backgroundColor = [UIColor whiteColor];
            [self.editDistanceView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onGetDistance:)]];
            
            self.editRemarkTextField.enabled = NO;
            self.editRemarkTextField.text = model.editRemark.trim;
            self.editRemarkView.userInteractionEnabled = YES;
            self.editRemarkView.backgroundColor = [UIColor whiteColor];
            [self.editRemarkView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onGetRemark:)]];
            
            
            break;
        default:
            break;
    }
    
    
}

-(void)onGetArriveLocation:(id)sender
{
    self.editArriveLocationView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
        [weakSelf.delegate doGetArriveLocation];
        weakSelf.editArriveLocationView.backgroundColor = [UIColor whiteColor];
    });
    
}

-(void)onGetEndTime:(id)sender
{
    self.editEndTimeView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
        [weakSelf.delegate doGetEndTime];
        weakSelf.editEndTimeView.backgroundColor = [UIColor whiteColor];
    });
    
}

-(void)onGetDistance:(id)sender
{
    self.editDistanceView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
        [weakSelf.delegate doGetDistance];
        weakSelf.editDistanceView.backgroundColor = [UIColor whiteColor];
    });
}

-(void)onGetRemark:(id)sender
{
    self.editRemarkView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
        [weakSelf.delegate doGetRemark];
        weakSelf.editRemarkView.backgroundColor = [UIColor whiteColor];
    });
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
