//
//  EnterpriseUserCollectionView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "EnterpriseUserCollectionView.h"
#import "FirstCollectionViewCell.h"
#import "BBAlertView.h"

@interface EnterpriseUserCollectionView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) NSArray *subviewCellList;

@end

@implementation EnterpriseUserCollectionView

- (NSArray *)subviewCellList
{
    if (IsNilOrNull(_subviewCellList)) {
        // 加载plist中的字典数组
        NSString *path = [[NSBundle mainBundle] pathForResource:HOME_CELL_PLIST ofType:nil];
        _subviewCellList = [NSArray arrayWithContentsOfFile:path];
    }
    return _subviewCellList;
}

#pragma mark - life cycle

-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    
    if (self) {
        
        [self registerClass:[FirstCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        
        self.delegate=self;
        self.dataSource=self;
        self.scrollEnabled=NO;
        
    }
    
    return self;
}

#pragma mark - UICollectionView Delegate method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.subviewCellList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FirstCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary *cellDict = self.subviewCellList[indexPath.row];
    
    cell.topImageView.image=[UIImage imageNamed:cellDict[@"icon"]];
    cell.topImageView.frame = CGRectMake((cell.frame.size.width-50*Scale)/2, 0, 50*Scale, 50*Scale);
    cell.detailLabel.text=cellDict[@"text"];
    cell.detailLabel.font = FONT(15);
    cell.detailLabel.textColor = UIColorFromRGB(0x333333);//[UIColor darkGrayColor];
    cell.detailLabel.top = cell.topImageView.bottom + 7;
    //    cell.backgroundColor=[UIColor whiteColor];
    //cell点击变色
    //    UIView *selectedBGView=[[UIView alloc]initWithFrame:cell.bounds];
    //    selectedBGView.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    //    cell.selectedBackgroundView=selectedBGView;
    
    if (indexPath.row == 1) {
        [cell showBadgeValue:self.orderCount];
    }
    else if (indexPath.row == 5) {
        [cell showBadgeValue:self.applyPartnerBadgeNumber];
    }
    else {
        [cell hideBadge];
    }
    
    
    return cell;
}
////cell点击变色
-(BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
///cell点击变色
-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *cellDict = self.subviewCellList[indexPath.row];
    NSString *viewController = cellDict[@"vc"];
    
    if ([viewController isEqualToString:@""]) {
        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该系统暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }
    else {
        if (_customDelegate && [_customDelegate respondsToSelector:@selector(onGotoPage:title:)]) {
            [_customDelegate onGotoPage:viewController title:cellDict[@"text"]];
        }
    }
    
}


@end
