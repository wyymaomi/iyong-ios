//
//  ForgetPasswordView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ForgetPasswordView.h"

@implementation ForgetPasswordView


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.logoImageView.frame = CGRectMake(15, 16, 30, 30);
    self.logoTextLabel.frame = CGRectMake(16, self.logoImageView.bottom+5*Scale, 50*Scale, 20*Scale);
    self.logoTextLabel.font = FONT(10);
    self.logoTextLabel.textColor = [UIColor whiteColor];
    self.logoTextLabel.text = @"iYONG";
    self.logoImageView.hidden = YES;
    self.logoTextLabel.hidden = YES;
    
    self.backButton.frame = CGRectMake(ViewWidth-60, self.logoImageView.top, 60, 30);
    [self.backButton setTitle:@"登录" forState:UIControlStateNormal];
    [self.backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backButton.titleLabel.font = FONT(20);
    
    self.titleLabel.frame = CGRectMake(30, 110, ViewWidth-60,22);
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = FONT(20);
    self.titleLabel.text = @"找回密码";
    
    self.accountView.frame = CGRectMake(60*Scale, self.titleLabel.bottom+65*Scale, ViewWidth-120*Scale, 32*Scale);
    self.accountIconImageView.frame = CGRectMake(0, 0, 22, 22);
    self.accountTextField.textColor = [UIColor whiteColor];
    self.accountTextField.font = FONT(16);
    self.accountTextField.placeholder = @"请输入手机号";
    self.accountTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.accountTextField.frame = CGRectMake(self.accountIconImageView.right+40*Scale, 0, self.accountView.width-self.accountIconImageView.right-40*Scale, self.accountView.height);
    self.accountLineView.frame = CGRectMake(0, self.accountView.height-0.5, self.accountView.width, 0.5);
    self.accountLineView.backgroundColor = [UIColor whiteColor];
    
    self.verifyCodeView.frame = CGRectMake(self.accountView.left, self.accountView.bottom+32*Scale, self.accountView.width, 32*Scale);
    self.verifyCodeIconImageView.frame = self.accountIconImageView.frame;
    self.verifyCodeTextField.frame = self.accountTextField.frame;
    self.verifyCodeTextField.textColor = [UIColor whiteColor];
    self.verifyCodeTextField.font = FONT(16);
    self.verifyCodeTextField.placeholder = @"请输入验证码";
    self.verifyCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.verifyCodeLineVIew.frame = self.accountLineView.frame;
    self.accountLineView.backgroundColor = [UIColor whiteColor];
    self.verifyCodeButton.frame = CGRectMake(self.verifyCodeView.width-70*Scale, 0, 70*Scale, 25*Scale);
    [self.verifyCodeButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.verifyCodeButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    [self.verifyCodeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
    self.verifyCodeButton.cornerRadius = 10;
    [self.verifyCodeButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    [self.verifyCodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    // 密码输入界面
    self.passwordView.frame = CGRectMake(self.accountView.left, self.verifyCodeView.bottom+25*Scale, self.accountView.width, self.accountView.height);
    self.passwordIconImageView.frame = self.accountIconImageView.frame;
    self.passwordTextField.frame = self.accountTextField.frame;
    self.passwordTextField.textColor = [UIColor whiteColor];
    self.passwordTextField.font = FONT(16);
    self.passwordTextField.textColor = [UIColor whiteColor];
    self.passwordTextField.placeholder = @"请输入6-16位数字字母组合密码";
    self.passwordLineView.frame = CGRectMake(0, self.passwordView.height-0.5,self.passwordView.width, 0.5);
    self.passwordLineView.backgroundColor = [UIColor whiteColor];
    
    self.repasswordView.frame = CGRectMake(self.accountView.left, self.passwordView.bottom+25*Scale, self.accountView.width, self.accountView.height);
    self.repasswordIconView.frame = self.accountIconImageView.frame;
    self.repasswordTextField.frame = self.accountTextField.frame;
    self.repasswordTextField.textColor = [UIColor whiteColor];
    self.repasswordTextField.font = FONT(16);
    self.repasswordTextField.textColor = [UIColor whiteColor];
    self.repasswordTextField.placeholder = @"请输入确认密码";
    self.repasswordLineView.frame = CGRectMake(0, self.repasswordView.height-0.5,self.repasswordView.width, 0.5);
    self.repasswordLineView.backgroundColor = [UIColor whiteColor];
    
    
    self.findPwdButton.frame = CGRectMake(36*Scale, self.repasswordView.bottom+80*Scale, ViewWidth-72*Scale, 42*Scale);
    [self.findPwdButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    [self.findPwdButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.findPwdButton.titleLabel.font = FONT(20);
    [self.findPwdButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.findPwdButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    self.findPwdButton.cornerRadius = 20;
    [self.findPwdButton setTitle:@"确认" forState:UIControlStateNormal];
    [self.findPwdButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.findPwdButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.passwordTextField.secureTextEntry = YES;
    
    self.repasswordTextField.secureTextEntry = YES;
    
}

@end
