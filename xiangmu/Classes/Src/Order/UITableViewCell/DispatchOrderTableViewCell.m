//
//  DispatchOrderTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DispatchOrderTableViewCell.h"
#import "UserManager.h"


@implementation DispatchOrderTableViewCell

+ (NSString *)cellId
{
    return @"DispatchOrderTableViewCell";
}

+ (instancetype)cellWithTableView:(UITableView *)tableView dispatchPriceModel:(DispatchPriceResponseModel*)dispatchPriceModel
{
//    if (IsNilOrNull(dispatchPriceModel)) {
//        return nil;
//    }
    
    static NSString *ID =  @"DispatchOrderTableViewCell";//[self cellId];
    DispatchOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
//    if (!cell) {
        if ( [dispatchPriceModel.rollbackStatus integerValue] == OrderRollbackStatusInit && [dispatchPriceModel.type integerValue] == OrderDispatchTypeFirst) {
            cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
        }
        else {
            cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] firstObject];
        }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0xDEDEDE);
//    }
    return cell;
}

+ (NSUInteger)getHeight:(DispatchPriceResponseModel*)dispatchPriceModel;
{
    if (IsNilOrNull(dispatchPriceModel)) {
        return 134;
    }
    
    switch ([dispatchPriceModel.type integerValue]) {
        case OrderDispatchTypeFirst:
            return 132;
        case OrderDispatchTypeBrowsableAndChangable:
        case OrderDispatchTypeBrowsableAndNoChangable:
            return 134;
        default:
            break;
    }
    
    return 0;
    
}

-(void)doGetDispatchPrice:(id)sender
{
    self.editDstPriceView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
        [weakSelf.delegate onGetDispatchPrice];
        weakSelf.editDstPriceView.backgroundColor = [UIColor whiteColor];
    });
    
}

-(void)doGetDispatchObject:(id)sender
{
    self.editDispatchObjectView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
        [weakSelf.delegate doGetDispatchCompany];
        weakSelf.editDispatchObjectView.backgroundColor = [UIColor whiteColor];
    });
}

-(void)initData:(DispatchPriceResponseModel*)dispatchPriceModel;
{

    if (IsNilOrNull(dispatchPriceModel)) {
        return;
    }
    
    self.model = dispatchPriceModel;

//    if ([dispatchPriceModel.rollbackStatus integerValue] == OrderRollbackStatusInit) {
//        [self initUnrollbackData:dispatchPriceModel];
//    }
    switch ([dispatchPriceModel.rollbackStatus integerValue]) {
        case OrderRollbackStatusInit:
            [self initUnrollbackData:dispatchPriceModel];
            break;
        case OrderRollbackStatusStart:
//            break;
        case OrderRollbackNotAgree:
//            break;
        case OrderRollbackHaveAgree:
            [self initWaitingAgreeRollbackData:dispatchPriceModel];
            break;
        default:
            break;
    }
    
    
    
}

//

// 已经发起改派
- (void)initWaitingAgreeRollbackData:(DispatchPriceResponseModel*)dispatchPriceModel
{
    self.srcPriceLabel.text = [dispatchPriceModel.srcPrice integerValue] > 0 ? [NSString stringWithFormat:@"¥%@", dispatchPriceModel.srcPrice.trim] : @"";
    self.dstPriceLabel.text = [dispatchPriceModel.dstPrice integerValue] > 0 ? [NSString stringWithFormat:@"¥%@", dispatchPriceModel.dstPrice.trim] : @"";
    self.dispatchCompanyLabel.text = dispatchPriceModel.companyInfo;
    
    // 区分是否自派外派
    if ([[UserManager sharedInstance] isSelfDispatch:dispatchPriceModel.dstCompanyId]) {
        self.dispatchPriceView.hidden = YES;
        self.dispatchCompanyLabel.text = @"自派";
    }
    else {
        self.dispatchPriceView.hidden = NO;
    }
    
    NSInteger rollbackStatus = [dispatchPriceModel.rollbackStatus integerValue];
    if (rollbackStatus == OrderRollbackStatusStart) {
        self.editButton.hidden = YES;
    }
    else {
        self.editButton.hidden = NO;
        [self.editButton setTitle:@"同意改派" forState:UIControlStateNormal];
        [self.editButton addTarget:self action:@selector(onAgreeRollback) forControlEvents:UIControlEventTouchUpInside];
    }


}

// 未改派时
- (void)initUnrollbackData:(DispatchPriceResponseModel*)dispatchPriceModel
{
    switch ([self.model.type integerValue]) {
        case OrderDispatchTypeBrowsableAndNoChangable:
            self.srcPriceLabel.text = [self.model.srcPrice integerValue] > 0 ? [NSString stringWithFormat:@"¥%@", self.model.srcPrice.trim] : @"";
            self.dstPriceLabel.text = [self.model.dstPrice integerValue] > 0 ? [NSString stringWithFormat:@"¥%@", self.model.dstPrice.trim] : @"";

            self.dispatchCompanyLabel.text = self.model.companyInfo;
            // 区分是否自派外派
            if ([[UserManager sharedInstance] isSelfDispatch:self.model.dstCompanyId]) {
                self.dispatchPriceView.hidden = YES;
                self.dispatchCompanyLabel.text = @"自派";
            }
            else {
                self.dispatchPriceView.hidden = NO;
            }
            
            // 改派订单按钮隐藏：不可改派
//            [self.editButton setTitle:@"改派订单" forState:UIControlStateNormal];
//            [self.editButton addTarget:self action:@selector(dispatchRollback:) forControlEvents:UIControlEventTouchUpInside];
            self.editButton.hidden = YES;
            break;
            
        case OrderDispatchTypeBrowsableAndChangable:
            self.srcPriceLabel.text = [dispatchPriceModel.srcPrice integerValue] > 0 ? [NSString stringWithFormat:@"¥%@", dispatchPriceModel.srcPrice.trim] : @"";
            self.dstPriceLabel.text = [dispatchPriceModel.dstPrice integerValue] > 0 ? [NSString stringWithFormat:@"¥%@", dispatchPriceModel.dstPrice.trim] : @"";
//            self.editButton.hidden = NO;
            self.dispatchCompanyLabel.text = dispatchPriceModel.companyInfo;
            // 区分是否自派外派
            if ([[UserManager sharedInstance] isSelfDispatch:dispatchPriceModel.dstCompanyId]) {
                self.dispatchPriceView.hidden = YES;
                self.dispatchCompanyLabel.text = @"自派";
            }
            else {
                self.dispatchPriceView.hidden = NO;
            }
            
            // 改派订单按钮
            [self.editButton setTitle:@"改派订单" forState:UIControlStateNormal];
            [self.editButton addTarget:self action:@selector(dispatchRollback:) forControlEvents:UIControlEventTouchUpInside];
            self.editButton.hidden = NO;
            
            break;
        case OrderDispatchTypeFirst:
            // 区分是否自派外派
            if ([[UserManager sharedInstance] isSelfDispatch:dispatchPriceModel.dstCompanyId]) {
                self.editDstPriceView.hidden = YES;
            }
            else {
                self.editDstPriceView.hidden = NO;
            }
            
            self.srcPriceTextField.text = [dispatchPriceModel.srcPrice integerValue] > 0 ? [NSString stringWithFormat:@"¥%@", dispatchPriceModel.srcPrice.trim] : @"";
            self.srcPriceTextField.enabled = NO;
            
            if (!self.editDstPriceView.hidden) {
                self.editDstPriceView.userInteractionEnabled = YES;
                self.editDispatchPriceTextField.enabled = NO;
                self.editDispatchPriceTextField.text = dispatchPriceModel.editDstPrice.trim;
                [self.editDstPriceView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doGetDispatchPrice:)]];
            }
            
            self.editDispatchObjectView.userInteractionEnabled = YES;
            self.editDispatchObjectTextField.enabled = NO;
            self.editDispatchObjectTextField.text = dispatchPriceModel.companyInfo.trim;
            [self.editDispatchObjectView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doGetDispatchObject:)]];
            
            
            
        default:
            break;
    }
}

- (void)dispatchRollback:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onDispatchRollbackBtnClick)]) {
        [self.delegate onDispatchRollbackBtnClick];
    }
}

- (void)onAgreeRollback
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onAgreeRollback)]) {
        [self.delegate onAgreeRollback];
    }
}

//- (void)awakeFromNib {
//    [super awakeFromNib];
//    // Initialization code
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

@end
