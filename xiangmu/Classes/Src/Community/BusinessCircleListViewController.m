//
//  BusinessCircleListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BusinessCircleListViewController.h"

#import "CommunityHeaderTableViewCell.h"
#import "BusinessCircleTableViewCell.h"
#import "InquireViewController.h"
#import "BusinessCircleModel.h"
#import "Enquire.h"
#import "BBAlertView.h"
#import "BidHttpMessage.h"
#import "MsgToolBox.h"
#import "CommunityHelper.h"
//#import "YYFPSLabel.h"
#import "GuideView.h"
#import "AppConfig.h"

//#import "BussinessSearchHttpMessage.h"

@interface BusinessCircleListViewController ()<BusinessCircleSearchDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

//@property (nonatomic, strong) BusinessCircleModelList *itemList;

@property (nonatomic, strong) BBAlertView *priceInputView;

@property (nonatomic, strong) NSTimer *timer; // timer

@property (nonatomic, strong) BidHttpMessage *bitHttpMessage;

@property (nonatomic, strong) UIButton *searchButton;

//@property (nonatomic, strong) YYFPSLabel *fpsLabel;

@property (nonatomic, strong) BussinessSearchHttpMessage *httpMessage;

@property (nonatomic, strong) UIWindow *maskWindow;
@property (nonatomic, assign) NSInteger clickIndex;
@property (nonatomic, strong) GuideView *guideView;

@end

@implementation BusinessCircleListViewController

- (BussinessSearchHttpMessage*)httpMessage
{
    if (!_httpMessage) {
        _httpMessage = [[BussinessSearchHttpMessage alloc] init];
        _httpMessage.type = [AppConfig currentConfig].serviceType;
    }
    return _httpMessage;
}

- (BidHttpMessage*)bitHttpMessage;
{
    if (_bitHttpMessage == nil) {
        _bitHttpMessage = [[BidHttpMessage alloc] init];
    }
    return _bitHttpMessage;
}

-(BBAlertView*)priceInputView
{
    if (!_priceInputView) {
        _priceInputView = [[BBAlertView alloc] initWithStyle: BBAlertViewPublishPrice Title:nil message:nil customView:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    }
    return _priceInputView;
}

- (UIButton*)searchButton
{
    if (!_searchButton) {
        _searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchButton setBackgroundImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
        [_searchButton addTarget:self action:@selector(gotoSearch) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchButton;
}

//-(YYFPSLabel*)fpsLabel
//{
//    if (!_fpsLabel) {
//        
//        _fpsLabel = [YYFPSLabel new];
//        [_fpsLabel sizeToFit];
//        _fpsLabel.bottom = self.view.height - 12 - 44;
//        _fpsLabel.left = 12;
//        _fpsLabel.alpha = 0;
//        
//    }
//    return _fpsLabel;
//}


#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"询价" style:UIBarButtonItemStyleDone target:self action:@selector(publish)];
    [self.view addSubview:self.pullRefreshTableView];
    [self.view addSubview:self.searchButton];
    
//    self.pullRefreshTableView.emptyDataSetSource = self;
//    self.pullRefreshTableView.emptyDataSetDelegate = self;
    
//    [self registKVO];
    
//    self.pullRefreshTableView.empty
    //#if DEBUG
    //    [self.view addSubview:self.fpsLabel];
    //#endif
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.searchButton.frame = CGRectMake(ViewWidth-60, self.view.frame.size.height-100, 40, 40);
    //#if DEBUG
    //    self.fpsLabel.bottom = self.view.frame.size.height-20;
    //#endif
    //    self.fpsLabel.frame = CGRectMake(12, self.view.frame.size.height-100, <#CGFloat width#>, <#CGFloat height#>)
    
    // Force your tableview margins (this may be a bad idea)
    if ([self.pullRefreshTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.pullRefreshTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.pullRefreshTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.pullRefreshTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initIntroduceView];
    
    [self enterFG];
    
    if (self.mode == BusinessListModeSearch) {
        return;
    }
    
    [self.pullRefreshTableView beginHeaderRefresh];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self enterBG];
    
}

- (void)dealloc
{
//    [self unRegistKVO];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark KVO



#pragma mark - 引导蒙板

- (GuideView*)guideView
{
    if (!_guideView) {
        _guideView = [[GuideView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _guideView.imageView.image = [UIImage imageNamed:@"guide_page4"];
        [_guideView.okButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _guideView;
}

- (void)buttonTapped:(id)sender
{
    DLog(@"buttonTapped:");
    
    if (self.clickIndex == 0) {
        self.guideView.imageView.image = [UIImage imageNamed:@"guide_page5"];
        self.clickIndex++;
    }
    else if (self.clickIndex == 1) {
        [self.guideView removeAllSubviews];
        [self.guideView removeFromSuperview];
        [[UserDefaults sharedInstance] setObject:@0 forKey:@"IsShwoIntroForBussinessCircle"];
    }
}


- (void)initIntroduceView
{
    
    NSNumber *value = [[UserDefaults sharedInstance] objectForKey:@"IsShwoIntroForBussinessCircle"];
    
    if (value == nil) {
        
        _maskWindow = [UIApplication sharedApplication].keyWindow;
        if (!_maskWindow)
            _maskWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
        [[[_maskWindow subviews] objectAtIndex:0] addSubview:self.guideView];
        
    }
    
}

#pragma mark - PullRefresh delegate method

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    
    self.httpMessage.time = nil;
    self.httpMessage.startTime = nil;
    self.httpMessage.endTime = nil;
    self.httpMessage.startPlace = nil;
    self.httpMessage.areaCode = [AppConfig currentConfig].defaultCityCode;
    self.httpMessage.type = [AppConfig currentConfig].serviceType;
    
    [self refreshData:nil isHeaderRefresh:YES];
    
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        [self.pullRefreshTableView endFooterRefresh];
        return;
    }
    
    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    EnquireModel *item = [self.dataList lastObject];
    if (item) {
        self.httpMessage.time = item.createTime;
        [self refreshData:item.createTime isHeaderRefresh:NO];
    }
    
}

-(void)refreshData:(NSString*)createTime isHeaderRefresh:(BOOL)isHeaderRefresh
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_ENQUIRY_LIST_API];
    
    if (isHeaderRefresh) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
//    self.loading = YES;
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:self.httpMessage.description success:^(NSDictionary* responseObject) {
        //        StrongSelf
//        weakSelf.loading = NO;
        weakSelf.loadStatus = LoadStatusFinish;
        [weakSelf.pullRefreshTableView endHeaderRefresh];
        [weakSelf.pullRefreshTableView endFooterRefresh];
        [weakSelf resetAction];
        weakSelf.pullRefreshTableView.footerHidden = NO;
        
        EnquireResponse *responseData = [[EnquireResponse alloc] initWithDictionary:responseObject error:nil];
        if ([responseData.code integerValue] == STATUS_OK) {
            
            [weakSelf.dataList addObjectsFromArray:responseData.data];
            [weakSelf.pullRefreshTableView reloadData];
            
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:[responseData.code integerValue]]];
            //            [strongSelf stopTimer];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        //        StrongSelf
//        weakSelf.loadFailed = YES;
        weakSelf.loadStatus = LoadStatusNetworkError;
        [weakSelf.pullRefreshTableView endHeaderRefresh];
        [weakSelf.pullRefreshTableView endFooterRefresh];
        [weakSelf resetAction];
//        [weakSelf showAlertView:errorDesc];
        
    }];
}

#pragma mark - 报价前先判断当前询价单是否已报过价
- (void)checkHasBid:(EnquireModel*)enquireModel
{
    self.nextAction = @selector(checkHasBid:);
    self.object1 = enquireModel;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_HAS_BID_API];
    NSString *params = [NSString stringWithFormat:@"enquiryId=%@", enquireModel.id];
    
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            BOOL result = [responseData[@"data"] boolValue];
            if (result) {
                [MsgToolBox showAlert:@"" content:@"一个订单不能重复报价"];
            }
            else {
                [strongSelf showBidDialog:enquireModel];
            }
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
}



#pragma mark - 出价

- (void)onBid:(id)sender
{
    
    NSString *errorMsg;
    
    UIButton *button = (UIButton*)sender;
    
    NSInteger index = button.tag;
    
    EnquireModel *item = self.dataList[index];
    
    BOOL isEnd = [item checkIsEnd];
    
    if (isEnd) {
        [MsgToolBox showAlert:@"" content:@"已结束询价"];
        return;
    }
    
    BOOL isSelfBid = [[UserManager sharedInstance] isSelfDispatch:item.companyId];
    if (isSelfBid) {
        [MsgToolBox showAlert:@"" content:@"不能对自己发布的询价进行报价！"];
        return;
    }
    
    // 判断是否是否有报价权限
    // 实名认证，车辆认证，企业认证
    
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] getUserInfo:^(id responseData) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        // 是否有报价权限
        NSString *errorMsg;
        if ([CommunityHelper isValidBusinessCircleBider:&errorMsg]) {
            if([CommunityHelper isCertifcationed:&errorMsg]) {
                // 同一订单是否已经报过价
                [weakSelf checkHasBid:item];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"报价企业需要进行"message:@"实名认证-车辆认证-企业认证" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 1) {
                        [weakSelf gotoPage:@"UserInfoViewController"];
                    }
                    
                }];
            }
        }
        else {
            [MsgToolBox showAlert:@"提示" content:errorMsg];
        }
        
        
    } andFailue:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        [weakSelf showAlertView:errorDesc];
        
    }];
    
    
    
    
    
}

- (void)showBidDialog:(EnquireModel*)item
{
    self.priceInputView = nil;
    
    [self.priceInputView show];
    
    WeakSelf
    self.priceInputView.textInputView.completeHandle = ^(NSString *vechileModel, NSString *vechileQuantity, NSString *price, NSString *remark) {
        StrongSelf
        strongSelf.bitHttpMessage.enquiryId = item.id;
        strongSelf.bitHttpMessage.model = vechileModel;
        strongSelf.bitHttpMessage.quantity = vechileQuantity;
        strongSelf.bitHttpMessage.price = price;
        strongSelf.bitHttpMessage.remark = remark;
        [strongSelf doBid];
        
    };
    
}

- (void)doBid
{
    NSString *errorMsg;
    if (![CommunityHelper isValidBidContent:self.bitHttpMessage errorMsg:&errorMsg]) {
        [MsgToolBox showAlert:@"提示" content:errorMsg];
        return;
    }
    
//    [strongSelf.priceInputView dismiss];
    
    [self.priceInputView dismiss];
    
    self.nextAction = @selector(doBid);
    self.object1 = nil;
    self.object2 = nil;
    
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_BID_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:self.bitHttpMessage.description success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"出价成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    
                }
                
            }];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
    
    
}

#pragma mark - 询价

- (void)publish
{
    // 是否有报价权限
    NSString *errorMsg;
    BOOL isValidBider = [CommunityHelper isValidPublisher:&errorMsg];
    if (!isValidBider) {
        [MsgToolBox showAlert:@"" content:errorMsg];
        return;
    }
    
    InquireViewController *viewController = [[InquireViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (void)onFullText:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    NSInteger index = button.tag;
    
    EnquireModel *item = self.dataList[index];
    
    BOOL isOpen = [item.isOpen boolValue];
    
    item.isOpen = [NSNumber numberWithBool:!isOpen];
    
    [self.pullRefreshTableView reloadData];
    
    
}

#pragma mark - 进入搜索界面

- (void)gotoSearch
{
    BusinessCircleSearchViewController *viewController = [[BusinessCircleSearchViewController alloc] init];
    viewController.searchDelegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void)onSearchResult:(BussinessSearchHttpMessage*)httpMessage;
{
    self.httpMessage = httpMessage;
    
    self.mode = BusinessListModeSearch;
    
    [self refreshData:nil isHeaderRefresh:YES];
    
}

#pragma mark - 收到通知

- (void)onMessageReceived:(id)sender
{
    
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.dataList.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EnquireModel *enquireModel = self.dataList[indexPath.row];
    return [BusinessCircleTableViewCell getCellHeight:enquireModel];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellID = [NSString stringWithFormat:@"CellIdentifier%ld%ld", (long)indexPath.section, (long)indexPath.row];
    
    BusinessCircleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        //         cell = [[[NSBundle mainBundle] loadNibNamed:@"BusinessCircleTableViewCell" owner:self options:nil] firstObject];
        cell = [[BusinessCircleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    EnquireModel *item = self.dataList[indexPath.row];
    [cell initData:item];
    //    cell.avatarImageView
//    [cell.avatarImageView downloadImage:item.companyLogoUrl placeholderImage:[UIImage imageNamed:@"icon_avatar"] success:^(id responseData) {
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//    }];
//    [cell.avatarImageView downloadImageFromAliyunOSS:item.companyLogoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//        
//    }];
    
    cell.publishPriceButton.tag = indexPath.row;
    [cell.publishPriceButton addTarget:self action:@selector(onBid:) forControlEvents:UIControlEventTouchUpInside];
    cell.fullTextButton.tag = indexPath.row;
    [cell.fullTextButton addTarget:self action:@selector(onFullText:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

//#pragma mark - DZNEmptyDataSetSource Methods
//
//- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
//{
//    NSString *text = nil;
//    UIFont *font = nil;
//    UIColor *textColor = nil;
//    
//    NSMutableDictionary *attributes = [NSMutableDictionary new];
//
//    if (self.loadStatus == LoadStatusNetworkError) {
//        text = @"网络请求失败";
//    }
//    else {
//        text = @"暂无数据";
//    }
//    
//    font = [UIFont systemFontOfSize:18];
//    textColor = [UIColor blackColor];//[UIColor colorWithHex:@"d9dce1"];
//    
//    if (font) [attributes setObject:font forKey:NSFontAttributeName];
//    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
//    
//    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
//}
//
//- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
//{
//    NSString *text = nil;
//    UIFont *font = nil;
//    UIColor *textColor = nil;
//    
//    NSMutableDictionary *attributes = [NSMutableDictionary new];
//    
//    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
//    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
//    paragraph.alignment = NSTextAlignmentCenter;
//    
//    if (self.loadStatus == LoadStatusNetworkError) {
//        text = @"请检查您的网络\n重新加载吧";
//    }
//    else {
//        text = @"";
//    }
//    
//    font = FONT(13);
//    textColor = UIColorFromRGB(0x4D4C4C);
//
//    
//    if (!text) {
//        return nil;
//    }
//    
//    if (font) [attributes setObject:font forKey:NSFontAttributeName];
//    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
//    if (paragraph) [attributes setObject:paragraph forKey:NSParagraphStyleAttributeName];
//    
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
//    
//    return attributedString;
//}
//
//- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
//{
//    if (self.loadStatus == LoadStatusNetworkError) {
//        return [UIImage imageNamed:@"img_network_error"];
//    }
//    else {
//        return [UIImage imageNamed:@"img_claim"];
//    }
//}
//
//- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
//{
//    NSString *text = nil;
//    UIFont *font = nil;
//    UIColor *textColor = nil;
//    
//    text = @"重新加载";
//    font = FONT(16);
//    textColor = UIColorFromRGB(0x575656);
//    
//    if (!text) {
//        return nil;
//    }
//    
//    NSMutableDictionary *attributes = [NSMutableDictionary new];
//    if (font) [attributes setObject:font forKey:NSFontAttributeName];
//    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
//    
//    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
//}
//
//#pragma mark - DZNEmptyDataSetDelegate Methods
//
//- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
//{
//    if (self.loadStatus == LoadStatusInit ||
//        self.loadStatus == LoadStatusLoading) {
//        return NO;
//    }
//    return YES;
//}
//
//- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
//{
//    return YES;
//}
//
//- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
//{
//    return YES;
//}
//
//- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view
//{
//    self.loadStatus = LoadStatusLoading;
//    [self.pullRefreshTableView beginHeaderRefresh];
//}
//
//- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
//{
//    self.loadStatus = LoadStatusInit;
//    [self.pullRefreshTableView beginHeaderRefresh];
//}


#pragma mark - method area

/**
 *  进入后台 停止倒计时
 */
-(void)enterBG {
    DLog(@"应用进入后台啦");
    [self stopTimer];
}

/**
 *  返回前台时重启倒计时
 */
-(void)enterFG {
    DLog(@"应用将要进入到前台");
    [self startCountDown];
}

/**
 *  开始倒计时
 */
-(void)startCountDown {
    
    //    _countDown = tick; //< 重置计时
    
    _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

/**
 *  停止倒计时
 */
- (void)stopTimer {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

/**
 *  倒计时逻辑
 */
-(void)timerFired:(NSTimer *)timer
{
    
    [self.pullRefreshTableView reloadData];
    
    //    switch (_countDown) {
    //        case 1:
    //            NSLog(@"重新发送");
    //
    //            [self stopTimer];
    //            break;
    //        default:
    //            _countDown -=1;
    //            NSLog(@"倒计时中：%d",_countDown);
    //            break;
    //    }
    
}

#pragma  mark - UIScrollView Delegate method

//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//#if DEBUG
//    if (self.fpsLabel.alpha == 0) {
//        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//            self.fpsLabel.alpha = 1;
//        } completion:NULL];
//    }
//#endif
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//#if DEBUG
//    if (!decelerate) {
//        if (self.fpsLabel.alpha != 0) {
//            [UIView animateWithDuration:1 delay:2 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//                self.fpsLabel.alpha = 0;
//            } completion:NULL];
//        }
//    }
//#endif
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//#if DEBUG
//    if (self.fpsLabel.alpha != 0) {
//        [UIView animateWithDuration:1 delay:2 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//            self.fpsLabel.alpha = 0;
//        } completion:NULL];
//    }
//#endif
//}
//
//- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
//#if DEBUG
//    if (self.fpsLabel.alpha == 0) {
//        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//            self.fpsLabel.alpha = 1;
//        } completion:^(BOOL finished) {
//        }];
//    }
//#endif
//}


@end
