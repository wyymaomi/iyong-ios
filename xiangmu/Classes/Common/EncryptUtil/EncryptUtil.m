//
//  EncryptUtil.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "EncryptUtil.h"
#import "NSString+Encrypt.h"
#import "BBRSACryptor.h"
#import "GTMBase64.h"
#import "RSASigner.h"
#import "RSADataSigner.h"
#import "CRSA.h"
#import "Base64.h"

@implementation EncryptUtil

/**
 *  随机生成DES密钥
 */
+(NSString*)generateDesKey;
{
    NSString *kRandomAlphabet = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity:kRandomLength];
    for (int i = 0; i < kRandomLength; i++) {
        [randomString appendFormat: @"%C", [kRandomAlphabet characterAtIndex:arc4random_uniform((u_int32_t)[kRandomAlphabet length])]];
    }
    
    return randomString;
}

/**
 *
 */
//
+(NSString*)generateEncryptRequest:(NSString*)bodyParams
                             token:(NSString*)token
                           session:(NSString*)session
{
    return [NSString stringWithFormat:@"a=%@&b=%@", [bodyParams desEncrypt:token], session];
}

#pragma mark - 用RSA加密密码
+(NSString*)getRSAEncryptPassword:(NSString*)password;
{
    if (IsStrEmpty(password)) {
        return nil;
    }
    
    BBRSACryptor *cryptor = [[BBRSACryptor alloc] init];
    [cryptor importServerRSAPublicKeyBase64:SERVER_RSA_PUBLIC_KEY];
    NSData *data = [cryptor encryptWithServerPublicKeyUsingPadding:RSA_PKCS1_PADDING
                                                   plainData:[password dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *rsa_password = [GTMBase64 stringByEncodingData:data];
    DLog(@"rsa_password = %@", rsa_password);
    
    return rsa_password;
    
}

#pragma mark - 用RSA加密登录参数
+(NSString*)getRSALoginParams:(NSString*)loginParams;
{
    if (IsStrEmpty(loginParams)) {
        return nil;
    }
    
    CRSA *cc = [CRSA shareInstance];
    // 写入公钥
    [cc writePukWithKey:SERVER_RSA_PUBLIC_KEY];
    NSString *en1 = [cc encryptByRsaWithCutData:loginParams keyType:KeyTypePublic];
    
    return en1;
    
//    [cc writePukWithKey:PubKey];
//    [cc writePrkWithKey:PriKey];
    
//    BBRSACryptor *cryptor = [[BBRSACryptor alloc] init];
//    [cryptor importServerRSAPublicKeyBase64:SERVER_RSA_PUBLIC_KEY];
//    NSData *data = [cryptor encryptWithServerPublicKeyUsingPadding:RSA_PKCS1_PADDING
//                                                         plainData:[loginParams dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    DLog(@"dataString = %@", dataString);
////    DLog(@"dataString = %@", [data ])
//    NSString *rsa_body_base64 = [GTMBase64 stringByEncodingData:data];
//    
//    return rsa_body_base64;
}

#pragma mark - 自动登录加密
+(NSDictionary*)encryptAutoLoginRequest:(NSString *)sha_body
                             desKey:(NSString *)desKey
                           rsa_body:(NSString*)rsa_body
{
    if (sha_body == nil || sha_body.length == 0) {
        return nil;
    }
    
    if (desKey == nil || desKey.length == 0) {
        return nil;
    }
    
    if (rsa_body == nil || rsa_body.length == 0) {
        return nil;
    }
    
    DLog(@"body = %@", sha_body);
    DLog(@"desKey = %@", desKey);
    DLog(@"rsa_body = %@", rsa_body);
    
    BBRSACryptor *cryptor = [[BBRSACryptor alloc] init];
    // 如果证书不存在，则生成RSA证书
    [cryptor importClientRSAPublicKeyBase64:CLIENT_RSA_PUBLIC_KEY];
    [cryptor importClientRSAPrivateKeyBase64:CLIENT_RSA_PRIVATE_KEY];
    [cryptor importServerRSAPublicKeyBase64:SERVER_RSA_PUBLIC_KEY];
    
    // 1.用SHA1算法计算T的散列值设为H
    NSString *sha1Str = sha_body;//[body sha1];
//    DLog(@"=============================");
//    DLog(@"1.用SHA1算法计算T的散列值设为H");
//    DLog(@"1------shaStr = %@", sha1Str);
    
    // 4.用KeyServerPublic加密KeyLocalDes得到K_RSA
    //服务器公钥加密
    NSData *k_rsa_data = [cryptor encryptWithServerPublicKeyUsingPadding:RSA_PKCS1_PADDING
                                                               plainData:[desKey dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *k_rsa = [GTMBase64 stringByEncodingData:k_rsa_data];
//    DLog(@"=============================");
//    DLog(@"4------k_rsa = %@", k_rsa);
    
    // 2.用KeyClientPrivate加密H得到H_RSA
    //客户端公钥加密
    NSData *data = [cryptor encryptWithPrivateKeyUsingPadding:RSA_PKCS1_PADDING
                                                    plainData:[sha1Str dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *h_rsa = [GTMBase64 stringByEncodingData:data];
//    DLog(@"=============================");
//    DLog(@"2-----h_rsa=%@", h_rsa);
    
    // 3.用KeyLocalDes加密H_RSA得到H_RSA_DES,加密KeyClientPublic得到K_DES,加密T得到T_DES
    NSString *clientPublicKey = [cryptor base64EncodedPublicKey];
    
    NSString *h_rsa_des = [h_rsa desEncrypt:desKey];
    NSString *k_des = [clientPublicKey desEncrypt:desKey];
    NSString *t_rsa = rsa_body;
//    DLog(@"=============================");
//    DLog(@"3------h_rsa_des = %@", h_rsa_des);
//    DLog(@"=============================");
//    DLog(@"4------k_des = %@", k_des);
//    DLog(@"=============================");
//    DLog(@"5------t_rsa = %@", t_rsa);
//    DLog(@"\n");
    
    
    // 5.传递内容为a=K_RSA&b=H_RSA_DES&c=K_DES&d=T_DES
    return @{@"a": k_rsa,
             @"b": h_rsa_des,
             @"c": k_des,
             @"d": t_rsa};
}

#pragma mark - 登录加密
+(NSDictionary*)encryptLoginRequest:(NSString *)body
                             desKey:(NSString*)desKey
{
    if (body == nil || body.length == 0) {
        return nil;
    }
    
    if (desKey == nil || desKey.length == 0) {
        return nil;
    }
    
    NSLog(@"body = %@", body);
    NSLog(@"desKey = %@", desKey);
    
    BBRSACryptor *cryptor = [[BBRSACryptor alloc] init];
    // 如果证书不存在，则生成RSA证书
//    [cryptor generateClientRSAKeyPairWithKeySize:1024];
    [cryptor importClientRSAPublicKeyBase64:CLIENT_RSA_PUBLIC_KEY];
    [cryptor importClientRSAPrivateKeyBase64:CLIENT_RSA_PRIVATE_KEY];
    [cryptor importServerRSAPublicKeyBase64:SERVER_RSA_PUBLIC_KEY];
    
    // 1.用SHA1算法计算T的散列值设为H
    NSString *sha1Str = [body sha1];
//    DLog(@"=============================");
//    DLog(@"1.用SHA1算法计算T的散列值设为H");
//    DLog(@"1------shaStr = %@", sha1Str);
    
    // 4.用KeyServerPublic加密KeyLocalDes得到K_RSA
    //服务器公钥加密
    NSData *k_rsa_data = [cryptor encryptWithServerPublicKeyUsingPadding:RSA_PKCS1_PADDING
                                                               plainData:[desKey dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *k_rsa = [GTMBase64 stringByEncodingData:k_rsa_data];
//    DLog(@"=============================");
//    DLog(@"4------k_rsa = %@", k_rsa);
    
    // 2.用KeyClientPrivate加密H得到H_RSA
    //客户端公钥加密
//    NSString *clientPrivateKey = [cryptor base64EncodedPrivateKey];
    NSData *data = [cryptor encryptWithPrivateKeyUsingPadding:RSA_PKCS1_PADDING
                                                    plainData:[sha1Str dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *h_rsa = [GTMBase64 stringByEncodingData:data];
//    DLog(@"=============================");
//    DLog(@"2-----h_rsa=%@", h_rsa);
    
    // 3.用KeyLocalDes加密H_RSA得到H_RSA_DES,加密KeyClientPublic得到K_DES,加密T得到T_DES
    NSString *clientPublicKey = [cryptor base64EncodedPublicKey];
    //    NSString *clientPrivateKey = [cryptor base64EncodedPrivateKey];
    
    NSString *h_rsa_des = [h_rsa desEncrypt:desKey];
    NSString *k_des = [clientPublicKey desEncrypt:desKey];
    NSString *t_rsa = [self getRSALoginParams:body];
//    DLog(@"=============================");
//    DLog(@"3------h_rsa_des = %@", h_rsa_des);
//    DLog(@"=============================");
//    DLog(@"4------k_des = %@", k_des);
//    DLog(@"=============================");
//    DLog(@"5------t_rsa = %@", t_rsa);
//    DLog(@"=============================");
    
    
    // 5.传递内容为a=K_RSA&b=H_RSA_DES&c=K_DES&d=T_DES
    return @{@"a": k_rsa,
             @"b": h_rsa_des,
             @"c": k_des,
             @"d": t_rsa};
}

#pragma mark - 加密登录

#pragma mark - 注册时加密拆分
+(NSDictionary*)encryptRegisterBody:(NSString*)param1
                         param2:(NSString*)param2
                         desKey:(NSString*)desKey
{
    
    if (param1 == nil || param1.length == 0) {
        return nil;
    }
    
    if (param2 == nil || param2.length == 0) {
        return nil;
    }
    
    if (desKey == nil || desKey.length == 0) {
        return nil;
    }
    
    NSLog(@"param1 = %@", param1);
    NSLog(@"param2 = %@", param2);
    NSLog(@"desKey = %@", desKey);
    
    BBRSACryptor *cryptor = [[BBRSACryptor alloc] init];
    // 如果证书不存在，则生成RSA证书
    [cryptor importClientRSAPublicKeyBase64:CLIENT_RSA_PUBLIC_KEY];
    [cryptor importClientRSAPrivateKeyBase64:CLIENT_RSA_PRIVATE_KEY];
    [cryptor importServerRSAPublicKeyBase64:SERVER_RSA_PUBLIC_KEY];
    
    // 1.用SHA1算法计算T的散列值设为H
    NSString *body = [NSString stringWithFormat:@"%@&%@", param1, param2];
    NSString *sha1Str = [body sha1];
//    DLog(@"=============================");
//    DLog(@"1.用SHA1算法计算T的散列值设为H");
//    DLog(@"1------shaStr = %@", sha1Str);
    
    // 4.用KeyServerPublic加密KeyLocalDes得到K_RSA
    //服务器公钥加密
    NSData *k_rsa_data = [cryptor encryptWithServerPublicKeyUsingPadding:RSA_PKCS1_PADDING
                                                               plainData:[desKey dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *k_rsa = [GTMBase64 stringByEncodingData:k_rsa_data];
//    DLog(@"=============================");
//    DLog(@"4------k_rsa = %@", k_rsa);
    
    // 2.用KeyClientPrivate加密H得到H_RSA
    //客户端公钥加密
    NSData *data = [cryptor encryptWithPrivateKeyUsingPadding:RSA_PKCS1_PADDING
                                                    plainData:[sha1Str dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *h_rsa = [GTMBase64 stringByEncodingData:data];
//    DLog(@"=============================");
//    DLog(@"2-----h_rsa=%@", h_rsa);
    
    // 3.用KeyLocalDes加密H_RSA得到H_RSA_DES,加密KeyClientPublic得到K_DES,加密T得到T_DES
    NSString *clientPublicKey = [cryptor base64EncodedPublicKey];
    
    NSString *h_rsa_des = [h_rsa desEncrypt:desKey];
    NSString *k_des = [clientPublicKey desEncrypt:desKey];
    NSString *t_rsa = [self getRSALoginParams:param1];
    NSString *t_des = [param2 desEncrypt:desKey];
//    DLog(@"=============================");
//    DLog(@"3------h_rsa_des = %@", h_rsa_des);
//    DLog(@"=============================");
//    DLog(@"4------k_des = %@", k_des);
//    DLog(@"=============================");
//    DLog(@"5------t_rsa = %@", t_rsa);
//    DLog(@"=============================");
    
    
    // 5.传递内容为a=K_RSA&b=H_RSA_DES&c=K_DES&d=T_RSA&e=T_DES
    return @{@"a": k_rsa,
             @"b": h_rsa_des,
             @"c": k_des,
             @"d": t_rsa,
             @"e": t_des};
}


//接收解密方法
//(1)用KeyServerPrivate解密a得到KeyLocalDes
//(2)用KeyLocalDes解密b,c,d分别得到H_RSA,KeyClientPublic,T
//(3)用KeyClientPublic解密H_RSA得到H
//(4)用SHA1算法计算T的散列值，与H进行比较，相等则表示未篡改。否则抛弃。
//(5)登录校验，通过则随机生成sessionId,会话保存KeyLocalDes,token,User信息。
//(6)生成返回信息R。验证成功则包含code,sessionId,token。否则只返回code。
//(7)用KeyLocalDes加密R，返回。
+(void)decryptBodyData:(NSString*)data
{
    
    BBRSACryptor *cryptor = [[BBRSACryptor alloc] init];
    [cryptor importServerRSAPublicKeyBase64:SERVER_RSA_PUBLIC_KEY];
    [cryptor importServerRSAPrivateKeyBase64:SERVER_RSA_PRIVATE_KEY];
    [cryptor importClientRSAPrivateKeyBase64:CLIENT_RSA_PRIVATE_KEY];
    [cryptor importClientRSAPublicKeyBase64:CLIENT_RSA_PUBLIC_KEY];
    
    
    

    __block NSString *desKey, *h_rsa, *keyClientPublic, *plainText;
    
    NSArray *array = [data componentsSeparatedByString:@"&"];
    
    [array enumerateObjectsUsingBlock:^(NSString *param, NSUInteger idx, BOOL * _Nonnull stop) {
        
//        if (idx == 0) {
//            
//            NSString *value = [param substringFromIndex:2];
//            
//            NSLog(@"value = %@", value);
//            
//            NSString *cipher = value;
//            NSLog(@"cipher = %@", cipher);
//            
//            NSData *cipherDesKeyData = [GTMBase64 decodeString:cipher];
//            NSData *plainDesKeyData = [cryptor decryptWithServerPrivateKeyUsingPadding:RSA_PKCS1_PADDING
//                                                                                    cipherData:cipherDesKeyData];
//            desKey = [[NSString alloc] initWithData:plainDesKeyData encoding:NSUTF8StringEncoding];
//            NSLog(@"desKey = %@",desKey);
//            
//        }
        if (idx == 1) {
            
            NSString *cipher = [param substringFromIndex:2];
            
            NSLog(@"--------------------");
            
            NSLog(@"cipher = %@", cipher);
            
            desKey = @"12345678";
            
            h_rsa = [cipher decryptUseDES:desKey];
            
            NSData *h_rsa_data = [h_rsa dataUsingEncoding:NSUTF8StringEncoding];
            
            NSLog(@"h_rsa = %@", h_rsa);
            
            NSData *plain_data = [cryptor decryptWithPublicKeyUsingPadding:RSA_PKCS1_PADDING
                                                                cipherData:h_rsa_data];
            
            NSLog(@"plainText = %@", [[NSString alloc] initWithData:plain_data encoding:NSUTF8StringEncoding]);
            
            
            NSLog(@"---------------------");
            
        }
//        else if (idx == 2) {
//            
//            NSString *cipher = [param substringFromIndex:2];
//            
//            NSLog(@"cipher = %@", cipher);
//            
//            keyClientPublic = [cipher decryptUseDES:desKey];
//            
//            NSLog(@"keyClientPublic = %@", keyClientPublic);
//            
//        }
        
//        else if (idx == 3) {
//            
//            NSString *cipher = [param substringFromIndex:2];
//            
//            NSLog(@"cipher = %@", cipher);
//            
//            plainText = [cipher decryptUseDES:desKey];
//            
//            NSLog(@"plainText = %@", plainText);
//            
//        }
        
        
    }];
    
    
    
//    return nil;
}


@end
