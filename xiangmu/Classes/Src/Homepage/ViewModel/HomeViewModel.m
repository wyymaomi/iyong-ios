//
//  HomeViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomeViewModel.h"

@implementation HomeViewModel

-(instancetype)init
{
    self = [super init];
    
    if (self) {
        
        _weddingList = [NSMutableArray new];
        _travelList = [NSMutableArray new];
        _commercialList = [NSMutableArray new];
        _airportList = [NSMutableArray new];
        _busServiceList = [NSMutableArray new];
        _charteredList = [NSMutableArray new];
        _goodsList = [NSMutableArray new];
        _truckList = [NSMutableArray new];
        
    }
    
    return self;
}

//@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *weddingList;
//@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *travelList;
//@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *commercialList;
//@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *airportList;
//@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *busServiceList; // 企业班车
//@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *charteredList; // 包车服务
//@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *goodsList; // 货运用车
//@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *truckList; // 拖车服务

-(NSMutableArray*)weddingList
{
    if (!_weddingList) {
        _weddingList = [NSMutableArray new];
    }
    return _weddingList;
}

-(NSMutableArray*)travelList
{
    if (!_travelList) {
        _travelList = [NSMutableArray new];
    }
    return _travelList;
}

-(NSMutableArray*)commercialList
{
    if (!_commercialList) {
        _commercialList = [NSMutableArray new];
    }
    return _commercialList;
}

-(NSMutableArray*)airportList
{
    if (!_airportList) {
        _airportList = [NSMutableArray new];
    }
    return _airportList;
}

-(NSMutableArray*)busServiceList
{
    if (!_busServiceList) {
        _busServiceList = [NSMutableArray new];
    }
    return _busServiceList;
}

-(NSMutableArray*)gpsCityList
{
    if (!_gpsCityList) {
        _gpsCityList = [NSMutableArray new];
    }
    return _gpsCityList;
}

-(NSMutableArray*)hangzhouList
{
    if (!_hangzhouList) {
        _hangzhouList = [NSMutableArray new];
    }
    
    return _hangzhouList;
}

- (NSMutableArray*)shenzhenList
{
    if (!_shenzhenList) {
        _shenzhenList = [NSMutableArray new];
    }
    
    return _shenzhenList;
}

- (NSMutableArray*)shanghaiList
{
    if (!_shanghaiList) {
        _shanghaiList = [NSMutableArray new];
    }
    return _shanghaiList;
}

-(NSMutableArray*)beijingList
{
    if (!_beijingList) {
        _beijingList = [NSMutableArray new];
    }
    return _beijingList;
}

- (NSMutableArray *)experienceList
{
    if (!_experienceList) {
        _experienceList = [NSMutableArray new];
    }
    return _experienceList;
}

- (NSMutableArray *)recommendList
{
    if (!_recommendList) {
        _recommendList = [NSMutableArray new];
    }
    return _recommendList;
}

- (NSMutableArray*)providerList
{
    if (!_providerList) {
        _providerList = [NSMutableArray new];
    }
    return _providerList;
}

- (NSMutableArray *)advList
{
    if (!_advList) {
        _advList = [NSMutableArray new];
    }
    return _advList;
}

@end
