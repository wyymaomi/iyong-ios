//
//  OrderViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderViewModel.h"

@implementation OrderViewModel

//+(BOOL)validateTwicePwd:(NSString*)pwd1 pwd2:(NSString*)pwd2 error:(NSString**)errorMsg;
//{
//    if (![pwd1 isEqualToString:pwd2]) {
//        *errorMsg = @"两次输入密码不同，请重新输入";
//        return NO;
//    }
//    return YES;
//}

+(BOOL)validateNewOrder:(NewOrderHttpMessage*)orderHttpMessage error:(NSString**)errorMsg;
{
    
    if (IsStrEmpty(orderHttpMessage.carTime.trim)) {
//        [self showAlertView:ALERT_MSG_CARTIME_NULL];
        *errorMsg = ALERT_MSG_CARTIME_NULL;
        return NO;
    }
    if (IsStrEmpty(orderHttpMessage.location.trim)) {
//        [self showAlertView:ALERT_MSG_START_LOCATION_NULL];
//        return;
        
        *errorMsg = ALERT_MSG_START_LOCATION_NULL;
        return NO;
    }
    if (IsStrEmpty(orderHttpMessage.itinerary.trim)) {
//        [self showAlertView:ALERT_MSG_ROUGH_ITINERARY_NULL];
//        return;
        
        *errorMsg = ALERT_MSG_ROUGH_ITINERARY_NULL;
        return NO;
    }
    if (IsStrEmpty(orderHttpMessage.customerName.trim)) {
//        [self showAlertView:ALERT_MSG_CUSTOMER_NULL];
//        return;
        
        *errorMsg = ALERT_MSG_CUSTOMER_NULL;
        return NO;
    }
    
    return YES;
}

@end
