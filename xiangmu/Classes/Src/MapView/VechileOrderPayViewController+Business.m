//
//  VechileOrderPayViewController+Business.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileOrderPayViewController+Business.h"
#import "AlipayOrder.h"
#import "SDKManager.h"
#import "SDKRegisterService.h"
#import "SDKPayService.h"
#import "WXApiObject.h"
#import "WXApi.h"
#import "BaseViewController+PaypwdCheck.h"
#import "CarTravelListViewController.h"

@implementation VechileOrderPayViewController (Business)

- (void)doAdPurchase
{
    
    if ([[VechileOrderManager sharedInstance] isPendingPayOvertime]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"支付超时，请重新下单" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        WeakSelf
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                //                [weakSelf pressedBackButton];
            }
        }];
        
        return;
    }
    
    NSString *orderId = [VechileOrderManager sharedInstance].vechileOrderHttpMessage.orderId;
    self.payHttpMessage.orderId = orderId;
    CGFloat amount = [VechileOrderManager sharedInstance].vechileOrderHttpMessage.carPrice;
    self.payHttpMessage.amount = [NSNumber numberWithFloat:amount];
    
    if (IsStrEmpty(orderId)) {
//        if ([self.payHttpMessage.payMode integerValue] == AdPayTypeMyAccount ||
//            [self.payHttpMessage.payMode integerValue] == AdPayTypePromotionAccount) {
//            [self checkHasPayPwd];
//        }
//        else {
//            [self submitPurchaseReqeust:self.payHttpMessage];
//        }
        [MsgToolBox showToast:@"没有订单号"];
        return;
    }
    else {
        // 账户余额或者推广账户余额支付
        if ([self.payHttpMessage.payMode integerValue] == AdPayTypeMyAccount ||
            [self.payHttpMessage.payMode integerValue] == AdPayTypePromotionAccount) {
            // 判断是否有足够的账户余额 使用账户余额支付时需用户实名认证
            if ([self.myAccountMoney floatValue] >= amount) {
                BOOL isRealnameCertificationed = [[UserManager sharedInstance].userModel isRealnameCertified];
                if (isRealnameCertificationed) {
                    [self checkHasPayPwd];
                }
                else {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请先实名认证" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"账户余额不足，请选择其他支付方式" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
            
        }
        else {
            [self submitPurchaseReqeust:self.payHttpMessage];
//            [self submitPay:self.adOrderId];
        }
        
    }
}

#pragma mark - Pay Delegate method
-(void)onPaySuccess;
{
//    if (IsStrEmpty(self.adOrderId)) {
//        [self submitPurchaseReqeust:self.httpMessage];
//    }
//    else {
//        [self submitPay:self.adOrderId];
//    }
    [self submitPurchaseReqeust:self.payHttpMessage];
    
}



-(void)getFinanceInfo
{
    self.nextAction = @selector(getFinanceInfo);
    self.object1 = nil;
    self.object2 = nil;
    
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_FINANCE_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            strongSelf.myAccountMoney = data[@"accountAmount"];
//            strongSelf.promotionAccountMoney = data[@"extensionAmount"];
            [strongSelf.groupTableView reloadData];
            //            strongSelf.myAccountTableView.creditLine = data[@"creditLine"];
            //            strongSelf.myAccountTableView.creditAmount = data[@"creditAmount"];
            //            strongSelf.myAccountTableView.accountAmount = data[@"accountAmount"];
        }
        else {
            //            strongSelf.myAccountTableView.creditLine = nil;
            //            strongSelf.myAccountTableView.creditAmount = nil;
            //            strongSelf.myAccountTableView.accountAmount = nil;
        }
        //        [strongSelf.myAccountTableView reloadData];
        
    } andFailure:^(NSString *errorDesc) {
        [weakSelf resetAction];
        DLog(@"response failure");
    }];
    
}

//adMainIds（广告ID数组，可以以英文逗号隔开）
//price（总价）
//payMode（支付方式：1余额支付；2推广余额支付；3支付宝支付；4微信支付）

- (void)submitPurchaseReqeust:(TravelorderPayHttpMessage*)httpMessage
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, TRAVEL_ORDER_PAY_API];
    NSString *params = httpMessage.description;
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary * responseData) {
        StrongSelf
        
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            
            if (httpMessage.payMode == [NSNumber numberWithInteger:AdPayTypeAlipay]) {
                NSString *data = responseData[@"data"];
                if (!IsStrEmpty(data)) {
                    [strongSelf doAlipayPay:data];
                }
                
            }
            else if (httpMessage.payMode == [NSNumber numberWithInteger:AdPayTypeWechat]) {
                
                NSDictionary *data = responseData[@"data"];
                if (data != nil) {
                    [strongSelf bizPay:data];
                }
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"支付成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        [weakSelf gotoCarTravelListPage];
//                        [self gotoPage:@"CarTravelListViewController"];
                    }
                    
                }];
            }
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

//
//选中商品调用支付宝极简支付
//
- (void)doAlipayPay:(NSString*)encryptOrderString
{
    
    //    NSDictionary *dict = @{kTradeNo:@"1234567890",//[self generateTradeNO],
    //                           kRechargeAmount:self.transAmountTextField.text.trim,
    //                           kPayTime:[[NSDate date] stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"]};
    //    id<PayModelAdapterProtocol> alipay = [[AlipayOrder alloc] init];
    
    [[SDKManager getPayService:SDKPlatformAlipay] payOrder:/*[alipay generateOrderString:dict]*/encryptOrderString callBack:^(NSString *signString, NSError *error) {
        DLog(@"signString = %@", signString);
        //        NSInteger resultStatus = [signString integerValue];
        //        if (resultStatus == 6001) {
        //            [MsgToolBox showAlert:@"" content:@"用户取消"];
        ////                        [self showTipMessage:@"用户取消"];
        //            //            [self ]
        //        }
        //        else if (resultStatus == 6002){
        //            [MsgToolBox showAlert:@"" content:@"网络连接错误"];
        ////                        [self showTipMessage:@"网络连接错误"];
        //        }
        //        else if (resultStatus == 4000){
        //            [MsgToolBox showAlert:@"" content:@"订单支付失败"];
        //            //            [self showTipMessage:@"订单支付失败"];
        //        }
        //        else if (resultStatus == 8000){
        //            [MsgToolBox showAlert:@"" content:@"正在处理中"];
        //            //            [self showTipMessage:@"正在处理中"];
        //        }
        //        else if (resultStatus == 9000){
        //            [MsgToolBox showAlert:@"" content:@"订单支付成功"];
        //            //            [self showTipMessage:@"订单支付成功"];
        //        }
        
    }];
    
}

#pragma mark - 微信支付

- (void)bizPay:(NSDictionary*)dict
{
    //    NSString *res = [WXApiRequestHandler jumpToBizPay];
    //    if( ![@"" isEqual:res] ){
    //        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"支付失败" message:res delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //
    //        [alter show];
    ////        [alter release];
    //    }
    
    NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
    
    //调起微信支付
    PayReq *req             = [[PayReq alloc] init];
    req.partnerId           = dict[@"partnerid"];
    req.prepayId            = [dict objectForKey:@"prepayid"];
    req.nonceStr            = [dict objectForKey:@"noncestr"];
    req.timeStamp           = stamp.intValue;
    req.package             = [dict objectForKey:@"package"];
    req.sign                = [dict objectForKey:@"sign"];
    [WXApi sendReq:req];
    
}

-(void)onThirdPayNotification:(id)sender
{
//    NSString *params = [NSString stringWithFormat:@"orderId=%@", s]
    NSString *params = [NSString stringWithFormat:@"orderId=%@", [VechileOrderManager sharedInstance].vechileOrderHttpMessage.orderId];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(THIRD_PAY_SUCCESS_API) requestMethod:POST params:params success:^(NSDictionary * responseData) {
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
//            [MsgToolBox showToast:@"支付成功！"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"支付成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
               
                if (buttonIndex == 0) {
//                    CarTravelListViewController *viewController = [[CarTravelListViewController alloc] init];
////                    viewController.listStatus = 2;
//                    viewController.source_from = 1;
//                    [weakSelf.navigationController pushViewController:viewController animated:YES];
//                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                    [weakSelf gotoCarTravelListPage];
                }
                
            }];
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        [MsgToolBox showToast:errorDesc];
        
    }];
}

-(void)onThirdPayFailureNotification:(id)sender
{
    
}

-(void)gotoCarTravelListPage
{
    CarTravelListViewController *viewController = [[CarTravelListViewController alloc] init];
    viewController.source_from = 1;
    [self.navigationController pushViewController:viewController animated:YES];
}



@end
