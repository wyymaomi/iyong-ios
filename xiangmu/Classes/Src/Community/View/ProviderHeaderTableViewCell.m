//
//  ProviderHeaderTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderHeaderTableViewCell.h"
#import "SDCycleScrollView.h"

@implementation ProviderHeaderTableViewCell

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    static NSString *CellIdentifier = @"ProviderHeaderTableViewCell";
    ProviderHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[ProviderHeaderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _bannerView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 160)];
        _bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        _bannerView.currentPageDotColor =  [UIColor whiteColor];
        [cell.contentView addSubview:_bannerView];
        
    }
    return (ProviderHeaderTableViewCell*)cell;
    
}


//+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
//{
//    static NSString *ID = @"ProviderDetailTableViewCell";
//    ProviderHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
//    if (!cell) {
//        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    return cell;
//}

+(CGFloat)getCellHeight
{
    return ViewWidth;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
