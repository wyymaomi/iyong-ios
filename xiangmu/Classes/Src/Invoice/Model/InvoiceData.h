//
//  InvoiceData.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InvoiceData : NSObject

@property (nonatomic, assign) CGFloat amount;

@property (nonatomic, assign) NSInteger times;

@end
