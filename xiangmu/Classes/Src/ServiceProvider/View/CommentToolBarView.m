//
//  CommentToolBarView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CommentToolBarView.h"

@implementation CommentToolBarView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.likeView];
        [self addSubview:self.favoriteView];
//        [self addSubview:self.telephoneView];
        [self addSubview:self.shareView];
        
    }
    return self;
}

//- (void)setViewModel:(ServiceProviderViewModel *)viewModel
//{
//    _viewModel = viewModel;
//
//    self.likeView.frame = _viewModel.toolLikeFrame;
//    self.favoriteView.frame = _viewModel.toolFavoriteFrame;
//    self.telephoneView.frame = _viewModel.toolTelFrame;
//}

#pragma mark - getter and setter

- (UIView*)shareView
{
    if (!_shareView) {
        _shareView = [UIView new];
        
        _shareView.backgroundColor = [UIColor whiteColor];
        
        [_shareView addSubview:self.shareImageView];
        
        [_shareView addSubview:self.shareLabel];
    }
    
    return _shareView;
}

- (UIImageView*)shareImageView;
{
    if (!_shareImageView) {
        _shareImageView = [UIImageView new];
        _shareImageView.image = [UIImage imageNamed:@"icon_share"];
    }
    return _shareImageView;
}

- (UILabel*)shareLabel
{
    if (!_shareLabel) {
        _shareLabel = [UILabel new];
        _shareLabel.text = @"分享";
        _shareLabel.textAlignment = UITextAlignmentCenter;
        _shareLabel.font = FONT(11);
    }
    
    return _shareLabel;
}



- (UIView*)telephoneView
{
    if (_telephoneView == nil) {
        _telephoneView = [UIView new];
        _telephoneView.backgroundColor = [UIColor whiteColor];
        [_telephoneView addSubview:self.telephoneButton];
    }
    return _telephoneView;
}

-(UIButton*)telephoneButton
{
    if (_telephoneButton == nil) {
        _telephoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_telephoneButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x01C216)] forState:UIControlStateNormal];
        [_telephoneButton setTitle:@"通话" forState:UIControlStateNormal];
        [_telephoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _telephoneButton.titleLabel.font = FONT(14);
        [_telephoneButton setImage:[UIImage imageNamed:@"icon_partner_dial"] forState:UIControlStateNormal];
        _telephoneButton.cornerRadius = 5;
    }
    return _telephoneButton;
}

// 收藏

- (UIView*)favoriteView
{
    if (_favoriteView == nil) {
        _favoriteView = [UIView new];
        _favoriteView.backgroundColor = [UIColor whiteColor];
        
        [_favoriteView addSubview:self.favoriteImageView];
        [_favoriteView addSubview:self.favoriteLabel];
        
    }
    return _favoriteView;
}

- (UIImageView *)favoriteImageView
{
    if (_favoriteImageView == nil) {
        _favoriteImageView = [UIImageView new];
        _favoriteImageView.image = [UIImage imageNamed:@"icon_discuss"];
    }
    return _favoriteImageView;
}

- (UILabel*)favoriteLabel
{
    if (_favoriteLabel == nil) {
        _favoriteLabel = [[UILabel alloc] init];
        
        _favoriteLabel.font = FONT(10);
        _favoriteLabel.textColor = [UIColor blackColor];
        _favoriteLabel.textAlignment = UITextAlignmentCenter;
        _favoriteLabel.text = @"评论";
        
    }
    
    return _favoriteLabel;
}

// 点赞

- (UIView*)likeView
{
    if (_likeView == nil) {
        _likeView = [[UIView alloc] init];
        
        _likeView.backgroundColor = [UIColor whiteColor];
        
        [_likeView addSubview:self.likeImageView];
        [_likeView addSubview:self.likeNumsLabel];
        
    }
    return _likeView;
}

- (UIImageView*)likeImageView
{
    if (_likeImageView == nil) {
        _likeImageView = [[UIImageView alloc] init];
        _likeImageView.image = [UIImage imageNamed:@"icon_heart"];
    }
    
    return _likeImageView;
}

- (UILabel*)likeNumsLabel
{
    if (_likeNumsLabel == nil) {
        _likeNumsLabel = [[UILabel alloc] init];
        //        _likeNumsLabel.textColor = [UIColor blueColor];
        _likeNumsLabel.font = FONT(13);
        _likeNumsLabel.textAlignment = UITextAlignmentCenter;
    }
    
    return _likeNumsLabel;
}


- (void)setViewModel:(UserExpericenViewModel *)viewModel
{
    _viewModel = viewModel;
    
//    self.likeView.frame = viewModel.toolLikeFrame;
    self.likeImageView.frame = CGRectMake((self.likeView.frame.size.width-18)/2, 17, 18, 18);
    self.likeNumsLabel.frame = CGRectMake(0,0,self.likeView.frame.size.width,21);
    self.likeNumsLabel.text = _viewModel.serviceModel.thumbsQuantity;
    self.likeNumsLabel.tag = 2000;
    self.likeImageView.tag = 2001;
    self.likeImageView.image = [_viewModel.serviceModel.thumbsUp boolValue] ? [UIImage imageNamed:@"icon_heart"] : [UIImage imageNamed:@"icon_heart2"];
//    NSDate *lastLikeDateTime = [NSDate dateWithTimeIntervalSince1970:[viewModel.serviceModel.thumbsTime doubleValue]/1000];
//    NSTimeInterval interval = [lastLikeDateTime timeIntervalSinceDate:[NSDate date]];
//    if (interval <  -24 * 60 * 60) {
//        self.likeImageView.image = [UIImage imageNamed:@"icon_heart2"];
//    }
//    else {
//        self.likeImageView.image = [UIImage imageNamed:@"icon_heart"];
//    }
    
//    self.favoriteView.frame = viewModel.toolFavoriteFrame;
    self.favoriteImageView.frame = CGRectMake((self.favoriteView.frame.size.width-18)/2, 3, 18, 18);
    self.favoriteLabel.frame = CGRectMake(0, 17, self.favoriteView.frame.size.width, 21);
//    NSString *favoriteImageName = [viewModel.serviceModel.collection boolValue]?@"icon_star":@"icon_gray_star";
//    self.favoriteImageView.image = [UIImage imageNamed:favoriteImageName];
    self.favoriteImageView.tag = 1000;
    
//    self.telephoneView.frame = viewModel.toolTelFrame;
//    NSInteger telButtonWidth = 55;
//    NSInteger telButtonHeight = 20;
//    self.telephoneButton.frame = CGRectMake((self.telephoneView.frame.size.width-telButtonWidth)/2, (self.telephoneView.frame.size.height-telButtonHeight)/2, telButtonWidth, telButtonHeight);
    
//    self.shareView.frame = viewModel.toolTelFrame;
//    self.shareView.tag = 2000;
    self.shareImageView.frame = CGRectMake((self.shareView.frame.size.width-18)/2, 3, 18, 18);
    self.shareLabel.frame = CGRectMake(0, 17, self.shareView.frame.size.width, 21);
    
    
}
@end
