//
//  OrderViewController.h
//  xiangmu
//
//  Created by David kim on 16/4/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "AddCarViewController.h"
#import "EditCarViewController.h"

@interface OrderViewController : LFNavController<Shuaxin,Cheliang>
@property (strong,nonatomic)UITableView *carTableView;
@end
