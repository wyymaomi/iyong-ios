//
//  ProtocolRegisterViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ProtocolRegisterViewController.h"

@interface ProtocolRegisterViewController ()

@end

@implementation ProtocolRegisterViewController

-(id)init
{
    if (self = [super init]) {
        
        self.title = @"iYONG平台用户使用协议";
        
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    bPopupLoginView = NO;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR]
                                                  forBarMetrics:UIBarMetricsDefault];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    bPopupLoginView = YES;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"RegisterProtocol" ofType:@"txt"];
    NSString *shellTitleText = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    self.textView.text = shellTitleText;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
