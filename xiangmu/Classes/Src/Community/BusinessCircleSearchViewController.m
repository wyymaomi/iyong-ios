//
//  BusinessCircleSearchViewController.m
//  xiangmu
//
//  Created by David kim on 16/9/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BusinessCircleSearchViewController.h"
#import "CommunityCateogryTableViewCell.h"
#import "CommunityHelper.h"
#import "InputHelperViewController.h"
#import "BussinessSearchHttpMessage.h"
#import "BussinessSearchTableView.h"
#import "LocationSearchViewController.h"
#import "BussinessSearchHttpMessage.h"
#import "GYZChooseCityController.h"

@interface BusinessCircleSearchViewController ()<BussinessSearchTableViewDelegate,GYZChooseCityDelegate,/*GYZCityGroupCellDelegate,*/ GYZChooseCityDelegate>

@property (nonatomic, strong) BussinessSearchTableView *searchTableView;
@property (nonatomic, strong) NSArray *vechileServiceList; // 用车类型


@end

@implementation BusinessCircleSearchViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"业务圈查询";
    [self.view addSubview:self.searchTableView];
    self.searchTableView.searchDelegate = self;
}

- (void)onSettingStartLocation;
{
    LocationSearchViewController *vc = [[LocationSearchViewController alloc] init];
    vc.title = @"出发地点";
    WeakSelf
    vc.selectLocationHandle = ^(NSString *text, NSString *cityCode) {
        weakSelf.searchTableView.httpMessage.startPlace = text;
        weakSelf.searchTableView.httpMessage.areaCode = cityCode;
        [weakSelf.searchTableView reloadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)search;
{
    if (self.searchDelegate && [self.searchDelegate respondsToSelector:@selector(onSearchResult:)]) {
        [self.searchDelegate onSearchResult:self.searchTableView.httpMessage];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 城市选择

- (void)selectCity
{
    GYZChooseCityController *cityPickerVC = [[GYZChooseCityController alloc] init];
    [cityPickerVC setDelegate:self];
    
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:cityPickerVC] animated:YES completion:^{
        //
    }];
    
}

- (void) cityPickerController:(GYZChooseCityController *)chooseCityController
                didSelectCity:(GYZCity *)city;
{
    if (IsStrEmpty(city.cityID)) {
        CommunityHelper *communityHelper = [CommunityHelper new];
        self.searchTableView.httpMessage.areaCode = [communityHelper getCityIdFromCityName:city.cityName];
    }
    else {
        self.searchTableView.httpMessage.areaCode = city.cityID;
    }
    
    [self.searchTableView reloadData];
    [chooseCityController dismissViewControllerAnimated:YES completion:nil];
}

- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController
{
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BussinessSearchTableView*)searchTableView
{
    if (!_searchTableView) {
        
        _searchTableView = [[BussinessSearchTableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        
    }
    
    return _searchTableView;
    
}



@end
