//
//  CameraPhotoService.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CameraPhotoServiceDelegate <NSObject>

//- (void)onGetImageData:(NSData*)imageData;
- (void)onGetImageData:(NSData*)iamgeData tag:(NSInteger)tag;


@end

typedef void(^CameraPhotoCompleteBlock)(NSInteger tag);
typedef void(^GetImageDataBlock)(NSData *imageData, NSInteger tag);

typedef NS_ENUM(NSUInteger, CameraPhotoType)
{
    CameraPhotoLarge=0,     // 车辆头像、营业支招、驾驶证、行驶证、身份证正反面 等
    CameraPhotoTypeMedium=1,
    CameraPhotoTypeSmall=2// 个人头像
};

@interface CameraPhotoService : NSObject


@property (nonatomic, weak) id<CameraPhotoServiceDelegate> delegate;
@property (nonatomic, assign) CameraPhotoType type;

- (id)initWithViewController:(id)navigationController type:(CameraPhotoType)type;
- (id)initWithViewController:(id)navigationController
                        type:(CameraPhotoType)type
               completeBlock:(CameraPhotoCompleteBlock)completeBlock
              onGetImageData:(GetImageDataBlock)onGetImageData;

- (void)show;

- (void)show:(NSUInteger)tag;

-(void)takePhoto;

-(void)LocalPhoto;


@end
