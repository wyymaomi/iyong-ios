//
//  ForgetPaypwdViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewModel.h"

@interface ForgetPaypwdViewModel : BaseViewModel

@property (nonatomic, strong) NSString *verificationCode; // 短信验证码
@property (nonatomic, strong) NSString *payPassword; // 新交易密码
@property (nonatomic, strong) NSString *rePayPassword; // 二次输入交易密码

-(NSString*)encryptMobileText:(NSString*)mobile;

- (BOOL)validateHttpDTO:(NSString**)errorMsg;// 判断输入的选项

@end
