//
//  RedEnvelopView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RedEnvelopView.h"

@interface RedEnvelopView ()

@property (nonatomic, strong) UIImageView *unOpenBackgroundImg; //未开启红包背景
@property (nonatomic, strong) UIImageView *unOpenTopImg;        //未开启红包开口
@property (nonatomic, strong) UIImageView *upOpenMoneyImg;      //未开启红包封口
@property (nonatomic, strong) UIButton *clickBtn;               //领取红包按钮
@property (nonatomic, strong) UIImageView *openBackgroundImg;   //开启红包背景

@property (strong, nonatomic) CADisplayLink *displayLink;

@property (nonatomic, strong) UILabel *detailsLabel;            //红包内容
@property (nonatomic, strong) UILabel *moneyLabel;              //红包金额
@property (nonatomic, strong) UILabel *moneyTextLabel;          //“代金券”
//@property (nonatomic, strong) UILabel *permissionLabel;         //使用权限
@property (nonatomic, strong) UIView *openTopView;              //打开红包的遮罩层

@end

@implementation RedEnvelopView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
