//
//  StartOrderViewController+Business.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "StartOrderViewController.h"
#import "TravelOrderModel.h"

@interface StartOrderViewController (Business)

-(void)getOrderDetail;

-(void)stopDriverTimer;

-(void)onReceiveCancelOrderMessage:(id)sender;

- (void)onOrderReceived:(NSNotification *)notification;

//-(void)orderReceived:(NSDictionary*)infoDic;

@end
