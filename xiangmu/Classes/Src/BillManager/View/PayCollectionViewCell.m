//
//  PayCollectionViewCell.m
//  xiangmu
//
//  Created by David kim on 16/3/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PayCollectionViewCell.h"

@implementation PayCollectionViewCell
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        for (int i=0; i<4; i++) {
            NSArray *nameArray=@[@"时间",@"机场",@"大道",@"客户"];
            NSArray *titleArray=@[@"2016-02-20  09:30",@"虹桥机场",@"龙东大道1号",@"韩先生 18121300418"];
            UIImageView *leftImageView=[[UIImageView alloc]initWithFrame:CGRectMake(20, 5+25*i, 20, 20)];
            leftImageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",nameArray[i]]];
            [self addSubview:leftImageView];
            
            self.dateLabel=[[UILabel alloc]initWithFrame:CGRectMake(45, 5+25*i, 120, 20)];
            self.dateLabel.text=titleArray[i];
            self.dateLabel.font=[UIFont systemFontOfSize:12];
            [self addSubview:self.dateLabel];
        }
        
        for (int j=0; j<2; j++) {
            NSArray *dayArray=@[@"¥ 1000",@"2016/3/11"];
            self.priceLabel=[[UILabel alloc]initWithFrame:CGRectMake(ViewWidth-115, 30+30*j, 100, 20)];
            self.priceLabel.text=dayArray[j];
            self.priceLabel.textAlignment=NSTextAlignmentCenter;
            self.priceLabel.textColor=[UIColor colorWithRed:254.0f/255.0f green:140.0f/255.0f blue:49.0f/255.0f alpha:1.0f];
            self.priceLabel.font=[UIFont systemFontOfSize:18];
            [self addSubview:self.priceLabel];
        }
    }
    return self;
}
@end
