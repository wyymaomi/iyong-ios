//
//  DaitgViewController.m
//  xiangmu
//
//  Created by David kim on 16/3/24.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PartnerWaitPassedViewController.h"
#import "DaitgCollectionViewCell.h"
#import "UserManager.h"
#import "HttpConstants.h"
#import "NetworkManager.h"
#import "MJRefresh.h"
//#import "UIViewController+Example.h"
#import "LoginViewController.h"

@interface PartnerWaitPassedViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
//    NSArray *dataArray;
    NSString *uid;
}

@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UICollectionView *collectionView;;
@end

@implementation PartnerWaitPassedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"合作伙伴待通过";
    [self createInterface];
    [self RefreshData];
    // Do any additional setup after loading the view.
}


-(void)createInterface
{
    UICollectionViewFlowLayout *flowlayout=[[UICollectionViewFlowLayout alloc]init];
    [flowlayout setItemSize:CGSizeMake(self.view.frame.size.width, 60)];
    [flowlayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    flowlayout.sectionInset=UIEdgeInsetsMake(5, 0, 5, 0);
    
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:flowlayout];
    self.collectionView.showsVerticalScrollIndicator=NO;
    self.collectionView.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    [self.collectionView registerClass:[DaitgCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=YES;
    [self.view addSubview:self.collectionView];
}
-(void)RefreshData
{
//    __unsafe_unretained __typeof(self) weakSelf = self;
    
    WeakSelf
    // 下拉刷新
    self.collectionView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 增加5条假数据
        [weakSelf downloadData];
        
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2)), dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
            
            // 结束刷新
            [weakSelf.collectionView.mj_header endRefreshing];
        });
    }];
    [self.collectionView.mj_header beginRefreshing];
    
    // 上拉刷新
    self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 增加5条假数据
        [weakSelf downloadData];
        
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2)), dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
            
            // 结束刷新
            [weakSelf.collectionView.mj_footer endRefreshing];
        });
    }];
    // 默认先隐藏footer
    self.collectionView.mj_footer.hidden = YES;
}
-(void)downloadData
{
    // 待通过好友数据获取
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Partner_ApplyList_API];
    NSString *parmas = [NSString stringWithFormat:@"time=%@", @""];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        
        DLog(@"response success");
        weakSelf.dataArray  = [responseData valueForKey:@"data"];
        //[self createInterface];
        [weakSelf.collectionView reloadData];
        NSLog(@"result = %@", responseData);
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        //                ViewController *viewController=[[ViewController alloc]init];
        //                [self.navigationController pushViewController:viewController animated:YES];
        
    }];
    
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DaitgCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor=[UIColor whiteColor];
    UIButton *agree=[UIButton buttonWithType:UIButtonTypeCustom];
    agree.frame=CGRectMake(ViewWidth-70, 15, 50, 30);
    agree.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:193.0f/255.0f blue:65.0f/255.0f alpha:1.0f];
    [agree addTarget:self action:@selector(gotoAgree:) forControlEvents:UIControlEventTouchUpInside];
    [agree setTitle:@"同意" forState:UIControlStateNormal];
    agree.layer.cornerRadius=5;
    agree.titleLabel.textColor=[UIColor whiteColor];
    //agree.layer.cornerRadius=13;
    agree.titleLabel.font=[UIFont systemFontOfSize:13];
    [cell addSubview:agree];
    
    //    UIButton *negelcted=[UIButton buttonWithType:UIButtonTypeCustom];
    //    negelcted.frame=CGRectMake(ViewWidth-65, 10, 35, 40);
    //    negelcted.backgroundColor=[UIColor colorWithRed:131.0f/255.0f green:131.0f/255.0f blue:131.0f/255.0f alpha:1.0f];
    //    [negelcted setTitle:@"忽略" forState:UIControlStateNormal];
    //    negelcted.titleLabel.textColor=[UIColor whiteColor];
    //    [negelcted addTarget:self action:@selector(gotoNegelct:) forControlEvents:UIControlEventTouchUpInside];
    //    //negelcted.layer.cornerRadius=13;
    //
    //    negelcted.titleLabel.font=[UIFont systemFontOfSize:13];
    //    [cell addSubview:negelcted];
    cell.company.text=[self.dataArray[indexPath.row] valueForKey:@"srcCompanyName"];
    cell.boss.text=[self.dataArray[indexPath.row] valueForKey:@"srcNickname"];
    return cell;
}
-(void)gotoAgree:(UIButton *)btn
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Partner_Accept_API];
    
    UICollectionViewCell *cell=(UICollectionViewCell *)[btn superview] ;
    NSIndexPath *indexpath=[self.collectionView indexPathForCell:cell];
    uid=[self.dataArray[indexpath.row] valueForKey:@"id"];
    NSString *parmas = [NSString stringWithFormat:@"id=%@",uid];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        
        NSLog(@"response success");
        weakSelf.dataArray  = [responseData valueForKey:@"data"];
        //[self createInterface];
        [weakSelf RefreshData];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"提示" message:@"通过好友申请" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:alertAction];
            [weakSelf presentViewController:alertController animated:YES completion:nil];
            
        }
        NSLog(@"result = %@", responseData);
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        //                ViewController *viewController=[[ViewController alloc]init];
        //                [self.navigationController pushViewController:viewController animated:YES];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
