//
//  ApplyForCreditLineViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/15.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ApplyForCreditLineViewController.h"
#import "TPKeyboardAvoidingTableView.h"
#import "VechileEditTableViewCell.h"
#import "CreditHttpMessage.h"
#import "InputHelperViewController.h"
#import "ActionSheetStringPicker.h"
#import "CreditLineApplyUtil.h"

@interface ApplyForCreditLineViewController ()<UITableViewDelegate, UITableViewDataSource,InputHelperDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (strong, nonatomic) CreditHttpMessage *creditHttpMessage;

@end

@implementation ApplyForCreditLineViewController


-(CreditHttpMessage*)creditHttpMessage
{
    if (_creditHttpMessage == nil) {
        _creditHttpMessage = [[CreditHttpMessage alloc] init];
    }
    return _creditHttpMessage;
}

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = (_type == 0) ? @"申请额度(企业)":@"申请额度(个人)";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 申请额度
-(void)onApply
{
    
//    UITextField *applyNameTextField = self.tableView cell
    
    
    
    NSString *errorMsg;
    BOOL result = [CreditLineApplyUtil validateCreditLineApply:self.creditHttpMessage type:_type errorMsg:&errorMsg];
    
    if (!result && !IsStrEmpty(errorMsg)) {
        [MsgToolBox showToast:errorMsg];
        return;
    }
    
//    if (IsStrEmpty(self.creditHttpMessage.applicant)) {
//        [self showAlertView:@"请输入申请人姓名"];
//        return;
//    }
//    if (_type == 0 && IsStrEmpty(self.creditHttpMessage.companyName)) {
//        [self showAlertView:@"请输入公司名"];
//        return;
//    }
//    if (IsStrEmpty(self.creditHttpMessage.mobile)) {
//        [self showAlertView:@"请输入手机号"];
//        return;
//    }
//    if (IsStrEmpty(self.creditHttpMessage.amountId)) {
//        [self showAlertView:@"请输入额度"];
//        return;
//    }
    
    self.nextAction = @selector(onApply);
    self.object1 = nil;
    self.object2 = nil;
    
    // 个人信息上传
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, (_type==CreditTypeCompany)?CREDIT_APPLY_COMPANY_API:CREDIT_APPLY_PERSON_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在申请额度，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:self.creditHttpMessage.description success:^(NSDictionary* responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"申请信息已提交，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            WeakSelf
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
                
            }];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

#pragma mark - 获取额度列表

-(void)getQuotaList
{
    self.nextAction = @selector(getQuotaList);
    self.object1 = nil;
    self.object2 = nil;
    
    //
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CREDIT_AMOUNT_LIST_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在获取额度列表，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            strongSelf.dataList = responseData[@"data"];
            
            NSMutableArray *dataRows = [NSMutableArray new];
            for(NSDictionary *dict in strongSelf.dataList)
            {
                [dataRows addObject:dict[@"amount"]];
            }
            
            NSInteger rowCount = [strongSelf tableView:strongSelf.tableView numberOfRowsInSection:0];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowCount-1 inSection:0];
            VechileEditTableViewCell *cell = [strongSelf.tableView cellForRowAtIndexPath:indexPath];
            [strongSelf showPickerView:dataRows sender:cell.textField];
            
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}


-(void)showPickerView:(NSArray*)array sender:(id)sender
{
    WeakSelf
    [ActionSheetStringPicker showPickerWithTitle:nil rows:array initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        StrongSelf
        if ([selectedValue isKindOfClass:[NSNumber class]]) {
        
//            NSNumber *selectedValueStr = array[selectedIndex];
            
            strongSelf.creditHttpMessage.amount = selectedValue;
            
//            NSInteger lastRow = (_type == 0)?3:2;
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:lastRow inSection:0];
//            VechileEditTableViewCell *cell = [strongSelf.tableView cellForRowAtIndexPath:indexPath];
//            cell.textField.text = [selectedValueStr stringValue];
            
            NSDictionary *dict = strongSelf.dataList[selectedIndex];
            strongSelf.creditHttpMessage.amountId = dict[@"id"];
            
            [strongSelf.tableView reloadData];
        }
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
        
        
    } origin:sender];
}

#pragma mark - UITextField Delegate method

//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    
//    NSInteger tag = textField.tag;
//    //    if (text.trim.length > 0) {
//    if (tag == CreditApplyMobile) {
//        self.creditHttpMessage.mobile = text.trim;
//    }
//    if (tag == CreditApplyUserName) {
//        self.creditHttpMessage.applicant = text.trim;
//    }
//    if (tag == CreditApplyCompanyName) {
//        self.creditHttpMessage.companyName = text.trim;
//    }
////    [self.tableView reloadData];
//    //    }
//    
////    NSString *text = [textField.text stringByReplacingOccurrencesOfString:range withString:string];
//    
//    return YES;
//}
//-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//    NSString *text = textField.text.trim;
//    NSInteger tag = textField.tag;
////    if (text.trim.length > 0) {
//        if (tag == CreditApplyMobile) {
//            self.creditHttpMessage.mobile = text.trim;
//        }
//        if (tag == CreditApplyUserName) {
//            self.creditHttpMessage.applicant = text.trim;
//        }
//        if (tag == CreditApplyCompanyName) {
//            self.creditHttpMessage.companyName = text.trim;
//        }
//        [self.tableView reloadData];
////    }
//    
////    return YES;
//    
//}

-(void)onTextDidChanged:(id)sender
{
    UITextField *textField = (UITextField*)sender;
    
    NSString *text = textField.text.trim;
    NSInteger tag = textField.tag;
    //    if (text.trim.length > 0) {
    if (tag == CreditApplyMobile) {
        self.creditHttpMessage.mobile = text.trim;
    }
    if (tag == CreditApplyUserName) {
        self.creditHttpMessage.applicant = text.trim;
    }
    if (tag == CreditApplyCompanyName) {
        self.creditHttpMessage.companyName = text.trim;
    }
//    [self.tableView reloadData];
    //    }

}

#pragma mark - UITable View Delegate method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.type == 0) {
        return 4;
    }
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 100;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0f;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 100)];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:self.title forState:UIControlStateNormal];
    button.frame = CGRectMake(15, 30, tableView.frame.size.width-30, 40);
    [button blueStyle];
    [button addTarget:self action:@selector(onApply) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VechileEditTableViewCell *cell = [VechileEditTableViewCell cellWithTableView:tableView];
//    cell.accessoryType = UITableViewCellAccessoryNone;
//    cell.textField.right = ViewWidth-20;
//    cell.textField.delegate = self;
    [cell.textField addTarget:self action:@selector(onTextDidChanged:) forControlEvents:UIControlEventEditingChanged];
    if (indexPath.section == 0) {
        if (_type == 0) {
            if (indexPath.row == CreditRowForCompanyUserName) {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.titleLabel.text = @"申请人";
                cell.textField.tag = CreditApplyUserName;
                cell.textField.placeholder = @"请输入姓名";
                cell.textField.text = self.creditHttpMessage.applicant.trim;
                cell.textField.delegate = self;
                cell.textField.enabled = YES;
            }
            else if (indexPath.row == CreditRowForCompanyCompanyName) {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.titleLabel.text = @"公司";
                cell.textField.tag = CreditApplyCompanyName;
                cell.textField.placeholder = @"请输入公司名";
                cell.textField.text = self.creditHttpMessage.companyName.trim;
                cell.textField.delegate = self;
                cell.textField.enabled = YES;
            }
            else if (indexPath.row == CreditRowForCompanyMobile) {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.titleLabel.text = @"手机号";
                cell.textField.tag = CreditApplyMobile;
                cell.textField.placeholder = @"请输入手机号";
                cell.textField.text = self.creditHttpMessage.mobile.trim;
                cell.textField.keyboardType = UIKeyboardTypeNumberPad;
                cell.textField.delegate = self;
                cell.textField.enabled = YES;
            }
            else if (indexPath.row == CreditRowForCompanyQuota) {
                cell.titleLabel.text = @"意向申请额度";
                cell.textField.tag = CreditApplyQuota;
                cell.textField.placeholder = @"选择额度";
                cell.textField.text = [self.creditHttpMessage.amount stringValue];
                cell.textField.enabled = NO;
            }
        }
        else {
            if (indexPath.row == CreditRowForPersonUserName) {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.titleLabel.text = @"申请人";
                cell.textField.tag = CreditApplyUserName;
                cell.textField.placeholder = @"请输入姓名";
                cell.textField.text = self.creditHttpMessage.applicant.trim;
                cell.textField.enabled = YES;
            }
            else if (indexPath.row == CreditRowForPersonMobile) {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.titleLabel.text = @"手机号";
                cell.textField.tag = CreditApplyMobile;
                cell.textField.placeholder = @"请输入手机号";
                cell.textField.keyboardType = UIKeyboardTypeNumberPad;
                cell.textField.text = self.creditHttpMessage.mobile.trim;
                cell.textField.enabled = YES;
            }
            else if (indexPath.row == CreditRowForPersonQuota) {
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.titleLabel.text = @"意向申请额度";
                cell.textField.tag = CreditApplyQuota;
                cell.textField.placeholder = @"选择额度";
                cell.textField.text = [self.creditHttpMessage.amount stringValue];
                cell.textField.enabled = NO;
            }
        }
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if ((_type ==  && indexPath.row == CreditRowForCompanyQuota) || (_type == 1 && indexPath.row == CreditRowForPersonQuota)) {
    if (indexPath.row == [self tableView:tableView numberOfRowsInSection:0]-1) {
        
        [self getQuotaList];
        
    }
//    else {
//        VechileEditTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//        if (cell != nil) {
//            InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
//            viewController.tag = cell.textField.tag;
//            viewController.delegate = self;
//            viewController.title = cell.titleLabel.text;
//            viewController.text = cell.textField.text;
//            [self.navigationController pushViewController:viewController animated:YES];
//            return;
//        }
//    }
    
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}


@end
