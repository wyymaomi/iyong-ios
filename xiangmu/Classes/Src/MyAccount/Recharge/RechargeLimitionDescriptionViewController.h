//
//  RechargeLimitionDescriptionViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, RechargeLimitRow) {
    RechargeLimitRowAlipay=1,
    RechargeLimitRowWepay=0
};

@interface RechargeLimitionDescriptionViewController : BaseViewController

@end
