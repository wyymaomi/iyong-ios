//
//  EditDiaoduViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/11.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "EditDiaoduViewController.h"
#import "MyUtil.h"
#import "AddCarTableViewCell.h"
#import "UserManager.h"
#import "NetworkManager.h"
#import "AddDiaoDetailViewController.h"

@interface EditDiaoduViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation EditDiaoduViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"昵称：%@", _phone];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(gotoSave)];
//    self.view.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
//    [self createMyNav];
    [self.view addSubview:self.tableView];
    // Do any additional setup after loading the view.
}
//-(void)createMyNav
//{
//    self.titleLabel=[self addNavTitle:CGRectMake(ViewWidth/2-30, 0, 60, 49) title:[NSString stringWithFormat:@"昵称:%@",_phone]];
//    self.titleLabel.textColor=[UIColor whiteColor];
//    
//    UIButton *btn=[self addNavBtn:CGRectMake(0, 0, 9, 15) title:nil target:self action:@selector(gotoBack) isLeft:YES];
//    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
//    
//    UIButton *btn1=[self addNavBtn:CGRectMake(0, 0, 40, 20) title:@"保存" target:self action:@selector(gotoSave) isLeft:NO];
//    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    
//    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:35.0f/255.0f green:70.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
//    self.navigationController.navigationBar.translucent=NO;
//}
-(UITableView *)tableView
{
    if (_tableView == nil) {
        self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 10, ViewWidth, ViewHeight) style:UITableViewStyleGrouped];
        //560
        self.tableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        self.tableView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
        self.tableView.scrollEnabled=YES;
        self.tableView.delegate=self;
        self.tableView.dataSource=self;
        self.tableView.showsVerticalScrollIndicator=NO;
        self.tableView.tableFooterView=[[UIView alloc]init];
    }
    return _tableView;
}
-(void)getRow:(NSInteger)row WithTitle:(NSString *)title
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=title;
    if (title.length==0) {
        if (row==0) {
            cell.rightLabel.text=@"请填写姓名";
        }
//        else if (row==1)
//        {
//            cell.rightLabel.text=@"请填写电话号码";
//        }
    }
}
-(void)gotoBack
{
    
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    if ([cell.rightLabel.text isEqualToString:@"请填写姓名"]) {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"提示" message:@"请填写完整信息" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertView addAction:alertAction];
        [self presentViewController:alertView animated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}
//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}
#pragma mark----UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
        return 1;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        return 20;
    }
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    AddCarTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    cell.backgroundColor=[UIColor whiteColor];
    if (cell==nil) {
        cell=[[AddCarTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
        //NSArray *nameArray=@[@"姓   名",@"电   话"];
        cell.textLabel.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        cell.textLabel.font=[UIFont systemFontOfSize:13];
        cell.textLabel.text=@"姓   名";
    if (indexPath.row==0) {
        cell.rightLabel.text=self.name;
    }
//    else
//    {
//        cell.rightLabel.text=self.phone;
//    }
    

       //显示最右边的箭头
    //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0) {
        AddCarTableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
        AddDiaoDetailViewController *addDiaodetail=[[AddDiaoDetailViewController alloc]init];
        addDiaodetail.name=[NSString stringWithFormat:@"%@",cell.textLabel.text];
        addDiaodetail.rightname=[NSString stringWithFormat:@"%@",cell.rightLabel.text];
        addDiaodetail.type=DiaoTypeEdit;
        addDiaodetail.delegate=self;
        addDiaodetail.row=indexPath.row;
        [self.navigationController pushViewController:addDiaodetail animated:YES];
    }
//    else if (indexPath.row==1)
//    {
//        UIAlertController *alertView=[UIAlertController alertControllerWithTitle:@"提示" message:@"调度员电话不可编辑" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *alertAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            
//        }];
//        [alertView addAction:alertAction];
//        [self presentViewController:alertView animated:YES completion:nil];
//       
//    }
    
}


-(void)downloadData
{
    NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:0 inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath1];
    
    
//    NSIndexPath *indexPath2=[NSIndexPath indexPathForRow:1 inSection:0];
//    AddCarTableViewCell *cell1=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath2];
    
   
    // 汽车信息获取
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Diaodu_Update_API];
    NSString *parmas = [NSString stringWithFormat:@"nickname=%@&username=%@",cell.rightLabel.text,self.phone];
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        
        NSLog(@"response success");
        if ([cell.rightLabel.text isEqualToString:@"请填写姓名"]) {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"提示" message:@"请填写完整信息" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alertView addAction:alertAction];
            [self presentViewController:alertView animated:YES completion:nil];
        }
        else
        {
        //添加成功,发送通知
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"editDiaoduyuan" object:nil];
            if ([_delegate respondsToSelector:@selector(editdiaodu)]) {
                [_delegate editdiaodu];
            }
        
        [self.navigationController popViewControllerAnimated:YES];
        }
        NSLog(@"result = %@", responseData);
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
    }];
    
}


-(void)gotoSave
{
    [self downloadData];
    //[self.navigationController popViewControllerAnimated:YES];
}



@end
