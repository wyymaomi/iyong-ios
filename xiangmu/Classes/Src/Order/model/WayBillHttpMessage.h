//
//  WayBillHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface WayBillHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *orderId;// 订单ID
@property (nonatomic, strong) NSString *location;// 到达地点
@property (nonatomic, strong) NSString *endTime;// 结束时间
@property (nonatomic, strong) NSString *distance;// 路程距离
@property (nonatomic, strong) NSString *remark;// 备注

@end
