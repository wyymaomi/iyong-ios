//
//  ExperienceCommentTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExperienceCommentViewModel.h"
#import "FollowCommentBodyView.h"

@interface ExperienceCommentTableViewCell : UITableViewCell

//@property (nonatomic, weak) id<UserExperienceCellDelegate> delegate;

@property (nonatomic, strong) ExperienceCommentViewModel *viewModel;

// 主体
@property (nonatomic, strong) FollowCommentBodyView *bodyView;
// 工具条
//@property (nonatomic, strong) CommentToolBarView *toolView;

+ (instancetype)momentsTableViewCellWithTableView:(UITableView *)tableView;

@property (nonatomic, strong) UIView *lineView;

@end
