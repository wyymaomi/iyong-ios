//
//  ActionSheetCityPicker.h
//  lottery
//
//  Created by wyy on 14-12-30.
//  Copyright (c) 2014年 ___QIHUITEL___. All rights reserved.
//

#import "AbstractActionSheetPicker.h"


typedef NS_ENUM(NSUInteger, AreaInfoType){
    AreaInfoTypeProvince = 1,
    AreaInfoTypeCity=2,
    AreaInfoTypeDistrict=3
};

typedef NS_ENUM(NSUInteger, AreaInfoLevel) {
    AreainfoLevelCity=2,
    AreainfoLevelDistrict=3
};

@protocol AreaInfoModel <NSObject>

@end


@interface AreaInfoModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *parent_id;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *level;
@property (nonatomic, strong) NSString<Optional> *sort;

@end

@interface AreaInfoList : JSONModel

@property (nonatomic, strong) NSArray<AreaInfoModel, Optional> *data;

@end

@class ActionSheetCityPicker;
//typedef void(^ActionCityDoneBlock)(ActionSheetCityPicker *picker, NSInteger selectedIndex, id selectedValue);
//typedef void(^AciontCityDoneBlock)(ActionSheetCityPicker *picker , NSString *province, NSString *city);
typedef void(^ActionCityDoneBlock)(ActionSheetCityPicker *picker, AreaInfoModel *province, AreaInfoModel *city, AreaInfoModel *district);
typedef void(^ActionCityCancelBlock)(ActionSheetCityPicker *picker);

static const float firstColumnWidth = 100.0f;
static const float secondColumnWidth = 100.0f;

@interface ActionSheetCityPicker : AbstractActionSheetPicker <UIPickerViewDelegate, UIPickerViewDataSource>
/**
 *  Create and display an action sheet picker.
 *
 *  @param title             Title label for picker
 *  @param data              is an array of strings to use for the picker's available selection choices
 *  @param index             is used to establish the initially selected row;
 *  @param target            must not be empty.  It should respond to "onSuccess" actions.
 *  @param successAction
 *  @param cancelActionOrNil cancelAction
 *  @param origin            must not be empty.  It can be either an originating container view or a UIBarButtonItem to use with a popover arrow.
 *
 *  @return  return instance of picker
 */
//+ (instancetype)showPickerWithTitle:(NSString *)title rows:(NSArray *)data initialSelection:(NSInteger)index target:(id)target successAction:(SEL)successAction cancelAction:(SEL)cancelActionOrNil origin:(id)origin;

// Create an action sheet picker, but don't display until a subsequent call to "showActionPicker".  Receiver must release the picker when ready. */
//- (instancetype)initWithTitle:(NSString *)title rows:(NSArray *)data initialSelection:(NSInteger)index target:(id)target successAction:(SEL)successAction cancelAction:(SEL)cancelActionOrNil origin:(id)origin;



//+ (instancetype)showPickerWithTitle:(NSString *)title rows:(NSArray *)strings initialSelection:(NSInteger)index doneBlock:(ActionCityDoneBlock)doneBlock cancelBlock:(ActionCityCancelBlock)cancelBlock origin:(id)origin;

//- (instancetype)initWithTitle:(NSString *)title rows:(NSArray *)strings initialSelection:(NSInteger)index doneBlock:(ActionCityDoneBlock)doneBlock cancelBlock:(ActionCityCancelBlock)cancelBlockOrNil origin:(id)origin;

+(instancetype)showPickerWithTitle:(NSString*)title
                         doneBlock:(ActionCityDoneBlock)doneBlock
                       cancelBlock:(ActionCityCancelBlock)cancelBlockOrNil
                            origin:(id)origin;

-(instancetype)initWithTitle:(NSString*)title
                       level:(NSUInteger)level
                   doneBlock:(ActionCityDoneBlock)doneBlock
                 cancelBlock:(ActionCityCancelBlock)cancelBlockOrNil
                      origin:(id)origin;

@property (nonatomic, copy) ActionCityDoneBlock onActionSheetDone;
@property (nonatomic, copy) ActionCityCancelBlock onActionSheetCancel;

@end
