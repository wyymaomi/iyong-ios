//
//  AdvDAO.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DAO.h"

@interface AdvDAO : DAO

- (NSMutableArray*)getAllAdv; // 获取所有广告
- (BOOL)updateAdvList:(NSArray*)advList; // 更新广告信息

@end
