//
//  WebViewController.h
//  lottery
//
//  Created by WYY on 14-6-5.
//
//

#import <UIKit/UIKit.h>
#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"
//#import "UIWebVIew.h"
#import "IMYWebView.h"
//#import "JHURLCache.h"

@interface BaseWebViewController : BaseViewController<UIWebViewDelegate, NJKWebViewProgressDelegate>

@property (strong,nonatomic) IMYWebView* webview;

@property (nonatomic, strong) NJKWebViewProgress *progressProxy;
@property (nonatomic, strong) NJKWebViewProgressView *progressView;

@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) NSString *url;

//-(void)setUserUrl:(NSString*)url;

@property (nonatomic, assign) BOOL hasLeftBarButtonItem;
@property (nonatomic, assign) BOOL hasRightBarButtonItem;

//@property (nonatomic, assign) NSInteger enter_type;
-(void)pressedBackbut:(UIButton*)button;
-(void)startRequest;


@end
