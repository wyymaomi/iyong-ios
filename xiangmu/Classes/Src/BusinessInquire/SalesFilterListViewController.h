//
//  SalesQueryViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "CarModel.h"

//@class CarModel;

@interface SalesFilterListViewController : BaseViewController

@property (nonatomic, strong) NSString *startDateTime;
@property (nonatomic, strong) NSString *endDateTime;
//@property (nonatomic, strong) NSString *carId;
//@property (nonatomic, strong) CarModel *carData;
@property (assign, nonatomic) BOOL isOuterDispatch;
@property (assign, nonatomic) BOOL isAll;// 是否全部；

@property (nonatomic, strong) CarModel *carData;

@end
