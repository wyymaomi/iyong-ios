//
//  BaseViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "GestureLoginViewController.h"
#import "LoginViewController.h"
#import "BBAlertView.h"


@interface BaseViewController ()<LoginDelegate>


@end

@implementation BaseViewController

#pragma mark - life cycle

-(id)init
{
    self = [super init];
    if (self) {
        self.hasLeftBackButton = YES;
        self.enter_type = ENTER_TYPE_PUSH;
        
        self.loadStatus = LoadStatusInit;
        
        // 接收未授权登录错误通知
        // 接收登录超时通知
        // 接收登录成功同志
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRecevieLoginOvertime) name:kOnLoginOvertimeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAuthNotification) name:kUnauthorizeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLoginSuccess) name:kLoginSuccessNotification object:nil];
        
        

    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

// 状态栏是否隐藏
- (BOOL)prefersStatusBarHidden
{
    return NO;
    //    return self.isStatusBarHidden;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar
     setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]
     forBarMetrics:UIBarMetricsDefault];
    
    //当应用进入后台时间超过10分钟，重新进入弹出登录界面
    AppDelegate *appDelegate = APP_DELEGATE;
    [appDelegate handleCurrentTime];
    
    bPopupLoginView = YES;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (IOS7_OR_LATER) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
}

-(void)dealloc
{
    DLog(@"%s",__FUNCTION__);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self unRegistLoadStatusKVO];
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
//    if (IOS7_OR_LATER) {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//    }
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }

    self.view.backgroundColor = UIColorFromRGB(0xEBEBEB);
    
    [self initBackButton];
    
    [self registLoadStatusKVO];
    
//#if DEBUG
//    [self.view addSubview:self.fpsLabel];
//#endif
    


}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
//    self.fpsLabel.top = self.view.frame.size.height-22*Scale;
}

#pragma mark - 初始化导航栏

#pragma mark - 初始化导航栏

-(void)setRightNavigationItemWithTitle:(NSString*)title selMethod:(SEL)selMethod;
{    
    UIBarButtonItem *rightitem = [[UIBarButtonItem alloc] initWithTitle:title style:(UIBarButtonItemStylePlain) target:self action:selMethod];
    NSDictionary *dic = [NSDictionary dictionaryWithObject:UIColorFromRGB(0xE06430) forKey:NSForegroundColorAttributeName];
    [rightitem setTitleTextAttributes:dic forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightitem;
}

-(void)setRightNavigationItemWithImage:(UIImage*)image selMehtod:(SEL)selMethod;
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:selMethod];
}



-(void)initBackButton
{
    if (self.hasLeftBackButton) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icon_black_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]  style:UIBarButtonItemStylePlain target:self action:@selector(pressedBackButton)];
    }
    
}

-(void)pressedBackButton
{
    if (self.enter_type == ENTER_TYPE_PRESENT) {
        if (self.navigationController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else if (self.enter_type == ENTER_TYPE_PUSH) {
        if (self.navigationController) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }

}

#pragma mark - 未授权登录

-(void)onReceiveAuthNotification
{
    
    if (!bPopupLoginView) {
        return;
    }
    
    receiveAuthMessageCount++;
    
    if (receiveAuthMessageCount == 1) {
        
        [[UserManager sharedInstance] doLogout];
//        WeakSelf
        
        [self showLoginAlertView];
    }
    
    
    
}

#pragma mark - 登录过期通知处理

-(void)onRecevieLoginOvertime
{
    
//    if (!bPopupLoginView) {
//        return;
//    }
    
    AppDelegate *appDelegate = APP_DELEGATE;
    appDelegate.receiveLoginOverTimeMessageCount++;
    
    if (appDelegate.receiveLoginOverTimeMessageCount == 1) {
    
        WeakSelf
        [[UserManager sharedInstance] doLogout];
        
        [[UserManager sharedInstance] doAutoLogin:^(NSInteger code_status) {
            //            StrongSelf
            [weakSelf hideHUDIndicatorViewAtCenter];
            
            if (code_status == STATUS_OK) {
                
//                [weakSelf dismissToRootViewController:YES completion:^{
                
                    receiveAuthMessageCount = NO;
                    
                    AppDelegate *appDelegate = APP_DELEGATE;
                    
                    appDelegate.receiveLoginOverTimeMessageCount = 0;
                    [appDelegate initResignTime];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
                    
//                }];
                
            }
            
        } failure:^(NSInteger code_status) {
            
            StrongSelf
            
            [strongSelf showLoginAlertView];
            
        }];
        
    }
    


}


-(BOOL)needLogin
{
//    if (![[UserManager sharedInstance] isLogin]) {
//        if ([[UserManager sharedInstance] isGesturePwdLogin]) {
//            GestureLoginViewController *gesturePwdController = [[GestureLoginViewController alloc] init];
//            gesturePwdController.enter_type = ENTER_TYPE_PRESENT;
//            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:gesturePwdController];
//            [[self topViewController] presentViewController:nav animated:YES completion:nil];
//        }
//        else {
//            LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//            loginViewController.enter_type = ENTER_TYPE_PRESENT;
//            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginViewController];
//            [[self topViewController] presentViewController:nav animated:YES completion:nil];
//        }
//        return YES;
//    }
    
    if (![[UserManager sharedInstance] isLogin]) {
        [self showLoginAlertView];
        return YES;
    }
    
    
    AppDelegate *appDelegate = APP_DELEGATE;
    if ([appDelegate isLoginOvertime]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
        return YES;
    }
    
    return NO;
}

- (void)saveNextAction:(SEL)selector object1:(id)object1 object2:(id)object2
{
    self.nextAction = selector;
    self.object1 = object1;
    self.object2 = object2;
}

-(void)resetAction;
{
    self.nextAction = nil;
    self.object1 = nil;
    self.object2 = nil;
}

-(void)onLoginSuccess
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    if (self.nextAction) {
        if ([self respondsToSelector:self.nextAction]) {
            [self performSelector:self.nextAction withObject:self.object1 withObject:self.object2];
        }
    }
    self.nextAction = nil;
    self.object1 = nil;
    self.object2 = nil;
#pragma clang diagnostic pop
}

#pragma mark - 监听loadStatus

- (void)registLoadStatusKVO
{
    [self addObserver:self
           forKeyPath:@"loadStatus"
              options:NSKeyValueObservingOptionNew
              context:NULL];
}

- (void)unRegistLoadStatusKVO
{
    [self removeObserver:self
              forKeyPath:@"loadStatus"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"loadStatus"])
    {
        if (_pullRefreshTableView) {
            [_pullRefreshTableView reloadEmptyDataSet];
        }
        else if (_headerPullRefreshTableView) {
            [_headerPullRefreshTableView reloadEmptyDataSet];
        }
    }
}

//- (void)setLoadStatus:(NSInteger)loadStatus
//{
//    if (_pullRefreshTableView) {
//        [_pullRefreshTableView reloadEmptyDataSet];
//    }
//    else if (_headerPullRefreshTableView) {
//        [_headerPullRefreshTableView reloadEmptyDataSet];
//    }
//}

-(NSArray*)codeMsgList
{
    if (_codeMsgList == nil) {
        // 加载plist中的字典数组
        NSString *path = [[NSBundle mainBundle] pathForResource:CODE_MSG_PLIST ofType:nil];
        _codeMsgList = [NSArray arrayWithContentsOfFile:path];
    }
    return _codeMsgList;
}

#pragma mark - 根据错误代码返回相应的错误信息
-(NSString*)getErrorMsg:(NSInteger)code
{
    for (id obj in self.codeMsgList) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = obj;
            if ([dict[@"code"] integerValue] == code) {
                return dict[@"msg"];
            }
        }
    }
    return MSG_FAILED;
}

- (UIViewController*)topViewController
{
    return [self topViewControllerWithRootViewController:self];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = nil;
    UIFont *font = nil;
    UIColor *textColor = nil;
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    
    if (self.loadStatus == LoadStatusNetworkError) {
        text = @"网络请求失败";
    }
    else {
        if (_emptyDataDelegate && [_emptyDataDelegate respondsToSelector:@selector(emptyTitleText)]) {
            text = [_emptyDataDelegate emptyTitleText];
        }
        else {
            text = @"";
        }
    }
    
    if (!text) {
        return nil;
    }
    
    font = [UIFont systemFontOfSize:18];
    textColor = [UIColor blackColor];//[UIColor colorWithHex:@"d9dce1"];
    
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = nil;
    UIFont *font = nil;
    UIColor *textColor = nil;
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    if (self.loadStatus == LoadStatusNetworkError) {
        text = @"请检查您的网络\n重新加载吧";
    }
    else {
        if (_emptyDataDelegate && [_emptyDataDelegate respondsToSelector:@selector(emptyDetailText)]) {
            text = [_emptyDataDelegate emptyDetailText];
        }
        else {
            text = @"暂无数据";
        }
    }
    
    font = FONT(13);
    textColor = UIColorFromRGB(0x4D4C4C);
    
    
    if (!text) {
        return nil;
    }
    
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    if (paragraph) [attributes setObject:paragraph forKey:NSParagraphStyleAttributeName];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
    
    return attributedString;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    if (self.loadStatus == LoadStatusNetworkError) {
        return [UIImage imageNamed:@"img_network_error"];
    }
    else {
        if (self.emptyDataDelegate && [self.emptyDataDelegate respondsToSelector:@selector(emptyIconImage)]) {
            return [self.emptyDataDelegate emptyIconImage];
        }
        return [UIImage imageNamed:@"img_claim"];
    }
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    NSString *text = nil;
    UIFont *font = nil;
    UIColor *textColor = nil;
    
    text = @"重新加载";
    font = FONT(16);
    textColor = UIColorFromRGB(0x575656);
    
    if (!text) {
        return nil;
    }
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - DZNEmptyDataSetDelegate Methods

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
{
    if (_pullRefreshTableView || _headerPullRefreshTableView) {
        if (self.loadStatus == LoadStatusInit ||
            self.loadStatus == LoadStatusLoading) {
            return NO;
        }
        return YES;
    }
    
    return NO;

}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view
{
    self.loadStatus = LoadStatusInit;
//    [self.pullRefreshTableView beginHeaderRefresh];
    if (_pullRefreshTableView) {
        [_pullRefreshTableView beginHeaderRefresh];
    }
    else if (_headerPullRefreshTableView) {
        [_headerPullRefreshTableView beginHeaderRefresh];
    }
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
{
    self.loadStatus = LoadStatusInit;
//    [self.pullRefreshTableView beginHeaderRefresh];
    if (_pullRefreshTableView) {
        [_pullRefreshTableView beginHeaderRefresh];
    }
    else if (_headerPullRefreshTableView) {
        [_headerPullRefreshTableView beginHeaderRefresh];
    }
}


#pragma mark - custom UITableView

- (TPKeyboardAvoidingTableView*)tpkeyboardAvoidingPlainTableView
{
    if (!_tpkeyboardAvoidingPlainTableView) {
        _tpkeyboardAvoidingPlainTableView = [[TPKeyboardAvoidingTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tpkeyboardAvoidingPlainTableView.backgroundColor = [UIColor clearColor];
        _tpkeyboardAvoidingPlainTableView.delegate = self;
        _tpkeyboardAvoidingPlainTableView.dataSource = self;
        _tpkeyboardAvoidingPlainTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _tpkeyboardAvoidingPlainTableView.tableFooterView = [UIView new];
    }
    return _tpkeyboardAvoidingPlainTableView;
}

-(TPKeyboardAvoidingTableView*)tpkeyboardAvoidingGroupTableView
{
    if (!_tpkeyboardAvoidingGroupTableView) {
        _tpkeyboardAvoidingGroupTableView = [[TPKeyboardAvoidingTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tpkeyboardAvoidingGroupTableView.backgroundColor = [UIColor clearColor];
        _tpkeyboardAvoidingGroupTableView.delegate = self;
        _tpkeyboardAvoidingGroupTableView.dataSource = self;
        _tpkeyboardAvoidingGroupTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _tpkeyboardAvoidingGroupTableView.tableFooterView = [UIView new];
    }
    return _tpkeyboardAvoidingGroupTableView;
}

-(PullRefreshTableView*)pullRefreshTableView
{
    if (_pullRefreshTableView == nil) {
        _pullRefreshTableView = [[PullRefreshTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _pullRefreshTableView.delegate = self;
        _pullRefreshTableView.dataSource = self;
        _pullRefreshTableView.pullRefreshDelegate = self;
        _pullRefreshTableView.hasFooterRefresh = NO;
        _pullRefreshTableView.hasHeaderRefresh = YES;
        _pullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _pullRefreshTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _pullRefreshTableView.tableFooterView = [UIView new];
        _pullRefreshTableView.emptyDataSetSource = self;
        _pullRefreshTableView.emptyDataSetDelegate = self;
    }
    return _pullRefreshTableView;
}

-(HeaderPullRefreshTableView*)headerPullRefreshTableView
{
    if (_headerPullRefreshTableView == nil) {
        _headerPullRefreshTableView = [[HeaderPullRefreshTableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
        _headerPullRefreshTableView.delegate = self;
        _headerPullRefreshTableView.dataSource = self;
        _headerPullRefreshTableView.pullRefreshDelegate = self;
        _headerPullRefreshTableView.footerHidden = YES;
        _headerPullRefreshTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _headerPullRefreshTableView.tableFooterView = [UIView new];
        _headerPullRefreshTableView.emptyDataSetSource = self;
        _headerPullRefreshTableView.emptyDataSetDelegate = self;
    }
    return _headerPullRefreshTableView;
    
    
}

- (UITableView*)plainTableView
{
    if (_plainTableView == nil) {
        
        _plainTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
        _plainTableView.backgroundColor = [UIColor clearColor];
        _plainTableView.delegate = self;
        _plainTableView.dataSource = self;
        _plainTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _plainTableView.tableFooterView = [UIView new];
        
    }
    return _plainTableView;
}

- (UITableView*)groupTableView
{
    if (_groupTableView == nil) {
        
        _groupTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        _groupTableView.backgroundColor = [UIColor clearColor];
        _groupTableView.delegate = self;
        _groupTableView.dataSource = self;
        _groupTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _groupTableView.tableFooterView = [UIView new];
        
    }
    
    return _groupTableView;
}

-(NSMutableArray*)dataList
{
    if (_dataList == nil) {
        _dataList = [NSMutableArray new];
    }
    return _dataList;
}

//-(YYFPSLabel*)fpsLabel
//{
//    if (!_fpsLabel) {
//        
//        _fpsLabel = [YYFPSLabel new];
//        [_fpsLabel sizeToFit];
//        _fpsLabel.bottom = self.view.height - 12*Scale - 44*Scale;
//        _fpsLabel.left = 12;
//        _fpsLabel.alpha = 0;
//        
//    }
//    return _fpsLabel;
//}

#pragma mark - UITableView delegate method

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

#pragma  mark - UIScrollView Delegate method

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
#if DEBUG
//    if (_fpsLabel) {
//        if (_fpsLabel.alpha == 0) {
//            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//                _fpsLabel.alpha = 1;
//            } completion:NULL];
//        }
//    }
    
#endif
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
#if DEBUG
//    if (!decelerate) {
//        if (_fpsLabel && _fpsLabel.alpha != 0) {
//            [UIView animateWithDuration:1 delay:2 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//                _fpsLabel.alpha = 0;
//            } completion:NULL];
//        }
//    }
#endif
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
#if DEBUG
//    if (_fpsLabel && _fpsLabel.alpha != 0) {
//        [UIView animateWithDuration:1 delay:2 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//            _fpsLabel.alpha = 0;
//        } completion:NULL];
//    }
#endif
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
#if DEBUG
//    if (_fpsLabel && _fpsLabel.alpha == 0) {
//        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//            _fpsLabel.alpha = 1;
//        } completion:^(BOOL finished) {
//        }];
//    }
#endif
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
