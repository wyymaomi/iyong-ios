//
//  ProviderHeaderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

@interface ProviderHeaderTableViewCell : UITableViewCell

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

//+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+(CGFloat)getCellHeight;

//@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@property (nonatomic, strong) SDCycleScrollView *bannerView;

@end
