//
//  AdvDetailModel.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvDetailModel.h"

@implementation AdvDetailModel

-(BOOL)canBuy;
{
    return ([self.status integerValue] == AdPurchaseStatusNotBuy) && ([self.purchaseFlag integerValue] == AdPurchaseFlagCanbuy);
}

@end
