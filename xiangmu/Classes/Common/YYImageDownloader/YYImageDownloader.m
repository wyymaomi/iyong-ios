//
//  YYImageDownloader.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "YYImageDownloader.h"
#import "AliyunOSSManager.h"

@interface YYImageDownloader()

@property (strong, nonatomic) NSOperationQueue *downloadQueue;
@property (strong, nonatomic) NSMutableArray *errorUrl;

@end

@implementation YYImageDownloader

+(YYImageDownloader*)sharedDownloader {
    static YYImageDownloader *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (instancetype)init {
    if (self = [super init]) {
        //        _queue = dispatch_queue_create("com.iyong.YYImageDownloaderBarrierQueue", DISPATCH_QUEUE_CONCURRENT);
        _downloadQueue = [NSOperationQueue new];
        _downloadQueue.maxConcurrentOperationCount = 4;
        _downloadQueue.name = @"com.iyong.YYImageDownloadQueue";
        
        _errorUrl = [NSMutableArray new];
        
        _downloadImageUrls = [NSMutableArray new];
        
        _runningOperations = [NSMutableDictionary new];
        
        
    }
    return self;
}


- (void)dealloc
{
    [self.downloadQueue cancelAllOperations];
}

- (void)downloadImageWithAvatarUrl:(NSString*)imageUrl
                       isThumbnail:(BOOL)isThumbnail
                         completed:(YYImageDonwloadCompleteBlock)completeBlock
{
    
    if (IsStrEmpty(imageUrl)) {
        if (completeBlock) {
            completeBlock(nil, nil);
        }
        return;
    }
    
    // 如果本地已经缓存图片
    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
    if (isThumbnail) {
        convertImgUrl = [convertImgUrl substringToIndex:[convertImgUrl indexOfString:@"."]];
        convertImgUrl = [convertImgUrl stringByAppendingString:@"-thumbnail.png"];
    }
    
//    if ([self.errorUrl containsObject:convertImgUrl]) {
//        if (completeBlock) {
//            completeBlock(nil,nil);
//        }
//        return;
//    }
    
    NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    if (imageData != nil && imageData.length > 0) {
        NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
        if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
            if (completeBlock) {
                completeBlock(imageData, nil);
            }
            return;
        }
    }
    
    NSBlockOperation *blockOperation = [NSBlockOperation new];
    [blockOperation addExecutionBlock:^{
        

        
        __weak __typeof(self)wself = self;
        
        [[NetworkManager sharedInstance] downloadImageData:imageUrl requestMethod:POST success:^(id responseData) {
            //    [[NetworkManager sharedInstance] downloadImageData:url requestMethod:POST params:params success:^(id responseData) {
            
            if (!wself) return;
            
            if ([responseData isKindOfClass:[NSData class]]) {
                
                if (!responseData) {
                    return;
                }
                
                NSData *imageData = responseData;
                
                // 判断是否为jpg或者png格式图片
                NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
                
                if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                    
                    [[FileCache defaultCache] saveData:imageData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                    
//                    dispatch_main_async_safe(^{
                    
                        if (completeBlock) {
                            completeBlock(imageData, nil);
                        }
                        
//                    });
                    
                    
                    
                }
                else {
                    
                    [self.errorUrl addObject:convertImgUrl];
                    
                    if (completeBlock) {
                        completeBlock(nil, nil);
                    }
                    
                }
                
            }
            
        } andFailure:^(NSString *errorDesc) {
            
            //            if (completeBlock) {
            //                completeBlock(nil, nil);
            //            }
            
            [self.errorUrl addObject:convertImgUrl];
            if (completeBlock) {
                completeBlock(nil, nil);
            }
            
            
        }];
        
        
        
        
    }];
    
    [_downloadQueue addOperation:blockOperation];
}

- (void)safeRemoveRunningOperations:(NSString*)imageUrl
{
    @synchronized (self.runningOperations) {
        if ([self.runningOperations objectForKey:imageUrl]) {
            [self.runningOperations removeObjectForKey:imageUrl];
        }
    }
}

- (void)cancelCurrentImageLoad
{
    
}

- (void)downloadImageWithImageUrl:(NSString*)imageUrl
                      isThumbnail:(BOOL)isThumbnail
                        completed:(YYImageDonwloadCompleteBlock)completeBlock
{
    // Invoking this method without a completedBlock is pointless
    NSAssert(completeBlock != nil, @"completeBlock could not be nil");
    
//    NSOperation *operation = [self.runningOperations objectForKey:imageUrl];
//    if (operation) {
//        [operation cancel];
//    }
 
//    if (IsStrEmpty(imageUrl) /*|| [imageUrl contains:@"null"] || ![imageUrl startsWith:@"img"]*/) {
//        dispatch_main_sync_safe(^{
//            if (completeBlock) {
//                completeBlock(nil,nil);
//            }
//            [self safeRemoveRunningOperations:imageUrl];
//        });
//        
//        return;
//    }
    


    
    // 如果本地已经缓存图片
    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
    if (isThumbnail) {
        convertImgUrl = [convertImgUrl substringToIndex:[convertImgUrl indexOfString:@"."]];
        convertImgUrl = [convertImgUrl stringByAppendingString:@"-thumbnail.png"];
    }
    
    NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    if (imageData != nil && imageData.length > 0) {
        NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
        if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
            
            //                if (![self.downloadImageUrls containsObject:imageUrl]) {
            //                    [self.downloadImageUrls addObject:imageUrl];
            //                }
            
            
            //            dispatch_main_sync_safe(^{
            
            [self safeRemoveRunningOperations:imageUrl];
            
            if (completeBlock) {
                completeBlock(imageData, nil);
            }
            
            //            })
            
            return;
        }
    }
    
    //        if ([self.errorUrl containsObject:convertImgUrl]) {
    //            if (completeBlock) {
    //                completeBlock(nil,nil);
    //            }
    //            return;
    //        }
    

    

    // 判断缓冲池中是否存在当前图片的操作
//    @synchronized (self.runningOperations) {
//        if (self.runningOperations[imageUrl]) {
//            DLog(@"正在下载。。。。imageUrl=%@", imageUrl);
//            return;
//        }
//    }


    
//    WeakSelf
    
    NSBlockOperation *blockOperation = [NSBlockOperation new];
    [blockOperation addExecutionBlock:^{
        

        
        
        
        [[AliyunOSSManager sharedInstance] downloadImage:imageUrl isThumbnail:isThumbnail complete:^(DownloadImageState state, NSData *data) {
            
            if (state == DownloadImageSuccess) {
                
//                if (!wself) {
//                    return;
//                }
                
                NSString *imageContentType = [NSData sd_contentTypeForImageData:data];
                // 是否为图片数据
                if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
//                    if (isThumbnail) {
//                        // 保存缩略图
//                        NSInteger maxFileSize = 300 * 1024;
//                        UIImage *tempImage = [UIImage imageWithData:data];
//                        NSData *compressedData = [ImageUtil compressImageWithData:tempImage toMaxFileSize:maxFileSize];
//                        [[FileCache defaultCache] saveData:compressedData forKey:convertImgUrl cacheAge:kCacheAgeForever];
//                    }
//                    else {
                        [[FileCache defaultCache] saveData:data forKey:convertImgUrl cacheAge:kCacheAgeForever];
//                    }
                    
//                    if (![self.downloadImageUrls containsObject:imageUrl]) {
//                        [self.downloadImageUrls addObject:imageUrl];
//                    }
                    
//                    dispatch_main_sync_safe(^{
                    
                        if (completeBlock) {
                            completeBlock(data, nil);
                        }
                        
                        [self safeRemoveRunningOperations:imageUrl];
                        
//                    })
                    
                    
                    
//                    @synchronized (self.runningOperations) {
//                        [strongSelf.runningOperations removeObjectForKey:imageUrl];
//                    }
                    
                }
                else {
//                    if ([self.downloadImageUrls containsObject:imageUrl]) {
//                        [self.downloadImageUrls removeObject:imageUrl];
//                    }
//                    dispatch_main_sync_safe(^{
                        if (completeBlock) {
                            completeBlock(nil, nil);
                        }
                        
                        [self safeRemoveRunningOperations:imageUrl];
//                    })
                    
                }
            }
            // 下载失败
            else if (state == DownloadImageFailed) {
//                [self.errorUrl addObject:convertImgUrl];
                
//                if ([self.downloadImageUrls containsObject:imageUrl]) {
//                    [self.downloadImageUrls removeObject:imageUrl];
//                }
                
//                dispatch_main_sync_safe(^{
                    if (completeBlock) {
                        completeBlock(nil, nil);
                    }
                    
                    [self safeRemoveRunningOperations:imageUrl];
//                })
                
                
            }
        }];
        
        
    }];
    
    
    [_downloadQueue addOperation:blockOperation];
    
    self.runningOperations[imageUrl] = blockOperation;
    
//    self.operations[imageUrl] = blockOperation;

    
//    self.runningOperations[imageUrl] = blockOperation;
}

@end
