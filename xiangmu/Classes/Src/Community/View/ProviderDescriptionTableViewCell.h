//
//  ProviderDescriptionTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProviderDetailViewModel.h"

@interface ProviderDescriptionTableViewCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UILabel *companyDescriptionLabel;

@property (nonatomic, strong) ProviderDetailViewModel *viewModel;

@property (nonatomic, strong) UILabel *companyDescriptionLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *lineView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
+ (CGFloat)getCellHeight;

@end
