//
//  EncryptUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kRandomLength 8

@interface EncryptUtil : NSObject

+ (NSString*) generateDesKey;

+(NSDictionary*)encryptLoginRequest:(NSString *)body
                             desKey:(NSString*)desKey;
+(NSDictionary*)encryptAutoLoginRequest:(NSString *)sha_body
                             desKey:(NSString *)desKey
                           rsa_body:(NSString*)rsa_body;
+(NSDictionary*)encryptRegisterBody:(NSString*)param1
                             param2:(NSString*)param2
                             desKey:(NSString*)desKey;

//+ (NSString*) encryptBodyData:(NSString*)body desKey:(NSString*)desKey;


+ (void)decryptBodyData:(NSString*)data;

+ (NSString*)generateEncryptRequest:(NSString*)bodyParams
                             token:(NSString*)token
                           session:(NSString*)session;

+ (NSString*)getRSAEncryptPassword:(NSString*)password;

+(NSString*)getRSALoginParams:(NSString*)loginParams;

@end
