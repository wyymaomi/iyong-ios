//
//  OrderHelperDetailViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface OrderHelperDetailViewController : BaseViewController

@property (nonatomic, assign) NSInteger selectedIndex;

@end
