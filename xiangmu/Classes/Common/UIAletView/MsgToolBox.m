//
//  MsgToolBox.m
//  UserExperienceDemo
//
//  Created by liuzhilong on 15/3/31.
//  Copyright (c) 2015年 alibaba. All rights reserved.
//

#import "MsgToolBox.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@implementation MsgToolBox

+ (void) showAlert:(NSString *)title content:(NSString *)content {
    // 保证在主线程上执行
//    if ([NSThread isMainThread]) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:content delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        [alertView show];
//    } else {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:content delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//            [alertView show];
//        });
//    }
    if ([NSThread isMainThread]) {
        [self showToast:content];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showToast:content];
        });
    }
}

//+(void)showToastOnView:(NSString*)message view:(UIView*)view
//{
////    AppDelegate *appDelegate = APP_DELEGATE;
////    UIView *view =
//    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
//    hud.mode = MBProgressHUDModeText;
//    [view addSubview:hud];
//    hud.removeFromSuperViewOnHide = YES;
//    hud.labelText = message;
//    [hud show:YES];
//    [hud hide:YES afterDelay:1.5f];
//}

+ (void)showToast:(NSString*)message
{
//    AppDelegate *appDelegate = APP_DELEGATE;
//    UIView *view = appDelegate.window;
    
    dispatch_main_sync_safe(^{
        
        UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
        
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithWindow:window];
        //    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:view];
        HUD.mode = MBProgressHUDModeText;
        [window addSubview:HUD];
        //    [view addSubview:HUD];
        HUD.removeFromSuperViewOnHide = YES;
        HUD.labelText = message;
        [HUD show:YES];
        [HUD hide:YES afterDelay:1.5f];
        
    });
    

    
}

@end
