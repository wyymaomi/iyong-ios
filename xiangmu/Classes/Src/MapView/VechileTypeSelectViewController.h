//
//  VechileTypeSelectViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "CarTypeModel.h"

@protocol VechileTypeSelectDelgate <NSObject>

-(void)onSelectVechile:(NSUInteger)tag;

@end

@interface VechileTypeSelectViewController : BaseViewController

@property (nonatomic, strong) NSMutableArray<CarTypeModel*> *carTypeList;

@property (nonatomic, weak) id<VechileTypeSelectDelgate> delegate;

@property (nonatomic, assign) NSUInteger carType;// 1-4

@end
