//
//  NetworkManager.m
//  xiangmu
//
//  Created by David kim on 16/3/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NetworkManager.h"

#import "AppDelegate.h"
#import "NetworkReach.h"
#import "UserManager.h"
#import "EncryptUtil.h"
#import "NSString+Encrypt.h"

@implementation NetworkManager

- (AFHTTPRequestOperationManager*)requestManager
{
    if (_requestManager == nil) {
        _requestManager = [AFHTTPRequestOperationManager manager];
    }
    
    return _requestManager;
}

- (void)cancelAllRequest
{
    [self.requestManager.operationQueue cancelAllOperations];
}

- (void)cancelHTTPOperationsWithMethod:(NSString *)method url:(NSString *)url
{
    NSError *error;
    
    NSString *pathToBeMatched = [[[self.requestManager.requestSerializer requestWithMethod:method URLString:[[NSURL URLWithString:url] absoluteString] parameters:nil error:&error] URL] path];
    
    for (NSOperation *operation in [self.requestManager.operationQueue operations]) {
        if (![operation isKindOfClass:[AFHTTPRequestOperation class]]) {
            continue;
        }
        BOOL hasMatchingMethod = !method || [method  isEqualToString:[[(AFHTTPRequestOperation *)operation request] HTTPMethod]];
        BOOL hasMatchingPath = [[[[(AFHTTPRequestOperation *)operation request] URL] path] isEqual:pathToBeMatched];
        
        if (hasMatchingMethod && hasMatchingPath) {
            [operation cancel];
        }
    }
}

-(void)startHttpGet:(NSString*)url
             params:(NSDictionary*)params
          withBlock:(void(^)(NSInteger code_status, NSDictionary *result, NSString *error))block;
{
    DLog(@"url = %@", url);
    DLog(@"params = %@", params);
    
    
//    // 检测网络连接
//    AppDelegate *appDelegate = APP_DELEGATE;
//    NetworkStatus status = appDelegate.networkReach.lastNetworkStatus;
//    if (NotReachable == status) {
//        if (block) {
//            block(1,nil,MSG_NETWORK_FAILED);
//        }
//        return;
//    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;

    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        DLog(@"responseObject : %@", responseObject);
        if (block) {
            block([responseObject[@"code"] integerValue], responseObject, responseObject[@"msg"]);
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (block) {
            block(1, nil, @"网络连接错误，请重试");
        }
        
    }];
}

-(void)startHttpPost:(NSString*)url
              params:(NSDictionary*)params
           withBlock:(void(^)(NSInteger code_status, NSDictionary *result, NSString *error))block;
{
    DLog(@"url = %@", url);
    DLog(@"params = %@", params);
    
//    // 检测网络连接
//    AppDelegate *appDelegate = APP_DELEGATE;
//    NetworkStatus status = appDelegate.networkReach.lastNetworkStatus;
//    if (NotReachable == status) {
//        if (block) {
//            block(1,nil,MSG_NETWORK_FAILED);
//        }
//        return;
//    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSString *token = [UserManager sharedInstance].token;
    if (!IsStrEmpty(token)) {
        [manager.requestSerializer setValue:[UserManager sharedInstance].token forHTTPHeaderField:@"token"];
    }
    
    [manager POST:url
       parameters:params
          success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
              DLog(@"url = %@, params = %@, responseObject = %@", url, params, responseObject);
//              dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                  if (block) {
                      block([responseObject[@"code"] integerValue], responseObject, nil);
                  }
//              });
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"error : %@", error.description);
              if (block) {
                  block(1, nil, MSG_NETWORK_FAILED);
              }
          }];
}

+ (instancetype)sharedInstance
{
    static NetworkManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NetworkManager alloc] init];
    });
    return instance;
}



#pragma mark - 图片下载
-(NSMutableURLRequest*)downloadImageURLRequest:(NSString*)method
                                     URLString:(NSString*)URLString
                                params:(NSString*)params
{
    if (![[UserManager sharedInstance] token]) {
        return nil;
    }
    
    NSString *localParams;
    if (IsStrEmpty(params)) {
        localParams = [NSString stringWithFormat:@"token=%@", [[UserManager sharedInstance] token]];
    }
    else {
        localParams = [NSString stringWithFormat:@"%@&token=%@", params, [[UserManager sharedInstance] token]];
    }
    DLog(@"param = %@", localParams);
    NSString *desKey = [[UserManager sharedInstance] desKey];
    NSString *desEncryptParam = [localParams desEncrypt:desKey];
    if (IsStrEmpty(desEncryptParam)) {
        return nil;
    }
    NSDictionary *encryptParams = @{@"a" : desEncryptParam};
//    DLog(@"encryptParams = %@", encryptParams);
    
    
    NSURL *url = [NSURL URLWithString:URLString];
    
    NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    mutableRequest.HTTPMethod = method;
    
    NSMutableData *postBody = [NSMutableData data];
    [postBody appendData:[[NSString stringWithFormat:@"a=%@", desEncryptParam] dataUsingEncoding:NSUTF8StringEncoding]];
    [mutableRequest setHTTPBody:postBody];
    
    
    return mutableRequest;
}

-(void)downloadImage:(NSString*)imageUrl
             success:(SuccessBlock)success
          andFailure:(FailureBlock)failure
{
    // 如果本地已经缓存图片
    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
    NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    if (imageData != nil && imageData.length > 23) {
        if (success) {
            success(imageData);
        }
        
        return;
    }
    else {
        if (failure) {
            failure(@"下载图片失败！");
        }
    }
    
    // 如果本地没有缓存图片 则保存到本地
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, DONWLOAD_IMAGE_API_ENCRYPT ];
    NSString *params = [NSString stringWithFormat:@"url=%@", imageUrl];
    [[NetworkManager sharedInstance] downloadImageData:url requestMethod:POST params:params success:^(id responseData) {
        if ([responseData isKindOfClass:[NSData class]]) {
            NSData *imageData = responseData;
            if (imageData.length > 23) {
                [[FileCache defaultCache] saveData:imageData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                if (success) {
                    success(imageData);
                }
            }
            

        }
    } andFailure:^(NSString *errorDesc) {
        if (failure) {
            failure(errorDesc);
        }
        
    }];
}

#pragma mark - 下载图片 包括加密和非加密

- (void)downloadImageData:(NSString*)imageUrl
            requestMethod:(RequestMethod)requestMethod
                  success:(SuccessBlock)success
               andFailure:(FailureBlock)failure
{
    if (requestMethod == POST) {
        
        // 如果已经登陆 进行加密传输
        NSString *url;
        NSDictionary *params;
        
        if ([[UserManager sharedInstance] token]) {
            
            url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, DONWLOAD_IMAGE_API_ENCRYPT];
            NSString *localParams = [NSString stringWithFormat:@"url=%@&token=%@", imageUrl,[StringUtil getSafeString:[UserManager sharedInstance].token]];
            
            DLog(@"param = %@", localParams);
            NSString *desKey = [[UserManager sharedInstance] desKey];
            NSString *desEncryptParam = [localParams desEncrypt:desKey];
            if (IsStrEmpty(desEncryptParam)) {
                return;
            }
            params = @{@"a" : desEncryptParam};
            DLog(@"encryptParams = %@", params);
            
        }
        else {
            url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, DOWNLOAD_IMAGE_API_NOENCRYPT];
            params = @{@"url":imageUrl};
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
        manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        [manager.requestSerializer setValue:[UserManager sharedInstance].token forHTTPHeaderField:@"token"];
        [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if (IsNilOrNull(responseObject) || ![responseObject isKindOfClass:[NSData class]]) {
                return;
            }
            
            if (success) {
                success(responseObject);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            DLog(@"error = %@", error.description);
            DLog(@"error.code = %ld", (long)error.code);
            
            
            if (failure) {
                failure(error.localizedDescription);
            }
            
            if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
                return;
            }
            
            
            
        }];
        
        
    }
}

#pragma mark - 加密传输
- (void)downloadImageData:(NSString*)url
              requestMethod:(RequestMethod)requestMethod
                     params:(NSString*)params
                    success:(SuccessBlock)success
                 andFailure:(FailureBlock)failure
{
    if (requestMethod == POST) {
        
        NSLog(@"url = %@", url);
        
        if (![[UserManager sharedInstance] token]) {
            return;
        }
        
//        // 检测网络连接
//        AppDelegate *appDelegate = APP_DELEGATE;
//        NetworkStatus status = appDelegate.networkReach.lastNetworkStatus;
//        if (NotReachable == status) {
//            if (failure) {
//                failure(MSG_NETWORK_FAILED);
//            }
//            return;
//        }
        
        NSString *localParams = [NSString stringWithFormat:@"%@&token=%@", params,[StringUtil getSafeString:[UserManager sharedInstance].token]];
        
        DLog(@"param = %@", localParams);
        NSString *desKey = [[UserManager sharedInstance] desKey];
        NSString *desEncryptParam = [localParams desEncrypt:desKey];
        if (IsStrEmpty(desEncryptParam)) {
            return;
        }
        NSDictionary *encryptParams = @{@"a" : desEncryptParam};
        DLog(@"encryptParams = %@", encryptParams);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
        manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        [manager.requestSerializer setValue:[UserManager sharedInstance].token forHTTPHeaderField:@"token"];
        [manager POST:url parameters:encryptParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if (IsNilOrNull(responseObject) || ![responseObject isKindOfClass:[NSData class]]) {
                return;
            }
            
            if (success) {
                success(responseObject);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            DLog(@"error = %@", error.description);
            DLog(@"error.code = %ld", (long)error.code);
            
            
            if (failure) {
                failure(error.localizedDescription);
            }
            
            if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
                return;
            }
            
            
            
        }];
        
    }
    else {
        
    }
}

#pragma mark - 加密传输
- (void)startEncryptRequest:(NSString*)url
              requestMethod:(RequestMethod)requestMethod
                     params:(NSString*)params
                    success:(SuccessBlock)success
                 andFailure:(FailureBlock)failure
{
    if (requestMethod == POST) {
        
        DLog(@"url = %@", url);
        
        // 判断是否已经登录
        if (![[UserManager sharedInstance] token]) {
            if (failure) {
                failure(MSG_LOGIN_TIP);
            }
            return;
        }
        
//        // 检测网络连接
//        AppDelegate *appDelegate = APP_DELEGATE;
//        NetworkStatus status = appDelegate.networkReach.lastNetworkStatus;
//        if (NotReachable == status) {
//            if (failure) {
//                failure(MSG_NETWORK_FAILED);
//            }
//            return;
//        }
        
        NSString *localParams;
        if (IsStrEmpty(params)) {
            localParams = [NSString stringWithFormat:@"token=%@", [[UserManager sharedInstance] token]];
        }
        else {
            localParams = [NSString stringWithFormat:@"%@&token=%@", params, [[UserManager sharedInstance] token]];
        }
        DLog(@"param = %@", localParams);
        NSString *desKey = [[UserManager sharedInstance] desKey];
        DLog(@"DES密钥：%@", desKey);
        NSString *desEncryptParam = [localParams desEncrypt:desKey];
        if (IsStrEmpty(desEncryptParam)) {
            return;
        }
        NSDictionary *encryptParams = @{@"a" : desEncryptParam};
        
//        DLog(@"desEncryptParams = %@", desEncryptParam);
        
        AFHTTPRequestOperationManager *manager = self.requestManager;
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
        manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
//        [manager.requestSerializer setValue:TOKEN forHTTPHeaderField:@"token-id"];
        [manager.requestSerializer setValue:[UserManager sharedInstance].token forHTTPHeaderField:@"token"];
        [manager POST:url.trim parameters:encryptParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if (IsNilOrNull(responseObject) || ![responseObject isKindOfClass:[NSData class]]) {
                return;
            }
            
            // 返回加密报文
            NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            if (IsStrEmpty(responseCipher)) {
                return;
            }
            
            // 解密返回的报文
            NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
            
            if (IsStrEmpty(decryptResponse)) {
                
                if (failure) {
                    failure(MSG_DECRYPT_FAILURE);
                }
                return;
            }
            
            // 转换成NSDictioanry
            NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
            if (IsNilOrNull(decryptData) || ![decryptData isKindOfClass:[NSData class]]) {
                return;
            }
            
            //////
            id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:nil];
            
            if (![result isKindOfClass:[NSDictionary class]]) {
                return;
            }
            
            DLog(@"url = %@, params = %@, responseObject = %@", url, params, result);
            
            NSDictionary *responseDict = result;
            if (responseDict[@"code"] != nil && ![responseDict[@"code"] isEqualToString:@""]) {
                
                // 登录成功后重新计算时间
                AppDelegate *appDelegate = APP_DELEGATE;
                [appDelegate initResignTime];
                
                NSInteger code_status = [responseDict[@"code"] integerValue];
                if (code_status == STATUS_OK) {
                    if (success) {
                        success(result);
                    }
                }
                else {
                    if (success) {
                        success(result);
                    }
                }
            }
            else {
                if (failure) {
                    failure(@"返回数据格式有误！");
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            DLog(@"error = %@", error.description);
            DLog(@"error.code = %ld", (long)error.code);

            if ([url contains:SET_DEVICE_ID_API]) {
                return;
            }
            if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
                return;
            }
            else{
                if (failure) {
                    failure(MSG_NETWORK_FAILED);
                }
            }
        }];
        
    }
    else {
        
    }
}

-(void)startRequestMethod:(RequestMethod)method parameters:(NSDictionary *)parameters url:(NSString *)url success:(SuccessBlock)success andFailure:(FailureBlock)failure
{
    if (method==POST) {
        
        NSLog(@"url=%@", url);
        NSLog(@"parameters = %@", parameters);
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        manager.requestSerializer.timeoutInterval = 30.0f;
        //        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (success) {
                NSLog(@"responseObject = %@", responseObject);
                success(responseObject);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error){
            if (failure) {
                failure(error.userInfo.description);
            }
        }];
    }
    //GET方法
    else
    {
        AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
        manager.requestSerializer.timeoutInterval=30.0f;
        [manager GET:url parameters:parameters success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (failure) {
                failure(error.userInfo.description);
            }
            //            switch (error.code) {
            //                case AFNetworkErrorType_NoNetwork:
            //                    break;
            //                case AFNetworkErrorType_TimedOut:
            //                default:
            //                    break;
            //            }
        }];
    }
}
//请求失败回调方法
//@param error 错误对象
+(void)requestFailed:(NSError *)error
{
    NSLog(@"-------\n%ld %@", (long)error.code,error.debugDescription);
    switch (error.code) {
        case AFNetworkErrorType_NoNetwork:
            break;
        case AFNetworkErrorType_TimedOut:
        default:
            break;
    }
}

#pragma mark - 

-(void)verifyPaypassword:(NSString*)payPwd success:(SuccessBlock)success andFailure:(FailureBlock)failure;
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_VALID_API];
    NSString *param = [NSString stringWithFormat:@"password=%@", [EncryptUtil getRSAEncryptPassword:payPwd]];
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        
        if (success) {
            success(responseData);
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (failure) {
            failure(errorDesc);
        }
        
    }];
}

-(void)getBankcardList:(SuccessBlock)success andFailure:(FailureBlock)failure;
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, BANKCARD_LIST_API];
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
        
        if (success) {
            success(responseData);
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (failure) {
            failure(errorDesc);
        }
        
    }];
}

- (void)getFinanceInfo:(SuccessBlock)success andFailue:(FailureBlock)failure;
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_FINANCE_API];
    
   
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(id responseData) {
        
        if (success) {
            success(responseData);
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (failure) {
            failure(errorDesc);
        }
        
        
    }];
}

- (void)getUserInfo:(SuccessBlock)success andFailue:(FailureBlock)failure;
{
    // 个人信息获取
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, My_Detail_API];
//    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {

        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            [UserManager sharedInstance].userModel = [[UserModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            
            if (success) {
                success(responseData);
            }
        }
        else {
            if (failure) {
                failure(getErrorMsg(code_status));
            }
        }
        
    } andFailure:^(NSString *errorDesc) {
        if (failure) {
            failure(errorDesc);
        }
    }];
}


@end
