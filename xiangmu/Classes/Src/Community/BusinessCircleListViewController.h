//
//  BusinessCircleListViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "BussinessSearchHttpMessage.h"
#import "BusinessCircleSearchViewController.h"

typedef enum : NSUInteger {
    BusinessListModeAll,
    BusinessListModeSearch
} BusinessListMode;

@interface BusinessCircleListViewController : BaseViewController

@property (nonatomic, weak) id<BusinessCircleSearchDelegate> delegate;

@property (nonatomic, assign) NSInteger mode;//
                        
//@property (nonatomic, strong) BussinessSearchHttpMessage *httpMessage;

@end
