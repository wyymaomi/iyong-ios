//
//  RSASigner.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "openssl_wrapper.h"
#import "NSDataEx.h"

@interface RSASigner : NSObject
{
    NSString *_publicKey;
}
//
//- (id)initWithPrivateKey:(NSString *)privateKey;

- (id)initWithPublicKey:(NSString *)publicKey;

- (NSString *)signString:(NSString *)string;

@end
