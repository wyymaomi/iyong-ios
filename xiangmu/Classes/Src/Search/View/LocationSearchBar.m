//
//  LocationSearchBar.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "LocationSearchBar.h"

@implementation LocationSearchBar

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.searchTextField.delegate = self;
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    [self.searchTextField addTarget:self action:@selector(textFieldDidChangeText:) forControlEvents:UIControlEventEditingChanged];
}


#pragma mark -
#pragma mark - UITextFieldDelegate


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchFieldShouldBeginEditing:)]) {
        return [self.delegate searchFieldShouldBeginEditing:textField];
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchFieldShouldEndEditing:)]) {
        return [self.delegate searchFieldShouldEndEditing:textField];
    }
    return YES;
}

-(void)textFieldDidChangeText:(UITextField*)textField
{
    UITextRange *selectedRange = [textField markedTextRange];
    NSString * newText = [textField textInRange:selectedRange];
    //获取高亮部分
    if(newText.length > 0)
    {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchBar:textDidChange:)]) {
        [self.delegate searchBar:textField textDidChange:textField.text.trim];
    }
}

@end
