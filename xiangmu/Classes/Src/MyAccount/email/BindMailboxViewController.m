//
//  BindMailboxViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BindMailboxViewController.h"
#import "UserHelper.h"

@interface BindMailboxViewController ()

@end

@implementation BindMailboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"邮箱";
    [self.okButton blueStyle];
    self.textField.text = self.emailbox;
    
    self.bgView.height = DefaultTableRowHeight;
    self.textField.height = DefaultTableRowHeight-self.textField.top*2;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(onBindMailbox:)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.textField becomeFirstResponder];
}


- (IBAction)onBindMailbox:(id)sender {
    
    [self.textField resignFirstResponder];
    
    NSString *errorMsg;
    if (![UserHelper validateEmail:self.textField.text.trim error:&errorMsg]) {
        [self showAlertView:errorMsg];
        return;
    }
    
    self.nextAction = @selector(onBindMailbox:);
    self.object1 = sender;
    self.object2 = nil;
    
    // 个人信息上传
//    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, BIND_MAILBOX_API];
    NSString *params = [NSString stringWithFormat:@"email=%@", self.textField.text.trim];
    
    [self showHUDIndicatorViewAtCenter:@"正在绑定邮箱，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(UserInfo_Update_API) requestMethod:POST params:params success:^(NSDictionary* responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
//            [strongSelf showAlertView:@"绑定邮箱成功"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"绑定邮箱成功！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    
                    [UserManager sharedInstance].userModel.email = strongSelf.textField.text.trim;
                    
                    if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(onBindMailboxSuccess:)]) {
                        [strongSelf.delegate onBindMailboxSuccess:strongSelf.textField.text.trim];
                    }
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
                
            }];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
