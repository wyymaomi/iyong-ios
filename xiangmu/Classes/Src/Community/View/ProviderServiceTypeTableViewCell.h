//
//  ProviderServiceTypeTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ProviderServiceTypeTableViewCell : UITableViewCell

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, strong) NSArray *typeNameArray;

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

//+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
//@property (nonatomic, strong) NSArray *vechileServiceList;

+(CGFloat)getCellHeight;

@end
