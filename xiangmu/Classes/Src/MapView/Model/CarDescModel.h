//
//  CarDescModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/4.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CarDescModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *carLargeImage;
@property (nonatomic, strong) NSString<Optional> *carSmallImage;
@property (nonatomic, strong) NSString<Optional> *carTypeName;
@property (nonatomic, strong) NSString<Optional> *carModelName;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *seatName;

@end
