//
//  GuideViewControllerViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "GuideViewController.h"
#import "MainTabBarController.h"
#import "AppDelegate.h"

#define GUIDE_PAGE_COUNT 5
@interface GuideViewController ()<UIScrollViewDelegate>

@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIScrollView *myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, ViewHeight)];
    for (NSUInteger i=0; i < GUIDE_PAGE_COUNT; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"guide%lu", (unsigned long)i]];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(ViewWidth * i, 0, ViewWidth, ViewHeight)];
        // 在最后一页创建按钮
        if (i == GUIDE_PAGE_COUNT-1) {
            // 必须设置用户交互 否则按键无法操作
            imageView.userInteractionEnabled = YES;
            UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
            button.frame = CGRectMake(50*Scale, ViewHeight-198*Scale, ViewWidth-100*Scale, 54*Scale);
            button.titleLabel.font = BOLD_FONT(20);
//            button.frame = CGRectMake(ViewWidth / 3, ViewHeight * 7 / 8, ViewWidth / 3, ViewHeight / 16);
            [button setTitle:@"马上出发" forState:UIControlStateNormal];
            [button setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
            button.cornerRadius = 5;
            button.borderColor =NAVBAR_BGCOLOR;
            button.borderWidth = 0.5;
//            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [button setBackgroundColor:UIColorFromRGB(0x00508f)];
//            button.layer.borderWidth = 0;
//            button.layer.cornerRadius = 5;
//            button.clipsToBounds = YES;
//            button.layer.borderColor = NAVBAR_BGCOLOR.CGColor;
//            button.layer.borderColor = [UIColor whiteColor].CGColor;
            [button addTarget:self action:@selector(go:) forControlEvents:UIControlEventTouchUpInside];
            [imageView addSubview:button];
        }
        imageView.image = image;
        [myScrollView addSubview:imageView];
    }
    myScrollView.bounces = NO;
    myScrollView.pagingEnabled = YES;
    myScrollView.showsHorizontalScrollIndicator = NO;
    myScrollView.contentSize = CGSizeMake(ViewWidth * GUIDE_PAGE_COUNT, ViewHeight);
    myScrollView.delegate = self;
    [self.view addSubview:myScrollView];
    
    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(ViewWidth/3, ViewHeight-35*Scale, ViewWidth/3, ViewHeight/16)];
//    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(ViewWidth / 3, ViewHeight * 15 / 16, ViewWidth / 3, ViewHeight / 16)];
    // 设置页数
    pageControl.numberOfPages = GUIDE_PAGE_COUNT;
    // 设置页码的点的颜色
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];//[UIColor whiteColor];
    // 设置当前页码的点颜色
    pageControl.currentPageIndicatorTintColor = NAVBAR_BGCOLOR;//UIColorFromRGB(0xF9BF3C);
    
    [self.view addSubview:pageControl];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // 计算当前在第几页
    pageControl.currentPage = (NSInteger)(scrollView.contentOffset.x / [UIScreen mainScreen].bounds.size.width);
}

// 点击按钮保存数据并切换根视图控制器
- (void) go:(UIButton *)sender{
    flag = YES;
//    NSUserDefaults *useDef = [NSUserDefaults standardUserDefaults];
//    // 保存用户数据
//    [useDef setBool:flag forKey:@"notFirst"];
//    [useDef synchronize];
//    [[UserDefaults sharedInstance] setObject:@1 forKey:kNotFirstLaunchApp];
    // 切换根视图控制器
//    self.view.window.rootViewController = [[MainTabBarController alloc] init];
    AppDelegate *appDelegate = APP_DELEGATE;
//    [appDelegate initViews];
    [appDelegate initViewsFromGuidePage];
}

@end
