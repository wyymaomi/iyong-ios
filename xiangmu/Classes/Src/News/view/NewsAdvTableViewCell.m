//
//  NewsAdvTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewsAdvTableViewCell.h"


@implementation NewsAdvTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12*Scale, 2*Scale, ViewWidth-24*Scale, 35*Scale)];
        //        _titleLabel = [[UITextField alloc] initWithFrame:CGRectMake(12*Scale, 10*Scale, ViewWidth-12*Scale-140*Scale, 40*Scale)];
        //        _titleLabel.scrollEnabled = NO;
        _titleLabel.userInteractionEnabled = NO;
        _titleLabel.textColor = UIColorFromRGB(0x212121);
        _titleLabel.font = FONT(16);
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.numberOfLines = 1;
        //        _titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        //        [_titleLabel sizeToFit];
        [self addSubview:_titleLabel];
        
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(_titleLabel.left, _titleLabel.bottom, _titleLabel.width, 175*Scale)];
        _imgView.contentMode = UIViewContentModeScaleAspectFit|UIViewContentModeCenter;
        [self addSubview:_imgView];
        
//        _sourceFromLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.left, [[self class] getCellHeight]-25*Scale, _titleLabel.width, 25*Scale)];
//        _sourceFromLabel.textColor = UIColorFromRGB(0xB9B9B9);
//        _sourceFromLabel.font = FONT(10*Scale);
//        _sourceFromLabel.numberOfLines = 1;
//        [self addSubview:_sourceFromLabel];
//        
//        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(ViewWidth-118*Scale, 10*Scale, 105*Scale, [[self class] getCellHeight]-20*Scale)];
//        [self addSubview:_imgView];
        
    }
    
    return self;
}

+(CGFloat)getCellHeight
{
    return 220*Scale;
}

-(void)initData:(NewsModel*)data
{
    if (!data) {
        return;
    }
    
    _titleLabel.text = data.title;
    
    //    CGSize contentSize = [self.commentModel.content textSize:CGSizeMake(self.bodyNameFrame.size.width, MAXFLOAT) font:_contentFont];
    
//    CGSize textSize = [data.title textSize:CGSizeMake(self.titleLabel.width, 35*Scale) font:self.titleLabel.font];
    
//    self.titleLabel.frame = CGRectMake(self.titleLabel.left, self.titleLabel.top, _titleLabel.width, textSize.height);
    //    [_titleLabel alignTop];
//    self.sourceFromLabel.text = [NSString stringWithFormat:@"来自%@", data.source];
    
//    self.imgView.frame = CGRectMake(_titleLabel.left, _titleLabel.bottom, _titleLabel.width, 175*Scale);
    self.imgView.contentMode = UIViewContentModeScaleAspectFill;
    self.imgView.clipsToBounds = YES;
    NSURL *url = [NSURL URLWithString:data.imgUrl];
    [self.imgView yy_setImageWithURL:url placeholder:DefaultPlaceholderImage];
    
//    [self.imgView yy_setImageWithURL:url placeholder:DefaultPlaceholderImage options:YYWebImageOptionAvoidSetImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//    
//        
//    } transform:^UIImage * _Nullable(UIImage * _Nonnull image, NSURL * _Nonnull url) {
//        
//        image = [image yy_imageByResizeToSize:CGSizeMake(self.imgView.width, self.imgView.height) contentMode:UIViewContentModeCenter];
//        return image;//[image yy_imageByRoundCornerRadius:10];
//        
//    } completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
//        
//        
//    }];
//    [self.imgView yy_setImageWithURL:url placeholder:DefaultPlaceholderImage];
    
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
