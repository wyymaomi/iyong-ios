//
//  MyTableViewCell+Additional.m
//  xiangmu
//
//  Created by David kim on 16/9/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyTableViewCell+Additional.h"
#import "StarView.h"
#import "CertifiedView.h"

@implementation MyTableViewCell (Additional)

- (void)initLevelData;
{
    self.name.text = @"等级";
    StarView *starView = [StarView new];
    starView.tag = 0;
    starView.top = (DefaultTableRowHeight-kStarFrameWith)/2;
    starView.height = kStarFrameWith;
    starView.width = kStarFrameWith * 5;
    starView.left = self.detailLabel.right-starView.width;
    [starView setStarsImages:[[UserManager sharedInstance].userModel.companyScor floatValue]];
    self.accessoryType = UITableViewCellAccessoryNone;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:starView];
}

- (void)initCertificationData
{
    self.name.text = @"认证信息";
    CertifiedView *certifiedView = [CertifiedView new];
    certifiedView.left = self.detailLabel.right - certifiedView.width;
    certifiedView.top = (DefaultTableRowHeight - certifiedView.height) / 2;
    BOOL realnameCertificationed = [[UserManager sharedInstance].userModel.certification boolValue];
    BOOL companyCertificationed = [[UserManager sharedInstance].userModel.companyCertification integerValue] == CompanyCertificationStatusFinished;
    BOOL vechileCertificationed = [[UserManager sharedInstance].userModel.carCertification boolValue];
    
    [certifiedView initData:realnameCertificationed isCompanyCertified:companyCertificationed isVechileCertified:vechileCertificationed];
    
    self.accessoryType = UITableViewCellAccessoryNone;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:certifiedView];
}

- (void)initRealnameCertifcationData;
{
    self.name.text = @"实名认证";
//    self.detailLabel.text = [UserManager sharedInstance].userModel.realName;
    UIImage *backgroundImage;
    NSString *verifyStr;
    UIColor *btnBgColor;
    BOOL isRealnameCertificationed = [[[UserManager sharedInstance].userModel certification] boolValue];
    if (isRealnameCertificationed) {
        verifyStr = @"已认证";
        btnBgColor = NAVBAR_BGCOLOR;//UIColorFromRGB(0x30a6df);
        backgroundImage = [UIImage imageNamed:@"icon_company_certificationed"];
    }
    else {
        verifyStr = @"未认证";
        btnBgColor = [UIColor lightGrayColor];
        backgroundImage = [UIImage imageNamed:@"icon_company_uncertificationed"];
        
    }
    self.verifyButton.hidden = NO;
//    [self.verifyButton setBackgroundImage:[UIImage imageWithColor:btnBgColor] forState:UIControlStateNormal];
//    [self.verifyButton setTitle:verifyStr forState:UIControlStateNormal];
//    self.verifyButton.cornerRadius = 5;
//    self.verifyButton.titleLabel.font = FONT(10);
    [self.verifyButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
//    CGSize textSize = [self.detailLabel.text sizeWithFont:self.detailLabel.font constrainedToSize:CGSizeMake(10000, 20)];
    CGFloat buttonWidth = 39*Scale;
    CGFloat buttonHeight = 15*Scale;
    NSInteger buttonLeft = self.detailLabel.right - /*textSize.width -*/ buttonWidth;
    self.verifyButton.frame = CGRectMake(buttonLeft, (DefaultTableRowHeight-buttonHeight)/2, buttonWidth, buttonHeight);
}

- (void)initCompanyCertificationData;
{
    self.name.text = @"企业认证";
    NSString *verifyStr;
    UIColor *btnBgColor;
    NSInteger companyCertification = [[UserManager sharedInstance].userModel.companyCertification integerValue];
    UIImage *backgroundImage;
    if (companyCertification == CompanyCertificationStatusInit) {
        verifyStr = @"未认证";
        btnBgColor = [UIColor lightGrayColor];
        backgroundImage = [UIImage imageNamed:@"icon_company_uncertificationed"];
    }
    else if (companyCertification == CompanyCertificationStatusInReview) {
        verifyStr = @"待认证";
        btnBgColor = [UIColor lightGrayColor];
        backgroundImage = [UIImage imageNamed:@"icon_company_wait_certification"];
    }
    else if (companyCertification == CompanyCertificationStatusFinished) {
        verifyStr = @"已认证";
        btnBgColor = NAVBAR_BGCOLOR;//UIColorFromRGB(0x30a6df);
        backgroundImage = [UIImage imageNamed:@"icon_company_certificationed"];
    }
    else if (companyCertification == CompanyCertificationStatusRejected) {
        verifyStr = @"已拒绝";
        btnBgColor = [UIColor redColor];
        backgroundImage = [UIImage imageNamed:@"icon_company_reject_certification"];
    }
    else {
        verifyStr = @"未认证";
        btnBgColor = [UIColor lightGrayColor];
    }
    
    self.detailLabel.text = @"";
    
    self.verifyButton.hidden = NO;
    [self.verifyButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
//    [self.verifyButton setTitle:verifyStr forState:UIControlStateNormal];
//    self.verifyButton.cornerRadius = 5;
//    self.verifyButton.titleLabel.font = FONT(10);
    
    CGFloat buttonWidth = 39*Scale;
    CGFloat buttonHeight = 15*Scale;
    self.verifyButton.frame = CGRectMake(self.detailLabel.right - buttonWidth, (self.contentView.frame.size.height-buttonHeight)/2, buttonWidth, buttonHeight);
}

- (void)initVechileCertificationData;
{
    self.name.text = @"车辆认证";
    NSString *verifyStr;
    UIColor *btnBgColor;
    BOOL vechileCertification = [[UserManager sharedInstance].userModel.carCertification boolValue];
    UIImage *backgroundImage;
    if (vechileCertification) {
        verifyStr = @"已认证";
        btnBgColor = NAVBAR_BGCOLOR;//UIColorFromRGB(0x30a6df);
        backgroundImage = [UIImage imageNamed:@"icon_company_uncertificationed"];
    }
    else {
        verifyStr = @"未认证";
        btnBgColor = [UIColor lightGrayColor];
        backgroundImage = [UIImage imageNamed:@"icon_company_certificationed"];
    }
    
    self.detailLabel.text = @"";
    
    self.verifyButton.hidden = NO;
    [self.verifyButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
//    [self.verifyButton setTitle:verifyStr forState:UIControlStateNormal];
//    self.verifyButton.cornerRadius = 5;
//    self.verifyButton.titleLabel.font = FONT(10);
    
    NSInteger buttonWidth = 45;
    NSInteger buttonHeight = 20;
    self.verifyButton.frame = CGRectMake(self.detailLabel.right - buttonWidth, (DefaultTableRowHeight-buttonHeight)/2, buttonWidth, buttonHeight);

}

- (void)initServiceProviderCertificationData;
{
    self.name.text = @"在线服务商发布";
    
    self.detailLabel.text = @"";
    
//    NSString *verifyStr;
//    UIColor *btnBgColor;
//    verifyStr = @"未认证";
//    btnBgColor = [UIColor lightGrayColor];
    
    self.verifyButton.hidden = YES;
//    [self.verifyButton setBackgroundImage:[UIImage imageWithColor:btnBgColor] forState:UIControlStateNormal];
//    [self.verifyButton setTitle:verifyStr forState:UIControlStateNormal];
//    self.verifyButton.cornerRadius = 5;
//    self.verifyButton.titleLabel.font = FONT(10);
    
//    NSInteger buttonWidth = 45;
//    NSInteger buttonHeight = 20;
//    self.verifyButton.frame = CGRectMake(self.detailLabel.right - buttonWidth, (self.contentView.frame.size.height-buttonHeight)/2, buttonWidth, buttonHeight);
    
//    self.verifyButton setBackgroundImage:[UIImage imageWithColor:btnBgColor] forState:UICotnrolState
}

@end
