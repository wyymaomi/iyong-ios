//
//  SaveAndTake.m
//  xiangmu
//
//  Created by David kim on 16/4/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SaveAndTake.h"

@implementation SaveAndTake
+(id)take:(NSString *)keystr
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:keystr];
}
+(void)save:(NSString *)str forKey:(NSString *)keystr
{
    [[NSUserDefaults standardUserDefaults]setObject:str forKey:keystr];
}
@end
