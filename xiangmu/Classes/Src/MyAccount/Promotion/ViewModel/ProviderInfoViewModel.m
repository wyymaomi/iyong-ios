//
//  ProviderInfoViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderInfoViewModel.h"

@implementation ProviderInfoViewModel

-(NSMutableArray*)todayDetailList
{
    if (_todayDetailList == nil) {
        _todayDetailList = [NSMutableArray new];
    }
    return _todayDetailList;
}

//-(NSMutableArray*)advList
//{
//    if (_advList == nil) {
//        _advList = [NSMutableArray new];
//    }
//    return _advList;
//}

-(NSMutableArray*)orderList
{
    if (!_orderList) {
        _orderList = [NSMutableArray new];
    }
    return _orderList;
}

-(NSArray*)sectionRows
{
    if (!_sectionRows) {
        _sectionRows = @[@1, [NSNumber numberWithInteger:self.currentAdvRevenue.list.count], @1, [NSNumber numberWithInteger:self.orderList.count]];
    }
    return _sectionRows;
}

-(NSArray*)rowHeightList
{
    if (!_rowHeightList) {
//        _rowHeightList = @[@]
    }
    
    return _rowHeightList;
}

//-(CurrentAdvRevenue*)currentAdvRevenue
//{
//    if (!_currentAdvRevenue) {
//        _currentAdvRevenue = [CurrentAdvRevenue]
//    }
//}

-(id)init
{
    self = [super init];
    if (self) {
        self.sectionNum = 4;
    }
    return self;
}



@end
