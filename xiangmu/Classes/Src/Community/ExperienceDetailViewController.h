//
//  CommentDetailViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "SDCycleScrollView.h"
#import "UserExpericenViewModel.h"

#import "CommentPopupView.h"
#import "PullRefreshTableView.h"
#import "ExperienceCommentViewModel.h"
#import "CommentBodyView.h"

#define TableViewSectionNumber 3

typedef NS_ENUM(NSInteger, CommentCellSection)
{
    CommentCellSectionAdvertisement = 0,
    CommentCellSectionComment,
    CommentCellSectionFollowComment
};


@interface ExperienceDetailViewController : BaseViewController

@property (nonatomic, strong) SDCycleScrollView *bannerView;

@property (nonatomic, strong) UserExpericenViewModel *experienceViewModel;

@property (nonatomic, strong) CommentPopupView *commentPopupView; // 评论框
//@property (weak, nonatomic) IBOutlet UIButton *discussButton;
//@property (strong, nonatomic) PullRefreshTableView *tableView;
@property (nonatomic, strong) UIButton *commentButton;
@property (nonatomic, strong) NSMutableArray<ExperienceCommentViewModel*> *followCommentList;

@property (nonatomic, strong) UIImage *avatarIconImage;
@property (nonatomic, strong) UIImageView *shareImageView;



@end
