//
//  CommunityTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommunityPublishModel.h"

@interface CommunityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;// 发布者头像
@property (weak, nonatomic) IBOutlet UILabel *publishLabel;// 发布者名称和公司名
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;// 开始时间
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;// 结束时间
@property (weak, nonatomic) IBOutlet UIImageView *vechileImageView;// 车辆图片
@property (weak, nonatomic) IBOutlet UILabel *vechileNumberLabel;// 车辆牌照
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;//司机名字
@property (weak, nonatomic) IBOutlet UILabel *driverMobileLabel;//司机手机号
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;//位置
@property (weak, nonatomic) IBOutlet UILabel *elapseTimeLabel;//发布时间

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
-(void)initData:(CommunityPublishModel*)communityPublishModel;

@end
