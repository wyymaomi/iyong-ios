//
//  HomepageViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HomepageViewController.h"
#import "HomepageViewController+Advertisement.h"
#import "HomepageViewController+Location.h"
#import "AccountInfoView.h"
#import "OrderCountModel.h"
#import "AppDelegate.h"
#import "AppConfig.h"
#import "HomeBottomView.h"
#import "CommunityHelper.h"
#import "AWCollectionViewDialLayout.h"
#import "HomeCollectionViewCell.h"
#import "HomeCollectionView.h"

@interface HomepageViewController ()<AccountInfoViewDelegate,AccountInfoTableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UIGestureRecognizerDelegate>
{
    AWCollectionViewDialLayout *collectionViewLayout;
    CGFloat radius;// = 240;//radiusSlider.value * 1000;
    CGFloat angularSpacing;// = 30;//angularSpacingSlider.value * 90;
    CGFloat xOffset;// = 240;//xOffsetSlider.value * 320;
    CGFloat cell_width;// = 240;
    CGFloat cell_height;// = 100;
    //    CGFloat cell_height;
}

//@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) AccountInfoView *accountInfoView;

@property (nonatomic, strong) HomeBottomView *homeBottomView;

@property (nonatomic, strong) NSArray *vechileServiceList;// 用车服务列表

@property (nonatomic, strong) UIImageView *avatarImageView;

@property (weak, nonatomic) IBOutlet UIImageView *halfCircleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;



@end

static NSString *cellId = @"HomeCollectionCellId";

@implementation HomepageViewController




#pragma mark - life cycle

- (id)init
{
    self = [super init];
    
    if (self) {
        
        self.hasLeftBackButton = NO;
        self.isAccountInfoShow = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onLogout) name:kOnLogoutNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onUpdateUserinfo) name:kOnUserinfoUpdateNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(setNavTitleView)
                                                     name:kOnGpsLocationNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_isAccountInfoShow) {
        [self.accountInfoView show];
    }
    else {
        //        _isAccountInfoShow = NO;
        [self.accountInfoView dismiss];
    }
    
    if ([[UserManager sharedInstance] isLogin]) {
        [self updateAvatarImg];
    }
}

//- (void)awakeFromNib
//{
//    [super awakeFromNib];
//
//    self.homeBottomView.top = self.view.frame.size.height-54;
//}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    DLog(@"viewWillLayoutSubviews");
    
    self.homeBottomView.top = self.view.frame.size.height-54;
    
    self.bgImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    self.avatarImageView.top = (self.view.frame.size.height-self.avatarImageView.frame.size.height)/2;
    
    self.halfCircleImageView.top = (self.view.frame.size.height-self.halfCircleImageView.height)/2;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DLog(@"viewDidLoad");
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icon_adv"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(onClickAdvIcon)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icon_account"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(onClickAccount)];
    
//    self.showAdv = NO;
    [self showAdv:YES];
    
    [self setNavTitleView];
    
    [self initViews];
    
}

-(void)setNavTitleView
{
    
    NSString *defaultCityName = IsStrEmpty([AppConfig currentConfig].defaultCityName)?DEFAULT_CITY_NAME:[AppConfig currentConfig].defaultCityName;
    NSString *titleText = [NSString stringWithFormat:@"%@ >", defaultCityName];
    //    NSString *titleText = [NSString stringWithFormat:@"社区 · %@ >", defaultCityName];
    //    CGSize titleTextSize = [titleText textSize:CGSizeMake(ViewWidth, 30) font:BOLD_FONT(18)];
    
    //    CGFloat titleViewWidth = titleTextSize.width;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    self.navigationItem.titleView = view;
    
    UIImage *image=[[UIImage imageNamed:@"icon_iyong"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImageView *imageView=[[UIImageView alloc]initWithImage:image];
    imageView.width = 23;
    imageView.height = 26;
    imageView.left = view.width/2-35;
    imageView.top = 2;
    [view addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(view.width/2-5, 0, 5, 30)];
    label.text = @"|";
    label.textColor = UIColorFromRGB(0x64685A);//[UIColor darkGrayColor];
    label.font = FONT(30);
    label.backgroundColor = [UIColor clearColor];
    [view addSubview:label];
    
    UILabel *cityLabel = [[UILabel alloc] initWithFrame:CGRectMake(view.width/2+8, 0, view.width/2, 30)];
    cityLabel.text = titleText;
    cityLabel.font = BOLD_FONT(20);
    cityLabel.textColor = [UIColor whiteColor];
    //    cityLabel.backgroundColor = [UIColor blueColor];
    cityLabel.textAlignment = UITextAlignmentLeft;
    cityLabel.userInteractionEnabled = YES;
    [cityLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectCity:)]];
    [view addSubview:cityLabel];
    
}

- (void)initCollectionView
{
    
    //    CGFloat radius;// = 240;//radiusSlider.value * 1000;
    //    CGFloat angularSpacing;// = 30;//angularSpacingSlider.value * 90;
    //    CGFloat xOffset;// = 240;//xOffsetSlider.value * 320;
    //    CGFloat cell_width;// = 240;
    //    CGFloat cell_height;// = 100;
    
    radius = 240;
    angularSpacing = 30;
    xOffset = 240;
    cell_width = 240;
    cell_height = 100;
    
    
    collectionViewLayout = [[AWCollectionViewDialLayout alloc] initWithRadius:radius andAngularSpacing:angularSpacing andCellSize:CGSizeMake(cell_width, cell_height) andAlignment:WHEELALIGNMENTCENTER andItemHeight:cell_height andXOffset:xOffset];
    [collectionViewLayout setShouldSnap:YES];
    collectionViewLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, self.view.frame.size.height) collectionViewLayout:collectionViewLayout];
    _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    _collectionView.backgroundColor = [UIColor clearColor];
    //    [_collectionView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideBottomView:)]];
    [self.view addSubview:_collectionView];
    
    [_collectionView registerClass:[HomeCollectionViewCell class] forCellWithReuseIdentifier:cellId];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.scrollEnabled = YES;
    _collectionView.userInteractionEnabled = YES;
    
    cell_width = 200;
    cell_height = 80;
    [collectionViewLayout setCellSize:CGSizeMake(cell_width, cell_height)];
    [collectionViewLayout setWheelType:WHEELALIGNMENTLEFT];
    //    [collectionViewLayout setWheelType:WHEELALIGNMENTCENTER];
    [collectionViewLayout setShouldFlip:YES];
    
    //    radius = ViewWidth*2/3;
    DLog(@"ViewWidth = %ld", (long)ViewWidth);
    radius = 200 * (ViewWidth/320);
    angularSpacing = 20;
    
    xOffset = radius;
    if (iPhone5) {
        xOffset = xOffset - 30;
    }
//    else if (iPhone6P) {
//        xOffset = xOffset-10;
//    }
    else if (iPhone6) {
        xOffset = xOffset + 30;
    }
    
    DLog(@"radius = %ld, angularSpacing = %ld, xOffset = %ld", (long)radius, (long)angularSpacing, (long)xOffset);
    
    [collectionViewLayout setDialRadius:radius];
    [collectionViewLayout setAngularSpacing:angularSpacing];
    [collectionViewLayout setXOffset:xOffset];
    
    [_collectionView reloadData];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideBottomView:)];
    tapGestureRecognizer.delegate = self;
    [_collectionView addGestureRecognizer:tapGestureRecognizer];
    
}


- (void)initViews
{
    _isAccountInfoShow = NO;
    
    [self initCollectionView];
    
    // 底部
    [self.view addSubview:self.homeBottomView];
    self.homeBottomView.top = self.view.frame.size.height-54;
    self.homeBottomView.hidden = YES;
    [self.homeBottomView.inquiryButton addTarget:self action:@selector(doInquiry) forControlEvents:UIControlEventTouchUpInside];
    [self.homeBottomView.bidButton addTarget:self action:@selector(doBid) forControlEvents:UIControlEventTouchUpInside];
    
    // 头像
    [self.view addSubview:self.avatarImageView];
    self.avatarImageView.userInteractionEnabled = YES;
    [self.avatarImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoAccountInfo)]];
    
}

#pragma mark - 显示隐藏底部

-(void)showBottomView:(NSInteger)index;
{
    //    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showBottomView:) object:nil];
    
    //    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer *)sender;
    //    //    NSLog(@"%d",[singleTap view].tag]);
    //    DLog(@"%ld", (long)[singleTap view].tag);
    DLog(@"%ld", (long)index);
    
    
    NSDictionary *item = self.vechileServiceList[index];
    NSInteger tag = [item[@"type"] integerValue];
    [AppConfig currentConfig].serviceType = [NSNumber numberWithInteger:tag];
    DLog(@"tag = %ld", (long)tag);
    
    self.homeBottomView.hidden = NO;
    self.homeBottomView.top = self.view.frame.size.height;
    
    WeakSelf
    [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        weakSelf.homeBottomView.top = self.view.frame.size.height-54;
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideBottomView:(id)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideBottomView:) object:sender];
    
    if ([self.homeBottomView isHidden]) {
        return;
    }
    
    WeakSelf
    [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        weakSelf.homeBottomView.top = self.view.frame.size.height;
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - 退出登录

- (void)onLogout
{
    _isAccountInfoShow = NO;
    
}

#pragma mark - 重登录


-(void)onRecevieLoginOvertime
{
    _isAccountInfoShow = NO;
    [self.accountInfoView dismiss];
    
    [super onRecevieLoginOvertime];
    
}

#pragma mark - 询价

- (void)doInquiry
{
    
    _isAccountInfoShow = NO;
    
    self.nextAction = @selector(doInquiry);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    [self gotoPage:@"InquireViewController"];
    
}

#pragma mark - 报价

- (void)doBid
{
    _isAccountInfoShow = NO;
    
    self.nextAction = @selector(doBid);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    [self gotoPage:@"BusinessCircleListViewController" animated:YES title:@"业务圈"];
    
}


#pragma mark - 点击广告

- (void)onClickAdvIcon
{
    self.showAdv = YES;
    [self showAdv:NO];
}

#pragma mark - 点击侧滑帐户菜单

- (void)onClickAccount
{
    self.nextAction = @selector(onClickAccount);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    _isAccountInfoShow = YES;
    self.accountInfoView.isNeedAnimation = YES;
    [self.accountInfoView show];
    self.accountInfoView.accountTableView.tableViewDelegate = self;
    
    [self doOrderCountQuery];
    [self doGetWaitPassedPartner];
}

#pragma mark - AccountInfoTableView Delegate method

-(void)didSelectRow:(NSInteger)row;
{
    _isAccountInfoShow = YES;
    self.accountInfoView.isNeedAnimation = NO;
    
    if (row == AccountInfoTableRowAvatar) {
        [self.accountInfoView dismiss];
        [self gotoPage:@"UserInfoViewController"];
    }
    
//    if (row == AccountInfoTableRowMyAccount) {
//        [self.accountInfoView dismiss];
//        [self gotoPage:@"MyAccountViewController" animated:YES title:@"我的帐户"];
//    }
//    if (row == AccountInfoTableRowMyTravel) {
//        [self.accountInfoView dismiss];
//        [self gotoPage:@"OrderStatusListViewController" animated:YES title:@"我的行程"];
//    }
    if (row == AccountInfoTableRowSettings) {
        [self.accountInfoView dismiss];
        [self gotoPage:@"SettingViewController"];
    }
//    if (row == AccountInfoTableRowMyInquiry) {
//        [self.accountInfoView dismiss];
//        [self gotoPage:@"MyBusinessCircleViewController"];
//    }
    self.nextAction = nil;
}

- (void)onGotoPage:(NSString*)vcPage title:(NSString *)title
{
    [self.accountInfoView dismiss];
    _isAccountInfoShow = YES;
    self.accountInfoView.isNeedAnimation = NO;
    
    [self gotoPage:vcPage animated:YES title:title];
    self.nextAction = nil;
    
}

#pragma mark - 转向帐户页面

- (void)gotoAccountInfo
{
    _isAccountInfoShow = NO;
    self.nextAction = @selector(gotoAccountInfo);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    [self gotoPage:@"UserInfoViewController"];
    self.nextAction = nil;
    
}




#pragma mark - 订单统计

-(void)doOrderCountQuery
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_COUNT_API];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary *responseDict) {
        StrongSelf
        
        NSInteger code_status = [responseDict[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            NSDictionary *data = responseDict[@"data"];
            OrderCountModel *orderCountModel = [[OrderCountModel alloc] initWithDictionary:data error:nil];
            NSInteger arrangeCarCount = [orderCountModel.arrangeCar integerValue];
            NSInteger fillWaybillCount = [orderCountModel.fillWaybill integerValue];
            NSInteger notBargainCount = [orderCountModel.notBargain integerValue];
            NSInteger strokeCount = [orderCountModel.stroke integerValue];
            NSInteger unpaidCount = [orderCountModel.unpaid integerValue];
            NSInteger untreatedCount = [orderCountModel.untreated integerValue];
            
            NSInteger count = arrangeCarCount + fillWaybillCount + notBargainCount + strokeCount + unpaidCount + untreatedCount;
            [strongSelf.accountInfoView refreshOrderCount:count];
        }
    } andFailure:^(NSString *errorDesc) {
        
    }];
}


-(void)doGetWaitPassedPartner
{
    // 待通过合作伙伴
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Partner_WaitPass_COUNT_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
        StrongSelf
        NSInteger code = [responseData[@"code"] integerValue];
        if (code == STATUS_OK) {
            NSInteger dataCount = [responseData[@"data"] integerValue];
            [strongSelf.accountInfoView refreshPartnerNumber:dataCount];
        }
    } andFailure:^(NSString *errorDesc) {
        
    }];
}

#pragma mark - 首页更新用户信息通知
- (void)onUpdateUserinfo
{
    WeakSelf
    [[NetworkManager sharedInstance] getUserInfo:^(NSDictionary *responseData) {
        
        // 收到用户信息通知更新头像等信息
        [weakSelf updateAvatarImg];
        if (weakSelf.accountInfoView.isVisible) {
            [weakSelf.accountInfoView.accountTableView reloadData];
        }
        
    } andFailue:^(NSString *errorDesc) {
        
    }];
}

- (void)updateAvatarImg
{
//    WeakSelf
//    [self.avatarImageView downloadImage:[UserManager sharedInstance].userModel.logoUrl placeholderImage:[UIImage imageNamed:@"icon_avatar"] success:^(id responseData) {
//        weakSelf.avatarImageView.cornerRadius = weakSelf.avatarImageView.frame.size.width/2;
//    } andFailure:^(NSString *errorDesc) {
//        
//    }];
}

#pragma mark - didReceiveMemroyWarning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Delegate method

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.vechileServiceList.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    HomeCollectionViewCell *cell;
    cell = [cv dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    
    cell.userInteractionEnabled = YES;
    
    NSDictionary *item = [self.vechileServiceList objectAtIndex:indexPath.item];
    
    NSString *playerName = [item valueForKey:@"title"];
    UILabel *nameLabel = (UILabel*)[cell viewWithTag:101];
    [nameLabel setText:playerName];
    
    UIImageView *imgView = (UIImageView*)[cell viewWithTag:100];
    [imgView setImage:[UIImage imageNamed:item[@"icon"]]];
    
    
    return cell;
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self showBottomView:indexPath.row];
}

#pragma mark - UITapGestureRecognizer delegate method

-(BOOL)gestureRecognizer:(UIGestureRecognizer*)gestureRecognizer shouldReceiveTouch:(UITouch*)touch{
    if (touch.view != _collectionView) {
        return NO;
    }
    
    return YES;
}

#pragma mark - getter and setter

- (NSArray *)vechileServiceList
{
    if (IsNilOrNull(_vechileServiceList)) {
        CommunityHelper *helper = [CommunityHelper new];
        _vechileServiceList = [helper vechileServiceList];
    }
    return _vechileServiceList;
}

- (AccountInfoView*)accountInfoView
{
    if (!_accountInfoView) {
        _accountInfoView = [[AccountInfoView alloc] init];
        _accountInfoView.delegate = self;
        _accountInfoView.isNeedAnimation = YES;
        _accountInfoView.isVisible = NO;
    }
    return _accountInfoView;
}

-(HomeBottomView*)homeBottomView
{
    if (!_homeBottomView) {
        _homeBottomView = [[HomeBottomView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-54, ViewWidth, 54)];
    }
    return _homeBottomView;
}

-(UIImageView*)avatarImageView;
{
    if (_avatarImageView == nil) {
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ViewWidth-120+14, (self.view.frame.size.height-120)/2, 120, 120)];
        _avatarImageView.image = [UIImage imageNamed:@"icon_avatar"];
    }
    return _avatarImageView;
}


@end
