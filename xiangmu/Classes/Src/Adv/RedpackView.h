//
//  RedpackView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedpackDAO.h"

@interface RedpackView : UIView

@property (nonatomic, strong) UILabel *moneyLabel;

@property (nonatomic, strong) UIButton *shareButton;

@property (nonatomic, strong) UIImageView *backgroundImageView;

@property (nonatomic, strong) UIImageView *shareBackgroundImageView;

@property (nonatomic, strong) RedpackDAO *redpackDAO;

@property (nonatomic, strong) NSString *shareUrl;

@end
