//
//  ImageCache.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kCacheAgeForever      (-1.)
#define kCacheAgeOneLifeCycle (-2.)
#define kCacheAgeDefault      (60. * 60. * 24. * 7.)  //a week

@interface FileCache : NSObject
{
    NSString*               _cachePath;
    dispatch_queue_t        _ioQueue;
    NSInteger               _maxCacheAge;
    NSMutableDictionary*    _cacheDictionary;
}

@property (nonatomic, strong) NSString *cachePath;

+(instancetype)defaultCache;

- (BOOL)hasCached:(NSString *)key;

//data
- (NSData *)dataForKey:(NSString *)key;
- (void)saveData:(NSData *)data forKey:(NSString *)key;
- (void)saveData:(NSData *)data forKey:(NSString *)key cacheAge:(NSTimeInterval)age;

//string
- (NSString *)stringForKey:(NSString *)key;
- (void)saveString:(NSString *)string forKey:(NSString *)key;
- (void)saveString:(NSString *)string forKey:(NSString *)key cacheAge:(NSTimeInterval)age;

//array
- (NSArray *)arrayForKey:(NSString *)key;
- (void)saveArray:(NSArray *)array forKey:(NSString *)key;
- (void)saveArray:(NSArray *)array forKey:(NSString *)key cacheAge:(NSTimeInterval)age;

- (NSString *)cachePathForKey:(NSString *)key;

- (void)removeDataForKey:(NSString *)key;
- (void)clearDisk;  //清空缓存
- (void)cleanDisk;  //清除过期的缓存

//Get the size used by the disk cache
- (unsigned long long)cacheSize;



@end
