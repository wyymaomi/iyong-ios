//
//  CommunityCateogryTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityCateogryTableViewCell.h"

@implementation CommunityCateogryTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    
    static NSString *ID =  @"CommunityCateogryTableViewCell";
    CommunityCateogryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
    }
    
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
