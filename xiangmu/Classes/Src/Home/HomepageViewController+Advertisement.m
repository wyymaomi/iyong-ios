//
//  HomepageViewController+Advertisement.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HomepageViewController+Advertisement.h"
//#import "BaseWebViewController.h"
#import "BaseWebpageViewController.h"
#import "AdvModel.h"
#import "AdvDAO.h"
#import "AppConfig.h"

@implementation HomepageViewController (Advertisement)

#pragma mark - 广告页面

- (void)showAdv:(BOOL)bAll
{
    WeakSelf
//    NSString *time;
    
//    if (bAll) {
//        time = @"";
//    }
//    else {
//        time = [StringUtil getSafeString:[AppConfig currentConfig].advLastUpdateStr];
//    }
//    NSString *time = ([AppConfig currentConfig].advLastUpdateStr == nil)?@"":[AppConfig currentConfig].advLastUpdateStr;
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ADV_LIST_URL];
    NSDictionary *params = @{/*@"time":time,*/
                             @"areaCode":[AppConfig currentConfig].defaultCityCode};
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        StrongSelf
        if (code_status == STATUS_OK) {

            AdvResponseData *responseData = [[AdvResponseData alloc] initWithDictionary:result error:nil];
//            if (IsArrEmpty(responseData.data)) {
//                [strongSelf onClickAdvIcon];
//                return;
//            }
            if (IsArrEmpty(responseData.data)) {
                return;
            }
            
            NSString *currentDate = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
            DLog(@"currentDate = %@", currentDate);
            [AppConfig currentConfig].advLastUpdateStr = currentDate;
            
            if (strongSelf.adView == nil) {
                strongSelf.adView = [SIDADView new];
                strongSelf.adView.delegate = strongSelf;
            }
//            if (self.showAdv) {
            [strongSelf.adView showAdv:responseData.data];
//                [strongSelf.adView show:responseData.data borderColor:[UIColor clearColor]];
//            }
//            strongSelf.advList = responseData.data;
//            AdvDAO *advDAO = [AdvDAO new];
//            [advDAO updateAdvList:responseData.data];
//            
//            double maxUpdateTime = [strongSelf getMaxUpdateTime:strongSelf.advList];
//            [AppConfig currentConfig].advLastUpdateStr = [NSString stringWithFormat:@"%.f", (double)maxUpdateTime];
            
        }
        else if (code_status == NETWORK_FAILED)
        {
            //            [weakSelf showAlertView:MSG_GET_VERIFY_FAILURE];
        }
        else {
//            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
        
    }];
    
    //    }
}

-(double)getMaxUpdateTime:(NSArray*)dataList
{
    
    double maxUpdateTime = 0L;
    for (AdvModel *adv in dataList) {
        if ([adv.updateTime doubleValue] > maxUpdateTime) {
            maxUpdateTime = [adv.updateTime doubleValue];
        }
    }
    return maxUpdateTime;
}

- (void)onClickAdvIcon;
{
    
    AdvDAO *advDAO = [AdvDAO new];
    NSArray *localAdvList = [advDAO getAllAdv];
    
    if (localAdvList.count == 0) {
        [self showAdv:YES];
        return;
    }
    
    if (self.adView == nil) {
        self.adView = [SIDADView new];
        self.adView.delegate = self;
    }
    [self.adView showAdv:localAdvList];
//    [self.adView show:localAdvList borderColor:[UIColor clearColor]];
    self.advList = localAdvList;
    
}




- (void)onClickAdv:(NSInteger)index;
{
    //    NSDictionary *dict = self.advList[index];
    
    AdvModel *advModel = self.advList[index];
    
    if (IsStrEmpty(advModel.linkUrl.trim)) {
        return;
    }

    [self gotoWebpage:advModel.linkUrl];
    
//    BaseWebpageViewController *viewController = [[BaseWebpageViewController alloc] init];
//    viewController.url = advModel.linkUrl;
//    viewController.enter_type = ENTER_TYPE_PUSH;
//    viewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:viewController animated:YES];
    
//    self.showAdv  = NO;
//    self.isFirstIn = NO;
    [self.adView dismiss];
}

- (void)onDismiss
{
//    self.showAdv = NO;
//    self.isFirstIn = NO;
    [self.adView dismiss];
}

#pragma mark - getter and setter

-(void)setAdView:(SIDADView *)adView
{
    objc_setAssociatedObject(self, &kAdView, adView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(SIDADView*)adView
{
    return objc_getAssociatedObject(self, &kAdView);
}

-(void)setAdvList:(NSArray *)advList
{
    objc_setAssociatedObject(self, &kAdvList, advList, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSArray*)advList
{
    return objc_getAssociatedObject(self, &kAdvList);
}

@end
