//
//  SaveAndTake.h
//  xiangmu
//
//  Created by David kim on 16/4/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaveAndTake : NSObject
+(void)save:(NSString *)str forKey:(NSString *)keystr;
+(id)take:(NSString *)keystr;
@end
