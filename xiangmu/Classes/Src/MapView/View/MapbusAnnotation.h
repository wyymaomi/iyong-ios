//
//  MapbusAnnotation.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/11/15.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKMapComponent.h>

// 地图上车辆Annotation

@interface MapbusAnnotation : BMKPointAnnotation

@property (nonatomic, strong) NSString *entityName;
@property (nonatomic, assign) CGFloat speed; // 速度
@property (nonatomic, assign) CGFloat direction;// 角度
@property (nonatomic, assign) CGFloat radius; // 精度
@property (nonatomic, assign) CGFloat height; // 高度


@property (nonatomic, strong) NSMutableArray<CLLocation*> *coordinateList; // 坐标数组
@property (nonatomic, assign) NSUInteger currentIndex;
@property (nonatomic, strong) NSArray<CLLocation*> *moveArray;
@property (nonatomic, assign) BOOL movingOver;

@property (nonatomic, strong) BMKAnnotationView *annotationView;
@property (nonatomic, strong) UIImage *annnationImage;

-(void)addLocation:(CLLocationCoordinate2D)coordinate;
-(void)startAnimation;

//获取该RouteAnnotation对应的BMKAnnotationView
- (BMKAnnotationView*)getRouteAnnotationView:(BMKMapView *)mapview;

@end
