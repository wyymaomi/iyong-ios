//
//  MyBusinessCircleViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, AlertViewType)
{
    AlertViewTypeCredit=0,
    AlertViewTypeAccount,
    AlertViewTypeNoMoney
};

@interface MyBusinessCircleViewController : BaseViewController

@end
