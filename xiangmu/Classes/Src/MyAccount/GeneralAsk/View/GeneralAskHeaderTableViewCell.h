//
//  GeneralAskHeaderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralAskHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *withDrawView;
@property (weak, nonatomic) IBOutlet UIView *rechargeView;
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
