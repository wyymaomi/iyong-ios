//
//  CarTravelTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CarTravelTableViewCell.h"
#import "VechileOrderManager.h"


@implementation CarTravelTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"CarTravelTableViewCell";
    CarTravelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!_model) {
        return;
    }
    
    NSUInteger orderStatus = [_model.orderStatus integerValue];
    switch(orderStatus) {
        case CarOrderStatusWaitReceive:// 待接单
            self.cancelOrderButton.hidden = NO;
            self.contactButton.hidden = YES;
            self.payButton.hidden = YES;
            
            self.cancelOrderButton.left = ViewWidth-80;
            
            break;
        case CarOrderStatusPendingPay:// 待支付
            self.cancelOrderButton.hidden = NO;
            self.contactButton.hidden = YES;
            self.payButton.hidden = NO;
            
            self.cancelOrderButton.left = ViewWidth-80;
            self.payButton.left = self.cancelOrderButton.left-80;
            
            break;
        case CarOrderStatusTravelStarted:// 已开始行程
//            self.statusLabel.text = @"已开始...";
            self.cancelOrderButton.hidden = YES;
            self.contactButton.hidden = NO;
            self.payButton.hidden = YES;
            
            self.contactButton.left = ViewWidth-80;
            break;
        case CarOrderStatusTravelWaitStart:
//            self.statusLabel.text = @"未开始...";
            self.cancelOrderButton.hidden = NO;
            self.contactButton.hidden = NO;
            self.payButton.hidden = YES;
            
            self.cancelOrderButton.left = ViewWidth-80;
            self.contactButton.left = self.cancelOrderButton.left-80;
            break;
        case CarOrderStatusTravelComplete:// 已经完成
            self.buttonsView.hidden = YES;
//            self.statusLabel.text = @"已完成...";
//            self.cancelOrderButton.hidden = YES;
//            self.contactButton.hidden = YES;
//            self.payButton.hidden = YES;
    }
    
}


-(void)refreshStatusButton
{
    
}

-(void)initData:(TravelOrderModel*)model
{
    if (!model) {
        return;
    }
    
    _model = model;
    
    // 出发日期
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:[model.startTime doubleValue]/1000];
//    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:[model]]
    self.startDateTimeLabel.text = [startDate stringWithDateFormat:@"MM月dd日 HH:mm"];
    
    // 地点
    self.startAddressLabel.text = model.startLocation;
    self.endAddressLabel.text = model.endLocation;
    
    NSUInteger orderStatus = [model.orderStatus integerValue];
    switch(orderStatus) {
        case CarOrderStatusWaitReceive:
            self.statusLabel.text = @"待接单...";
            self.vechileInfoLabel.hidden = NO;
            self.buttonsView.top = self.priceLabel.bottom+10;
            break;
        case CarOrderStatusPendingPay:
            self.statusLabel.text = @"待支付...";
            self.vechileInfoLabel.text = [NSString stringWithFormat:@"%@ %@", model.carModel,model.carNumber];
            self.buttonsView.top = self.vechileInfoLabel.bottom+10;
            break;
        case CarOrderStatusTravelStarted:
            self.statusLabel.text = @"已开始";
            self.vechileInfoLabel.text = [NSString stringWithFormat:@"%@ %@", model.carModel, model.carNumber];
            self.buttonsView.top = self.vechileInfoLabel.bottom+10;
            break;
        case CarOrderStatusTravelWaitStart:
            self.statusLabel.text = @"未开始";
            self.vechileInfoLabel.text = [NSString stringWithFormat:@"%@ %@", model.carModel, model.carNumber];
            self.buttonsView.top = self.vechileInfoLabel.bottom + 10;
            break;
        case CarOrderStatusTravelComplete:
            self.statusLabel.text = @"已完成";
            self.vechileInfoLabel.text = [NSString stringWithFormat:@"%@ %@", model.carModel, model.carNumber];
            self.buttonsView.top = self.vechileInfoLabel.bottom + 10;
            break;
        case CarOrderStatusTravelCancelled:
            self.statusLabel.text = @"已取消";
            self.vechileInfoLabel.hidden = YES;
            self.buttonsView.hidden = YES;
            break;
        case CarOrderStatusPayExpire:
            self.statusLabel.text = @"支付已过期";
            self.vechileInfoLabel.hidden = YES;
            self.buttonsView.hidden = YES;
            break;
        case CarOrderStatusPayInConfirming:
            self.statusLabel.text = @"支付确认中";
            self.vechileInfoLabel.hidden = NO;
            self.buttonsView.hidden = YES;
    }
    
    
    NSString *vechileModel=@"";
    if (orderStatus == CarOrderStatusWaitReceive) {
//        NSArray *carType = @[@"舒适型", @"商务型", @"豪华型"];
//        vechileModel = carType[[model.carType integerValue]-1];
        NSArray *carDescList = [[VechileOrderManager sharedInstance] getVechileDescTypeList];
        
        for (CarDescModel *carDesc in carDescList) {
            
            if ([carDesc.type integerValue] == [model.carType integerValue]) {
//                cell.addressTextField.text = [NSString stringWithFormat:@"%@%@", carDesc.carTypeName, carDesc.seatName];
                vechileModel = carDesc.carTypeName;
                break;
            }
            
        }
    }
    else {
//        vechileModel = [NSString stringWithFormat:@"%@(%@)",model.carModel, @"沪MN8888"];
    }
    NSString *carPrice = [model.amount stringValue];
    NSString *yuan = @"元";
    NSString *distance = [NSString stringWithFormat:@"(共计：%.f公里)", [model.mileage floatValue]];//@"(共计：18公里)";
    self.priceLabel.text = [NSString stringWithFormat:@"一口价 %@%@ %@ %@", carPrice, yuan, distance, [StringUtil getSafeString:vechileModel]];
    NSString *priceText = self.priceLabel.text;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:priceText];
    [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0x515151),
                                      NSFontAttributeName:FONT(15)}
                              range:[priceText rangeOfString:vechileModel]];
    [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xe06430),
                                      NSFontAttributeName:BOLD_FONT(18)}
                              range:[priceText rangeOfString:carPrice]];
    [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xe06430),
                                      NSFontAttributeName:FONT(12)}
                              range:[priceText rangeOfString:yuan]];
    [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xe06430),
                                      NSFontAttributeName:FONT(15)}
                              range:[priceText rangeOfString:distance]];
    self.priceLabel.attributedText = attributedString;
    
    [self setNeedsLayout];
    
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(CGFloat)getCellHeight
{
    return 240;
//    return 216;
//    return 175;
}

@end
