//
//  ProviderOrderListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderOrderListViewController.h"
#import "ProviderAdvTableViewCell.h"
#import "ProviderOrderListViewController+NetworkRequest.h"
//#import "ProviderInfoViewController+NetworkRequest.h"

@interface ProviderOrderListViewController ()

@end

@implementation ProviderOrderListViewController

//-(ProviderOrde)

-(ProviderInfoViewModel*)viewModel
{
    if (!_viewModel) {
        _viewModel = [ProviderInfoViewModel new];
    }
    return _viewModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单列表";
    [self.view addSubview:self.pullRefreshTableView];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.pullRefreshTableView beginHeaderRefresh];
}

- (void)onHeaderRefresh
{
    [self fetchProviderAdOrderListWithDetail:nil];
}

-(void)onFooterRefresh
{
    if (self.viewModel.orderList.count == 0) {
        return;
    }
    
    ProviderOrderModel *model = [self.viewModel.orderList lastObject];
    if (model) {
        NSString *time = model.createTime;
        [self fetchProviderAdOrderListWithDetail:time];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //    return self.viewModel.sectionNum;
//    return 4;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    return [self.viewModel.sectionRows[section] integerValue];
//    NSArray *sectionRows = @[@1, [NSNumber numberWithInteger:self.viewModel.currentAdvRevenue.list.count], @1, [NSNumber numberWithInteger:self.viewModel.orderList.count]];
//    return [sectionRows[section] integerValue];
    return self.viewModel.orderList.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == ProviderInfoSectionBalance) {
//        return [ProviderBalanceTableViewCell getCellHeight];
//    }
//    else if (indexPath.section == ProviderInfoSectionTodayDetail){
//        return [TodayPromotionDetailTableViewCell getCellHeight];
//    }
//    else if (indexPath.section == ProviderInfoSectionRank) {
//        return [ProviderRankTableViewCell getCellHeight];
//    }
//    else if (indexPath.section == ProviderInfoSectionAdvList) {
//        return [ProviderAdvTableViewCell getCellHeight];
//    }
//    return 60;
    return [ProviderAdvTableViewCell getCellHeight];
}


//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if (section == ProviderInfoSectionTodayDetail) {
//        return 30;
//    }
//    //    if (section == ProviderInfoSectionAdvList) {
//    //        return 5;
//    //    }
//    return 0.001f;
//}

//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    if (section == ProviderInfoSectionTodayDetail) {
//        CurrentPromotionDetailHeaderView *headerView = [CurrentPromotionDetailHeaderView customView];
//        return headerView;
//    }
//    return [UIView new];
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    if (section == ProviderInfoSectionTodayDetail) {
//        return 30;
//    }
//    if (section == ProviderInfoSectionAdvList) {
//        if (self.viewModel.orderList.count > 0) {
//            return 50;
//        }
//    }
//    
//    return 0.0001f;
//}



//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    
//    if (section == ProviderInfoSectionTodayDetail) {
//        CurrentPromotionDetailFooterView *footerView = [CurrentPromotionDetailFooterView customView];
//        footerView.clickNumLabel.text = [self.viewModel.currentAdvRevenue.thumbsCountTotal stringValue];
//        footerView.viewNumLabel.text = [self.viewModel.currentAdvRevenue.viewCountTotal stringValue];
//        footerView.totalPriceLabel.text = [self.viewModel.currentAdvRevenue.amountTotal stringValue];
//        return footerView;
//    }
//    if (section == ProviderInfoSectionAdvList && self.viewModel.orderList.count > 0) {
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 35)];
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        button.frame = CGRectMake(10, 5, ViewWidth-20, 40);
//        button.backgroundColor = [UIColor whiteColor];
//        [button setTitle:@"查看更多..." forState:UIControlStateNormal];
//        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//        button.titleLabel.font = FONT(12);
//        [button addTarget:self action:@selector(onClickMoreButton:) forControlEvents:UIControlEventTouchUpInside];
//        [view addSubview:button];
//        return view;
//    }
//    
//    //    UIView *view = [UIView new];
//    //    view.backgroundColor = [UIColor lightGrayColor];
//    //    return view;
//    return [UIView new];
//    
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if (indexPath.section == ProviderInfoSectionBalance) {
//        ProviderBalanceTableViewCell *cell = [ProviderBalanceTableViewCell cellWithTableView:tableView indexPath:indexPath];
//        //        cell.currentPromotionAccountLabel.text = self.viewModel.current
//        cell.currentPromotionAccountLabel.text = [NSString stringWithFormat:@"%.2f元", [self.currentPomotionAccountMoney floatValue]];
//        cell.promotionBalanceLabel.text = [NSString stringWithFormat:@"%.2f元", [self.promotionAccountMoney floatValue]];
//        cell.accountBalanceLabel.text = [NSString stringWithFormat:@"%.2f元", [self.myAccountMoney floatValue]];
//        
//        return cell;
//    }
//    else if (indexPath.section == ProviderInfoSectionTodayDetail) {
//        TodayPromotionDetailTableViewCell *cell = [TodayPromotionDetailTableViewCell cellWithTableView:tableView indexPath:indexPath];
//        cell.hidden = (self.viewModel.currentAdvRevenue.list.count==0)?YES:NO;
//        AdvRevenueDetail *data = self.viewModel.currentAdvRevenue.list[indexPath.row];
//        cell.timeSegmentLabel.text = [NSString stringWithFormat:@"%@:00/%@:00", data.startHour, data.endHour];
//        cell.clickNumLabel.text = [data.viewCount stringValue];
//        cell.thumbUpLabel.text = [data.thumbsUpCount stringValue];
//        cell.priceLabel.text = [data.costs stringValue];
//        return cell;
//    }
//    else if (indexPath.section == ProviderInfoSectionRank) {
//        ProviderRankTableViewCell *cell = [ProviderRankTableViewCell cellWithTableView:tableView indexPath:indexPath];
//        
//        ProviderRankSumModel *rankSumModel = self.viewModel.rankSumModel;
//        
//        if (rankSumModel) {
//            // 全国排名
//            // 今天
//            cell.todayRankLabel.text = [NSString stringWithFormat:@"NO.%@", rankSumModel.rankingToday.viewRanking];
//            cell.todayViewNumDiffLabel.text = [NSString stringWithFormat:@"%ld", (long)abs([rankSumModel.rankingToday.viewDiff integerValue])];// rankSumModel.rankingToday.viewDiff;
//            BOOL isTodayUpArrow = [rankSumModel.rankingToday.viewDiff integerValue] > 0;
//            cell.todayArrowLabel.text = isTodayUpArrow?@"↑":@"↓";
//            cell.todayArrowLabel.textColor = isTodayUpArrow?[UIColor redColor]:NAVBAR_BGCOLOR;
//            // 昨天
//            cell.yesterdayRankLabel.text = [NSString stringWithFormat:@"NO.%@", rankSumModel.rankingYestoday.viewRanking];
//            cell.yesterdayViewNumDiffLabel.text = [NSString stringWithFormat:@"%ld", (long)abs([rankSumModel.rankingYestoday.viewDiff integerValue])];//self.viewModel.rankSumModel.rankingYestoday.viewDiff;
//            BOOL isYesterdayUpArrow = [rankSumModel.rankingYestoday.viewDiff integerValue] > 0;
//            cell.yesterdayArrowLabel.text = isYesterdayUpArrow?@"↑":@"↓";
//            cell.yesterdayArrowLabel.textColor = isYesterdayUpArrow?[UIColor redColor]:NAVBAR_BGCOLOR;
//            
//            // 区域排名
//            // 今天
//            cell.todayAreaRankLabel.text = [NSString stringWithFormat:@"NO.%@", rankSumModel.rankingToday.viewRankingArea];
//            cell.todayAreaViewNumDiffLabel.text = [NSString stringWithFormat:@"%ld", (long)abs([rankSumModel.rankingToday.viewDiffArea integerValue])];
//            BOOL isTodayAreaUpArrow = [rankSumModel.rankingToday.viewDiffArea integerValue]>0;
//            cell.todayAreaArrowLabel.text = isTodayAreaUpArrow?@"↑":@"↓";
//            cell.todayAreaArrowLabel.textColor = isTodayAreaUpArrow?[UIColor redColor]:NAVBAR_BGCOLOR;
//            // 昨天
//            cell.yesterdayAreaRankLabel.text = [NSString stringWithFormat:@"NO.%@", rankSumModel.rankingYestoday.viewRankingArea];
//            cell.yesterdayAreaViewNumDiffLabel.text = [NSString stringWithFormat:@"%ld", (long)abs([rankSumModel.rankingYestoday.viewDiffArea integerValue])];//rankSumModel.rankingYestoday.viewDiffArea;
//            BOOL isYesterdayAreaUpArrow = [rankSumModel.rankingYestoday.viewDiffArea integerValue]>0;
//            cell.yesterdayAreaArrowLabel.text = isYesterdayUpArrow?@"↑":@"↓";
//            cell.yesterdayAreaArrowLabel.textColor = isYesterdayAreaUpArrow?[UIColor redColor]:NAVBAR_BGCOLOR;
//        }
//        
//        return cell;
//    }
//    else if (indexPath.section == ProviderInfoSectionAdvList) {
    
        ProviderAdvTableViewCell *cell = [ProviderAdvTableViewCell cellWithTableView:tableView indexPath:indexPath];
        
        ProviderOrderModel *order = self.viewModel.orderList[indexPath.row];
        
        [cell initData:order];
        
        cell.deleteButton.tag = indexPath.row;
        
        cell.payButton.tag = indexPath.row;
        
        cell.cancelButton.tag = indexPath.row;
        
        [cell.payButton addTarget:self action:@selector(continuePayOrder:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.cancelButton addTarget:self action:@selector(cancelOrder:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.deleteButton addTarget:self action:@selector(deleteOrder:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
//    }
    //    if (indexPath.section == 0) {
    //
    //        AdvAccountPayTableViewCell *cell = [AdvAccountPayTableViewCell cellWithTableView:tableView indexPath:indexPath];
    //        cell.radioButton.selected = indexPath.row == self.payType ? YES : NO;
    //        if (indexPath.row == 0) {
    //            cell.amountLabel.text = [NSString stringWithFormat:@"%.1f元", [self.promotionAccountMoney floatValue]];
    //        }
    //        else {
    //            cell.amountLabel.text = [NSString stringWithFormat:@"%.1f元", [self.myAccountMoney floatValue]];
    //        }
    //        return cell;
    //    }
    //    if (indexPath.section == [tableView numberOfSections]-1) {
    //
    //        AdvPayTableViewCell *cell = [AdvPayTableViewCell cellWithTableView:tableView indexPath:indexPath];
    //
    //        cell.radioButton.selected = indexPath.row == self.payType-2 ? YES : NO;
    //        
    //        return cell;
    //        
    //    }
//    return nil;
    
}


@end
