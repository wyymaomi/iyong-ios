//
//  TravelModel.h
//  iYONGDriver
//
//  Created by 湛思科技 on 2017/7/7.
//  Copyright © 2017年 Shanghai Zhansi Tech. All rights reserved.
//

#import <JSONModel/JSONModel.h>

//id 行程ID
//userId 用户ID
//companyId 公司ID
//startTime 开始时间(时间戳：毫秒)
//endTime 结束时间(时间戳：毫秒)
//startLocation 开始地点
//endLocation 结束地点
//travel 当前行程公里数

@protocol TravelModel <NSObject>

@end

@interface TravelModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;//行程ID
@property (nonatomic, strong) NSString<Optional> *userId; // 用户ID
@property (nonatomic, strong) NSString<Optional> *companyId; // 公司ID
@property (nonatomic, strong) NSNumber<Optional> *startTime; // 开始时间（时间戳：毫秒)
@property (nonatomic, strong) NSNumber<Optional> *endTime; // 结束时间（时间戳：毫秒）
@property (nonatomic, strong) NSString<Optional> *startLocation; // 开始地点
@property (nonatomic, strong) NSString<Optional> *endLocation; // 结束地点
@property (nonatomic, strong) NSNumber<Optional> *travel; // 当前行程公里数
@property (nonatomic, strong) NSNumber<Optional> *mileage; // 当前行程公里数

@end

@interface TravelModelResponse : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<Optional, TravelModel> *data;

@end
