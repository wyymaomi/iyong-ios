//
//  PublishHeaderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/7.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"

@interface PublishHeaderTableViewCell : UITableViewCell

@property (nonatomic, strong) HeaderView *headerView;

@property (nonatomic, strong) UIView *lineView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

@end
