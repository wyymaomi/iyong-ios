//
//  ServiceProviderViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "OnlineServiceModel.h"
@class OnlineServiceModel;

@interface ServiceProviderViewModel : NSObject

/**
 *  数据模型
 */
@property (nonatomic ,strong) OnlineServiceModel *serviceModel;

/**
 *  主体Frame
 */
@property (nonatomic ,assign) CGRect momentsBodyFrame;

@property (nonatomic, assign) CGRect bodyCompanyFrame;
//昵称Frame
@property (nonatomic ,assign) CGRect bodyNameFrame;
//头像Frame
@property (nonatomic ,assign) CGRect bodyIconFrame;
//时间Frame
@property (nonatomic ,assign) CGRect bodyTimeFrame;
//正文Frame
@property (nonatomic ,assign) CGRect bodyTextFrame;
//图片Frame
@property (nonatomic ,assign) CGRect bodyPhotoFrame;

/**
 *  工具条Frame
 */
@property (nonatomic, assign) CGRect momentsToolBarFrame;

//点赞Frame
@property (nonatomic ,assign) CGRect toolLikeFrame;
//收藏Frame
@property (nonatomic ,assign) CGRect toolFavoriteFrame;
// 通话Frame
@property (nonatomic, assign) CGRect toolTelFrame;

/**
 *  cell高度
 */
@property (nonatomic ,assign) CGFloat cellHeight;



@end
