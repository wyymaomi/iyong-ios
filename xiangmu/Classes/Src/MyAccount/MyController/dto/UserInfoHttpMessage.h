//
//  UserInfoHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/10.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface UserInfoHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *nickname;   // 昵称
@property (nonatomic, strong) NSString *companyName; // 公司名称
//@property (nonatomic, strong) NSString *fileContent; // 上传头像
@property (nonatomic, strong) NSString *logoUrl; // 头像URL

@end
