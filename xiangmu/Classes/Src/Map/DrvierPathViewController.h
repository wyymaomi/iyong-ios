//
//  DrvierPathViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "TrackHistoryModel.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>

@class SportAnnotationView;


@interface DrvierPathViewController : BaseViewController
{
    BMKPolygon *pathPloygon;
    BMKPointAnnotation *sportAnnotation;
    SportAnnotationView *sportAnnotationView;
    
    NSMutableArray *sportNodes;//轨迹点
    NSInteger sportNodeNum;//轨迹点数
    NSInteger currentIndex;//当前结点
}

@property (strong, nonatomic) BMKMapView *mapView;

@property (nonatomic, strong) NSDictionary *trackHistoryDict;

@property (nonatomic, strong) TrackHistoryModel *trackHistoryModel;


@end
