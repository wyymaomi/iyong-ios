//
//  InvoiceSettingItemCellTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/1.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"

@interface InvoiceSettingItemCellTableViewCell : CustomTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *invoiceTItleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactAddressLabel;

@end
