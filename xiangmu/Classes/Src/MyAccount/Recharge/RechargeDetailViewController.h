//
//  RechargeDetailViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface RechargeDetailViewController : BaseViewController

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, strong) NSString *bankcardInfo;
@property (nonatomic, strong) NSString *transAmount;

@end
