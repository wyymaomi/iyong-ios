//
//  ThirdPayNoteViewController.m
//  xiangmu
//
//  Created by David kim on 16/9/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ThirdPayNoteViewController.h"

@interface ThirdPayNoteViewController ()

@end

@implementation ThirdPayNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"费用说明";
    
    NSString *str1 = @"1、清洁费：如客户在用车时发生呕吐，踩蹬等情况造成车辆脏乱的，客户需要额外支付清洁费：100/次\n\n";
    NSString *str2 = @"2、差旅费：连续多日用车订单，驾驶员需要陪同在异地过夜的情况下，客户需按300元/晚的标准支付食宿费；连续多日订单但在同城内，驾驶员可回家休息的情况下，该费用客户不用支付；\n\n";
    NSString *str3 = @"3、路桥费、停车费：用车过程中产生的路桥费(含高速费)、停车费等费用，由客户自行支付，客户要求驾驶员垫付的，按照实际金额2倍在余额中扣除；";
    NSString *str = [NSString stringWithFormat:@"%@%@%@", str1, str2, str3];

//    CGSize size = [str textSize:CGSizeMake(ViewWidth-40, 10000) font:FONT(14)];
    CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(ViewWidth-40,10000.0f)lineBreakMode:UILineBreakModeWordWrap];
    
    NSUInteger x = 20;
    NSUInteger y = 20;
    UILabel *lb = [[UILabel alloc]initWithFrame:CGRectMake(x,y,size.width,size.height)];
    lb.numberOfLines = 0; // 最关键的一句
    lb.text = str;
    lb.textColor = [UIColor darkGrayColor];
    lb.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:lb];
//    [lb release];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
