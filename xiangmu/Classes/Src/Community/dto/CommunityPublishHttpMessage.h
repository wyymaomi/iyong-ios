//
//  CommunityPublishModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface CommunityPublishHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *carId;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *endTime;
@property (nonatomic, strong) NSString *seat;

@end
