//
//  AddBankcardNumberTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AddBankcardNumberTableViewCell.h"

@implementation AddBankcardNumberTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath
{
    static NSString *ID = @"AddBankcardNumberTableViewCell";
    AddBankcardNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
