//
//  InputHelperViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "InputHelperViewController.h"
#import "UserHelper.h"
#import "VechileUtils.h"
//#import "ABRootViewController.h"
//#import "CNContactRootViewController.h"
#import "ContactListService.h"
#import "InputHelperViewController+NetworkRequest.h"

@interface InputHelperViewController ()<UITextFieldDelegate,/*ABRootViewControllerDelegate, CNContactRootViewControllerDelegate,*/ ContactListServiceDelegate>

@end

@implementation InputHelperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    self.textFieldBackgroundView.height = DefaultTableRowHeight;
//    self.textField.height = DefaultTableRowHeight-self.textField.top*2;
//    self.textField.textColor = DefaultTableRowTitleTextColor;
//    self.textField.font = DefaultTableRowTitleFont;
    
    switch ((self.tag)) {
        case OrderTextFieldOriginPrice:
            self.title = @"接单价格";
            break;
        case OrderTextFieldDispatchPrice:
            self.title = @"发单价格";
            break;
        case OrderTextFieldArriveLocation:
            self.title = @"到达地点";
            break;
        case OrderTextFieldDistance:
            self.title = @"里程";
            break;
        case OrderTextFieldSettlementPrice:
            self.title = @"结算价格";
            break;
        case MyNicknameTextField:
            self.title = @"昵称";
            break;
        case MyCompanyTextField:
            self.title = @"公司名称";
            break;
        case CompanyTextFieldBusinessAddress:
            self.title = @"企业经营地址";
            break;
        case UseCarTelephone:
            self.title = @"用车人联系电话";
    }
    
    [self initNavBar];
    self.textField.delegate = self;
    if (self.text != nil && self.text.length > 0) {
        self.textField.text = self.text;
    }
    switch (self.tag) {
        case OrderTextFieldCustomerMobile:
        case VechileTextFieldMobile:
        case UseCarTelephone:
            self.textField.keyboardType = UIKeyboardTypeNumberPad;
            self.addContactView.hidden = NO;
            break;
        case CreditApplyMobile:
        case InquireVechileNumber:
            self.textField.keyboardType = UIKeyboardTypeNumberPad;
            self.addContactView.hidden = YES;
            break;
        case OrderTextFieldOriginPrice:
        case OrderTextFieldDispatchPrice:
        case OrderTextFieldSettlementPrice:
            self.textField.keyboardType = UIKeyboardTypeDecimalPad;
            break;
        default:
            self.textField.keyboardType = UIKeyboardTypeDefault;
            break;
    }
    
    [self.addContactView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onGetPhoneContact)]];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.textField canBecomeFirstResponder]) {
        [self.textField becomeFirstResponder];
    }
}

-(void)initNavBar
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(pressedSaveButton:)];
}


-(void)pressedSaveButton:(id)sender
{
    NSString *errorMsg;
    
    
    if (self.tag == OrderTextFieldCarModel) {
        [self.delegate onTextDidChanged:self.tag text:self.textField.text.trim];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if (self.textField.text.trim.length == 0) {
        [self showAlertView:ALERT_MSG_NO_CONTENT];
        return;
    }
    
    
    if (self.tag == OrderTextFieldCustomerMobile || self.tag == VechileTextFieldMobile || self.tag == CreditApplyMobile || self.tag == UseCarTelephone) {
        
        if (![UserHelper validatePhoneNum:self.textField.text.trim error:&errorMsg]) {
            [self showAlertView:errorMsg];
            return;
        }
    }
    else if (self.tag == VechileTextFieldNumber /*|| self.tag == OrderTextFieldCarModel*/) {
        
        if (![VechileUtils validateCarId:self.textField.text.trim error:&errorMsg]) {
            [self showAlertView:errorMsg];
            return;
        }
    }
    else if (self.tag == MyNicknameTextField){
        [self updateUserInfo:MyNicknameTextField text:self.textField.text.trim];
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onTextDidChanged:text:)]) {
        [self.delegate onTextDidChanged:self.tag text:self.textField.text.trim];
    }
    
    if (_completeHandle) {
        _completeHandle(self.tag, self.textField.text.trim);
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}

-(void)onGetPhoneContact
{
    
    ContactListService *contactListService = [[ContactListService alloc] initWithViewController:self];
    contactListService.delegate = self;
    [contactListService showContactList];

}

- (void)onGetMobile:(NSString*)mobile;
{
    self.textField.text = mobile.trim;
}


#pragma mark - UITextField delegate method

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if (self.tag == OrderTextFieldOriginPrice || self.tag == OrderTextFieldDispatchPrice || self.tag == OrderTextFieldSettlementPrice || self.tag == InquireVechileNumber) {
        if ((range.location == 0) && ([string isEqualToString:@"0"]))
        {
            return NO;
        }
        
        if ((range.location == 0) && ([string isEqualToString:@"."])) {
            return NO;
        }
        
        //限制非数字的字符
        //        if ((string.length > 0 && ![string isMatchedByRegex:@"^[0-9]*$"]) /*|| [[string trim] intValue] > 99*/) {
        //            return NO;
        //        }
    }
    
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
