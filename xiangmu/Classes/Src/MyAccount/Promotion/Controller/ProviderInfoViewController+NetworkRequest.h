//
//  ProviderInfoViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderInfoViewController.h"

@interface ProviderInfoViewController (ProviderInfoNetworkRequest)

-(void)getFinanceInfo;

-(void)fetchTodayBalance;

- (void)fetchProviderRankList;

- (void)fetchProviderCurrentAdvRevenue;

- (void)fetchProviderAdOrderListWithDetail;

-(void)continuePayOrder:(id)sender;

-(void)cancelOrder:(id)sender;

-(void)deleteOrder:(id)sender;

@end
