//
//  InquireView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CertifiedView;
@class StarView;

@interface InquireView : UIView

@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextView *remarkTextView;
@property (weak, nonatomic) IBOutlet UIButton *generateOrderButton;
@property (weak, nonatomic) IBOutlet UIButton *addPartnerButton;
@property (weak, nonatomic) IBOutlet CertifiedView *certifiedView;
@property (weak, nonatomic) IBOutlet StarView *starView;
@property (weak, nonatomic) IBOutlet UIButton *telButton;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *vechileModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *vechileQuantityLabel;

@end
