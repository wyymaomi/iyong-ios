//
//  CompanyCertificationViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "CompanyCertificationViewModel.h"

@protocol CompanyCertificationDelegate <NSObject>

-(void)onCompanyCertification;

@end

typedef NS_ENUM(NSUInteger, CompanyCertificationRow) {
    CompanyCertificationRowArea = 0,
    CompanyCertificationRowCompanyName,
    CompanyCertificationRowAddress
};

@interface CompanyCertificationViewController : BaseViewController

//@property (nonatomic, strong) NSData *imgData;

@property (nonatomic, weak) id<CompanyCertificationDelegate> delegate;

@property (nonatomic, strong) CompanyCertificationViewModel *viewModel;

@end
