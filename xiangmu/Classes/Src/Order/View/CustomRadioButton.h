//
//  CustomRadioButton.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomRadioButton : UIView

@property (assign, nonatomic) BOOL isSelected;
@property (weak, nonatomic) IBOutlet UIButton *clickButton;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIImageView *radioButtonImage;

@end
