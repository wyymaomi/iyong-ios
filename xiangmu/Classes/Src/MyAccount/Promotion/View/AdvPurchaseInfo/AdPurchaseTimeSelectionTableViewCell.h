//
//  AdPurchaseTimeSelectionTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvPurchaseViewModel.h"
#import "TimeSegmentView.h"

@protocol AdPurchaseTimeSelectionTableViewCellDelegate <NSObject>

//- (void)onClickButton:(NSString*)timeSegmentText adId:(NSString*)adId price:(NSNumber*)price isSelected:(BOOL)isSelected;
- (void)onClickButton:(NSInteger)index timeSegmentText:(NSString*)timeSegmentText adId:(NSString*)adId price:(NSNumber*)price endTime:(double)endTime isSelected:(BOOL)isSelected;

-(BOOL)shouldSelectTimeSegmentView;

@end

@interface AdPurchaseTimeSelectionTableViewCell : UITableViewCell<TimeViewDelegate>

@property (nonatomic, strong) NSMutableArray *buttonArray;

@property (nonatomic, strong) AdvPurchaseViewModel *viewModel;

@property (nonatomic, weak) id<AdPurchaseTimeSelectionTableViewCellDelegate> cellDelegate;

+(CGFloat)getCellHeight;

@end
