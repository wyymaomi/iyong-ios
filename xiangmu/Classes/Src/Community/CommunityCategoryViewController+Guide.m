//
//  CommunityCategoryViewController+Guide.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityCategoryViewController+Guide.h"
#import "CommunityCategoryViewController+Adv.h"


@implementation CommunityCategoryViewController (Guide)

- (void)initIntroduceView
{
    
    NSNumber *value = [[UserDefaults sharedInstance] objectForKey:@"IsShowIntroCommunity"];
    
    if (value == nil) {
    
        self.guideView = [[GuideView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [self.guideView.okButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
 
        self.maskWindow = [UIApplication sharedApplication].keyWindow;
        if (!self.maskWindow)
            self.maskWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
        [[[self.maskWindow subviews] objectAtIndex:0] addSubview:self.guideView];
        
    }
    
}


- (void)buttonTapped:(id)sender
{
    DLog(@"buttonTapped:");
    
//    UIButton *button = (UIButton*)sender;

    if (self.clickIndex < 3) {
        @autoreleasepool {
            self.clickIndex++;
//            UIImage *image = [UIImage newImageFromResource:[NSString stringWithFormat:@"guide%d.png",i+1]];
            NSString *imageName = [[NSString alloc] initWithFormat:@"guide_page%ld", (long)self.clickIndex];
            UIImage *image = kImgFromFile(imageName, @"png");
            self.guideView.imageView.image = image;
//            self.guideView.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"guide_page%d", self.clickIndex]];
        }
    }
    else {
        [[UserDefaults sharedInstance] setObject:@0 forKey:@"IsShowIntroCommunity"];
        [self.guideView removeAllSubviews];
        [self.guideView removeFromSuperview];
        self.guideView = nil;
        self.maskWindow = nil;
        
        [self showAdv:NO];
        
    }
    
}


#pragma mark - getter and setter

- (void)setClickIndex:(NSInteger)clickIndex
{
    NSNumber *clickIndexNumber = [NSNumber numberWithInteger:clickIndex];
    objc_setAssociatedObject(self, &kClickIndex, clickIndexNumber, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSInteger)clickIndex
{
    NSNumber *clickIndexNumber = objc_getAssociatedObject(self, &kClickIndex);
    return [clickIndexNumber integerValue];
}


- (void)setGuideView:(GuideView *)guideView
{
    objc_setAssociatedObject(self, &kGuideView, guideView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(GuideView*)guideView
{
    return objc_getAssociatedObject(self, &kGuideView);
}

- (void)setMaskWindow:(UIWindow *)maskWindow
{
    objc_setAssociatedObject(self, &kMaskWindow, maskWindow, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UIWindow*)maskWindow
{
    return objc_getAssociatedObject(self, &kMaskWindow);
}

@end
