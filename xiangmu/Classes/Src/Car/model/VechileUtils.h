//
//  VechileUtils.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VechileUtils : NSObject

// 用户名校验
//+ (BOOL)validateUserName:(NSString *)userName error:(NSString **)errorMsg;

// 车牌号验证
+ (BOOL)validateCarId:(NSString*)carId error:(NSString **)errorMsg;

@end
