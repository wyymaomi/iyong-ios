//
//  BaseHttpDTO.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseHttpMessage : NSObject

-(NSString*)description;

@end
