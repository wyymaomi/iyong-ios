//
//  RechargeViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

static NSString *upmpNotificeUrl = @"";

@interface RechargeViewController : BaseViewController

@property (nonatomic, assign) NSInteger type;// 0:充值；1:提现
@property (nonatomic, strong) NSString *accountMoney;// 可提现金额

@end
