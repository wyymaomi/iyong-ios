//
//  MyTravelTableViewCell.h
//  iYONGDriver
//
//  Created by 湛思科技 on 2017/6/21.
//  Copyright © 2017年 Shanghai Zhansi Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "TraceModel.h"
#import "TravelModel.h"

@interface MyTravelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *startLocation;
@property (weak, nonatomic) IBOutlet UILabel *endLocation;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

//@property (nonatomic, strong) TraceModel *traceModel;

@property (nonatomic, strong) TravelModel *travelModel;

+(CGFloat)getCellHeight;

@end
