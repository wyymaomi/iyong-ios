//
//  AppConstants.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h

//#define DONWLOAD_SWITCH 1

#define NAVBAR_TITLE_TEXT_COLOR [UIColor blackColor]
#define NAVBAR_BGCOLOR UIColorFromRGB(0x09a892)
#define BUTTON_HIGHLIGHTED_COLOR UIColorFromRGB(0x077e6d)
//#define NAVBAR_BGCOLOR UIColorFromRGB(0x29538C)
//#define NAVBAR_BGCOLOR UIColorFromRGB(0x48a1dc)

#define DATABASE_VERSION @"2.6"

#define DEFAULT_CITY_NAME @"上海市"
#define DEFAULT_CITY_CODE @"310100"


#define SH_CITY_CODE @"310100"
#define BJ_CITY_CODE @"110100"
#define HZ_CITY_CODE @"330100"
#define SZ_CITY_CODE @"440300"


#define SPLASH_TIME 2

#define REQUEST_TIME_OUT    60      // 请求超时时间
#define LOGIN_OVER_TIME     15*60  // 登录超时时间
//static NSInteger showLoginAlertTime = 0;
static BOOL bPopupLoginView; // 当前界面是否需要弹出登录窗口
static NSInteger receiveAuthMessageCount; // 收到登录消息次数
//static NSInteger receiveLoginOverTimeMessageCount; // 登录超时通知次数

//#define MSG_DECRYPT_FAILURE @"decrypt failed!"

//#define KEY_SESSION     @"key_session"      // session
#define KEY_TOKEN       @"key_token"        // token
#define KEY_DES_KEY     @"key_des_key"      // DES 密钥
#define KEY_SHA_BODY    @"key_sha_body"     // sha加密的body
#define KEY_RSA_BODY    @"key_rsa_body"     // RSA加密的body
#define KEY_GESTURE_PWD @"key_gesture_pwd"  // 手势密码
#define KEY_USER_MODEL  @"key_user_model"
#define KEY_THIRDPARTY_ID @"key_thirdParty_id"
#define KEY_THIRDPARTY_LOGIN_PARAMS @"key_thirdparty_login_param"

//#define kGesturePwdSetSuccessNotification   @"kGesturePwdSetSuccessNotification"        //手势密码设置成功后退出之前的登录界面
//#define kRegisterSuccessNotification        @"kRegisterSuccessNotification"             // 注册成功后通知登录界面
#define kNeedLoginNotification              @"kNeedLoginNotification"                   // 需要登录通知
#define kUnauthorizeNotification            @"Unauthorize_Notification"                 // 未授权通知
#define kLoginSuccessNotification           @"kLoginSuccessNotification"
#define kOnLoginOvertimeNotification        @"kEnterbackgroundNotification"             // 进入后台通知
#define kOnLogoutNotification               @"kLogoutNotification"                      // 退出登录通知
#define kOnGetOrderCountNotification        @"kOnGetOrderCountNotification"             // 取得订单数量
#define kOnGetWaitPasswdPartnerNotification @"kOnGetWaitPasswdPartnerNotification"      // 待通过合作伙伴数量通知
//#define kOnNewBussinessCircleMsgNotification @"
#define kCCPDidReceiveMessageNotification @"CCPDidReceiveMessageNotification"
#define kBussinessCircleNewMessageNotification @"kBussinessCircleNewMessageNotification" // 业务圈通知
#define kOnGpsLocationNotification          @"kOnGpsLocationNotification"                // 定位成功通知
#define kDisplayAdvNotification             @"kDisplayAdvNotification"                      // 通知显示广告
#define kOnUserinfoUpdateNotification       @"kOnUserinfoUpdateNotification"                // 用户信息更新通知
#define kOnRefreshHomeExperienceListNotification @"kOnRefreshHomeExperienceListNotification" // 刷新首页用户体验通知
#define kAdvPurchaseSuccessNotification     @"kAdvPurchaseSuccessNotification"          // 广告支付成功通知
#define kAdvPurchaseFailureNotification     @"kAdvPurchaseFailureNotification"          // 广告支付失败通知
#define kRefreshBankcardListNotification    @"kRefreshBankcardListNotification"         // 刷新银行卡列表
#define kOnCompanyCertificationedNotification @"kOnCompanyCertificationedNotification"   // 企业认证成功通知
#define kOnCancelCarOrderSuccessNotification @"kOnCancelCarOrderSuccessNotification" // 刷新行程列表通知
#define kGetOrderReceivedNotification @"kGetOrderReceivedNotification" // 用户收到司机已接单响应

static NSString *const kUpdateUserLocationNotification = @"kUpdateUserLocationNotification";
static NSString *const kStopRefreshTimerNotification = @"kStopRefreshTimerNotification";


#define BBAlertLeavel  300

#pragma mark - 行程相关

static NSUInteger MapbusPageSize = 20;
static NSUInteger TrackPageSize=1000;
#define KEY_LastTravelStarttime @"lastTravelStartTime"
#define KEY_LastTravelStartLocation @"lastTravelStartLocation"


#pragma mark - Bugly App ID
#if DEBUG
static NSString *const BuglyAppID = @"94f5d18a42"; // bugly App ID
#else 
static NSString *const BuglyAppID = @"9a91f93fa4";
#endif

#pragma mark - 极光推送appKey
#define JPUSH_APPKEY @"89ee1b6f8371952d401267c4"


#pragma mark - 阿里云推送通知appkey、appsecret
static NSString *const AliyunPushNotificationAppKey = @"23383699";
static NSString *const AliyunPushNotificationAppSecret = @"bc5181c977032567743289af9eb028c1";

#pragma mark - Aliyun OSS
static NSString * const AccessKey = @"JBXMINDJTl5UPZWZ";
static NSString * const SecretKey = @"TFpC41SZ3tsY3lxOcAGH8EzfU3h6KH";
static NSString * const endPoint = @"https://oss-cn-shanghai.aliyuncs.com";
static NSString * const multipartUploadKey = @"multipartUploadObject";
#ifdef TEST
static NSString * const BucketName = @"iyongtest";
#else
static NSString * const BucketName = @"iyong";
#endif

#pragma mark - 腾讯地图API
#if DEBUG
#define TENCENT_MAP_APP_KEY @"ZGPBZ-HUEKQ-DIA56-GGUQC-7ZMQO-BZFEC"
#else
#define TENCENT_MAP_APP_KEY @"22FBZ-ZP23X-POO4C-Z7JTI-OL5TT-DCFRJ"
//#define TENCENT_MAP_APP_KEY @"OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77"
#endif

#pragma mark - 消息分类

#define BUSINESS_MSG_CODE @"10"

#pragma mark - 环信AppKey
#ifdef TEST
#define EaseMobAppKey @"1122170328115534#iyong"
#else
#define EaseMobAppKey @"1122170328115534#iyongproduct"
#endif

#pragma mark - 百度地图

static NSString *Baidu_Web_AK = @"HylSvX9DfiwVKpwWvR1V8OB0ZaPCba8K";
#ifdef TEST
static NSUInteger serviceID = 143874;
#else
static NSUInteger serviceID = 143870;
#endif
static NSString *AK = @"N0mHEkunzGiWn0TfbQHfGhjcd5zVWT6U";
static NSString *mcode = @"com.chinaiyong.iyong";
//static NSString *entityName = @"entityB";
static NSUInteger serverFenceID = 0;
static NSUInteger localFenceID = 0;

#endif /* AppConstants_h */
