//
//  HomeBottomView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/9.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HomeBottomView.h"

@implementation HomeBottomView


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = ColorFromRGBWithAlpha(0xFFFFFF, 0.5);
        [self addSubview:self.inquiryButton];
        [self addSubview:self.bidButton];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _inquiryButton.height = 34 * ViewHeight / 568;
    _inquiryButton.top = (self.frame.size.height-_inquiryButton.height)/2;
    
    _bidButton.height = _inquiryButton.height;
    _bidButton.top = (self.frame.size.height - _bidButton.height)/2;
    
//    [self setNeedsLayout];
}

-(UIButton*)inquiryButton
{
    if (_inquiryButton == nil) {
        _inquiryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _inquiryButton.frame = CGRectMake(18, /*(self.frame.size.height-34*ViewHeight/568)/2*/0, ViewWidth/2 - 18*2, 34*ViewHeight/568);
        [_inquiryButton setTitle:@"询价" forState:UIControlStateNormal];
        [_inquiryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_inquiryButton blueStyle];
        [_inquiryButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x203d73)] forState:UIControlStateNormal];
        [_inquiryButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x203d73)] forState:UIControlStateHighlighted];
//        [_inquiryButton setBackgroundColor:UIColorFromRGB(0x203d73)];
        
    }
    
    return _inquiryButton;
}

-(UIButton*)bidButton
{
    if (!_bidButton) {
        _bidButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _bidButton.frame = CGRectMake(ViewWidth/2+18, /*(self.frame.size.height-34*ViewHeight/568)/2*/0, ViewWidth/2-36, 34*ViewHeight/568);
        [_bidButton setTitle:@"报价" forState:UIControlStateNormal];
        [_bidButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_bidButton blueStyle];
        [_bidButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x203d73)] forState:UIControlStateNormal];
        [_bidButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x203d73)] forState:UIControlStateHighlighted];
    }
    
    return _bidButton;
}


@end
