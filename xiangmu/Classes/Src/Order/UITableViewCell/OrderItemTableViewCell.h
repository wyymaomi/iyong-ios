//
//  UntreatedOrderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderListItem;

@interface OrderItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *carDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *startLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveLocationLabel;
+ (instancetype)cellWithTableView:(UITableView *)tableView;

-(void)initData:(OrderListItem*)item;

@end
