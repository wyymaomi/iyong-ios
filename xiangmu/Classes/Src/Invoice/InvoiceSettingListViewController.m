//
//  InvoiceSettingListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/30.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceSettingListViewController.h"
#import "InvoiceSettingItemCellTableViewCell.h"
#import "InvoiceinfoHttpMessage.h"
#import "InvoiceSettingEditViewController.h"

@interface InvoiceSettingListViewController ()<EmptyDataDelegate>

@end

@implementation InvoiceSettingListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"开票设置";
    
    [self setRightNavigationItemWithTitle:@"添加" selMethod:@selector(addInvoiceSetting)];
    
    [self.view addSubview:self.headerPullRefreshTableView];
    
    
    
    self.emptyDataDelegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.headerPullRefreshTableView beginHeaderRefresh];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [InvoiceSettingItemCellTableViewCell getCellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    InvoiceSettingItemCellTableViewCell *cell = [InvoiceSettingItemCellTableViewCell cellWithTableView:tableView];
    
    InvoiceSettingHttpMessage *invoiceSettingInfo = self.dataList[indexPath.row];
    
    cell.invoiceTItleLabel.text = invoiceSettingInfo.header;
    
    cell.contactAddressLabel.text = [NSString stringWithFormat:@"%@%@", invoiceSettingInfo.areaName, invoiceSettingInfo.address];
    
    return cell;
    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    InvoiceSettingHttpMessage *settingHttpMessage = self.dataList[indexPath.row];
    
    if (self.sourceFrom == InvoiceListSourceFromMyInvoice) {
        
        InvoiceSettingEditViewController *viewController = [InvoiceSettingEditViewController new];
        viewController.invoiceSettingHttpMessage = settingHttpMessage;
        viewController.editMode = InvoiceEditModeUpdate;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if (self.sourceFrom == InvoiceListSourceFromEditInfo) {
        
        if (_completeHandle) {
//            _completeHandle(self.tag, self.textField.text.trim);
            _completeHandle(settingHttpMessage);
            
            [self pressedBackButton];
        }
        
        
    }
    
//    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
//        [self.customDelegate didSelectRowAtIndexPath:indexPath];
//    }
    
    
}


#pragma mark - 列表为空默认显示

-(UIImage*)emptyIconImage
{
    return [UIImage imageNamed:@"img_exclaim"];
}

#pragma mark - EmptyDataDelegate methods

- (NSString*)emptyDetailText;
{
    return @"暂无预设开票信息";
}

#pragma mark - 添加开票设置


-(void)addInvoiceSetting
{
    [self gotoPage:@"InvoiceSettingEditViewController"];
}

#pragma mark - 获取发票设置列表

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    
    if ([self needLogin]) {
        return;
    }
    
    [self fetchInvoiceSettingList:nil];
    
//    [self fetchOrderList:self.listStatus time:nil];
    
}

-(void)onFooterRefresh
{
    self.nextAction = @selector(onFooterRefresh);
    
    if ([self needLogin]) {
        return;
    }
    
//    TravelOrderModel *model = [self.dataList lastObject];
//    [self fetchOrderList:self.listStatus time:[model.createTime stringValue]];
    
    //    GrabOrderModel *model = [self.dataList lastObject];
    //    [self fetchCanreceiveOrderList:[model.createTime stringValue]];
}


-(void)fetchInvoiceSettingList:(NSString*)time
{
    if (IsStrEmpty(time)) {
        self.headerPullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
//    NSString *params = [NSString stringWithFormat:@"listStatus=%lu&time=%@", (unsigned long)listStatus, [StringUtil getSafeString:time]];
    
    self.loadStatus = LoadStatusInit;
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(GET_INVOICE_SETTING_LIST_API) requestMethod:POST params:nil success:^(NSDictionary * responseData) {
        
        [weakSelf.headerPullRefreshTableView endHeaderRefresh];
        [weakSelf.headerPullRefreshTableView endFooterRefresh];
        [weakSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *dataList = responseData[@"data"];
            for (NSDictionary *dictionary in dataList) {
                
                InvoiceSettingHttpMessage *invoiceInfoHttpMessage = [[InvoiceSettingHttpMessage alloc] init];
                
//                id 发票设置ID
//                userId 当前用户ID
//                header 发票抬头
//                taxpayerNumber  纳税人识别号
//                registerAddress 注册地址
//                openingBank 开户银行
//                bankAccount 银行帐号
//                contact 联系人
//                tel 联系电话
//                areaCode 地区编码
//                areaName 所在区域
//                address 详细地址
//                remark 客户备注
                
                invoiceInfoHttpMessage.id = dictionary[@"id"];
                invoiceInfoHttpMessage.header = dictionary[@"header"];
                invoiceInfoHttpMessage.taxpayerNumber = dictionary[@"taxpayerNumber"];
                invoiceInfoHttpMessage.registerAddress = dictionary[@"registerAddress"];
                invoiceInfoHttpMessage.openingBank = dictionary[@"openingBank"];
                invoiceInfoHttpMessage.bankAccount = dictionary[@"bankAccount"];
                invoiceInfoHttpMessage.contact = dictionary[@"contact"];
                invoiceInfoHttpMessage.tel = dictionary[@"tel"];
                invoiceInfoHttpMessage.areaCode = dictionary[@"areaCode"];
                invoiceInfoHttpMessage.areaName = dictionary[@"areaName"];
                invoiceInfoHttpMessage.address = dictionary[@"address"];
                invoiceInfoHttpMessage.remark = dictionary[@"remark"];
                
                [weakSelf.dataList addObject:invoiceInfoHttpMessage];
                
                //                GrabOrderModel *model = [[GrabOrderModel alloc] initWithDictionary:dictionary error:nil];
                //                [weakSelf.dataList addObject:model];
                
//                TravelOrderModel *model = [[TravelOrderModel alloc] initWithDictionary:dictionary error:nil];
//                [weakSelf.dataList addObject:model];
                
            }
            
            weakSelf.loadStatus = LoadStatusFinish;
            
            [weakSelf.headerPullRefreshTableView reloadData];
            
            
            
            
        }
        else {
            
            weakSelf.loadStatus = LoadStatusFinish;
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf resetAction];
        weakSelf.loadStatus = LoadStatusNetworkError;
        
        
    }];
}

@end
