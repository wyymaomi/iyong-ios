//
//  CustomActionSheetDatePicker.m
//
//  Created by 湛思科技 on 16/4/19.
//  Copyright © 2016年 我家有福. All rights reserved.
//

#import "CustomActionSheetDatePicker.h"
#import "NSDate+Category.h"

#define CHOOSE_DAY_COUNT 7 //可选时间天数
#define CHOOSE_MIN_HOUR  0 //可选最小时段
#define CHOOSE_MAX_HOUR  24 //可选最大时段
#define ONE_DAY 24*60*60

@interface CustomActionSheetDatePicker ()<UIPickerViewDataSource,UIPickerViewDelegate>
//pickerView数据源
@property (strong, nonatomic) NSArray *dataSourceForDayComponent;
@property (strong, nonatomic) NSArray *dataSourceForHourComponent;
@property (strong, nonatomic) NSArray *dataSourceForMinuteComponent;
@property (assign, nonatomic) BOOL isToday;// 是否今天
@end

@implementation CustomActionSheetDatePicker

+(instancetype)showPickerWithTitle:(NSString*)title
                         doneBlock:(CustomActionDateDoneBlock)doneBlock
                       cancelBlock:(CustomActionDateCancelBlock)cancelBlockOrNil
                            origin:(id)origin;
{
    CustomActionSheetDatePicker *picker = [[CustomActionSheetDatePicker alloc] initWithTitle:title doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    [picker showActionSheetPicker];
    return picker;
}

-(instancetype)initWithTitle:(NSString*)title
                   doneBlock:(CustomActionDateDoneBlock)doneBlock
                 cancelBlock:(CustomActionDateCancelBlock)cancelBlockOrNil
                      origin:(id)origin
{
    self = [self initWithTarget:nil successAction:nil cancelAction:nil origin:origin];
    if (self) {
        self.title = title;
        self.isToday = YES;
        
        [self initResultDate];
        
        self.onActionSheetDone = doneBlock;
        self.onActionSheetCancel = cancelBlockOrNil;
        
    }
    return self;
}

- (UIPickerView *)configuredPickerView {
    
    CGRect pickerFrame = CGRectMake(0, 40, self.viewSize.width, 216);
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    pickerView.showsSelectionIndicator = YES;
    
//    self.startDate = [NSDate dateWithTimeInterval:60*60 sinceDate:[NSDate date]];
    
    [pickerView selectRow:0 inComponent:0 animated:YES];
    [pickerView selectRow:0 inComponent:1 animated:YES];
    [pickerView selectRow:0 inComponent:2 animated:YES];
    
//    if ([pickerView numberOfRowsInComponent:0] > 7) {
//        [pickerView selectRow:7 inComponent:0 animated:YES];
//        self.isToday = YES;
//    }
    
//    NSInteger hour = [[NSDate date] hour];
//    NSInteger minute = [[NSDate date] minute];
//    [pickerView selectRow:hour inComponent:1 animated:YES];
//    [pickerView selectRow:minute inComponent:2 animated:YES];

    
    return pickerView;
}

#pragma mark - 数据初始化
-(NSDate *)startDate
{
    if (_startDate == nil) {
        _startDate = [NSDate dateWithTimeInterval:60*60 sinceDate:[NSDate date]];
        NSInteger minute = [_startDate minute];
//        NSInteger hour = [_startDate hour];
        if (minute > 50) {
            _startDate = [NSDate dateWithTimeInterval:(60-minute)*60 sinceDate:_startDate];
        }
    }
    return _startDate;
}

- (NSString *)resultDate
{
    if (!_resultDate) {
        _resultDate = [self.date stringWithDateFormat:@"yyyy-MM-dd HH:mm"];
    }
    return _resultDate;
}

- (NSDate *)date
{
    if (!_date) {
//        _date = [NSDate date];
//        NSDate *currentDate = [NSDate date];
        NSTimeInterval oneHour = 60 * 60;
        _date = [NSDate dateWithTimeInterval:oneHour sinceDate:[NSDate date]];
//        _date = [currentDate dateWithTimeInterval:
//        NSTimeInterval  oneDay = 1*60*60;  //1天的长度
        //        self.date = [NSDate dateWithTimeInterval:(dayRow-7)*oneDay sinceDate:[NSDate date]];
//        self.date = [NSDate dateWithTimeInterval:dayRow*oneDay sinceDate:[NSDate date]];
    }
    return _date;
}

- (NSString*)year {
    if (!_year) {
//        NSDate *date = [NSDate date]'
        _year = [NSString stringWithFormat:@"%ld", (long)[self.startDate year]];
    }
    return _year;
}

- (NSString *)day{
    if(!_day){
        _day = self.dataSourceForDayComponent[0];//self.dataSourceForDayComponent[7];
    }
    return _day;
}

- (NSString *)hour{
    if(!_hour){
        // 取得当前小时数字
//        NSInteger hour = [[NSDate date] hour];
//        NSInteger hour = [self.startDate hour];
//        _hour = self.dataSourceForHourComponent[hour];
        _hour = self.dataSourceForHourComponent[0];
    }
    return _hour;
}

- (NSString *)minute{
    if(!_minute){
        // 取得当前分数数字
//        NSInteger minute = [[NSDate date] minute];
//        _minute = self.dataSourceForMinuteComponent[minute];
        _minute = self.dataSourceForMinuteComponent[0];
    }
    return _minute;
}

- (NSArray *)dataSourceForDayComponent{
    
    if(!_dataSourceForDayComponent){
        
        NSMutableArray *arr = [NSMutableArray new];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *str = [NSString stringWithFormat:@"%@ 00:00:00",[formatter stringFromDate:self.startDate]];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *baseDate = [formatter dateFromString:str];
        DLog(@" %@",str);
        

        
        for (NSInteger i = 0; i < CHOOSE_DAY_COUNT; i++) {
            if (i == 0) {
                [arr addObject:@"今天"];
            }
            else if (i == 1) {
                [arr addObject:@"明天"];
            }
            else {
                NSDate *date = [NSDate dateWithTimeInterval:24*60*60*i sinceDate:baseDate];
                [formatter setDateFormat:@"M月dd日EE"];
                NSMutableString *dateStr = [[formatter stringFromDate:date] mutableCopy];
                [arr addObject:dateStr];
            }
        }
        // 加入7天前的日期
//        NSMutableArray *lastSevenDays = [NSMutableArray new];
//        for (NSInteger i = 7; i > 0; i--) {
//            NSDate *date = [NSDate dateWithTimeInterval:-24*60*60*i sinceDate:baseDate];
//            [formatter setDateFormat:@"M月dd日EE"];
//            NSMutableString *dateStr = [[formatter stringFromDate:date] mutableCopy];
//            [lastSevenDays addObject:dateStr];
//        }
//        [arr insertObjects:lastSevenDays atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, lastSevenDays.count)]];
        
        _dataSourceForDayComponent = [arr mutableCopy];
    }
    return _dataSourceForDayComponent;
}


- (NSArray *)dataSourceForHourComponent{
    
    if(!_dataSourceForHourComponent){
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
        [formatter setDateFormat:@"HH"];
        NSString *hour = [formatter stringFromDate:self.startDate];
        

        NSMutableArray *arr = [NSMutableArray array];
        
//        NSDateFormatter *minuteFormatter = [[NSDateFormatter alloc] init];
//        [minuteFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
//        [minuteFormatter setDateFormat:@"mm"];
//        NSString *minute = [formatter stringFromDate:self.startDate];
        
        NSInteger startHour = 0;
        
        if (self.isToday) {
            startHour = [hour integerValue];
        }
        
        for (NSInteger i = startHour; i < 24; i++) {
            if (i < 10) {
                [arr addObject:[NSString stringWithFormat:@"0%ld时", (long)i]];
            }
            else {
                [arr addObject:[NSString stringWithFormat:@"%ld时", (long)i]];
            }
        }
        
        _dataSourceForHourComponent = [arr copy];
        
    }
    return _dataSourceForHourComponent;
}


- (NSArray *)dataSourceForMinuteComponent{
    
    if(!_dataSourceForMinuteComponent){
        
        NSMutableArray *arr = [NSMutableArray array];
        
        NSArray *tempArrays = @[@0, @10, @20, @30, @40, @50];
//        NSArray *tempArrays = @[@0, /*@5,*/ @10, /*@15,*/ @20, /*@25,*/ @30, /*@35,*/ @40, /*@45,*/ @50/*, @55*/];
        
        // 判断选择的是否为当前小时
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
        [formatter setDateFormat:@"HH"];
        NSString *hour = [formatter stringFromDate:self.startDate];
        NSString *tempHour ;
        if([hour integerValue] < 10){
            tempHour = [NSString stringWithFormat:@"0%ld时", (long)[hour integerValue]];
        }else{
            tempHour = [NSString stringWithFormat:@"%ld时", (long)[hour integerValue]];
        }
        DLog(@"self.hour = %@", self.hour);
        
        if (self.isToday && [self.hour isEqualToString:tempHour]) {
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
            [formatter setDateFormat:@"mm"];
            NSString *minute = [formatter stringFromDate:[NSDate date]];
            DLog(@"当前分钟数为%@", minute);
            
            NSInteger startMinute = 0;
            for (NSInteger i = 0; i < tempArrays.count; i++) {
                NSNumber *number = tempArrays[i];
                if ([minute integerValue] <= [number integerValue]) {
                    startMinute = [number integerValue];
                    break;
                }
            }
            
            // 分钟数大于50
//            if ([minute integerValue] > 50) {
//                startMinute = 0;
//            }
            
            NSMutableArray *arr = [NSMutableArray new];
            for (NSInteger i = startMinute; i < 60; i = i + 10) {
                if (i < 10) {
                    [arr addObject:[NSString stringWithFormat:@"0%ld分", (long)i]];
                }
                else {
                    [arr addObject:[NSString stringWithFormat:@"%ld分", (long)i]];
                }
                
            }
            _dataSourceForMinuteComponent = [arr mutableCopy];
        }
        else {
            for (NSInteger i = 0; i < 60; i=i+10) {
                if (i < 10) {
                    [arr addObject:[NSString stringWithFormat:@"0%ld分", i]];
                }
                else {
                    [arr addObject:[NSString stringWithFormat:@"%ld分", i]];
                }
            }
            _dataSourceForMinuteComponent = [arr mutableCopy];
        }
        
        DLog(@"dataSourceForMinuteComponent = %@", _dataSourceForMinuteComponent);
        
//        _dataSourceForMinuteComponent = @[@"00分", @"10分", ]
        
//        NSMutableArray *arr = [NSMutableArray array];
        
//        for (NSInteger i = 0; i < 60; i++) {
//            if (i < 10) {
//                [arr addObject:[NSString stringWithFormat:@"0%ld分", (long)i]];
//            }
//            else {
//                [arr addObject:[NSString stringWithFormat:@"%ld分", (long)i]];
//            }
//        }
        
//        _dataSourceForMinuteComponent = [arr copy];
        
    }
    return _dataSourceForMinuteComponent;
}

#pragma mark - UIPickerViewDataSource,UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
//    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    switch (component) {
        case 0:
            return self.dataSourceForDayComponent.count;
            break;
        case 1:
            return self.dataSourceForHourComponent.count;
            break;
        case 2:
            return self.dataSourceForMinuteComponent.count;
//            return self.dataSourceForHourComponent.count==0?0:self.dataSourceForMinuteComponent.count;
            break;
        default:
            return 0;
            break;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    switch (component) {
        case 0:
            return self.dataSourceForDayComponent[row];
            break;
        case 1:
            return self.dataSourceForHourComponent[row];
            break;
        case 2:
            return self.dataSourceForMinuteComponent[row];
            break;
        default:
            return 0;
            break;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (component == 0) {
        
        if (row == 0) {
            
            //选择的day是今天
            self.isToday = YES;
            
        }
        else {
            
            //选择的day不是今天
            self.isToday = NO;
        }
        
//        self.dataSourceForDayComponent = nil;
        self.dataSourceForHourComponent = nil;
        self.dataSourceForMinuteComponent = nil;
        self.hour = nil;
        self.minute = nil;
        
        self.day = self.dataSourceForDayComponent[row];
        
        [(UIPickerView*)self.pickerView reloadComponent:1];
        [(UIPickerView*)self.pickerView reloadComponent:2];
        [(UIPickerView*)self.pickerView selectRow:0 inComponent:1 animated:YES];
        [(UIPickerView*)self.pickerView selectRow:0 inComponent:2 animated:YES];
        
    }
    else if (component == 1)
    {
        //选择了新的小时
        self.hour = self.dataSourceForHourComponent[row];
        
        self.dataSourceForMinuteComponent = nil;
        self.minute = nil;
        
        [(UIPickerView*)self.pickerView reloadComponent:2];
        [(UIPickerView*)self.pickerView selectRow:0 inComponent:2 animated:YES];
        
    }
    else if (component == 2)
    {
        self.minute = self.dataSourceForMinuteComponent[row];
    }
//    [self selectedDate];
    
    [self didSelectRow:row inComponent:(NSInteger)component];
    
}

- (void)didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    NSInteger dayRow;
//    NSDate *date = [NSDate date];
    if (component == 0) {
        dayRow = row;
        
        NSTimeInterval  oneDay = 24*60*60;  //1天的长度
//        self.date = [NSDate dateWithTimeInterval:(dayRow-7)*oneDay sinceDate:[NSDate date]];
        self.date = [NSDate dateWithTimeInterval:dayRow*oneDay sinceDate:self.startDate];
        
    }
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
    [formatter setDateFormat:@"MM月dd日EE"];
    self.day = [formatter stringFromDate:self.date];
    
    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
    [formatter setDateFormat:@"yyyy年"];
    self.year = [formatter stringFromDate:self.date];
    NSString *dateStr = [NSString stringWithFormat:@"%@%@ %@%@",self.year,self.day,self.hour,self.minute];
    
    [formatter setDateFormat:@"yyyy年MM月dd日EE HH时mm分"];
    NSDate *result = [formatter dateFromString:dateStr];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    self.resultDate = [formatter stringFromDate:result];
    
    DLog(@"self.resultDate = %@", self.resultDate);
    
}

-(void)initResultDate
{
//    self.hour = self.dataSourceForHourComponent[0];
//    self.minute = self.dataSourceForMinuteComponent[0];
    _dataSourceForHourComponent = nil;
    _dataSourceForMinuteComponent = nil;
    _minute = nil;
    _hour = nil;
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
    [formatter setDateFormat:@"MM月dd日EE"];
    self.day = [formatter stringFromDate:self.startDate];
    

    NSString *dateStr = [NSString stringWithFormat:@"%@年%@ %@%@", self.year, self.day, self.hour, self.minute];
    DLog(@"dateStr = %@", dateStr);
//    NSDateFormatter *formatter = [NSDateFormatter new];
//    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
    [formatter setDateFormat:@"yyyy年MM月dd日EE HH时mm分"];
    NSDate *result = [formatter dateFromString:dateStr];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    self.resultDate = [formatter stringFromDate:result];
//    self.resultDate = dateStr;
    DLog(@"self.resultDate = %@", self.resultDate);
}


//- (void)selectedDate
//{

//    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_CN"]];
//    [formatter setDateFormat:@"yyyy年"];
//    NSString *str = [formatter stringFromDate:[NSDate date]];
//    NSString *dateStr = [NSString stringWithFormat:@"%@%@ %@%@",str,self.day,self.hour,self.minute];
//    
//    [formatter setDateFormat:@"yyyy年MM月dd日EE HH时mm分"];
//    NSDate *date = [formatter dateFromString:dateStr];
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//    self.resultDate = [formatter stringFromDate:date];
//    
//}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    switch (component) {
        case 0:
//            return [UIScreen mainScreen].bounds.size.width*0.5;
            return pickerView.frame.size.width * 0.5;
            break;
        case 1:
//            return [UIScreen mainScreen].bounds.size.width*0.25;
            return pickerView.frame.size.width * 0.25;
            break;
        case 2:
//            return [UIScreen mainScreen].bounds.size.width*0.25;
            return pickerView.frame.size.width * 0.25;
            break;
        default:
            return [UIScreen mainScreen].bounds.size.width*0.25;
            break;
    }
}

- (void)notifyTarget:(id)target didSucceedWithAction:(SEL)successAction origin:(id)origin {
    
    if (self.onActionSheetDone) {
        _onActionSheetDone(self, self.resultDate);
        return;
    }
    else if (target && [target respondsToSelector:successAction]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        //        [target performSelector:successAction withObject:timeZone withObject:origin];
#pragma clang diagnostic pop
        return;
    }
    NSLog(@"Invalid target/action ( %s / %s ) combination used for ActionSheetPicker", object_getClassName(target), sel_getName(successAction));
}



@end
