//
//  ProviderDetailViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "SDCycleScrollView.h"
#import "SDCollectionViewCell.h"
#import "PullRefreshTableView.h"
#import "ProviderDetailViewModel.h"
#import "ProviderCommentViewModel.h"
#import "CommentPopupView.h"

#define ProviderSectionNumber 5

typedef NS_ENUM(NSInteger, ProviderCell)
{
    ProviderCellAd = 0,
    ProviderCellCompanyInfo,
    ProviderCellCompanyType,
    ProviderCellCompanyDescription,
    ProviderCellCompanyDiscuss
};

@interface ProviderDetailViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIButton *telButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *bookButton;


@property (nonatomic, strong) NSString *companyId;
@property (weak, nonatomic) IBOutlet PullRefreshTableView *tableView;
@property (nonatomic, strong) SDCycleScrollView *bannerView;

@property (nonatomic, strong) ProviderDetailViewModel *viewModel;
@property (nonatomic, strong) NSMutableArray<ProviderCommentViewModel*> *commentList;// 评论列表
@property (nonatomic, strong) CommentPopupView *commentPopupView; // 评论框

@property (nonatomic, strong) UIImage *avatarIconImage;
@property (nonatomic, strong) UIImageView *shareImageView;
//@property (nonatomic, strong) UIImage *shareImage;// 分享的图片

@property (weak, nonatomic) IBOutlet UIView *topbarView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;



- (void)setNavigationView;

@end
