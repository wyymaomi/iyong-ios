//
//  AdPurchaseCalendarTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdPurchaseCalendarTableViewCell.h"
#import "DateUtil.h"
#import "FSCalendar.h"

@implementation AdPurchaseCalendarTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        // 450 for iPad and 300 for iPhone
        CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
        FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, height)];
        calendar.dataSource = self;
        calendar.delegate = self;
        calendar.allowsMultipleSelection = NO;
//        calendar.scrollEnabled = NO;
//        calendar.allowsMultipleSelection = YES;
        calendar.swipeToChooseGesture.enabled = NO;
        calendar.backgroundColor = [UIColor whiteColor];
        calendar.appearance.headerMinimumDissolvedAlpha = 0;
        calendar.appearance.titleFont = BOLD_FONT(14);
        calendar.appearance.caseOptions = FSCalendarCaseOptionsWeekdayUsesSingleUpperCase;
        calendar.appearance.headerDateFormat = @"yyyy年MM月";
        calendar.appearance.headerTitleColor = NAVBAR_BGCOLOR;
        calendar.appearance.titleDefaultColor = NAVBAR_BGCOLOR;
        calendar.appearance.titleSelectionColor = [UIColor whiteColor];
        calendar.appearance.weekdayTextColor = NAVBAR_BGCOLOR;
        calendar.placeholderType = FSCalendarPlaceholderTypeNone;
        calendar.appearance.borderDefaultColor = NAVBAR_BGCOLOR;
        calendar.appearance.borderSelectionColor = NAVBAR_BGCOLOR;
        calendar.appearance.selectionColor = NAVBAR_BGCOLOR;
        calendar.appearance.borderRadius = 0.3;
//        calendar.appearance.scroll
        [calendar registerClass:[CustomCalendarCell class] forCellReuseIdentifier:@"cell"];
        
        [self.contentView addSubview:calendar];
        self.calendar = calendar;
        
        UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        previousButton.frame = CGRectMake(0, 5, 95, 34);
        previousButton.frame = CGRectMake(110*Scale, 5, 15, 34);
        previousButton.backgroundColor = [UIColor whiteColor];
        previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [previousButton setImage:[UIImage imageNamed:@"icon_date_prev"] forState:UIControlStateNormal];
        [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:previousButton];
        self.previousButton = previousButton;
        
        UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        nextButton.frame = CGRectMake(ViewWidth-95, 5, 95, 34);
        nextButton.frame = CGRectMake(ViewWidth-130*Scale, 5, 15, 34);
        nextButton.backgroundColor = [UIColor whiteColor];
        nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [nextButton setImage:[UIImage imageNamed:@"icon_date_next"] forState:UIControlStateNormal];
        [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:nextButton];
        self.nextButton = nextButton;
    }
    
    return self;
}

- (void)previousClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    
    if (![[NSDate date] isPreviousMonth:previousMonth]) {
        [self.calendar setCurrentPage:previousMonth animated:YES];
    }
    
    
//    DLog(@"currentMonth = %@", currentMonth);
//    DLog(@"previousMonth = %@", previousMonth);
    
    // 比较两个日期的月份大小
//    NSInteger iPreviousMonth = [previousMonth month];
//    NSInteger iCurrentMonth = [[NSDate date] month];
//    NSInteger compareResult = [[NSDate date] compare:currentMonth];
//    if (compareResult != NSOrderedDescending) {
//        if (_cellDelegate && [_cellDelegate respondsToSelector:@selector(onSelectMonth:)]) {
////            [NSThread sleepForTimeInterval:5000];
//            [_cellDelegate onSelectMonth:previousMonth];
////            [self.calendar setCurrentPage:previousMonth animated:YES];
//        }
//    }
    

    
}

- (void)nextClicked:(id)sender
{
    
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:NO];
    
//    DLog(@"currentMonth = %@", [currentMonth stringWithDateFormat:@"yyyy-MM-dd"]);
//    DLog(@"nextMonth = %@", [nextMonth stringWithDateFormat:@"yyyy-MM-dd"]);
    
    
//    NSInteger compareResult = [[NSDate date] compare:currentMonth];
//    if (compareResult == NSOrderedDescending || compareResult == NSOrderedSame) {
//        if (_cellDelegate && [_cellDelegate respondsToSelector:@selector(onSelectMonth:)]) {
////            [NSThread sleepForTimeInterval:5000];
//            [_cellDelegate onSelectMonth:nextMonth];
////            nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:2 toDate:currentMonth options:0];
////            [self.calendar setCurrentPage:nextMonth animated:YES];
//        }
//    }
    
}

+(CGFloat)getCellHeight
{
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 450 : 300;
    return height;
}

- (void)setViewModel:(AdvPurchaseViewModel *)viewModel
{
//    if (_viewModel == nil) {
//        return;
//    }
    if (!viewModel || viewModel.saleAdvSumList == nil) {
        return;
    }
    
    _viewModel = viewModel;
    
//    [self.calendar reloadData];
//    self.viewModel = viewModel;
//    [self.calendar reloadData];
    
    [self.calendar reloadData];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - FSCalendarDataSource delegate method

//#if 0
/**
 * Asks the data source for a cell to insert in a particular data of the calendar.
 */
- (FSCalendarCell *)calendar:(FSCalendar *)calendar cellForDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    CustomCalendarCell *cell = [calendar dequeueReusableCellWithIdentifier:@"cell" forDate:date atMonthPosition:monthPosition];
    cell.badgeLabel.hidden = YES;
    
    
    if (self.viewModel == nil) {
        return cell;
    }
    
    NSDateFormatter *formatter = [[NSDate class] sharedDateFormatter];
    NSString *currentDateStr = [formatter stringFromDate:date];
    
    NSNumber *count = self.viewModel.advSumDictionary[currentDateStr];
    if (count == nil) {
        return cell;
    }
    else {
        NSInteger iCount = [count integerValue];
        if (iCount > 0) {
            cell.badgeLabel.hidden = NO;
            cell.badgeLabel.text = [count stringValue];
        }
        else {
            cell.badgeLabel.hidden = YES;
        }
    }
//    for (AdvSumModel *data in self.viewModel.saleAdvSumList) {
////        DLog(@"cellForDate %@", [formatter stringFromDate:date]);
//        NSString *date = data.date;
//        if ([date isEqualToString:currentDateStr]) {
//            NSInteger count = [data.count integerValue];
//            if (count > 0) {
//                cell.badgeLabel.hidden = NO;
//                cell.badgeLabel.text = [NSString stringWithFormat:@"%ld", (long)count];
//            }
//            else {
//                cell.badgeLabel.hidden = YES;
//            }
//        }
//    }
    
    return cell;
}
//#endif


#pragma mark - FSCalendarDelegateAppearance delegate method

/**
 * Asks the delegate for day text color in unselected state for the specific date.
 */
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date;
{
    NSDate *currentDate = [NSDate date];

    NSInteger compareResult = [DateUtil compareOneDay:currentDate withAnotherDay:date];
    if (compareResult < 0) {
        NSDateFormatter *formatter = [[NSDate class] sharedDateFormatter];
        NSString *currentDateStr = [formatter stringFromDate:date];
        //    NSDate *currentDate = [NSDate date];
        
        NSNumber *count = self.viewModel.advSumDictionary[currentDateStr];
        if (count == nil) {
            //        return cell;
            return [UIColor lightGrayColor];
        }
        else {
            NSInteger iCount = [count integerValue];
            if (iCount > 0) {
                return NAVBAR_BGCOLOR;
                //            cell.badgeLabel.hidden = NO;
                //            cell.badgeLabel.text = [count stringValue];
            }
            else {
                return [UIColor lightGrayColor];
                //            cell.badgeLabel.hidden = YES;
            }
        }
//        return NAVBAR_BGCOLOR;
    }
    if (compareResult == 0) {
        return [UIColor whiteColor];
    }
    return [UIColor lightGrayColor];
}

//#if 0
/**
 * Asks the delegate for a border color in unselected state for the specific date.
 */
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderDefaultColorForDate:(NSDate *)date;
{
    NSDateFormatter *formatter = [[NSDate class] sharedDateFormatter];
    NSString *currentDateStr = [formatter stringFromDate:date];
//    NSDate *currentDate = [NSDate date];
 
    NSNumber *count = self.viewModel.advSumDictionary[currentDateStr];
    if (count == nil) {
//        return cell;
        return [UIColor lightGrayColor];
    }
    else {
        NSInteger iCount = [count integerValue];
        if (iCount > 0) {
            return NAVBAR_BGCOLOR;
//            cell.badgeLabel.hidden = NO;
//            cell.badgeLabel.text = [count stringValue];
        }
        else {
            return [UIColor lightGrayColor];
//            cell.badgeLabel.hidden = YES;
        }
    }
    
//    NSInteger compareResult = [DateUtil compareOneDay:todayDate withAnotherDay:date];
//    if (compareResult <= 0) {
//
//        __block UIColor *borderDefaultColor;
//        [self.viewModel.saleAdvSumList enumerateObjectsUsingBlock:^(AdvSumModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            NSDateFormatter *formatter = [[NSDate class] sharedDateFormatter];
//            NSString *dateStr = [formatter stringFromDate:date];
//            if ([obj.date isEqualToString:dateStr]) {
//                NSInteger count = [obj.count integerValue];
//                if (count > 0) {
//                    borderDefaultColor = NAVBAR_BGCOLOR;
//                    *stop = YES;
//                }
//                else {
//                    borderDefaultColor = [UIColor lightGrayColor];
//                    *stop = YES;
//                }
//            }
//        }];
//        return borderDefaultColor;
//    }
//    else {
//        return [UIColor lightGrayColor];
//    }
    return [UIColor lightGrayColor];
}
//#endif

#pragma mark - FSCalendarDelegate

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar;
{
    
    NSDate *date = calendar.currentPage;
    DLog(@"calendarCurrentPageDidChange:date = %@", [date stringWithDateFormat:@"yyyy-MM-dd"]);
//    NSInteger compareResult = [[NSDate date] compare:date];
//    DLog(@"compareResult = %d", compareResult);
//    if (compareResult == NSOrderedAscending || compareResult == NSOrderedSame) {
    if (![[NSDate date] isPreviousMonth:date]) {
        if (_cellDelegate && [_cellDelegate respondsToSelector:@selector(onSelectMonth:)]) {
            [_cellDelegate onSelectMonth:date];
        }
    }
}

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    calendar.frame = (CGRect){calendar.frame.origin,bounds.size};
    
//    self.eventLabel.frame = CGRectMake(0, CGRectGetMaxY(calendar.frame)+10, self.view.frame.size.width, 50);
    
}

- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSDate *todayDate = [NSDate date];
    NSInteger compareResult = [DateUtil compareOneDay:todayDate withAnotherDay:date];
    if (compareResult > 0) {
        return NO;
    }
    else {
        
        
//       __block BOOL shouldSelect = NO;
        NSDateFormatter *formatter = [[NSDate class] sharedDateFormatter];
        NSString *dateStr = [formatter stringFromDate:date];
        
        NSNumber *count = self.viewModel.advSumDictionary[dateStr];
        if (count == nil) {
            return NO;
        }
        else {
            NSInteger iCount = [count integerValue];
            if (iCount > 0) {
                return YES;
            }
        }
        
//        [self.viewModel.saleAdvSumList enumerateObjectsUsingBlock:^(AdvSumModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            
//            if ([obj.date isEqualToString:dateStr]) {
//                NSInteger count = [obj.count integerValue];
//                if (count > 0) {
//                    shouldSelect = YES;
//                    *stop = YES;
//                }
//                else {
//                    shouldSelect = NO;
//                    *stop = YES;
//                }
//            }
//        }];
        
//        return NO;

    }
    
    return NO;
    
//    return shouldSelect;
//    return monthPosition == FSCalendarMonthPositionCurrent;
}

- (BOOL)calendar:(FSCalendar *)calendar shouldDeselectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{

    return monthPosition == FSCalendarMonthPositionCurrent;
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSDateFormatter *formatter = [[NSDate class] sharedDateFormatter];
    NSLog(@"did select date %@", [formatter stringFromDate:date]);
    NSString *dateStr = [formatter stringFromDate:date];
    if (_cellDelegate && [_cellDelegate respondsToSelector:@selector(onClickDate:)]) {
        [_cellDelegate onClickDate:dateStr];
    }
}

- (void)calendar:(FSCalendar *)calendar didDeselectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
//    NSLog(@"did deselect date %@",[self.dateFormatter stringFromDate:date]);
//    [self configureVisibleCells];
    
//    DLog(@"did ")
    
    
    
    NSDateFormatter *formatter = [[NSDate class] sharedDateFormatter];
    NSString *dateStr = [formatter stringFromDate:date];
    if (_cellDelegate && [_cellDelegate respondsToSelector:@selector(onDeselectDate:)]) {
        [_cellDelegate onDeselectDate:dateStr];
    }
    
}

//- (NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventDefaultColorsForDate:(NSDate *)date
//{
//    if ([self.gregorian isDateInToday:date]) {
//        return @[[UIColor orangeColor]];
//    }
//    return @[appearance.eventDefaultColor];
//}



@end
