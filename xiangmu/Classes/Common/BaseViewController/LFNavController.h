//
//  LFNavController.h
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface LFNavController : BaseViewController
//- (void)showHUDIndicatorViewAtCenter:(NSString*)indiTitle;
//- (void)hideHUDIndicatorViewAtCenter;
//- (void)showAlertView:(NSString*)msg;
//添加导航上面的按钮
-(UIButton *)addNavBtn:(CGRect)frame title:(NSString *)title target:(id)target action:(SEL)action isLeft:(BOOL)isLeft;

//添加导航上面的文字
-(UILabel *)addNavTitle:(CGRect)frame title:(NSString *)title;
@end
