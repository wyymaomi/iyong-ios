//
//  AccountInfoView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AccountInfoView.h"
#import "AccountInfoTableView.h"
#import "FirstCollectionViewCell.h"
#import "BBAlertView.h"

@interface AccountInfoView ()<EnterpriseUserCollectionViewDelegate>

@property (nonatomic, strong) UIView        *bgView;
@property (nonatomic, assign) double        *bgViewWidth;


@property (nonatomic, strong) UIWindow      *maskWindow;
@property (nonatomic, strong) UIView        *maskView;

@property (nonatomic, strong) UIView        *subBgView;
@property (nonatomic, strong) UIButton      *collapseButton;
@property (nonatomic, strong) UILabel       *titleLabel;
//@property (nonatomic, strong) NSArray       *subviewCellList;
@property (nonatomic, assign) BOOL          isBottom;// 是否处于底部





@end

//static CGFloat accountInfoViewWidth = ViewWidth - ViewWidth/3

@implementation AccountInfoView

//- (NSArray *)subviewCellList
//{
//    if (IsNilOrNull(_subviewCellList)) {
//        // 加载plist中的字典数组
//        NSString *path = [[NSBundle mainBundle] pathForResource:HOME_CELL_PLIST ofType:nil];
//        _subviewCellList = [NSArray arrayWithContentsOfFile:path];
//    }
//    return _subviewCellList;
//}


- (EnterpriseUserCollectionView*)collectionView
{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *flowlayout=[[UICollectionViewFlowLayout alloc]init];
        [flowlayout setItemSize:CGSizeMake((ViewWidth-(ViewWidth/3)-2)/3, 78)];
        [flowlayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        //flowlayout.sectionInset=UIEdgeInsetsMake(0, 0, 0, 0);
        flowlayout.minimumInteritemSpacing = 0.5f ;//最小行间距,(当垂直布局时是行间距,当水平布局时是列间距)
        flowlayout.minimumLineSpacing = 0.5f ;//两个单元格之间的最小间距
        
        _collectionView = [[EnterpriseUserCollectionView alloc]initWithFrame:CGRectMake(0, self.titleLabel.bottom + 10, ViewWidth-(ViewWidth/3), 78*4) collectionViewLayout:flowlayout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.customDelegate = self;
//        _collectionView.backgroundColor = [UIColor blackColor];
//        _collectionView.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
//        [_collectionView registerClass:[FirstCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
//        _collectionView.showsVerticalScrollIndicator=NO;
//        
//        _collectionView.delegate=self;
//        _collectionView.dataSource=self;
//        _collectionView.scrollEnabled=NO;

    }
    
    return _collectionView;
}

- (UIButton*)collapseButton
{
    if (!_collapseButton) {
        
        _collapseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _collapseButton.frame = CGRectMake((ViewWidth-(ViewWidth/3)-20)/2,12, 20, 20);
        [_collapseButton setBackgroundImage:[UIImage imageNamed:@"icon_up_arrow"] forState:UIControlStateNormal];
        [_collapseButton addTarget:self action:@selector(onCollapseButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return _collapseButton;
}

-(UILabel*)titleLabel
{
    if (!_titleLabel) {
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.collapseButton.bottom + 10, ViewWidth-(ViewWidth/3), 20)];
        _titleLabel.text = @"企业用户";
        _titleLabel.textColor = [UIColor darkGrayColor];
        _titleLabel.textAlignment = UITextAlignmentCenter;
        _titleLabel.font = FONT(12);
        _titleLabel.backgroundColor = [UIColor clearColor];
    }
    
    return _titleLabel;
}

-(UIView*)subBgView
{
    if (!_subBgView) {
        _subBgView = ({
            UIView *view = [[UIView alloc] initWithFrame: CGRectMake(0, ViewHeight-78-72, ViewWidth-(ViewWidth/3), 4 * 78 + 100)];
            view.backgroundColor = UIColorFromRGB(0xF7F7F7);
            view.clipsToBounds = YES;
            view;
            
        });
        
//        _collapseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        
    }
    return _subBgView;
}

- (AccountInfoTableView*)accountTableView
{
    if (_accountTableView == nil) {
        _accountTableView = [[AccountInfoTableView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth - (ViewWidth/3), ViewHeight) style:UITableViewStylePlain];
    }
    return _accountTableView;
}

- (UIWindow *)maskWindow
{
    if (!_maskWindow) {
        
        _maskWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        //edited by gjf 修改alertview leavel
        _maskWindow.windowLevel = UIWindowLevelStatusBar + BBAlertLeavel;
        _maskWindow.backgroundColor = [UIColor clearColor];
        _maskWindow.hidden = YES;
        
        UIView *maskView = ({
            
            UIView *view = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
            view.backgroundColor = [UIColor clearColor];
//            view.alpha = 0.9;
            view;
        });
        [_maskWindow addSubview:maskView];
        _maskView = maskView;
        
        
        _bgView = ({
            
            UIView *view =[[UIView alloc]initWithFrame: CGRectMake(ViewWidth/3, 0, ViewWidth-(ViewWidth/3), ViewHeight)];
            view.backgroundColor = UIColorFromRGB(0xF7F7F7);
            view.clipsToBounds = YES;
            view.layer.borderWidth=1;
            view;
            
        });
        
        [_maskWindow addSubview:_bgView];
        
    }
    return _maskWindow;
}


#pragma mark - life cycle

-(id)init
{
    self = [super init];
    
    if (self) {
        _isNeedAnimation = YES;
        _isVisible = NO;
    }
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
//    _bgViewWidth = 1.0;
//    self.bgViewWidth = self.frame.size.width-self.frame.size.width/3;
//    self.bgViewWidth = (CGFloat)(ViewWidth*1.0-ViewWidth*1.0/3.0);
    
}

- (void)onCollapseButtonClick
{
    _isBottom = !_isBottom;
    
    WeakSelf
    if (_isBottom) {
        [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
           weakSelf.subBgView.top = 154;
            weakSelf.collapseButton.transform = CGAffineTransformRotate(weakSelf.collapseButton.transform, -M_PI);
        } completion:^(BOOL finished) {
            //        [self.mj_popupViewController viewDidAppear:NO];
        }];
        
    }
    else {
        
        [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            weakSelf.subBgView.top = ViewHeight-78-72;
            weakSelf.collapseButton.transform = CGAffineTransformRotate(weakSelf.collapseButton.transform, M_PI);
        } completion:^(BOOL finished) {
            //        [self.mj_popupViewController viewDidAppear:NO];
        }];
        
    }
    
}

- (void)show
{
//    if (_isVisible) {
//        return;
//    }
    
    _isVisible = YES;
    
    [self.maskWindow makeKeyAndVisible];
    
    _maskView.userInteractionEnabled = YES;
    UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDismiss:)];
//    sigleTapRecognizer.numberOfTapsRequired = 1;
    [_maskView addGestureRecognizer:sigleTapRecognizer];
    
    [_bgView addSubview:self.accountTableView];
    
    [self.subBgView addSubview:self.collapseButton];
//    self.collapseButton.top = ViewHeight-78-40;
    [self.subBgView addSubview:self.titleLabel];
    [self.subBgView addSubview:self.collectionView];
    
    [_bgView addSubview:_subBgView];
    
    _bgView.left = ViewWidth;
    
//    popupView.frame = popupStartRect;
//    popupView.alpha = 1.0f;
    if (self.isNeedAnimation) {
        WeakSelf
        [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            //        [self.mj_popupViewController viewWillAppear:NO];
            //        self.mj_popupBackgroundView.alpha = 1.0f;
            //        popupView.frame = popupEndRect;
            weakSelf.bgView.left = ViewWidth/3;
        } completion:^(BOOL finished) {
            //        [self.mj_popupViewController viewDidAppear:NO];
        }];
    }
    else {
        self.bgView.left = ViewWidth/3;
    }

    
}

- (void)dismiss
{
    
    if (!_isVisible) {
        return;
    }
    
    [_subBgView removeAllSubviews];
    
    [_maskView removeFromSuperview];
    [_bgView removeFromSuperview];
    _maskWindow = nil;
}


//- (void)show:(NSArray*)images borderColor:(UIColor*)color
//{
//    [self.maskWindow makeKeyAndVisible];
//    
//    _maskView.userInteractionEnabled = YES;
//    UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDismiss:)];
//    sigleTapRecognizer.numberOfTapsRequired = 1;
//    [_maskView addGestureRecognizer:sigleTapRecognizer];
//}

- (void)onDismiss:(id)sender
{

    if (!_isVisible) {
        return;
    }
    
    [_collectionView removeAllSubviews];
    [_collectionView removeFromSuperview];
    _collectionView = nil;
    
    [self.subBgView removeAllSubviews];
    [_maskView removeFromSuperview];
    [_bgView removeFromSuperview];
    _maskWindow = nil;
}

-(void)refreshOrderCount:(NSInteger)orderCount
{
    self.accountTableView.orderCount = orderCount;
    [self.accountTableView reloadData];
    self.collectionView.orderCount = orderCount;
    [self.collectionView reloadData];
}

- (void)refreshPartnerNumber:(NSInteger)partnerNumber
{
    self.collectionView.applyPartnerBadgeNumber = partnerNumber;
    [self.collectionView reloadData];
}

- (void)onGotoPage:(NSString*)vcPage title:(NSString*)title;
{
    if (_delegate && [_delegate respondsToSelector:@selector(onGotoPage:title:)]) {
        [_delegate onGotoPage:vcPage title:title];
    }
}
//
//#pragma mark - UICollectionView Delegate method
//
//-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
//{
//    return 1;
//}
//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
//{
//    return self.subviewCellList.count;
//}
//
//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    FirstCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
//    
//    NSDictionary *cellDict = self.subviewCellList[indexPath.row];
//    
//    cell.topImageView.image=[UIImage imageNamed:cellDict[@"icon"]];
//    cell.topImageView.frame = CGRectMake((cell.frame.size.width-36)/2, 10, 36, 36);
//    cell.detailLabel.text=cellDict[@"text"];
//    cell.detailLabel.font = FONT(10);
//    cell.detailLabel.textColor = [UIColor darkGrayColor];
//    cell.detailLabel.top = cell.topImageView.bottom + 5;
////    cell.backgroundColor=[UIColor whiteColor];
//    //cell点击变色
////    UIView *selectedBGView=[[UIView alloc]initWithFrame:cell.bounds];
////    selectedBGView.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
////    cell.selectedBackgroundView=selectedBGView;
//    
//    if (indexPath.row == 1) {
////        [cell showBadgeValue:self.orderCountBadgeNumber];
//    }
//    else if (indexPath.row == 5) {
////        AppDelegate *delegate = APP_DELEGATE;
////        [cell showBadgeValue:[NSString stringWithFormat:@"%ld", (unsigned long)delegate.applyPartnerBadgeNumber]];
//    }
//    else {
//        [cell hideBadge];
//    }
//
//    
//    return cell;
//}
//////cell点击变色
//-(BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
/////cell点击变色
//-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}
//-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    NSDictionary *cellDict = self.subviewCellList[indexPath.row];
//    NSString *viewController = cellDict[@"vc"];
//    
//    if ([viewController isEqualToString:@""]) {
//        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该系统暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        [alertView show];
//    }
//    else {
////        if (_delegate && [_delegate respondsToSelector:@selector(onGotoPage:)]) {
////            [_delegate onGotoPage:viewController];
////        }
//        if (_delegate && [_delegate respondsToSelector:@selector(onGotoPage:title:)]) {
//            [_delegate onGotoPage:viewController title:cellDict[@"text"]];
//        }
//    }
//    
//}


@end
