//
//  HomeExperienceCollectionView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeExperienceCollectionViewDelegate <NSObject>

- (void)onClickExperienceItem:(NSInteger)index;

@end

@interface HomeExperienceCollectionView : UICollectionView<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) NSMutableArray *dataList;

@property (nonatomic, weak) id<HomeExperienceCollectionViewDelegate> customDelegate;

@end
