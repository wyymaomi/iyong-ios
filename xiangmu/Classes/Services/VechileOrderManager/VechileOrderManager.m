//
//  VechileOrderManager.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/28.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileOrderManager.h"
#import "UIViewController+TggAlertExtension.h"
#import "VechileOrderHttpMessage.h"
#import "CarDescModel.h"

@implementation VechileOrderManager

+ (instancetype)sharedInstance
{
    static VechileOrderManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[VechileOrderManager alloc] init];
    });
    return instance;
}

-(NSArray<CarDescModel*>*)getVechileDescTypeList;
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"cartype.plist" ofType:nil];
    NSArray *carTypeList = [NSArray arrayWithContentsOfFile:path];
    
    NSMutableArray *carList = [NSMutableArray new];
    
    [carTypeList enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
        
        CarDescModel *model = [[CarDescModel alloc] initWithDictionary:dict error:nil];
        [carList addObject:model];
        
        
    }];
    
    return [carList mutableCopy];
}

-(VechileOrderHttpMessage*)vechileOrderHttpMessage
{
    if (!_vechileOrderHttpMessage) {
        _vechileOrderHttpMessage = [VechileOrderHttpMessage new];
    }
    return _vechileOrderHttpMessage;
}

-(void)contactDriver:(NSString*)mobile;
{
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",mobile];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(BOOL)isPendingPayOvertime
{
    
    if (self.vechileOrderHttpMessage.receiveOrderTime == 0) {
        return NO;
    }
    
//    return NO;
    NSTimeInterval interval = 15 * 60;
    NSDate *driverReceiveOrderDate = [NSDate dateWithTimeIntervalSince1970:self.vechileOrderHttpMessage.receiveOrderTime/1000.0f];
    NSDate *payOvertime = [[NSDate alloc] initWithTimeInterval:interval sinceDate:driverReceiveOrderDate];
    NSTimeInterval payOverTimstamp = [payOvertime timeIntervalSince1970];
    NSTimeInterval currentTimestamp = [[NSDate date] timeIntervalSince1970];
    
    
    if (currentTimestamp >= payOverTimstamp) {
        return YES;
    }
    
    return NO;
    
}

/**
 *
 *
 *
 */
-(void)cancelOrder:(NSString*)orderId carOrderMode:(NSUInteger)carOrderMode viewController:(UIViewController*)viewController isCompletePay:(BOOL)isCompletePay
{
    
    // 未支付时取消预约提示“是否取消预约”
    if (!isCompletePay) {
        
        
        
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"是否取消当前用车订单" message:nil delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"确定", nil];
            WeakSelf
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 1) {
                    [weakSelf doCancelOrder:orderId viewController:viewController];
                }
                
            }];
            return;
        
        
        
    }
    
    //用户取消订单时如果是实时订单不要弹出取消扣费的提示框
    
    if (carOrderMode == CarModeImmediate) {
        [self doCancelOrder:orderId viewController:viewController];
        return;
    }
    
    [viewController showHUDIndicatorViewAtCenter:@""];
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(CANCEL_CAR_ORDER_REMIND_API) requestMethod:POST params:nil success:^(id responseData) {
        
        [viewController hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *data = responseData[@"data"];
            
            NSMutableString *message = [NSMutableString new];
            
            NSInteger count = data.count;
            for (NSDictionary *dict in data) {
                [message appendFormat:@"出发前%@分钟取消订单将扣除支付金额%@%%", dict[@"ratio"], dict[@"time"]];
                count--;
                if (count > 0) {
                    [message appendString:@"\n"];
                }

            }
            
#if 1
            
            
            
            WeakSelf
            
            [viewController tgg_presentAlertViewWithMainTitle:nil TextLeftMessage:message firstAction:@"返回" secondAction:@"确定" successBlock:^(NSUInteger selectedIndex) {
                
                if (selectedIndex == 0) {
//                    [weakSelf doCancelOrder:orderId];
                }
                else {
                    [weakSelf doCancelOrder:orderId viewController:viewController];
                }
                
            }];
            
#endif
            
        }
        else {
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        [viewController hideHUDIndicatorViewAtCenter];
        [MsgToolBox showToast:errorDesc];
        
    }];
}



-(void)doCancelOrder:(NSString*)orderId viewController:(UIViewController*)viewController
{
    
    NSString *params = [NSString stringWithFormat:@"orderId=%@", orderId];
    
    [viewController showHUDIndicatorViewAtCenter:@"正在取消预约..."];
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(CANCEL_CAR_ORDER_API) requestMethod:POST params:params success:^(id responseData) {
        
        [viewController hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            [MsgToolBox showToast:@"取消预约成功"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kOnCancelCarOrderSuccessNotification object:nil];
            
        }
        else {
            
            [MsgToolBox showToast:getErrorMsg(code_status)];
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [viewController hideHUDIndicatorViewAtCenter];
        
        [MsgToolBox showToast:errorDesc];
        
    }];
    
}

@end
