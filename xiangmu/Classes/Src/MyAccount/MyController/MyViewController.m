//
//  MyViewController.m
//  load
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//
//不是最新的
#import "MyViewController.h"

#import "MyTableViewCell.h"
#import "MeduTableViewCell.h"
#import "MyUtil.h"
#import "SaveAndTake.h"
#import "NetworkManager.h"
#import "HttpConstants.h"
#import "UserManager.h"
#import "BankcardViewController.h"
//#import "MyBillManagerViewController.h"
#import "DiaoduViewController.h"
#import "ForgetPasswordViewController.h"
#import "UserInfoViewController.h"
//#import "MycardViewController.h"
//#import "RecommendViewController.h"
#import "PasswordManagerTableViewController.h"
#import "AccountBalanceViewController.h"
#import "PasswordSetViewController.h"
#import "GesturePasswordController.h"
#import "RewardViewController.h"
//#import "PayViewController.h"
#import "TradeRecordViewController.h"
#import "CreditLineViewController.h"
#import "BillPayViewController.h"
#import "MyBusinessCircleViewController.h"
#import "RealnameCertificationViewController.h"
#import "AppDelegate.h"
#import "BBAlertView.h"
#import "StarView.h"
#import "CertifiedView.h"
#import "MyTableViewCell+Additional.h"
//#import "CertifiedView.h"


@interface MyViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,MyDetailViewControllerDelegate>

{
    NSMutableArray *_nameArray;
    NSDictionary *_userData;
    MeduTableViewCell *_cell;
    //    NSDictionary *dicArray;
    NSNumber *_creditLine;// 信用额度
    NSNumber *_creditAmount;// 信用余额
    NSNumber *_accountAmount;// 账户余额
    NSString *_logoUrl;// 公司LOGO
}
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic, assign) NSInteger level; // 账号level
@property (nonatomic, strong) NSMutableDictionary *imageDictionary;

@end

@implementation MyViewController

-(NSMutableDictionary*)imageDictionary
{
    if (_imageDictionary == nil) {
        _imageDictionary = [NSMutableDictionary new];
    }
    return _imageDictionary;
}


#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我";
    [self createTableView];
    [self getAccountInfo];
    
    avatarImg = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-self.view.frame.size.width/8, 30, self.view.frame.size.width/4, self.view.frame.size.width/4)];
    avatarImg.userInteractionEnabled=YES;
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"imagePath"])
    {
        NSString *imagePath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"] stringByAppendingPathComponent:@"current.png"];
        
        NSLog(@"%@",[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"] stringByAppendingPathComponent:@"current.png"]);
        
        avatarImg.image = [UIImage imageWithContentsOfFile:imagePath];
    }
    else
    {
        avatarImg.image=[UIImage imageNamed:@"icon_avatar"];
    }
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getAccountInfo];
    //    [self getFinanceInfo];
}


#pragma mark - business method

-(void)getAccountInfo
{
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, My_Detail_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        NSLog(@"response success");
        NSInteger code_status = [responseData[@"code"] integerValue];
        if(code_status == STATUS_OK) {
            
        }
        _userData  = responseData[@"data"];
        strongSelf.level = [_userData[@"level"] integerValue];
        UserModel *userModel = [[UserModel alloc] initWithDictionary:responseData[@"data"] error:nil];
        [UserManager sharedInstance].userModel = userModel;
        
        [strongSelf.tableView reloadData];
        
        // 如果logoUrl不空则下载图片
        NSString *logoUrl = _userData[@"logoUrl"];
        if (!IsStrEmpty(logoUrl)) {
            [strongSelf downloadAvartarImage:logoUrl];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"网络连接错误，请重试" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
        //        [alert show];
        //        return;
        
    }];
    
}

-(void)downloadAvartarImage:(NSString*)imgUrl
{
    NSString *convertImgUrl = [imgUrl replaceAll:@"/" with:@"-"];
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            [strongSelf.imageDictionary setValue:imageData forKey:convertImgUrl];
            [strongSelf.tableView reloadData];
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}



-(void)createTableView
{
    _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-114) style:UITableViewStyleGrouped];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.scrollEnabled=YES;
    _tableView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    [self.view addSubview:_tableView];
}

//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}


#pragma mark ----UITableView代理

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.level == 1) {
        return 4;
    }
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 5;
    }
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return 60;
    }
    return 40;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //  MyTableViewCell *cell=(MyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    //    if (self.level == 1) {
    if (indexPath.section==0) {
        UserInfoViewController *mydetailtabelVC=[[UserInfoViewController alloc]init];
        mydetailtabelVC.personDelegate = self;
        mydetailtabelVC.indexpath = indexPath;
        mydetailtabelVC.hidesBottomBarWhenPushed = YES;
        
        NSString *logoUrl =  _userData[@"logoUrl"];
        if (!IsStrEmpty(logoUrl)) {
            NSString *convertImageUrl = [logoUrl replaceAll:@"/" with:@"-"];
            NSData *imageData = self.imageDictionary[convertImageUrl];
            if (imageData != nil && imageData.length > 23) {
                mydetailtabelVC.avatarImageData = imageData;
            }
        }
        else {
            
        }
        
        [self.navigationController pushViewController:mydetailtabelVC animated:YES];
        
    }
    else {
        if (self.level == 1) {
            
            switch (indexPath.section) {
                case 1:
                {
                    if (indexPath.row == 2) {
                        RealnameCertificationViewController *viewController = [[RealnameCertificationViewController alloc] init];
                        viewController.type = [[UserManager sharedInstance].userModel.certification boolValue]?0:1;
                        viewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:viewController animated:YES];
                    }
                    if (indexPath.row == 3) {
                        [self gotoPage:@"CompanyCertificationViewController"];
                    }
                    if (indexPath.row == 4) {
                        [self gotoPage:@"CarTableViewController"];
                    }
                    
                }
                    break;
                case 2:
                {
                    [self gotoPage:@"MyAccountViewController"];

                }
                    break;
                case 3:
                {
                    [self gotoPage:@"SettingViewController"];
                }
                    
                    break;
            }
        }
        else {
            
            switch (indexPath.section) {
                case 1:
                {
                    if (indexPath.row == 2) {
                        RealnameCertificationViewController *viewController = [[RealnameCertificationViewController alloc] init];
                        viewController.type = [[UserManager sharedInstance].userModel.certification boolValue]?0:1;
                        [self.navigationController pushViewController:viewController animated:YES];
                    }
                    if (indexPath.row == 3) {
                        [self gotoPage:@"CompanyCertificationViewController"];
                    }
                    if (indexPath.row == 4) {
                        [self gotoPage:@"CarTableViewController"];
                    }
                }
                    break;
                case 2:
                {
                    [self gotoPage:@"SettingViewController"];
                }
                    
                    break;
                case 3:
                    
                    break;
            }
        }
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        static NSString *cellid = @"accountInfoCellID";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        
        
        if (cell==nil) {
        
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            //显示最右边的箭头
            //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            //            UIImageView *rightImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-15, 23, 23/3, 42/3)];
            //            rightImageView.image = [UIImage imageNamed:@"icon_disclosure"];
            //            [cell.contentView addSubview:rightImageView];
            
            UIImageView *leftImageView=[[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 40, 40)];
            leftImageView.tag = 110;
            [cell.contentView addSubview:leftImageView];
            
            
            UILabel *firstlabel=[MyUtil createLabelFrame:CGRectMake(70, 10, 60, 20) title:nil font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
            
            firstlabel.tag = 111;
            [cell.contentView addSubview:firstlabel];
            
            UILabel *secondlabel=[MyUtil createLabelFrame:CGRectMake(140, 10, 100, 20) title:nil font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
            secondlabel.tag = 112;
            [cell.contentView addSubview:secondlabel];
            
            UILabel *thirdlabel=[MyUtil createLabelFrame:CGRectMake(70, 32, 260, 20) title:nil font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
            thirdlabel.tag = 113;
            [cell.contentView addSubview:thirdlabel];
        }
    
        
        UIImageView *headImage = (UIImageView *)[cell.contentView viewWithTag:110];
        UILabel *nickLbl = (UILabel *)[cell.contentView viewWithTag:111];
        UILabel *usernameLbl = (UILabel *)[cell.contentView viewWithTag:112];
        UILabel *companyLbl = (UILabel *)[cell.contentView viewWithTag:113];
        
        nickLbl.text = [_userData valueForKey:@"nickname"];
        usernameLbl.text = [_userData  valueForKey:@"username"];
        companyLbl.text = [_userData  valueForKey:@"companyName"];
        
        NSString *logoUrl =  _userData[@"logoUrl"];
        if (IsStrEmpty(logoUrl)) {
            headImage.image = [UIImage imageNamed:@"icon_avatar"];
            headImage.cornerRadius = 0;
        }
        else {
            NSString *convertImageUrl = [logoUrl replaceAll:@"/" with:@"-"];
            NSData *imageData = self.imageDictionary[convertImageUrl];
            if (imageData != nil && imageData.length > 23) {
                headImage.image = [UIImage imageWithData:imageData];
                headImage.cornerRadius = 20;
            }
            else {
                headImage.image = [UIImage imageNamed:@"icon_avatar"];
                headImage.cornerRadius = 0;
            }
        }
        
        return cell;
    }
    else
    {
        
        static NSString *cellid=@"cellId";
        MyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
        
//        if (cell == nil) {
            cell = [[MyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
//            if (indexPath.section == 1) {
//                
//            }
//        }
        
        
        if (indexPath.section == 1) {
            if (indexPath.row == MeCertificationRowLevel) {
                [cell initLevelData];
            }
            if (indexPath.row == MeCertificationRowCertificationInfo) {
                [cell initCertificationData];
            }
            if (indexPath.row == MeCertificationRowRealname) {
                [cell initRealnameCertifcationData];
            }
            if (indexPath.row == MeCertificationRowCompany) {
                [cell initCompanyCertificationData];
            }
            if (indexPath.row == MeCertificationRowVechile) {
                [cell initVechileCertificationData];
            }
            
        }
        if (self.level == 1) {
            if (indexPath.section == 2) {
                cell.name.text = @"我的账户";
            }
            if (indexPath.section == 3) {
                cell.name.text = @"设置";
            }
            
            return cell;
        }
        else {
            if (indexPath.section == 2) {
                cell.name.text = @"设置";
            }
            
            return cell;
        }
        
    }
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

-(void)gotoWrite
{
    NSLog(@"改写");
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(100, 10, ViewWidth-100-55, 20)];
    textField.font=[UIFont systemFontOfSize:13];
    textField.tag=100;
    textField.layer.borderColor=[[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f]CGColor];
    textField.layer.borderWidth=1;
    MyTableViewCell *cellView=(MyTableViewCell *)[_tableView cellForRowAtIndexPath:indexPath];
    [cellView addSubview:textField];
}
-(void)gotoChange
{
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    UITextField *text = [self.view viewWithTag:100];
    [text resignFirstResponder];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    UITextField *text5=[self.view viewWithTag:100];
    [text5 resignFirstResponder];
    return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //    [self downloadData];
    //    [_tableView reloadData];
    self.tabBarController.tabBar.hidden=NO;
}

//刷新个人信息
- (void)refreshMyInfo:(NSIndexPath *)indexpath
{
    [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationFade];
}

@end
