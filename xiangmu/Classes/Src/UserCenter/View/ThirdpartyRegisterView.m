//
//  ThirdpartyRegisterView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/9.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ThirdpartyRegisterView.h"

@implementation ThirdpartyRegisterView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.closeButton.frame = CGRectMake(15, 18, 15, 15);
    
    self.bingPhoneLabel.frame = CGRectMake(30, 110, ViewWidth-60,22);
    self.bingPhoneLabel.textColor = [UIColor whiteColor];
    self.bingPhoneLabel.font = FONT(20);
    
    self.accountView.frame = CGRectMake(60*Scale, self.bingPhoneLabel.bottom+60*Scale, ViewWidth-120*Scale, 32*Scale);
    self.accountImgView.frame = CGRectMake(0, 0, 22, 22);
    self.accountTextField.textColor = [UIColor whiteColor];
    self.accountTextField.font = FONT(16);
    self.accountTextField.placeholder = @"请输入手机号";
    self.accountTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.accountTextField.frame = CGRectMake(self.accountImgView.right+40*Scale, 0, self.accountView.width-self.accountImgView.right-40*Scale, self.accountView.height);
    self.accountLineView.frame = CGRectMake(0, self.accountView.height-0.5, self.accountView.width, 0.5);
    self.accountLineView.backgroundColor = [UIColor whiteColor];
    
    self.verifyCodeView.frame = CGRectMake(self.accountView.left, self.accountView.bottom+32*Scale, self.accountView.width, 32*Scale);
    self.verifyCodeIconView.frame = self.accountImgView.frame;
    self.verifyCodeTextField.frame = self.accountTextField.frame;
    self.verifyCodeTextField.textColor = [UIColor whiteColor];
    self.verifyCodeTextField.font = FONT(16);
    self.verifyCodeTextField.placeholder = @"请输入验证码";
    self.verifyCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.verifyCodeLineView.frame = self.accountLineView.frame;
    self.accountLineView.backgroundColor = [UIColor whiteColor];
    self.verifyCodeButton.frame = CGRectMake(self.verifyCodeView.width-70*Scale, 0, 70*Scale, 25*Scale);
    [self.verifyCodeButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.verifyCodeButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    [self.verifyCodeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
    self.verifyCodeButton.cornerRadius = 10;
    [self.verifyCodeButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    [self.verifyCodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    // 推荐码
    self.recommendCodeView.frame = CGRectMake(self.accountView.left, self.verifyCodeView.bottom+25*Scale, self.accountView.width, self.accountView.height);
    self.recommendCodeIconImageView.frame = self.accountImgView.frame;
    self.recommendCodeTextField.frame = self.accountTextField.frame;
    self.recommendCodeTextField.textColor = [UIColor whiteColor];
    self.recommendCodeTextField.font = FONT(16);
    self.recommendCodeTextField.placeholder = @"推荐码(可选择)";
    self.recommendCodeLineView.frame = CGRectMake(0, self.recommendCodeView.height-0.5, self.recommendCodeView.width, 0.5);
    self.recommendCodeLineView.backgroundColor = [UIColor whiteColor];
    
    self.registerButton.frame = CGRectMake(36*Scale, self.recommendCodeView.bottom+100*Scale, ViewWidth-72*Scale, 42*Scale);
    [self.registerButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.registerButton.titleLabel.font = FONT(20);
    [self.registerButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.registerButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    self.registerButton.cornerRadius = 20;
    [self.registerButton setTitle:@"立即绑定" forState:UIControlStateNormal];
    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.registerButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];

}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
