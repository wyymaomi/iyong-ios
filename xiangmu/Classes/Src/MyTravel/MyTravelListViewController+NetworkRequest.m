//
//  MyTravelListViewController+NetworkRequest.m
//  iYONGDriver
//
//  Created by 湛思科技 on 2017/7/7.
//  Copyright © 2017年 Shanghai Zhansi Tech. All rights reserved.
//

#import "MyTravelListViewController+NetworkRequest.h"

@implementation MyTravelListViewController (NetworkRequest)

-(void)fetchTraveList:(NSString*)time driverId:(NSString*)driverId
{
    if (IsStrEmpty(time)) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    NSString *params = [NSString stringWithFormat:@"time=%@&driverId=%@&companyId=%@", [StringUtil getSafeString:time], driverId,[UserManager sharedInstance].userModel.companyId];
    
    self.loadStatus = LoadStatusInit;
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(TRAVEL_LIST_API) requestMethod:POST params:params success:^(NSDictionary * responseData) {
        
        [weakSelf.pullRefreshTableView endHeaderRefresh];
        [weakSelf.pullRefreshTableView endFooterRefresh];
        [weakSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *dataList = responseData[@"data"];
            for (NSDictionary *dictionary in dataList) {
                
                TravelModel *model = [[TravelModel alloc] initWithDictionary:dictionary error:nil];
                [weakSelf.dataList addObject:model];
                
            }
            
            [weakSelf.pullRefreshTableView reloadData];
            
            
        }
        else {
            
            weakSelf.loadStatus = LoadStatusFinish;
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf resetAction];
        weakSelf.loadStatus = LoadStatusNetworkError;
        
        
    }];
    
}

@end
