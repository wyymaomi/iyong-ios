//
//  IdleShareViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface IdleShareViewController : BaseViewController

@property (nonatomic, assign) NSInteger mode;

@end
