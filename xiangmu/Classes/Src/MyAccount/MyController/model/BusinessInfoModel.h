//
//  BusinessInfoModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface BusinessInfoModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *imgs; // 图片数组
@property (nonatomic, strong) NSString<Optional> *companyId; // 公司ID
@property (nonatomic, strong) NSString<Optional> *blurb; // 公司简介
@property (nonatomic, strong) NSString<Optional> *business;
//@property (nonatomic, strong) NSString<Optional> *img1;
//@property (nonatomic, strong) NSString<Optional> *img2;
//@property (nonatomic, strong) NSString<Optional> *img3;
//@property (nonatomic, strong) NSString<Optional> *img4;

@end
