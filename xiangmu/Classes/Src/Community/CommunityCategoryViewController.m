//
//  CommunityCategoryViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityCategoryViewController.h"
#import "CommunityCategoryViewController+Adv.h"
#import "CommunityCategoryViewController+Guide.h"
#import "CommunityCateogryTableViewCell.h"
#import "GestureLoginViewController.h"
#import "LoginViewController.h"
#import "CommunityHelper.h"
#import "MsgToolBox.h"
#import "BBAlertView.h"
#import "AppConfig.h"
#import "GuideView.h"
#import "BBAlertView.h"
#import "AppConfig.h"
#import "GYZChooseCityController.h"

#import "DateUtil.h"
#import "SIDADView.h"
//#import "BaseWebViewController.h"

@interface CommunityCategoryViewController ()<GYZChooseCityDelegate>

@property (nonatomic, strong) NSArray *categoryCellList;
@property (nonatomic, strong) UIButton *button;

@property (nonatomic, strong) SIDADView *adView;// 广告
@property (nonatomic, strong) NSArray *advList; // 广告链接地址

@end

@implementation CommunityCategoryViewController

- (NSArray*)categoryCellList
{
    if (_categoryCellList == nil) {
        _categoryCellList = @[@[@{@"title":@"业务圈", @"icon": @"icon_BusinessCircle"},
                                @{@"title":@"我的询价", @"icon":@"icon_my_inquire"}],
                              @[@{@"title":@"行业闲置共享", @"icon":@"icon_IndustryIdleShare"},
                                @{@"title":@"个人闲置共享", @"icon":@"icon_PersonalIdleShare"}],
                              @[@{@"title":@"附近企业", @"icon":@"icon_NearbyCompany"},
                                @{@"title":@"附近个人", @"icon":@"icon_NearbyPerson"}],
                              @[@{@"title":@"企业招聘", @"icon":@"icon_company_job"},
                                @{@"title":@"个人应聘", @"icon":@"icon_person_job"}]];
    }
    return _categoryCellList;
}

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"社区";
    
    self.isFirstIn = YES;
    
    [self.view addSubview:self.groupTableView];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icon_adv"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(onClickAdvIcon)];
    
    [self setNavTitleView];
    
    NSNumber *value = [[UserDefaults sharedInstance] objectForKey:@"IsShowIntroCommunity"];
    if (value == nil) {
        [self initIntroduceView];
    }
    else {
        [self showAdv:NO];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onReceiveNewBusinessMessage:)
                                                 name:kBussinessCircleNewMessageNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setNavTitleView)
                                                 name:kOnGpsLocationNotification
                                               object:nil];


    

    
}

-(void)setNavTitleView
{

    NSString *defaultCityName = IsStrEmpty([AppConfig currentConfig].defaultCityName)?DEFAULT_CITY_NAME:[AppConfig currentConfig].defaultCityName;
    NSString *titleText = [NSString stringWithFormat:@"社区 · %@ >", defaultCityName];
    CGSize titleTextSize = [titleText textSize:CGSizeMake(ViewWidth, 30) font:BOLD_FONT(18)];
    
    CGFloat titleViewWidth = titleTextSize.width /*+ 23/3*/;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake((ViewWidth - titleViewWidth)/2, 0, titleViewWidth, 30)];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, titleTextSize.width, 30);
    [button setTitle:titleText forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = BOLD_FONT(18);
    [button addTarget:self action:@selector(didSelectCity:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    self.navigationItem.titleView = view;
    
}

- (void)didSelectCity:(id)sender
{
    GYZChooseCityController *cityPickerVC = [[GYZChooseCityController alloc] init];
    [cityPickerVC setDelegate:self];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:cityPickerVC] animated:YES completion:^{
        //
    }];
}

#pragma mark - GYZCityPickerDelegate
- (void) cityPickerController:(GYZChooseCityController *)chooseCityController didSelectCity:(GYZCity *)city
{
    [AppConfig currentConfig].defaultCityName = city.cityName;
    [AppConfig currentConfig].defaultCityCode = city.cityID;
    
    [self setNavTitleView];

//    [self.searchBar.cityButton setTitle:city.cityName forState:UIControlStateNormal];
//    [LocationService saveCityToConfig:city.cityName];
//    self.searchBar.searchTextField.text = @"";
    
    [chooseCityController dismissViewControllerAnimated:YES completion:nil];
}

- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController
{
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}


- (void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    
//    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0xe3c139);

    [self.groupTableView reloadData];
    
#if DEBUG
//    [self convertDateToStringUsingNewDateFormatter];
//    
//    [self convertDateToStringUsingSingletonFormatter];
//
//    [self convertDateToStringUsingCLocaltime];
#endif

}


#pragma mark - didReceiveMemoryWarning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - onReceiveNewBusinessMessage

- (void)onReceiveNewBusinessMessage:(id)sender
{
    [self.groupTableView reloadData];
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.categoryCellList.count;
//    return self.categoryList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = self.categoryCellList[section];
    return array.count;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    CommunityCateogryTableViewCell *cell = [CommunityCateogryTableViewCell cellWithTableView:tableView];
    NSArray *array = self.categoryCellList[indexPath.section];
    NSDictionary *dict = array[indexPath.row];
    cell.titleLabel.text = dict[@"title"];
    cell.iconImageView.image = [UIImage imageNamed:dict[@"icon"]];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        NSInteger msgCount = [[AppConfig currentConfig].businessCircleNumber integerValue];
        if (msgCount > 0) {
            cell.redDotImageView.hidden = NO;
        }
        else {
            cell.redDotImageView.hidden = YES;
        }
    }
//    if (indexPath.section == 1) {
//        cell.iconImageView.width = 18;
//        cell.iconImageView.height = 8;
//        cell.iconImageView.top = ([self tableView:tableView heightForRowAtIndexPath:indexPath] - cell.iconImageView.height)/2;
//    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section  == 0) {
        return 15;
    }
    return 20;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0001f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            NSString *errorMsg;
            BOOL isValidBrowser = [CommunityHelper isValidBusienssCircleBrowser:&errorMsg];
            if (!isValidBrowser) {
//                [MsgToolBox showAlert:@"" content:errorMsg];
                WeakSelf
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请先登录" message:@"" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        if ([[UserManager sharedInstance] isGesturePwdLogin]) {
                            GestureLoginViewController *gesturePwdController = [[GestureLoginViewController alloc] init];
                            gesturePwdController.enter_type = ENTER_TYPE_PRESENT;
//                            gesturePwdController.loginDelegate = self;
                            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:gesturePwdController];
                            [weakSelf presentViewController:nav animated:YES completion:nil];
                        }
                        else {
                            LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                            loginViewController.enter_type = ENTER_TYPE_PRESENT;
//                            loginViewController.loginDelegate = self;
                            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginViewController];
                            [weakSelf presentViewController:nav animated:YES completion:nil];
                        }
                    }
                }];
                return;
            }
            
            [AppConfig currentConfig].businessCircleNumber = @0;
            
            Class cls = NSClassFromString(@"BusinessCircleListViewController");
            BaseViewController *vc = [[cls alloc]init];
            vc.title = @"业务圈";
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        if (indexPath.row == 1) {
            
            if (![[UserManager sharedInstance] isLogin]) {
                WeakSelf
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请先登录" message:@"" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        if ([[UserManager sharedInstance] isGesturePwdLogin]) {
                            GestureLoginViewController *gesturePwdController = [[GestureLoginViewController alloc] init];
                            gesturePwdController.enter_type = ENTER_TYPE_PRESENT;
//                            gesturePwdController.loginDelegate = self;
                            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:gesturePwdController];
                            [weakSelf presentViewController:nav animated:YES completion:nil];
                        }
                        else {
                            LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                            loginViewController.enter_type = ENTER_TYPE_PRESENT;
//                            loginViewController.loginDelegate = self;
                            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginViewController];
                            [weakSelf presentViewController:nav animated:YES completion:nil];
                        }
                    }
                }];
                return;
            }
            Class cls = NSClassFromString(@"MyBusinessCircleViewController");
            BaseViewController *vc = [[cls alloc]init];
            vc.title = @"我的询价";
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }

    }
    else {
        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该系统暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }

}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}


#pragma mark - test

//- (void)convertDateToStringUsingNewDateFormatter
//{
//    then = CFAbsoluteTimeGetCurrent();
//    for (NSUInteger i = 0; i < ITERATIONS; i++) {
//        NSDateFormatter *newDateForMatter = [[NSDateFormatter alloc] init];
//        [newDateForMatter setTimeZone:self.timeZone];
//        [newDateForMatter setDateFormat:@"yyyy-MM-dd"];
//        self.dateAsString = [newDateForMatter stringFromDate:[NSDate date]];
////        NSDate *date = [NSDate date];
////        self.dateAsString = [NSDate nowString];
//    }
//    now = CFAbsoluteTimeGetCurrent();
//    NSLog(@"Convert date to string using NSDateFormatter costs time: %f seconds!\n", now - then);
//}
//
- (void)convertDateToStringUsingSingletonFormatter
{
    then = CFAbsoluteTimeGetCurrent();
    for (NSUInteger i = 0; i < ITERATIONS; i++) {
//        self.dateAsString = [NSDate nowString];//[self.dateFormatter stringFromDate:[NSDate date]];
        self.dateAsString = [DateUtil formatDateTime:@"1477621087" dateFomatter:@"yyyy-MM-dd HH:mm"];
    }
    now = CFAbsoluteTimeGetCurrent();
    NSLog(@"Convert date to string using Singleton Formatter costs time: %f seconds!\n", now - then);
}
//
//
//
//- (void)convertDateToStringUsingCLocaltime
//{
//    then = CFAbsoluteTimeGetCurrent();
//    for (NSUInteger i = 0; i < ITERATIONS; i++) {
//        time_t timeInterval = [NSDate date].timeIntervalSince1970;
//        struct tm *cTime = localtime(&timeInterval);
//        self.dateAsString = [NSString stringWithFormat:@"%d-%02d-%02d", cTime->tm_year + 1900, cTime->tm_mon + 1, cTime->tm_mday];
//        //        NSLog(@"dateAsString = %@", self.dateAsString);
//    }
//    now = CFAbsoluteTimeGetCurrent();
//    NSLog(@"Convert date to string using C localtime costs time: %f seconds!\n", now - then);
//}


@end
