//
//  CommentView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessCircleModel.h"

@interface CommentView : UIView

- (void)updateWithItem:(BusinessCircleModel *)item;

+(CGFloat) getHeight:(BusinessCircleModel *) item maxWidth:(CGFloat)maxWidth;

@end
