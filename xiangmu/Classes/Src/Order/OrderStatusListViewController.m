//
//  OrderStatusListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderStatusListViewController.h"

#import "OrderStatusTableViewCell.h"
#import "OrderListViewController.h"
#import "OrderHelpViewController.h"
#import "OrderCountModel.h"
//#import "ViewController.h"


@interface OrderStatusListViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *orderStatusList;
@property (strong, nonatomic) NSArray *urlList;
@property (strong, nonatomic) OrderCountModel *orderCountModel;

@end

@implementation OrderStatusListViewController

#pragma mark - life cycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self doOrderCountQuery];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"订单";
    if (self.source_from == SOURCE_FROM_TAB_CONTROL) {
        self.hasLeftBackButton = NO;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_ask"] style:UIBarButtonItemStyleDone target:self action:@selector(openHelpViewController)];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
    self.tableView.tableFooterView = [UIView new];
    
}


#pragma mark - 打开订单常见问题界面

-(void)openHelpViewController
{
    OrderHelpViewController *viewController = [[OrderHelpViewController alloc] init];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - 公司订单统计查询

-(void)doOrderCountQuery
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_COUNT_API];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary *responseDict) {
        StrongSelf
        
        NSInteger code_status = [responseDict[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            NSDictionary *data = responseDict[@"data"];
            strongSelf.orderCountModel = [[OrderCountModel alloc] initWithDictionary:data error:nil];
            [strongSelf.tableView reloadData];
            
        }
    } andFailure:^(NSString *errorDesc) {
        //        StrongSelf
        //        [strongSelf.tableView.mj_footer endRefreshing];
        //        [strongSelf resetAction];
    }];
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    
//}

-(NSArray*)orderStatusList
{
    if (IsArrEmpty(_orderStatusList)) {
        // 加载plist中的字典数据
        NSString *path = [[NSBundle mainBundle] pathForResource:@"OrderStatusCell.plist" ofType:nil];
        _orderStatusList = [NSArray arrayWithContentsOfFile:path];
    }
    return _orderStatusList;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView delegate method

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.orderStatusList.count;
    }
    else if (section == 1){
        return 2;
    }
    return 0;
}

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    return nil;
//}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderStatusTableViewCell *cell = [OrderStatusTableViewCell cellWithTableView:tableView];
    
    
    NSString *badgeNumStr;
    if (indexPath.section == 0) {
        NSDictionary *dictionary = self.orderStatusList[indexPath.row];
        cell.titleLabel.text = dictionary[@"title"];
        cell.iconImageView.image = [UIImage imageNamed:dictionary[@"icon"]];
        
        if (indexPath.row == OrderStatusRowUntreated) {
            badgeNumStr = [self.orderCountModel.untreated stringValue];
        }
        if (indexPath.row == OrderStatusRowStroke) {
            badgeNumStr = [self.orderCountModel.stroke stringValue];
        }
        if (indexPath.row == OrderStatusRowWaitBargin) {
            badgeNumStr = [self.orderCountModel.notBargain stringValue];
        }
        if (indexPath.row == OrderStatusRowUnpaid) {
            badgeNumStr = [self.orderCountModel.unpaid stringValue];
        }
        if (indexPath.row == OrderStatusRowCompleted) {
            badgeNumStr = [self.orderCountModel.completed stringValue];
            cell.badgeButton.hidden = YES;
//            cell.badgeLabel.hidden = YES;
            return cell;
        }
    }
    if (indexPath.section == 1){
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"安排车辆";
            cell.iconImageView.image = [UIImage imageNamed:@"icon_order_vechile_arrange"];
            badgeNumStr = [self.orderCountModel.arrangeCar stringValue];
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"填写路单";
            cell.iconImageView.image = [UIImage imageNamed:@"icon_order_fill"];
            badgeNumStr = [self.orderCountModel.fillWaybill stringValue];
        }
    }
    
//    badgeNumStr = @"10000";
    if (IsStrEmpty(badgeNumStr)|| [badgeNumStr isEqualToString:@"0"]) {
        cell.badgeButton.hidden = YES;
        cell.badgeButton.alpha = 0;
//        cell.badgeLabel.text = @"0";
//        cell.badgeLabel.hidden = YES;
    }
    else {
//        if ([badgeNumStr integerValue] > 99) {
//            badgeNumStr = @"99+";
//        }
        
        [cell.badgeButton setTitle:badgeNumStr forState:UIControlStateNormal];
        cell.badgeButton.hidden = NO;
//        cell.badgeLabel.text = badgeNumStr;
//        cell.badgeLabel.hidden = NO;
    }
    return cell;
}

-(NSArray*)urlList
{
    if (_urlList == nil) {
        _urlList = @[ORDER_LIST_UNTREATED_API,
                     ORDER_LIST_STROKE_API,
                     ORDER_LIST_UNBARGAIN_API,
                     ORDER_LIST_UNPAID_API,
                     ORDER_LIST_COMPLETED_API,
                     ORDER_LIST_API,
                     ORDER_LIST_API];
    }
    return _urlList;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self resetAction];
    if ([self needLogin]) {
        self.nextAction = @selector(tableView:didSelectRowAtIndexPath:);
        self.object1 = tableView;
        self.object2 = indexPath;
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.indexPath = indexPath;
    if (indexPath.section == 0) {
        OrderListViewController *viewController = [[OrderListViewController alloc] init];
        NSDictionary *dict = self.orderStatusList[indexPath.row];
        viewController.orderStatus = [dict[@"status"] integerValue];
        viewController.title = dict[@"title"];
        viewController.requestUrlStr = self.urlList[indexPath.row];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if (indexPath.section == 1) {
        OrderListViewController *vc = [[OrderListViewController alloc] init];
        vc.orderStatus = indexPath.row == 0 ? 3 : 4;
        if (vc.orderStatus == 3) {
            vc.title = @"安排车辆";
        }
        else if (vc.orderStatus == 4) {
            vc.title = @"填写路单";
        }
        vc.requestUrlStr = self.urlList[indexPath.section * 5 + indexPath.row];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}


@end
