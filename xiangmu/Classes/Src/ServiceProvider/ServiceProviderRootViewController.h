//
//  ServiceProviderRootViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceProviderRootViewController : BaseViewController

@property (nonatomic, assign) BOOL showAdv; // 是否显示广告界面

@end
