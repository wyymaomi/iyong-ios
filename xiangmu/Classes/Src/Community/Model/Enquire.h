//
//  Communtiy.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol EnquireModel <NSObject>
@end

@protocol BidModel <NSObject>
@end


@interface BidModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *carCertification;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *companyName;
@property (nonatomic, strong) NSString<Optional> *enquiryId;
@property (nonatomic, strong) NSNumber<Optional> *enterpriseCertification;
@property (nonatomic, strong) NSString<Optional> *model;
@property (nonatomic, strong) NSNumber<Optional> *partner;
@property (nonatomic, strong) NSNumber<Optional> *price;
@property (nonatomic, strong) NSNumber<Optional> *quantity;
@property (nonatomic, strong) NSNumber<Optional> *realNameCertification;
@property (nonatomic, strong) NSString<Optional> *remark;
@property (nonatomic, strong) NSNumber<Optional> *companyScor;//星级
@property (nonatomic, strong) NSString<Optional> *mobile; // 手机号
@property (nonatomic, strong) NSString<Optional> *nickname;
//@property (nonatomic, strong) NSString<Optional> *quantity;
//@property (nonatomic, strong) NSString<Optional> *model;

@end

@interface EnquireModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *companyLogoUrl;
@property (nonatomic, strong) NSString<Optional> *nickname;// 昵称
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *startTime;//用车时间
@property (nonatomic, strong) NSString<Optional> *endTime;// 结束时间
@property (nonatomic, strong) NSString<Optional> *startPlace;// 出发地点
@property (nonatomic, strong) NSString<Optional> *endPlace; // 结束地点
@property (nonatomic, strong) NSString<Optional> *model; // 车型
@property (nonatomic, strong) NSString<Optional> *quantity; // 数量
//@property (nonatomic, strong) NSString *location;//出发地点
@property (nonatomic, strong) NSString<Optional> *itinerary;//大致行程
@property (nonatomic, strong) NSNumber<Optional> *releaseTime; // 发布时间段
@property (nonatomic, strong) NSNumber<Optional> *end;// 是否询价结束
@property (nonatomic, strong) NSString *customerName;//用车联系人
@property (nonatomic, strong) NSString *customerMobile;//用车人电话
//@property (nonatomic, strong) NSString *remark;//备注
//@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSNumber<Optional> *advance;// 是否需要第三方垫付
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *areaCode;

@property (nonatomic, strong) NSMutableArray<BidModel, Optional> *bids;

@property (nonatomic, strong) NSNumber<Optional> *isOpen;


- (BOOL)checkIsEnd;

@end

@interface EnquireResponse : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<EnquireModel,Optional> *data;


@end
