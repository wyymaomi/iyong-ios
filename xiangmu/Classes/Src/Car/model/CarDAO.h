//
//  CarDAO.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DAO.h"

@interface CarDAO : DAO

- (NSMutableArray*)getAllCar:(NSString*)companyId;// 获取对应用户名下的车辆列表
- (NSMutableArray*)getCertifiedCarList:(NSString*)companyId; // 获取已认证车辆列表
- (BOOL)updateCarList:(NSArray*)carList;// 更新车辆信息
- (BOOL)deleteCarById:(NSString*)carId;

@end
