//
//  AdPayViewController+Logic.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdPayViewController.h"

@interface AdPayViewController (Logic)

- (void)doAdPurchase;

-(void)getFinanceInfo;

@end
