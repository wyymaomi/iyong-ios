//
//  HomepageTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomepageCollectionView.h"

@interface HomepageTableViewCell : UITableViewCell

//@property (weak, nonatomic) IBOutlet UIImageView *plusIconImageView;
@property (weak, nonatomic) IBOutlet UIButton *disclosureButton;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) HomepageCollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *dataList;

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

-(void)initDataList:(NSInteger)row dataList:(NSMutableArray *)dataList;

//- (void)initList;


@end
