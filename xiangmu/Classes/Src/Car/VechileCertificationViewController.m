//
//  VechileCertificationViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "VechileCertificationViewController.h"
#import "VechileCertficationHttpMessage.h"
#import "CommonEditTableViewCell.h"
#import "ActionSheetDatePicker.h"
#import "ActionSheetStringPicker.h"
#import "InputHelperViewController.h"
#import "UploadImageManage.h"
#import "GTMBase64.h"
#import "DateUtil.h"
#import "VechileCertificationModel.h"
#import "CameraPhotoService.h"

//static CGFloat lastFooterViewHeight = (ViewWidth-64)*3/4*2+22*4+50;

@interface VechileCertificationViewController ()<UIActionSheetDelegate>/*<UIActionSheetDelegate, UploadImageManageDelegate>*/

//@property (nonatomic, strong) VechileCertificationModel *vechileCertificationModel;
@property (nonatomic, strong) VechileCertficationHttpMessage *httpMessage;
//@property (nonatomic, strong) UploadImageManage *uploadImageManager;
@property (nonatomic, strong) CameraPhotoService *cameraPhotoService;

@property (nonatomic, assign) CGFloat firstFooterViewHeight;// 驾照、行驶证照片高度
@property (nonatomic, assign) CGFloat lastFooterViewHeight;// 身份证照片高度


@end

@implementation VechileCertificationViewController


- (CameraPhotoService *)cameraPhotoService
{
    if (!_cameraPhotoService) {
        WeakSelf
        _cameraPhotoService = [[CameraPhotoService alloc] initWithViewController:self.navigationController type:CameraPhotoLarge completeBlock:^(NSInteger tag){
            [weakSelf.groupTableView reloadData];
        } onGetImageData:^(NSData *imageData, NSInteger tag) {
            NSString *fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imageData] encoding:NSUTF8StringEncoding];
            switch (tag) {
                case ImageViewTagDriverLicense:
                    weakSelf.httpMessage.driverLicenseContent = fileContent;
                    break;
                case ImageViewTagVechileLicense:
                    weakSelf.httpMessage.drivingLicenseContent = fileContent;
                    break;
                case ImageViewTagIdcard:
                    weakSelf.httpMessage.idFrontContent = fileContent;
                    break;
                case ImageViewTagIdcardBack:
                    weakSelf.httpMessage.idBackContent = fileContent;
                    break;
            }
            
//            [self.groupTableView reloadData];

//            weakSelf.vechileHttpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imageData] encoding:NSUTF8StringEncoding];
        }];
    }
    return _cameraPhotoService;
}

- (VechileCertficationHttpMessage*)httpMessage
{
    if (!_httpMessage) {
        _httpMessage = [[VechileCertficationHttpMessage alloc] init];
        _httpMessage.idType = @0;
    }
    return _httpMessage;
}



#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"车辆认证";
    [self.view addSubview:self.groupTableView];
//    self.bEditMode = YES;
    self.httpMessage.carId = self.carId;
    
    [self doGetVechileCertificationInfo];
    
    _lastFooterViewHeight = (ViewWidth-64)*3/5*2+22*4+50;
    
    _firstFooterViewHeight = (ViewWidth-64)*3/5 + 22*2;
    
}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    
//
//    
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 业务逻辑

#pragma mark - 获取认证信息

- (void)doGetVechileCertificationInfo
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_CAR_CERTIFICATION_API];
    NSString *params = [NSString stringWithFormat:@"carId=%@", self.carId];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary * responseData) {
        
        StrongSelf
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            VechileCertificationModel *vechileCertificationModel = [[VechileCertificationModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            
//            strongSelf.httpMessage.driverLicenseNumber = vechileCertificationModel.driverLicenseNumber;
            strongSelf.httpMessage.carId = strongSelf.carId;
//            strongSelf.httpMessage.driverLicenseExpiryDate = [DateUtil formatDateTime:vechileCertificationModel.driverLicenseExpiryDate dateFomatter:@"yyyy-MM-dd"];
//            strongSelf.httpMessage.driverLicenseReceiveDate = [DateUtil formatDateTime:vechileCertificationModel.driverLicenseReceiveDate dateFomatter:@"yyyy-MM-dd"];
//            strongSelf.httpMessage.drivingLicenseExpiry = [DateUtil formatDateTime:vechileCertificationModel.drivingLicenseExpiry dateFomatter:@"yyyy-MM-dd"];
//            strongSelf.httpMessage.drivingLicenseReceiveDate = [DateUtil formatDateTime:vechileCertificationModel.drivingLicenseReceiveDate dateFomatter:@"yyyy-MM-dd"];
            
//            strongSelf.httpMessage.idType = vechileCertificationModel.idType;
            if (!IsNilOrNull(vechileCertificationModel.idType)) {
                strongSelf.httpMessage.idType = vechileCertificationModel.idType;
            }
            
//            strongSelf.httpMessage.idNumber = vechileCertificationModel.idNumber;
//            strongSelf.httpMessage.idExpiryDate = [DateUtil formatDateTime:vechileCertificationModel.idExpiryDate dateFomatter:@"yyyy-MM-dd"];
            [strongSelf downloadIdBackImg:vechileCertificationModel.idBackUrl];
            [strongSelf downloadIdFrontImg:vechileCertificationModel.idFrontUrl];
            [strongSelf downloadDriverLicenseImg:vechileCertificationModel.driverLicenseUrl];
            [strongSelf downloadVechileLicenseImg:vechileCertificationModel.drivingLicenseUrl];
            
            [strongSelf.groupTableView reloadData];
    
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];

}

- (void)downloadDriverLicenseImg:(NSString*)imgUrl
{
    if (IsStrEmpty(imgUrl)) {
        return;
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            
            strongSelf.httpMessage.driverLicenseContent = [GTMBase64 stringByEncodingData:imageData];
            [strongSelf.groupTableView reloadData];
            
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
    
}

- (void)downloadVechileLicenseImg:(NSString*)imgUrl
{
    if (IsStrEmpty(imgUrl)) {
        return;
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            
            strongSelf.httpMessage.drivingLicenseContent = [GTMBase64 stringByEncodingData:imageData];
            [strongSelf.groupTableView reloadData];
            
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

- (void)downloadIdFrontImg:(NSString*)imgUrl
{
    if (IsStrEmpty(imgUrl)) {
        return;
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            
            strongSelf.httpMessage.idFrontContent = [GTMBase64 stringByEncodingData:imageData];
            [strongSelf.groupTableView reloadData];
            
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

- (void)downloadIdBackImg:(NSString*)imgUrl
{
    if (IsStrEmpty(imgUrl)) {
        return;
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            
            strongSelf.httpMessage.idBackContent = [GTMBase64 stringByEncodingData:imageData];
            [strongSelf.groupTableView reloadData];
            
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

#pragma mark - 上传认证信息

- (void)doVechileCertificationed
{
    
    NSString *errorMsg = [self.httpMessage getErrorMsg];
    if (errorMsg) {
        [self showAlertView:errorMsg];
        return;
    }

    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_CERTIFICATION_API];
    NSString *params = self.httpMessage.description;
    
    [self hideHUDIndicatorViewAtCenter];
    [self showHUDIndicatorViewAtCenter:@"正在上传车辆认证信息，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"认证申请已提交，请等待认证审核" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];

}




#pragma mark - 选择日期

-(void)selectDate:(NSInteger)section
{
//    WeakSelf
//    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
//        StrongSelf
//        if ([selectedDate isKindOfClass:[NSDate class]]) {
//            NSDate *date = selectedDate;
//            NSString *dateString = [date stringWithDateFormat:@"yyyy-MM-dd"];
//            
//            switch (section) {
//                case SectionTagDriverLicenseGetDate:
//                    strongSelf.httpMessage.driverLicenseReceiveDate = dateString;
//                    break;
//                case SectionTagDriverLicenseValidateDate:
//                    strongSelf.httpMessage.driverLicenseExpiryDate = dateString;
//                    break;
//                case SectionTagVechileLicenseGetDate:
//                    strongSelf.httpMessage.drivingLicenseReceiveDate = dateString;
//                    break;
//                case SectionTagVechileLicenseValidateDate:
//                    strongSelf.httpMessage.drivingLicenseExpiry = dateString;
//                    break;
//                case SectionTagIDValidateDate:
//                    strongSelf.httpMessage.idExpiryDate = dateString;
//                    break;
//            }
//            
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
//            [strongSelf.groupTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
//            
//        }
//    } cancelBlock:^(ActionSheetDatePicker *picker) {
//        
//    } origin:self.view];
}

-(void)showIDTypePickerView:(id)sender//:(NSArray*)array
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"身份证件类型" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"身份证", @"护照",@"居住证", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{

    switch (buttonIndex) {
        case 0:
            self.httpMessage.idType = @0;
            break;
        case 1:
            self.httpMessage.idType = @1;
            break;
        case 2:
            self.httpMessage.idType = @2;
            break;
//        case 3:
//            self.httpMessage.idType = @3;
            break;
        default:
            break;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:SectionTagIDType];
    
    [self.groupTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (void)onPickerImageView:(id)sender
{
    //用tag传值判断
    UITapGestureRecognizer *tap = (UITapGestureRecognizer *)sender;
    UIView *view = (UIView *)tap.view;
    [self.cameraPhotoService show:view.tag];
//    [self.uploadImageManager showActionSheet:view.tag];
}



#pragma mark - uploadImageManage delegate method

//- (void)didFinishPicker:(UIImage*)image actionSheet:(UIActionSheet *)actionSheet
//{
//    NSData *data = UIImageJPEGRepresentation(image, 1);
//    
//    NSString *fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:data] encoding:NSUTF8StringEncoding];
//    
//    DLog(@"data.size = %lu", (unsigned long)data.length);
//    
//    switch (actionSheet.tag) {
//        case ImageViewTagDriverLicense:
//            self.httpMessage.driverLicenseContent = fileContent;
//            break;
//        case ImageViewTagVechileLicense:
//            self.httpMessage.drivingLicenseContent = fileContent;
//            break;
//        case ImageViewTagIdcard:
//            self.httpMessage.idFrontContent = fileContent;
//            break;
//        case ImageViewTagIdcardBack:
//            self.httpMessage.idBackContent = fileContent;
//            break;
//    }
//    
//    [self.groupTableView reloadData];
//}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 8;
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if (section == 3 || section == 4) {
//        return 170;
//    }
    
    return 10;
}



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0 || section == 1) {
//        return (ViewWidth-64)*3/4+22*2;
        return _firstFooterViewHeight;
    }
    if (section == 2) {
        return _lastFooterViewHeight;
//        return (ViewWidth-64)*3/4*2+10*4+50;
    }
    return 0.00001f;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == SectionTagDriverLicenseNumber) {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, _firstFooterViewHeight)];

        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, 22, (ViewWidth-64), (ViewWidth-64)*3/5)];
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, 22, (ViewWidth - 64), (170 - 22 * 2))];
        imageView.image = [UIImage imageNamed:@"img_DrivingLicense"];
        imageView.userInteractionEnabled = YES;
        imageView.tag = ImageViewTagDriverLicense;
        if (!IsStrEmpty(self.httpMessage.driverLicenseContent)) {
            NSData *decodeImageData = [GTMBase64 decodeString:self.httpMessage.driverLicenseContent];
            imageView.image = [UIImage imageWithData:decodeImageData];
        }
        
        if (self.bEditMode) {
            [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPickerImageView:)]];
        }
        
        [headerView addSubview:imageView];
        
        return headerView;
        
    }
    if (section == SectionTagVechileLicenseGetDate) {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, _firstFooterViewHeight)];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, 22, (ViewWidth-64), (ViewWidth-64)*3/5)];
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, 22, (ViewWidth - 64), (170 - 22 * 2))];
        imageView.image = [UIImage imageNamed:@"img_DrivingPermits"];
        imageView.userInteractionEnabled = YES;
        imageView.tag = ImageViewTagVechileLicense;
        
        if (!IsStrEmpty(self.httpMessage.drivingLicenseContent)) {
            NSData *decodeImageData = [GTMBase64 decodeString:self.httpMessage.drivingLicenseContent];
            imageView.image = [UIImage imageWithData:decodeImageData];
        }
        
        [headerView addSubview:imageView];
        
        if (self.bEditMode) {
            [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPickerImageView:)]];
        }
        
        return headerView;
        
    }
    if (section == SectionTagIDType) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, _lastFooterViewHeight)];
        
        CGFloat tempHeight = 20;
        UIImageView *idcardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, tempHeight, ViewWidth-32*2, (ViewWidth-64)*3/5)];
        idcardImageView.userInteractionEnabled = YES;
        idcardImageView.image = [UIImage imageNamed:@"img_idcard_position"];
        idcardImageView.tag = ImageViewTagIdcard;
        [view addSubview:idcardImageView];
        if (self.bEditMode) {
            [idcardImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPickerImageView:)]];
        }
        
        if (!IsStrEmpty(self.httpMessage.idFrontContent)) {
            NSData *decodeImageData = [GTMBase64 decodeString:self.httpMessage.idFrontContent];
            idcardImageView.image = [UIImage imageWithData:decodeImageData];
        }
        
        UIImageView *idcardBackImageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, idcardImageView.bottom+20, ViewWidth-32*2, (ViewWidth-64)*3/5)];
        idcardBackImageView.userInteractionEnabled = YES;
        idcardBackImageView.image = [UIImage imageNamed:@"img_idcard_back"];
        idcardBackImageView.tag = ImageViewTagIdcardBack;
        [view addSubview:idcardBackImageView];
        if (self.bEditMode) {
            [idcardBackImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPickerImageView:)]];
        }
        if (!IsStrEmpty(self.httpMessage.idBackContent)) {
            NSData *decodeImageData = [GTMBase64 decodeString:self.httpMessage.idBackContent];
            idcardBackImageView.image = [UIImage imageWithData:decodeImageData];
        }
        
        if (_bEditMode) {
            UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [okButton setTitle:@"确认绑定" forState:UIControlStateNormal];
            okButton.frame = CGRectMake(10, idcardBackImageView.bottom+20, ViewWidth-20, 50);
            [okButton blueStyle];
            [okButton addTarget:self action:@selector(doVechileCertificationed) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:okButton];
        }

        
        return view;
        
    }

    return [[UIView alloc] init];
    
}



- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    if (section == 3) {
//        
//        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 170)];
//        
//        
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, 22, (ViewWidth - 64), (170 - 22 * 2))];
//        imageView.image = [UIImage imageNamed:@"img_DrivingLicense"];
//        imageView.userInteractionEnabled = YES;
//        imageView.tag = ImageViewTagDriverLicense;
//        if (!IsStrEmpty(self.httpMessage.driverLicenseContent)) {
//            NSData *decodeImageData = [GTMBase64 decodeString:self.httpMessage.driverLicenseContent];
//            imageView.image = [UIImage imageWithData:decodeImageData];
//        }
//        
//        if (self.bEditMode) {
//            [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPickerImageView:)]];
//        }
//        
//        [headerView addSubview:imageView];
//        
//        return headerView;
//        
//    }
//    if (section == 4) {
//        
//        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 170)];
//        
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, 22, (ViewWidth - 64), (170 - 22 * 2))];
//        imageView.image = [UIImage imageNamed:@"img_DrivingPermits"];
//        imageView.userInteractionEnabled = YES;
//        imageView.tag = ImageViewTagVechileLicense;
//        
//        if (!IsStrEmpty(self.httpMessage.drivingLicenseContent)) {
//            NSData *decodeImageData = [GTMBase64 decodeString:self.httpMessage.drivingLicenseContent];
//            imageView.image = [UIImage imageWithData:decodeImageData];
//        }
//        
//        [headerView addSubview:imageView];
//        
//        if (self.bEditMode) {
//            [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPickerImageView:)]];
//        }
//        
//        return headerView;
//        
//    }
    
    return [[UIView alloc] init];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CommonEditTableViewCell *cell = [CommonEditTableViewCell cellWithTableView:tableView indexPath:indexPath];
    if (indexPath.section == SectionTagDriverLicenseNumber) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.titleLabel.text = @"驾驶证件";
        cell.detailTextField.hidden = YES;
//        cell.detailTextField.tag = DriverLicenseNoTextField;
//        cell.detailTextField.placeholder = @"请输入驾驶证件号";
//        cell.detailTextField.text = self.httpMessage.driverLicenseNumber;
    }
//    if (indexPath.section == SectionTagDriverLicenseGetDate) {
//        cell.titleLabel.text = @"领取驾照日期";
//        cell.detailTextField.tag = DriverLicenseGetDateTextField;
//        cell.detailTextField.placeholder = @"选择日期";
//        cell.detailTextField.text = self.httpMessage.driverLicenseReceiveDate;
//    }
//    if (indexPath.section == SectionTagDriverLicenseValidateDate) {
//        cell.titleLabel.text = @"证件有效日期";
//        cell.detailTextField.tag = DriverLicenseValidateDateTextField;
//        cell.detailTextField.placeholder = @"选择日期";
//        cell.detailTextField.text = self.httpMessage.driverLicenseExpiryDate;
//    }
    if (indexPath.section == SectionTagVechileLicenseGetDate) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.titleLabel.text = @"行驶证";
        cell.detailTextField.hidden = YES;
//        cell.detailTextField.tag = VehicleLicenseGetDateTextField;
//        cell.detailTextField.placeholder = @"选择日期";
//        cell.detailTextField.text = self.httpMessage.drivingLicenseReceiveDate;
    }
//    if (indexPath.section == SectionTagVechileLicenseValidateDate) {
//        cell.titleLabel.text = @"证件有效日期";
//        cell.detailTextField.tag = VechileLicenseValidateDateTextField;
//        cell.detailTextField.placeholder = @"选择日期";
//        cell.detailTextField.text = self.httpMessage.drivingLicenseExpiry;
//    }
    if (indexPath.section == SectionTagIDType) {
        cell.titleLabel.text = @"证件类型";
        cell.detailTextField.tag = IDTypeTextField;
        cell.detailTextField.placeholder = @"请选择证件类型";
        cell.detailTextField.text = [self.httpMessage getIdTypeString];
    }
//    if (indexPath.section == SectionTagIDNumber) {
//        cell.titleLabel.text = @"证件号";
//        cell.detailTextField.tag = IDNumberTextField;
//        cell.detailTextField.placeholder = @"请输入证件号";
//        cell.detailTextField.text = self.httpMessage.idNumber;
//    }
//    if (indexPath.section == SectionTagIDValidateDate) {
//        cell.titleLabel.text = @"证件有效日期";
//        cell.detailTextField.tag = IDValidateDateTextField;
//        cell.detailTextField.placeholder = @"选择日期";
//        cell.detailTextField.text = self.httpMessage.idExpiryDate;
//    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.bEditMode == NO) {
        return;
    }
    
//    if (/*indexPath.section == SectionTagDriverLicenseGetDate ||
//        indexPath.section == SectionTagDriverLicenseValidateDate ||*/
//        indexPath.section == SectionTagVechileLicenseGetDate /*||
//        indexPath.section == SectionTagVechileLicenseValidateDate ||
//        indexPath.section == SectionTagIDValidateDate*/) {
//        
//        [self selectDate:indexPath.section];
//        
//    }
//    if (indexPath.section == SectionTagDriverLicenseNumber /*||
//        indexPath.section == SectionTagIDNumber*/) {
//        
//        CommonEditTableViewCell *cell = [self.groupTableView cellForRowAtIndexPath:indexPath];
//        
//        InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
//        viewController.title = cell.titleLabel.text;
//        viewController.text = cell.detailTextField.text.trim;
//        viewController.tag = cell.detailTextField.tag;
//        
//        WeakSelf
//        viewController.completeHandle = ^(NSInteger tag, NSString *text){
//            StrongSelf
//            if (tag == IDNumberTextField) {
////                strongSelf.httpMessage.idNumber = text;
//            }
//            else if (tag == DriverLicenseNoTextField) {
////                strongSelf.httpMessage.driverLicenseNumber = text;
//            }
//            [strongSelf.groupTableView reloadData];
//        };
//        [self.navigationController pushViewController:viewController animated:YES];
//        
//        
//    }
    if (indexPath.section == SectionTagIDType) {
        CommonEditTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self showIDTypePickerView:cell.detailTextField];
        
    }
    
}




@end
