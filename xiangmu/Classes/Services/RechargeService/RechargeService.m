//
//  RechargeService.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RechargeService.h"
#import "SDKManager.h"
#import "SDKAlipayServiceImpl.h"
#import "WXApi.h"

@implementation RechargeService

+ (instancetype)sharedInstance
{
    static RechargeService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[RechargeService alloc] init];
    });
    return instance;
}

#pragma mark - 支付宝支付

- (void)doAlipayPay:(NSString*)encryptOrderString;
{
    
    //    NSDictionary *dict = @{kTradeNo:@"1234567890",//[self generateTradeNO],
    //                           kRechargeAmount:self.transAmountTextField.text.trim,
    //                           kPayTime:[[NSDate date] stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"]};
    //    id<PayModelAdapterProtocol> alipay = [[AlipayOrder alloc] init];
    
    [[SDKManager getPayService:SDKPlatformAlipay] payOrder:/*[alipay generateOrderString:dict]*/encryptOrderString callBack:^(NSString *signString, NSError *error) {
        DLog(@"signString = %@", signString);
        //        NSInteger resultStatus = [signString integerValue];
        //        if (resultStatus == 6001) {
        //            [MsgToolBox showAlert:@"" content:@"用户取消"];
        ////                        [self showTipMessage:@"用户取消"];
        //            //            [self ]
        //        }
        //        else if (resultStatus == 6002){
        //            [MsgToolBox showAlert:@"" content:@"网络连接错误"];
        ////                        [self showTipMessage:@"网络连接错误"];
        //        }
        //        else if (resultStatus == 4000){
        //            [MsgToolBox showAlert:@"" content:@"订单支付失败"];
        //            //            [self showTipMessage:@"订单支付失败"];
        //        }
        //        else if (resultStatus == 8000){
        //            [MsgToolBox showAlert:@"" content:@"正在处理中"];
        //            //            [self showTipMessage:@"正在处理中"];
        //        }
        //        else if (resultStatus == 9000){
        //            [MsgToolBox showAlert:@"" content:@"订单支付成功"];
        //            //            [self showTipMessage:@"订单支付成功"];
        //        }
        
    }];
    
}


#pragma mark - 微信支付

- (void)bizPay:(NSDictionary*)dict;
{
    //    NSString *res = [WXApiRequestHandler jumpToBizPay];
    //    if( ![@"" isEqual:res] ){
    //        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"支付失败" message:res delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //
    //        [alter show];
    ////        [alter release];
    //    }
    
    NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
    
    //调起微信支付
    PayReq *req             = [[PayReq alloc] init];
    req.partnerId           = dict[@"partnerid"];
    req.prepayId            = [dict objectForKey:@"prepayid"];
    req.nonceStr            = [dict objectForKey:@"noncestr"];
    req.timeStamp           = stamp.intValue;
    req.package             = [dict objectForKey:@"package"];
    req.sign                = [dict objectForKey:@"sign"];
    [WXApi sendReq:req];
    
}

#pragma mark - 环迅支付



@end
