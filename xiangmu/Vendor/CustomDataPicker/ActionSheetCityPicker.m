//
//  ActionSheetCityPicker.m
//  lottery
//
//  Created by wyy on 14-12-30.
//  Copyright (c) 2014年 ___QIHUITEL___. All rights reserved.
//

#import "ActionSheetCityPicker.h"

//{
//    "id":"110000",
//    "areaname":"北京",
//    "parentid":"0",
//    "shortname":"北京",
//    "level":"1",
//    "sort":"1"
//},

@implementation AreaInfoModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"areaname": @"name",
                                                       @"parentid": @"parent_id"
                                                       }];
}

@end

@implementation AreaInfoList

@end

@interface ActionSheetCityPicker ()

@property (nonatomic, strong) AreaInfoList *areaList;

@property (nonatomic, strong) AreaInfoModel *selectedProvince;
@property (nonatomic, strong) AreaInfoModel *selectedCity;
@property (nonatomic, strong) AreaInfoModel *selectedDistrict;
//@property (nonatomic, strong) NSString *selectedProvince;
//@property (nonatomic, strong) NSString *selectedCity;

//@property (nonatomic, strong) NSDictionary *provinceAndCityDictionary;
@property (nonatomic, strong) NSArray *provinces;

@property (nonatomic, assign) NSInteger level;

@end

@implementation ActionSheetCityPicker

-(AreaInfoList*)areaList
{
    if (!_areaList) {
        // 读取本地资源文件
        NSError *error=nil;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"areas_district" ofType:@"json"];
        DLog(@"filePath = %@", filePath);
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:filePath]) {
            NSLog(@"file is exist");
//            @autoreleasepool {
                NSData *data = [NSData dataWithContentsOfFile:filePath];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                _areaList = [[AreaInfoList alloc] initWithDictionary:dict error:&error];
//            }

        }
    }
    return _areaList;
}

-(NSArray*)provinces
{
    if (_provinces == nil) {
//        @autoreleasepool {
            NSMutableArray *provinces = [NSMutableArray new];
            for (AreaInfoModel *areaModel in self.areaList.data) {
                if ([areaModel.level integerValue] == AreaInfoTypeProvince) {
                    [provinces addObject:areaModel];
                }
            }
            _provinces = [provinces sortedArrayUsingComparator:^(AreaInfoModel *m1, AreaInfoModel *m2){
                if ([m1.sort integerValue] > [m2.sort integerValue]) {
                    return NSOrderedDescending;
                }
                return NSOrderedAscending;
            }];
//        }
        
        
    }
    return _provinces;
}



+(instancetype)showPickerWithTitle:(NSString*)title
                         doneBlock:(ActionCityDoneBlock)doneBlock
                       cancelBlock:(ActionCityCancelBlock)cancelBlockOrNil
                            origin:(id)origin
{
    ActionSheetCityPicker *picker = [[ActionSheetCityPicker alloc] initWithTitle:title level:AreainfoLevelCity doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    picker.level = AreainfoLevelCity;
    [picker showActionSheetPicker];
    return picker;
}


-(instancetype)initWithTitle:(NSString*)title
                       level:(NSUInteger)level
                   doneBlock:(ActionCityDoneBlock)doneBlock
                 cancelBlock:(ActionCityCancelBlock)cancelBlockOrNil
                      origin:(id)origin
{
    self = [self initWithTarget:nil successAction:nil cancelAction:nil origin:origin];
    if (self) {
        self.title = title;
        self.level = level;
        self.onActionSheetDone = doneBlock;
        self.onActionSheetCancel = cancelBlockOrNil;
    }
    return self;
}

- (UIView *)configuredPickerView {
    
    [self setSelectedRows];
    
    CGRect pickerFrame = CGRectMake(0, 40, self.viewSize.width, 216);
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    pickerView.showsSelectionIndicator = YES;
    
    [self selectCurrentCity:pickerView];
    
    //need to keep a reference to the picker so we can clear the DataSource / Delegate when dismissing
    self.pickerView = pickerView;
    
    return pickerView;
}


- (void)setSelectedRows
{
    self.selectedProvince = self.provinces[0];
    self.selectedCity = [self getCitiesByProvince:self.selectedProvince][0];
    self.selectedDistrict = [self getDistrictByCityId:self.selectedCity][0];
}

- (void)selectCurrentCity:(UIPickerView *)pickerView
{
    NSUInteger rowProvince = [self.provinces indexOfObject:self.selectedProvince];
    NSUInteger rowCity = [[self getCitiesByProvince:self.selectedProvince] indexOfObject:self.selectedCity];
    NSUInteger rowDistrict = [[self getDistrictByCityId:self.selectedCity] indexOfObject:self.selectedDistrict];
    
    if ((rowProvince != NSNotFound) && (rowCity != NSNotFound)) {
        [pickerView selectRow:rowProvince inComponent:0 animated:YES];
        [pickerView reloadComponent:1];
        [pickerView selectRow:rowCity inComponent:1 animated:YES];
        [pickerView selectRow:rowDistrict inComponent:2 animated:YES];
    }
    else {
        [pickerView selectRow:0 inComponent:0 animated:YES];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
}

- (void)notifyTarget:(id)target didSucceedWithAction:(SEL)successAction origin:(id)origin {
    
    if (self.onActionSheetDone) {
//        self.onActionSheetDone(self, self.selectedProvince, self.selectedCity);
        self.onActionSheetDone(self, self.selectedProvince, self.selectedCity, self.selectedDistrict);
        return;
    }
    else if (target && [target respondsToSelector:successAction]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
//        [target performSelector:successAction withObject:timeZone withObject:origin];
#pragma clang diagnostic pop
        return;
    }
    NSLog(@"Invalid target/action ( %s / %s ) combination used for ActionSheetPicker", object_getClassName(target), sel_getName(successAction));
}

- (void)notifyTarget:(id)target didCancelWithAction:(SEL)cancelAction origin:(id)origin {
    if (self.onActionSheetCancel) {
        _onActionSheetCancel(self);
        return;
    }
    else if (target && cancelAction && [target respondsToSelector:cancelAction]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [target performSelector:cancelAction withObject:origin];
#pragma clang diagnostic pop
    }
}



#pragma mark - UIPickerViewDelegate / DataSource
//
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    id obj = [self.data objectAtIndex:(NSUInteger) row];
//
//    // return the object if it is already a NSString,
//    // otherwise, return the description, just like the toString() method in Java
//    // else, return nil to prevent exception
//
//    if ([obj isKindOfClass:[NSString class]])
//        return obj;
//
//    if ([obj respondsToSelector:@selector(description)])
//        return [obj performSelector:@selector(description)];
//
//    return nil;
//}
//


/////////////////////////////////////////////////////////////////////////
#pragma mark - UIPickerViewDataSource Implementation
/////////////////////////////////////////////////////////////////////////

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.level;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
//    DLog(@"numberOfRowInComponent");
    // Returns
    switch (component) {
        case 0: return [self.provinces count];
        case 1: return [[self getCitiesByProvince:self.selectedProvince] count];
        case 2: return [[self getDistrictByCityId:self.selectedCity] count];
        default:break;
    }
    return 0;
}

/////////////////////////////////////////////////////////////////////////
#pragma mark UIPickerViewDelegate Implementation
/////////////////////////////////////////////////////////////////////////

// returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
//    DLog(@"widthForComponent");
    switch (component) {
            
        case 0: return firstColumnWidth;
        case 1: return secondColumnWidth;
        case 2: return secondColumnWidth;
        default:break;
//            return ViewWidth/self.level;
    }
    
    return 0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
//    DLog(@"viewForRow");
    UILabel *pickerLabel = (UILabel *)view;
    
    if (pickerLabel == nil) {
        CGRect frame = CGRectZero;
        
        
        switch (component) {
            case 0: frame = CGRectMake(0.0, 0.0, firstColumnWidth, 32);
                break;
            case 1:
                frame = CGRectMake(0.0, 0.0, secondColumnWidth, 32);
                break;
            case 2:
                frame = CGRectMake(0, 0, secondColumnWidth, 32);
                break;
            default:
                assert(NO);
                break;
        }
        
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setMinimumScaleFactor:0.5];
        [pickerLabel setAdjustsFontSizeToFitWidth:YES];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont systemFontOfSize:20]];
    }
    
    NSString *text;
    switch (component) {
        case 0:
        {
            AreaInfoModel *province = self.provinces[row];
//            self.selectedProvince = province;
            text = province.name;
            break;
        }

        case 1:
        {
            AreaInfoModel *city = [self getCitiesByProvince:self.selectedProvince][row];
            text = city.name;
            break;
        }
        case 2:
        {
            AreaInfoModel *district = [self getDistrictByCityId:self.selectedCity][row];
            text = district.name;
        }
        default:break;
    }
    
    [pickerLabel setText:text];
    
    return pickerLabel;
    
}

/////////////////////////////////////////////////////////////////////////

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    DLog(@"didSelectRow");
    switch (component) {
        case 0:
        {
            self.selectedProvince = (self.provinces)[(NSUInteger) row];
            [pickerView reloadComponent:1];
            [(UIPickerView*)self.pickerView selectRow:0 inComponent:1 animated:YES];
            self.selectedCity = [self getCitiesByProvince:self.selectedProvince][(NSUInteger) [pickerView selectedRowInComponent:1]];
            
            
//            self.selectedCity = [self getCitiesByProvince:self.selectedProvince][0];
            if (self.level == AreainfoLevelDistrict) {
                [pickerView reloadComponent:(self.level-1)];
                [(UIPickerView*)self.pickerView selectRow:0 inComponent:2 animated:YES];
                self.selectedDistrict = [self getDistrictByCityId:self.selectedCity][(NSUInteger)[pickerView selectedRowInComponent:2]];
                
            }   
            return;
        }
            
        case 1:
            self.selectedCity = [self getCitiesByProvince:self.selectedProvince][(NSUInteger) row];
            if (self.level == AreainfoLevelDistrict) {
                
                [(UIPickerView*)self.pickerView selectRow:0 inComponent:2 animated:YES];
                [pickerView reloadComponent:(2)];
                self.selectedDistrict = [self getDistrictByCityId:self.selectedCity][(NSUInteger)[pickerView selectedRowInComponent:2]];
                
            }
            return;
        case 2:
            self.selectedDistrict = [self getDistrictByCityId:self.selectedCity][(NSUInteger)row];
        default:break;
    }
}

#pragma mark - 获得城市下的区号
-(NSMutableArray*)getDistrictByCityId:(AreaInfoModel*)city
{
    NSMutableArray *districtList = [NSMutableArray new];
    [self.areaList.data enumerateObjectsUsingBlock:^(AreaInfoModel *district, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([district.level integerValue] == AreaInfoTypeDistrict && [district.parent_id isEqualToString:city.id]) {
            [districtList addObject:district];
        }
        
    }];
    
    return districtList;
}

#pragma mark - 获得省份下的城市列表
-(NSMutableArray *)getCitiesByProvince:(AreaInfoModel*)province
{
    NSMutableArray *citiesInProvince = [NSMutableArray new];
    for (AreaInfoModel *cityData in self.areaList.data) {
        if ([cityData.level integerValue] == AreaInfoTypeCity && [cityData.parent_id isEqualToString:province.id]) {
            [citiesInProvince addObject:cityData];
        }
    }
    return citiesInProvince;
};

#pragma mark - 点击按钮
- (void)customButtonPressed:(id)sender {
    UIBarButtonItem *button = (UIBarButtonItem*)sender;
    NSInteger index = button.tag;
    NSAssert((index >= 0 && index < self.customButtons.count), @"Bad custom button tag: %ld, custom button count: %lu", (long)index, (unsigned long)self.customButtons.count);
    
    NSDictionary *buttonDetails = (self.customButtons)[(NSUInteger) index];
    NSAssert(buttonDetails != NULL, @"Custom button dictionary is invalid");
    
    ActionType actionType = (ActionType) [buttonDetails[kActionType] intValue];
    switch (actionType) {
        case ActionTypeValue: {
            [self setSelectedRows];
//            [self selectCurrentCity:(UIPickerView*)self.pickerView];
            break;
        }
            
        case ActionTypeBlock:
        case ActionTypeSelector:
            [super customButtonPressed:sender];
            break;
        default:
            NSAssert(false, @"Unknown action type");
            break;
    }
}


@end
