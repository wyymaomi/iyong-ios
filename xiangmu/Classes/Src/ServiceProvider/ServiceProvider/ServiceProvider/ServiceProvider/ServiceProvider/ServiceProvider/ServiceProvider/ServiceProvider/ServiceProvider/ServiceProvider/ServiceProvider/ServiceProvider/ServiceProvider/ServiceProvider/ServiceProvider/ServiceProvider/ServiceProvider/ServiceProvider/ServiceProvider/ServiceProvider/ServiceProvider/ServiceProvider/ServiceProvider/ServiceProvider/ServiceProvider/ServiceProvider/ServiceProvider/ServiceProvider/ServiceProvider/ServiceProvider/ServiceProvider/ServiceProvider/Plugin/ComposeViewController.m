//
//  ComposeViewController.m
//  ArtSky
//
//  Created by Adam on 16/4/12.
//  Copyright © 2016年 fule. All rights reserved.
//

#import "ComposeViewController.h"
#import "ZTTextView.h"
#import "DFPlainGridImageView.h"

#import "UploadParamModel.h"
#import "ZTHttpTool.h"

#import "TZImagePickerController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "MJPhotoBrowser.h"
#import "MJPhoto.h"

#import "MMPopupItem.h"
#import "MMSheetView.h"

#define kPhotosMax          9
#define kTextViewH          120
#define ImageGridWidth      [UIScreen mainScreen].bounds.size.width * 0.7

@interface ComposeViewController () <DFPlainGridImageViewDelegate, TZImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{

    NSUInteger uploadCount;
}

@property (weak, nonatomic) ZTTextView *textView;
@property (weak, nonatomic) DFPlainGridImageView *gridView;
@property (nonatomic, strong) NSMutableArray *images;

@property (nonatomic, strong) UIView *mask;
@property (nonatomic, strong) UIImagePickerController *pickerController;
@property (strong, nonatomic) UIPanGestureRecognizer *panGestureRecognizer;
@property (strong, nonatomic) UITapGestureRecognizer *tapGestureRecognizer;

@property (copy, nonatomic) NSString *strCommentId;

@end

@implementation ComposeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupTextView];
    
    uploadCount = 0;
    [self initWithImages:nil];
    [self setUpImageGridView];
    
    _mask = [[UIView alloc] initWithFrame:self.view.bounds];
    _mask.backgroundColor = [UIColor clearColor];
    _mask.hidden = YES;
    [self.view addSubview:_mask];
    
    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPanAndTap:)];
    [_mask addGestureRecognizer:_panGestureRecognizer];
    _panGestureRecognizer.maximumNumberOfTouches = 1;
    
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPanAndTap:)];
    [_mask addGestureRecognizer:_tapGestureRecognizer];
    
    [self refreshGridImageView];
    
    
//    if (self.mComposetSentEventBlock) {
//        self.mComposetSentEventBlock(YES);
//    }
}

- (void)initWithImages:(NSArray *)aImages
{
    NSMutableArray *images = [NSMutableArray array];
    if (aImages != nil) {
        [images addObjectsFromArray:aImages];
        [images addObject:[UIImage imageNamed:@"AlbumAddBtn"]];
    } else {
        [images addObject:[UIImage imageNamed:@"AlbumAddBtn"]];
    }
    
    _images = images;
}

- (void)setupTextView
{
    ZTTextView *textView = [[ZTTextView alloc] init];
    textView.frame = CGRectMake(0, 0, SCREEN_WIDTH, kTextViewH);
    textView.placeholder = @"这一刻的想法...";
    [self.view addSubview:textView];
    _textView = textView;
}

- (void)setUpImageGridView
{
    DFPlainGridImageView *gridView = [[DFPlainGridImageView alloc] initWithFrame:CGRectZero];
    gridView.delegate = self;
    [self.view addSubview:gridView];
    _gridView = gridView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) refreshGridImageView
{
    CGFloat x, y, width, heigh;
    x = 10;
    y = CGRectGetMaxY(_textView.frame) + 10;
    width  = ImageGridWidth;
    heigh = [DFPlainGridImageView getHeight:_images maxWidth:width];
    
    _gridView.frame = CGRectMake(x, y, width, heigh);
    [_gridView updateWithImages:_images];
}

- (UIBarButtonItem *)leftBarButtonItem
{
    return [self text:@"取消" selector:@selector(cancel) target:self isRight:NO];
}

- (UIBarButtonItem *)rightBarButtonItem
{
    return [self text:@"发送" selector:@selector(send) target:self isRight:YES];
}

-(void) cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)checkComposeAvailable
{
    if ( _textView.text.length > 0 ) {
        
        NSString *cleanString = [_textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if ( cleanString.length > 0 ) {
            
            return YES;
        }
    }

    if (_images.count > 1) {
        
        return YES;
    }
    
    return NO;
}

-(void) send
{
    
    if ([self checkComposeAvailable]) {

        [_images removeLastObject];
        
        NSString *cleanString = [_textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [self onSendTextImage:cleanString];
        
        [self cancel];
    } else {
        
        [self showHUDWithText:@"请输入文字内容，或者选择图片。"];
    }
    
}

-(void) onPanAndTap:(UIGestureRecognizer *) gesture
{
    _mask.hidden = YES;
    [_textView resignFirstResponder];
}

//选择照片后得到数据
-(void)onSendTextImage:(NSString *)text
{
    
    [self uploadCommentMehtod:text];
}

- (void)uploadCommentMehtod:(NSString *)content
{
    
    NSMutableDictionary *dataDict = [NSMutableDictionary dictionary];
    [dataDict setObject:[AppManager instance].userId forKey:@"userId"];
    [dataDict setObject:[AppManager instance].composeLogicId forKey:@"resourceId"];
    [dataDict setObject:@([AppManager instance].composeLogicType) forKey:@"type"];
    [dataDict setObject:content forKey:@"content"];
    
    NSMutableDictionary *paramDict = [CommonUtils getParamDict:@"addComment"
                                                      dataDict:dataDict];
    
    [ZTHttpTool post:HOST_URL
              params:paramDict
             success:^(id json) {
                 
                 NSDictionary *backDic = json;
                 
                 
                 if (backDic != nil) {
                     
                     NSString *errCodeStr = (NSString *)[backDic valueForKey:@"code"];
                     
                     if ( [errCodeStr integerValue] == 0 ) {
                         
                         NSDictionary *resultDic = [backDic valueForKey:@"result"];
                         
                         self.strCommentId = resultDic[@"commentId"];
                         
                         if (_images.count > 0) {
                             
                             [self doUploadImges];
                         } else {
                             [self uploadDone];
                         }
                         
                     } else {
                         [self showHUDWithText:[backDic valueForKey:@"msg"]];
                     }
                 } else {
                     [self showHUDWithText:LocaleStringForKey(NSReturnDataIsEmpty, nil)];
                 }
                 
             } failure:^(NSError *error) {
                 
                 DLog(@"请求失败-%@", error);
             }];
}

- (void)uploadDone
{

    [self.composeSendDelegate handleComposeSent];

}

- (void)doUploadImges
{
    for (id img in _images) {
        
        [self uploadImageMehtod:img commentId:self.strCommentId];
    }
}

- (void)uploadImageMehtod:(UIImage *)image commentId:(NSString *)commentId
{
    
    // 创建参数模型
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[AppManager instance].userId forKey:@"userId"];
    [parameters setObject:[CommonUtils currentTimeInterval] forKey:@"time"];
    [parameters setObject:commentId forKey:@"commentId"];
    
    // 创建上传的模型
    UploadParamModel *uploadP = [[UploadParamModel alloc] init];
    uploadP.data = UIImageJPEGRepresentation(image, 0.3);
    uploadP.name = @"pic";
    uploadP.fileName = @"image.jpg";
    uploadP.mimeType = @"image/jpeg";
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/addCommentWithPic", HOST_URL];
    // 注意：以后如果一个方法，要传很多参数，就把参数包装成一个模型
    [ZTHttpTool upload:urlStr parameters:parameters uploadParam:uploadP success:^(id responseObject) {
        
        DLog(@"responseObject = %@", responseObject);
        uploadCount ++;
        
        [self checkUploadAll];
    } failure:^(NSError *error) {
        //        if (failure) {
        //            failure(error);
        //        }
    }];
    
}

- (void)checkUploadAll
{
    NSInteger count = 0;
    if (_images.count > kPhotosMax)  {
        count = kPhotosMax;
    } else {
        count = _images.count;
    }
    
    if (uploadCount >= count) {
        
//        if (self.mComposetSentEventBlock) {
//            self.mComposetSentEventBlock(YES);
//        }
        
        [self uploadDone];
    }
}

#pragma mark - DFPlainGridImageViewDelegate

-(void)onClick:(NSUInteger)index
{
    [_textView resignFirstResponder];
    
    if (_images.count <kPhotosMax && index == _images.count-1) {
        
        [self chooseImage];
    } else {
        
        NSUInteger count;
        
        if (_images.count > kPhotosMax)  {
            count = kPhotosMax;
        } else {
            count = _images.count - 1;
        }
        
        NSMutableArray *photos = [NSMutableArray array];
        
        for (int i=0; i<count; i++) {
            
            MJPhoto *photo = [[MJPhoto alloc] init];
            photo.image = _images[i];
            
            [photos addObject:photo];
        }
        
        MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
        browser.photos = photos;
        browser.currentPhotoIndex = index;
        
        [browser show];
    }
}

- (void)onLongPress:(NSUInteger)index
{
    
    if (_images.count < kPhotosMax && index == _images.count-1) {
        return;
    }
    
    MMPopupItemHandler block = ^(NSInteger i){
        switch (i) {
            case 0:
                [_images removeObjectAtIndex:index];
                [self refreshGridImageView];
                break;
            default:
                break;
        }
    };
    
    NSArray *items = @[MMItemMake(@"删除", MMItemTypeNormal, block)];
    
    MMSheetView *sheetView = [[MMSheetView alloc] initWithTitle:@"" items:items];
    [sheetView show];
    
}

- (void) chooseImage
{
    
    actionSheetType = ActionSheetType_Change_Photo;
    UIActionSheet *alert = [[UIActionSheet alloc] initWithTitle:@"选择图片"
                                                       delegate:self
                                              cancelButtonTitle:LocaleStringForKey(NSCancelTitle, nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:LocaleStringForKey(NSTakePhotoTitle, nil), LocaleStringForKey(NSChoosePhotosTitle, nil), nil];
    [alert showInView:[self view]];
}


#pragma mark - UIActionSheetDelegate method
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (actionSheetType) {
            
        case ActionSheetType_Change_Photo:
        {
            
            if (buttonIndex == 0) {
                
                if ([self isSupportCapture]) {
                    
                    [self takePhoto];
                }
            } else if (buttonIndex == 1) {
                
                [self pickFromAlbum];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void) takePhoto
{
    _pickerController = [[UIImagePickerController alloc] init];
    _pickerController.delegate = self;
    _pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:_pickerController animated:YES completion:nil];
}

- (void) pickFromAlbum
{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:(10-_images.count) delegate:self];
    imagePickerVc.allowPickingVideo = NO;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}


#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *) photos sourceAssets:(NSArray *)assets
{
    NSLog(@"%@", photos);
    
    for (UIImage *image in photos) {
        [_images insertObject:image atIndex:(_images.count-1)];
    }
    
    [self refreshGridImageView];
    
}
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *) photos sourceAssets:(NSArray *)assets infos:(NSArray<NSDictionary *> *)infos
{
    
}

#pragma mark - UIImagePickerControllerDelegate method

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [_pickerController dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [_images insertObject:image atIndex:(_images.count-1)];
    
    [self refreshGridImageView];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [_pickerController dismissViewControllerAnimated:YES completion:nil];
}

@end
