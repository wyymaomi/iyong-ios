//
//  CarDAO.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CarDAO.h"
#import "CarModel.h"

@implementation CarDAO

- (NSMutableArray*)getAllCar:(NSString*)companyId
{
    __block NSMutableArray *array = nil;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString *sql = [NSString stringWithFormat:@"select * from info_car where companyId = ? ORDER BY updateTime DESC"];
        FMResultSet *rs = [db executeQuery:sql, companyId];
        if (!rs) {
            [rs close];
            return;
        }
        
        array = [NSMutableArray new];
        while ([rs next]) {
            CarModel *carModel = [[CarModel alloc] init];
            carModel.companyId = companyId;
            carModel.id = [rs stringForColumn:@"id"];
            carModel.imgUrl = [rs stringForColumn:@"imgUrl"];
            carModel.number = [rs stringForColumn:@"number"];
            carModel.model = [rs stringForColumn:@"model"];
            carModel.driver = [rs stringForColumn:@"driver"];
            carModel.age = [rs stringForColumn:@"age"];
            carModel.mobile = [rs stringForColumn:@"mobile"];
            carModel.sex = [NSNumber numberWithInteger:[rs intForColumn:@"sex"]];
            carModel.inspectionDate = [NSNumber numberWithDouble:[rs doubleForColumn:@"inspectionDate"]];
            carModel.insuranceDate = [NSNumber numberWithDouble:[rs doubleForColumn:@"insuranceDate"]];
            carModel.certificationStatus = [NSNumber numberWithInteger:[rs intForColumn:@"certificationStatus"]];
            [array addObject:carModel];
        }
        
        
    }];
    return array;
}

- (NSMutableArray*)getCertifiedCarList:(NSString*)companyId
{
    
    __block NSMutableArray *array = nil;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString *sql = [NSString stringWithFormat:@"select * from info_car where companyId = ? AND certificationStatus = 3 ORDER BY updateTime DESC"];
        FMResultSet *rs = [db executeQuery:sql, companyId];
        if (!rs) {
            [rs close];
            return;
        }
        
        array = [NSMutableArray new];
        while ([rs next]) {
            CarModel *carModel = [[CarModel alloc] init];
            carModel.companyId = companyId;
            carModel.id = [rs stringForColumn:@"id"];
            carModel.imgUrl = [rs stringForColumn:@"imgUrl"];
            carModel.number = [rs stringForColumn:@"number"];
            carModel.model = [rs stringForColumn:@"model"];
            carModel.driver = [rs stringForColumn:@"driver"];
            carModel.age = [rs stringForColumn:@"age"];
            carModel.mobile = [rs stringForColumn:@"mobile"];
            carModel.sex = [NSNumber numberWithInteger:[rs intForColumn:@"sex"]];
            carModel.inspectionDate = [NSNumber numberWithDouble:[rs doubleForColumn:@"inspectionDate"]];
            carModel.insuranceDate = [NSNumber numberWithDouble:[rs doubleForColumn:@"insuranceDate"]];
            carModel.certificationStatus = [NSNumber numberWithInteger:[rs intForColumn:@"certificationStatus"]];
            [array addObject:carModel];
        }
        
        
    }];
    return array;
    
}

- (BOOL)updateCarList:(NSArray*)carList;
{
    
    __block BOOL isSuccess = NO;
    
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        for (CarModel *carModel in carList) {
            
            if (carModel == nil || IsStrEmpty(carModel.id)) {
                continue;
            }
            
            if ([carModel.valid boolValue]) {
                NSString *sql = @"replace into info_car(id, companyId, imgUrl, number, model, driver, mobile, sex, age, inspectionDate, insuranceDate, updateTime, certificationStatus) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                [db executeUpdate:sql, carModel.id, carModel.companyId, carModel.imgUrl, carModel.number, carModel.model, carModel.driver, carModel.mobile, carModel.sex, carModel.age, carModel.inspectionDate, carModel.insuranceDate, carModel.updateTime, carModel.certificationStatus];
            }
            else {
                NSString *carId = carModel.id;
                NSString *sql = [NSString stringWithFormat:@"delete from info_car where id = ?"];
                [db executeUpdate:sql, carId];
            }
            
        }
        
        isSuccess = YES;
        
    }];
    return isSuccess;
}

- (BOOL)deleteCarById:(NSString*)carId;
{
    if (IsStrEmpty(carId)) {
        return NO;
    }
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {

        NSString *sql = [NSString stringWithFormat:@"delete from info_car where id = ?"];
        [db executeUpdate:sql, carId];
        
    }];
    
    return YES;
}

@end
