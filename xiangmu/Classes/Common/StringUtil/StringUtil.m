//
//  StringUtil.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "StringUtil.h"
#import "OpenUDID.h"

@implementation StringUtil

+(NSString*)createUDID
{
    
    Class openUDID = NSClassFromString( @"OpenUDID" );
    if ( openUDID )
    {
        return [openUDID value];
    }
    
    return nil;
    
}

+ (NSString*)getSafeString:(NSString*)string;
{
    if (IsStrEmpty(string) || [string contains:@"null"] || [string contains:@"<null>"] || [string contains:@"(null)"]) {
        return @"";
    }
    return string;
}

#pragma mark - 身份证识别
+(BOOL)isIdentityCardNo:(NSString*)idCardStr
{
    if (idCardStr.length != 18) {
        return  NO;
    }
    NSArray* codeArray = [NSArray arrayWithObjects:@"7",@"9",@"10",@"5",@"8",@"4",@"2",@"1",@"6",@"3",@"7",@"9",@"10",@"5",@"8",@"4",@"2", nil];
    NSDictionary* checkCodeDic = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1",@"0",@"X",@"9",@"8",@"7",@"6",@"5",@"4",@"3",@"2", nil]  forKeys:[NSArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil]];
    
    NSScanner* scan = [NSScanner scannerWithString:[idCardStr substringToIndex:17]];
    
    int val;
    BOOL isNum = [scan scanInt:&val] && [scan isAtEnd];
    if (!isNum) {
        return NO;
    }
    int sumValue = 0;
    
    for (int i =0; i<17; i++) {
        sumValue+=[[idCardStr substringWithRange:NSMakeRange(i , 1) ] intValue]* [[codeArray objectAtIndex:i] intValue];
    }
    
    NSString* strlast = [checkCodeDic objectForKey:[NSString stringWithFormat:@"%d",sumValue%11]];
    
    if ([strlast isEqualToString: [[idCardStr substringWithRange:NSMakeRange(17, 1)]uppercaseString]]) {
        return YES;
    }
    return  NO;
}

+(NSString*)encodeBindIdcardNo:(NSString*)idCardStr
{
    if (IsStrEmpty(idCardStr)) {
        return @"";
    }
    NSString *idcardNoEncode = [NSString stringWithFormat:@"%@*****************%@", [idCardStr substringToIndex:1], [idCardStr substringFromIndex:(idCardStr.length - 1)]];
    return idcardNoEncode;
}

+(NSString*)encodeBankcardNo:(NSString*)bankcardNoStr
{
    if (IsStrEmpty(bankcardNoStr)) {
        return @"";
    }
    NSString *idcardNoEncode = [NSString stringWithFormat:@"**** **** **** %@", [bankcardNoStr substringFromIndex:bankcardNoStr.length-4]];
    return idcardNoEncode;
}

/**
 @method 获取指定宽度情况ixa，字符串value的高度
 @param value 待计算的字符串
 @param fontSize 字体的大小
 @param andWidth 限制字符串显示区域的宽度
 @result float 返回的高度
 */
+ (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    if (IsStrEmpty(value)) {
        return 0;
    }
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];//此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.height;
}

+ (float) widthForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    if (IsStrEmpty(value)) {
        return 0;
    }
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];//此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.width;
}

+(CGSize)sizeForString:(NSString*)value fontSize:(float)fontSize andWith:(float)width
{
//    if (IsStrEmpty(value)) {
//        return nil;
//    }
    
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];//此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit;
}



@end
