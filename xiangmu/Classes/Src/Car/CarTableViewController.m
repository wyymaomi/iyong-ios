//
//  CarTableViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CarTableViewController.h"
//#import "PullRefreshTableView.h"
#import "HeaderPullRefreshTableView.h"
#import "VechileTableViewCell.h"
#import "VechileEditViewController.h"
#import "ImageUtil.h"
#import "UserManager.h"
#import "AppConfig.h"
#import "CarDAO.h"
#import "MJRefresh.h"
#import "VechileEditViewModel.h"

@interface CarTableViewController ()<UITableViewDelegate, UITableViewDataSource, PullRefreshTableViewDelegate,EmptyDataDelegate>

@property (nonatomic, strong) NSMutableDictionary *imageDictionary;
@property (nonatomic, strong) CarDAO *carDAO;

@end

@implementation CarTableViewController


-(CarDAO*)carDAO
{
    if (_carDAO == nil) {
        _carDAO = [[CarDAO alloc] init];
    }
    return _carDAO;
}

-(NSMutableDictionary*)imageDictionary
{
    if (_imageDictionary == nil) {
        _imageDictionary = [NSMutableDictionary new];
    }
    return _imageDictionary;
}


#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"车辆管理";
    [self initNavigationBar];
    [self.view addSubview:self.headerPullRefreshTableView];
    self.headerPullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.emptyDataDelegate = self;
    
}

-(void)initNavigationBar
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"添加" style:UIBarButtonItemStyleDone target:self action:@selector(doAddCar)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.headerPullRefreshTableView beginHeaderRefresh];
}


#pragma mark - business logic method

-(void)onHeaderRefresh
{
    [self refreshData];
}

-(void)doAddCar
{
    VechileEditViewController *vc = [[VechileEditViewController alloc] init];
    vc.editType = 0;
    [self.navigationController pushViewController:vc animated:YES];
}

-(double)getMaxUpdateTime:(NSArray*)dataList
{
    
    double maxUpdateTime = 0L;
    for (CarModel *car in dataList) {
        if ([car.updateTime doubleValue] > maxUpdateTime) {
            maxUpdateTime = [car.updateTime doubleValue];
        }
    }
    return maxUpdateTime;
}

-(void)refreshData
{
    
    self.nextAction = @selector(refreshData);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *companyId = [[UserManager sharedInstance].userModel companyId];
    if (self.listType == VechileListTypeAll) {
        self.dataList = [self.carDAO getAllCar:companyId];
    }
    else if (self.listType == VechileListTypecertified) {
        self.dataList = [self.carDAO getCertifiedCarList:companyId];
    }
    
    [self.headerPullRefreshTableView reloadData];
    for (NSInteger i = 0; i < self.dataList.count; i++) {
        CarModel *carModel = self.dataList[i];
        if (!IsStrEmpty(carModel.imgUrl)) {
            [self downloadCarImage:carModel.imgUrl];
        }
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_LIST_API];
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:[AppConfig currentConfig].carLastUpdateStr]];
    
//    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf resetAction];
//        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf.headerPullRefreshTableView endHeaderRefresh];
        strongSelf.loadStatus = LoadStatusFinish;
        
        NSError *error;
        CarListModel *carList = [[CarListModel alloc] initWithDictionary:responseData error:&error];
        if (error != nil) {
            return;
        }
        NSInteger code_status = [carList.code integerValue];
        if (code_status == STATUS_OK) {
            
            if (!IsArrEmpty(carList.data)) {
                // 增量更新
                // 本地数据库更新
                [strongSelf.carDAO updateCarList:carList.data];
                
                if (strongSelf.listType == VechileListTypeAll) {
                    strongSelf.dataList = [strongSelf.carDAO getAllCar:companyId];
                }
                else if(strongSelf.listType == VechileListTypecertified){
                    strongSelf.dataList = [strongSelf.carDAO getCertifiedCarList:companyId];
                }
                
                // 本地缓存最新更新时间，取列表中最大的updateTime
                double maxUpdateTime = [strongSelf getMaxUpdateTime:carList.data];
                [AppConfig currentConfig].carLastUpdateStr = [NSString stringWithFormat:@"%.f", (double)maxUpdateTime];
                [strongSelf.headerPullRefreshTableView reloadData];
                
//                for (NSInteger i = 0; i < carList.data.count; i++) {
//                    CarModel *carModel = carList.data[i];
//                    if (!IsStrEmpty(carModel.imgUrl) && [carModel.valid boolValue]) {
//                        [strongSelf downloadCarImage:carModel.imgUrl];
//                    }
//                }
            }
            
        }
        else {
            [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
//            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    }andFailure:^(NSString *errorDesc) {
        StrongSelf
//        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf.headerPullRefreshTableView endHeaderRefresh];
        [strongSelf resetAction];
        strongSelf.loadStatus = LoadStatusNetworkError;
    }];
}

-(void)downloadCarImage:(NSString*)imgUrl
{
    NSString *convertImgUrl = [imgUrl replaceAll:@"/" with:@"-"];
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            [strongSelf.imageDictionary setValue:imageData forKey:convertImgUrl];
            [strongSelf.headerPullRefreshTableView reloadData];
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

-(void)deleteVechileItem:(NSString*)carId
{
    self.nextAction = @selector(deleteVechileItem:);
    self.object1 = carId;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_Delete_API];
    NSString *parmas = [NSString stringWithFormat:@"id=%@", carId];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        StrongSelf
        NSLog(@"response success");
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"车辆删除成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [strongSelf.carDAO deleteCarById:carId];
                    [strongSelf.headerPullRefreshTableView beginHeaderRefresh];
                }
            }];
        }
        else {
            [strongSelf showAlertView:[self getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        StrongSelf
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
    }];
}

- (NSString*)emptyTitleText;
{
//    return @"暂时没有车辆数据";
    return nil;
}

- (NSString*)emptyDetailText;
{
    if (self.listType == VechileListTypeAll) {
        return @"您还没有添加车辆数据\n快去添加车辆并且认证吧";
    }
    else {
        return @"您还没有添加认证车辆数据\n快去添加车辆并且认证吧";
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return self.dataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    if (section == 0) {
        return 0.00001f;
    }
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 94;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellIdentifier";
    VechileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"VechileTableViewCell" owner:nil options:nil] lastObject];
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    CarModel *model = self.dataList[indexPath.section];
    cell.carNumberLbl.text = model.number;
    cell.driverNameLbl.text = model.driver;
    cell.driverMobile.text = model.mobile;
    
//    [cell.vechileImageView downloadImage:model.imgUrl placeholderImage:[UIImage imageNamed:@"img_vechile_default"] success:nil andFailure:nil];
    
//    CGSize imageSize = CGSizeMake(100, 80);
    
    // Configure the cell...
//    if (IsStrEmpty(model.imgUrl)) {
//        cell.vechileImageView.image = [UIImage imageNamed:@"img_vechile_default"];
//    }
//    else {
//        NSString *key = [model.imgUrl replaceAll:@"/" with:@"-"];
//        if (self.imageDictionary[key] == nil) {
//            cell.vechileImageView.image = [UIImage imageNamed:@"img_vechile_default"];
//        }
//        else {
//            NSData *data = self.imageDictionary[key];
//            model.imgData = data;
//            UIImage *image = [UIImage imageWithData:data];
//            cell.vechileImageView.image = image;//[ImageUtil imageWithImage:image scaledToSize:imageSize];
//        }
//    }

//    [cell.vechileImageView downloadImage:model.imgUrl placeholderImage:[UIImage imageNamed:@"img_vechile_default"] success:^(id responseData) {
//    [cell.vechileImageView downloadImageFromAliyunOSS:model.imgUrl isThumbnail:YES placeholderImage:[UIImage imageNamed:@"img_vechile_default"] success:nil andFailure:nil];
        
//    } andFailure:^(NSString *errorDesc) {
//        
//        
//    }];
    return cell;
}

//删除操作
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //获取到相应cell
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        CarModel *model = self.dataList[indexPath.section];
        WeakSelf
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否删除" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [weakSelf deleteVechileItem:model.id];
            }
        }];
        
    }
}


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CarModel *model = self.dataList[indexPath.section];
    if (self.source_from == SOURCE_FROM_ORDER_MANAGER) {
        [self addCarModel:model];
    }
    else if (self.source_from == SOURCE_FROM_SALES_QUERY) {
        if (self.carDelegate && [self.carDelegate respondsToSelector:@selector(onGetCarData:)]) {
            [self.carDelegate onGetCarData:model];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (self.source_from == SOURCE_FROM_COMMUNITY_PUBLISH) {
        if (self.carDelegate && [self.carDelegate respondsToSelector:@selector(onGetCarData:)]) {
            [self.carDelegate onGetCarData:model];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else {
        VechileEditViewController *vc = [[VechileEditViewController alloc] init];
        vc.editType = 1;
        vc.vechileViewModel.vechileModel = model;
//        vc.vechileModel = model;
        NSString *key = [model.imgUrl replaceAll:@"/" with:@"-"];
        if (self.imageDictionary[key] != nil) {
            NSData *data = self.imageDictionary[key];
            vc.imageData = data;
        }
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)addCarModel:(CarModel*)model
{
    NSString *params = [NSString stringWithFormat:@"orderId=%@&carId=%@", self.orderId, model.id];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_SAVE_CARINFO_API];
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
//        id code_status = responseData[@"code"];
//        if ([code_status isKindOfClass:[NSDictionary class]]) {
        
        NSInteger code = [responseData[@"code"] integerValue];
        if (code == STATUS_OK) {
            
            id data = responseData[@"data"];
            if ([data isKindOfClass:[NSDictionary class]]) {
                NSDictionary *carData = responseData[@"data"];
                if ([carData[@"status"] integerValue] == 3) {
                    NSDictionary *vo = carData[@"vo"];
                    if (strongSelf.carDelegate && [strongSelf.carDelegate respondsToSelector:@selector(onGetVechileData:)]) {
                        [strongSelf.carDelegate onGetVechileData:vo];
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                }
                
            }
            else if ([data isKindOfClass:[NSArray class]]) {
                NSArray *dataList = responseData[@"data"];
                
                if (!IsArrEmpty(dataList)) {
                    for (NSDictionary *dict in dataList) {
                        NSUInteger status = [dict[@"status"] integerValue];
                        NSDictionary *vo = dict[@"vo"];
                        
                        if (status == 3) {
                            
                            if (strongSelf.carDelegate && [strongSelf.carDelegate respondsToSelector:@selector(onGetVechileData:)]) {
                                [strongSelf.carDelegate onGetVechileData:vo];
                                [strongSelf.navigationController popViewControllerAnimated:YES];
                            }
                            
                        }
                    }
                }
            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code]];
        }
    }andFailure:^(NSString *errorDesc) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf showAlertView:errorDesc];
    }];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
