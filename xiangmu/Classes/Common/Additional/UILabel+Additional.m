//
//  UILabel+Additional.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UILabel+Additional.h"

@implementation UILabel (Additional)

#pragma mark - label 的不同文字设置不同的颜色
-(void)setTextColor:(UIColor*)textColor range:(NSRange)range
{
    if (IOS6_OR_LATER) {
        NSDictionary *attributes = @{NSForegroundColorAttributeName: self.textColor};
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text attributes:attributes];
        
        [attributedText setAttributes:@{NSForegroundColorAttributeName:textColor}
                                range:range];
        self.attributedText = attributedText;
    }
}

-(void)setTextCustomAttribute:(UIColor*)textColor font:(UIFont*)font string:(NSString*)string
{
    if (IsStrEmpty(string)) {
        return;
    }
    
    if (IOS6_OR_LATER) {
        
//        NSDictionary *attributes = @{NSForegroundColorAttributeName:self.text
        
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
        [attributedText addAttributes:@{NSForegroundColorAttributeName:textColor} range:[self.text rangeOfString:string]];
        [attributedText addAttribute:NSFontAttributeName value:font range:[self.text rangeOfString:string]];
        self.attributedText = attributedText;
    }
}

-(void)setTextColor:(UIColor *)textColor text:(NSString*)text;
{
    if (IsStrEmpty(text)) {
        return;
    }
    
    if (IOS6_OR_LATER) {
        
        NSInteger begin = [self.text indexOfString:text];
        NSInteger len = text.length;
        
        NSDictionary *attributes = @{NSForegroundColorAttributeName: self.textColor};
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text attributes:attributes];
        [attributedText addAttributes:@{NSForegroundColorAttributeName:textColor}
                                range:NSMakeRange(begin, len)];
        self.attributedText = attributedText;
        
    }
}

-(void)setTextFont:(UIFont*)font text:(NSString*)text;
{
    if (IsStrEmpty(text)) {
        return;
    }
    
    if (IOS6_OR_LATER) {
        
        NSInteger begin = [self.text indexOfString:text];
        NSInteger len = text.length;

        NSDictionary *attributes = @{NSFontAttributeName:self.font};
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
        
        [attributedText addAttribute:NSFontAttributeName value:font range:[self.text rangeOfString:text]];
//        [attributedText addAttribute:NSFontAttributeName value:font range:NSMakeRange(begin, len)];
        
//        [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold"size:14.0] range:NSMakeRange(0, 13)];  
//        [attributedText addAttributes:attributes range:NSMakeRange(begin, len)];
//        [attributedText addAttributes:@{NSFontAttributeName: font}
//                                range:NSMakeRange(begin, len)];
        self.attributedText = attributedText;
//        [self sizeToFit];
        
//        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text attributes:attributes];
        
//        NSDictionary *attributes = @{NSTextFont
        
//        [noteStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:redRangeTwo];
//        [cell.priceLabel setAttributedText:noteStr];
//        [cell.priceLabel sizeToFit];
    }
    
}

- (void)alignTop {
    CGSize fontSize = [self.text sizeWithFont:self.font];
    double finalHeight = fontSize.height * self.numberOfLines;
    double finalWidth = self.frame.size.width;    //expected width of label
    CGSize theStringSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:self.lineBreakMode];
    int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
    for(int i=0; i<newLinesToPad; i++)
        self.text = [self.text stringByAppendingString:@"\n "];
}

@end
