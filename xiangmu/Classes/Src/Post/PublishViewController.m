//
//  PublishViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "PublishViewController.h"
#import "PostExperienceViewController.h"

@interface PublishViewController ()

@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *postExperienceButton;
@property (weak, nonatomic) IBOutlet UIButton *postServiceProviderButton;

@end

@implementation PublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor blackColor];
    
    [self setNavTitleView];
    
//    self.title = @"发布";
    [self.postExperienceButton addTarget:self action:@selector(postExperience) forControlEvents:UIControlEventTouchUpInside];
    [self.postServiceProviderButton addTarget:self action:@selector(postServiceProvider) forControlEvents:UIControlEventTouchUpInside];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
}

-(void)setNavTitleView
{
    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    button.backgroundColor = [UIColor clearColor];
//    UIImage *imageForButton = [UIImage imageNamed:@"icon_gps"];
//    [button setImage:imageForButton forState:UIControlStateNormal];
//    // 设置文字
//    NSString *cityName = [AppConfig currentConfig].gpsCityName;
//    NSString *buttonTitleStr = cityName;
//    [button setTitle:buttonTitleStr forState:UIControlStateNormal];
//    button.titleLabel.font = FONT(15);
//    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    CGSize buttonTitleLabelSize = [buttonTitleStr sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}];
//    CGSize buttonImageSize = imageForButton.size;
//    button.frame = CGRectMake(0, 0, buttonImageSize.width + buttonTitleLabelSize.width + 20, buttonImageSize.height);
//    //    button.frame = CGRectMake(0, 0, 100, 100);
//    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
//    [button setImageEdgeInsets:UIEdgeInsetsMake(0.0, -20, 0.0, 0.0)];
//    //    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
//    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.width, 30)];
    titleLabel.text = @"发布";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = BOLD_FONT(20);
    titleLabel.textAlignment = UITextAlignmentCenter;
    [view addSubview:titleLabel];
    self.navigationItem.titleView = view;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_advertisement"] style:UIBarButtonItemStyleDone target:self action:@selector(onClickAdvIcon)];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickCloseButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)postExperience
{
    PostExperienceViewController *viewController = [[PostExperienceViewController alloc] init];
    viewController.type = TableTypeUserExperience;
    [self presentViewController:viewController animated:YES completion:nil];
//    [self presentPage:viewController animated:YES];
//    [self presentPage:@"PostExperienceViewController" animated:YES];
}

- (void)postServiceProvider
{
//    [self presentPage:@"PostExperienceViewController" animated:YES];
    
#pragma mark - 跳转到服务商编辑页面
    
//    - (void)onPublish
//    {
//        
        self.nextAction = @selector(postServiceProvider);
        self.object1 = nil;
        self.object2 = nil;
        
        if ([self needLogin]) {
            return;
        }
        
        // 服务商权限校验
        if ([[UserManager sharedInstance].userModel isRealnameCertified] && [[UserManager sharedInstance].userModel isCompanyCertified]) {
            [self resetAction];
            PostExperienceViewController *viewController = [[PostExperienceViewController alloc] init];
            viewController.type = TableTypeCompanyBusiness;
            [self presentViewController:viewController animated:YES completion:nil];
        }
        else {
            [self resetAction];
            WeakSelf
            [YYAlertView showAlertView:@"" message:@"请先进行实名认证和企业认证" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [weakSelf gotoPage:@"UserInfoViewController"];
                }
                
            }];
        }
        
//    }

    

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
