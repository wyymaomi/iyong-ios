//
//  MyDetailTableViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RegisterUserInfoViewController.h"

#import "MyTableViewCell.h"
#import "MyUtil.h"
#import "NetworkManager.h"
#import "HttpConstants.h"
#import "UserManager.h"
#import "GTMBase64.h"

#import "AvatarViewController.h"
#import "InputHelperViewController.h"
#import "RealnameCertificationViewController.h"
#import "BindMailboxViewController.h"
//#import "GestureLoginViewController.h"
#import "GesturePasswordController.h"
#import "CompanyCertificationViewController.h"
#import "MyTableViewCell+Additional.h"



@interface RegisterUserInfoViewController ()<UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate, InputHelperDelegate,RealnameCertificationDelegate, MailboxDelegate,CompanyCertificationDelegate>
{
    
}

//@property (nonatomic, strong) UIButton *okButton;
//@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSString *realName;
@property (nonatomic, assign) NSInteger companyCertification;
@property (nonatomic, assign) BOOL idCertification;
@property (nonatomic, strong) NSString *email;

@end

@implementation RegisterUserInfoViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    bPopupLoginView = NO;
    
    self.title = @"个人信息";
    
    [self.view addSubview:self.groupTableView];
    
}

-(void)onNextButtonClick
{
    

    // edited by wyy 2016/11/10
    // 头像、昵称必填，公司名称、实名认证非必填
    if (IsStrEmpty(self.imageFileContents)) {
        [self showAlertView:@"请设置头像"];
        return;
    }
    
    NSIndexPath *nickNameIndexPath = [NSIndexPath indexPathForRow:RegisterUserInfoRowNickName inSection:0];
    MyTableViewCell *nickNameCell = (MyTableViewCell*)[self.groupTableView cellForRowAtIndexPath:nickNameIndexPath];
    NSString *nickName = nickNameCell.detailLabel.text.trim;
    if (IsStrEmpty(nickName)) {
        [self showAlertView:@"请输入昵称"];
        return;
    }
    
    NSIndexPath *companyNameIndexPath = [NSIndexPath indexPathForRow:RegisterUserInfoRowCompanyName inSection:0];
    MyTableViewCell *companyNameCell = (MyTableViewCell*)[self.groupTableView cellForRowAtIndexPath:companyNameIndexPath];
    NSString *companyName = [StringUtil getSafeString:companyNameCell.detailLabel.text.trim];
    //    if (IsStrEmpty(companyName)) {
    //        [self showAlertView:@"请输入公司名称"];
    //        return;
    //    }
    
//    BOOL isIdcardCertificationed = self.idCertification;//[[UserManager sharedInstance].userModel.certification boolValue];
//    if (!isIdcardCertificationed) {
//        [self showAlertView:@"请先进行实名认证"];
//        return;
//    }
    
    
    self.nextAction = @selector(onNextButtonClick);
    self.object1 = nil;
    self.object2 = nil;
    
    // 个人信息上传
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL,UserInfo_Update_API];
    NSString *params = [NSString stringWithFormat:@"nickname=%@&companyName=%@", nickName,companyName];
    if (!IsStrEmpty(self.imageFileContents)) {
        params = [params stringByAppendingFormat:@"&fileContent=%@", self.imageFileContents];
    }
    
    [self showHUDIndicatorViewAtCenter:@"正在保存用户信息，请稍候..."];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView  alloc] initWithTitle:@"" message:@"保存用户信息成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    
                    if ([[UserManager sharedInstance] gesturePasswordIsExit]) {
                        
                        [strongSelf dismissToRootViewController:YES completion:nil];
                        
                    }
                    
                    else {
                        
                        [strongSelf gotoNextPage];
                        
                    }
                    
                }
                
            }];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
    
    
}

-(void)gotoNextPage
{
    GesturePasswordController *viewController = [[GesturePasswordController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
    
}


//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([self.groupTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.groupTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.groupTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.groupTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}


#pragma mark - AvatarViewController Delegate method
-(void)onGetAvatarImage:(NSData*)imageData;
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:RegisterUserInfoRowAvatar inSection:0];
    MyTableViewCell *cell = (MyTableViewCell *)[self.groupTableView cellForRowAtIndexPath:indexPath];
    UIImageView *headView = (UIImageView *)[cell.contentView viewWithTag:900];
    headView.image = [UIImage imageWithData:imageData];
    self.imageFileContents = [[NSString alloc] initWithData:[GTMBase64 encodeData:imageData] encoding:NSUTF8StringEncoding];
    
}

#pragma mark - Real name certification delegate method
-(void)onRealnameCertificationResult:(BOOL)isCertificationed realName:(NSString*)realName idcardNo:(NSString*)idCardNo;
{

    _idCertification = isCertificationed;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:RegisterUserInfoRowIdCertification inSection:0];
    [self.groupTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    

}


#pragma mark - InputHelperTextDelegate
-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
{
    //    NSIndexPath *indexPath = [NSIndexPath indexPath]
    if (tag == MyNicknameTextField) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:RegisterUserInfoRowNickName inSection:0];
        MyTableViewCell *cell = (MyTableViewCell*)[self.groupTableView cellForRowAtIndexPath:indexPath];
        cell.detailLabel.text = text.trim;
    }
    else if (tag == MyCompanyTextField) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:RegisterUserInfoRowCompanyName inSection:0];
        MyTableViewCell *cell = (MyTableViewCell*)[self.groupTableView cellForRowAtIndexPath:indexPath];
        cell.detailLabel.text = text.trim;
    }
}

#pragma mark - Bind email delegate method

-(void)onBindMailboxSuccess:(NSString*)emailBox;
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:RegisterUserInfoRowUsrInfoBindEmail inSection:0];
    MyTableViewCell *cell = (MyTableViewCell*)[self.groupTableView cellForRowAtIndexPath:indexPath];
    cell.detailLabel.text = emailBox;
    
}

#pragma mark - Company Certificationed

- (void)onCompanyCertification
{
    _companyCertification =  CompanyCertificationStatusInReview;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:RegisterUserInfoRowCompanyCertification inSection:0];
    [self.groupTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //    return 3;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row==0) {
        return 60;
    }
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 100;
    }
    return 0.00001f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 100)];
//        if (self.source_from == SOURCE_FROM_REGISTER) {
            UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [okButton setTitle:@"下一步" forState:UIControlStateNormal];
            okButton.frame = CGRectMake(15, 20, ViewWidth-30, 40);
            [okButton blueStyle];
            [okButton addTarget:self action:@selector(onNextButtonClick) forControlEvents:UIControlEventTouchUpInside];
            
            [view addSubview:okButton];
//        }
        
        
        return view;
        
    }
    
    return [[UIView alloc] init];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellid=@"cellid";
    MyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[MyTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
        
        UIImageView *avatarImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-80,10 ,40,40)];
        avatarImageView.tag=900;
        [cell.contentView addSubview:avatarImageView];
        
//        NSInteger level = [[UserManager sharedInstance].userModel.level integerValue];
//        if (level != 1) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        }
        
    }
    UIImageView *headView=(UIImageView *)[cell.contentView viewWithTag:900];
    
    if (indexPath.section == 0) {
        
        NSArray *nameArray = @[@"头像", @"手机号", @"实名认证", @"昵称", @"公司名称", @"企业认证", @"邮箱帐号"];
        cell.name.text=nameArray[indexPath.row];
        
        if (indexPath.row == RegisterUserInfoRowAvatar) {
            cell.disclosureImageView.frame = CGRectMake(cell.disclosureImageView.origin.x, (cell.contentView.size.height-42/3)/2, 23/3, 42/3);
            
            if (self.avatarImageData == nil || self.avatarImageData.length == 0) {
                headView.image = [UIImage imageNamed:@"icon_avatar"];
            }
            else {
                headView.image = [UIImage imageWithData:self.avatarImageData];
                headView.cornerRadius = 20;
            }
        }
        else if(indexPath.row == RegisterUserInfoRowMobile) {
            cell.disclosureImageView.hidden = YES;
            cell.detailLabel.text = [UserManager sharedInstance].userModel.username;
        }
        
        else if(indexPath.row == RegisterUserInfoRowIdCertification) {
            cell.name.text = @"实名认证";
            cell.detailLabel.text = self.realName;//[UserManager sharedInstance].userModel.realName;
            
            NSString *verifyStr;
            UIColor *btnBgColor;
            if (self.idCertification) {
                verifyStr = @"已认证";
                btnBgColor = UIColorFromRGB(0x30a6df);
            }
            else {
                verifyStr = @"未认证";
                btnBgColor = [UIColor lightGrayColor];
            }
            cell.verifyButton.hidden = NO;
            [cell.verifyButton setBackgroundImage:[UIImage imageWithColor:btnBgColor] forState:UIControlStateNormal];
            [cell.verifyButton setTitle:verifyStr forState:UIControlStateNormal];
            cell.verifyButton.cornerRadius = 5;
            cell.verifyButton.titleLabel.font = FONT(10);
            
            CGSize textSize = [cell.detailLabel.text textSize:CGSizeMake(MAXFLOAT, 20) font:cell.detailLabel.font];
//            CGSize textSize = [cell.detailLabel.text sizeWithFont:cell.detailLabel.font constrainedToSize:CGSizeMake(10000, 20)];
            NSInteger buttonWidth = 45;
            NSInteger buttonHeight = 20;
            NSInteger buttonLeft = cell.detailLabel.right - textSize.width - buttonWidth - 10;
            cell.verifyButton.frame = CGRectMake(buttonLeft, (cell.contentView.frame.size.height-buttonHeight)/2, buttonWidth, buttonHeight);
            
            
        }
        if (indexPath.row == RegisterUserInfoRowNickName) {
            cell.detailLabel.text = [UserManager sharedInstance].userModel.nickname;
        }
        if (indexPath.row == RegisterUserInfoRowCompanyName) {
            cell.detailLabel.text = [UserManager sharedInstance].userModel.companyName;
        }
        if (indexPath.row == RegisterUserInfoRowUsrInfoBindEmail) {
            cell.name.text = @"邮箱帐号";
            cell.detailLabel.text = [UserManager sharedInstance].userModel.email;
            
        }
//        if (indexPath.row == RegisterUserInfoRowCompanyCertification) {
            if (indexPath.row == RegisterUserInfoRowCompanyCertification) {
                
                NSString *verifyStr;
                UIColor *btnBgColor;
                
                if (_companyCertification == CompanyCertificationStatusInit) {
                    verifyStr = @"未认证";
                    btnBgColor = [UIColor lightGrayColor];
                }
                else if (_companyCertification == CompanyCertificationStatusInReview) {
                    verifyStr = @"待认证";
                    btnBgColor = [UIColor lightGrayColor];
                }
                else if (_companyCertification == CompanyCertificationStatusFinished) {
                    verifyStr = @"已认证";
                    btnBgColor = UIColorFromRGB(0x30a6df);
                }
                else if (_companyCertification == CompanyCertificationStatusRejected) {
                    verifyStr = @"已拒绝";
                    btnBgColor = [UIColor redColor];
                }
                else {
                    verifyStr = @"未认证";
                    btnBgColor = [UIColor lightGrayColor];
                }
                
                cell.detailLabel.text = @"";
                
                cell.verifyButton.hidden = NO;
                [cell.verifyButton setBackgroundImage:[UIImage imageWithColor:btnBgColor] forState:UIControlStateNormal];
                [cell.verifyButton setTitle:verifyStr forState:UIControlStateNormal];
                cell.verifyButton.cornerRadius = 5;
                cell.verifyButton.titleLabel.font = FONT(10);
                
                NSInteger buttonWidth = 45;
                NSInteger buttonHeight = 20;
                cell.verifyButton.frame = CGRectMake(cell.detailLabel.right - buttonWidth - 10, (cell.contentView.frame.size.height-buttonHeight)/2, buttonWidth, buttonHeight);
                

        }
       
        
    }
    
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // 调度员不可编辑 只有老板才可以编辑
    
//    NSInteger level = [[UserManager sharedInstance].userModel.level integerValue];
//    if (level == 1) {
    
        MyTableViewCell *cell=(MyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (indexPath.section == 0) {
            if (indexPath.row == RegisterUserInfoRowAvatar) {
                AvatarViewController *viewController=[[AvatarViewController alloc]init];
                viewController.delegate=self;
                viewController.avatarImageData = self.avatarImageData;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            if (indexPath.row == RegisterUserInfoRowIdCertification) {
                
                RealnameCertificationViewController *viewController = [[RealnameCertificationViewController alloc] init];
                viewController.type = self.idCertification?0:1;
                viewController.delegate = self;
                [self.navigationController pushViewController:viewController animated:YES];
                
            }
            if (indexPath.row == RegisterUserInfoRowNickName) {
                
                InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
                viewController.text = cell.detailLabel.text;
                viewController.tag = MyNicknameTextField;
                viewController.delegate = self;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            if (indexPath.row == RegisterUserInfoRowCompanyName) {
                InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
                viewController.text = cell.detailLabel.text;
                viewController.tag = MyCompanyTextField;
                viewController.delegate = self;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            if (indexPath.row == RegisterUserInfoRowUsrInfoBindEmail) {
                MyTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                BindMailboxViewController *viewController = [[BindMailboxViewController alloc] init];
                viewController.emailbox = cell.detailLabel.text;//[UserManager sharedInstance].userModel.email;
                viewController.delegate = self;
                [self.navigationController pushViewController:viewController animated:YES];
                
            }
            if (indexPath.row == RegisterUserInfoRowCompanyCertification) {
                CompanyCertificationViewController *viewController = [[CompanyCertificationViewController alloc] init];
                viewController.delegate = self;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            
        }
}



//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}


@end
