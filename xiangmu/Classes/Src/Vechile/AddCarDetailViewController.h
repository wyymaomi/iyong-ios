//
//  AddCarDetailViewController.h
//  xiangmu
//
//  Created by David kim on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"

typedef NS_ENUM(NSInteger,SearchType)
{
    SearchTypeAdd=10,
    SearchTypeEdit,
};

@protocol Get<NSObject>
-(void)getRow:(NSInteger )row WithTitle:(NSString *)title;
@end
@protocol Sex <NSObject>

-(void)ChangeRow:(NSInteger )row WithTitle:(NSString *)title;

@end
@protocol Date <NSObject>

-(void)KnowRow:(NSInteger)row WithDate:(NSString *)date;

@end

@protocol Age <NSObject>

-(void)AgeRow:(NSInteger)row WithTitle:(NSString *)title;

@end
@interface AddCarDetailViewController : LFNavController
@property (nonatomic,assign)NSInteger section;
@property (nonatomic,assign)NSInteger row;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *rightname;
@property (nonatomic,copy)NSString *dateStr;
@property (nonatomic,assign)id<Get,Sex,Date,Age>delegate;

@property (nonatomic,assign)SearchType type;
@end
