//
//  VechileManageViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileManageViewController.h"
#import "VechileManageViewController+NetworkRequest.h"
#import "VechileManageTableViewCell.h"
#import "CustomSegment.h"
#import "MyTravelListViewController.h"
#import "VechileLocationViewController.h"

@interface VechileManageViewController ()<CustomSegmentDelegate>

@property (nonatomic, strong) CustomSegment *segment;

@end

@implementation VechileManageViewController

-(CustomSegment*)segment
{
    if (!_segment) {
        _segment = [[CustomSegment alloc] initWithFrame:CGRectZero];
    }
    return _segment;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"车辆管理";
    
    self.bindStatus = BindStatusFinish;
    
    // Do any additional setup after loading the view.
    [self.view addSubview:self.pullRefreshTableView];
    
    self.segment.frame = CGRectMake(0, 0, ViewWidth, 40*Scale);
    self.segment.items = @[@"已绑定", @"绑定中", @"已拒绝"];
    self.segment.delegate = self;
    [self.view addSubview:self.segment];
    
    self.pullRefreshTableView.top = self.segment.bottom+10*Scale;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"绑定司机" style:UIBarButtonItemStyleDone target:self action:@selector(onBindDriver)];
    
}

-(void)onBindDriver
{
    [self gotoPage:@"BindDriverViewController"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    self.pullRefreshTableView.top = 40*Scale;
    
    [self.pullRefreshTableView beginHeaderRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PullRefreshDelgate method

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    [self fetchDriverList:self.bindStatus createTime:@""];
    
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        return;
    }
    
    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    BindDriverModel *model = [self.dataList lastObject];
    [self fetchDriverList:self.bindStatus createTime:model.createTime];
    
}

#pragma mark - CustomSegmentDelegate delegate method

-(void)didSelectAtIndex:(NSInteger)index;
{
    if (index == 0) {
        self.bindStatus = BindStatusFinish;
    }
    else if (index == 1) {
        self.bindStatus = BindStatusInProgress;
    }
    else if (index == 2) {
        self.bindStatus = BindStatusReject;
    }
    [self.pullRefreshTableView beginHeaderRefresh];
    
}

#pragma mark - UITableView Delegate method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    DLog(@"-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView");
//    return 1;
    return self.dataList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    DLog(@"tableView:(UITableView*)tableView numberOfRowsInSection:(NSIndexPath*)indexPath");
    //    return 1;
//    return self.dataList.count;
//    return 2;
//    return self.dataList.count;
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [VechileManageTableViewCell getCellHeight];
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    //    DLog(@"- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section");
//    return 10;
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.5;
//}
//
//- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    //    DLog(@"- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section");
//    return [UIView new];
//}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    VechileManageTableViewCell *cell = [VechileManageTableViewCell cellWithTableView:tableView];
    BindDriverModel *model = self.dataList[indexPath.section];
    cell.vechileModelLabel.text = model.model;
    cell.driverName.text = model.name;
    cell.vechileNumberLabel.text = model.carNumber;
    cell.driverMobile.text = model.username;
    cell.monthDistanceLabel.text = [NSString stringWithFormat:@"当月：%@公里", model.curMonthTravel];
    [cell.vechileImageView yy_setImageWithObjectKey:model.imgUrl placeholder:VechilePlaceholderImage manager:[YYWebImageManager  sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
    }];
    
    NSInteger status = self.bindStatus;//[model.status integerValue];
    cell.operationButton.tag = indexPath.section;
    if (status == 1) {
        [cell.operationButton setTitle:@"确认中" forState:UIControlStateNormal];
        cell.operationButton.enabled = NO;
    }
    else if (status == 2) {
        [cell.operationButton setTitle:@"注销" forState:UIControlStateNormal];
        cell.operationButton.enabled = YES;
        [cell.operationButton addTarget:self action:@selector(unbindDriver:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (status == 3) {
        cell.operationButton.enabled = NO;
        [cell.operationButton setTitle:@"已拒绝" forState:UIControlStateNormal];
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.bindStatus == BindStatusFinish) {
        BindDriverModel *model = self.dataList[indexPath.section];
        
//        VechileLocationViewController *viewController = [VechileLocationViewController new];
//        viewController.entityName = model.driverId;
//        [self.navigationController pushViewController:viewController animated:YES];
        
        MyTravelListViewController *viewController = [[MyTravelListViewController alloc] init];
        viewController.driverId = model.driverId;
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    
}
@end
