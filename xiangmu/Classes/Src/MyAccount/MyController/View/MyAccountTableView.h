//
//  MyAccountTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableView.h"

@protocol MyAccountTableViewDelegate <NSObject>

- (void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath;

@end

typedef NS_ENUM(NSInteger, MyAccountFirstSectionRows)
{
    MyAccountRowCreditLine = 0,// 信用额度
    MyAccountRowCreditAmount,// 信用余额
    MyAccountRowAccountBalance,// 帐户余额
    MyAccountRowBonus,// 奖励
    MyAccountRowTradeRecord // 交易记录
};

typedef NS_ENUM(NSInteger, MyAccountSecondRows)
{
    MyAccountRowBankcard = 0, // 银行卡
    MyAccountPasswordManage // 密码管理
};

@interface MyAccountTableView : BaseTableView

@property (nonatomic, weak) id<MyAccountTableViewDelegate> accountDelegate;

@property (nonatomic, strong) NSArray *cellList;

@property (nonatomic, strong) NSNumber *creditLine;// 信用额度
@property (nonatomic, strong) NSNumber *creditAmount;// 信用余额
@property (nonatomic, strong) NSNumber *accountAmount;// 账户余额

@end
