//
//  RedpackView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RedpackView.h"
#import "ShareUtil.h"
#import "AppDelegate.h"
#import "BBAlertView.h"
#import "ShareUtil.h"

@interface RedpackView ()<UMSocialShareMenuViewDelegate>



@property (nonatomic, strong) CAShapeLayer *lineLayer;
//@property (nonatomic, strong) UIButton *sen

@end

@implementation RedpackView

-(RedpackDAO*)redpackDAO
{
    if (!_redpackDAO) {
        _redpackDAO = [[RedpackDAO alloc] init];
    }
    return _redpackDAO;
}

-(UILabel*)moneyLabel
{
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc] init];
//        _moneyLabel.font = BOLD_FONT(20);
    }
    return _moneyLabel;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        [self wxRedPacket:frame];
    }
    
    return self;
}

-(UIImageView*)backgroundImageView
{
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] init];
        _backgroundImageView.image = [UIImage imageNamed:@"img_bg_red_envelop"];
    }
    return _backgroundImageView;
}

-(UIImageView*)shareBackgroundImageView
{
    if (!_shareBackgroundImageView) {
        _shareBackgroundImageView = [[UIImageView alloc] init];
        _shareBackgroundImageView.image = [UIImage imageNamed:@"img_redpack_opened"];
    }
    return _shareBackgroundImageView;
}

-(void)wxRedPacket:(CGRect)frame
{
    
    self.backgroundImageView.frame = CGRectMake(0, 46*Scale, frame.size.width, frame.size.height-46*Scale);
    [self addSubview:self.backgroundImageView];
    
    self.shareBackgroundImageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    [self addSubview:self.shareBackgroundImageView];
    self.shareBackgroundImageView.hidden = YES;
    
    self.moneyLabel.frame = CGRectMake(0, 65*Scale, 180*Scale, 70*Scale);
    self.moneyLabel.hidden = YES;
    self.moneyLabel.textAlignment = UITextAlignmentRight;
//    [self.moneyLabel sizeToFit];
    self.moneyLabel.font = BOLD_FONT(100*Scale);
    [self addSubview:self.moneyLabel];

    
    //发红包按钮
    UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.frame = CGRectMake((frame.size.width-140*Scale)/2, 97*Scale, 124*Scale, 124*Scale);
    sendBtn.centerY = frame.size.height/2;
    sendBtn.centerX = frame.size.width/2;
    [sendBtn setBackgroundImage:[UIImage imageNamed:@"btn_open_redpack"] forState:UIControlStateNormal];
    [sendBtn addTarget:self action:@selector(moveAnimation:) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.layer.zPosition = 3;
    [self addSubview:sendBtn];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = sendBtn.frame;
    [shareButton setBackgroundImage:[UIImage imageNamed:@"icon_share_redpack"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(clickShare) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:shareButton];
    _shareButton = shareButton;
    _shareButton.hidden = YES;
    
    // 今天是否已经获取红包或者红包已入账
//    if ([[UserManager sharedInstance] isLogin] && ([self.redpackDAO isExistTodayRedpack:0] || [self.redpackDAO isExistTodayRedpack:1])) {
    // 该账号是否领取过红包，红包只能领取一次
    if ([[UserManager sharedInstance] isLogin] && ([self.redpackDAO isExistRedpack:1] || [self.redpackDAO isExistRedpack:0] || [self.redpackDAO isExistRedpack:2])) {
    
        NSString *amount = [self.redpackDAO getRedpackMoney];
        
        AppDelegate *appDelegate = APP_DELEGATE;
        appDelegate.redpackMoney = amount;

        // 显示为红包已打开状态，显示分享按钮
        self.backgroundImageView.hidden = YES;
        self.shareBackgroundImageView.hidden = NO;
//        AppDelegate *appDelegate = APP_DELEGATE;
        self.moneyLabel.text = appDelegate.redpackMoney;
        
        self.moneyLabel.hidden = NO;
        sendBtn.hidden = YES;
        self.shareButton.hidden = NO;
        
    }
    
}

-(void)moveAnimation:(UIButton*)sender
{
    if (![UserManager sharedInstance].isLogin) {
        BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"请先登录" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"登录"];
        [alertView setConfirmBlock:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kNeedLoginNotification object:nil];
        }];
        [alertView show];
        return;
    }
    
#if 0
    
    if ([self.redpackDAO isExistTodayRedpack:1]) {
        NSString *msg = @"您今天已经领取过红包，明天再来吧";
        [MsgToolBox showToast:msg];
        
//#if DEBUG
////        [self clickShare];
//        self.shareButton.hidden = NO;
//#endif
        return;
    }
    
#endif
    
    // 已经领取过红包
    if ([self.redpackDAO isExistRedpack:1]) {
        
//        [MsgToolBox showToast:@"您已经领取过红包"];
        
        [self showShareAlertView:YES];
        
        return;
    }
    
    // 不是新注册用户
    if ([self.redpackDAO isExistRedpack:2]) {
        
        [self showShareAlertView:NO];
        
        return;
        
    }
    
    // 开始领红包
    
    CABasicAnimation *transformAnima = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    //    transformAnima.fromValue = @(M_PI_2);
    transformAnima.toValue = [NSNumber numberWithFloat: M_PI];
    transformAnima.duration = 0.5;
    transformAnima.cumulative = YES;
    transformAnima.autoreverses = NO;
    transformAnima.repeatCount = HUGE_VALF;
    transformAnima.fillMode = kCAFillModeForwards;
    transformAnima.removedOnCompletion = NO;
    transformAnima.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    sender.layer.zPosition = 5;
    sender.layer.zPosition = sender.layer.frame.size.width/2.f;
    [sender.layer addAnimation:transformAnima forKey:@"rotationAnimationY"];

    
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(GET_RED_PACK_API) requestMethod:POST params:@"" success:^(id responseData) {
        
        StrongSelf
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSNumber *amount = responseData[@"data"];
            
            [strongSelf.redpackDAO insertRedpack:[amount stringValue] flag:0];
            
            // 保存红包全局变量
            AppDelegate *appDelegate = APP_DELEGATE;
            appDelegate.redpackMoney = [amount stringValue];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [sender.layer removeAnimationForKey:@"rotationAnimationY"];
                
                strongSelf.backgroundImageView.hidden = YES;
                
                strongSelf.shareBackgroundImageView.hidden = NO;
                
                [sender removeFromSuperview];
                
                strongSelf.shareButton.hidden = NO;
                
                strongSelf.moneyLabel.hidden = NO;
                
                strongSelf.moneyLabel.text = [amount stringValue];
                
            });
            
        }
        
        else {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [sender.layer removeAnimationForKey:@"rotationAnimationY"];
                
                // 已领取过红包 缓存当前账号红包
                if (code_status == 1906) {
                    
                    [weakSelf.redpackDAO insertRedpack:@"30" flag:1];
                    
                    // 保存红包全局变量
                    AppDelegate *appDelegate = APP_DELEGATE;
                    appDelegate.redpackMoney = @"30";
                    
                    [weakSelf showShareAlertView:YES];
                    
                }
                else if (code_status == 1910) {// 不是新注册用户
                    
                    [weakSelf.redpackDAO insertRedpack:@"30" flag:2];
                    
                    // 保存红包全局变量
                    AppDelegate *appDelegate = APP_DELEGATE;
                    appDelegate.redpackMoney = @"30";
                    
                    [weakSelf showShareAlertView:NO];
                    
                }
                else {
                    [MsgToolBox showToast:getErrorMsg(code_status)];
                }
                
            });
            
            
            
            
            
        }
        
        
        
    } andFailure:^(NSString *errorDesc) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [sender.layer removeAnimationForKey:@"rotationAnimationY"];
            
            [MsgToolBox showToast:errorDesc];
            
        });
        

        
    }];
    

    
}

-(void)showShareAlertView:(BOOL)isNewUser
{
    NSString *message;
    if (isNewUser) {
        message = @"您已领取过红包，您还可以继续分享";
    }
    else {
//        message = @"不是新注册用户，不能领取红包";
        message = @"不是新注册用户，不能领取红包，您还可以继续分享此次红包活动";
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"继续分享", nil];
    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
        
        if (buttonIndex == 1) {
            [self shareToWechatTimeline];
        }
        
    }];
}

#pragma mark - 点击分享按钮
-(void)clickShare
{
    
//    if ([self.redpackDAO isExistTodayRedpack:1])
//    {
//        [MsgToolBox showToast:@"红包已到账"];
//        return;
//    }
    
    // 领取过红包再次分享
    if ([self.redpackDAO isExistRedpack:1]) {
        
        [self showShareAlertView:YES];
        
        return;
    }
    
    if ([self.redpackDAO isExistRedpack:2]) {
        
        [self showShareAlertView:NO];
        
        return;
        
    }
    
    [self shareToWechatTimeline];
    

    
}

-(void)shareToWechatTimeline
{
    NSInteger platformType = UMSocialPlatformType_WechatTimeLine;
#if DEBUG
    platformType = UMSocialPlatformType_WechatSession;
#endif
    
    //    UIImage *icon = [UIImage imageNamed:@"AppIcon72x72"];
    //    NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
    //    NSString *icon = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
    //    UIImage *shareImage = [UIImage imageNamed:icon];
    
//    AppDelegate *appDelegate = APP_DELEGATE;
    
    UIImage *shareImage = [UIImage imageNamed:@"icon_share_red_envelop"];
    
//    NSString *title = [NSString stringWithFormat:@"有人@你，他在爱佣领取了%@元现金红包，邀您来领取！", appDelegate.redpackMoney];
    NSString *title = @"有人@你，他在爱佣领取了免费用车红包，邀您来领取！";

    NSString *description = @"爱佣出行5000万现金红包免费送，赶紧下载注册领取";
    
    WeakSelf
    [ShareUtil shareWebPageToPlatformType:platformType title:title desc:description icon:shareImage webpageUrl:self.shareUrl shareSuccess:^(id data) {
        
        // 分享成功
        
        DLog(@"data = %@", data);
        
        // 红包已领取未分享
        if ([weakSelf.redpackDAO isExistRedpack:0]) {
            
            [weakSelf handleGetRedpackSuccess];
            
        }
        
    } shareFailure:^(id data, NSError *error) {
        
        [MsgToolBox showToast:@"分享失败，请重试"];
        
    }];
    //
}

-(void)handleGetRedpackSuccess
{
    AppDelegate *appDelegate = APP_DELEGATE;
    NSString *redpackMoney = appDelegate.redpackMoney;
    NSString *params = [NSString stringWithFormat:@"amount=%@", appDelegate.redpackMoney];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(SAVE_RED_PACK_API) requestMethod:POST params:params success:^(id responseData) {
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            [weakSelf.redpackDAO updateRedpackStatus:1];
            
            NSString *message = [NSString stringWithFormat:@"%@元红包已放入您的账户",redpackMoney];
            
            [MsgToolBox showToast:message];
            
        }
        else {
            
            [MsgToolBox showToast:getErrorMsg(code_status)];
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        [MsgToolBox showToast:errorDesc];
    
    }];
    
}




@end
