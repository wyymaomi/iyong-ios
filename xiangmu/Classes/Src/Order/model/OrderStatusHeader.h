//
//  OrderStatusHeader.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  用来指示是否展开收缩，以及标题、图标
 */
@interface OrderStatusHeader : NSObject

@property (nonatomic, assign) NSInteger cell_height;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) BOOL canOpen;// 是否可以打开
@property (nonatomic, assign) BOOL isOpen;// 收缩/打开状态

-(id)initWithDict:(NSDictionary*)dict;

@end
