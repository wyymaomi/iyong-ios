//
//  ProviderOrderListViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "ProviderInfoViewModel.h"

@interface ProviderOrderListViewController : BaseViewController

@property (nonatomic, strong) ProviderInfoViewModel *viewModel;

@end
