//
//  RechargeAmountTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RechargeAmountTableViewCellDelegate <NSObject>

-(void)onSelectItem:(NSInteger)tag;

@end

@interface RechargeAmountTableViewCell : UITableViewCell

@property (nonatomic, weak) id<RechargeAmountTableViewCellDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *amountList;

@property (nonatomic, strong) UIButton *lastButton;

@property (nonatomic, strong) UIButton *rechargeButton;

+(CGFloat)getCellHeight;

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
