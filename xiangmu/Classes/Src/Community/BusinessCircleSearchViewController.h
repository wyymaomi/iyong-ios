//
//  BusinessCircleSearchViewController.h
//  xiangmu
//
//  Created by David kim on 16/9/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@class BussinessSearchHttpMessage;

@protocol BusinessCircleSearchDelegate <NSObject>

- (void)onSearchResult:(BussinessSearchHttpMessage*)httpMessage;

@end


@interface BusinessCircleSearchViewController : BaseViewController

@property (nonatomic, weak) id<BusinessCircleSearchDelegate> searchDelegate;


@end
