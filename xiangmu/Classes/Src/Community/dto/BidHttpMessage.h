//
//  BidHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface BidHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *enquiryId;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *remark;

-(NSString*)validateData;

@end
