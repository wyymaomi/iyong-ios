//
//  InvoiceHistoryListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/1.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceHistoryListViewController.h"
#import "InvoiceHistoryTableViewCell.h"
#import "InvoiceInfoViewController.h"

@interface InvoiceHistoryListViewController ()

@end

@implementation InvoiceHistoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"开票历史";
    
//    [self setRightNavigationItemWithTitle:@"添加" selMethod:@selector(addInvoiceSetting)];
    
    [self.view addSubview:self.pullRefreshTableView];
    
    [self.pullRefreshTableView beginHeaderRefresh];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [InvoiceHistoryTableViewCell getCellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    InvoiceSettingItemCellTableViewCell *cell = [InvoiceSettingItemCellTableViewCell cellWithTableView:tableView];
//    
//    InvoiceSettingHttpMessage *invoiceSettingInfo = self.dataList[indexPath.row];
//    
//    cell.invoiceTItleLabel.text = invoiceSettingInfo.header;
//    
//    cell.contactAddressLabel.text = [NSString stringWithFormat:@"%@%@", invoiceSettingInfo.areaName, invoiceSettingInfo.address];
//    
//    return cell;
    
    InvoiceHistoryTableViewCell *cell = [InvoiceHistoryTableViewCell cellWithTableView:tableView];
    
    
    InvoiceinfoHttpMessage  *invoiceData = self.dataList[indexPath.row];
    
    cell.invoiceTitleLabel.text = invoiceData.header;
    cell.totalMoneyLabel.text = [NSString stringWithFormat:@"金额：%@元", invoiceData.totalAmount];
    cell.quantityLabel.text = [NSString stringWithFormat:@"数量：%@", invoiceData.totalQuantity];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[invoiceData.createTime doubleValue]/1000];
    cell.dateTimeLabel.text = [date stringWithDateFormat:@"MM月dd日"];

    return cell;
    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    InvoiceinfoHttpMessage  *invoiceData = self.dataList[indexPath.row];
    
    InvoiceInfoViewController *viewController = [[InvoiceInfoViewController alloc] init];
    
    viewController.mode = InvoiceInfoModeViewDetail;
    
    viewController.invoiceInfoHttpMessage = invoiceData;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
    
//    InvoiceSettingHttpMessage *settingHttpMessage = self.dataList[indexPath.row];
//    
//    if (self.sourceFrom == InvoiceListSourceFromMyInvoice) {
//        
//        InvoiceSettingEditViewController *viewController = [InvoiceSettingEditViewController new];
//        viewController.invoiceSettingHttpMessage = settingHttpMessage;
//        viewController.editMode = InvoiceEditModeUpdate;
//        [self.navigationController pushViewController:viewController animated:YES];
//    }
//    else if (self.sourceFrom == InvoiceListSourceFromEditInfo) {
//        
//        if (_completeHandle) {
//            //            _completeHandle(self.tag, self.textField.text.trim);
//            _completeHandle(settingHttpMessage);
//            
//            [self pressedBackButton];
//        }
//        
//        
//    }
    
    //    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
    //        [self.customDelegate didSelectRowAtIndexPath:indexPath];
    //    }
    
    
}

#pragma mark -

#pragma mark - 获取发票设置列表

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    
    if ([self needLogin]) {
        return;
    }
    
    [self fetchInvoiceList:nil];
    
    //    [self fetchOrderList:self.listStatus time:nil];
    
}

-(void)onFooterRefresh
{
    self.nextAction = @selector(onFooterRefresh);
    
    if ([self needLogin]) {
        return;
    }
    
    InvoiceinfoHttpMessage *data = [self.dataList lastObject];
    
    [self fetchInvoiceList:[data.createTime stringValue]];
    
    //    TravelOrderModel *model = [self.dataList lastObject];
    //    [self fetchOrderList:self.listStatus time:[model.createTime stringValue]];
    
    //    GrabOrderModel *model = [self.dataList lastObject];
    //    [self fetchCanreceiveOrderList:[model.createTime stringValue]];
}


-(void)fetchInvoiceList:(NSString*)time
{
    if (IsStrEmpty(time)) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    NSString *params = [NSString stringWithFormat:@"time=%@",  [StringUtil getSafeString:time]];
    
    self.loadStatus = LoadStatusInit;
    
//    NSString *time
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(GET_INVOICE_LIST_API) requestMethod:POST params:params success:^(NSDictionary * responseData) {
        
        [weakSelf.pullRefreshTableView endHeaderRefresh];
        [weakSelf.pullRefreshTableView endFooterRefresh];
        [weakSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *dataList = responseData[@"data"];
            for (NSDictionary *dictionary in dataList) {
                
                InvoiceinfoHttpMessage *invoiceInfoHttpMessage = [[InvoiceinfoHttpMessage alloc] init];
                
                //                id 发票设置ID
                //                userId 当前用户ID
                //                header 发票抬头
                //                taxpayerNumber  纳税人识别号
                //                registerAddress 注册地址
                //                openingBank 开户银行
                //                bankAccount 银行帐号
                //                contact 联系人
                //                tel 联系电话
                //                areaCode 地区编码
                //                areaName 所在区域
                //                address 详细地址
                //                remark 客户备注
                
//                invoiceInfoHttpMessage.id = dictionary[@"id"];
                invoiceInfoHttpMessage.header = dictionary[@"header"];
                invoiceInfoHttpMessage.taxpayerNumber = dictionary[@"taxpayerNumber"];
                invoiceInfoHttpMessage.registerAddress = dictionary[@"registerAddress"];
                invoiceInfoHttpMessage.openingBank = dictionary[@"openingBank"];
                invoiceInfoHttpMessage.bankAccount = dictionary[@"bankAccount"];
                invoiceInfoHttpMessage.contact = dictionary[@"contact"];
                invoiceInfoHttpMessage.tel = dictionary[@"tel"];
                invoiceInfoHttpMessage.areaCode = dictionary[@"areaCode"];
                invoiceInfoHttpMessage.areaName = dictionary[@"areaName"];
                invoiceInfoHttpMessage.address = dictionary[@"address"];
                invoiceInfoHttpMessage.remark = dictionary[@"remark"];
                invoiceInfoHttpMessage.createTime = dictionary[@"createTime"];
                invoiceInfoHttpMessage.totalQuantity = dictionary[@"totalQuantity"];
                invoiceInfoHttpMessage.totalAmount = dictionary[@"totalAmount"];
                
                [weakSelf.dataList addObject:invoiceInfoHttpMessage];
                
                //                GrabOrderModel *model = [[GrabOrderModel alloc] initWithDictionary:dictionary error:nil];
                //                [weakSelf.dataList addObject:model];
                
                //                TravelOrderModel *model = [[TravelOrderModel alloc] initWithDictionary:dictionary error:nil];
                //                [weakSelf.dataList addObject:model];
                
            }
            
            weakSelf.loadStatus = LoadStatusFinish;
            
            [weakSelf.pullRefreshTableView reloadData];
            
            
            
            
        }
        else {
            
            weakSelf.loadStatus = LoadStatusFinish;
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf resetAction];
        weakSelf.loadStatus = LoadStatusNetworkError;
        
        
    }];
}





@end
