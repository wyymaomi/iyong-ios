//
//  TabbarManager.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "TabbarManager.h"
#import "NewMainTabBarController.h"

@implementation TabbarManager

//-(id<TabbarDelegate>)createTabBarController
-(CustomTabBarViewController*)createTabBarController
{
    return [[NewMainTabBarController alloc] init];
}

@end
