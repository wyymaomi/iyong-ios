//
//  PullRefreshCollectionView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PullRefreshCollectionViewDelegate <NSObject>

- (void)onHeaderRefresh;
- (void)onFooterRefresh;

@end

@interface PullRefreshCollectionView : UICollectionView

@property (nonatomic, assign) BOOL hasHeaderRefresh;
@property (nonatomic, assign) BOOL hasFooterRefresh;
@property (nonatomic, assign) BOOL footerHidden;

@property (nonatomic, weak) id<PullRefreshCollectionViewDelegate> pullRefreshDelegate;

-(void)beginHeaderRefresh;
-(void)endHeaderRefresh;

-(void)beginFooterRefresh;
-(void)endFooterRefresh;

- (void)setupHeaderView;
- (void)setupFooterView;

@end
