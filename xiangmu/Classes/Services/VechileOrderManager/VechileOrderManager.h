//
//  VechileOrderManager.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/28.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VechileOrderHttpMessage.h"
#import "CarDescModel.h"

@interface VechileOrderManager : NSObject<UIAlertViewDelegate>

@property (nonatomic, strong) VechileOrderHttpMessage *vechileOrderHttpMessage;

+ (instancetype)sharedInstance;

- (void)contactDriver:(NSString*)mobile;

- (BOOL)isPendingPayOvertime;

-(void)cancelOrder:(NSString*)orderId carOrderMode:(NSUInteger)carOrderMode viewController:(UIViewController*)viewController isCompletePay:(BOOL)isCompletePay;

//- (void)cancelOrder:(NSString*)orderId viewController:(UIViewController*)viewController isCompletePay:(BOOL)isCompletePay;

-(NSArray<CarDescModel*>*)getVechileDescTypeList;

@end
