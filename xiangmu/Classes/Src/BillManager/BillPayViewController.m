//
//  BillPayViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BillPayViewController.h"
#import "BillPayTableViewCell.h"
//#import "BillRepayHeaderTableViewCell.h"
#import "BillModel.h"
#import "BillRepayHeaderView.h"
#import "BillRepayFooterView.h"
#import "OrderManagerViewController.h"

@interface BillPayViewController ()<PullRefreshTableViewDelegate, UITableViewDelegate, UITableViewDataSource, BillPayTableViewCellDelegate,EmptyDataDelegate>

//@property (nonatomic, assign) NSInteger totalRepayMoney;// 总共需要还款的金额
@property (nonatomic, assign) NSInteger totalMoney;
@property (nonatomic, strong) NSMutableArray *idList;
//@property (nonatomic, strong) PullRefreshTableView *billRepayTableView;

@end

@implementation BillPayViewController

-(NSMutableArray*)idList
{
    if (_idList == nil) {
        _idList = [NSMutableArray new];
    }
    return _idList;
}


#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"信用还款";
    self.emptyDataDelegate = self;
    
    BillRepayHeaderView *headerView = [BillRepayHeaderView customView];
    headerView.frame = CGRectMake(0, 0, ViewWidth, 50);
    [self.view addSubview:headerView];
    self.totalRepayLabel = headerView.totalMoneyLabel;
    
    [self.view addSubview:self.pullRefreshTableView];
    self.pullRefreshTableView.frame = CGRectMake(0, 55, ViewWidth, ViewHeight-55-55);
    self.pullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    BillRepayFooterView *footerView = [BillRepayFooterView customView];
    footerView.frame = CGRectMake(0, ViewHeight-110, ViewWidth, 44);
    [self.view addSubview:footerView];
    self.selectAllButton = footerView.checkButton;
    self.totalLabel = footerView.amountMoneyLabel;
    self.payButton = footerView.payButton;
    self.payButton.enabled = NO;
    [self.payButton addTarget:self action:@selector(onPay:) forControlEvents:UIControlEventTouchUpInside];
    [self.selectAllButton addTarget:self action:@selector(checkAll:) forControlEvents:UIControlEventTouchUpInside];
    
    self.totalLabel.text = @"";
    self.totalRepayLabel.text = @"总还款金额";
    [self.payButton darkRedStyle];
    self.payButton.enabled = NO;
    
    [self registKVO];
}

-(void)dealloc
{
    [self unRegistKVO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.pullRefreshTableView beginHeaderRefresh];
}

-(void)initData
{
    self.totalMoney = 0;
    [self.idList removeAllObjects];
    self.selectAllButton.selected = NO;
}

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    [self refreshData:nil isHeaderRefresh:YES];
    
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        [self.pullRefreshTableView endFooterRefresh];
        return;
    }
    
    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    BillRepayModel *model = [self.dataList lastObject];
    [self refreshData:model.repaymentTime isHeaderRefresh:NO];
    
}

-(void)refreshData:(NSString*)createTime isHeaderRefresh:(BOOL)isHeaderRefresh
{
    
    if (isHeaderRefresh) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    [self initData];
    self.loadStatus = LoadStatusLoading;
    
    WeakSelf
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:createTime]];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CREDIT_UNREPAYMENT_LIST_API];
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseObject) {
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        strongSelf.pullRefreshTableView.footerHidden = NO;
        strongSelf.loadStatus = LoadStatusFinish;
        
        BillRepayResponseData *responseData = [[BillRepayResponseData alloc] initWithDictionary:responseObject error:nil];
        if ([responseData.code integerValue] == STATUS_OK) {
            
            NSString *totalMoneyText = [NSString stringWithFormat:@"¥%@", [responseData.data.totalAmount stringValue]];
            strongSelf.totalRepayLabel.text = [NSString stringWithFormat:@"总还款金额 %@", totalMoneyText];
            [strongSelf.totalRepayLabel setTextColor:UIColorFromRGB(0xD51618) text:totalMoneyText];
            
            [strongSelf.dataList addObjectsFromArray:responseData.data.bills];
            [strongSelf.pullRefreshTableView reloadData];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[responseData.code integerValue]]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        strongSelf.loadStatus = LoadStatusNetworkError;
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        
//        [strongSelf showAlertView:errorDesc];
        
    }];
}

- (IBAction)checkAll:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    
    for (NSInteger i = 0; i < self.dataList.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        BillPayTableViewCell *cell = [self.pullRefreshTableView cellForRowAtIndexPath:indexPath];
        cell.checkButton.selected = btn.selected;
        BillRepayModel *repayModel = self.dataList[i];
        repayModel.checked = [NSNumber numberWithBool:btn.isSelected];
        if (btn.isSelected) {
            [self.idList addObject:repayModel.id];
        }
        else {
            [self.idList removeObject:repayModel.id];
        }
    }
    
    [self calculateTotoal];
    
}

-(void)onCheckButton:(NSUInteger)tag checked:(BOOL)checked;
{
    BillRepayModel *repayModel = self.dataList[tag];
    repayModel.checked = [NSNumber numberWithBool:checked];
    if (checked) {
        [self.idList addObject:repayModel.id];
    }
    else {
       [self.idList removeObject:repayModel.id];
    }
    [self calculateTotoal];
    if (self.idList.count == self.dataList.count) {
        self.selectAllButton.selected = YES;
    }
    else {
        self.selectAllButton.selected = NO;
    }
}

-(void)calculateTotoal
{

    self.totalMoney = 0;
    for (BillRepayModel *repayModel in self.dataList) {
        if ([repayModel.checked boolValue]) {
            self.totalMoney += [repayModel.price integerValue];
        }
    }
    
    
}

- (IBAction)onPay:(id)sender {
    
//    UIButton *button = (UIButton*)sender;
    
    if (self.totalMoney > 0 && self.idList.count > 0) {
        [self doRepay];
    }
    
}

-(void)onSelectRowAtIndex:(NSUInteger)rowIndex
{
    [self gotoOrderDetailPage:rowIndex];
}

-(void)gotoOrderDetailPage:(NSInteger)index
{
    BillRepayModel *model = self.dataList[index];
    NSString *params = [NSString stringWithFormat:@"orderId=%@", model.orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_DETAIL_API];
    
    [self showHUDIndicatorViewAtCenter:MSG_ORDER_DETAIL];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        id code = responseData[@"code"];
        if (IsNilOrNull(code)) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *array = responseData[@"data"];
            
            if (!IsArrEmpty(array)) {
                
                NSInteger orderStatus = 0;
                OrderManagerViewController *vc = [[OrderManagerViewController alloc] init];
                
                for (NSDictionary *dict in array) {
                    
                    NSUInteger status = [dict[@"status"] integerValue];
                    NSDictionary *vo = dict[@"vo"];
                    if (status == 1) {
                        vc.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusNew;
                    }
                    if (status == 2) {
                        vc.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusDispatch;
                    }
                    if (status == 3) {
                        vc.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusVechileArrange;
                    }
                    if (status == 4) {
                        vc.wayBillModel = [[WayBillResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusWayBill;
                    }
                    if (status == 5) {
                        vc.barginAndPayModel = [[BarginAndPayResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusBarginAndPay;
                    }
                }
                
                vc.source_from = SOURCE_FROM_ORDER_LIST;
                vc.orderStatus = orderStatus;
                vc.orderMode = OrderModeDetail;
                vc.orderId = model.orderId;
                vc.title = @"订单详情";
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

#pragma makr - 还款
-(void)doRepay
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CREDIT_PAY_API];
    NSString *params = [NSString stringWithFormat:@"ids=%@&price=%ld", [self.idList componentsJoinedByString:@","], (long)self.totalMoney];
    
    [self showHUDIndicatorViewAtCenter:@"正在还款，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"还款成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [strongSelf.pullRefreshTableView beginHeaderRefresh];
                }
                
            }];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
}

#pragma mark -
#pragma mark KVO

- (void)registKVO
{
//    [super registKVO];
    [self addObserver:self
           forKeyPath:@"totalMoney"
              options:NSKeyValueObservingOptionNew
              context:NULL];
}

- (void)unRegistKVO
{
    [self removeObserver:self
              forKeyPath:@"totalMoney"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"totalMoney"])
    {
        if (_totalMoney == 0) {
            self.payButton.enabled = NO;
            self.totalLabel.text = @"";
        }
        else {
            self.payButton.enabled = YES;
            self.totalLabel.text = [NSString stringWithFormat:@"合计：¥%ld", (long)self.totalMoney];
            [self.totalLabel setTextColor:UIColorFromRGB(0xD51618) text:[NSString stringWithFormat:@"¥%ld", (long)self.totalMoney]];
        }
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == 0) {
//        return 60;
//    }
//    else {
        if (indexPath.row < self.dataList.count -1) {
            return [BillPayTableViewCell getCellHeight];
        }
        return [BillPayTableViewCell getCellHeight]-10;
//    }
//    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BillPayTableViewCell *cell = [BillPayTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.delegate = self;
    BillRepayModel *data = self.dataList[indexPath.row];
    [cell initData:data];
    return cell;
    
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

#pragma mark - EmptyData Delegate method

- (NSString*)emptyDetailText;
{
    return @"暂时没有需要还款的账单";
}


@end
