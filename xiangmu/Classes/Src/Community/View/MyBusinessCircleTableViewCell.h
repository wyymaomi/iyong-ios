//
//  MyBusinessCircleTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BusinessCircleTableViewCell.h"

@class BidModel;

@protocol MyBusinessCircleTableViewCellDelegate <NSObject>

- (void)onGenerateOrder:(BidModel*)bidModel enquireModel:(EnquireModel*)enquireModel;
- (void)onAddPartner:(NSString*)partnerId;
- (void)onMobile:(NSString*)mobile;

@end

@interface MyBusinessCircleTableViewCell : BusinessCircleTableViewCell

@property (nonatomic, strong) UIView *bidsView;
@property (nonatomic, strong) UILabel *companyLabel;

@property (nonatomic, weak) id<MyBusinessCircleTableViewCellDelegate> delegate;

@end
