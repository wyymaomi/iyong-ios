//
//  WayBillModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@interface WayBillResponseModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *distance;
@property (nonatomic, strong) NSString<Optional> *endTime;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *location;
@property (nonatomic, strong) NSString<Optional> *orderId;
@property (nonatomic, strong) NSString<Optional> *remark;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *editEndTime;
@property (nonatomic, strong) NSString<Optional> *editLocation;
@property (nonatomic, strong) NSString<Optional> *editDistance;
@property (nonatomic, strong) NSString<Optional> *editRemark;

@end
