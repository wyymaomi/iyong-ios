//
//  InputPaypwdView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "InputPaypwdView.h"

@implementation InputPaypwdView

-(PayInputView*)payInputView
{
    if (!_payInputView) {
        _payInputView = [[PayInputView alloc] initWithFrame:CGRectMake(15, self.rateMoneyLabel.bottom + 60, self.frame.size.width - 30, 45)];
        _payInputView.backgroundColor = [UIColor whiteColor];
        _payInputView.borderWidth = 1;
        _payInputView.layer.borderColor =  UIColorFromRGB(0xC9C9C9).CGColor;
        _payInputView.pwdCount = 6;
    }
    return _payInputView;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self addSubview:self.payInputView];
}

@end
