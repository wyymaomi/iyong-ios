//
//  CommunityPublishModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol CommunityPublishModel <NSObject>
@end

@interface CommunityPublishModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *carDriver;
@property (nonatomic, strong) NSString<Optional> *carId;
@property (nonatomic, strong) NSString<Optional> *carImgUrl;
@property (nonatomic, strong) NSString<Optional> *carMobile;
@property (nonatomic, strong) NSString<Optional> *carNumber;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *companyName;
@property (nonatomic, strong) NSString<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *endTime;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *nickname;
@property (nonatomic, strong) NSString<Optional> *seat;
@property (nonatomic, strong) NSString<Optional> *startTime;
@property (nonatomic, strong) NSString<Optional> *time;
@property (nonatomic, strong) NSString<Optional> *logoUrl;

@end


@interface CommunityResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<CommunityPublishModel,Optional> *data;

@end
