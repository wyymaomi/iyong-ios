//
//  CommunityCarTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityCarTableViewCell.h"

@implementation CommunityCarTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"CommunityCarTableViewCell";
    CommunityCarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.driverMobileButton.lineColor = UIColorFromRGB(0x00A04E);
        
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
