//
//  RegisterView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface RegisterView : UIView

@property (weak, nonatomic) IBOutlet UIView *recommendCodeView;
@property (weak, nonatomic) IBOutlet UIImageView *recommendCodeIconImageView;
@property (weak, nonatomic) IBOutlet UITextField *recommendCodeTextField;
@property (weak, nonatomic) IBOutlet UIView *recommendCodeLineView;

@property (weak, nonatomic) IBOutlet UILabel *logoTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *verifyCodeButton;
@property (weak, nonatomic) IBOutlet UIView *verifyCodeLineView;
@property (weak, nonatomic) IBOutlet UITextField *verifyCodeTextField;
@property (weak, nonatomic) IBOutlet UIImageView *verifyCodeIconImageView;
@property (weak, nonatomic) IBOutlet UIView *verifyCodeView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *tpkeyboardAvoidingScrollView;
@property (weak, nonatomic) IBOutlet UILabel *registerTextLabel;
@property (weak, nonatomic) IBOutlet UIView *accountView;
@property (weak, nonatomic) IBOutlet UIImageView *accountImageView;
@property (weak, nonatomic) IBOutlet UITextField *accountTextField;
@property (weak, nonatomic) IBOutlet UIView *accountLineView;
@property (weak, nonatomic) IBOutlet UIView *pwdView;
@property (weak, nonatomic) IBOutlet UIImageView *pwdIconImageView;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UIView *pwdLineView;
//@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (nonatomic, strong) UIView *registerProtocolView;
@property (nonatomic, strong) UIButton *registerProtocolButton;

@end
