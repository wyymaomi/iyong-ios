//
//  BindBnkcardViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface BindBankcardViewController : BaseViewController

@property (nonatomic, strong) NSString *bankcardName;

@property (weak, nonatomic) IBOutlet UITextField *bankcardNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *bankcardNoTextField;
@property (weak, nonatomic) IBOutlet UIButton *okButton;


@end
