//
//  LoginViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/7.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController (NetworkRequest)

- (void)getAuthWithUserInfoFromQQ;

- (void)getAuthWithUserInfoFromSina;

- (void)getAuthWithUserInfoFromWechat;

-(void)doLogin;

@end
