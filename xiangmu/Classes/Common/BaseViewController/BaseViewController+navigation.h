//
//  BaseViewController+navigation.h
//  xiangmu
//
//  Created by David kim on 16/9/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface UIViewController (navigation)

- (void)gotoPage:(NSString*)vcName;
- (void)gotoPage:(NSString*)vcName animated:(BOOL)animated;
- (void)gotoPage:(NSString*)vcName animated:(BOOL)animated title:(NSString*)title;

- (void)presentPage:(NSString*)vcName animated:(BOOL)animated;

-(void)openWebPage:(NSString*)url;

@end
