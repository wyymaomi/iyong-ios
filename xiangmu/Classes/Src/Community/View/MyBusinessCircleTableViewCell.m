//
//  MyBusinessCircleTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyBusinessCircleTableViewCell.h"
#import "DateUtil.h"
#import "InquireView.h"
#import "CertifiedView.h"
#import "StarView.h"

@implementation MyBusinessCircleTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xEBEBEB);
        
        [self.contentView addSubview:self.companyLabel];
        [self.contentView addSubview:self.bidsView];
        
        self.userInfoView.hidden = YES;
        self.companyLabel.hidden = NO;
        self.countDownView.hidden = YES;
        self.publishPriceButton.hidden = YES;
        self.roughItineraryView.hidden = YES;
        
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    self.bidsView.left = 0;
    self.bidsView.top = self.elapseTimeLabel.bottom + 10;
    self.bidsView.width = ViewWidth;
    self.bidsView.height = 215 * self.enquireModel.bids.count + 10;
    
    NSInteger top = 0;
    NSInteger separator = 10;
    for (NSInteger i = 0; i < self.enquireModel.bids.count; i++) {
        InquireView *inquireView = [self.bidsView subviews][i];
        inquireView.frame = CGRectMake(20, top + (215 + separator) * i, ViewWidth-40, 215);
    }
    
}

-(void)adjustBaseInfoPosition
{
//    self.countDownView.top = 15;
//    self.userInfoView.top = self.countDownView.bottom+2;
    self.startTimeView.top = self.companyLabel.bottom+2;
    self.statusButton.bottom = self.startTimeView.bottom;
    self.endTimeView.top = self.startTimeView.bottom;
    self.startLocationView.top = self.endTimeView.bottom;
    self.endLocationView.top = self.startLocationView.bottom;
    self.vechileModelView.top = self.endLocationView.bottom;
    self.vechileNumberView.top = self.vechileModelView.bottom;
    
    self.roughItineraryView.top = self.vechileNumberView.bottom;
    self.thirdPayLabel.top = self.roughItineraryView.bottom;
    self.fullTextButton.top = self.vechileNumberView.bottom;
    self.elapseTimeLabel.top = self.vechileModelView.top + 3 * kLineHeight;
    self.publishPriceButton.bottom = self.elapseTimeLabel.bottom-5;
}

+(CGFloat)getCellHeight:(EnquireModel*)data;
{
    
    if (data == nil) {
        return 0;
    }
    
    CGFloat height = 220;
    
    if (data.bids.count > 0) {
        
        height += 225 * data.bids.count + 10;
    
    }
    
    
    if (![data.isOpen boolValue]) {
        
        return height;
        
    }
    
    
//    if (IsStrEmpty(data.itinerary)) {
//        
//        height  += height + 30;
//        
//        return height;
//        
//    }
    
    CGSize labelSize = [data.itinerary textSize:CGSizeMake(ViewWidth-kTextLabelLeft-kLineRightInset, MAXFLOAT) font:FONT(13)];
    height = height + labelSize.height + 10 + kLineHeight;
    
    return height;
    
}

- (void)initData:(EnquireModel*)data;
{
    self.enquireModel = data;
    
    self.companyLabel.text = [UserManager sharedInstance].userModel.companyName;
    if (IsStrEmpty([UserManager sharedInstance].userModel.companyName)) {
        if (IsStrEmpty([UserManager sharedInstance].userModel.nickname)) {
            self.companyLabel.text = [UserManager sharedInstance].userModel.username;
        }
        else {
            self.companyLabel.text = [UserManager sharedInstance].userModel.nickname;
        }
    }

    self.startDateTimeLabel.text = [DateUtil formatDateTime:data.startTime dateFomatter:@"yyyy-MM-dd HH:mm"];
    self.endDateTimeLabel.text = [DateUtil formatDateTime:data.endTime dateFomatter:@"yyyy-MM-dd HH:mm"];
    self.startLocationLabel.text = data.startPlace.trim;
    self.endLocationLabel.text = data.endPlace.trim;
    self.vechileModelLabel.text = data.model.trim;
    self.vechileNumberLabel.text = data.quantity.trim;
    self.elapseTimeLabel.text = [DateUtil timerAgo:data.createTime];
    self.roughItineraryLabel.text = data.itinerary.trim;
    
    self.thirdPayLabel.text = @"不需要第三方包含垫付费用";
    if ([data.advance boolValue]) {
        self.thirdPayLabel.text = @"需要第三方包含垫付费用";
    }
    
    BOOL isEnd = [data checkIsEnd];
    if (isEnd) {
        [self.statusButton grayStyle];
        [self.statusButton setTitle:@"已结束" forState:UIControlStateNormal];
    }
    else {
        [self.statusButton yellowStyle];
        [self.statusButton setTitle:@"询价中..." forState:UIControlStateNormal];
    }
    
    
    [self.bidsView removeAllSubviews];
    [self.bidsView clipsToBounds];
    
    for (NSInteger i = 0; i < data.bids.count; i++) {
        
        BidModel *bid = data.bids[i];
        
        InquireView *inquireView = [[InquireView alloc] init];
        
        inquireView.backgroundColor = [UIColor clearColor];
        
        inquireView.companyLabel.text = bid.companyName;
        inquireView.remarkTextView.text = bid.remark;
        inquireView.priceLabel.text = [NSString stringWithFormat:@"¥%@", [bid.price stringValue]];
        inquireView.vechileQuantityLabel.text = [bid.quantity stringValue];
        inquireView.vechileModelLabel.text = bid.model;
        inquireView.nicknameLabel.text = bid.nickname;
        
        inquireView.generateOrderButton.tag = i;
        [inquireView.generateOrderButton addTarget:self action:@selector(doGenerateOrder:) forControlEvents:UIControlEventTouchUpInside];
        
        inquireView.addPartnerButton.tag = i;
        [inquireView.addPartnerButton addTarget:self action:@selector(onAddPartner:) forControlEvents:UIControlEventTouchUpInside];
        
        inquireView.telButton.tag = i;
        [inquireView.telButton addTarget:self action:@selector(onCallMobile:) forControlEvents:UIControlEventTouchUpInside];
        
        [inquireView.certifiedView
         initData:[bid.realNameCertification boolValue]
         isCompanyCertified:[bid.enterpriseCertification boolValue]
         isVechileCertified:[bid.carCertification boolValue]];
        
        inquireView.starView.width = kStarFrameWith * 5;
        [inquireView.starView setStarsImages:[bid.companyScor floatValue]];
        
        
        [self.bidsView addSubview:inquireView];
        
        
    }
    
    [self setNeedsLayout];
    
}

- (void)doGenerateOrder:(id)sender
{
    UIButton *button = (UIButton*)sender;

    BidModel *bid = self.enquireModel.bids[button.tag];
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onGenerateOrder:enquireModel:)]) {
        [self.delegate onGenerateOrder:bid enquireModel:self.enquireModel];
    }
}

- (void)onAddPartner:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    BidModel *bid = self.enquireModel.bids[button.tag];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onAddPartner:)]) {
        [self.delegate onAddPartner:bid.companyId];
    }

}

- (void)onCallMobile:(id)sender
{
    UIButton *button = (UIButton*)sender;
    BidModel *bid = self.enquireModel.bids[button.tag];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onMobile:)]) {
        [self.delegate onMobile:bid.mobile];
    }
}

-(UIView*)bidsView
{
    if (!_bidsView) {
        
        _bidsView = [UIView new];
        _bidsView.backgroundColor = [UIColor clearColor];
    
    }
    
    return _bidsView;
}

- (UILabel*)companyLabel
{
    if (!_companyLabel) {
        
        _companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLineLeftInset, 10, ViewWidth-kLineRightInset, kLineHeight)];
        _companyLabel.backgroundColor = [UIColor clearColor];
        _companyLabel.font = FONT(13);
        _companyLabel.textColor = UIColorFromRGB(0x808080);
        
    }
    return _companyLabel;
}

@end
