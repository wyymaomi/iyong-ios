//
//  TodayPromotionDetailTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayPromotionDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeSegmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *clickNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *thumbUpLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+(CGFloat)getCellHeight;

@end
