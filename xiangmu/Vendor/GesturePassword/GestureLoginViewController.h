//
//  GestureLoginViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseViewController.h"
#import "TentacleView.h"
#import "GesturePasswordView.h"

@protocol GesturePasswordControllerDelegate <NSObject>

-(void)onGestureLoginSuccess;

@end

@interface GestureLoginViewController : BaseViewController<VerificationDelegate,GesturePasswordDelegate>

//@property (nonatomic, weak) id<GesturePasswordControllerDelegate> loginDelegate;

@property (nonatomic, strong) UIViewController *previousViewController;// 要返回的界面
@property (nonatomic, assign) SEL previousMethod;// 要执行的方法

@end
