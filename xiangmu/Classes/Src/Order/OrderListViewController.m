//
//  OrderListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderListViewController.h"
#import "OrderItemTableViewCell.h"
#import "OrderManagerViewController.h"
#import "PullRefreshTableView.h"
#import "MJRefresh.h"

@interface OrderListViewController ()<UITableViewDelegate, UITableViewDataSource, PullRefreshTableViewDelegate>

//@property (strong, nonatomic) PullRefreshTableView *tableView;

@end

@implementation OrderListViewController

//-(PullRefreshTableView*)tableView
//{
//    if (_tableView == nil) {
//        _tableView = [[PullRefreshTableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
//        _tableView.delegate = self;
//        _tableView.dataSource = self;
//        _tableView.pullRefreshDelegate = self;
//        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        [self.view addSubview:_tableView];
//    }
//    return _tableView;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view addSubview:self.pullRefreshTableView];
    self.pullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.pullRefreshTableView beginHeaderRefresh];
}

#pragma mark - pull refresh delegate method

- (void)onFooterRefresh
{
    if (IsArrEmpty(self.dataList)) {
        return;
    }
    
    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    OrderListItem *listItem = [self.dataList lastObject];
    NSString *params;
    NSString *carTime = [listItem.carTime stringValue];
    if (IsNilOrNull(carTime)) {
        params = [NSString stringWithFormat:@"status=%lu&time=", (unsigned long)self.orderStatus];
    }
    else {
        params = [NSString stringWithFormat:@"status=%lu&time=%@", (unsigned long)self.orderStatus, carTime];
    }
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, self.requestUrlStr];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseDict) {
        StrongSelf
        [strongSelf.pullRefreshTableView.mj_footer endRefreshing];
        [self resetAction];
        
        NSInteger code_status = [responseDict[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            OrderListResponseData *responseData = [[OrderListResponseData alloc] initWithDictionary:responseDict error:nil];
            if (!IsArrEmpty(responseData.data)) {
                [strongSelf.dataList addObjectsFromArray:responseData.data];
                [strongSelf.pullRefreshTableView reloadData];
            }
        }
        else {
            
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf.pullRefreshTableView.mj_footer endRefreshing];
        [strongSelf resetAction];
    }];
    
}

- (void)onHeaderRefresh {
    
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
//    self.tableView.mj_footer.hidden = YES;
    
    self.loadStatus = LoadStatusLoading;
    NSString *params = (self.orderStatus == 3 || self.orderStatus == 4) ? [NSString stringWithFormat:@"status=%ld&time=", (unsigned long)self.orderStatus]:@"time=";
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, self.requestUrlStr];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseDict) {
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf resetAction];
        strongSelf.loadStatus = LoadStatusFinish;
        
        id code = responseDict[@"code"];
        if (IsNilOrNull(code)) {
            return;
        }
        NSInteger code_status = [responseDict[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            OrderListResponseData *responseData = [[OrderListResponseData alloc] initWithDictionary:responseDict error:nil];
                strongSelf.dataList = responseData.data;
                [strongSelf.pullRefreshTableView reloadData];
        }
        else {
            [strongSelf showAlertView:[self getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
//        [strongSelf showAlertView:errorDesc];
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf resetAction];
        strongSelf.loadStatus = LoadStatusNetworkError;
    }];
    
}

-(void)deleteOrder:(NSString*)orderId
{
    self.nextAction = @selector(deleteOrder:);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *params = [NSString stringWithFormat:@"orderId=%@", orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_DELETE_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在删除订单，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"订单删除成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [strongSelf.pullRefreshTableView beginHeaderRefresh];
                }
            }];
        }
        else {
            [strongSelf showAlertView:[self getErrorMsg:code_status]];
        }
        
        
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
        
    }];
}

#pragma mark - UITableView delegate method

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.001f;
    }
    return 10;
//    return 5.0f;
//    return 0.1f;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.00001f;
////    if (section == self.orderList.count - 1) {
////        return 0.000001f;
////    }
////    return 10;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)index{
    return 80;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderItemTableViewCell *cell = [OrderItemTableViewCell cellWithTableView:tableView];
    OrderListItem *listItem = self.dataList[indexPath.section];
    [cell initData:listItem];
    return cell;
}

//删除操作
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.orderStatus == OrderStatusDispatch) {
        return YES;
    }
    return NO;
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        OrderListItem *orderData = self.dataList[indexPath.section];
//        [self deleteOrder:orderData.id];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否删除" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        WeakSelf
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [weakSelf deleteOrder:orderData.id];
            }
        }];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([self needLogin]) {
        self.nextAction = @selector(tableView:didSelectRowAtIndexPath:);
        self.object1 = tableView;
        self.object2 = indexPath;
//        return;
//    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    OrderListItem *listItem = self.dataList[indexPath.section];
    NSString *params = [NSString stringWithFormat:@"orderId=%@", listItem.id];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_DETAIL_API];
    
    [self showHUDIndicatorViewAtCenter:MSG_ORDER_DETAIL];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        id code = responseData[@"code"];
        if (IsNilOrNull(code)) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *array = responseData[@"data"];
            
            if (!IsArrEmpty(array)) {
                
                NSInteger orderStatus = 0;
                OrderManagerViewController *vc = [[OrderManagerViewController alloc] init];
 
                for (NSDictionary *dict in array) {

                    NSUInteger status = [dict[@"status"] integerValue];
                    NSDictionary *vo = dict[@"vo"];
                    if (status == 1) {
                        vc.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusNew;
                    }
                    if (status == 2) {
                        vc.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusDispatch;
                    }
                    if (status == 3) {
                        vc.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:vo error:nil];
                        if (!IsStrEmpty(vc.vechileArrangeModel.imgUrl)) {
                            [vc downloadCarImage:vc.vechileArrangeModel.imgUrl];
                        }
                        orderStatus = OrderStatusVechileArrange;
                    }
                    if (status == 4) {
                        vc.wayBillModel = [[WayBillResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusWayBill;
                    }
                    if (status == 5) {
                        vc.barginAndPayModel = [[BarginAndPayResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusBarginAndPay;
                    }
                }
                
                vc.source_from = SOURCE_FROM_ORDER_LIST;
                vc.orderStatus = orderStatus;
                vc.orderMode = OrderModeDetail;
                vc.orderId = listItem.id;
                vc.title = @"订单";
                vc.hidesBottomBarWhenPushed = YES;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            [weakSelf showAlertView:[self getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
    
}

////去掉UItableview headerview黏性(sticky)
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (scrollView == self.tableView)
//    {
//        CGFloat sectionHeaderHeight = 10; //sectionHeaderHeight
//        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//        }
//    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
