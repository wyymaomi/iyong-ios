//
//  RealnameCertificationViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@protocol RealnameCertificationDelegate <NSObject>

@optional
-(void)onRealnameCertificationResult:(BOOL)isCertificationed realName:(NSString*)realName idcardNo:(NSString*)idCardNo;

@end

typedef enum : NSUInteger {
    RealnameModeNoEdit,  // 不可编辑
    RealnameModeEdit,   // 编辑
    RealnameModeBindbankcard,   // 绑定银行卡界面实名认证
} RealnameMode;


@interface RealnameCertificationViewController : BaseViewController
{
    BBBasicBlock    _cancelBlock;
    BBBasicBlock    _confirmBlock;
}

@property (nonatomic, assign) NSInteger type;// 0: 显示信息界面；1:编辑界面 2: 绑定银行卡界面实名认证
@property (nonatomic, weak) id<RealnameCertificationDelegate> delegate;

/*!
 @abstract      实名失败的回调
 @discussion    如果你不想用代理的方式来进行回调，可使用该方法
 @param         block  点击取消后执行的程序块
 */
- (void)setCancelBlock:(BBBasicBlock)block;

/*!
 @abstract      实名认证成功的回调
 @discussion    如果你不想用代理的方式来进行回调，可使用该方法
 @param         block  点击确定后执行的程序块
 */
- (void)setConfirmBlock:(BBBasicBlock)block;

@end
