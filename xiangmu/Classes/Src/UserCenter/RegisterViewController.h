//
//  RegisterViewController.h
//  xiangmu
//
//  Created by David kim on 16/4/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "RegisterView.h"
#import "ThirdpartyLoginHttpMessage.h"
#import "ThirdpartyRegisterView.h"

@protocol Username <NSObject>

-(void)writeUsername:(NSString *)username WithPassword:(NSString *)password;

@end

typedef enum : NSUInteger {
    TFTypeMobile,
//    TFTypeRefMobile,
    TFTypeVerifyCode,
    TFTypePassword,
    TFTypeRePassword,
    TFTypeNickName,
    TFTypeCompany
} TextFieldType;


@interface RegisterViewController :LFNavController

@property(nonatomic,assign)id<Username>delegate;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollview;

@property (nonatomic, strong) RegisterView *registerView;

//@property (nonatomic, )

@property (nonatomic, strong) ThirdpartyRegisterView *thirdpartyRegisterView; // 第三方注册View

@property (nonatomic, assign) RegisterType registerType ; // 0:自注册；1:第三方登录注册
@property (nonatomic, assign) ThirdpartyId thirdPartyId; // 第三方平台ID
@property (nonatomic, strong) ThirdpartyLoginHttpMessage *thirdPartyLoginHttpMessage;

@property (nonatomic, strong) UITextField *mobileTextField;
//@property (nonatomic, strong) UITextField *pwdTextField;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *verifyCodeBtn;

-(void)gotoNextPage;

//@property (nonatomic, strong) 

@end

