//
//  NetworkReach.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NetworkReach.h"
#import "AppDelegate.h"
#import "BBAlertView.h"
#import "MBProgressHUD.h"

@interface NetworkReach()


@property (nonatomic, strong) BBAlertView  *networkAlert;

- (void)updateInterfaceWithReachability:(Reachability *)curReach;

@end

@implementation NetworkReach

- (void)initNetwork
{
    self.lastNetworkStatus = ReachableViaWiFi;
    
    // Reachability使用了通知，当网络状态发生变化时发送通知kReachabilityChangedNotification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    // 被通知函数运行的线程应该由startNotifier函数执行的线程决定
    WeakSelf
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // 检测指定服务器是否可达
        NSString *remoteHostName = @"www.baidu.com";
        weakSelf.hostReach = [Reachability reachabilityWithHostName:remoteHostName];
        [weakSelf.hostReach startNotifier];
        // 检测默认路由是否可达
        weakSelf.routerReachability = [Reachability reachabilityForInternetConnection];
        [weakSelf.routerReachability startNotifier];
        // 开启当前线程消息循环
        [[NSRunLoop currentRunLoop] run];
    });

}

/// 当网络状态发生变化时调用
- (void)reachabilityChanged: (NSNotification*)notfication
{
    Reachability* curReach = [notfication object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

-(void)updateInterfaceWithReachability:(Reachability *)curReach
{
    
    DLog(@"\n ======Reachable === <%d>=== \n",curReach.currentReachabilityStatus);
    
    
//    _reachableCount++;
//    
//    if (1 == _reachableCount) {
//        
//        return;
//    }
    
    //    [NSObject cancelPreviousPerformRequestsWithTarget:self
    //                                             selector:@selector(showNetworkAlertMessage)
    //                                               object:nil];
    
//    [NSObject cancelPreviousPerformRequestsWithTarget:self
//                                             selector:@selector(changeStatus)
//                                               object:nil];
    
    NetworkStatus status = [curReach currentReachabilityStatus];
    
    if (status != self.lastNetworkStatus) {
        
        self.lastNetworkStatus = status;
        
        if (NotReachable == curReach.currentReachabilityStatus) {
            
            DLog(@"NotReachable");
            
            [self showNetworkErrorMessage];
        
        }
        else if (ReachableViaWiFi == curReach.currentReachabilityStatus) {
            
            DLog(@"ReachableViaWiFi");
            
        }
        else if (ReachableViaWWAN == curReach.currentReachabilityStatus) {
            
            DLog(@"ReachableViaWiFi");
            
        }
        
        
    }
 
}

//- (void)show

- (void)changeStatus
{
//    AppDelegate *appDelegate = APP_DELEGATE;
//    UIView *contentView = appDelegate.window;
    if (_hostReach.currentReachabilityStatus != self.lastNetworkStatus)
    {
        if (NotReachable == _hostReach.currentReachabilityStatus) {
            
        }
//        if (ReachableViaWiFi == _hostReach.currentReachabilityStatus){
//            
////            [contentView showTipViewAtCenter:L(@"APPDelegate_ChangedToWifi")];
//            
//        }
//        else
//        {
////            [contentView showTipViewAtCenter:L(@"APPDelegate_ChangedToNormal")];
//        }
        
        self.lastNetworkStatus = _hostReach.currentReachabilityStatus;
    }
}

- (void)showNetworkErrorMessage
{
    AppDelegate *appDelegate = APP_DELEGATE;
    UIView *view = appDelegate.window;
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:view];
    HUD.mode = MBProgressHUDModeText;
    [view addSubview:HUD];
    HUD.removeFromSuperViewOnHide = YES;
    [HUD show:YES];
    [HUD hide:YES afterDelay:1.5f];
    HUD.labelText = kMsgNotReachable;
    
}


@end
