//
//  BillRepayHeaderView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillRepayHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLabel;

@end
