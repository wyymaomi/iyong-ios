//
//  VechileHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface VechileHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *fileContent;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *driver;
@property (nonatomic, strong) NSString *age;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *inspectionDate;
@property (nonatomic, strong) NSString *insuranceDate;
@property (nonatomic, strong) NSString *id;

@end
