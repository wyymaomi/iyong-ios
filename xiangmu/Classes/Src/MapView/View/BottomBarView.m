//
//  BottomBarView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BottomBarView.h"

@implementation BottomBarView

-(void)setEnabled:(BOOL)isEnable;
{
    _isEnabled = isEnable;
    
    if (isEnable) {
        [self.orderButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xE06430)] forState:UIControlStateNormal];
//        self.orderButton.enabled = YES;
    }
    else {
        [self.orderButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x999999)] forState:UIControlStateNormal];
//        self.orderButton.enabled = NO;
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
//    if (!IsStrEmpty(self.priceLabel.text)) {
//        [self.priceLabel setTextFont:FONT(13) text:@"元"];
//    }
    
    
    CGSize priceSize = [self.priceLabel.text textSize:CGSizeMake(ViewWidth, 50*Scale) font:self.priceLabel.font];
    self.priceLabel.width = priceSize.width;
    
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.priceLabel.text];
//    [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xe06430),
//                                      NSFontAttributeName:FONT(12)}
//                              range:[self.priceLabel.text rangeOfString:@"元"]];
//    self.priceLabel.attributedText = attributedString;
    
//    CGSize distanceSize = [self.distanceLabel.text textSize:CGSizeMake(ViewWidth, 50*Scale) font:self.distanceLabel.font];
//    self.distanceLabel.frame = CGRectMake(self.priceLabel.right+5, self.distanceLabel.origin.y, distanceSize.width, self.priceLabel.height);
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
