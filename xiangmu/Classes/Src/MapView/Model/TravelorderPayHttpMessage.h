//
//  TravelorderPayHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/28.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

//payMode 支付方式(1.信用余额，2=账户余额，3=推广余额，4=支付宝, 5=iyong，6=微信支付)
//amount 支付金额

@interface TravelorderPayHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *orderId;// 订单ID
@property (nonatomic, strong) NSNumber *payMode;//payMode 支付方式(1.信用余额，2=账户余额，3=推广余额，4=支付宝, 5=iyong，6=微信支付)
@property (nonatomic, strong) NSNumber *amount;//amount 支付金额

@end
