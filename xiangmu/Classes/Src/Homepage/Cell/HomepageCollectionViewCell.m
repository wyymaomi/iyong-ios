//
//  HomepageCollectionViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomepageCollectionViewCell.h"

@implementation HomepageCollectionViewCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.companyLabel.top = self.imageView.bottom;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
