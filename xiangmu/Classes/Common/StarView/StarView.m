//
//  StarView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "StarView.h"





@implementation StarView

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        
//        [self setStarsImages:5];
        
    }
    return self;
}

- (void)setStarsImages:(CGFloat)starFloat;
{
    
//    CGFloat evaValue = 0;
//    
//    if (evaluation != nil || ![evaluation isEqualToString:@""])
//    {
//        evaValue = [evaluation floatValue];
//    }
    
    for (int i = 0; i < 5; i++)
    {
        NSString *selecter = [NSString stringWithFormat:@"star%d", i+1];
        
//        SuppressPerformSelectorLeakWarning
//        (UIImageView *star = [self performSelector:NSSelectorFromString(selecter)];
       
        UIImageView *star = [self performSelector:NSSelectorFromString(selecter)];
        
//         SuppressPerformSelectorLeakWarning (
         if (starFloat-i < 0.5)
         {
//             star.image = [UIImage imageNamed:@"icon_grayStar"];
             star.image = [UIImage imageNamed:@"icon_star_gray"];
         }
//         else if (starFloat-i >= 0.5 && starFloat-i < 1)
//         {
//             star.image = [UIImage imageNamed:@"halfStart.png"];
//         }
         else if (starFloat-i >= 0.5)
         {
//             star.image = [UIImage imageNamed:@"icon_star"];
             star.image = [UIImage imageNamed:@"icon_star_green"];
         };
//        )
        
    }
}


- (UIImageView *)star1
{
    
    if (!_star1) {
        _star1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kStarSizeWidth, kStarSizeHeight)];
        _star1.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview: _star1];
    }
    
    return _star1;
}

- (UIImageView *)star2
{
    
    if (!_star2) {
        _star2 = [[UIImageView alloc] initWithFrame:CGRectMake(kStarFrameWith, 0, kStarSizeWidth, kStarSizeHeight)];
        _star2.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview: _star2];
    }
    
    return _star2;
}

- (UIImageView *)star3
{
    
    if (!_star3) {
        _star3 = [[UIImageView alloc] initWithFrame:CGRectMake(kStarFrameWith*2, 0, kStarSizeWidth, kStarSizeHeight)];
        _star3.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview: _star3];
    }
    
    return _star3;
}

- (UIImageView *)star4
{
    
    if (!_star4) {
        _star4 = [[UIImageView alloc] initWithFrame:CGRectMake(kStarFrameWith*3, 0, kStarSizeWidth, kStarSizeHeight)];
        _star4.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview: _star4];
    }
    
    return _star4;
}

- (UIImageView *)star5
{
    
    if (!_star5) {
        _star5 = [[UIImageView alloc] initWithFrame:CGRectMake(kStarFrameWith*4, 0, kStarSizeWidth, kStarSizeHeight)];
        _star5.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview: _star5];
    }
    
    return _star5;
}


@end
