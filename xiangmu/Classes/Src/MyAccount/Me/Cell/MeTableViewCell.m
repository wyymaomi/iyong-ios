//
//  MeTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MeTableViewCell.h"

@implementation MeTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    
    static NSString *ID =  @"MeTableViewCell";
    MeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    //    cell.nickNameLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.nickname] ;
//    //    cell.mobileLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.username];
//    //    cell.companyLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.companyName];
//    cell.usernameLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.username];
//    
//    cell.avatarImageView.cornerRadius = cell.avatarImageView.frame.size.width/2;
//    cell.avatarImageView.borderWidth = 2;
//    cell.avatarImageView.borderColor = [UIColor whiteColor];
//    
//    //    LRWeakSelf(cell)
//    [cell.avatarImageView downloadImage:[UserManager sharedInstance].userModel.logoUrl placeholderImage:[UIImage imageNamed:@"icon_avatar"] success:^(id responseData) {
//        
//    } andFailure:^(NSString *errorDesc) {
//        //        weakcell.avatarImageView.cornerRadius = 5;
//    }];
    
    
    return cell;
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    self.reddotImageView.width = 10*Scale;
    
    self.reddotImageView.height = 10*Scale;
    
    self.reddotImageView.top = (self.height-self.reddotImageView.height)/2;
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
