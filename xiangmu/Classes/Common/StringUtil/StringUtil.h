//
//  StringUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringUtil : NSObject

+ (NSString*)getSafeString:(NSString*)string;

+ (BOOL)isIdentityCardNo:(NSString*)idCardStr;

+ (NSString*)encodeBindIdcardNo:(NSString*)idCardStr;

+ (NSString*)encodeBankcardNo:(NSString*)bankcardNoStr;

+ (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width;

+ (float) widthForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width;

+(CGSize)sizeForString:(NSString*)value fontSize:(float)fontSize andWith:(float)width;

+(NSString*)createUDID;

@end
