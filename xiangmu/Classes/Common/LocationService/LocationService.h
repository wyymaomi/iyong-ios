//
//  LocationService.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/24.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

//returnCode 错误码
enum _SNLocateCityErrorCode {
    LocateCitySuccess = 0,    //定位成功
    LocateCityFail,           //定位失败
    LocateCityAddressFail,    //获取详细地址信息失败
    LocateCityServiceUnable,  //定位服务不可用
    LocateCityUserDenied,     //用户不允许定位
};

@protocol LocationServiceDelegate <NSObject>

- (void)onLocateCityServiceUnable;
- (void)onLocateCityUserDenied;
- (void)onLocateCitySuccess:(NSString*)city;
- (void)onLocateCityFail;
- (void)onLocateCityAddressFail;

@end

@interface LocationService : NSObject<CLLocationManagerDelegate>
{
    CLLocationManager *manager;
    CLGeocoder *geocoder;
}

AS_SINGLETON(LocationService)

//responses
@property (nonatomic, assign) NSInteger     responseStatus; //定位状态
@property (nonatomic, strong) NSDictionary *addressInfoDic; //地址信息dic
@property (nonatomic, copy)   NSString     *cityName;       //城市名称 如：“南京市”
@property (nonatomic, assign) CLLocationCoordinate2D coordinate; //当前用户坐标
@property (nonatomic, assign) CGFloat     timeOutDefault;

@property (nonatomic, weak) id<LocationServiceDelegate> delegate;

-(void)startLocation;

//将定位到的城市保存到Config,需要手动调用
+ (void)saveCityToConfig:(NSString *)cityName;


@end
