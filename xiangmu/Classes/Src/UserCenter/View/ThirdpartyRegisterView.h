//
//  ThirdpartyRegisterView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/9.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface ThirdpartyRegisterView : UIView

@property (weak, nonatomic) IBOutlet UIView *recommendCodeView;
@property (weak, nonatomic) IBOutlet UIImageView *recommendCodeIconImageView;
@property (weak, nonatomic) IBOutlet UITextField *recommendCodeTextField;
@property (weak, nonatomic) IBOutlet UIView *recommendCodeLineView;

@property (weak, nonatomic) IBOutlet UIView *verifyCodeLineView;
@property (weak, nonatomic) IBOutlet UIButton *verifyCodeButton;
@property (weak, nonatomic) IBOutlet UITextField *verifyCodeTextField;
@property (weak, nonatomic) IBOutlet UIImageView *verifyCodeIconView;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *tpkeyboardAvoidingScrollView;

@property (weak, nonatomic) IBOutlet UILabel *bingPhoneLabel;

@property (weak, nonatomic) IBOutlet UIView *accountView;
@property (weak, nonatomic) IBOutlet UIView *verifyCodeView;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UIImageView *accountImgView;
@property (weak, nonatomic) IBOutlet UITextField *accountTextField;
@property (weak, nonatomic) IBOutlet UIView *accountLineView;

@end
