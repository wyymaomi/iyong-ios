//
//  FollowCommentBodyView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "FollowCommentBodyView.h"

@implementation FollowCommentBodyView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.iconView];
        [self addSubview:self.nameLabel];
//        [self addSubview:self.companyLabel];
        //        [self addSubview:self.starView];
        [self addSubview:self.timeLabel];
        [self addSubview:self.contentLabel];
        //        [self addSubview:self.photosView];
        
    }
    return self;
}

- (void)setViewModel:(ExperienceCommentViewModel *)viewModel
{
    _viewModel = viewModel;
    
    // 给子控件设置frame
    [self setFrame];
    
    // 为子控件赋值
    [self setData];
    
}

- (void)setData
{
    self.iconView.image = [UIImage imageNamed:@"icon_avatar"];
    self.nameLabel.text = self.viewModel.commentModel.nickname;
//    self.companyLabel.text = self.viewModel.serviceModel.companyName;
    self.timeLabel.text = [self.viewModel.commentModel displayTime];
    self.contentLabel.text = self.viewModel.commentModel.content;
//    self.timeLabel.text = self.viewModel.commentModel display
    //    [self.starView setStarsImages:[self.viewModel.serviceModel.companyScor integerValue]];
    
//    WeakSelf
    // 下载头像图片
//    [self.iconView downloadImage:self.viewModel.commentModel.commenterLogoUrl placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        weakSelf.iconView.cornerRadius = weakSelf.iconView.width/2;
//    } andFailure:^(NSString *errorDesc) {
//        weakSelf.iconView.cornerRadius = weakSelf.iconView.width/2;
//    }];
    
//    [self.iconView downloadImageFromAliyunOSS:self.viewModel.commentModel.commenterLogoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:^{ andFailure:nil];
    
//    LRWeakSelf(self.)
    
    self.iconView.cornerRadius = self.iconView.width/2;
    
//    [self.iconView downloadImage:self.viewModel.commentModel.commenterLogoUrl placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        
////        weakSelf.iconView.cornerRadius = self.iconView.width/2;
//        
//    } andFailure:^(NSString *errorDesc) {
//       
////        weakSelf.iconView.cornerRadius = self.iconView.width/2;
//        
//    }];
    
    [self.iconView yy_setImageWithObjectKey:self.viewModel.commentModel.commenterLogoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
    }];
    
}

- (void)setFrame
{
    self.nameLabel.font = self.viewModel.nameFont;
    self.contentLabel.font = self.viewModel.contentFont;
    self.nameLabel.font = self.viewModel.nameFont;
    self.timeLabel.font = self.viewModel.timeFont;
    
    self.iconView.frame = self.viewModel.bodyIconFrame;
    self.nameLabel.frame = self.viewModel.bodyNameFrame;
    self.contentLabel.frame = self.viewModel.bodyTextFrame;
    self.timeLabel.frame = self.viewModel.bodyTimeFrame;
//    self.companyLabel.frame = self.viewModel.bodyTimeFrame;
    //    self.starView.frame = ViewWidth
//    self.starView.left = ViewWidth-self.starView.width-12;
//    self.starView.top  = self.viewModel.bodyNameFrame.origin.y;
    
    
    // 如果没有图片则隐藏图片View
    //    if ([self.viewModel.serviceModel hasPhoto]) {
    //
    //        self.photosView.hidden = NO;
    //        self.photosView.frame = self.viewModel.bodyPhotoFrame;
    //
    //        NSInteger photoWidth = 70;
    //        NSInteger photoHeight = self.photosView.height;
    //        //        NSInteger imageTop = (self.photosView.frame.size.height - photoHeight)/2;
    //        NSInteger separator = (self.photosView.width - photoWidth * 4) / 3;
    //        //        NSInteger imageLeft = 15;
    //
    //        self.photosView.photoWidth = photoWidth;
    //        self.photosView.photoHeight = photoHeight;
    //        self.photosView.photoMargin = separator;
    //
    //        NSMutableArray *imgUrls = [NSMutableArray new];
    //        if (!IsStrEmpty([StringUtil getSafeString:self.viewModel.serviceModel.img1])) {
    //            [imgUrls addObject:self.viewModel.serviceModel.img1];
    //        }
    //        if (!IsStrEmpty([StringUtil getSafeString:self.viewModel.serviceModel.img2])) {
    //            [imgUrls addObject:self.viewModel.serviceModel.img2];
    //        }
    //        if (!IsStrEmpty([StringUtil getSafeString:self.viewModel.serviceModel.img3])) {
    //            [imgUrls addObject:self.viewModel.serviceModel.img3];
    //        }
    //        if (!IsStrEmpty([StringUtil getSafeString:self.viewModel.serviceModel.img4])) {
    //            [imgUrls addObject:self.viewModel.serviceModel.img4];
    //        }
    //        [imgUrls addObject:self.viewModel.serviceModel.img1];
    //        [imgUrls addObject:self.viewModel.serviceModel.img2];
    //        [imgUrls addObject:self.viewModel.serviceModel.img3];
    //        [imgUrls addObject:self.viewModel.serviceModel.img4];
    
    //        self.photosView.originalUrls = [imgUrls mutableCopy];
    //        self.photosView.thumbnailUrls = [imgUrls mutableCopy];
    //
    //        self.photosView.photosMaxCol = 4;
    
    //        for (NSInteger i = 0; i < self.imageViewArray.count; i++) {
    //
    //            UIImageView *imageView = self.imageViewArray[i];
    //
    //            if (i == 0) {
    //                if (self.viewModel.serviceModel.img1.trim.length > 0) {
    //                    imageView.frame = CGRectMake(imageLeft, imageTop, photoWidth, photoHeight);
    //                    imageLeft = imageView.right + separator;
    //                }
    //            }
    //
    //            if (i == 1) {
    //                if (self.viewModel.serviceModel.img2.trim.length > 0) {
    //                    imageView.frame = CGRectMake(imageLeft, imageTop, photoWidth, photoHeight);
    //                    imageLeft = imageView.right + separator;
    //                }
    //            }
    //
    //            if (i == 2) {
    //                if (self.viewModel.serviceModel.img3.trim.length > 0) {
    //                    imageView.frame = CGRectMake(imageLeft, imageTop, photoWidth, photoHeight);
    //                    imageLeft = imageView.right + separator;
    //                }
    //            }
    //
    //            if (i == 3) {
    //                if (self.viewModel.serviceModel.img4.trim.length > 0) {
    //                    imageView.frame = CGRectMake(imageLeft, imageTop, photoWidth, photoHeight);
    //                    imageLeft = imageView.right + separator;
    //                }
    //            }
    //
    //
    //        }
    
    //        if (self.viewModel.serviceModel.img1.trim.length > 0) {
    //            self.photoImageView1.frame = CGRectMake(15, edgeTop, photoWidth, photoHeight);
    //            imageLeft = self.photoImageView1.right + separator;
    ////            DLog(@"imgView1:imageLeft = %d", imageLeft);
    //        }
    //        if (self.viewModel.serviceModel.img2.trim.length > 0) {
    //            self.photoImageView2.frame = CGRectMake(imageLeft, edgeTop, photoWidth, photoHeight);
    //            imageLeft = self.photoImageView2.right + separator;
    ////            DLog(@"imgView2:imageLeft = %d", (long)imageLeft);
    //        }
    //        if (self.viewModel.serviceModel.img3.trim.length > 0) {
    //            self.photoImageView3.frame = CGRectMake(imageLeft, edgeTop, photoWidth, photoHeight);
    //            imageLeft = self.photoImageView3.right + separator;
    ////            DLog(@"imgView3:imageLeft = %d", imageLeft);
    //        }
    //        if (self.viewModel.serviceModel.img4.trim.length > 0) {
    //            self.photoImageView4.frame = CGRectMake(imageLeft, edgeTop, photoWidth, photoHeight);
    //        }
    
    
    //    }
    //    else {
    ////        self.photosView.hidden = YES;
    //    }
}

//- (void)onClickImageView:(UITapGestureRecognizer *)recognizer
//{
    //    UIImageView *imageView
    
    //    (UITapGestureRecognizer *)recognizer
    
//    NSInteger tag = recognizer.view.tag;
    
    //    UIImageView *imageView = [self viewWithTag:tag];
    
//    if (self.cell && self.cell.delegate && [self.cell.delegate respondsToSelector:@selector(onClickImageView:)]) {
        //        [self.cell.delegate onClickImageView:tag];
//    }
    
    
//}

#pragma mark - getter and setter

//- (StarView*)starView
//{
//    if (_starView == nil) {
//        _starView = [StarView new];
//        _starView.frame = CGRectMake(0, 0, 60, 20);
//    }
//    return _starView;
//}

//- (PYPhotosView*)photosView
//{
//    if (_photosView == nil) {
//        _photosView = [PYPhotosView new];
//        _photosView.backgroundColor = [UIColor whiteColor];

//        for (NSInteger i = 0; i < 4; i++) {
//            UIImageView *imageView = [UIImageView new];
//            [_photosView addSubview:imageView];
//            imageView.cornerRadius = 8;
//            imageView.tag = i * 1000;
//            [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
////            [_photosView addSubview:imageView];
//            [self.imageViewArray addObject:imageView];
//        }

//        [self addSubview:_photosView];

//        _photoImageView1 = [UIImageView new];
//        _photoImageView1.image = [UIImage imageNamed:@"img_vechile_default"];
//        [_photosView addSubview:_photoImageView1];
//        _photoImageView1.cornerRadius = 8;
//        _photoImageView1.tag = 0;
////        _photoImageView1 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:tag)]
//
//        _photoImageView2 = [UIImageView new];
//        _photoImageView2.image = [UIImage imageNamed:@"img_vechile_default"];
//        [_photosView addSubview:_photoImageView2];
//        _photoImageView2.cornerRadius = 8;
//
//        _photoImageView3 = [UIImageView new];
//        _photoImageView3.image = [UIImage imageNamed:@"img_vechile_default"];
//        [_photosView addSubview:_photoImageView3];
//        _photoImageView3.cornerRadius = 8;
//
//        _photoImageView4 = [UIImageView new];
//        _photoImageView4.image = [UIImage imageNamed:@"img_vechile_default"];
//        [_photosView addSubview:_photoImageView4];
//        _photoImageView4.cornerRadius = 8;

//    }
//    return _photosView;
//}

- (UIView*)separatorLineView
{
    if (_separatorLineView == nil) {
        _separatorLineView = [UIView new];
        _separatorLineView.backgroundColor = [UIColor clearColor];
    }
    return _separatorLineView;
}


- (UIImageView*)iconView
{
    if (_iconView == nil) {
        _iconView = [[UIImageView alloc] init];
    }
    return _iconView;
}

- (UILabel*)nameLabel;
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = FONT(13);
        //        _nameLabel.textColor = UIColorFromRGB(0xA9A9A9);
    }
    
    return _nameLabel;
}

- (UILabel*)companyLabel
{
    if (_companyLabel == nil) {
        _companyLabel = [[UILabel alloc] init];
        _companyLabel.font = FONT(13);
        //        _companyLabel.textColor = UIColorFromRGB(0xA9A9A9);
    }
    
    return _companyLabel;
}

- (UILabel*)contentLabel
{
    if (_contentLabel == nil) {
        _contentLabel = [UILabel new];
        _contentLabel.font = FONT(13);
        //        _contentLabel.textColor = UIColorFromRGB(0xA9A9A9);
        _contentLabel.numberOfLines = 0;
        [_contentLabel sizeToFit];
        
    }
    
    return _contentLabel;
}

- (UILabel*)timeLabel
{
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        _timeLabel.textColor = UIColorFromRGB(0x9a9a9a);
    }
    return _timeLabel;
}

@end
