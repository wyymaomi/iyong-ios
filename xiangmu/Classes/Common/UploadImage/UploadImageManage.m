//
//  UploadImageManage.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UploadImageManage.h"

@interface UploadImageManage ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UIActionSheet *actionSheet;

@end

@implementation UploadImageManage

//DEF_SINGLETON(UploadImageManage)

- (instancetype)initWithViewController:(UIViewController*)viewController;
{
    
    if (self = [super init]) {
        
        self.viewController = viewController;
        
    }
    
    return self;

}

- (void)showActionSheet;
{
    [self showActionSheet:0];
}

-(void)showActionSheet:(NSInteger)tag
{
    _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"拍照", @"从相册选择", nil];
    _actionSheet.tag = tag;
    [_actionSheet showInView:self.viewController.view];
}

#pragma mark - UIActionSheet Delegate Method

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        
    }
    switch (buttonIndex) {
        case 0:
            [self takePhoto];
            break;
        case 1:
            [self LocalPhoto];
            break;
        default:
            break;
    }
}

//开始拍照
-(void)takePhoto
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.delegate = self;
        //设置拍照后的图片可被编辑
        cameraPicker.allowsEditing = YES;
        cameraPicker.sourceType = sourceType;
        
        [self.viewController presentViewController:cameraPicker animated:YES completion:nil];
    }else
    {
        NSLog(@"模拟其中无法打开照相机,请在真机中使用");
    }
}

//打开本地相册
-(void)LocalPhoto
{
    UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
    
    photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    photoPicker.delegate = self;
    //设置选择后的图片可被编辑
    photoPicker.allowsEditing = YES;
    [self.viewController presentViewController:photoPicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllDelegate method

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]) {
        UIImage* image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        
        CGSize imageSize = CGSizeMake(320, 300);
        NSInteger maxImageFileLength = 320*240;
        
        UIImage *tempImage = [ImageUtil imageWithImage:image scaledToSize:imageSize];
        tempImage = [ImageUtil compressImage:tempImage toMaxFileSize:maxImageFileLength];
//        self.avatarImageView.image = tempImage;
//        
//        NSData *data = UIImageJPEGRepresentation(tempImage, 1);
//        DLog(@"data.size = %lu", (unsigned long)data.length);
//        
//        self.vechileHttpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:data] encoding:NSUTF8StringEncoding];
//        
//        [self.tableView reloadData];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
//        if (_delegate && [_delegate respondsToSelector:@selector(didFinishPicker:)]) {
//            [_delegate didFinishPicker:tempImage];
//        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(didFinishPicker:actionSheet:)]) {
            [self.delegate didFinishPicker:tempImage actionSheet:self.actionSheet];
        }
        
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"您取消了选择图片");
    [picker dismissViewControllerAnimated:YES completion:nil];
}


@end
