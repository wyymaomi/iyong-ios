//
//  HomeViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomeViewController+NetworkRequest.h"
#import "AppConfig.h"
#import "AdvModel.h"

@implementation HomeViewController (NetworkRequest)

- (void)fetchBannerAdvList
{
    [self.viewModel.advList removeAllObjects];
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, BANNER_ADV_LIST_URL];
    NSDictionary *params = @{/*@"time":time,*/
                             @"type": @2/*,
                             @"areaCode":[AppConfig currentConfig].defaultCityCode*/};
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        StrongSelf
        if (code_status == STATUS_OK) {
            
            
            NSError *error;
            AdvResponseData *responseData = [[AdvResponseData alloc] initWithDictionary:result error:&error];
            
            if (!error) {
                
                for (NSInteger i = 0; i < responseData.data.count; i++) {
                    @autoreleasepool {
                        AdvModel *advModel = responseData.data[i];
                        [strongSelf.viewModel.advList addObject:advModel];
                    }
                    
                    
                }
                
            }
            
            [strongSelf refreshView];
            
        }
        else if (code_status == NETWORK_FAILED)
        {
            //            [weakSelf showAlertView:MSG_GET_VERIFY_FAILURE];
            [strongSelf refreshView];
        }
        else {
            
            [strongSelf refreshView];
            //            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
        
    }];
    
}

- (void)refreshView
{
//    @synchronized (self) {
    
//    [self.headerPullRefreshTableView reloadData];
//    [self.headerPullRefreshTableView endHeaderRefresh];
//    return;
    
    if (self.downloadCount < self.requestMaxCount) {
        dispatch_semaphore_wait(_lock, DISPATCH_TIME_FOREVER);
        self.downloadCount++;
        [self.headerPullRefreshTableView reloadData];
        dispatch_semaphore_signal(_lock);
        
    }
    if (self.downloadCount == self.requestMaxCount) {

    
        dispatch_main_sync_safe(^{
        
            DLog(@"WYY refresh table view");
            
            if (self.headerPullRefreshTableView) {
                [self.headerPullRefreshTableView reloadData];
                [self.headerPullRefreshTableView endHeaderRefresh];
                
            }
            

            
        });
        
    } else {
     
         DLog(@"WYY Loading");
    }
   
//        if (self.downloadCount == self.requestMaxCount) {
////            dispatch_async(dispatch_main, <#^(void)block#>)
////            dispatch_main_async_safe(^{
//            
////                return;
////            });
//
//        }
//        else {
//            
//            self.downloadCount++;
//        }
    
//    }

}

#pragma makr - 根据分类请求商户列表

- (void)fetchServiceProviderByType:(NSUInteger)pageIndex areaCode:(NSString*)areaCode businessType:(NSUInteger)businessType
{
    if (pageIndex < 1) {
        return;
    }
    
//    NSString *url
    
    [self.viewModel.weddingList removeAllObjects];
    [self.viewModel.travelList removeAllObjects];
    [self.viewModel.charteredList removeAllObjects];
    [self.viewModel.commercialList removeAllObjects];
    [self.viewModel.airportList removeAllObjects];
    [self.viewModel.busServiceList removeAllObjects];
    [self.viewModel.goodsList removeAllObjects];
    [self.viewModel.truckList removeAllObjects];
    
    NSDictionary *params  = @{@"pageIndex" : [NSString stringWithFormat:@"%ld", (long)pageIndex],
                              @"areaCode" : areaCode,
                              @"businesType" : [NSNumber numberWithInteger:businessType]};
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startHttpPost:APIWithBaseUrl(HOME_PROVIDER_TYPE_LIST_API) params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        if (code_status == STATUS_OK) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
                if (error) {
                    [strongSelf refreshView];
                    return;
                }
                
                NSArray<OnlineServiceModel*> *list = responseData.data.list;
                
                NSMutableArray<OnlineServiceModel*> *dataList = [NSMutableArray new];
                
                for (NSInteger i = 0; i < list.count; i++) {
                    OnlineServiceModel *model = list[i];
                    //                    if (NotNilAndNull(model)) {
                    [dataList addObject:model];
                    //                    }
                }
                
                
                switch (businessType) {
                    case BusinessTypeCommercial:
                        strongSelf.viewModel.commercialList = dataList;
                        break;
                    case BusinessTypeCompanyBus:
                        strongSelf.viewModel.busServiceList = dataList;
                        break;
                    case BusinessTypeGoods:
                        strongSelf.viewModel.goodsList = dataList;
                        break;
                    case BusinessTypeTruck:
                        strongSelf.viewModel.truckList = dataList;
                        break;
                    case BusinessTypeAirport:
                        strongSelf.viewModel.airportList = dataList;
                        break;
                    case BusinessTypeTravel:
                        strongSelf.viewModel.travelList = dataList;
                        break;
                    case BusinessTypeCharter:
                        strongSelf.viewModel.charteredList = dataList;
                        break;
                    case BusinessTypeWedding:
                        strongSelf.viewModel.weddingList = dataList;
                        break;
                        
                    default:
                        break;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
                    [strongSelf refreshView];
                    
                });
                
            });
            
        }
        else {
            
            [strongSelf refreshView];
            
        }
        
    }];
    
//    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(HOME_PROVIDER_TYPE_LIST_API) requestMethod:POST params:params success:^(id responseData) {
//        
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//        
//        
//    }];
}

#pragma mark - 请求gps定位城市列表
//-(void)fetchGpsServiceProviderListByPageIndex:(NS)

#pragma mark - 按页码分页方式请求服务商数据
// requestType:0 查询北上杭深全国城市商户信息；1:查询gps定位城市商户信息
- (void)fetchServiceProviderByPageIndex:(NSInteger)pageIndex areaCode:(NSString*)areaCode requestType:(NSUInteger)requestType
{
    
    if (pageIndex < 1) {
        return;
    }
    
    
    
    NSString *url;
    url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, HOME_PROVIDER_LIST_API];
    //    if ([self.title isEqualToString:@"我的收藏"]) {
    //        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, MY_FAVORITE_SERVICER_API];
    //    }
    
    NSDictionary *params = @{@"pageIndex": [NSString stringWithFormat:@"%ld", (long)pageIndex],
                             @"areaCode":areaCode};
    
    // 判断当前选择城市是否北上杭深
//    NSString *defaultAreaCode = [AppConfig currentConfig].defaultCityCode;
//    if ([areaCode isEqualToString:BJ_CITY_CODE] || [areaCode isEqualToString:SH_CITY_CODE] ||
//        [areaCode isEqualToString:HZ_CITY_CODE] || [areaCode isEqualToString:SZ_CITY_CODE]) {
//        //        self.tableViewRows = 8;
//        [self.viewModel.gpsCityList removeAllObjects];
//        self.requestMaxCount = 8;
//    }
    
//    if ([areaCode isEqualToString:SZ_CITY_CODE]) {
//        [self.viewModel.shenzhenList removeAllObjects];
//    }
//    else if ([areaCode isEqualToString:HZ_CITY_CODE]) {
//        [self.viewModel.hangzhouList removeAllObjects];
//    }
//    else if ([areaCode isEqualToString:BJ_CITY_CODE]) {
//        [self.viewModel.beijingList removeAllObjects];
//    }
//    else if ([areaCode isEqualToString:SH_CITY_CODE]) {
//        [self.viewModel.shanghaiList removeAllObjects];
//    }
//    else if ([areaCode isEqualToString:@""]) {
//        [self.viewModel.providerList removeAllObjects];
//    }
//    else {
//        [self.viewModel.gpsCityList removeAllObjects];
//    }
    if ([areaCode isEqualToString:@""]) {
        [self.viewModel.providerList removeAllObjects];
    }
    else {
        [self.viewModel.gpsCityList removeAllObjects];
    }
    
//    __block NSMutableArray *dataList = [NSMutableArray new];
    
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        if (code_status == STATUS_OK) {
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
                if (error) {
                    [strongSelf refreshView];
                    return;
                }
                
                NSArray<OnlineServiceModel*> *list = responseData.data.list;
                NSMutableArray<OnlineServiceModel*> *dataList = [NSMutableArray new];
                
                for (NSInteger i = 0; i < list.count; i++) {
                    OnlineServiceModel *model = list[i];
//                    if (NotNilAndNull(model)) {
                        [dataList addObject:model];
//                    }
                }
                
                if (requestType == 0) {
//                    if ([areaCode isEqualToString:SZ_CITY_CODE]) {
//                        strongSelf.viewModel.shenzhenList = dataList;
//                    }
//                    else if ([areaCode isEqualToString:HZ_CITY_CODE]) {
//                        strongSelf.viewModel.hangzhouList = dataList;
//                    }
//                    else if ([areaCode isEqualToString:BJ_CITY_CODE]) {
//                        
//                        strongSelf.viewModel.beijingList = dataList;
//                    }
//                    else if ([areaCode isEqualToString:SH_CITY_CODE]) {
//                        strongSelf.viewModel.shanghaiList = dataList;
//                    }
//                    else
                        if ([areaCode isEqualToString:@""]) {
                        strongSelf.viewModel.providerList = dataList;
                    }
                    else {
                        strongSelf.viewModel.gpsCityList = dataList;
                    }
                }
                else {
                    strongSelf.viewModel.gpsCityList = dataList;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
                    [strongSelf refreshView];
                    
                });
                
            });
            
        }
        else if (code_status == NETWORK_FAILED) {
            strongSelf.loadStatus = LoadStatusNetworkError;
//            [strongSelf.headerPullRefreshTableView endHeaderRefresh];
            [strongSelf refreshView];
            
        }
        else {
            [strongSelf refreshView];
//            [strongSelf.headerPullRefreshTableView endHeaderRefresh];
            strongSelf.loadStatus = LoadStatusFinish;
//            NSError *error;
//            OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
//            if (error) {
//                return;
//            }
            //            [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
        }
        
    }];
    
    
}

#pragma mark -
- (void)fetchRecommendList
{
    
    [self.viewModel.recommendList removeAllObjects];
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, HOME_RECOMMENT_PROVIDER_LIST_API];
    NSDictionary *params = @{@"pageIndex": @1};
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        if (code_status == STATUS_OK) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
                if (!error) {
//                    return;
                    
                    NSArray<OnlineServiceModel*> *list = responseData.data.list;
                    //                if (list.count > 0) {
                    //                    self.commonPage.pageIndex++;
                    //                }
                    for (NSInteger i = 0; i < list.count; i++) {
                        OnlineServiceModel *model = list[i];
                        [strongSelf.viewModel.recommendList addObject:model];
                    }
                }
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
//                    [strongSelf.headerPullRefreshTableView reloadData];
                    [strongSelf refreshView];
                    
//                    [strongSelf.headerPullRefreshTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:HomeTableRowRecommend inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    
                });
                
            });

            
        }
        else {
            [strongSelf refreshView];
        }
        
    }];
}

- (void)fetchExperienceList
{
//    [self.recommendList removeAllObjects];
    [self.viewModel.experienceList removeAllObjects];
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, HOME_EXPERIENCE_LIST_API];
    NSDictionary *params = @{@"pageIndex": @1};
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        if (code_status == STATUS_OK) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
                if (!error) {
                    
                    NSArray<UserExpericeModel*> *list = responseData.data.list;
                    
                    for (NSInteger i = 0; i < list.count; i++) {
                        UserExpericeModel *model = list[i];
//                        if (model) {
                            [strongSelf.viewModel.experienceList addObject:model];
//                        }
                    }
                    
                }
                
                
                
//                dispatch_async(dispatch_get_main_queue(), ^{
                
//                    [strongSelf.headerPullRefreshTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:HomeTableRowExperience inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    
//                    [strongSelf.headerPullRefreshTableView reloadData];
                    [strongSelf refreshView];
                    
//                });
                
            });
            
            
        }
        else {
            [strongSelf refreshView];
        }
        
    }];

}

@end
