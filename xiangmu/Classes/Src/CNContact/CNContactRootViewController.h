//
//  CNContactRootViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/10.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ContactsUI/ContactsUI.h>


@protocol CNContactRootViewControllerDelegate <NSObject>

-(void)onGetContactMobile:(NSString*)phone;

@end

@interface CNContactRootViewController : CNContactPickerViewController<CNContactPickerDelegate>

@property (nonatomic, weak) id<CNContactRootViewControllerDelegate> controllerDelegate;

@end
