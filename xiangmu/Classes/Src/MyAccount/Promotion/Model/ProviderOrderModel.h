//
//  ProviderOrderModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol ProviderOrderDetailModel <NSObject>


@end

@interface ProviderOrderDetailModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *adOrderId;
@property (nonatomic, strong) NSString<Optional> *price;
@property (nonatomic, strong) NSString<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *position;
@property (nonatomic, strong) NSString<Optional> *startTime;
@property (nonatomic, strong) NSString<Optional> *endTime;
@property (nonatomic, strong) NSString<Optional> *areaCode;
@property (nonatomic, strong) NSString<Optional> *areaName;
@property (nonatomic, strong) NSString<Optional> *adMainId;

@end

@interface ProviderOrderModel : JSONModel
//@property (nonatomic, strong) NSString<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *orderCode;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *payTime;
@property (nonatomic, strong) NSString<Optional> *price;
@property (nonatomic, strong) NSNumber<Optional> *orderStatus;
@property (nonatomic, strong) NSNumber<Optional> *payMode;
@property (nonatomic, strong) NSNumber<Optional> *position;
@property (nonatomic, strong) NSString<Optional> *createTime;
@property (nonatomic, strong) NSArray<Optional,ProviderOrderDetailModel> *details;

@end
