//
//  TextInputHelperViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "TextInputHelperViewController.h"

#define MAX_LENGTH_OF_ROUGH_ITINERY 200
#define MAX_LENGTH_OF_REMARK 100

@interface TextInputHelperViewController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *msgLabel;

@end

@implementation TextInputHelperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavBar];
    if (self.text != nil && self.text.length > 0) {
        self.textView.text = self.text;
    }
    if (_tag == OrderTextFieldStartLocation) {
        self.textView.placeholder = @"请输入出发地点";
//        self.msgLabel.text = [NSString stringWithFormat:@"%ld字以内", (long)MAX_LENGTH_OF_REMARK];
    }
    else if (_tag == OrderTextFieldItinerary || _tag == InquireRoughtTravel) {
        self.textView.placeholder = @"请输入大致行程";
        self.msgLabel.text = [NSString stringWithFormat:@"%ld字以内", (long)MAX_LENGTH_OF_ROUGH_ITINERY];
    }
    else if (_tag == OrderTextFieldRemark) {
        self.textView.text = STR_ORDER_REMARK;
        self.textView.placeholder = STR_ORDER_REMARK;
        self.msgLabel.text = [NSString stringWithFormat:@"%ld字以内", (long)MAX_LENGTH_OF_REMARK];
    }
    else if (_tag == OrderTextFieldRoadRemark) {
        self.msgLabel.text = [NSString stringWithFormat:@"%ld字以内", (long)MAX_LENGTH_OF_REMARK];
//        self.msgLabel = [NSString stringWithFormat:@"%ld字以内", (long)MAX_LENGTH_OF_REMARK];
    }
    self.textView.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.textView canBecomeFirstResponder]) {
        [self.textView becomeFirstResponder];
    }
}

-(void)initNavBar
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_back"] style:UIBarButtonItemStylePlain target:self action:@selector(pressedBackButton:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(pressedSaveButton:)];
}

-(void)pressedBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)pressedSaveButton:(id)sender
{
    // 路单信息可以为空，非必填
    if (self.tag == OrderTextFieldRoadRemark) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(onTextDidChanged:text:)]) {
            [self.delegate onTextDidChanged:self.tag text:self.textView.text];
        }
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (self.textView.text.length > 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(onTextDidChanged:text:)]) {
            [self.delegate onTextDidChanged:self.tag text:self.textView.text];
        }
        
        if (_completeHandle) {
            _completeHandle(self.tag, self.textView.text.trim);
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self showAlertView:ALERT_MSG_NO_CONTENT];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (_tag == OrderTextFieldItinerary || _tag == InquireRoughtTravel) {
        if (range.location >= MAX_LENGTH_OF_ROUGH_ITINERY) {
            [self showAlertView:ALERT_MSG_ROUGH_ITINERARY_CONTENT_LIMIT];
            return NO;
        }
    }
    else if (_tag == OrderTextFieldRemark || _tag == OrderTextFieldRoadRemark) {
        if (range.location >= MAX_LENGTH_OF_REMARK) {
            [self showAlertView:ALERT_MSG_REMAKR_CONTENT_LIMIT];
            return NO;
        }
    }
    else if (range.location >= 30)
    {
        [self showAlertView:ALERT_MSG_CONTENT_LIMIT];
        return NO;
    }
    return YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
