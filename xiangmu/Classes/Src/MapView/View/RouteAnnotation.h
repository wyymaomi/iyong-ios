//
//  RouteAnnotation.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKMapComponent.h>

@interface RouteAnnotation : BMKPointAnnotation

@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) NSInteger degree;

//获取该RouteAnnotation对应的BMKAnnotationView
- (BMKAnnotationView*)getRouteAnnotationView:(BMKMapView *)mapview;

@end
