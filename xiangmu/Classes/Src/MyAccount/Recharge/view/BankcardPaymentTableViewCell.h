//
//  BankcardPaymentTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankcardPaymentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bankcardLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;
+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

@end
