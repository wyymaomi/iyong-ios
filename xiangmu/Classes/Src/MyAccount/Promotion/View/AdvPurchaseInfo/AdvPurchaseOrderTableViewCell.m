//
//  AdvPurchaseOrderTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPurchaseOrderTableViewCell.h"

@implementation AdvPurchaseOrderTableViewCell

//-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    
//    if (self) {
//        
//        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdvPurchaseOrderTableViewCell class]) owner:nil options:nil] lastObject];
//
//    }
//    
//    return self;
//}

+(CGFloat)getCellHeight
{
    return 52;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.leftBarView.frame = CGRectMake(15, 5, ViewWidth-15-50, self.height-10);
    
//    self.deleteIcon.left = self.leftBarView.right;
//    self.deleteIcon.frame = CGRectMake(self.leftBarView.right, self.leftBarView.top, 32, self.height-10);
    
    self.deleteButton.frame = CGRectMake(self.leftBarView.right, self.leftBarView.top, 32, self.height-10);
}

- (void)setOrder:(AdPurchaseOrder *)order
{
    if (order == nil) {
        return;
    }
    
    _order = order;
    
    
    self.dateLabel.text = [_order displayDate];
//    self.dateLabel.text = _order.date;
    
    self.timeSegmentLabel.text = _order.timeSegment;
    
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@", [_order.price stringValue]];//[_order.price stringValue];
    
    
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    [self.deleteIcon addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickDeleteButton)]];

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
