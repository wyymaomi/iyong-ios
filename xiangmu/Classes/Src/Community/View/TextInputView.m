//
//  TextInputView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "TextInputView.h"

@implementation TextInputView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
//    self.vechileQuantityTextField.keyboardType = UIKeyboardTypeNumberPad;
//
//    self.priceTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    
    
    [self.cancelButton whiteStyle];
    
    [self.okButton blueStyle];
    
    [self.okButton addTarget:self action:@selector(onOKButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.cancelButton.cornerRadius = 5;
    
    self.okButton.cornerRadius = 5;
    
    self.vechileQuantityTextField.delegate = self;
    
    self.priceTextField.delegate = self;
    
    // UITextField 文字偏移
//    
//    UILabel *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 12)];
//    leftView.backgroundColor = [UIColor clearColor];
//    
//    self.priceTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.priceTextField.leftView = leftView;
//    
//    self.vechileQuantityTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.vechileQuantityTextField.leftView = leftView;
//    
//    self.vechileModelTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.vechileModelTextField.leftView = leftView;
//    
//    self.remarkTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.remarkTextField.leftView = leftView;
    
//    self.priceTextField.textInputMode = UITextFieldInp
    
}

- (void)onOKButtonClick
{
    
    NSString *price = self.priceTextField.text.trim;
    NSString *vechileModel = self.vechileModelTextField.text.trim;
    NSString *vechileQuantity = self.vechileQuantityTextField.text.trim;
    NSString *remark = self.remarkTextField.text.trim;
    
    if (self.completeHandle) {
        
        self.completeHandle(vechileModel, vechileQuantity, price, remark);
    
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    [self becomeFirstResponder];
    
    [self.priceTextField resignFirstResponder];
    [self.vechileModelTextField resignFirstResponder];
    [self.vechileQuantityTextField resignFirstResponder];
    [self.remarkTextField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
//    if (self.tag == OrderTextFieldOriginPrice || self.tag == OrderTextFieldDispatchPrice || self.tag == OrderTextFieldSettlementPrice || self.tag == InquireVechileNumber) {
        if ((range.location == 0) && ([string isEqualToString:@"0"]))
        {
            return NO;
        }
        
        if ((range.location == 0) && ([string isEqualToString:@"."])) {
            return NO;
        }
        
        //限制非数字的字符
        //        if ((string.length > 0 && ![string isMatchedByRegex:@"^[0-9]*$"]) /*|| [[string trim] intValue] > 99*/) {
        //            return NO;
        //        }
//    }
    
    
    return YES;
}

@end
