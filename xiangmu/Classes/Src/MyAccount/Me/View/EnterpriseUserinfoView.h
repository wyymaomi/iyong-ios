//
//  EnterpriseUserinfoView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/16.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterpriseUserinfoView : UIView

@property (weak, nonatomic) IBOutlet UIButton *companyPublishButton;
@property (weak, nonatomic) IBOutlet UILabel *companyPublishLabel;

@property (weak, nonatomic) IBOutlet UIButton *waitButton;
@property (weak, nonatomic) IBOutlet UILabel *waitLabel;

@property (weak, nonatomic) IBOutlet UIButton *providerInfoButton;
@property (weak, nonatomic) IBOutlet UILabel *providerInfoLabel;

@property (weak, nonatomic) IBOutlet UILabel *providerPromotionLabel;
@property (weak, nonatomic) IBOutlet UIButton *providerPromotionButton;

@end
