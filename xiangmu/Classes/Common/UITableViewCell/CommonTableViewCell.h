//
//  CommonTableViewCell.h
//  xiangmu
//
//  Created by David kim on 16/9/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *badgeValueBtn;

+ (CGFloat)getCellHeight;

@end
