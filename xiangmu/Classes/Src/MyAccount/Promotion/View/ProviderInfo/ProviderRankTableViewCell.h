//
//  ProviderRankTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderRankTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *todayRankLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayViewNumDiffLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayArrowLabel;
@property (weak, nonatomic) IBOutlet UILabel *yesterdayArrowLabel;
@property (weak, nonatomic) IBOutlet UILabel *yesterdayRankLabel;
@property (weak, nonatomic) IBOutlet UILabel *yesterdayViewNumDiffLabel;

@property (weak, nonatomic) IBOutlet UILabel *todayAreaRankLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayAreaViewNumDiffLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayAreaArrowLabel;
@property (weak, nonatomic) IBOutlet UILabel *yesterdayAreaRankLabel;
@property (weak, nonatomic) IBOutlet UILabel *yesterdayAreaViewNumDiffLabel;
@property (weak, nonatomic) IBOutlet UILabel *yesterdayAreaArrowLabel;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+(CGFloat)getCellHeight;

@end
