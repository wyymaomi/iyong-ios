//
//  InvoiceCompanyinfoTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"

@interface InvoiceCompanyinfoTableViewCell : CustomTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *taxpayerNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *registAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bankAccountLabel;

@end
