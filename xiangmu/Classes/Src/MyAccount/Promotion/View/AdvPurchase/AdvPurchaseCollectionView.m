//
//  AdvPurchaseCollectionView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/4/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPurchaseCollectionView.h"
#import "AdvPurchaseCollectionViewCell.h"

static NSString *advPurchaseColectionViewwCellId = @"AdvPurchaseCollectionViewCell";

@implementation AdvPurchaseCollectionView

-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    
    if (self) {
        
        [self initViews];
        
    }
    
    return self;
}

- (void)initViews
{
    
    [self registerClass:[AdvPurchaseCollectionViewCell class] forCellWithReuseIdentifier:advPurchaseColectionViewwCellId];
    
//    [self registerClass:[AdvPurchaseCollectionViewCell class] forCellWithReuseIdentifier:@"AdvPurchaseCollectionViewCell"];
    
    self.delegate=self;
    self.dataSource=self;
//    self.scrollEnabled=NO;
}

#pragma mark - UICollectionView Delegate method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;

}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    AdvPurchaseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:advPurchaseColectionViewwCellId forIndexPath:indexPath];
    
    cell.titleLabel.text = [NSString stringWithFormat:@"第%ld版位", indexPath.row+1];
    
    cell.purchaseButton.tag = indexPath.row;
    
    [cell.purchaseButton addTarget:self action:@selector(onClickPurchaseButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}

- (void)onClickPurchaseButton:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if (_customDelegate && [_customDelegate respondsToSelector:@selector(onClickItem:tag:)]) {
        [_customDelegate onClickItem:button.tag tag:self.tag];
    }
}

////cell点击变色
//-(BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
///cell点击变色
//-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger row = indexPath.row;
    
//    if ( _customDelegate && [_customDelegate respondsToSelector:@selector(onClickItem:tag:)]) {
//        //        [_customDelegate onSelectItem:row];
//        [_customDelegate onClickItem:row tag:self.tag];
//    }
    
    //    NSDictionary *cellDict = self.subviewCellList[indexPath.row];
    //    NSString *viewController = cellDict[@"vc"];
    //
    //    if ([viewController isEqualToString:@""]) {
    //        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该系统暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    //        [alertView show];
    //    }
    //    else {
    //        if (_customDelegate && [_customDelegate respondsToSelector:@selector(onGotoPage:title:)]) {
    //            [_customDelegate onGotoPage:viewController title:cellDict[@"text"]];
    //        }
    //    }
    
}



@end
