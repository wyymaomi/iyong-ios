//
//  AddressTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AddressTableViewCell.h"

@implementation AddressTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"AddressTableViewCellIdentifier";
    AddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

+(CGFloat)getCellHeight
{
    return 40*Scale;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.addressTextField.left = 60*Scale;
    self.addressTextField.width = self.frame.size.width-self.addressTextField.left;
    
//    if (.row == AddressTableViewRowTypeTime) {
////        cell.iconImageView.image = [UIImage imageNamed:@"icon_time"];
//        self.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-21*Scale)/2, 21*Scale, 21*Scale);
////        cell.addressTextField.placeholder = @"请选择用车日期";
////        cell.addressTextField.text = self.vechileOrderHttpMessage.carDateTime;
//    }
//    if (indexPath.row == AddressTableViewRowTypeCarType) {
//        cell.iconImageView.image = [UIImage imageNamed:@"icon_car_type"];
//        cell.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-17.5*Scale)/2, 22*Scale , 17.5*Scale);
//        cell.addressTextField.placeholder = @"请选择用车类型";
//        if (self.vechileOrderHttpMessage.carType == VechileTypeComfortable) {
//            cell.addressTextField.text = @"舒适型7座";
//        }
//        else if (self.vechileOrderHttpMessage.carType == VechielTypeLuxure) {
//            cell.addressTextField.text = @"豪华型5座";
//        }
//        else if (self.vechileOrderHttpMessage.carType == VechileTypeCommercial) {
//            cell.addressTextField.text = @"商务型5座";
//        }
//        
//    }
//    if (indexPath.row == AddressTableViewRowTypeStartAddress) {
//        cell.iconImageView.image = [UIImage imageNamed:@"icon_address_up"];
//        cell.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-27*Scale)/2, 19*Scale, 27*Scale);
//        cell.addressTextField.placeholder = @"请输入上车地点";
//        cell.addressTextField.text = self.vechileOrderHttpMessage.startAddress;
//    }
//    if (indexPath.row == AddressTAbleViewRowTypeEndAddress) {
//        cell.iconImageView.image = [UIImage imageNamed:@"icon_address_down"];
//        cell.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-27*Scale)/2, 19*Scale, 27*Scale);
//        cell.addressTextField.placeholder = @"请输入下车地点";
//        cell.addressTextField.text = self.vechileOrderHttpMessage.endAddress;
//    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
