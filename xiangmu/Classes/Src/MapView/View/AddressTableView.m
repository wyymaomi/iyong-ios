//
//  AddressTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AddressTableView.h"
#import "AddressTableViewCell.h"
#import "LocationSearchViewController.h"
#import "VechileOrderManager.h"

@implementation AddressTableView

-(id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
//    _vechileOrderHttpMessage = [VechileOrderHttpMessage new];
    
//    _vechileOrderHttpMessage = [VechileOrderHttpMessage new];
//    _vechileOrderHttpMessage.carType = 1;
    
    self = [super initWithFrame:frame style:style];
    self.delegate = self;
    self.dataSource = self;
    self.scrollEnabled = NO;
    self.timeType = 1;

    
    
    return self;
}

-(VechileOrderHttpMessage*)vechileOrderHttpMessage
{
    if (!_vechileOrderHttpMessage) {
        _vechileOrderHttpMessage = [VechileOrderHttpMessage new];
       
    }
    return _vechileOrderHttpMessage;
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.vechileOrderHttpMessage.useCarMode == CarModeImmediate) {
        if (indexPath.row == 0) {
            return 0;
        }
    }
    return [AddressTableViewCell getCellHeight];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"AddressTableViewCellIdentifier";
    
    AddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [AddressTableViewCell cellWithTableView:tableView];
//        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AddressTableViewCell) owner:self options:nil] lastObject];
    }
    
    CGFloat cellHeight = [AddressTableViewCell getCellHeight];
    
    if (indexPath.row == AddressTableViewRowTypeTime) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon_time"];
        cell.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-21*Scale)/2, 21*Scale, 21*Scale);
        cell.addressTextField.placeholder = @"请选择用车日期";
        cell.addressTextField.text = self.vechileOrderHttpMessage.carDateTime;
        
        if (self.vechileOrderHttpMessage.useCarMode == CarModeImmediate) {
            cell.hidden = YES;
        }
    }
    if (indexPath.row == AddressTableViewRowTypeCarType) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon_car_type"];
        cell.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-17.5*Scale)/2, 22*Scale , 17.5*Scale);
        cell.addressTextField.placeholder = @"请选择用车类型";
        
        NSArray *carDescList = [[VechileOrderManager sharedInstance] getVechileDescTypeList];
        
        for (CarDescModel *carDesc in carDescList) {
            
            if ([carDesc.type integerValue] == self.vechileOrderHttpMessage.carType) {
                cell.addressTextField.text = [NSString stringWithFormat:@"%@%@", carDesc.carTypeName, carDesc.seatName];
                break;
            }
            
        }
        

        
    }
    
    if (indexPath.row == AddressTableViewRowTypeStartAddress) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon_address_up"];
        cell.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-27*Scale)/2, 19*Scale, 27*Scale);
        cell.addressTextField.placeholder = @"请输入上车地点";
        cell.addressTextField.text = self.vechileOrderHttpMessage.startLocation;
    }
    if (indexPath.row == AddressTAbleViewRowTypeEndAddress) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon_address_down"];
        cell.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-27*Scale)/2, 19*Scale, 27*Scale);
        cell.addressTextField.placeholder = @"请输入下车地点";
        cell.addressTextField.text = self.vechileOrderHttpMessage.endLocation;
    }
    
    if (indexPath.row == AddressTAbleViewRowTypeTelephone) {
        
        cell.iconImageView.image = [UIImage imageNamed:@"icon_car_mobile"];
        cell.iconImageView.frame = CGRectMake(15*Scale, (cellHeight-27*Scale)/2, 19*Scale, 27*Scale);
        cell.addressTextField.placeholder = @"请输入用车人联系电话(可选填)";
        cell.addressTextField.text = self.vechileOrderHttpMessage.telephone;
//        cell.addressTextField.enabled=YES;
        
    }
    
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectItemOnTag:)]) {
        [self.customDelegate didSelectItemOnTag:indexPath.row];
    }
    
//    if (indexPath.row == AddressTableViewRowTypeStartAddress) {
//
//    }
    
}





@end
