//
//  HttpConstants.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef HttpConstants_h
#define HttpConstants_h

#pragma mark HTTP服务器域名

//#if TEST
////#define HOST_NAME @"http://192.168.1.199:7070"
////#define HOST_NAME @"http://192.168.1.199:8080"
////#define HOST_NAME @"http://app.chinaiyong.com:8080"
//#define HOST_NAME @"http://139.196.211.227:7070"
//#else
////#define HOST_NAME @"http://139.196.211.227:7070"
//#define HOST_NAME @"http://app.chinaiyong.com:8080"
////#define HOST_NAME @"http://192.168.1.199:7070"
//#endif



#ifdef TEST
//#define HOST_NAME @"http://app.chinaiyong.com:8080"
//#define HOST_NAME @"http://192.168.1.153:7070"
//#define HOST_NAME @"http://app.chinaiyong.com:8080"
#define HOST_NAME @"http://test.chinaiyong.com:7070"
#define WEB_PAGE_URL @"http://test.chinaiyong.com:7070/iyong-web/views/"
//#define HOST_NAME @"https://chinaiyong.com:7443"
//#define HOST_NAME @"http://139.196.211.227:7070"
#else
#define HOST_NAME @"http://app.chinaiyong.com:8080"
#define WEB_PAGE_URL @"http://app.chinaiyong.com:8080/iyong-web/views/"
//#define HOST_NAME @"http://app.chinaiyong.com:8080"
#endif

#define RESOURSE_WEB_PAGE @"http://iyong-public.oss-cn-shanghai.aliyuncs.com/driver/document/"

#define DRIVER_EXPLAIN_HTML @"driver_explain.html"  //驾驶员服务要求和问题解决

#define DRIVER_RULE_HTML @"driver_rule.html"  //法律声明及隐私政策

#define PAY_EXPLAIN_HTML @"pay_explain.html"   //支付说明

#define PRICE_EXPLAIN_HTML @"views/travel/price_explain.html" // 价格说明

#define INVOICE_EXPLAIN_HTML @"invoice_explain.html" // 开票说明

#define RECHARGE_EXPLAIN_HTML @"http://iyong-public.oss-cn-shanghai.aliyuncs.com/iyong/recharge/recharge_question.html" // 充值说明

#define DRAW_MONEY_EXPLAIN_HTML @"http://iyong-public.oss-cn-shanghai.aliyuncs.com/iyong/withdraw/withdraw_question.html" // 提现说明


#define BAIDU_SUGGESTION_PLACE_API @"http://api.map.baidu.com/place/v2/suggestion"
#define BAIDU_PLACE_API @"http://api.map.baidu.com/place/v2/detail"
#define BAIDU_ROUTE_API @"http://api.map.baidu.com/routematrix/v2/driving"


#define TENCENT_MAP_API_URL  @"http://apis.map.qq.com/ws/place/v1/suggestion"

#define HTTP_BASE_URL [NSString stringWithFormat:@"%@/%@", HOST_NAME, @"iyong-web"] //@"http://app.chinaiyong.com:8080/iyong-web"
#define UPMP_NOTICE_URL [NSString stringWithFormat:@"%@/%@", HOST_NAME, @"iyong-pay/pay/receive_payinfo"]//@"http://app.chinaiyong.com:8080/iyong-pay/pay/receive_payinfo"

#define PROVIDER_DETAIL_SHARE_API_URL @"share/provider.html?companyId="
#define EXPERIENCE_DETAIL_SHARE_API_URL @"share/experience.html?experienceId="

//http://test.chinaiyong.com:7070/iyong-web/share/provider.html?companyId=8a99969259450cbf01594900f1160004
#define PROVIDER_DETAIL_SHARE_URL [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PROVIDER_DETAIL_SHARE_API_URL]
#define EXPERIENCE_DETAIL_SHARE_URL [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_DETAIL_SHARE_API_URL]

#define APIWithBaseUrl(API_URL) [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, API_URL]

#pragma mark - HTTP response code
typedef NS_ENUM(NSInteger, HTTP_RESPONSE_CODE) {
    NO_DISPATCH_PRICE = 1485, // 没有输入接单价
    NO_AVAILABEL_MOENY = 1145,// 没有足够的信用余额或余额
//    STATUS_COPY       = 1302,
    STATUS_OK         = 1000,//数据下载成功
    STATUS_FAIL       = 1001,//失败
    NETWORK_FAILED    = 1,//网络问题
    LOGIN_TIMEOUT     = 2
};

//#define REQUEST_TIME_OUT 30

#pragma mark - 接口API

#define OSS_SIGN_API @"oss/sign"
//登录
#define LOGIN_API @"login/login"
//注册
#define Register_API @"login/register"
#define REGISTER_VERIFY_CODE_API @"sms/get_register_captcha" // 注册获取验证码
#define VERIFY_CODE_API @"sms/get_captcha" // 获取验证码
#define RESET_PWD_API @"login/reset_password" // 忘记密码
#define CHECK_VERSION_API @"version/check_version" // 检测版本号
#define SET_DEVICE_ID_API @"user/set_deviceid" // 设置用户deviceId
#define LOGIN_REGISTER_WEIBO_API @"login/register_weibo" // 注册微博账户
#define LOGIN_REGISTER_WECHAT_API @"login/register_wechat"
#define LOGIN_REGISTER_QQ_API @"login/register_qq"
#define LOGIN_WEHAT_API @"login/login_wechat"
#define LOGIN_QQ_API @"login/login_qq"
#define LOGIN_WEIBO_API @"login/login_weibo"
#define HX_USER_INFO_API @"im/huanxin_current_user_info"// 查询当前登录商户聊天账号信息

#pragma mark - 手势密码相关API
#define SET_GESTURE_PWD_API @"user/set_signPwd" // 设置手势密码
#define HAS_GESTURE_PWD_API @"user/has_signPwd" // 是否已设置手势密码
#define VALIDATE_GESTURE_PWD_API @"user/validate_signPwd" // 验证手势密码(请求模式与登录相同)
#define GET_FINANCE_API @"company/get_finance" // 获取财务信息接口
#define GET_FINANCE_URL_API @"finance/get_finance_h5_url" // 获取金融服务H5链接
#define IDCARD_CERTIFICATION_API @"company/idcard_certificate" // 实名认证接口
#define COMPANY_CERTIFICATION_API @"company/company_certification" // 企业认证接口
#define GET_COMPANY_CERTIFICATION_API @"company/get_company_certification" // 获取企业认证接口
#define UPDATE_COMPANY_CERTIFICATION_API @"company/update_company_certification" // 更新企业认证接口
#define BIND_MAILBOX_API @"company/save_email" // 绑定邮箱
#define ADD_BANKCARD_API @"company/add_bankcard" // 添加银行卡
#define DELETE_BANKCARD_API @"company/delete_bankcard" // 删除银行卡
#define BANKCARD_LIST_API @"company/bankcard_list" // 查询公司银行卡列表
//#define RECHARGE_BILL_NO_API @"company/save_rechargeorder" // 商户系统生成订单号
#define RECHARGE_BILL_NO_API @"company/save_ips_rechargeorder" // 商户系统生成订单号
#define RECHARGE_CALLBACK_API @"company/save_payrecord" // 保存充值订单交易记录
#define SAVE_RECHARGE_ORDER @"user/recharge/save_recharge_order" // 保存充值订单
#define WITHDRAW_API @"company/withdraw_application" // 提现申请
#define WITHDRAW_LIST_API @"company/withdraw_list" // 提现记录
#define WITHDRAW_RATE_API @"company/withdraw_rate" // 提现费率
#define CREDIT_AMOUNT_LIST_API @"company/credit_amount_list" // 信用额度列表
#define CREDIT_APPLY_PERSON_API @"company/credit_apply_person" // 个人信用额度申请
#define CREDIT_APPLY_COMPANY_API @"company/credit_apply_company" // 公司信用额度申请

#pragma mark - 广告列表查询
#define ADV_LIST_URL @"ad/adinfo_list" // 广告列表查询
#define BANNER_ADV_LIST_URL @"adhome/get_banner_ad_info"


#pragma mark - 社区相关API

#define COMMUNITY_PARTNER_IDLECAR_LIST_API @"community/partner_idlecar_list" // 合作伙伴空闲车辆列表
#define COMMUNITY_PUBLISH_API @"community/save"
#define COMMUNITY_ENQUIRY_API @"community/businesscircle/enquiry"// 发布询价单
#define COMMUNITY_ENQUIRY_LIST_API @"community/businesscircle/enquiry_list" // 查询询价列表
#define COMMUNITY_BID_API @"community/businesscircle/bid" // 出价
#define COMMUNITY_MY_BUSINESS_CIRCLE_API @"community/businesscircle/my_enquiry_list" // 我的业务圈
#define COMMUNITY_END_ENQUIRY_API @"community/businesscircle/end_enquiry" // 结束询价
#define COMMUNITY_HAS_BID_API @"community/businesscircle/has_bid" // 对当前询价单是否已报过价
#define COMMUNITY_GENERATE_ORDER_API @"community/businesscircle/generate_order" // 生成订单

//车辆列表
#define CAR_LIST_API @"car/list"
//添加车辆
#define CAR_SAVE_API @"car/save"
//删除车辆
#define CAR_Delete_API @"car/delete"
//编辑车辆里面的数据显示
#define CAR_Query_API @"car/query"
//编辑车辆
#define CAR_Update_API @"car/update"
#define CAR_CERTIFICATION_API @"car/car_certification" // 车辆认证
#define GET_CAR_CERTIFICATION_API @"car/get_car_certification" // 根据车辆ID查询车辆认证信息

//添加合作伙伴 搜索
#define Partner_Query_API @"company/query"
#define Partner_WaitPass_COUNT_API @"partner/wait_pass_count"
//添加合作伙伴 申请
#define Partner_Apply_API @"partner/apply"
//添加合作伙伴 待通过列表
#define Partner_ApplyList_API @"partner/applylist"
//添加合作伙伴 待通过中通过
#define Partner_Accept_API @"partner/accept"
//添加合作伙伴 待通过中不通过
#define Partner_Ignore_API @"partner/ignore"
//我的合作伙伴 列表
#define Partner_List_API @"partner/list"
#define PARTNER_DELETE_API @"partner/delete"
//账单
#define Order_Partner_list_API @"partner/partner_list"
//我的信息
#define My_Detail_API @"user/query"
//调度员列表
#define Diaodu_List_API @"dispatcher/list"
//添加调度员
#define Diaodu_Add_API @"dispatcher/add_dsp"
//添加调度员
#define Diaodu_Add_API @"dispatcher/add_dsp"
//删除调度员
#define Diaodu_Delete_API @"dispatcher/delete"
//编辑调度员
#define Diaodu_Update_API @"dispatcher/update"
//修改用户信息
#define UserInfo_Update_API @"user/update"
//添加推荐人
#define Company_Add_Inviter @"company/add_inviter"
//推荐人列表
#define Company_Inviter_List @"company/inviter_list"

#pragma mark - 下载图片接口
#define DONWLOAD_IMAGE_API_ENCRYPT @"transfer/download"
#define DOWNLOAD_IMAGE_API_NOENCRYPT @"transfer/download_logo"

#pragma mark - 营业查询接口
#define SALES_LIST_API @"business/list" // 营业查询列表
#define SALES_SUMMARY_API @"business/summary" // 营业查询
#define SEND_REPORT_API @"business/send_report" // 以邮件发送营业报表

#pragma mark - 交易记录接口
#define TRADE_RECORD_LIST_API @"transaction/list" // 交易列表
#define TRADE_SUMMARY_API @"transaction/summary" // 月度交易汇总

#pragma mark - 登录密码更改接口
//#define LOGIN_PWD_CHANGE_API @"user/update_psd" // 登录密码更新
#define LOGIN_PWD_CHANGE_API @"user/update_password" // 登录密码更新

#pragma mark - 支付密码接口
#define PAY_PWD_HAS_API @"user/has_payPwd"// 是否有支付密码接口
#define PAY_PWD_SET_API @"user/set_payPwd" // 设置支付密码接口
#define PAY_PWD_UPDATE_API @"user/update_payPassword" // 更新支付密码接口
#define PAY_PWD_VALID_API @"user/validate_payPwd" // 验证支付密码接口
#define PAY_PWD_RESET_API @"user/reset_payPassword" // 支付密码重置接口

#pragma mark - 账单管理接口
#define BILL_LIST_COLLECTED_API @"bill/collected_list" //已收款账单列表接口
#define BILL_LIST_UNCOLLECTED_API @"bill/uncollected_list" // 未收款账单列表接口
#define BILL_LIST_PAID_API @"bill/alreadyPaid_list" // 已付款账单列表
#define BILL_LIST_UNPAID_API @"bill/unpaid_list" // 未付款账单列表
#define CREDIT_UNREPAYMENT_LIST_API @"bill/norepayment_list" // 未还款信用账单列表接口
#define CREDIT_PAY_API @"bill/repayment" // 信用账单还款

#pragma mark - 订单接口
#define ORDER_PARTNER_LIST_API @"partner/list"// 派单对象接口
#define ORDER_SMS_API @"order/sms_send" // 订单发送短信接口
#define ORDER_COPY_API @"order/order_copy" // 复制订单信息接口
#define ORDER_DELETE_API @"order/delete" // 订单删除接口
#define ORDER_IS_SELF_DISPATCH_API @"order/is_selfbuilt_order" // 是否为自建自派订单

#define ORDER_SUBMIT_API @"order/submit"
#define ORDER_SAVE_API @"order/save_order" // 保存订单信息
#define ORDER_DISPATCH_API @"order/submit_chain" // 保存派单信息
#define ORDER_SAVE_CARINFO_API @"order/save_carinfo" // 安排车辆：选择车辆
#define ORDER_ARRANGE_VECHILE_API @"order/submit_carinfo" // 安排车辆：司机已确认
#define ORDER_SAVE_WAYBILL_API @"order/submit_waybill" // 保存路单信息
#define ORDER_BARGAIN_API @"order/submit_bargain" // 请求结算
#define ORDER_AGREE_PAY_API @"order/submit_agreePay" // 同意付款
#define ORDER_AGREE_ROLLBACK_CHAIN_API @"order/agree_rollback_chain" // 下游同意改派
#define ORDER_PAY_API @"order/submit_pay" // 付款

#define ORDER_UPDATE_ORDER @"order/update_order" // 修改订单信息
#define ORDER_ROLLBACK_CHAIN @"order/rollback_chain" // 派单改派
#define ORDER_ROLLBACK_CARINFO @"order/rollback_carinfo" // 派单改派
#define ORDER_UPDATE_WAYBILL @"order/update_waybill" // 修改路单
#define ORDER_ROLLBACK_BARGIN @"order/rollback_bargain" // 结算回滚

#define ORDER_LIST_API @"order/list" // 订单列表查询（车辆安排中：3，填写路单中：4）参数：status，time
#define ORDER_LIST_UNTREATED_API @"order/list/untreated" // 未处理订单列表查询
#define ORDER_LIST_STROKE_API @"order/list/stroke" // 行程中订单列表查询
#define ORDER_LIST_UNBARGAIN_API @"order/list/unbargain" // 待结算订单列表查询
#define ORDER_LIST_UNPAID_API @"order/list/unpaid" // 待付款订单列表查询
#define ORDER_LIST_COMPLETED_API @"order/list/completed" // 已完成订单列表查询

#define ORDER_DETAIL_API @"order/detail" // 订单详情
#define ORDER_COUNT_API @"company/order_count" // 公司订单统计查询

#define ORDER_DETAIL_BASE_API @"order/detail_order" // 订单基础信息
#define ORDER_DETAIL_CHAIN_API @"order/detail_chain"// 派单
#define ORDER_DETAIL_CARINFO_API @"order/detail_carinfo" // 安排车辆
#define ORDER_DETAIL_WAYBILL_API @"order/detail_waybill" // 路单
#define ORDER_DETAIL_BARGIN_API @"order/detail_bargain" // 结算

#define ONLINE_SERVICE_PROVIDER_LIST_API @"company/provider/online_provider_list" // 在线服务商列表（不需要加密）
#define SERVICER_FAVORITE_API @"company/provider/collection" // 收藏服务商
#define SERVICER_CANCEL_FAVORITE_API @"company/provider/cancel_collection" // 取消收藏服务商
#define MY_FAVORITE_SERVICER_API @"company/provider/my_collection" // 我的收藏
#define SERVICER_LIKE_API @"company/provider/thumbs_up" // 点赞服务商
#define SERVICER_SAVE_API @"company/provider/save_business" // 保存经营范围
#define SERVICER_GET_API @"company/provider/query_business" // 获取经营范围
#define EXPERIENCE_SAVE_API @"community/experience/publish_experience" // 发布用户体验
#define EXPERIENCE_LIST_API @"community/experience/experience_list" // 用户体验列表
#define EXPERIENCE_DETAIL_API @"community/experience/get_experience_detail" // 用车体验详情
#define EXPERIENCE_LIKE_API @"community/experience/thumbs_up" // 点赞评论
#define EXPERIENCE_COMMENT_LIST_API @"community/experience/comment_list" // 用户体验评论列表
#define EXPERIENCE_COMMENT_PUBLISH_API @"community/experience/save_comment" // 用户体验发表评论
#define GET_SERVICE_DETAIL_API @"company/provider/get_provider_detail" // 服务商详情
#define PROVIDER_COMMENT_LIST_API @"company/provider/comment_list" // 服务商评论列表
#define SAVE_PROVIDER_COMMENT_API @"company/provider/save_comment" // 服务商发表评论
#define GET_PROVIDER_DETAIL_API @"company/provider/get_provider_info" // 查询服务商（发布服务商用)
//#define PUBLISH_EXPERIENCE_API @"community/experience/publish_experience" // 发布用车体验
#define UPLOAD_PROVIDER_API @"company/provider/save_provider" // 发布服务商信息

#define HOME_PROVIDER_TYPE_LIST_API @"company/provider/homepage_provider_list" // 首页服务商列表（分类用）
#define HOME_PROVIDER_LIST_API @"adhome/get_ad_info"
//#define HOME_PROVIDER_LIST_API @"company/provider/homepage_provider_list" // 首页服务商列表
#define HOME_RECOMMENT_PROVIDER_LIST_API @"company/provider/recommend_provider_list" // 查看推荐服务商列表
#define HOME_EXPERIENCE_LIST_API @"community/experience/homepage_experience_list" // 首页用车体验列表

#define ADMAIN_SUM_LIST_API @"admain/adMain_sum_list" // 可售广告列表
#define ADMIN_LIST_API @"admain/adMain_list" // 可购广告详细信息
#define AD_PURCHASE_API @"adorder/order_ad" // 用户下单购买广告
#define SUM_CURRENT_PAYMENT_API @"adorder/sum_current_payment"// 用户当天消费统计
#define COMPANY_RAKING_API @"company/company_ranking_all" // 用户浏览量排名信息
#define PROVIDER_AD_CURRENT_API @"admain/list_user_ad_profit_current" // 用户当天广告收益
//#define PROVIDER_AD_ORDER_LIST @"/adorder/customer_ad_order_list" // 用户订单列表
//#define PROVIDER_AD_ORDER_DETAIL_LIST @"adorder/customer_ad_order_info" //用户订单详情
#define PROVIDER_AD_ORDER_LIST_API @"adorder/customer_ad_order_list_wd" // 用户广告订单列表带详情
#define PROVIDER_CANCEL_AD_ORDER_API @"adorder/cancel_ad_order"// 取消广告订单
#define PROVIDER_DELETE_AD_ORDER_API @"adorder/delete_ad_order" // 删除广告订单
#define PROVIDER_AD_ORDER_INFO @"adorder/customer_ad_order_info" // 广告订单信息
#define PROVIDER_PAY_ORDER_API @"adorder/pay_order" // 订单支付
#define PROVIDER_HAS_UNFINISH_ORDER_API @"adorder/has_unfinished_order" // 是否有未完成的订单

#pragma mark - 红包
#define GET_RED_PACK_API @"red_envelope/get_red_envelope" 
#define SAVE_RED_PACK_API @"red_envelope/save_red_envelope"

#pragma mark - 资讯
#define NEWS_LIST_API @"information/list"

#pragma mark - 绑定司机
#define BIND_DRIVER_API @"driver/binding/invite_driver"
#define BIND_DRIVER_LIST_API @"driver/binding/list_by_company"
#define UNBIND_DRIVER_API @"driver/binding/relieve_binding"
#define TRAVEL_LIST_API @"driver/travel/list"
#define CAR_TYPE_LIST_API @"travel/order/car_type_price_list"
#define PUBLISH_TRAVEL_API @"travel/order/publish"
#define GET_TRAVEL_ORDER_DETAIL_API @"travel/order/get_order"
#define TRAVEL_ORDER_PAY_API @"travel/order/order_pay"
#define MY_TRAVEL_ORDER_LIST_API @"travel/order/user/user_order_list"
#define THIRD_PAY_SUCCESS_API @"travel/order/third_paid"
#define CANCEL_CAR_ORDER_REMIND_API @"travel/order/user/cancel_order_remind"
#define CANCEL_CAR_ORDER_API @"travel/order/user/cancel_order"

#pragma mark - 发票管理
#define GET_INVOICE_SETTING_LIST_API @"user/invoice/invoice_setting_list"
#define ADD_NEW_INVOICE_SETTING_API @"user/invoice/add_invoice_setting"
#define UPDATE_INVOICE_SETTING_API @"user/invoice/update_invoice_setting"
#define GET_INVOICE_AVAILABE_AMOUNT_API @"user/invoice/get_available_amount"
#define ADD_INVOICE_API @"user/invoice/add_invoice"
#define GET_INVOICE_LIST_API @"user/invoice/invoice_list"

#pragma mark - 充值活动接口
#define RECHARGE_ACTIVITY_API @"user/recharge/recharge_activity_remind"

//#define EXPERIENCE_SAVE_API @"/community/experience/publish_experience" // 

#endif /* HttpConstants_h */
