//
//  MainTabBarController+EMConnection.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MainTabBarController.h"
#import <UserNotifications/UserNotifications.h>

//两次提示的默认间隔
//static const CGFloat kDefaultPlaySoundInterval = 3.0;
//static NSString *kMessageType = @"MessageType";
//static NSString *kConversationChatter = @"ConversationChatter";
//static NSString *kGroupName = @"GroupName";

@interface MainTabBarController (EMConnection)

- (void)jumpToChatList;

- (void)setupUntreatedApplyCount;

- (void)setupUnreadMessageCount;

- (void)networkChanged:(EMConnectionState)connectionState;

- (void)didReceiveLocalNotification:(UILocalNotification *)notification;

- (void)didReceiveUserNotification:(UNNotification *)notification;

- (void)playSoundAndVibration;

- (void)showNotificationWithMessage:(EMMessage *)message;

@end
