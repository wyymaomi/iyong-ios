//
//  DispatchCompanyTableViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyPartnerListViewController.h"
#import "HeaderPullRefreshTableView.h"
#import "MyPartnerTableViewCell.h"
#import "AddpartnerViewController.h"
#import "PartnerModel.h"
#import "PartnerDAO.h"
#import "AppConfig.h"

@interface MyPartnerListViewController ()<UITableViewDelegate,UITableViewDataSource,  PullRefreshTableViewDelegate, UIActionSheetDelegate, EmptyDataDelegate>

//@property (nonatomic, strong) HeaderPullRefreshTableView *tableView;
//@property (nonatomic, strong) NSArray *dataList;
@property (nonatomic, strong) PartnerDAO *partnerDAO;
@property (nonatomic, strong) NSMutableDictionary *imageDictionary;
@property (nonatomic, strong) NSString *mobile;


@end

@implementation MyPartnerListViewController

-(PartnerDAO*)partnerDAO
{
    if (_partnerDAO == nil) {
        _partnerDAO = [[PartnerDAO alloc] init];
    }
    return _partnerDAO;
}

//
//-(NSArray*)dataList
//{
//    if (IsNilOrNull(_dataList)) {
//        _dataList = [NSArray new];
//        
//    }
//    return _dataList;
//}

-(NSMutableDictionary*)imageDictionary
{
    if (_imageDictionary == nil) {
        _imageDictionary = [NSMutableDictionary new];
    }
    return _imageDictionary;
}

//-(HeaderPullRefreshTableView*)tableView
//{
//    if (_tableView == nil) {
//        _tableView = [[HeaderPullRefreshTableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
//        _tableView.delegate = self;
//        _tableView.dataSource = self;
//        _tableView.pullRefreshDelegate = self;
//        _tableView.hasFooterRefresh = YES;
//        _tableView.hasHeaderRefresh = YES;
//        _tableView.footerHidden = YES;
//        [self.view addSubview:_tableView];
//    }
//    return _tableView;
//}

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.source_from == SOURCE_FROM_ORDER_MANAGER) {
        self.title = @"派单对象";
    }
    else {
        self.title = @"我的合作伙伴";
    }

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"添加" style:UIBarButtonItemStyleDone target:self action:@selector(addPartner)];
    
    [self.view addSubview:self.headerPullRefreshTableView];
    
    self.emptyDataDelegate = self;
    
//    [self.tableView beginHeaderRefresh];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.headerPullRefreshTableView beginHeaderRefresh];
}


#pragma mark - pull refresh delegate method

-(double)getMaxUpdateTime:(NSArray*)dataList
{
    
    double maxUpdateTime = 0L;
    
    for (PartnerModel *partnerData in dataList) {
        if ([partnerData.updateTime doubleValue] > maxUpdateTime /*&& [partnerData.valid integerValue] == 1*/) {
            maxUpdateTime = [partnerData.updateTime doubleValue];
        }
    }
    return maxUpdateTime;
}

-(void)onHeaderRefresh;
{
    
    NSString *companyId = [[UserManager sharedInstance].userModel companyId];
    self.dataList = [self.partnerDAO getAllPartner:companyId];
    [self.headerPullRefreshTableView reloadData];
    for (NSInteger i = 0; i < self.dataList.count; i++) {
        PartnerModel *partnerModel = self.dataList[i];
        if (!IsStrEmpty(partnerModel.logoUrl)) {
            [self downloadPartnerImage:partnerModel.logoUrl];//:carModel.imgUrl];
        }
    }
    
    // 下载好友公司logo图片

    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_PARTNER_LIST_API];
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:[AppConfig currentConfig].partnerLastUpdateStr]];
    
#if 1

    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        
        StrongSelf
        [strongSelf.headerPullRefreshTableView endHeaderRefresh];
        strongSelf.loadStatus = LoadStatusFinish;
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        PartnerResponseModel *responseModel = [[PartnerResponseModel alloc] initWithDictionary:responseData error:nil];
        
        if (code_status == STATUS_OK) {
            id data = responseData[@"data"];
            if ([data isKindOfClass:[NSArray class]]) {
                
                
                if (!IsArrEmpty(responseModel.data)) {
                    // 增量更新
                    // 本地数据库更新
                    [strongSelf.partnerDAO updatePartnerList:responseModel.data];
                    strongSelf.dataList = [strongSelf.partnerDAO getAllPartner:companyId];
                    
                    // 本地缓存最新更新时间，取列表中最大的updateTime
                    double maxUpdateTime = [strongSelf getMaxUpdateTime:responseModel.data];
                    [AppConfig currentConfig].partnerLastUpdateStr = [NSString stringWithFormat:@"%.f", (double)maxUpdateTime];
                    [strongSelf.headerPullRefreshTableView reloadData];
                    
                    // 下载好友公司logo图片
                    for (NSInteger i = 0; i < strongSelf.dataList.count; i++) {
                        PartnerModel *partnerModel = strongSelf.dataList[i];
                        if (!IsStrEmpty(partnerModel.logoUrl)) {
                            [strongSelf downloadPartnerImage:partnerModel.logoUrl];//:carModel.imgUrl];
                        }
                    }
                }
            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        weakSelf.loadStatus = LoadStatusNetworkError;
        [weakSelf.headerPullRefreshTableView endHeaderRefresh];
        
    }];
#endif
}

-(void)downloadPartnerImage:(NSString*)imgUrl
{
    NSString *convertImgUrl = [imgUrl replaceAll:@"/" with:@"-"];
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            [strongSelf.imageDictionary setValue:imageData forKey:convertImgUrl];
            [strongSelf.headerPullRefreshTableView reloadData];
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

-(void)deletePartnerById:(NSString*)partnerId
{
    self.nextAction = @selector(deletePartnerById:);
    self.object1 = partnerId;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PARTNER_DELETE_API];
    NSString *parmas = [NSString stringWithFormat:@"partnerId=%@", partnerId];
    
    [self showHUDIndicatorViewAtCenter:@"正在删除合作伙伴，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"合作伙伴删除成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    NSString *companyId = [[UserManager sharedInstance].userModel companyId];
                    [strongSelf.partnerDAO deletePartnerById:partnerId companyId:companyId];
                    [strongSelf.headerPullRefreshTableView beginHeaderRefresh];
                }
            }];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
    }];
}

-(void)dial:(UIButton*)button
{
    NSInteger index = button.tag;
    PartnerModel *item = self.dataList[index];
    self.mobile = item.mobile.trim;
    if (!IsStrEmpty(self.mobile)) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil
                                      delegate:self
                                      cancelButtonTitle:@"取消"
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:self.mobile,nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
        [actionSheet showInView:self.view];
    }
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        NSString *num = [[NSString alloc] initWithFormat:@"tel://%@",self.mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]];
    }
}

#pragma mark - 添加合作伙伴
-(void)addPartner
{
    AddpartnerViewController *addpartnerVC = [[AddpartnerViewController alloc]init];
    [self.navigationController pushViewController:addpartnerVC animated:YES];
}

#pragma mark - UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.source_from == SOURCE_FROM_ORDER_MANAGER) {
        return self.dataList.count + 1;
    }
    return self.dataList.count;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyPartnerTableViewCell *cell = [MyPartnerTableViewCell cellWithTableView:tableView];
    cell.logoImageView.cornerRadius = 0;
    cell.logoImageView.frame = CGRectMake(cell.logoImageView.origin.x, (cell.contentView.size.height-cell.logoImageView.frame.size.height)/2, cell.logoImageView.frame.size.width, cell.logoImageView.frame.size.height);
    
    if (self.source_from == SOURCE_FROM_ORDER_MANAGER) {
        if (indexPath.row == 0) {
            cell.nickNameLabel.text = @"自派";
            cell.companyLabel.text = @"";
            cell.dialButton.hidden = YES;
            cell.nickNameLabel.frame = CGRectMake(cell.nickNameLabel.frame.origin.x, 0, cell.nickNameLabel.frame.size.width, 70);
        }
        else {
            NSInteger index = indexPath.row - 1;
            PartnerModel *item = self.dataList[index];
            cell.nickNameLabel.text = item.nickname;
            cell.companyLabel.text = item.companyName;
            cell.dialButton.tag = index;
            [cell.dialButton addTarget:self action:@selector(dial:) forControlEvents:UIControlEventTouchUpInside];
            
            if (IsStrEmpty(item.logoUrl)) {
                cell.logoImageView.image = [UIImage imageNamed:@"icon_iYong"];
            }
            else {
                NSString *key = [item.logoUrl replaceAll:@"/" with:@"-"];
                if (self.imageDictionary[key] == nil) {
                    cell.logoImageView.image = [UIImage imageNamed:@"icon_iYong"];
                }
                else {
                    NSData *data = self.imageDictionary[key];
                    item.imgData = data;
                    UIImage *image = [UIImage imageWithData:data];
                    cell.logoImageView.image = image;//[ImageUtil imageWithImage:image scaledToSize:imageSize];
                    cell.logoImageView.cornerRadius = cell.logoImageView.frame.size.width/2;
                }
            }
        }
    }
    else {
        NSInteger index = indexPath.row;
        PartnerModel *item = self.dataList[index];
        cell.nickNameLabel.text = item.nickname;
        cell.companyLabel.text = item.companyName;
        cell.dialButton.tag = index;
        [cell.dialButton addTarget:self action:@selector(dial:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (IsStrEmpty(item.logoUrl)) {
            cell.logoImageView.image = [UIImage imageNamed:@"icon_iYong"];
        }
        else {
            NSString *key = [item.logoUrl replaceAll:@"/" with:@"-"];
            if (self.imageDictionary[key] == nil) {
                cell.logoImageView.image = [UIImage imageNamed:@"icon_iYong"];
            }
            else {
                NSData *data = self.imageDictionary[key];
                item.imgData = data;
                UIImage *image = [UIImage imageWithData:data];
                cell.logoImageView.image = image;//[ImageUtil imageWithImage:image scaledToSize:imageSize];
                cell.logoImageView.cornerRadius = cell.logoImageView.frame.size.width/2;
            }
        }
        
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.source_from == SOURCE_FROM_ORDER_MANAGER) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (self.source_from == SOURCE_FROM_ORDER_MANAGER) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if (indexPath.row == 0) {
            if (self.companyDelegate && [self.companyDelegate respondsToSelector:@selector(onSelfDispatch)]) {
                [self.companyDelegate onSelfDispatch];
            }
        }
        else {
            if (self.companyDelegate && [self.companyDelegate respondsToSelector:@selector(onGetDispatchCompany:)]) {
                NSInteger index = indexPath.row - 1;
                PartnerModel *partnerModel = self.dataList[index];
                [self.companyDelegate onGetDispatchCompany:partnerModel];
            }
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
  
}



//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

//删除操作
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.source_from == SOURCE_FROM_ORDER_MANAGER) {
        if (indexPath.row == 0) {
            return NO;
        }
    }
    return YES;
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //获取到相应cell
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSInteger index = indexPath.row;
        if (self.source_from == SOURCE_FROM_ORDER_MANAGER) {
            index = indexPath.row - 1;
        }
        PartnerModel *model = self.dataList[index];
        WeakSelf
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否删除" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [weakSelf deletePartnerById:model.id];
            }
        }];
    }
}

#pragma mark - EmptyDataDelegate method

- (NSString*)emptyDetailText;
{
    return @"您还没有添加合作伙伴\n赶快去添加合作伙伴吧";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
