//
//  NewRechargeViewController+ThirdPay.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewRechargeViewController.h"

@interface NewRechargeViewController (ThirdPay)

-(void)saveRechargeOrder:(NSUInteger)rechargeMode rechargeAmount:(CGFloat)rechargeAmount;

@end
