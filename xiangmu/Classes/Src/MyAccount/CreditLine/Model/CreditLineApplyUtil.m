//
//  CreditLineApplyUtil.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/11.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CreditLineApplyUtil.h"

@implementation CreditLineApplyUtil

+(BOOL)validateCreditLineApply:(CreditHttpMessage*)httpMessage type:(NSUInteger)type errorMsg:(NSString**)errorMsg;
{
    if (IsStrEmpty(httpMessage.applicant)) {
//        [self showAlertView:@"请输入申请人姓名"];
        *errorMsg = @"请输入申请人姓名";
        return NO;
    }
    if (type == 0 && IsStrEmpty(httpMessage.companyName)) {
//        [self showAlertView:@"请输入公司名"];
//        return;
        *errorMsg = @"请输入公司名";
        return NO;
    }
    
    // 判断手机号是否合法
    if (IsStrEmpty(httpMessage.mobile)) {
//        [self showAlertView:@"请输入手机号"];
//        return;
        *errorMsg = @"请输入手机号";
        return NO;
    }
    
    if (httpMessage.mobile.length != 11) {
        *errorMsg = @"请输入11位手机号";
        return NO;
    }
    
    NSString *mobileNoRegex = @"1[0-9]{10,10}";
    NSPredicate *mobileNoTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNoRegex];
    BOOL validateOK = [mobileNoTest evaluateWithObject:httpMessage.mobile];
    
    if (!validateOK) {
        *errorMsg = @"手机号格式错误";
        return NO;
    }

    
    
    if (IsStrEmpty(httpMessage.amountId)) {
//        [self showAlertView:@"请输入额度"];
//        return;
        *errorMsg = @"请输入额度";
        return NO;
    }
    
    return YES;
}

@end
