//
//  NewBankTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewBankTableViewCell.h"

@implementation NewBankTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath
{
    static NSString *ID = @"NewBankTableViewCell";
    NewBankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        
        [cell.addBankcardButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_radio_unselected"] forState:UIControlStateNormal];
        [cell.addBankcardButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_radio_selected"] forState:UIControlStateSelected];
    }
    return cell;
}

+(CGFloat)getCellHeight;
{
    return 62;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
