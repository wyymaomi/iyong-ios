//
//  CurrentPromotionDetailFooterView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentPromotionDetailFooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *clickNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *viewNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

@end
