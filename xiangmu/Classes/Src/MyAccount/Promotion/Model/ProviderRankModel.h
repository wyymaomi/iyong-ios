//
//  ProviderRankModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

//@protocol

@interface ProviderRankModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *thumbsupDiff;
@property (nonatomic, strong) NSString<Optional> *thumbsupDiffArea;
@property (nonatomic, strong) NSString<Optional> *thumbsupRanking;
@property (nonatomic, strong) NSString<Optional> *thumbsupRankingArea;
@property (nonatomic, strong) NSString<Optional> *viewDiff;
@property (nonatomic, strong) NSString<Optional> *viewDiffArea;
@property (nonatomic, strong) NSString<Optional> *viewRanking;
@property (nonatomic, strong) NSString<Optional> *viewRankingArea;

@end

@interface ProviderRankSumModel : JSONModel

@property (nonatomic, strong) ProviderRankModel *rankingToday;
@property (nonatomic, strong) ProviderRankModel *rankingYestoday;

@end
