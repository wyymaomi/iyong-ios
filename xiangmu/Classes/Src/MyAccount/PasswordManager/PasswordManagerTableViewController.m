//
//  MimaManagerTableViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PasswordManagerTableViewController.h"
#import "MyUtil.h"
#import "PasswordSetViewController.h"
#import "GesturePasswordController.h"

@interface PasswordManagerTableViewController ()

@end

@implementation PasswordManagerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"密码管理";
    [self.view addSubview:self.groupTableView];
    self.groupTableView.rowHeight = DefaultTableRowHeight;
}

-(void)gotoBack
{
    [self.navigationController popViewControllerAnimated:YES];
    self.tabBarController.tabBar.hidden=NO;
}
//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([self.groupTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.groupTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.groupTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.groupTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}
//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger level = [[UserManager sharedInstance].userModel.level integerValue];
    if (level == 1) {
        return 3;
    }
    else if (level == 2) {
        return 2;
    }
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellid=@"cellid";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    NSArray *nameArray=@[@"设置登录密码",@"设置手势密码",@"设置交易密码"];
    cell.textLabel.text=nameArray[indexPath.row];
    
    cell.textLabel.textColor = DefaultTableRowTitleTextColor;
    cell.textLabel.font = DefaultTableRowTitleFont;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10*Scale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.00001f;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 40;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row==0) {
        PasswordSetViewController *mimaVC=[[PasswordSetViewController alloc]init];
        mimaVC.type=MimaTypeLoad;
        [self.navigationController pushViewController:mimaVC animated:YES];
    }
    else if (indexPath.row==1)
    {
        GesturePasswordController *viewController = [[GesturePasswordController alloc] init];
        viewController.source_from = SOURCE_FROM_ABOUT_ME;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else
    {
        PasswordSetViewController *mimaVC=[[PasswordSetViewController alloc]init];
        mimaVC.type=MimaTypeJiaoyi;
        [self.navigationController pushViewController:mimaVC animated:YES];
        
    }
//    [self.navigationController pushViewController:mimaVC animated:YES];
}


@end
