//
//  InvoiceSettingItemCellTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/1.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceSettingItemCellTableViewCell.h"

@implementation InvoiceSettingItemCellTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"InvoiceSettingItemCellTableViewCellID";
    InvoiceSettingItemCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
    }
    return cell;
}


+(CGFloat)getCellHeight
{
    return 70;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
