//
//  NSObject+Swizzle.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Swizzle)

- (void)swizzleMethod:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector;

@end
