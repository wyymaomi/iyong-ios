//
//  HomeViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/27.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomeViewController.h"
#import "HomepageTableViewCell.h"
#import "AppConfig.h"
#import "HomeExperienceTableViewCell.h"
#import "BaseViewController+Advertisement.h"
#import "AdvModel.h"
#import "HomeViewController+NetworkRequest.h"
#import "ServiceProviderListController.h"
//#import "CommentDetailViewController.h"
#import "ExperienceDetailViewController.h"
#import "UserExpericenViewModel.h"
//#import "BaseWebViewController.h"
#import "BaseWebpageViewController.h"
#import "HomeViewController+Location.h"
#import "MyUtil.h"
#import "BaseViewController+Advertisement.h"

static NSString *HomeProviderCellIdentifier = @"HomeProviderCellIdentifier";
static NSString *HomeExperienceCellIdentifier = @"HomeExperienceCellIdentifier";

@interface HomeViewController ()<HomepageCollectionViewDelegate,PullRefreshTableViewDelegate,SDCycleScrollViewDelegate,HomeExperienceCollectionViewDelegate>



@end

@implementation HomeViewController

//-(NSMutableDictionary*)providerListDict
//{
//    if (!_providerListDict) {
//        _providerListDict = @{@{@"row":[NSNumber numberWithInteger:HomeTableRowCommercial],
//                                @"list": self.viewMode
//    }
//    return _providerListDict;
//}

- (HomeViewModel*)viewModel
{
    if (!_viewModel) {
        _viewModel = [[HomeViewModel alloc] init];
    }
    return _viewModel;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        
        DLog(@"WYY HomeViewController init");
        
        self.isFirstIn = YES;
        
        _lock = dispatch_semaphore_create(1);
        
        
            

        
    }
    return self;
}

-(void)loadView
{
    [super loadView];
    
    DLog(@"WYY loadView");
    
//    [self setNavTitleView];
//    [self.view addSubview:self.headerPullRefreshTableView];
//    self.headerPullRefreshTableView.delegate = self;
//    self.headerPullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.headerPullRefreshTableView.pullRefreshDelegate = self;
//    
//    [self.headerPullRefreshTableView registerClass:[HomepageTableViewCell class] forCellReuseIdentifier:HomeProviderCellIdentifier];
//    //
//    //    [self.headerPullRefreshTableView registerClass:[HomeExperienceTableViewCell class] forCellReuseIdentifier:HomeExperienceCellIdentifier];
//    //    [self.headerPullRefreshTableView reloadData];
//    
//    //    [self setNavTitleView];
//    
//    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DLog(@"WYY HomeViewController viewDidLoad");
    // Do any additional setup after loading the view from its nib.
    
    [self setNavTitleView];
    [self.view addSubview:self.headerPullRefreshTableView];
    self.headerPullRefreshTableView.delegate = self;
    self.headerPullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.headerPullRefreshTableView.pullRefreshDelegate = self;
    
    [self.headerPullRefreshTableView registerClass:[HomepageTableViewCell class] forCellReuseIdentifier:HomeProviderCellIdentifier];


    [self showAdvFirstTime];
    
    
    [self fetchAllData];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLoginSuccessNotification) name:kLoginSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGpsLocation) name:kOnGpsLocationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLoginNotification) name:kNeedLoginNotification object:nil];
    
//    [self doSomething];

}

//-(void)showMaskView
//{
//    s
//}

-(void)showTopWindow
{
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    window.windowLevel = UIWindowLevelStatusBar;
    window.rootViewController = self;
    
    window.backgroundColor = [UIColor clearColor];
    
//    window.alpha = 0.7;
    
    UIView *maskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    maskView.backgroundColor = [UIColor blackColor];
    maskView.alpha = 0.7;
    [window addSubview:maskView];
    
//    _maskView= ({
//        UIView *view = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
//        view.backgroundColor = [UIColor blackColor];
//        view.alpha = 0.7;
//        view;
//    });
    
    [window makeKeyAndVisible];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //设置电池条为白色
//    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.isFirstIn) {
        [self showAdvFirstTime];
        self.isFirstIn = NO;
    }
    
//        [self showAdvFirstTime];
    
//    if (self.) {
//        <#statements#>
//    }
    
//    [self showAdv]
    
    [_bannerView setupTimer];
    
//    self.navigationController.statusBarStyle = UIStatusBarStyleLightContent;
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x333333)] forBarMetrics:UIBarMetricsDefault];
    
    //设置电池条为白色
//    [self setNeedsStatusBarAppearanceUpdate];
//    self.setNeedsStatusBarAppearanceUpdate()
//    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
//    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x333333);
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_bannerView invalidateTimer];
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forBarMetrics:UIBarMetricsDefault];
    
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent]; 
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x333333);
    
//    self.navigationController.navigationBar.backgroundColor = NAVBAR_BGCOLOR;
}

//- (UIStatusBarStyle)preferredStatusBarStyle {
//    return UIStatusBarStyleLightContent;
//}
//
//// 状态栏是否隐藏
//- (BOOL)prefersStatusBarHidden
//{
//    return NO;
////    return self.isStatusBarHidden;
//}


-(void)receiveLoginNotification
{
    [self onDismiss];
    self.advDisplayType = AdvDisplayTypePause;
    [self presentLogin];
}

-(void)receiveLoginSuccessNotification
{
    if (self.advDisplayType == AdvDisplayTypePause) {
        [self showAdv:NO];
    }
}

-(void)onGpsLocation
{
//    g currentConfig].defaultCityCode = city.cityID;
    NSString *defaultAreaCode = [AppConfig currentConfig].defaultCityCode;
    // 定位成功后 当前城市和定位城市一样时不重新发送请求
    if ([self.currentCityCode isEqualToString:defaultAreaCode]) {
        return;
    }
//    self.gpsCityShortname = [MyUtil getCityShortnameFromCityCode:defaultAreaCode];
    [self setNavTitleView];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_, <#unsigned long flags#>), <#^(void)block#>)
    [self fetchAllData];
}

- (void)fetchAllData
{
    
    self.downloadCount=0;
    
    
    NSString *defaultAreaCode = [AppConfig currentConfig].defaultCityCode;
    
    self.currentCityCode = defaultAreaCode;
    
    self.gpsCityShortname = [MyUtil getCityShortnameFromCityCode:defaultAreaCode];
    
    
    DLog(@"WYY fetchAllData");
    
    self.requestMaxCount = 4;
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 查询banner
        [self fetchBannerAdvList];
//    });
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        // 查询推荐
        [self fetchRecommendList];
        
//    });
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        // 查询全国服务商数据
        [self fetchServiceProviderByPageIndex:1 areaCode:@"" requestType:0];
        
//    });
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        // 查询gps城市服务商数据
        [self fetchServiceProviderByPageIndex:1 areaCode:self.currentCityCode requestType:0];
        
//    });


//    [self fetchExperienceList];


//    for (NSUInteger i = BusinessTypeWedding; i < BusinessTypeTruck+1; i++) {
//        [self fetchServiceProviderByType:1 areaCode:self.currentCityCode businessType:i];
//    }

    
}

//- (void)doSomething {
//    NSMutableArray *collection = @[].mutableCopy;
//    for (int i = 0; i < 10e6; ++i) {
//        NSString *str = [NSString stringWithFormat:@"hi + %d", i];
//        [collection addObject:str];
//    }
//    DLog(@"finished!");
//}


- (void)onHeaderRefresh
{
    [self fetchAllData];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setNavTitleView
{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor clearColor];
    UIImage *imageForButton = [UIImage imageNamed:@"icon_gps"];
    [button setImage:imageForButton forState:UIControlStateNormal];
    // 设置文字
    NSString *cityName = [AppConfig currentConfig].defaultCityName;//[AppConfig currentConfig].gpsCityName;
    NSString *buttonTitleStr = cityName;
    if (cityName.length > 4) {
        buttonTitleStr = [cityName substringToIndex:4];
    }
//    NSString *buttonTitleStr = [cityName substringToIndex:4];
    [button setTitle:buttonTitleStr forState:UIControlStateNormal];
    button.titleLabel.font = FONT(15);
    [button setTitleColor:NAVBAR_TITLE_TEXT_COLOR forState:UIControlStateNormal];
    CGSize buttonTitleLabelSize = [buttonTitleStr sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}];
    CGSize buttonImageSize = imageForButton.size;
    button.frame = CGRectMake(0, 0, buttonImageSize.width + buttonTitleLabelSize.width+5, buttonImageSize.height);
    //    button.frame = CGRectMake(0, 0, 100, 100);
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
//    [button setImageEdgeInsets:UIEdgeInsetsMake(0.0, -20, 0.0, 0.0)];
    [button addTarget:self action:@selector(didSelectCity:) forControlEvents:UIControlEventTouchUpInside];
    //    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//    button.backgroundColor = [UIColor redColor];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    
//    self.title = @"爱佣出行";
    [self.navigationItem setTitle:@"爱佣出行"];
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
////    view.backgroundColor = [UIColor yellowColor];
////    view.backgroundColor =
//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.width, 30)];
//    titleLabel.text = @"爱佣出行";
//    titleLabel.textColor = [UIColor blackColor];
//    titleLabel.font = BOLD_FONT(20);
//    titleLabel.textAlignment = UITextAlignmentCenter;
////    [self.navigationItem.titleView addSubview:titleLabel];
//    [view addSubview:titleLabel];
//    self.navigationItem.titleView = view;
    
//    UIImage *rightImage = [[UIImage imageNamed:@"icon_advertisement"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icon_advertisement"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(onClickAdvIcon)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithstyle:UIBarButtonItemStyleDone target:self action:@selector(onClickAdvIcon)];
    
}

-(void)onClickHomeTypeCell:(UIButton*)button
{
    DLog(@"button.tag = %ld", (long)button.tag);
//    [button setHighlighted:YES];
    ServiceProviderListController *viewController = [[ServiceProviderListController alloc] init];
    viewController.hidesBottomBarWhenPushed = YES;
    viewController.areaCode = [AppConfig currentConfig].defaultCityCode;
   
    NSString *cityShortName = [MyUtil getCityShortnameFromCityCode:viewController.areaCode];
    //        viewController.title = [NSString stringWithFormat:@"%@地区服务商", cityShortName];
    viewController.title = [MyUtil homeDisplayCityName:cityShortName];
    NSDictionary *dictionary = homeTypeList()[button.tag];
    viewController.businessType = [dictionary[@"type"] integerValue];
    viewController.title = dictionary[@"title"];
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void)onClickPlusButton:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    ServiceProviderListController *viewController = [[ServiceProviderListController alloc] init];
    viewController.hidesBottomBarWhenPushed = YES;
    
    NSInteger tag = button.tag;
//    NSString *shortCityName = [MyUtil ]
    if (tag == HomeTableRowServicer) {
        viewController.areaCode = @"";
//        viewController.title = @"全国地区服务商";
    }
//    if (tag == HomeTableRowBeijing) {
//        viewController.areaCode = BJ_CITY_CODE;
//    }
//    if (tag == HomeTableRowShanghai) {
//        viewController.areaCode = SH_CITY_CODE;
//    }
//    if (tag == HomeTableRowHangzhou) {
//        viewController.areaCode = HZ_CITY_CODE;
//    }
//    if (tag == HomeTableRowShenzhen) {
//        viewController.areaCode = SZ_CITY_CODE;
//    }
    if (tag >= HomeTableRowGpsCity) {
        viewController.areaCode = [AppConfig currentConfig].defaultCityCode;
    }
    
    if ([viewController.areaCode isEqualToString:@""]) {
        viewController.title = @"全国服务商";
    }
    else {
        NSString *cityShortName = [MyUtil getCityShortnameFromCityCode:viewController.areaCode];
//        viewController.title = [NSString stringWithFormat:@"%@地区服务商", cityShortName];
        viewController.title = [MyUtil homeDisplayCityName:cityShortName];
    }
    
    
    if (tag > HomeTableRowGpsCity) {
        viewController.businessType = [self getBusinessType:tag];
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

-(NSInteger)getBusinessType:(NSUInteger)row
{
    switch (row) {
        case HomeTableRowCharter:
            return BusinessTypeCharter;
        case HomeTableRowWedding:
            return BusinessTypeWedding;
        case HomeTableRowAirport:
            return BusinessTypeAirport;
        case HomeTableRowBus:
            return BusinessTypeCompanyBus;
        case HomeTableRowGoods:
            return BusinessTypeGoods;
        case HomeTableRowTruck:
            return BusinessTypeTruck;
        case HomeTableRowTravel:
            return BusinessTypeTravel;
        case HomeTableRowCommercial:
            return BusinessTypeCommercial;
        default:
            break;
    }
    return BusinessTypeNone;
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return TABLE_ROW_COUNT;
//    return self.tableViewRows;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == HomeTableRowBanner) {
        return 175*Scale;
    }
    if (indexPath.row == HomeTableRowBusinessType) {
        return 150*Scale;
    }
    
    if (indexPath.row == HomeTableRowRecommend && self.viewModel.recommendList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowServicer && self.viewModel.providerList.count == 0) {
        return 0;
    }
//    if (indexPath.row == HomeTableRowExperience && self.viewModel.experienceList.count == 0) {
//        return 0;
//    }
//    if (indexPath.row == HomeTableRowShenzhen && self.viewModel.shenzhenList.count == 0) {
//        return 0;
//    }
//    if (indexPath.row == HomeTableRowHangzhou && self.viewModel.hangzhouList.count == 0) {
//        return 0;
//    }
//    if (indexPath.row == HomeTableRowBeijing && self.viewModel.beijingList.count == 0) {
//        return 0;
//    }
//    if (indexPath.row == HomeTableRowShanghai && self.viewModel.shanghaiList.count == 0) {
//        return 0;
//    }
    
    
    if (indexPath.row == HomeTableRowGpsCity && self.viewModel.gpsCityList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowWedding && self.viewModel.weddingList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowTravel && self.viewModel.travelList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowTruck && self.viewModel.truckList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowGoods && self.viewModel.goodsList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowAirport && self.viewModel.airportList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowCharter && self.viewModel.charteredList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowCommercial && self.viewModel.commercialList.count == 0) {
        return 0;
    }
    if (indexPath.row == HomeTableRowBus && self.viewModel.busServiceList.count == 0) {
        return 0;
    }
//    if (indexPath.row == HomeTableRowBus && self.viewModel.) {
//        <#statements#>
//    }
//    if (indexPath.row == HomeTableRowAirport && self.viewModel.airportList.count == 0) {
//        return 0;
//    }
    
    
    return 220*Scale;
    
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%lu%lu", (long)indexPath.section, (long)indexPath.row];
    
//    NSString *CellIdentifier = 
    // 广告条
    if (indexPath.row == HomeTableRowBanner) {
        
        static NSString *CellIdentifier = @"HomeTableBannerCellIdentifier";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            _bannerView = [[SDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 175*Scale)];
//            _bannerView.autoScrollTimeInterval = 3.8;
            _bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
            _bannerView.delegate = self;
            _bannerView.displayType = SDCycleScrollViewDisplayTypeBanner;
            _bannerView.currentPageDotColor =  NAVBAR_BGCOLOR;//UIColorFromRGB(0x09a892);//[UIColor whiteColor];
            _bannerView.downloadFromAliyunOSS = YES;
            _bannerView.placeholderImage = [UIImage imageNamed:@"img_placeholder"];
            [cell.contentView addSubview:_bannerView];
            
        }
        
        NSMutableArray *advImageList = [NSMutableArray new];
        
        for (AdvModel *advModel in self.viewModel.advList) {
            [advImageList addObject:advModel.imageUrl];
        }
        
        _bannerView.aliyunOSSImageArray = advImageList;
        
        return cell;
        
    }
    // 分类
    if (indexPath.row == HomeTableRowBusinessType) {
        
        static NSString *CellIdentifier = @"HomeTableBusinessTypeCellIdentifier";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            
            cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 315/2*Scale)];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            NSInteger number = 8;
            NSInteger startX = 22;
            NSInteger startY = 15;//titleLabel.bottom+17;
            NSInteger imgWidth = 70*Scale, imgHeight = 59*Scale;
            NSInteger separatorX = (ViewWidth-startX * 2 - imgWidth * 4)/3,separatorY = 11.5;
            
//            NSArray *imgName = @[@"]
            
            for (NSInteger i = 0; i < number; i++) {
                
                NSDictionary *dictionary = homeTypeList()[i];//serviceTypeList()[i];
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setImage:[UIImage imageNamed:dictionary[@"home_icon"]] forState:UIControlStateNormal];
                [button setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
//                [button setBackgroundImage:[UIImage imageNamed:dictionary[@"home_icon"]] forState:UIControlStateNormal];
                button.frame = CGRectMake(startX + (imgWidth + separatorX) * (i%4), startY+(imgHeight+separatorY)*(i/4), imgWidth, imgHeight);
                button.adjustsImageWhenHighlighted = YES;
//                button.showsTouchWhenHighlighted = YES;
//                [button setHighlighted:YES];
//                [button setShowsTouchWhenHighlighted:YES];
                [button addTarget:self action:@selector(onClickHomeTypeCell:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:button];
                button.tag = i;//[dictionary[@"type"] integerValue];
                
                if (i%4<3) {
                    UIView *view = [UIView new];
                    CGFloat separatorLineStartY = startY + (imgHeight+separatorY)*(i/4)+10*Scale;
                    view.frame = CGRectMake(button.right+separatorY/2, separatorLineStartY, 1, 75/2*Scale);
                    view.backgroundColor = [UIColor lightGrayColor];
                    [cell.contentView addSubview:view];
                }
//                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(startX + (imgWidth + separatorX) * (i % 4), startY + (imgHeight + separatorY) * (i / 4), imgWidth, imgHeight)];
//                NSString *imgName = [NSString stringWithFormat:@"%@_gray", dictionary[@"icon"]];
//                imageView.image = [UIImage imageNamed:imgName];
//                NSString *hightLightName = [NSString stringWithFormat:@"%@_blue", dictionary[@"icon"]];
//                imageView.highlightedImage = [UIImage imageNamed:hightLightName];
//                [cell.contentView addSubview:imageView];
//                [imageView setHighlighted:NO];
//                imageView.tag = i+1;
                
//                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(startX+(imgWidth+separatorX)*(i%4), imageView.bottom+5, imgWidth, 20)];
//                label.font = FONT(11);
//                label.textAlignment = UITextAlignmentCenter;
//                label.text = dictionary[@"title"];//nameArray[i];
//                label.highlightedTextColor = NAVBAR_BGCOLOR;
//                label.textColor = UIColorFromRGB(0xBEBEBE);
//                label.tag = (i+1)*1000;
//                [cell.contentView addSubview:label];
//                [label setHighlighted:NO];
            }
            
            
        }
    
        return cell;
        
    }
    
    
//    if (indexPath.row == HomeTableRowExperience) {
//        
//        
//        HomeExperienceTableViewCell *cell = (HomeExperienceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:HomeExperienceCellIdentifier forIndexPath:indexPath];
//        
////        if (!cell) {
////            cell = [[HomeExperienceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HomeExperienceCellIdentifier];
////        }
//        
//            if (self.viewModel.experienceList.count == 0) {
//                cell.hidden = YES;
//                return cell;
//            }
//            cell.collectionView.customDelegate = self;
//            cell.dataList = self.viewModel.experienceList;
//            [cell.plusButton addTarget:self action:@selector(onClickExperiencePlusButton:) forControlEvents:UIControlEventTouchUpInside];
//            return cell;
////        }
//
//    }
    else {
//        static NSString *CellIdentifier = @"HomepageTableViewCellID";
//        HomepageTableViewCell *cell = (HomepageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        if (!cell) {
//            cell = [[HomepageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//        }
        
        static NSString *CellIdentifier = @"HomeProviderCellIdentifier";
        
        HomepageTableViewCell *cell = (HomepageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:HomeProviderCellIdentifier forIndexPath:indexPath];
        
        
        
//        NSMutableArray *dataList;
        
//        if (indexPath.row == ) {
//            <#statements#>
//        }
        
//        NSMutableArray *dataList;
//        switch(indexPath.row)
//        {
//            case HomeTableRowServicer:
//                break;
//            case HomeTableRowRecommend:
//                break;
//            case HomeTableRowGpsCity:
//                break;
//            case HomeTableRowWedding:
//                break;
//        }
        
        
//        cell initDataList:indexPath.row dataList:<#(NSMutableArray *)#>
        
//        if (!cell) {
//            cell =  (HomepageTableViewCell*)[[HomepageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HomeProviderCellIdentifier];
//        }

//        [cell initTitleView:indexPath.row];
        
//        if (indexPath.row == HomeTableRowServicer) {
//            cell.dataList = self.viewModel.providerList;
//        }
//        if (indexPath.row == HomeTableRowRecommend) {
//            cell.dataList = self.viewModel.recommendList;
//        }
//        if (indexPath.row == HomeTableRowBeijing) {
//            cell.dataList = self.viewModel.beijingList;
//            cell.titleLabel.text = [MyUtil homeDisplayCityName:@"北京"];
//        }
//        if (indexPath.row == HomeTableRowShanghai) {
//            cell.dataList = self.viewModel.shanghaiList;
//            cell.titleLabel.text = [MyUtil homeDisplayCityName:@"上海"];
//        }
//        if (indexPath.row == HomeTableRowHangzhou) {
//            cell.dataList = self.viewModel.hangzhouList;
//            cell.titleLabel.text = [MyUtil homeDisplayCityName:@"杭州"];
//        }
//        if (indexPath.row == HomeTableRowShenzhen) {
//            cell.dataList = self.viewModel.shenzhenList;
//            cell.titleLabel.text = [MyUtil homeDisplayCityName:@"深圳"];
//        }
        
        [cell initDataList:indexPath.row dataList:[self getProviderList:indexPath.row]];
        
        if (indexPath.row == HomeTableRowGpsCity) {
//            cell.dataList = self.viewModel.gpsCityList;
            cell.titleLabel.text = [MyUtil homeDisplayCityName:self.gpsCityShortname];
            
        }
        
        cell.plusButton.tag = indexPath.row;
        cell.collectionView.tag = indexPath.row;
        [cell.plusButton addTarget:self action:@selector(onClickPlusButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.disclosureButton addTarget:self action:@selector(onClickPlusButton:) forControlEvents:UIControlEventTouchUpInside];
        cell.collectionView.customDelegate = self;
        
        return cell;
    }
    
    return nil;
    
    
}

-(NSMutableArray*)getProviderList:(NSInteger)row
{
//    NSMutableArray *dataList;
    switch (row) {
        case HomeTableRowRecommend:// 推荐
            return self.viewModel.recommendList;
        case HomeTableRowServicer:
            return self.viewModel.providerList;
        case HomeTableRowGpsCity:
            return self.viewModel.gpsCityList;
        case HomeTableRowWedding:
            return self.viewModel.weddingList;
        case HomeTableRowTravel:
            return self.viewModel.travelList;
        case HomeTableRowCommercial:
            return self.viewModel.commercialList;
        case HomeTableRowAirport:
            return self.viewModel.airportList;
        case HomeTableRowBus:
            return self.viewModel.busServiceList;
        case HomeTableRowCharter:
            return self.viewModel.charteredList;
        case HomeTableRowGoods:
            return self.viewModel.goodsList;
        case HomeTableRowTruck:
            return self.viewModel.truckList;
            
        default:
            break;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    ServiceProviderViewModel *viewModel = self.dataList[indexPath.section];
//    OnlineServiceModel *data = viewModel.serviceModel;
//    ProviderDetailViewController *viewController = [[ProviderDetailViewController alloc] init];
//    viewController.companyId = data.companyId;
//    viewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:viewController animated:YES];
    
    //    viewController.
    //    [self gotoPage:@"ProviderDetailViewController"];
}

- (void)onClickExperiencePlusButton:(id)sender
{
    [self gotoPage:@"UserExperienceListViewController"];
}

- (void)onClickItem:(NSInteger)row tag:(NSInteger)tag
{
    NSMutableArray *dataList = [self getProviderList:tag];
    OnlineServiceModel *data;
    data = dataList[row];
    ProviderDetailViewController *viewController = [[ProviderDetailViewController alloc] init];
    viewController.companyId = data.companyId;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)onClickExperienceItem:(NSInteger)index;
{
    UserExpericenViewModel *viewModel = [[UserExpericenViewModel alloc] init];
    viewModel.serviceModel = self.viewModel.experienceList[index];
    ExperienceDetailViewController *viewController = [[ExperienceDetailViewController alloc] init];
    viewController.experienceViewModel = viewModel;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index;
{
    AdvModel *advModel = self.viewModel.advList[index];
    
    if (IsStrEmpty(advModel.linkUrl.trim)) {
        if (IsStrEmpty(advModel.companyId)) {
            return;
        }
        else {
            NSString *companyId = advModel.companyId;
            ProviderDetailViewController *viewController = [[ProviderDetailViewController alloc] init];
            viewController.companyId = companyId;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        return;
    }

//    [self gotoWebpage:advModel.linkUrl];
    
    [self openWebView:advModel.linkUrl];
    
//    BaseWebpageViewController *viewController = [[BaseWebpageViewController alloc] init];
//    viewController.url = advModel.linkUrl;
//    viewController.enter_type = ENTER_TYPE_PUSH;
//    viewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:viewController animated:YES];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y ) {
//        <#statements#>
    }
}




@end
