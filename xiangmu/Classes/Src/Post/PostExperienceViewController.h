//
//  PostExperienceViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, TableViewRow)
{
    TableViewRowPhotoHeader = 0,
    TableViewRowPickerPhoto,
//    TableViewRowCommentHeader,
    TableViewRowCommentContent
};

typedef NS_ENUM(NSInteger, CompanyTableRow) {
    CompanyTableRowPhotoExampleHeader = 0,
    CompanyTableRowPhotoExample,
    CompanyTableRowPhotoHeader,
    CompanyTableRowPickerPhoto,
//    CompanyTableRowServicetypeHeader,
    CompanyTableRowServiceType,
//    CompanyTableRowCommentHeader,
    CompanyTableRowCommentContent
};

typedef NS_ENUM(NSInteger, TableType)
{
    TableTypeUserExperience,
    TableTypeCompanyBusiness
};

//typedef NS_ENUM(NSInteger, TableViewRowHeight)
//{
//    TableViewRowHeight
//};

@interface PostExperienceViewController : BaseViewController

@property (nonatomic, strong) UITextView *contentTextView; // 发布体验内容

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tpkeyboardAvoidingTableView;

@property (nonatomic, assign) NSInteger type;// 显示类型

@property (nonatomic, assign) NSInteger businessType; // 经营范围类型

//@property (nonatomic, strong) NSMutableArray *businessTypeArray;

//@property (nonatomic, strong) NSMutableArray *titleArray;


@end
