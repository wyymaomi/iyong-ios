//
//  ExperienceDetailViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/7.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ExperienceDetailViewController.h"

@interface ExperienceDetailViewController (NetworkRequest)

- (void)addLikeNumber;

- (void)fetchFollowCommentList:(NSString*)time;

- (void)uploadComment:(NSString*)content;

-(void)fetchExperienceDetail;

@end
