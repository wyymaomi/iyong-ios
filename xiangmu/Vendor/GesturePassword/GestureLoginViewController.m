//
//  GestureLoginViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "GestureLoginViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "HyperlinksButton.h"
#import "UserDefaults.h"
#import "NSString+Encrypt.h"
#import "NetworkManager.h"
//#import "GesturePasswordController.h"


@interface GestureLoginViewController ()<LoginDelegate>
{
//    NSString *_password;
}

@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) GesturePasswordView * gesturePasswordView;
@property (nonatomic, strong) HyperlinksButton *accountLoginButton;// 用户名账号登录
@property (nonatomic, strong) UILabel *userNameLabel;// 用户名

@end

@implementation GestureLoginViewController

-(HyperlinksButton*)accountLoginButton
{
    if (IsNilOrNull(_accountLoginButton)) {
        HyperlinksButton *button = [HyperlinksButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(20, self.view.frame.size.height - 100, ViewWidth - 40, 34);
        [button setTitle:@"用户名账号登录" forState:UIControlStateNormal];
        button.titleLabel.font = FONT(14);
        [button setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
//        [button setTitleColor:UIColorFromRGB(0x28548d) forState:UIControlStateNormal];
        button.backgroundColor = [UIColor clearColor];
        [button setLineColor:NAVBAR_BGCOLOR];
//        [button setLineColor:UIColorFromRGB(0x28548d)];
        [button addTarget:self action:@selector(onUsernameLogin) forControlEvents:UIControlEventTouchUpInside];
        _accountLoginButton = button;
    }
    return _accountLoginButton;
}

-(GesturePasswordView*)gesturePasswordView
{
    if (_gesturePasswordView == nil) {
        _gesturePasswordView = [[GesturePasswordView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    }
    return _gesturePasswordView;
}

-(UILabel*)userNameLabel
{
    if (_userNameLabel == nil) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 20)];
        label.backgroundColor = [UIColor whiteColor];
    }
    return _userNameLabel;
}

#pragma mark - life cycle

-(id)init
{
    if (self = [super init]) {
        self.title = @"手势密码登录";
        bPopupLoginView = NO;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
//    bPopupLoginView = NO;
    
    [self verify];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - 验证手势密码
- (void)verify{
    [self.gesturePasswordView.tentacleView setRerificationDelegate:self];
    [self.gesturePasswordView.tentacleView setStyle:1];
    self.gesturePasswordView.state.font = FONT(20);
    self.gesturePasswordView.state.text = [UserManager sharedInstance].userModel.username;//[[UserManager sharedInstance] userName];
    [self.gesturePasswordView setGesturePasswordDelegate:self];
    [self.view addSubview:self.gesturePasswordView];
    [self.view addSubview:self.accountLoginButton];
    
}

#pragma mark - 判断是否已存在手势密码
- (BOOL)exist{
    //    KeychainItemWrapper * keychin = [[KeychainItemWrapper alloc]initWithIdentifier:@"Gesture" accessGroup:nil];
    //    password = [keychin objectForKey:(__bridge id)kSecValueData];
    //    password = [[UserDefaults sharedInstance] objectForKey:KEY_GESTURE_PWD];
    //    if ([password isEqualToString:@""])return NO;
    //    return YES;
    self.password = [[UserDefaults sharedInstance] objectForKey:KEY_GESTURE_PWD];
    return [[UserManager sharedInstance] gesturePasswordIsExit];
}

- (BOOL)verification:(NSString *)result{
    self.password = [[UserDefaults sharedInstance] objectForKey:KEY_GESTURE_PWD];
    // 本地验证手势密码
    
    if ([[result sha1] isEqualToString:_password]) {
        
//        if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(onGestureLoginSuccess)]) {
//            [self.loginDelegate onGestureLoginSuccess];
//        }
        
        [self showHUDIndicatorViewAtCenter:@"正在验证手势密码，请稍候"];
        WeakSelf
        [[UserManager sharedInstance] doAutoLogin:^(NSInteger code_status) {
//            StrongSelf
            [weakSelf hideHUDIndicatorViewAtCenter];
            
            if (code_status == STATUS_OK) {
                
                [weakSelf dismissToRootViewController:YES completion:^{
                    
                    receiveAuthMessageCount = NO;
                    
                    AppDelegate *appDelegate = APP_DELEGATE;
                    
                    appDelegate.receiveLoginOverTimeMessageCount = 0;
                    [appDelegate initResignTime];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
                    
                }];
            }
            else {
                
                [weakSelf hideHUDIndicatorViewAtCenter];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"手势密码验证失败，请重试" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        [weakSelf.gesturePasswordView.tentacleView enterArgin];
                    }
                }];
            }
            
        } failure:^(NSInteger code_status) {

            [weakSelf hideHUDIndicatorViewAtCenter];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"手势密码验证失败，请重试" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf.gesturePasswordView.tentacleView enterArgin];
                }
            }];
            
        }];
        
        return YES;
    }
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"手势密码错误，请重新输入" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    WeakSelf
    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            [weakSelf.gesturePasswordView.tentacleView enterArgin];
        }
    }];
    
    return NO;
}



-(void)onUsernameLogin
{
    [self presentAccountLogin];
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
