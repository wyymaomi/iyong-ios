//
//  MyTravelListViewController.h
//  iYONGDriver
//
//  Created by 湛思科技 on 2017/6/27.
//  Copyright © 2017年 Shanghai Zhansi Tech. All rights reserved.
//

#import "BaseViewController.h"
#import "BaiduTraceSDK/BaiduTraceSDK.h"

@interface MyTravelListViewController : BaseViewController

@property (nonatomic, strong) NSString *entityName;

@property (nonatomic, strong) NSMutableArray *trackResultList;

@property (nonatomic, strong) NSString *driverId;



@end
