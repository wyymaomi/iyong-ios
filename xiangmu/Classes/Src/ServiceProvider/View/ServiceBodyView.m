//
//  ServiceBodyView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ServiceBodyView.h"
#import "OnlineServiceTableViewCell.h"
#import "ServiceProviderViewModel.h"
#import "OnlineServiceModel.h"
#import "StarView.h"

@interface ServiceBodyView ()

@property (nonatomic, strong) NSMutableArray *imageViewArray;


@end

@implementation ServiceBodyView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.iconView];
        [self addSubview:self.nameLabel];
        [self addSubview:self.companyLabel];
        [self addSubview:self.starView];
        [self addSubview:self.businessLabel];
        [self addSubview:self.photosView];
        
    }
    return self;
}

- (void)setViewModel:(ServiceProviderViewModel *)viewModel
{
    _viewModel = viewModel;
    
    // 给子控件设置frame
    [self setFrame];
    
    // 为子控件赋值
    [self setData];

}

- (void)setData
{
    self.iconView.image = [UIImage imageNamed:@"icon_avatar"];
    self.nameLabel.text = [StringUtil getSafeString:self.viewModel.serviceModel.nickname];
    self.companyLabel.text = [StringUtil getSafeString:self.viewModel.serviceModel.companyName];
//    self.businessLabel.text = [StringUtil getSafeString:self.viewModel.serviceModel.business];
    [self.starView setStarsImages:[self.viewModel.serviceModel.companyScor integerValue]];
    
    WeakSelf
    // 下载头像图片
//    [self.iconView downloadImage:self.viewModel.serviceModel.logoUrl placeholderImage:[UIImage imageNamed:@"icon_avatar"] success:^(id responseData) {
//        weakSelf.iconView.cornerRadius = weakSelf.iconView.width/2;
//    } andFailure:^(NSString *errorDesc) {
//        weakSelf.iconView.cornerRadius = weakSelf.iconView.width/2;
//    }];
//    [self.iconView downloadImageFromAliyunOSS:self.viewModel.serviceModel.logoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        weakSelf.iconView.cornerRadius = weakSelf.iconView.width/2;
//    } andFailure:^(NSString *errorDesc) {
//        weakSelf.iconView.cornerRadius = weakSelf.iconView.width/2;
//    }];
    
    self.iconView.cornerRadius = self.iconView.width/2;
    [self.iconView yy_setImageWithObjectKey:self.viewModel.serviceModel.logoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
        
    }];
    
}

- (void)setFrame
{
    self.iconView.frame = self.viewModel.bodyIconFrame;
    self.nameLabel.frame = self.viewModel.bodyNameFrame;
    self.businessLabel.frame = self.viewModel.bodyTextFrame;
    self.companyLabel.frame = self.viewModel.bodyTimeFrame;
//    self.starView.frame = ViewWidth
    self.starView.left = ViewWidth-self.starView.width-12;
    self.starView.top  = self.viewModel.bodyNameFrame.origin.y;
    
    
    // 如果没有图片则隐藏图片View
    if ([self.viewModel.serviceModel hasPhoto]) {
        
        self.photosView.hidden = NO;
        self.photosView.frame = self.viewModel.bodyPhotoFrame;
        
        NSInteger photoWidth = 70;
        NSInteger photoHeight = self.photosView.height;
        NSInteger separator = (self.photosView.width - photoWidth * 4) / 3;
        
        self.photosView.photoWidth = photoWidth;
        self.photosView.photoHeight = photoHeight;
        self.photosView.photoMargin = separator;
        
        NSMutableArray *imgUrls = [NSMutableArray new];
        
        if (!IsStrEmpty([StringUtil getSafeString:self.viewModel.serviceModel.img1])) {
            [imgUrls addObject:self.viewModel.serviceModel.img1];
        }
        if (!IsStrEmpty([StringUtil getSafeString:self.viewModel.serviceModel.img2])) {
            [imgUrls addObject:self.viewModel.serviceModel.img2];
        }
        if (!IsStrEmpty([StringUtil getSafeString:self.viewModel.serviceModel.img3])) {
            [imgUrls addObject:self.viewModel.serviceModel.img3];
        }
        if (!IsStrEmpty([StringUtil getSafeString:self.viewModel.serviceModel.img4])) {
            [imgUrls addObject:self.viewModel.serviceModel.img4];
        }
        
        self.photosView.originalUrls = [imgUrls mutableCopy];
        self.photosView.thumbnailUrls = [imgUrls mutableCopy];
        
        self.photosView.photosMaxCol = 4;
        
    }
    else {
        self.photosView.hidden = YES;
    }
}

- (void)onClickImageView:(UITapGestureRecognizer *)recognizer
{
    
    NSInteger tag = recognizer.view.tag;
    
    if (self.cell && self.cell.delegate && [self.cell.delegate respondsToSelector:@selector(onClickImageView:)]) {
        [self.cell.delegate onClickImageView:tag];
    }
    
    
}

#pragma mark - getter and setter

- (StarView*)starView
{
    if (_starView == nil) {
        _starView = [StarView new];
        _starView.frame = CGRectMake(0, 0, 60, 20);
    }
    return _starView;
}

- (PYPhotosView*)photosView
{
    if (_photosView == nil) {
        _photosView = [PYPhotosView new];
        _photosView.backgroundColor = [UIColor whiteColor];
        
    }
    return _photosView;
}

- (UIView*)separatorLineView
{
    if (_separatorLineView == nil) {
        _separatorLineView = [UIView new];
        _separatorLineView.backgroundColor = [UIColor clearColor];
    }
    return _separatorLineView;
}


- (UIImageView*)iconView
{
    if (_iconView == nil) {
        _iconView = [[UIImageView alloc] init];
    }
    return _iconView;
}

- (UILabel*)nameLabel;
{
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = FONT(13);
//        _nameLabel.textColor = UIColorFromRGB(0xA9A9A9);
    }
    
    return _nameLabel;
}

- (UILabel*)companyLabel
{
    if (_companyLabel == nil) {
        _companyLabel = [[UILabel alloc] init];
        _companyLabel.font = FONT(13);
//        _companyLabel.textColor = UIColorFromRGB(0xA9A9A9);
    }
    
    return _companyLabel;
}

- (UILabel*)businessLabel
{
    if (_businessLabel == nil) {
        _businessLabel = [UILabel new];
        _businessLabel.font = FONT(13);
//        _businessLabel.textColor = UIColorFromRGB(0xA9A9A9);
        _businessLabel.numberOfLines = 0;
    }
    
    return _businessLabel;
}

- (NSMutableArray *)imageViewArray
{
    if (_imageViewArray == nil) {
        _imageViewArray = [NSMutableArray array];
    }
    return _imageViewArray;
}



@end
