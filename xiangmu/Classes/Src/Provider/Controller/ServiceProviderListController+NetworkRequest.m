//
//  ServiceProviderListController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ServiceProviderListController+NetworkRequest.h"

@implementation ServiceProviderListController (NetworkRequest)

#pragma mark - 按页码分页方式请求服务商数据

- (void)fetchServiceProviderByPageIndex:(NSInteger)pageIndex
{
    
    if (pageIndex < 1) {
        return;
    }
    
    NSString *url;
    url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ONLINE_SERVICE_PROVIDER_LIST_API];
    //    if ([self.title isEqualToString:@"我的收藏"]) {
    //        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, MY_FAVORITE_SERVICER_API];
    //    }
    
    NSDictionary *params = @{@"pageIndex": [NSString stringWithFormat:@"%ld", (long)pageIndex],
                             @"areaCode": self.areaCode,
                             @"businesType": [NSNumber numberWithInteger:self.businessType]};
    
    if (pageIndex == 1) {
        [self.dataList removeAllObjects];
    }
    
    //    if (isHeaderRefresh) {
    //        self.pullRefreshTableView.footerHidden = YES;
    //        [self.dataList removeAllObjects];
    //    }
    
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        //        [strongSelf.pullRefreshTableView endHeaderRefresh];
        //        [strongSelf.pullRefreshTableView endFooterRefresh];
        
        [strongSelf.collectionView endHeaderRefresh];
        [strongSelf.collectionView endFooterRefresh];
        
        [strongSelf resetAction];
        //        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        if (code_status == STATUS_OK) {
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
                if (error) {
                    return;
                }
                
                NSArray<OnlineServiceModel*> *list = responseData.data.list;
                if (list.count > 0) {
                    self.commonPage.pageIndex++;
                }
                
                for (NSInteger i = 0; i < list.count; i++) {
                    OnlineServiceModel *model = list[i];
                    //                    ServiceProviderViewModel *viewModel = [[ServiceProviderViewModel alloc] init];
                    //                    viewModel.serviceModel = model;
                    //                    [strongSelf.dataList addObject:viewModel];
                    [strongSelf.dataList addObject:model];
                }
                
                strongSelf.collectionView.dataList = strongSelf.dataList;
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    strongSelf.loadStatus = LoadStatusFinish;
                    
                    [strongSelf.collectionView reloadData];
                    
//                    [UIView performWithoutAnimation:^{
//                        [strongSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
//                    }];
//                    [UIView performWithoutAnimation:^{
//                        [strongSelf.collectionView reloadData];
//                    }];
                    
//                    strongSelf.collectionView.hidden = YES;
////                    [strongSelf.collectionView reloadData];
////                    strongSelf.collectionView.hidden = NO;
//                    [strongSelf.collectionView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
//                    strongSelf.collectionView.hidden = NO;
                    
                });
                
            });
            
        }
        else if (code_status == NETWORK_FAILED) {
            strongSelf.loadStatus = LoadStatusNetworkError;
        }
        else {
            strongSelf.loadStatus = LoadStatusFinish;
//            NSError *error;
//            OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
//            if (error) {
//                return;
//            }
            //            [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
        }
        
    }];
    
    
}

- (void)fetchMyFavoriteList:/*(NSString*)quantity time:*/(NSString*)time
{
    //    NSDictionary *params = @{@"quantity":[StringUtil getSafeString:quantity],
    //                             @"time":[StringUtil getSafeString:time],
    //                             @"token":[StringUtil getSafeString:[UserManager sharedInstance].token]};
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, MY_FAVORITE_SERVICER_API];
//    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:time]];
//    NSString *params = [NSString stringWithFormat:@"quantity=%@&time=%@&token=%@", [StringUtil getSafeString:quantity], [StringUtil getSafeString:time], [StringUtil getSafeString:[UserManager sharedInstance].token]];
//    if ([self.title isEqualToString:@"我的收藏"]) {
//        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, MY_FAVORITE_SERVICER_API];
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:time]];
//    }
    
    if (IsStrEmpty(time)) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *result) {
        
        StrongSelf
        
        [strongSelf.collectionView endHeaderRefresh];
        [strongSelf.collectionView endFooterRefresh];
        [strongSelf resetAction];
        strongSelf.collectionView.footerHidden = NO;
        strongSelf.loadStatus = LoadStatusFinish;
        
        NSInteger code_status = [result[@"code"] integerValue];
        //        NSDictionary *result = responseData[@"data"];
        
        if (code_status == STATUS_OK) {
            
            strongSelf.loadStatus = LoadStatusFinish;
            
            if ([result[@"data"] isKindOfClass:[NSArray class]]) {
                NSArray *data = result[@"data"];
                
                for (NSInteger i = 0; i < data.count; i++) {
                    NSDictionary *dict = data[i];
//                    NSError *error;
                    OnlineServiceModel *model = [[OnlineServiceModel alloc] initWithDictionary:dict error:nil];
                    [strongSelf.dataList addObject:model];
                }
            }

            strongSelf.collectionView.dataList = strongSelf.dataList;
            
            
            [UIView performWithoutAnimation:^{
                
                [strongSelf.collectionView reloadData];
                
            }];
            
//            [UIView animateWithDuration:0 animations:^{
//                
//                [strongSelf.collectionView reloadData];
////                [strongSelf.collectionView performBatchUpdates:^{
////
////                } completion:nil];
//            }];
            
//            [strongSelf.collectionView reloadData];
            
//            [UIView performWithoutAnimation:^{
//                [strongSelf.collectionView reloadData];
//            }];
//            [strongSelf.collectionView reloadData];
            //            }
            
        }
        else {
            [strongSelf showAlertView:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf.collectionView endHeaderRefresh];
        [strongSelf.collectionView endFooterRefresh];
        [strongSelf resetAction];
        
        strongSelf.loadStatus = LoadStatusNetworkError;
        
    }];
    
}




@end
