//
//  ProviderBalanceTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderBalanceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *currentPromotionAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountBalanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *promotionBalanceLabel;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+(CGFloat)getCellHeight;

@end
