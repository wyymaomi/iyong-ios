//
//  NetworkManager.h
//  xiangmu
//
//  Created by David kim on 16/3/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFNetworking.h"

typedef void(^SuccessBlock)(id responseData);
typedef void(^FailureBlock)(NSString *errorDesc);

typedef NS_ENUM(NSUInteger,RequestMethod){
    POST=0,
    GET,
};

typedef NS_ENUM(NSInteger,AFNetworkErrorType){
    AFNetworkErrorType_TimedOut=NSURLErrorTimedOut, //-1001 请求超时
    AFNetworkErrorType_UnURL=NSURLErrorUnsupportedURL, //-1002 不支持的url
    AFNetworkErrorType_NoNetwork=NSURLErrorNotConnectedToInternet, //-1009 断网
    AFNetworkErrorType_404Failed=NSURLErrorBadServerResponse, //-1011 404错误
    AFNetworkErrorType_Unauthorized = 401 // 401错误
};

@interface NetworkManager : NSObject

@property (nonatomic, strong) AFHTTPRequestOperationManager *requestManager;

+ (instancetype)sharedInstance;

#pragma mark - 取消所有正在进行的网络请求
- (void)cancelAllRequest;

// 取消某个请求
- (void)cancelHTTPOperationsWithMethod:(NSString *)method url:(NSString *)url;

#pragma mark - 生成图片下载URLRequest
-(NSMutableURLRequest*)downloadImageURLRequest:(NSString*)method
                                     URLString:(NSString*)URLString
                                        params:(NSString*)params;

#pragma mark - 图片下载
-(void)downloadImage:(NSString*)imageUrl
             success:(SuccessBlock)success
          andFailure:(FailureBlock)failure;

- (void)downloadImageData:(NSString*)url
            requestMethod:(RequestMethod)requestMethod
                   params:(NSString*)params
                  success:(SuccessBlock)success
               andFailure:(FailureBlock)failure;

#pragma mark - 加密请求
- (void)startEncryptRequest:(NSString*)url
              requestMethod:(RequestMethod)requestMethod
                     params:(NSString*)params
                    success:(SuccessBlock)success
                 andFailure:(FailureBlock)failure;

#pragma mark - 非加密请求
-(void)startHttpPost:(NSString*)url
              params:(NSDictionary*)params
           withBlock:(void(^)(NSInteger code_status, NSDictionary *result, NSString *error))block;

//网络请求
//+(void)startRequestMethod:(RequestMethod)method parameters:(NSDictionary *)parameters url:(NSString *)url success:(SuccessBlock)success andFailure:(FailureBlock)failure;

-(void)startHttpGet:(NSString*)url
             params:(NSDictionary*)params
          withBlock:(void(^)(NSInteger code_status, NSDictionary *result, NSString *error))block;

#pragma mark - 具体业务请求
-(void)verifyPaypassword:(NSString*)params success:(SuccessBlock)success andFailure:(FailureBlock)failure;
-(void)getBankcardList:(SuccessBlock)success andFailure:(FailureBlock)failure;
- (void)getFinanceInfo:(SuccessBlock)success andFailue:(FailureBlock)failure;
- (void)getUserInfo:(SuccessBlock)success andFailue:(FailureBlock)failure;

@end
