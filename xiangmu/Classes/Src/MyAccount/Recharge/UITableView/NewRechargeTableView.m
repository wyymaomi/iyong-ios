//
//  NewRechargeTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewRechargeTableView.h"
#import "NewBankTableViewCell.h"
#import "RechargeAmountTableViewCell.h"
#import "AdvPayTableViewCell.h"

@implementation NewRechargeTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

-(id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    [super setup];
    
    self.rechargeType = RechargeTypeAlipay;
    
    self.rechargeAmount = DEFAULT_RECHARGE_AMOUNT;
    
    
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.backgroundColor = [UIColor whiteColor];
    self.alpha = 1;
    
    self.scrollEnabled = NO;
    
    self.delegate = self;
    self.dataSource = self;
    
    self.tableFooterView = [UIView new];
    
}

#pragma mark - 选择金额

-(void)onSelectItem:(NSInteger)tag
{
    self.rechargeAmount = tag;
    
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(selectChargeAmount:)]) {
        [self.customDelegate selectChargeAmount:tag];
    }
}

-(void)onRecharge:(id)sender
{
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(doRecharge)]) {
        [self.customDelegate doRecharge];
    }
}

-(void)onClickAddBankcard
{
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(selectBankcard)]) {
        [self.customDelegate selectBankcard];
    }
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
//    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 3;
    }
    return 1;
    //    return 5;
//    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [NewBankTableViewCell getCellHeight];
        //        return 154;
//        return [AccountHeaderTableViewCell getCellHeight];
    }
    return [RechargeAmountTableViewCell getCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == RechargeTypeRowAlipay) {
            self.rechargeType = RechargeTypeAlipay;
            [tableView reloadData];
        }
        else if (indexPath.row == RechargeTypeRowWechat) {
            self.rechargeType = RechargeTypeWechat;
            [tableView reloadData];
        }
        else if (indexPath.row == RechargeTypeRowHuanxun) {
            [self onClickAddBankcard];
        }
        

        
//        if (self.delegate && [self.delegate respondsToSelector:@selector(selectBankcard)]) {
//            [self.delegate selectBankcard];
//        }
        
//        if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(selectBankcard)]) {
//            [self.customDelegate selectBankcard];
//        }
        
    }
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == RechargeTypeRowAlipay || indexPath.row == RechargeTypeRowWechat) {
            
            AdvPayTableViewCell *cell = [AdvPayTableViewCell cellWithTableView:tableView indexPath:indexPath];
            
            if (indexPath.row == RechargeTypeRowAlipay) {
//                cell.radioButton.selected = adPayType == AdPayTypeAlipay ? YES : NO;
                cell.radioButton.selected = self.rechargeType == RechargeTypeAlipay  ? YES : NO;
            }
            else if (indexPath.row == RechargeTypeRowWechat) {
                cell.radioButton.selected = self.rechargeType == RechargeTypeWechat ? YES : NO;
//                cell.radioButton.selected = adPayType == AdPayTypeWechat ? YES : NO;
            }
            
            cell.radioButton.tag = indexPath.section * 1000 + indexPath.row;
            [cell.radioButton addTarget:self action:@selector(onClickRadioButton:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        
        else if (indexPath.row == RechargeTypeRowHuanxun) {
            
            NewBankTableViewCell *cell = [NewBankTableViewCell cellWithTableView:tableView indexPath:indexPath];
            cell.addBankcardButton.tag = indexPath.section * 1000 + indexPath.row;
            [cell.addBankcardButton addTarget:self action:@selector(onClickAddBankcard) forControlEvents:UIControlEventTouchUpInside];
            cell.addBankcardButton.selected = self.rechargeType == RechargeTypeIPS?YES:NO;
            return cell;
            
        }
        

    }
    
    else if (indexPath.section == 1) {
        RechargeAmountTableViewCell *cell = [[RechargeAmountTableViewCell alloc] initWithStyle:tableView style:UITableViewCellStyleDefault reuseIdentifier:@""];
        cell.delegate = self;
        [cell.rechargeButton addTarget:self action:@selector(onRecharge:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    return nil;
}

- (void)onClickRadioButton:(UIButton*)button
{
    NSInteger tag = button.tag;
    NSInteger section = tag/1000;
    NSInteger row = tag%1000;
    //    self tableView
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
//    [self tableView:self.groupTableView didSelectRowAtIndexPath:indexPath];
    [self tableView:self didSelectRowAtIndexPath:indexPath];
}


@end
