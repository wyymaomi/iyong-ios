//
//  Bankcard.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/4.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol Bankcard <NSObject>
@end

@interface Bankcard : JSONModel

@property (nonatomic, strong) NSString<Optional> *bankName;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *number;
@property (nonatomic, strong) NSString<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *id;

@property (nonatomic, strong) NSString<Optional> *bankNameWithCard;

@end

@interface BankcardResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<Bankcard,Optional> *data;

@end
