//
//  HomepageCollectionView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomepageCollectionView.h"
#import "HomepageCollectionViewCell.h"
#import "OnlineServiceModel.h"

@interface HomepageCollectionView ()<UICollectionViewDelegate, UICollectionViewDataSource>



@end

@implementation HomepageCollectionView

#pragma mark - life cycle

-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    
    if (self) {
        
        [self initViews];
        
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self initViews];
        
    }
    
    return self;
}

- (void)initViews
{
    
    [self registerNib:[UINib nibWithNibName:@"HomepageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
//    [self registerClass:[HomepageCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    self.delegate=self;
    self.dataSource=self;
    self.scrollEnabled=NO;
}

#pragma mark - UICollectionView Delegate method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    return self.subviewCellList.count;
//    return self.dataList.count;
    
    return self.dataList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    HomepageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    OnlineServiceModel *data = self.dataList[indexPath.row];
    
    if (!data) {
        DLog(@"data == NULL");
        
        return cell;
    }
    
    NSString *imgName = data.imgs[0];
    
#if DONWLOAD_SWITCH
    [cell.imageView downloadImageFromAliyunOSS:imgName isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
#else
    [cell.imageView yy_setImageWithObjectKey:imgName placeholder:DefaultPlaceholderImage imageSize:DefaultImageSize manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
        
    }];
#endif
    

    
    cell.companyLabel.text = data.companyName;
    
    cell.commnetLabel.text = [NSString stringWithFormat:@"%@条评论", data.commentQuantity];
    
    [cell.starView setStarsImages:[data.companyScor floatValue]];
    
//    cell.commnetLabel.text = [NSString stringWithFormat:@"%@条评论", data.comment]
//    [cell.commnetLabel setText:[NSString stringWithFormat:@"%@条评论"], data.comm]
//    cell.imageView downloadImageFromAliyunOSS:data.imgs isThumbnail:<#(BOOL)#> placeholderImage:<#(UIImage *)#> success:<#^(id responseData)success#> andFailure:<#^(NSString *errorDesc)failure#>
    
    
//    ServiceProviderViewModel *viewModel = self.dataList[indexPath.section];
//    OnlineServiceModel *data = viewModel.serviceModel;
    
    return cell;
}

////cell点击变色
//-(BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
///cell点击变色
//-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger row = indexPath.row;
    
    if ( _customDelegate && [_customDelegate respondsToSelector:@selector(onClickItem:tag:)]) {
//        [_customDelegate onSelectItem:row];
        [_customDelegate onClickItem:row tag:self.tag];
    }
    
//    NSDictionary *cellDict = self.subviewCellList[indexPath.row];
//    NSString *viewController = cellDict[@"vc"];
//    
//    if ([viewController isEqualToString:@""]) {
//        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该系统暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        [alertView show];
//    }
//    else {
//        if (_customDelegate && [_customDelegate respondsToSelector:@selector(onGotoPage:title:)]) {
//            [_customDelegate onGotoPage:viewController title:cellDict[@"text"]];
//        }
//    }
    
}


@end
