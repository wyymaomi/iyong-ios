//
//  CNContactRootViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/10.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CNContactRootViewController.h"

@implementation CNContactRootViewController

//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
    
//    [self.navigationController.navigationBar setBackgroundColor:NAVBAR_BGCOLOR];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR]
//                                                  forBarMetrics:UIBarMetricsDefault];
    
//}

-(id)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
    }
    return self;
}

#pragma mark - 代理方法
// 控制器点击取消的时候调用
- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker;
{
    NSLog(@"点击了取消");
}

// 点击了联系人的时候调用, 如果实现了这个方法, 就无法进入联系人详情界面
//- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact {
//    
//    // contact属性就是联系人的信息
//    NSLog(@"%@---%@", contact.namePrefix, contact.familyName);
//    
//    // 获取联系人的电话号码
//    NSArray<CNLabeledValue<CNPhoneNumber*>*> *phoneNumbers = contact.phoneNumbers;
//    
//    // 注意, 由于这个数组规定了泛型, 所以要使用遍历器来取出每一个特定类型的对象, 才能取到里面的属性
//    [phoneNumbers enumerateObjectsUsingBlock:^(CNLabeledValue<CNPhoneNumber*> * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        
//        NSLog(@"%@--%@", obj.label, obj.value.stringValue);
//        
//        if ([obj.label contains:@"Mobile"]) {
//            
//            NSString *phoneNO = obj.value.stringValue;
//            
//            if (IsStrEmpty(phoneNO)) {
//                return;
//            }
//            
//            if ([phoneNO hasPrefix:@"+"]) {
//                phoneNO = [phoneNO substringFromIndex:3];
//            }
//            if ([phoneNO startsWith:@"86"]) {
//                phoneNO = [phoneNO substringFromIndex:2];
//            }
//            
//            phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
//            phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"(" withString:@""];
//            phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@")" withString:@""];
//            phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@" " withString:@""];
//            NSLog(@"%@", phoneNO);
//            
//            if (self.controllerDelegate && [self.controllerDelegate respondsToSelector:@selector(onGetContactMobile:)]) {
//                [self.controllerDelegate onGetContactMobile:phoneNO];
//            }
//        }
//        
//    }];
//}

// 点击了联系人的确切属性的时候调用, 注意, 这两个方法只能实现一个
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty {
    DLog(@"%@---%@", contactProperty.key, contactProperty.value);
    
    // 获取手机号
    if ([CNContactPhoneNumbersKey isEqualToString:contactProperty.key]) {
        CNPhoneNumber *phoneNumber = contactProperty.value;
        NSString *phoneNO = phoneNumber.stringValue;
        
        if (IsStrEmpty(phoneNO)) {
            return;
        }
        
        if ([phoneNO hasPrefix:@"+"]) {
            phoneNO = [phoneNO substringFromIndex:3];
        }
        if ([phoneNO startsWith:@"86"]) {
            phoneNO = [phoneNO substringFromIndex:2];
        }
        
        phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
        phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"(" withString:@""];
        phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@")" withString:@""];
        phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@" " withString:@""];
        DLog(@"%@", phoneNO);
        
        if (self.controllerDelegate && [self.controllerDelegate respondsToSelector:@selector(onGetContactMobile:)]) {
            [self.controllerDelegate onGetContactMobile:phoneNO];
        }
    }
}

@end
