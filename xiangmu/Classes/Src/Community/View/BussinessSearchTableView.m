//
//  BussinessSearchTableView.m
//  xiangmu
//
//  Created by David kim on 16/9/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BussinessSearchTableView.h"
#import "InputHelperViewController.h"
#import "GYZChooseCityController.h"
#import "GYZChooseCityDelegate.h"
#import "CommunityCateogryTableViewCell.h"
#import "BussinessSearchHttpMessage.h"
#import "BusinessCircleListViewController.h"
#import "CustomActionSheetDatePicker.h"
#import "CommunityHelper.h"
#import "MsgToolBox.h"
#import "CommunityHelper.h"
#import "ActionSheetStringPicker.h"

@interface BussinessSearchTableView ()<GYZChooseCityDelegate>

@property (nonatomic, strong) NSMutableArray *cityDatas;
@property (nonatomic, strong) CommunityHelper *communityHelper;

@end

@implementation BussinessSearchTableView

- (CommunityHelper*)communityHelper
{
    if (!_communityHelper) {
        _communityHelper = [CommunityHelper new];
    }
    return _communityHelper;
}


- (void)setup {
    [super setup];
    
    self.delegate = self;
    self.dataSource = self;
    
    self.tableFooterView = [[UIView alloc] init];
//    self.numberOfSection = 1;
//    self.numberOfRow = 5;
    self.rowHeight = 44;
    
}

#pragma mark - 城市选择

- (void)selectCity
{
    
    if (self.searchDelegate && [self.searchDelegate respondsToSelector:@selector(selectCity)]) {
        [self.searchDelegate selectCity];
    }

}

#pragma mark - 设置时间

- (void)setStartTime:(id)sender
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@""
                                           doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
                                               StrongSelf
                                               strongSelf.httpMessage.startTime = selectDateTime;
                                               [strongSelf reloadData];
                                           } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                               
                                           } origin:sender];
}

-(void)setEndTime:(id)sender
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@""
                                           doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
                                               StrongSelf
                                               
                                               strongSelf.httpMessage.endTime = selectDateTime;
                                               
                                               NSDate *date1 = [NSDate dateFromString:strongSelf.httpMessage.startTime dateFormat:@"yyyy-MM-dd HH:mm"];
                                               NSDate *date2 = [NSDate dateFromString:strongSelf.httpMessage.endTime dateFormat:@"yyyy-MM-dd HH:mm"];
                                               NSInteger compareResult = [NSDate dateCompare:date1 secondDate:date2];
                                               if (compareResult >=0) {
                                                   
                                                   strongSelf.httpMessage.endTime = @"";
                                                   
                                                   [MsgToolBox showAlert:@"" content:@"结束时间必须大于开始时间"];
                                                   
//                                                   [strongSelf showAlertView:@"结束时间必须大于开始时间"];
                                               }
                                               else {
                                                   
                                                   [strongSelf reloadData];
                                               }
                                               
                                           } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                               
                                               
                                           } origin:sender];
}

- (void)selectServiceType:(id)sender
{
    NSArray *serviceList = [self.communityHelper getServicetypeNameList];
    WeakSelf
    [ActionSheetStringPicker showPickerWithTitle:@"请选择用车类型" rows:serviceList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        weakSelf.httpMessage.type = [NSNumber numberWithInteger:selectedIndex+1];
        [weakSelf reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
        
    } origin:sender];
}

- (void)onSearch
{
    if (self.searchDelegate && [self.searchDelegate respondsToSelector:@selector(search)]) {
        [self.searchDelegate search];
    }
}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CommunityCateogryTableViewCell *cell = [CommunityCateogryTableViewCell cellWithTableView:tableView];
    cell.titleLabel.text = self.titleList[indexPath.row];
    cell.iconImageView.image = [UIImage imageNamed:self.iconList[indexPath.row]];
    cell.textField.enabled = NO;
    cell.textField.hidden = NO;
    
    if (indexPath.row == SearchRowCity) {
        cell.textField.placeholder = @"请选择出发城市";
        CommunityHelper *communityHelper = [CommunityHelper new];
        cell.textField.text = [communityHelper getCityNameFromAreacode:self.httpMessage.areaCode];
    }
    
    
    if (indexPath.row == SearchRowStartTime) {
        cell.textField.placeholder = @"请选择出发时间";
        cell.textField.text = self.httpMessage.startTime;
        
        cell.iconImageView.width = 21.5f;
        cell.iconImageView.height = 30;
        cell.iconImageView.top = (cell.frame.size.height - cell.iconImageView.height) / 2;
        cell.iconImageView.left = 12;
    }
    if (indexPath.row == SearchRowEndTime) {
        cell.textField.placeholder = @"请选择结束时间";
        cell.textField.text = self.httpMessage.endTime;
        cell.iconImageView.width = 21.5f;
        cell.iconImageView.height = 30;
        cell.iconImageView.top = (cell.frame.size.height - cell.iconImageView.height) / 2;
        cell.iconImageView.left = 12;
    }
    if (indexPath.row == SearchRowStartLocation) {
        cell.textField.placeholder = @"请输入出发地点";
        cell.textField.text = self.httpMessage.startPlace;
        
        cell.iconImageView.width = 21.5f;
        cell.iconImageView.height = 30;
        cell.iconImageView.top = (cell.frame.size.height - cell.iconImageView.height) / 2;
        cell.iconImageView.left = 12;
    }
    if (indexPath.row == SearchRowServiceType) {
        cell.textField.placeholder = @"请选择用车类型";
        cell.textField.text = [_communityHelper getServicetypeNameFromServiceId:[self.httpMessage.type integerValue]];
        
//        cell.textField.text = self.httpMessage.type;
        cell.iconImageView.width = 25;
        cell.iconImageView.height = 25;
        cell.iconImageView.top = (cell.frame.size.height - cell.iconImageView.height) / 2;
        cell.iconImageView.left = 12;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CommunityCateogryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == SearchRowCity) {
            
            [self selectCity];
            
        }
        
        if (indexPath.row == SearchRowStartTime) {
            
            [self setStartTime:cell.textField];
            
        }
        
        if (indexPath.row == SearchRowEndTime) {
            
            [self setEndTime:cell.textField];
            
        }
        
        if (indexPath.row == SearchRowStartLocation) {
            
            if (self.searchDelegate && [self.searchDelegate respondsToSelector:@selector(onSettingStartLocation)]) {
                [self.searchDelegate onSettingStartLocation];
            }
        }
        
        if (indexPath.row == SearchRowServiceType) {
            
            [self selectServiceType:cell.textField];
            
            
        }
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [UIView new];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [view addSubview:button];
    button.frame = CGRectMake(20, 20, ViewWidth-40, 40);
    [button setTitle:@"确    定" forState:UIControlStateNormal];
    [button blueStyle];
    [button addTarget:self action:@selector(onSearch) forControlEvents:UIControlEventTouchUpInside];
    
    return view;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

#pragma mark - 

- (BussinessSearchHttpMessage*)httpMessage
{
    if (!_httpMessage) {
        _httpMessage = [[BussinessSearchHttpMessage alloc] init];
        _httpMessage.type = @0;
    }
    return _httpMessage;
}

- (NSArray*)titleList
{
    if (!_titleList) {
        _titleList = @[@"选择城市", @"开始时间", @"结束时间", @"出发地点",@"用车类型"];
    }
    return _titleList;
}

- (NSArray*)iconList
{
    if (!_iconList) {
        _iconList = @[@"icon_city", @"icon_car_time", @"icon_end_time", @"icon_start",@"icon_servicetype"];
    }
    return _iconList;
}








@end
