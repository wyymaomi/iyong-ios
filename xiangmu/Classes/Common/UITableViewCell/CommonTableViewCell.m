//
//  CommonTableViewCell.m
//  xiangmu
//
//  Created by David kim on 16/9/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommonTableViewCell.h"

@implementation CommonTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [self addSubview:self.iconImageView];
        
        [self addSubview:self.titleLabel];
        
        [self addSubview:self.badgeValueBtn];
        
        
    }
    return self;
}

+ (CGFloat)getCellHeight;
{
    return 50;
}

- (void)layoutSubviews
{
    self.iconImageView.frame = CGRectMake(15, self.frame.size.height/2-18/2, 18, 18);
    self.titleLabel.frame = CGRectMake(38, 0, 200, self.frame.size.height);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UILabel*)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = FONT(14);
        _titleLabel.textColor = [UIColor darkTextColor];
    }
    return _titleLabel;
}

-(UIImageView*)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
    }
    
    return _iconImageView;
}

- (UIButton *)badgeValueBtn
{
    if (!_badgeValueBtn)
    {
        _badgeValueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_badgeValueBtn setBackgroundImage:[UIImage imageNamed:@"icon_msg_circle"] forState:UIControlStateNormal];
        [_badgeValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _badgeValueBtn.alpha = 0;
        _badgeValueBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        CGFloat btnWidth = 22;
        CGFloat btnHeight = 22;
        _badgeValueBtn.frame = CGRectMake(self.frame.size.width- 15 -  btnWidth - 5, (self.frame.size.height - btnHeight)/2,  btnWidth, btnHeight);
        [_badgeValueBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        _badgeValueBtn.hidden = YES;
        //        _badgeValueBtn.layer.zPosition = 1;
        [_badgeValueBtn setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [_badgeValueBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    }
    return _badgeValueBtn;
}

-(void)hideBadge
{
    self.badgeValueBtn.hidden = YES;
    self.badgeValueBtn.alpha = 0;
}

-(void)showBadgeValue:(NSString*)number
{
    self.badgeValueBtn.hidden = NO;
    self.badgeValueBtn.alpha = 1;
    if ([number isEqualToString:@"0"] || IsStrEmpty(number))
    {
        self.badgeValueBtn.alpha = 0;
        self.badgeValueBtn.hidden = NO;
        number = @"0";
        //        self.badgeValueBtn.hidden = YES;
        return;
    }
    
    NSUInteger length = [number length];
    
    CGSize size = [@"9" textSize:CGSizeMake(64, 20) font:FONT(12)];
//    CGSize size = [@"9" sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(64, 20)];
    
    
    CGRect rect = self.badgeValueBtn.frame;
    rect.size.width = size.width * (length + 0.5)>22?size.width*(length+0.5):22;
    
    self.badgeValueBtn.frame = rect;
    [self.badgeValueBtn setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.badgeValueBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    self.badgeValueBtn.alpha = 1.0;
    [self.badgeValueBtn setTitle:number forState:UIControlStateNormal];
    [self.badgeValueBtn setTitle:number forState:UIControlStateDisabled];
    //    }
}


@end
