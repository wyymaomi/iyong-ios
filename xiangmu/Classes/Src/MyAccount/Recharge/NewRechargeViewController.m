//
//  NewRechargeViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NewRechargeViewController.h"
#import "RechargeTableViewCell.h"
#import "RechargetypeTableViewCell.h"
#import "SDKManager.h"
#import "AlipayOrder.h"
#import "SDKRegisterService.h"
#import "SDKPayService.h"
#import "Bankcard.h"
#import "BBAlertView.h"
#import "BankcardViewController.h"
#import "UpmpPay.h"
#import <IPSUPMPSDK/IPSUpmpViewController.h>
#import "RechargeDetailViewController.h"
#import "PayUtil.h"
#import "NewBankTableViewCell.h"
#import "NewRechargeViewController+ThirdPay.h"
//#import "BankcardResponseData.h"
//#import "SDKAuthService.h"
//#import "SDKShareService.h"

//#import <AlipaySDK/AlipaySDK.h>
//#import "SDKAlipayService"

#import "NewRechargeTableView.h"

@interface NewRechargeViewController ()<UITextFieldDelegate,BancardListViewDelegate,IPSUPMPDelegate, NewRechargeTableViewDelegate>

@property (nonatomic, strong) NewRechargeTableView *tableView;

@property (strong, nonatomic) UIButton *okButton;
@property (nonatomic, strong) UILabel *messageLabel;// 提示文字
@property (nonatomic, strong) UITextField *transAmountTextField;//交易金额输入框
@property (nonatomic, strong) NSArray *rechargeTypeList;
@property (nonatomic, assign) RechargeType rechargeType;

@property (nonatomic, strong) BBAlertView *bankcardAlertView;
@property (nonatomic, strong) BBAlertView *paymentAlertView;
@property (nonatomic, strong) Bankcard *bankcard;

@end

@implementation NewRechargeViewController

-(NewRechargeTableView*)tableView
{
    if (!_tableView) {
        _tableView = [[NewRechargeTableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
        _tableView.customDelegate = self;
    }
    return _tableView;
}

-(BBAlertView*)bankcardAlertView
{
    if (!_bankcardAlertView) {
        _bankcardAlertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleBankcard Title:nil message:nil customView:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
        _bankcardAlertView.bankcardListView.delegate = self;
    }
    return _bankcardAlertView;
}

-(BBAlertView*)paymentAlertView
{
    if (!_paymentAlertView) {
        _paymentAlertView = [[BBAlertView alloc] initWithStyle: BBAlertViewStylePayment Title:nil message:nil customView:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    }
    return _paymentAlertView;
}

- (NSArray*)rechargeTypeList
{
    if (!_rechargeTypeList) {
        _rechargeTypeList = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RechrageType.plist" ofType:nil]];
    }
    
    return _rechargeTypeList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"充值";

    
    [self.view addSubview:self.tableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRechargeSuccess) name:kAdvPurchaseSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRechargeFailure) name:kAdvPurchaseFailureNotification object:nil];
    
//    self.rechargeType = RechargeTypeAlipay;
//    
//    self.rechargeAmount = 1000;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBankcardList) name:kRefreshBankcardListNotification object:nil];
    
//    [self.view addSubview:self.tpkeyboardAvoidingGroupTableView];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"限额说明" style:UIBarButtonItemStyleDone target:self action:@selector(gotoLimitAmount)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Recharge delegate method

-(void)refreshBankcardList
{
    [self.bankcardAlertView.bankcardListView.tableView reloadData];
}

-(void)onAddBankcard
{
    [self.bankcardAlertView dismiss];
    BankcardViewController *viewController = [[BankcardViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)selectBankcard;
{
    [self showBankcardListView];
}

#pragma mark - 充值回调接口

//-(void)selectChargeAmount:(NSInteger)amount;
//{
//    self.rechargeAmount = amount;
//}

-(void)doRecharge;
{
    
    NSUInteger rechargeMode = self.tableView.rechargeType;
    NSUInteger rechargeAmount = self.tableView.rechargeAmount;
    
    if (rechargeMode == RechargeTypeAlipay || rechargeMode == RechargeTypeWechat) {
        [self saveRechargeOrder:rechargeMode rechargeAmount:rechargeAmount];
        return;
    }
    
    // 判断是否选择了银行卡
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    NewBankTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if (IsStrEmpty(cell.bankcardLabel.text.trim)) {
        [self showAlertView:@"请选择银行卡"];
        return;
    }
    
    if (self.tableView.rechargeAmount == 0) {
        [MsgToolBox showToast:@"请选择充值金额"];
        return;
    }
    
    self.paymentAlertView.paymentView.moneyLabel.text = [NSString stringWithFormat:@"¥ %ld", (long)self.tableView.rechargeAmount];
//#endif
    
    self.paymentAlertView.paymentView.rateMoneyLabel.hidden = YES;
    WeakSelf
    self.paymentAlertView.paymentView.payInputView.clickBlock = ^(){
        [weakSelf.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
    };
    self.paymentAlertView.paymentView.payInputView.completeHandle = ^(NSString *payPassword) {
        [weakSelf checkPayPwd:payPassword];
    };
    [self.paymentAlertView show];
    [self.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
    
}

-(void)checkPayPwd:(NSString*)inputPwd
{
    self.nextAction = @selector(checkPayPwd:);
    self.object1 = inputPwd;
    self.object2 = nil;
    
    [self.paymentAlertView.paymentView.payInputView resetInputView];
    [self.paymentAlertView.paymentView.payInputView.pwdTextField resignFirstResponder];
    [self.paymentAlertView dismiss];
    [self showHUDIndicatorViewAtCenter:@"正在校验支付密码，请稍候"];
    
    WeakSelf
    [[NetworkManager sharedInstance] verifyPaypassword:inputPwd success:^(NSDictionary *responseData) {
        //        StrongSelf
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
//                [weakSelf getUpmpBillNo];
                
                // 设置过交易密码
                
                CGFloat rechargeType = weakSelf.tableView.rechargeType;
                NSUInteger rechargeAmount = weakSelf.tableView.rechargeAmount;
                
#ifdef TEST
//                rechargeType = 0.02;
#endif
                
                [weakSelf saveRechargeOrder:rechargeType rechargeAmount:rechargeAmount];
                
            }
            else if([data integerValue] == 0){
                // 校验支付密码失败
                //                [MsgToolBox showToast:@"支付密码输入校验失败，请重新输入"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"支付密码输入校验失败，请重新输入" delegate:self cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        [weakSelf.paymentAlertView show];
                        [weakSelf.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
                        [weakSelf.paymentAlertView.paymentView.payInputView resetInputView];
                    }
                    
                }];
            }
        }
        else {
            // 校验支付密码失败
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[self getErrorMsg:code_status] delegate:self cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [weakSelf.paymentAlertView show];
                    [weakSelf.paymentAlertView.paymentView.payInputView.pwdTextField becomeFirstResponder];
                    [weakSelf.paymentAlertView.paymentView.payInputView resetInputView];
                }
                
            }];
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
    }];
}


#pragma mark - 选择银行卡

-(void)showBankcardListView
{
    [self getBankcardList];
}

-(void)getBankcardList
{
    self.nextAction = @selector(getBankcardList);
    self.object1 = nil;
    self.object2 = nil;
    
    [self showHUDIndicatorViewAtCenter:@"正在查询银行卡列表，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] getBankcardList:^(id responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        BankcardResponseData *bankcardResponseData = [[BankcardResponseData alloc] initWithDictionary:responseData error:nil];
        if ([bankcardResponseData.code integerValue] == STATUS_OK) {
            NSArray *dataList = bankcardResponseData.data;
            if (dataList.count > 0 ) {
                strongSelf.bankcardAlertView.bankcardListView.dataList = bankcardResponseData.data;
                [strongSelf.bankcardAlertView.bankcardListView.tableView reloadData];
                [strongSelf.bankcardAlertView show];
            }
            else {
                
                // 如果没有绑定银行卡，跳转到银行卡绑定界面绑定银行卡
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定银行卡" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        BankcardViewController *viewController = [[BankcardViewController alloc] init];
                        [strongSelf.navigationController pushViewController:viewController animated:YES];
                    }
                }];
            }
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[bankcardResponseData.code integerValue]]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

-(void)onBankcardListSelect:(NSInteger)selectedRow;
{
    if (self.bankcardAlertView.bankcardListView.dataList.count > 0) {
        self.bankcard = self.bankcardAlertView.bankcardListView.dataList[selectedRow];
        
        self.tableView.rechargeType = RechargeTypeIPS;
        [self.tableView reloadData];
        
        // 刷新tableView，填入银行卡号
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        
        NewBankTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.bankcardLabel.text = [NSString stringWithFormat:@"%@(%@)", self.bankcard.bankName, [self.bankcard.number substringFromIndex:self.bankcard.number.length-4]];
//        self.tableView.rechargeType = RechargeTypeIPS;
        
//        [self.tableView reloadData];
        
        [self.bankcardAlertView dismiss];
    }
    
}

#pragma mark - 获取环讯支付订单号

-(void)getUpmpBillNo;
{
    NSUInteger rechargeAmount = self.tableView.rechargeAmount;
    
    NSDictionary *dict = @{kRechargeAmount:/*self.transAmountTextField.text.trim*/[NSNumber numberWithFloat:rechargeAmount],
                           kBankcardNo:self.bankcard.number};
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, RECHARGE_BILL_NO_API];
    
    UpmppayOrder *payOrder = [UpmppayOrder new];
    UpmpPay *upmpPay = [payOrder getUpmpData:@"" tranAmt:dict[kRechargeAmount] bankcardNo:dict[kBankcardNo]];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:upmpPay.description success:^(id responseData) {
        StrongSelf
        NSDictionary *result = (NSDictionary*)responseData;
        NSInteger code = [result[@"code"] integerValue];
        NSDictionary *data = result[@"data"];
        
        if (code == STATUS_OK) {
            NSString *billNo = data[@"orderCode"];
            NSString *notifyUrl = data[@"notifyUrl"];
            if (!IsStrEmpty(billNo) && !IsStrEmpty(notifyUrl)) {
                upmpPay.merBillNo = billNo;
                upmpPay.merNoticeUrl = notifyUrl;
                [strongSelf upmpPay:upmpPay];
            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf showAlertView:errorDesc];
    }];
}

-(void)upmpPay:(NSString*)merBillNo notifyUrl:(NSString*)notifyUrl
{
    CGFloat rechargeAmount = self.tableView.rechargeAmount;
//#ifdef TEST
//    rechargeAmount = 0.02;
//#endif
    
    NSDictionary *dict = @{kRechargeAmount:[NSNumber numberWithFloat:rechargeAmount],
                           kBankcardNo:self.bankcard.number};
    
    UpmppayOrder *payOrder = [UpmppayOrder new];
    UpmpPay *upmpPay = [payOrder getUpmpData:@"" tranAmt:dict[kRechargeAmount] bankcardNo:dict[kBankcardNo]];
    upmpPay.merBillNo = merBillNo;
    upmpPay.merNoticeUrl = notifyUrl;
    [self upmpPay:upmpPay];
    
}

-(void)upmpPay:(UpmpPay*)upmpPay
{
    UpmppayOrder *payOrder = [UpmppayOrder new];
    NSString *requestDES;
    NSString *signString = [payOrder generateSignString:upmpPay merRequestDes:&requestDES];
    [IPSUpmpViewController IPSStartPayWithMerCode:UPMP_MER_CODE bankCard:self.bankcard.number sign:signString merRequestInfo:requestDES delegate:self viewController:self];
}

#pragma mark - Upmp delegate method
-(void)orderCompletedStatus:(NSString*)status
                    merCode:(NSString*)merCode
                  merBillNo:(NSString*)merBillNo
                  orderDesc:(NSString*)orderDesc
                    tranAmt:(NSString*)tranAmt;
{
    if ([status isEqualToString:@"000000"]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"充值成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        WeakSelf
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            [weakSelf pressedBackButton];
        }];
        
    }
    else {
        [MsgToolBox showToast:@"充值失败，请重试"];

    }
    
}




#pragma mark - 充值


- (NSString *)generateTradeNO
{
    static int kNumber = 15;
    
    NSString *sourceStr = @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSMutableString *resultStr = [[NSMutableString alloc] init];
    srand((unsigned)time(0));
    for (int i = 0; i < kNumber; i++)
    {
        unsigned index = rand() % [sourceStr length];
        NSString *oneStr = [sourceStr substringWithRange:NSMakeRange(index, 1)];
        [resultStr appendString:oneStr];
    }
    return resultStr;
}

#pragma mark - 限额说明

- (void)gotoLimitAmount
{
    [self gotoPage:@"RechargeLimitionDescriptionViewController"];
}

#pragma mark - 充值结果

-(void)onRechargeSuccess
{
//    [MsgToolBox showToast:@"充值成功]
    
    WeakSelf
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"充值成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
        [weakSelf pressedBackButton];
    }];
}

-(void)onRechargeFailure
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"充值失败，请重试" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
}


#pragma mark - getter and setter






@end
