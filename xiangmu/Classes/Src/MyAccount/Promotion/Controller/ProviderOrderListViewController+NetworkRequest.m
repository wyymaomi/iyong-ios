//
//  ProviderOrderListViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderOrderListViewController+NetworkRequest.h"
#import "AlipayOrder.h"
#import "SDKManager.h"
#import "SDKRegisterService.h"
#import "SDKPayService.h"
#import "WXApiObject.h"
#import "WXApi.h"
#import "StringUtil.h"
//#import "ProviderInfo"

@implementation ProviderOrderListViewController (NetworkRequest)

#pragma mark - fetchProviderAdOrderListWithDetail
- (void)fetchProviderAdOrderListWithDetail:(NSString*)time
{
    
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:time]];
    
    if (IsStrEmpty(time)) {
        [self.viewModel.orderList removeAllObjects];
    }
    
//    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_AD_ORDER_LIST_API) requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        
//        strongSelf.pullRefreshTableView
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
//        strongSelf.loadStatus = LoadStatusFinish;
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSArray *data = responseData[@"data"];
                
                for (NSDictionary *dict in data) {
                    
                    NSError *error;
                    ProviderOrderModel *order = [[ProviderOrderModel alloc] initWithDictionary:dict error:&error];
                    if (!error) {
                        [strongSelf.viewModel.orderList addObject:order];
                    }
                    
                }
                
                
//                strongSelf.dataList = strongSelf.viewModel.orderList;
                
                dispatch_main_async_safe(^{
                    
                    [strongSelf.pullRefreshTableView reloadData];
                    
                });
                
            });
            
            
        }
        else {
//            strongSelf.loadStatus = LoadStatus
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
//        strongSelf.loadStatus = LoadStatusNetworkError;
        
    }];
}


-(void)continuePayOrder:(UIButton*)button
{
    ProviderOrderModel *model = self.viewModel.orderList[button.tag];
    [self submitPay:model.id payMode:[model.payMode integerValue]];
}

-(void)cancelOrder:(UIButton*)button
{
    ProviderOrderModel *model = self.viewModel.orderList[button.tag];
    [self requestCancelOrder:model];
//    [self requestCancelOrder:model.id];
}

-(void)deleteOrder:(id)sender
{
    UIButton *button = (UIButton*)sender;
    ProviderOrderModel *model = self.viewModel.orderList[button.tag];
//    [self requestDeleteOrder:model.id];
    [self requestDeleteOrder:model];
}

-(void)requestDeleteOrder:(ProviderOrderModel*)data
{
    NSString *params = [NSString stringWithFormat:@"adOrderId=%@", data.id];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_DELETE_AD_ORDER_API) requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        
        NSUInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            [MsgToolBox showToast:@"删除订单成功"];
            
            [strongSelf.viewModel.orderList removeObject:data];
            
            [strongSelf.pullRefreshTableView reloadData];
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
        
    }];
}

- (void)requestCancelOrder:(ProviderOrderModel*)model
{
    NSString *params = [NSString stringWithFormat:@"adOrderId=%@", model.id];
    //    [self showHUDIndicatorViewAtCenter:@"正在取消订单，请稍候...."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_CANCEL_AD_ORDER_API) requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            [MsgToolBox showToast:@"取消订单成功"];
            
            model.orderStatus = [NSNumber numberWithInteger:AdOrderStatusCancelled];

            [strongSelf.pullRefreshTableView reloadData];
            
//            strongSelf.pullRefreshTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:<#(NSInteger)#> inSection:<#(NSInteger)#>]] withRowAnimation:<#(UITableViewRowAnimation)#>
//            strong
            
//            model.orderStatus = OrderStatus
            
//            [strongSelf fetchProviderAdOrderListWithDetail];
            
        }
        else {
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

- (void)submitPay:(NSString*)adOrderId payMode:(NSUInteger)payMode
{
    NSString *params = [NSString stringWithFormat:@"orderId=%@", adOrderId];
    //    NSString *params = [NSString stringWithFormat:@"%@&orderId=%@", self.httpMessage.description, adOrderId];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_PAY_ORDER_API) requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            
            if (payMode == AdPayTypeAlipay) {
                NSString *data = responseData[@"data"];
                if (!IsStrEmpty(data)) {
                    [strongSelf doAlipayPay:data];
                }
                
            }
            else if (payMode == AdPayTypeWechat) {
                
                NSDictionary *data = responseData[@"data"];
                if (data != nil) {
                    [strongSelf bizPay:data];
                }
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"支付成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
//                        [strongSelf fetchProviderAdOrderListWithDetail];
                        //                        [strongSelf.navigationController popToRootViewControllerAnimated:YES];
                    }
                    
                }];
            }
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

//
//选中商品调用支付宝极简支付
//
- (void)doAlipayPay:(NSString*)encryptOrderString
{
    
    //    NSDictionary *dict = @{kTradeNo:@"1234567890",//[self generateTradeNO],
    //                           kRechargeAmount:self.transAmountTextField.text.trim,
    //                           kPayTime:[[NSDate date] stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"]};
    //    id<PayModelAdapterProtocol> alipay = [[AlipayOrder alloc] init];
    
    [[SDKManager getPayService:SDKPlatformAlipay] payOrder:/*[alipay generateOrderString:dict]*/encryptOrderString callBack:^(NSString *signString, NSError *error) {
        DLog(@"signString = %@", signString);
        //        NSInteger resultStatus = [signString integerValue];
        //        if (resultStatus == 6001) {
        //            [MsgToolBox showAlert:@"" content:@"用户取消"];
        ////                        [self showTipMessage:@"用户取消"];
        //            //            [self ]
        //        }
        //        else if (resultStatus == 6002){
        //            [MsgToolBox showAlert:@"" content:@"网络连接错误"];
        ////                        [self showTipMessage:@"网络连接错误"];
        //        }
        //        else if (resultStatus == 4000){
        //            [MsgToolBox showAlert:@"" content:@"订单支付失败"];
        //            //            [self showTipMessage:@"订单支付失败"];
        //        }
        //        else if (resultStatus == 8000){
        //            [MsgToolBox showAlert:@"" content:@"正在处理中"];
        //            //            [self showTipMessage:@"正在处理中"];
        //        }
        //        else if (resultStatus == 9000){
        //            [MsgToolBox showAlert:@"" content:@"订单支付成功"];
        //            //            [self showTipMessage:@"订单支付成功"];
        //        }
        
    }];
    
}

#pragma mark - 微信支付

- (void)bizPay:(NSDictionary*)dict
{
    //    NSString *res = [WXApiRequestHandler jumpToBizPay];
    //    if( ![@"" isEqual:res] ){
    //        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"支付失败" message:res delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //
    //        [alter show];
    ////        [alter release];
    //    }
    
    NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
    
    //调起微信支付
    PayReq *req             = [[PayReq alloc] init];
    req.partnerId           = dict[@"partnerid"];
    req.prepayId            = [dict objectForKey:@"prepayid"];
    req.nonceStr            = [dict objectForKey:@"noncestr"];
    req.timeStamp           = stamp.intValue;
    req.package             = [dict objectForKey:@"package"];
    req.sign                = [dict objectForKey:@"sign"];
    [WXApi sendReq:req];
    
}



@end
