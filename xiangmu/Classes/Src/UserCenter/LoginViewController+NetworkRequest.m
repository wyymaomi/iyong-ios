//
//  LoginViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/7.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "LoginViewController+NetworkRequest.h"
#import <UMSocialCore/UMSocialCore.h>
#import "BaseViewController+HandleLoginSuccess.h"
#import "UserHelper.h"

@implementation LoginViewController (NetworkRequest)

- (void)getAuthWithUserInfoFromQQ
{
    WeakSelf
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_QQ currentViewController:nil completion:^(id result, NSError *error) {
        if (error) {
            DLog(@"error = %@", error.localizedDescription);
        } else {
            UMSocialUserInfoResponse *resp = result;
            
            // 授权信息
            NSLog(@"QQ uid: %@", resp.uid);
            NSLog(@"QQ openid: %@", resp.openid);
            NSLog(@"QQ accessToken: %@", resp.accessToken);
            NSLog(@"QQ expiration: %@", resp.expiration);
            
            // 用户信息
            NSLog(@"QQ name: %@", resp.name);
            NSLog(@"QQ iconurl: %@", resp.iconurl);
            NSLog(@"QQ gender: %@", resp.gender);
            
            // 第三方平台SDK源数据
            NSLog(@"QQ originalResponse: %@", resp.originalResponse);
            
            
            weakSelf.thirdPartyLoginHttpMessage.uid = resp.uid;
            weakSelf.thirdPartyLoginHttpMessage.openid = resp.openid;
            weakSelf.thirdPartyLoginHttpMessage.refreshToken = resp.refreshToken;
            weakSelf.thirdPartyLoginHttpMessage.accessToken = resp.accessToken;
            weakSelf.thirdPartyLoginHttpMessage.logoUrl = resp.iconurl;
//            weakSelf.qqLoginHttpMessage.gender = resp.gender;
            if ([resp.gender isEqualToString:@"女"] || [resp.gender.lowercaseString isEqualToString:@"f"]) {
                weakSelf.thirdPartyLoginHttpMessage.gender = @"1";
            }
            else {
                weakSelf.thirdPartyLoginHttpMessage.gender = @"0";
            }
            weakSelf.thirdPartyLoginHttpMessage.nickname = resp.name;
            
            weakSelf.thirdPartyId = ThirdpartyIdQQ;

            [weakSelf doThirdpartLogin:weakSelf.thirdPartyLoginHttpMessage type:ThirdpartyIdQQ];
            
        }
    }];
}

- (void)getAuthWithUserInfoFromSina
{
    WeakSelf
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_Sina currentViewController:nil completion:^(id result, NSError *error) {
        if (error) {
            DLog(@"error = %@", error.localizedDescription);
        } else {
            UMSocialUserInfoResponse *resp = result;
            
            // 授权信息
            NSLog(@"Sina uid: %@", resp.uid);
            NSLog(@"Sina accessToken: %@", resp.accessToken);
            NSLog(@"Sina refreshToken: %@", resp.refreshToken);
            NSLog(@"Sina expiration: %@", resp.expiration);
            
            // 用户信息
            NSLog(@"Sina name: %@", resp.name);
            NSLog(@"Sina iconurl: %@", resp.iconurl);
            NSLog(@"Sina gender: %@", resp.gender);
            
            // 第三方平台SDK源数据
            NSLog(@"Sina originalResponse: %@", resp.originalResponse);
            
            weakSelf.thirdPartyLoginHttpMessage.uid = resp.uid;
            weakSelf.thirdPartyLoginHttpMessage.openid = resp.openid;
            weakSelf.thirdPartyLoginHttpMessage.refreshToken = resp.refreshToken;
            weakSelf.thirdPartyLoginHttpMessage.accessToken = resp.accessToken;
            weakSelf.thirdPartyLoginHttpMessage.logoUrl = resp.iconurl;
            // 1男，0女
            if ([resp.gender isEqualToString:@"女"] || [resp.gender.lowercaseString isEqualToString:@"f"]) {
                weakSelf.thirdPartyLoginHttpMessage.gender = @"0";
            }
            else {
                weakSelf.thirdPartyLoginHttpMessage.gender = @"1";
            }
            weakSelf.thirdPartyLoginHttpMessage.nickname = resp.name;
            
            weakSelf.thirdPartyId = ThirdpartyIdWeibo;
            
            [weakSelf doThirdpartLogin:weakSelf.thirdPartyLoginHttpMessage type:ThirdpartyIdWeibo];
        }
    }];
}

- (void)getAuthWithUserInfoFromWechat
{
    WeakSelf
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        if (error) {
            
        } else {
            UMSocialUserInfoResponse *resp = result;
            
            // 授权信息
            NSLog(@"Wechat uid: %@", resp.uid);
            NSLog(@"Wechat openid: %@", resp.openid);
            NSLog(@"Wechat accessToken: %@", resp.accessToken);
            NSLog(@"Wechat refreshToken: %@", resp.refreshToken);
            NSLog(@"Wechat expiration: %@", resp.expiration);
            
            // 用户信息
            NSLog(@"Wechat name: %@", resp.name);
            NSLog(@"Wechat iconurl: %@", resp.iconurl);
            NSLog(@"Wechat gender: %@", resp.gender);
            
            // 第三方平台SDK源数据
            NSLog(@"Wechat originalResponse: %@", resp.originalResponse);
            
            weakSelf.thirdPartyLoginHttpMessage.uid = resp.uid;
            weakSelf.thirdPartyLoginHttpMessage.openid = resp.openid;
            weakSelf.thirdPartyLoginHttpMessage.refreshToken = resp.refreshToken;
            weakSelf.thirdPartyLoginHttpMessage.accessToken = resp.accessToken;
            weakSelf.thirdPartyLoginHttpMessage.logoUrl = resp.iconurl;
            // 1男，0女
            if ([resp.gender isEqualToString:@"女"] || [resp.gender.lowercaseString isEqualToString:@"f"]) {
                weakSelf.thirdPartyLoginHttpMessage.gender = @"0";
            }
            else {
                weakSelf.thirdPartyLoginHttpMessage.gender = @"1";
            }
            weakSelf.thirdPartyLoginHttpMessage.nickname = resp.name;
            
            weakSelf.thirdPartyId = ThirdpartyIdWechat;
            
            [weakSelf doThirdpartLogin:weakSelf.thirdPartyLoginHttpMessage type:ThirdpartyIdWechat];
            
        }
    }];
}

- (void)doThirdpartLogin:(ThirdpartyLoginHttpMessage*)baseThirdpartyHttpMessage type:(NSInteger)type
{
    [self showHUDIndicatorViewAtCenter:MSG_IS_LOGING];
    
    WeakSelf
    
    NSString *params = baseThirdpartyHttpMessage.description;
    
    [[UserManager sharedInstance] doThirdPartyLogin:params thirdPartyId:type success:^(NSInteger code_status) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        if (code_status == STATUS_OK) {
            
            [weakSelf handleLoginSuccess];
            
        }
        
    } failure:^(NSInteger code_status) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        if (code_status == 1163) {
            [weakSelf gotoThirdpartyRegister:baseThirdpartyHttpMessage];
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    }];
    
}


-(void)doLogin
{
    
#ifdef RELEASE
    NSString *error;
    if (![UserHelper validateUserName:self.userNameTextField.text.trim error:&error]) {
        [self showAlertView:error];
        return;
    }
//    if (![UserHelper validatePassWord:self.userPasswordTextField.text.trim error:&error]) {
//        [self showAlertView:error];
//        return;
//    }
    
    if (![UserHelper validLoginPwd:self.userPasswordTextField.text.trim error:&error]) {
        [self showAlertView:error];
        return;
    }
#else
    if (IsStrEmpty(self.userNameTextField.text.trim)) {
        [self showAlertView:@"请输入用户名"];
        return;
    }
    if (IsStrEmpty(self.userPasswordTextField.text.trim)) {
        [self showAlertView:@"请输入密码"];
        return;
    }
#endif
    
    
    // 先登录
    [self showHUDIndicatorViewAtCenter:MSG_IS_LOGING];
    WeakSelf
    [[UserManager sharedInstance] doLogin:self.userNameTextField.text.trim password:self.userPasswordTextField.text.trim success:^(NSInteger code_status) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
                if (code_status == STATUS_OK) {
        
                    [strongSelf handleLoginSuccess];
                    
//                    [strongSelf handleLoginSuccess];
        
                }
                else{
                    [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
                }
        
    } failure:^(NSInteger code_status) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        //        [strongSelf showAlertView:MSG_LOGIN_FAILURE];
        
    }];
    
}

//-(void)dismissLoginWindow
//{
//    [MsgToolBox showToast:@"登录成功"];
//    
//    AppDelegate *appDelegate = APP_DELEGATE;
//    [appDelegate initResignTime];
//    appDelegate.receiveLoginOverTimeMessageCount = 0;
//    receiveAuthMessageCount = NO;
//    
//    if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
//        [self.loginDelegate onLoginSuccess];
//    }
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
//    [self dismissToRootViewController:YES completion:nil];
//}
//
//- (void)handleLoginSuccess
//{
//    WeakSelf
//    // 如果未设置手势密码
//    if (![[UserManager sharedInstance] gesturePasswordIsExit]) {
//        
//        BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"设置手势密码" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定"];
//        
//        [alertView setConfirmBlock:^{
//            
//            [weakSelf presentGestureSetting:SOURCE_FROM_LOGIN];
//            
//        }];
//        
//        [alertView setCancelBlock:^{
//            
//            [weakSelf dismissLoginWindow];
//            
//        }];
//        
//        [alertView show];
//        
//        
//#if 0 // 产品说要统一弹窗样式
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息" message:@"您尚未设置手势密码\n是否立即设置？" delegate:nil cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
//        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//            if (buttonIndex == 1) {
//                [weakSelf presentGestureSetting:SOURCE_FROM_LOGIN];
//            }
//            else {
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"登录成功" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
//                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//                    if (buttonIndex == 0) {
//
//                        AppDelegate *appDelegate = APP_DELEGATE;
//                        [appDelegate initResignTime];
//                        appDelegate.receiveLoginOverTimeMessageCount = 0;
//                        receiveAuthMessageCount = NO;
//                        
//                        if (weakSelf.loginDelegate && [weakSelf.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
//                            [weakSelf.loginDelegate onLoginSuccess];
//                        }
//                        
//                        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
//                        [weakSelf dismissToRootViewController:YES completion:nil];
//
//                    }
//                    
//                }];
//            }
//        }];
//#endif
//    }
//    else {
//        
//        [self dismissLoginWindow];
//        
////        [MsgToolBox showToast:@"登录成功"];
////        
////        AppDelegate *appDelegate = APP_DELEGATE;
////        [appDelegate initResignTime];
////        appDelegate.receiveLoginOverTimeMessageCount = 0;
////        receiveAuthMessageCount = NO;
////        
////        if (weakSelf.loginDelegate && [weakSelf.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
////            [weakSelf.loginDelegate onLoginSuccess];
////        }
////        
////        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
////        [weakSelf dismissToRootViewController:YES completion:nil];
//
//#if 0 // 产品说要统一弹窗样式
//        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"登录成功" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
//        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//            
//            if (buttonIndex == 0) {
//                
//                AppDelegate *appDelegate = APP_DELEGATE;
//                [appDelegate initResignTime];
//                appDelegate.receiveLoginOverTimeMessageCount = 0;
//                receiveAuthMessageCount = NO;
//                
//                if (weakSelf.loginDelegate && [weakSelf.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
//                    [weakSelf.loginDelegate onLoginSuccess];
//                }
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
//                [weakSelf dismissToRootViewController:YES completion:nil];
//                
//            }
//            
//        }];
//#endif
//    }
//    
////    AppDelegate *appDelegate = APP_DELEGATE;
////    [appDelegate initResignTime];
////    appDelegate.receiveLoginOverTimeMessageCount = 0;
////    receiveAuthMessageCount = NO;
////    
////    if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
////        [self.loginDelegate onLoginSuccess];
////    }
////    
////    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
////    [self dismissToRootViewController:YES completion:nil];
//    
//}




@end
