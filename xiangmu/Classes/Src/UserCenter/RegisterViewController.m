//
//  RegisterViewController.m
//  xiangmu
//
//  Created by David kim on 16/4/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RegisterViewController.h"
#import "MyUtil.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "UserManager.h"
#import "HttpConstants.h"
#import "UCHelper/UserHelper.h"
#import "ProtocolRegisterViewController.h"
#import "RegisterUserInfoViewController.h"
#import "UserInfoViewController.h"
#import "RegisterViewController+NetworkRequest.h"

@interface RegisterViewController ()

//@property (nonatomic, strong) UITextView 
@end

@implementation RegisterViewController

- (ThirdpartyRegisterView*)thirdpartyRegisterView
{
    if (!_thirdpartyRegisterView) {
        _thirdpartyRegisterView = [ThirdpartyRegisterView customView];
    }
    return _thirdpartyRegisterView;
}

- (RegisterView*)registerView
{
    if (!_registerView) {
        _registerView = [RegisterView customView];
    }
    return _registerView;
}


- (id)init
{
    self = [super init];
    
    if (self) {
        
        self.title = @"注册";
        
        bPopupLoginView = NO;
        
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    bPopupLoginView = NO;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];

}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    bPopupLoginView = YES;
    
    [self.navigationController setNavigationBarHidden:NO];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];

    // Do any additional setup after loading the view.
    if (self.registerType == RegisterTypeMobile) {
        [self.view addSubview:self.registerView];
        [self.registerView.registerButton addTarget:self action:@selector(doRegister) forControlEvents:UIControlEventTouchUpInside];
        self.mobileTextField = self.registerView.accountTextField;
        self.verifyCodeBtn = self.registerView.verifyCodeButton;
        [self.verifyCodeBtn addTarget:self action:@selector(doGetVerifyCode) forControlEvents:UIControlEventTouchUpInside];
        [self.registerView.backButton addTarget:self action:@selector(clickBackButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.registerView.registerProtocolButton addTarget:self action:@selector(clickMember:) forControlEvents:UIControlEventTouchUpInside];
        
//        [self addProtocolView];
    }
    else {
        [self.view addSubview:self.thirdpartyRegisterView];
        self.mobileTextField = self.thirdpartyRegisterView.accountTextField;
        [self.thirdpartyRegisterView.registerButton addTarget:self action:@selector(doRegisterThirdparty) forControlEvents:UIControlEventTouchUpInside];
        [self.thirdpartyRegisterView.closeButton addTarget:self action:@selector(clickBackButton:) forControlEvents:UIControlEventTouchUpInside];
        self.verifyCodeBtn = self.thirdpartyRegisterView.verifyCodeButton;
        [self.verifyCodeBtn addTarget:self action:@selector(doGetVerifyCode) forControlEvents:UIControlEventTouchUpInside];
        
    }

    
}

//- (void)addProtocolView
//{
////    NSString *cotnent = @"注册账号即表示同意尊重
//    
//    UIView *view = [UIView new];
//    
//    NSInteger labelY = 0;//self.registerView.registerButton.bottom+20*Scale;
//    NSString *contentStr = @"注册帐号即表示同意遵守《 爱佣服务条款 》";
//    CGSize contentSize = [contentStr sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
//    UILabel *label=[MyUtil createLabelFrame:CGRectMake((ViewWidth-contentSize.width)/2, labelY, contentSize.width, 20) title:contentStr font:[UIFont systemFontOfSize:12] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor whiteColor]];
//    
//    
//    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:contentStr];
//    //设置：在0-3个单位长度内的内容显示成红色
//    [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(12, 7)];
//    label.attributedText = str;
//    [view addSubview:label];
////    [self.registerView.tpkeyboardAvoidingScrollView addSubview:label];
//    
//    
//    NSString *subContent =  @"注册帐号即表示同意遵守《 ";
//    CGSize subContentSize = [StringUtil sizeForString:subContent fontSize:12 andWith:MAXFLOAT];
//    NSString *buttonText = @"爱佣服务条款";
//    CGFloat buttonWidth = [StringUtil widthForString:buttonText fontSize:12 andWidth:MAXFLOAT];
//    
//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(label.left+subContentSize.width, labelY, buttonWidth, 20)];
//    [button setBackgroundColor:[UIColor clearColor]];
//    [button addTarget:self action:@selector(clickMember:) forControlEvents:UIControlEventTouchUpInside];
//    [view addSubview:button];
////    [self.registerView.tpkeyboardAvoidingScrollView addSubview:button];
//    
//    UIView *lineView = [UIView new];
//    lineView.backgroundColor = [UIColor whiteColor];
//    lineView.frame = CGRectMake(label.left, label.bottom+5, label.width, 0.5);
//    [view addSubview:lineView];
//    
//    view.frame = CGRectMake(0, self.registerView.registerButton.bottom+20*Scale, ViewWidth, 20);
//    [self.registerView.tpkeyboardAvoidingScrollView addSubview:view];
//    
//}

- (BOOL)prefersStatusBarHidden
{
    return YES;//隐藏为YES，显示为NO
}

- (void)clickBackButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dealloc
{
    [self.verifyCodeBtn cancelTimer];
}

//-(void)createInterface
//{
//    
//    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake((ViewWidth-253)/2, 50, 253, 66)];
//    imageView.image=[UIImage imageNamed:@"广告语"];
//    [self.scrollview addSubview:imageView];
//    
//    NSInteger tfStartY = 135;
//    NSArray *nameArray=@[@"请输入手机号", /*@"推荐人手机号",*/ @"请输入验证码", @"请输入密码(至少包含1个英文字母)",@"确认密码(至少包含1个英文字母)"/*,@"昵称",@"公司名称(非必填)", */];
//    for (int i=0; i<nameArray.count; i++)
//    {
//        UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(0, tfStartY+40*i,ViewWidth, 40)];
//        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 30*i, 20, 30)];
//        textField.tag=100+i;
//        textField.leftView=view;
//        textField.layer.masksToBounds=YES;
//        textField.leftViewMode=UITextFieldViewModeAlways;
//        textField.placeholder=nameArray[i];
//        textField.layer.borderWidth=0.5;
//        textField.backgroundColor=[UIColor whiteColor];
//        textField.font=[UIFont systemFontOfSize:13];
//        textField.layer.borderColor=[[UIColor colorWithRed:212.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]CGColor];
//        if (i == TFTypeMobile /*|| i == TFTypeRefMobile*/) {
//            textField.keyboardType = UIKeyboardTypeNumberPad;
//        }
//        else if (i == TFTypeVerifyCode){// 验证码
//            UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(ViewWidth-130, 0, 70, 30)];
//            //rightView.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:193.0f/255.0f blue:65.0f/255.0f alpha:1.0f];
//            UIButton *sendYanZheng=[UIButton buttonWithType:UIButtonTypeCustom];
//            [sendYanZheng setFrame:CGRectMake(0,0,70,30)];
//            [sendYanZheng addTarget:self action:@selector(doGetVerifyCode) forControlEvents:UIControlEventTouchUpInside];
//            [sendYanZheng setTitle:@"获取验证码" forState:UIControlStateNormal];
//            sendYanZheng.titleLabel.font=[UIFont systemFontOfSize:13];
//            sendYanZheng.titleLabel.textAlignment=NSTextAlignmentCenter;
//            sendYanZheng.backgroundColor = UIColorFromRGB(0x27558c);
//            [sendYanZheng setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
////            sendYanZheng.layer.cornerRadius = 5;
//            sendYanZheng.cornerRadius = 5;
//            [sendYanZheng setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
//            [sendYanZheng setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x27558c)] forState:UIControlStateNormal];
//            self.verifyCodeBtn = sendYanZheng;
//            [rightView addSubview:sendYanZheng];
//            textField.rightView = rightView;
//            textField.keyboardType = UIKeyboardTypeNumberPad;
//            textField.rightViewMode = UITextFieldViewModeAlways;
//        }
//        else if(i == TFTypePassword || i == TFTypeRePassword) { // 密码
//            textField.keyboardType = UIKeyboardTypeDefault;
//            textField.secureTextEntry = YES;
//        }
//        else {
//            textField.keyboardType = UIKeyboardTypeDefault;
//        }
//        [self.scrollview addSubview:textField];
//        
//    }
//    
//    NSInteger labelY = tfStartY + 40 * 5 + 10;
//    NSString *contentStr = @"注册帐号即表示同意遵守《 爱佣服务条款 》";
//    CGSize contentSize = [contentStr sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
//    UILabel *label=[MyUtil createLabelFrame:CGRectMake((ViewWidth-contentSize.width)/2, labelY, contentSize.width, 20) title:contentStr font:[UIFont systemFontOfSize:12] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:188.0f/255.0f green:188.0f/255.0f blue:188.0f/255.0f alpha:1.0f]];
//
//
//    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:contentStr];
//    //设置：在0-3个单位长度内的内容显示成红色
//    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:71.0f/255.0f green:107.0f/255.0f blue:154.0f/255.0f alpha:1.0f] range:NSMakeRange(12, 7)];
//    label.attributedText = str;
//    [self.scrollview addSubview:label];
//    
//    
//    NSString *subContent =  @"注册帐号即表示同意遵守《 ";
//    CGSize subContentSize = [StringUtil sizeForString:subContent fontSize:12 andWith:MAXFLOAT];
//    NSString *buttonText = @"爱佣服务条款";
//    CGFloat buttonWidth = [StringUtil widthForString:buttonText fontSize:12 andWidth:MAXFLOAT];
//    
//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(label.left+subContentSize.width, labelY, buttonWidth, 20)];
//    [button setBackgroundColor:[UIColor clearColor]];
//    [button addTarget:self action:@selector(clickMember:) forControlEvents:UIControlEventTouchUpInside];
//    [self.scrollview addSubview:button];
//    
//    // 注册按钮 edited by wyy 2016/11/10
//    NSInteger btnY = labelY + 60;
////    NSInteger btnY = ViewHeight-60;
//    UIButton *btn=[MyUtil createBtnFrame:CGRectMake(20, btnY, ViewWidth-40, 40) title:@"注    册" bgImageName:nil target:self action:@selector(doRegister)];
//    btn.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:150.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
//    [btn blueStyle];
//    [self.scrollview addSubview:btn];
//    
//    self.scrollview.contentSize = CGSizeMake(ViewWidth, CGRectGetMaxY(btn.frame) + 10);
//    self.scrollview.showsHorizontalScrollIndicator = NO;
//}


-(void)clickMember:(id)sender
{
    ProtocolRegisterViewController *viewController = [[ProtocolRegisterViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}



-(void)gotoNextPage
{
    
//    UserInfoViewController *viewController = [[UserInfoViewController alloc] init];
//    viewController.source_from = SOURCE_FROM_REGISTER;
//    bPopupLoginView = NO;
//    [self.navigationController pushViewController:viewController animated:YES];
    
//    RegisterUserInfoViewController
    
    RegisterUserInfoViewController *viewController = [RegisterUserInfoViewController new];
//    bPopupLoginView = NO;
    [self.navigationController pushViewController:viewController animated:YES];
    
}






-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
