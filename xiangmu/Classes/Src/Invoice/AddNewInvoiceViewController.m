//
//  AddNewInvoiceViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AddNewInvoiceViewController.h"
#import "InvoiceAmountTableViewCell.h"
#import "EditInvoiceinfoViewController.h"
#import "InvoiceData.h"
#import "InvoiceManager.h"


@interface AddNewInvoiceViewController ()<InvoiceAmountTableViewCellDelegate>

@property (nonatomic, strong) UILabel *amountLabel;
@property (nonatomic, strong) UIButton *nextStepButton;
@property (nonatomic, strong) UIButton *addAmountButton;

@property (nonatomic, assign) NSUInteger tableRowCount;

@property (nonatomic, assign) CGFloat limitAmount;// 可开额度
@property (nonatomic, assign) CGFloat totalAmount;// 实际开的额度

@property (nonatomic, strong) NSMutableArray<InvoiceData*> *amountList;

@end

@implementation AddNewInvoiceViewController

//-(InvoiceinfoHttpMessage*)invoiceinfoHttpMessage
//{
//    if (!_invoiceinfoHttpMessage) {
//        _invoiceinfoHttpMessage = [InvoiceinfoHttpMessage new];
//    }
//    return _invoiceinfoHttpMessage;
//}

-(NSMutableArray*)amountList
{
    if (!_amountList) {
        _amountList = [NSMutableArray array];
    }
    return _amountList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"金额开票";
    
    self.view.backgroundColor = UIColorFromRGB(0xEBEBEB);
    
    [self registKVO];
    
//    self.limitAmount = 10000;
    
    self.tableRowCount = 0;
    
    [self.view addSubview:self.tpkeyboardAvoidingGroupTableView];
    
    [self initHeaderView];
    
    self.invoiceinfoHttpMessage = [InvoiceManager sharedInstance].invoiceinfoHttpMessage;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self getAvailabelAmount];
    
}

-(void)viewWillLayoutSubviews
{
    self.nextStepButton.top = self.view.frame.size.height-74;
    
    self.tpkeyboardAvoidingGroupTableView.frame = CGRectMake(0, 128, ViewWidth, self.view.height-128-74-5);
}

-(void)initHeaderView
{
    _amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 24, ViewWidth, 20)];
    _amountLabel.font = FONT(17);
    _amountLabel.textColor = [UIColor blackColor];
    _amountLabel.text = @"本次最高可开0.00元";
    _amountLabel.textAlignment = UITextAlignmentCenter;
//    [_amountLabel sizeToFit];
    [self.view addSubview:_amountLabel];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, _amountLabel.bottom+5, ViewWidth, 15)];
    label.font = FONT(13);
    label.text = @"您可以同时添加多张发票";
    label.textColor = UIColorFromRGB(0x313131);
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = UITextAlignmentCenter;
//    [label sizeToFit];
    [self.view addSubview:label];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:@"icon_add_amount"] forState:UIControlStateNormal];
    button.frame = CGRectMake((ViewWidth-133)/2, 75, 133, 42);
    [button addTarget:self action:@selector(onClickAddamount:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [okButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xe06430)] forState:UIControlStateNormal];
    okButton.frame = CGRectMake(10, self.view.frame.size.height-74, ViewWidth-20, 45);
    okButton.titleLabel.font = FONT(16);
    [okButton setTitle:@"下一步" forState:UIControlStateNormal];
    [okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(gotoNextPage) forControlEvents:UIControlEventTouchUpInside];
    okButton.cornerRadius = 5;
    [self.view addSubview:okButton];
    self.nextStepButton = okButton;
    
    
}

-(void)dealloc
{
    [self unRegistKVO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 添加金额

-(void)onClickAddamount:(UIButton*)button
{
    if (self.limitAmount == 0) {
        [MsgToolBox showToast:@"您当前可开发票额度为0"];
        return;
    }
    self.tableRowCount++;
    [self.tpkeyboardAvoidingGroupTableView reloadData];

    InvoiceData *invoiceData = [InvoiceData new];
    invoiceData.amount = 0;
    invoiceData.times = 1;
    [self.amountList addObject:invoiceData];
    
}

#pragma mark - 下一步

-(void)gotoNextPage
{
    
    if (self.limitAmount == 0) {
        [MsgToolBox showToast:@"您当前可开发票额度为0"];
        return;
    }
    
    // 开票金额是否小于可开票金额
    if (self.totalAmount == 0) {
        [MsgToolBox showToast:@"请输入开票金额"];
        return;
    }
    if (self.totalAmount > self.limitAmount) {
        [MsgToolBox showToast:@"开票金额不能大于可开票金额"];
        return;
    }
    
    
    __block NSInteger invoiceQuantity = 0;
    [self.amountList enumerateObjectsUsingBlock:^(InvoiceData *data, NSUInteger idx, BOOL * _Nonnull stop) {
        invoiceQuantity += data.times;
    }];
    
    self.invoiceinfoHttpMessage.totalAmount = [NSNumber numberWithInteger:self.totalAmount];
    self.invoiceinfoHttpMessage.totalQuantity = [NSNumber numberWithInteger:invoiceQuantity];
    
    EditInvoiceinfoViewController *viewController = [[EditInvoiceinfoViewController alloc] init];
    viewController.invoiceinfoHttpMessage = self.invoiceinfoHttpMessage;
    viewController.amountList = self.amountList;
    [self.navigationController pushViewController:viewController animated:YES];
    
    
//    [self gotoPage:@"EditInvoiceinfoViewController"];
}

#pragma mark - AmountDelegate method

-(void)onInvoiceAmountChanged:(CGFloat)amount tag:(NSInteger)tag;
{
    InvoiceData *data = self.amountList[tag];
    data.amount = amount;
    
    [self calculateInvoiceAmount];
}

-(void)onTimesValueChanged:(NSInteger)times tag:(NSInteger)tag;
{
    // 去掉某一行的金额
    if (times == 0 && tag > 0) {
        self.tableRowCount--;
        [self.tpkeyboardAvoidingGroupTableView reloadData];
        [self.amountList removeObjectAtIndex:tag];
        [self calculateInvoiceAmount];
    }
    else {
        InvoiceData *data = self.amountList[tag];
        data.times = times;
        [self calculateInvoiceAmount];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.tableRowCount;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.00001f;
}

//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    if (section == [self numberOfSectionsInTableView:tableView]-1) {
//        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 77)];
//        
//        UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [okButton setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xe06430)] forState:UIControlStateNormal];
//        okButton.frame = CGRectMake(10, 23, ViewWidth-20, 45);
//        okButton.titleLabel.font = FONT(16);
//        [okButton setTitle:@"提交" forState:UIControlStateNormal];
//        [okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        okButton.cornerRadius = 5;
//        [view addSubview:okButton];
//        
//        return view;
//    }
//    
//    return [UIView new];
//}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    InvoiceAmountTableViewCell *cell = [InvoiceAmountTableViewCell cellWithTableView:tableView];

    cell.tag = indexPath.section;
    cell.amountTextField.tag = indexPath.section;
    cell.addButton.tag = indexPath.section;
    cell.minusButton.tag = indexPath.section;
    cell.timesTextField.tag = indexPath.section;
    
    InvoiceData *data = self.amountList[indexPath.section];
    cell.amountTextField.text = data.amount == 0 ? @"" : [NSString stringWithFormat:@"%.f", data.amount];
    cell.timesTextField.text = [NSString stringWithFormat:@"%ld", (long)data.times];

    cell.amountDelegate = self;
    
    if (cell.tag == self.tableRowCount - 1) {
        [cell.amountTextField becomeFirstResponder];
    }
    
    return cell;
    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //
    //    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
    //        [self.customDelegate didSelectRowAtIndexPath:indexPath];
    //    }
    
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark KVO

- (void)registKVO
{
    [self addObserver:self forKeyPath:@"amountList" options:NSKeyValueObservingOptionNew context:NULL];
    
    [self addObserver:self forKeyPath:@"totalAmount" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)unRegistKVO
{
    [self removeObserver:self forKeyPath:@"amountList"];
    
    [self removeObserver:self forKeyPath:@"totalAmount"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"amountList"])
    {
        [self calculateInvoiceAmount];
//        if (_totalMoney == 0) {
//            self.payButton.enabled = NO;
//            self.totalLabel.text = @"";
//        }
//        else {
//            self.payButton.enabled = YES;
//            self.totalLabel.text = [NSString stringWithFormat:@"合计：¥%ld", (long)self.totalMoney];
//            [self.totalLabel setTextColor:UIColorFromRGB(0xD51618) text:[NSString stringWithFormat:@"¥%ld", (long)self.totalMoney]];
//        }
    }
    else if ([keyPath isEqualToString:@"totalAmount"])
    {
//        [self.nextStepButton setTitle:[NSString stringWithFormat:@"¥%.f", self.totalAmount] forState:UIControlStateNormal];
    }
}

-(void)calculateInvoiceAmount
{
    self.totalAmount = 0;
    
    [self.amountList enumerateObjectsUsingBlock:^(InvoiceData *data, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSInteger amount = data.amount * data.times;
        
        self.totalAmount += amount;
        
        
    }];
}

#pragma mark - 查询开票额度

-(void)getAvailabelAmount
{
    [self showHUDIndicatorViewAtCenter:@"正在查询可开发票额度，请稍候..."];
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(GET_INVOICE_AVAILABE_AMOUNT_API) requestMethod:POST params:nil success:^(id responseData) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSString *data = responseData[@"data"];
            
            weakSelf.limitAmount = [data integerValue];
            
            weakSelf.amountLabel.text  = [NSString stringWithFormat:@"本次最高可开%@元", data];
            
            // 如果金额为0
            if (weakSelf.limitAmount == 0) {
                
                self.nextStepButton.enabled = NO;
                
            }
            
        }
        else {
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
        
    }];
}


@end
