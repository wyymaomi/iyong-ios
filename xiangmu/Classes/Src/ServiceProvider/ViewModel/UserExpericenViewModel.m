//
//  UserExpericenViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "UserExpericenViewModel.h"
//#import "OnlineServiceModel.h"


@implementation UserExpericenViewModel

- (void)setServiceModel:(UserExpericeModel *)serviceModel
{
    _serviceModel = serviceModel;
    //    _moment = moment;
//    //计算主体Frame
    [self setMomentsBodyFrames];
//    //计算工具条Frame
    [self setMomentsToolBarFrames];
//    [self setMomentsToolBarFrames];
//    //计算CellHeight
    [self setCellHeight];
}

//计算Code圈主体Frame
- (void)setMomentsBodyFrames{
    
    // 公司基本信息
    //    CGFloat companyX = 0;
    //    CGFloat companyY = 0;
    //    CGFloat companyWidth = ViewWidth;
    //    CGFloat companyHeight = 50;
    //    self.bodyCompanyFrame = CGRectMake(companyX, companyY, companyWidth, companyHeight);
    
    _nickNameFont = FONT(18);
    _timeFont = FONT(15);
    _contentFont = FONT(15);
    
    //头像
    CGFloat iconWidth = 40;
    CGFloat iconHeight = 40;
    CGFloat iconY = 15;
    CGFloat iconX = ViewWidth-iconWidth-24;
    self.bodyIconFrame = CGRectMake(iconX, iconY, iconWidth, iconHeight);
    
    //昵称
//    CGFloat nameX = circleCelliconWH + circleCellMargin * 2;
//    CGFloat nameY = iconY;
//    //    CGFloat nameW = 120;
//    CGSize nameSize = [self.serviceModel.nickname sizeWithAttributes:circleCellNameattributes];
//    self.bodyNameFrame = (CGRect){{nameX,nameY},{nameSize.width, iconH/2}};
    CGSize nameSize = [self.serviceModel.nickname textSize:CGSizeMake(ViewWidth/2, MAXFLOAT) font:_nickNameFont];
    self.bodyNameFrame = (CGRect){{24,iconY},{nameSize.width, 20}};
    
    // 时间
//    CGFloat timeX = nameX;
//    CGFloat timeY =  CGRectGetMaxY(self.bodyNameFrame) + 5;
//    CGSize timeSize = [self.serviceModel.companyName boundingRectWithSize:CGSizeMake(ViewWidth-timeX-circleCellMargin, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:circleCellTimeattributes context:nil].size;
//    self.bodyTimeFrame = (CGRect){{timeX,timeY},timeSize};
    CGSize timeSize = [[self.serviceModel dispalyTime] textSize:CGSizeMake(80, MAXFLOAT) font:_timeFont];
    self.bodyTimeFrame = (CGRect){{CGRectGetMaxX(self.bodyNameFrame)+10,iconY},{timeSize.width, 20}};
    
    // 如果公司名称为空 昵称垂直居中
//    if (IsStrEmpty(self.serviceModel.companyName)) {
//        self.bodyNameFrame = (CGRect){{nameX, nameY}, {nameSize.width, iconH}};
//    }
    
    //正文
//    CGFloat textX = circleCellMargin;
//    CGFloat textY = CGRectGetMaxY(self.bodyIconFrame) + 5;
//    if (IsStrEmpty(self.serviceModel.content)) {
//        self.bodyTextFrame = (CGRect){{textX, CGRectGetMaxY(self.bodyIconFrame) + 1},{0,0}};
//    }
//    else {
//        CGFloat textW = circleCellWidth - circleCellMargin * 2;
//        CGSize textSize = [self.serviceModel.content boundingRectWithSize:CGSizeMake(textW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:circleCellTextattributes context:nil].size;
//        self.bodyTextFrame = (CGRect){{textX,textY},textSize};
//    }
    
    CGSize textSize = [self.serviceModel.content textSize:CGSizeMake(ViewWidth-27*2, MAXFLOAT) font:_contentFont];
    self.bodyTextFrame = (CGRect){{27,CGRectGetMaxY(self.bodyNameFrame)+37},{ViewWidth-27*2, textSize.height}};

    
    
    //图片 (判断是否有图片)  //    if ([self.serviceModel hasPhoto]) {
//        CGFloat photosX = 15;
//        CGFloat photosY = CGRectGetMaxY(self.bodyTextFrame) + 5;
//        CGFloat photosW = (ViewWidth -  15 * 2);
//        CGFloat photosH = 60;
//        self.bodyPhotoFrame = CGRectMake(photosX, photosY, photosW, photosH);
//        //主体frame
//        CGFloat bodyH = CGRectGetMaxY(self.bodyPhotoFrame) + circleCellMargin;
//        self.momentsBodyFrame = CGRectMake(0, 0, circleCellWidth, bodyH);
//    }else {
        //主体frame
        CGFloat bodyH = CGRectGetMaxY(self.bodyTextFrame) + 30;
        self.momentsBodyFrame = CGRectMake(0, 0, ViewWidth, bodyH);
//    }
}

//计算Code圈工具条Frame
- (void)setMomentsToolBarFrames
{
    self.browsesFont = FONT(14);
    self.commentsFont = FONT(14);
    self.likesFont = FONT(14);
    //工具条frame
    CGFloat toolBarX = 0;
    CGFloat toolBarY = CGRectGetMaxY(self.bodyNameFrame)+10;
    CGFloat toolBarW = ViewWidth;
    CGFloat toolBarH = circleCellToolBarHeight;
    self.momentsToolBarFrame = CGRectMake(toolBarX, toolBarY, toolBarW, toolBarH);
    
    CGFloat toolBrowseIconX = 24;
    CGFloat toolBrowseIconY = toolBarY+2;
    CGFloat toolBrowseIconWidth = 15;
    CGFloat toolBrowseIconHeight = 12;
    self.toolViewNumIconFrame = CGRectMake(toolBrowseIconX, toolBrowseIconY, toolBrowseIconWidth, toolBrowseIconHeight);
    
    CGFloat toolBrowseTextX = CGRectGetMaxX(self.toolViewNumIconFrame)+5;
    CGFloat toolBrowseTextY = toolBarY;
    CGSize browseTextSize = [self.serviceModel.browseQuantity textSize:CGSizeMake(ViewWidth, MAXFLOAT) font:self.browsesFont];
    CGFloat toolBrowseTextWidth = browseTextSize.width<20?20:browseTextSize.width;
    CGFloat toolBrowseHeight = 15;
    self.toolViewNumTextFrame = (CGRect){{toolBrowseTextX,toolBrowseTextY},{toolBrowseTextWidth, toolBrowseHeight}};
    
    CGFloat toolCommentIconX = CGRectGetMaxX(self.toolViewNumTextFrame)+10;
    CGFloat toolCommentIconY = toolBarY;
    CGFloat toolCommentIconWidth = 15;
    CGFloat toolCommentIconHeight = 15;
    self.toolCommentNumIconFrame = CGRectMake(toolCommentIconX, toolCommentIconY, toolCommentIconWidth, toolCommentIconHeight);
    
    CGFloat toolCommentTextX = CGRectGetMaxX(self.toolCommentNumIconFrame)+5;
    CGFloat toolCommentTextY = toolBarY;
    CGSize commentTextSize = [self.serviceModel.commentQuantity textSize:CGSizeMake(ViewWidth, MAXFLOAT) font:self.browsesFont];
    CGFloat toolCommentTextWidth = commentTextSize.width<20?20:commentTextSize.width;
    CGFloat toolCommentTextHeight = 15;
//    CGSize commentsTextSize = [self.serviceModel.commentQuantity textSize:CGSizeMake(50, MAXFLOAT) font:self.commentsFont];
    self.toolCommentNumTextFrame = (CGRect){{toolCommentTextX, toolCommentTextY}, {toolCommentTextWidth,toolCommentTextHeight}};
    
    CGFloat toolLikeIconX = CGRectGetMaxX(self.toolCommentNumTextFrame)+10;
    CGFloat toolLikeIconY = toolBarY;
    CGFloat toolLikeIconWidth = 15;
    CGFloat toolLikeIconHeight = 15;
    self.toolLikeIconFrame = CGRectMake(toolLikeIconX, toolLikeIconY, toolLikeIconWidth, toolLikeIconHeight);
    
    CGFloat toolLikeTextX = CGRectGetMaxX(self.toolLikeIconFrame)+5;
    CGFloat toolLikeTextY = toolBarY;
//    CGFloat t
    CGSize likeTextSize = [self.serviceModel.thumbsQuantity textSize:CGSizeMake(ViewWidth, MAXFLOAT) font:self.likesFont];
    CGFloat toolLikeTextWidth = likeTextSize.width<20?20:likeTextSize.width;
    CGFloat toolLikeTextHeight = 15;
//    CGSize likesTextSize = [self.serviceModel.thumbsQuantity textSize:CGSizeMake(50, MAXFLOAT) font:self.likesFont];
    self.toolLikeTextFrame = CGRectMake(toolLikeTextX, toolLikeTextY, toolLikeTextWidth, toolLikeTextHeight);
    
    // 通话
//    CGFloat telX = 0;
//    CGFloat telY = 0;
//    CGFloat telW = toolBarW/3;
//    CGFloat telH = circleCellToolBarHeight;
//    self.toolTelFrame = CGRectMake(telX, telY, telW, telH);
    
    //收藏
//    CGFloat commentX = telX + telW + 1;
//    CGFloat commentY = 0;
//    CGFloat commentW = telW;
//    CGFloat commentH = telH;
//    self.toolFavoriteFrame = CGRectMake(commentX, commentY, commentW, commentH);
    
    //点赞
//    CGFloat likeX = commentX + commentW + 1;
//    CGFloat likeY = 0;
//    CGFloat likeW = toolBarW / 3;
//    CGFloat likeH = circleCellToolBarHeight;
//    self.toolLikeFrame = CGRectMake(likeX, likeY, likeW, likeH);
}

- (void)setCellHeight{
    
    self.cellHeight = CGRectGetMaxY(self.momentsBodyFrame);
//    self.cellHeight = CGRectGetMaxY(self.momentsToolBarFrame);
}


@end
