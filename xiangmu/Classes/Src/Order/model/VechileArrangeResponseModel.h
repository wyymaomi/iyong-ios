//
//  VechileArrangeModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@interface VechileArrangeResponseModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *orderId;
@property (nonatomic, strong) NSString<Optional> *carId;// 车辆ID
@property (nonatomic, strong) NSNumber<Optional> *confirm;
@property (nonatomic, strong) NSString<Optional> *number;// 车牌号
@property (nonatomic, strong) NSString<Optional> *driver;// 司机名字
@property (nonatomic, strong) NSString<Optional> *model;// 车型
@property (nonatomic, strong) NSString<Optional> *mobile;// 司机手机号
@property (nonatomic, strong) NSString<Optional> *imgUrl;// 车辆图片下载地址

@end
