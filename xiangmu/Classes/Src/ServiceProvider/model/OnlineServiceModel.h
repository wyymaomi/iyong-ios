//
//  OnlineServiceModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol OnlineServiceModel <NSObject>

@end

@protocol OnlineServiceModelList <NSObject>

@end

@interface OnlineServiceModel : JSONModel

//@property (nonatomic, strong) NSString<Optional> *business;// 经营描述(业务范围)
@property (nonatomic, strong) NSString<Optional> *img1;
@property (nonatomic, strong) NSString<Optional> *img2;
@property (nonatomic, strong) NSString<Optional> *img3;
@property (nonatomic, strong) NSString<Optional> *img4;
@property (nonatomic, strong) NSString<Optional> *thumbsQuantity;// 点赞数量
@property (nonatomic, strong) NSNumber<Optional> *thumbsTime;// 最后一次点赞时间
@property (nonatomic, strong) NSString<Optional> *companyName;// 公司名
@property (nonatomic, strong) NSString<Optional> *nickname;// 昵称
@property (nonatomic, strong) NSNumber<Optional> *companyScor;// 公司评分
@property (nonatomic, strong) NSString<Optional> *logoUrl;// 公司LOGO
@property (nonatomic, strong) NSString<Optional> *mobile;// 手机号码
@property (nonatomic, strong) NSNumber<Optional> *collection;//是否已收藏
@property (nonatomic, strong) NSString<Optional> *companyId; // 公司ID
@property (nonatomic, strong) NSString<Optional> *collectionTime;
@property (nonatomic, strong) NSNumber<Optional> *thumbsUp;// 是否已经点过赞
@property (nonatomic, strong) NSString<Optional> *commentQuantity; // 评论数量
@property (nonatomic, strong) NSArray<Optional> *imgs; // 图片数组
@property (nonatomic, strong) NSArray<Optional> *business; // 业务范围
@property (nonatomic, strong) NSString<Optional> *areaCode;
@property (nonatomic, strong) NSString<Optional> *blurb;
@property (nonatomic, strong) NSNumber<Optional> *collectionQuantity;
@property (nonatomic, strong) NSNumber<Optional> *browseQuantity;
//@property (nonatomic, strong) NSNumber<Optional> *browseQuantity;
//@property (nonatomic, strong) NSString<Optional> *commentQuantity; // 评论数量

- (BOOL)hasPhoto;

@end

@interface OnlineServiceModelList : JSONModel

@property (nonatomic, assign) NSNumber<Optional> *pageIndex;// 当前页码
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel, Optional> *list;// 列表

@end


@interface OnlineServiceResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) OnlineServiceModelList<Optional> *data;

@end


