//
//  GuideView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "GuideView.h"

@implementation GuideView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self initView];
        
    }
    
    return self;
}

- (void)initView;
{
    self.imageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.imageView.image = [UIImage imageNamed:@"guide_page0"];
    [self addSubview:self.imageView];
//    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
////    bgImage.image = [UIImage imageNamed:@"guide_page5"];
//    bgImage.image = [UIImage imageNamed:@"guide_page0"];
//    bgImage.contentMode = UIViewContentModeScaleToFill;
//    [self addSubview:bgImage];
    
    
    [self addSubview:self.okButton];
}

-(UIButton*)okButton
{
    if (_okButton == nil) {
        _okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _okButton.frame = CGRectMake(ViewWidth-128/2, ViewHeight-140, 128, 40);
        _okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _okButton.frame = CGRectMake((ViewWidth-128)/2, ViewHeight-140, 128, 40);
        [_okButton setTitle:@"我知道了" forState:UIControlStateNormal];
        [_okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _okButton.backgroundColor = [UIColor clearColor];
        _okButton.cornerRadius = 5;
        _okButton.borderColor = [UIColor whiteColor];
        _okButton.borderWidth = 0.5;
        _okButton.enabled = YES;
        _okButton.userInteractionEnabled = YES;
//        [self addSubview:_okButton];
    }
    return _okButton;
}

@end
