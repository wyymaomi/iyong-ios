//
//  VechileManageTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/30.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VechileManageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *vechileModelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *vechileImageView;
@property (weak, nonatomic) IBOutlet UIButton *operationButton;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UILabel *vechileNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverMobile;
@property (weak, nonatomic) IBOutlet UILabel *monthDistanceLabel;

+(CGFloat)getCellHeight;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
