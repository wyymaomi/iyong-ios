//
//  InvoiceAmountTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InvoiceAmountTableViewCellDelegate<NSObject>

-(void)onTimesValueChanged:(NSInteger)times tag:(NSInteger)tag;

-(void)onInvoiceAmountChanged:(CGFloat)amount tag:(NSInteger)tag;

@end


@interface InvoiceAmountTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *minusButton;

@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (weak, nonatomic) IBOutlet UITextField *amountTextField; // 金额文本输入框

@property (weak, nonatomic) IBOutlet UITextField *timesTextField;// 倍数文本输入框

@property (nonatomic, weak) id<InvoiceAmountTableViewCellDelegate> amountDelegate;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
