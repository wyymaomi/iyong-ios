//
//  HomeExperienceTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeExperienceCollectionView.h"

@interface HomeExperienceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (nonatomic, strong) HomeExperienceCollectionView *collectionView;

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@property (nonatomic, strong) NSMutableArray *dataList;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@end
