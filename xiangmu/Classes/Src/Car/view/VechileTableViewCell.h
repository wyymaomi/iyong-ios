//
//  CarTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VechileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *carNumberLbl;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *driverMobile;
@property (weak, nonatomic) IBOutlet UIImageView *vechileImageView;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;

@end
