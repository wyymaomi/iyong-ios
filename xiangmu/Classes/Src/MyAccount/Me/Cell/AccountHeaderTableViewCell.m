//
//  AccountHeaderTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AccountHeaderTableViewCell.h"

@implementation AccountHeaderTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    
    static NSString *ID =  @"CommunityHeaderTableViewCell";
    AccountHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    cell.nickNameLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.nickname] ;
//    cell.mobileLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.username];
//    cell.companyLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.companyName];
//    cell.usernameLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.nickname];
    
    cell.usernameLabel.text = [[UserManager sharedInstance].userModel displayNickname];
    cell.avatarImageView.cornerRadius = cell.avatarImageView.frame.size.width/2;
    cell.avatarImageView.borderWidth = 2;
    cell.avatarImageView.borderColor = [UIColor whiteColor];
    
//    LRWeakSelf(cell)
//    [cell.avatarImageView downloadImage:[UserManager sharedInstance].userModel.logoUrl placeholderImage:[UIImage imageNamed:@"icon_avatar"] success:^(id responseData) {
//
//    } andFailure:^(NSString *errorDesc) {
////        weakcell.avatarImageView.cornerRadius = 5;
//    }];
    
//    [cell.avatarImageView downloadImageFromAliyunOSS:[UserManager sharedInstance].userModel.logoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//        
//    }];
    
    [cell.avatarImageView yy_setImageWithObjectKey:[UserManager sharedInstance].userModel.logoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
        
    }];
    
    
    return cell;
}

+(CGFloat)getCellHeight
{
    return 150;
}

-(void)initData
{
//    BOOL isCompanyCertified = [[UserManager sharedInstance].userModel.companyCertification integerValue] == CompanyCertificationStatusFinished;
//    BOOL isPersonCertified = [[UserManager sharedInstance].userModel.certification boolValue];
//    BOOL isVechileCertified = [[UserManager sharedInstance].userModel.carCertification boolValue];
//    [self.certifiedView initData:isPersonCertified isCompanyCertified:isCompanyCertified isVechileCertified:isVechileCertified];
//    
//    self.starView.width = kStarFrameWith * 5;
//    [self.starView setStarsImages:[[UserManager sharedInstance].userModel.companyScor floatValue]];
    
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
