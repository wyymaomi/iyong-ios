//
//  YYAliyunOSSImageOperation.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "YYAliyunOSSImageOperation.h"
#import "UIImage+ImageDecoder.h"
#import <SDWebImage/UIImage+MultiFormat.h>
//#import <SDWebImage/SDWebImae

#import "UIImage+YYWebImage.h"
#import <ImageIO/ImageIO.h>
#import <libkern/OSAtomic.h>

#if __has_include(<YYImage/YYImage.h>)
#import <YYImage/YYImage.h>
#else
#import "YYImage.h"
#endif

#define MIN_PROGRESSIVE_TIME_INTERVAL 0.2
#define MIN_PROGRESSIVE_BLUR_TIME_INTERVAL 0.4

/// Returns YES if the right-bottom pixel is filled.
static BOOL YYCGImageLastPixelFilled(CGImageRef image) {
    if (!image) return NO;
    size_t width = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);
    if (width == 0 || height == 0) return NO;
    CGContextRef ctx = CGBitmapContextCreate(NULL, 1, 1, 8, 0, YYCGColorSpaceGetDeviceRGB(), kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrderDefault);
    if (!ctx) return NO;
    CGContextDrawImage(ctx, CGRectMake( -(int)width + 1, 0, width, height), image);
    uint8_t *bytes = CGBitmapContextGetData(ctx);
    BOOL isAlpha = bytes && bytes[0] == 0;
    CFRelease(ctx);
    return !isAlpha;
}

/// Returns JPEG SOS (Start Of Scan) Marker
static NSData *JPEGSOSMarker() {
    // "Start Of Scan" Marker
    static NSData *marker = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        uint8_t bytes[2] = {0xFF, 0xDA};
        marker = [NSData dataWithBytes:bytes length:2];
    });
    return marker;
}

@interface YYAliyunOSSImageOperation ()

@property (readwrite, getter=isExecuting) BOOL executing;
@property (readwrite, getter=isFinished) BOOL finished;
@property (readwrite, getter=isCancelled) BOOL cancelled;
@property (readwrite, getter=isStarted) BOOL started;
@property (nonatomic, strong) NSRecursiveLock *lock;
@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *data;
@property (nonatomic, assign) NSInteger expectedSize;
@property (nonatomic, assign) UIBackgroundTaskIdentifier taskID;

@property (nonatomic, assign) NSTimeInterval lastProgressiveDecodeTimestamp;
@property (nonatomic, strong) YYImageDecoder *progressiveDecoder;
@property (nonatomic, assign) BOOL progressiveIgnored;
@property (nonatomic, assign) BOOL progressiveDetected;
@property (nonatomic, assign) NSUInteger progressiveScanedLength;
@property (nonatomic, assign) NSUInteger progressiveDisplayCount;

@property (nonatomic, copy) YYWebImageProgressBlock progress;
@property (nonatomic, copy) YYWebImageTransformBlock transform;
//@property (nonatomic, copy) YYWebImageCompletionBlock completion;
@property (nonatomic, copy) YYAliyunOSSImageCompletionBlock completion;

@end

@implementation YYAliyunOSSImageOperation

@synthesize executing = _executing;
@synthesize finished = _finished;
@synthesize cancelled = _cancelled;


/// Network thread entry point.
+(void)_networkThreadMain:(id)object {
    @autoreleasepool {
        [[NSThread currentThread] setName:@"com.ibireme.webimage.request"];
        NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
        [runLoop addPort:[NSMachPort port] forMode:NSDefaultRunLoopMode];
        [runLoop run];
    }
}

/// Global image request network thread, used by NSURLConnection delegate.
+(NSThread*)_networkThread {
    static NSThread *thread = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        thread = [[NSThread alloc] initWithTarget:self selector:@selector(_networkThreadMain:) object:nil];
        if ([thread respondsToSelector:@selector(setQualityOfService:)]) {
            thread.qualityOfService = NSQualityOfServiceBackground;
        }
        [thread start];
    });
    return thread;
}

//// Global image queue, used for image reading and decoding.
+ (dispatch_queue_t)_imageQueue {
//#ifdef YYDispatchQueuePool_h
//    return YYDispatchQueueGetForQOS(NSQualityOfServiceUserInitiated);
//#else
#define MAX_QUEUE_COUNT 16
    static int queueCount;
    static dispatch_queue_t queues[MAX_QUEUE_COUNT];
    static dispatch_once_t onceToken;
    static int32_t counter = 0;
    dispatch_once(&onceToken, ^{
        queueCount = (int)[NSProcessInfo processInfo].activeProcessorCount;
        queueCount = queueCount < 1 ? 1 : queueCount > MAX_QUEUE_COUNT ? MAX_QUEUE_COUNT : queueCount;
//        queueCount = MAX_QUEUE_COUNT;
        if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
            for (NSUInteger i = 0; i < queueCount; i++) {
                dispatch_queue_attr_t attr = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_UTILITY, 0);
//                dispatch_queue_attr_t attr = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_CONCURRENT, QOS_CLASS_UTILITY, 0);
                queues[i] = dispatch_queue_create("com.ibireme.image.decode", attr);
            }
        } else {
            for (NSUInteger i = 0; i < queueCount; i++) {
                queues[i] = dispatch_queue_create("com.ibireme.image.decode", DISPATCH_QUEUE_SERIAL);
//                queues[i] = dispatch_queue_create("com.ibireme.image.decode", DISPATCH_QUEUE_CONCURRENT);
                dispatch_set_target_queue(queues[i], dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0));
            }
        }
    });
    int32_t cur = OSAtomicIncrement32(&counter);
    if (cur < 0) cur = -cur;
    return queues[(cur) % queueCount];
#undef MAX_QUEUE_COUNT
//#endif
}

-(instancetype) init
{
    @throw [NSException exceptionWithName:@"YYAliyunOSSImageOperation init error" reason:@"YYAliyunOSSImageOperation must be initialized with a request.  Use the designated initializer to init" userInfo:nil];
//    return [self initWithRequestUrl:@"" options:0 cache:nil cacheKey:nil progress:nil completion:nil];
//    return [self initWithRequestUrl:@"" cache:nil cacheKey:nil progress:nil completion:nil];
//    return [self in]
    return nil;
}

-(instancetype)initWithRequestUrl:(NSString*)objectKey
//                          options:(YYWebImageOptions)options
                          imgSize:(CGSize)imgSize
                            cache:(YYImageCache*)cache
                         cacheKey:(NSString*)cacheKey
                         progress:(YYWebImageProgressBlock)progress
                       completion:(YYAliyunOSSImageCompletionBlock)completion
{
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    if (!objectKey) {
        return nil;
    }
    
    
    _objectKey = objectKey;
    _cache = cache;
    _cacheKey = cacheKey;
    _imageSize = imgSize;
    _executing = NO;
    _finished = NO;
    _cancelled = NO;
    _progress = progress;
    _completion = completion;
    _taskID = UIBackgroundTaskInvalid;
    _lock = [NSRecursiveLock new];
    
    return self;
}

-(void)dealloc
{
    [_lock lock];
    if (_taskID != UIBackgroundTaskInvalid) {
        
    }
    if ([self isExecuting]) {
        self.cancelled = YES;
        self.finished = YES;
        if (_getObjectRequest) {
            [_getObjectRequest cancel];
        }
        if (_completion) {
            @autoreleasepool {
                _completion(nil, _objectKey, YYWebImageFromNone, YYWebImageStageCancelled, nil);
            }
        }
    }
    [_lock unlock];
}

-(void)_endBackgroundTask {
    [_lock lock];
    if (_taskID != UIBackgroundTaskInvalid) {

    }
    
    [_lock unlock];
}

#pragma mark - Runs in operation thread

-(void)_finish {
    self.executing = NO;
    self.finished = YES;
    [self _endBackgroundTask];
}

// runs on network thread
-(void)_startOperation {
    
    DLog(@"_startOperation : objectKey = %@", _objectKey);
    
    if ([self isCancelled]) {
        return;
    }
    
    // get image from cache
    @autoreleasepool {
        if (_cache) {
            // get image from memory cache
            UIImage *image = [_cache getImageForKey:_cacheKey];
            if (image) {
                [_lock lock];
                if (![self isCancelled]) {
                    if (_completion) {
                        _completion(image, _objectKey, YYWebImageFromMemoryCache, YYWebImageStageFinished, nil);
                    }
                }
                [self _finish];
                [_lock unlock];
                return;
            }
            __weak typeof(self) _self = self;
            dispatch_async([self.class _imageQueue], ^{
                
                __strong typeof(_self) self = _self;
                
                if (!self || [self isCancelled]) {
                    return;
                }
                
                UIImage *image = [self.cache getImageForKey:self.cacheKey withType:YYImageCacheTypeDisk];
                if (image) {
                    // if image is in disk cache , then save it into memory cache
                    [self.cache setImage:image imageData:nil forKey:self.cacheKey withType:YYImageCacheTypeMemory];
                    [self performSelector:@selector(_didReceiveImageFromDiskCache:) onThread:[self.class _networkThread] withObject:image waitUntilDone:NO];
//                    if (_completion) {
//                        _completion(image, _objectKey, YYWebImageFromDiskCache, YYWebImageStageFinished,nil);
//                    }
                }
                else {
                    [self performSelector:@selector(_startRequest:) onThread:[self.class _networkThread] withObject:nil waitUntilDone:NO];
                }
                
                return;
            });
        }
    }
//    [self _startRequest:nil];
}

// runs on network thread
- (void)_didReceiveImageFromDiskCache:(UIImage *)image {
    @autoreleasepool {
        [_lock lock];
        if (![self isCancelled]) {
            if (image) {
                if (_completion) {
                    _completion(image, _objectKey, YYWebImageFromDiskCache, YYWebImageStageFinished, nil);
                }
//                if (_completion) _completion(image, _request.URL, YYWebImageFromDiskCache, YYWebImageStageFinished, nil);
                [self _finish];
            } else {
                [self _startRequest:nil];
            }
        }
        [_lock unlock];
    }
}

//__strong typeof(_self) self = _self;
//if (!self || [self isCancelled]) return;
//UIImage *image = [self.cache getImageForKey:self.cacheKey withType:YYImageCacheTypeDisk];
//if (image) {
//    [self.cache setImage:image imageData:nil forKey:self.cacheKey withType:YYImageCacheTypeMemory];
//    [self performSelector:@selector(_didReceiveImageFromDiskCache:) onThread:[self.class _networkThread] withObject:image waitUntilDone:NO];
//} else {
//    [self performSelector:@selector(_startRequest:) onThread:[self.class _networkThread] withObject:nil waitUntilDone:NO];
//}

-(void)_startRequest:(id)object {
    
    DLog(@"_startRequest : objectKey = %@", _objectKey);
    
    if ([self isCancelled]) {
        return;
    }
    
    @autoreleasepool {
        
        [_lock lock];
        
        
        
        _getObjectRequest = [OSSGetObjectRequest new];
        _getObjectRequest.bucketName = BucketName;
        _getObjectRequest.objectKey = self.objectKey;
        
        
        
        if (self.imageSize.width > 0 && self.imageSize.height > 0) {
            _getObjectRequest.xOssProcess = [NSString stringWithFormat:@"image/resize,w_%.f,h_%.f,m_fill/auto-orient,1/quality,q_90/sharpen,1/format,src", self.imageSize.width, self.imageSize.height];
        }
        
        _getObjectRequest.downloadProgress = ^(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
//            NSLog(@"%lld, %lld, %lld", bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
        };
        
        OSSTask * getTask = [client getObject:_getObjectRequest];
        
        CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
        
        [getTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                DLog(@"download object success!");
                OSSGetObjectResult *getResult = task.result;
                [self _didDownloadDataSuccess:getResult];
                CFAbsoluteTime endTime = CFAbsoluteTimeGetCurrent();
                DLog(@"objectKey: %@, download take time = %f", _objectKey, endTime-startTime);
                DLog(@"download dota length: %lu", (unsigned long)[getResult.downloadedData length]);
            } else {
                DLog(@"download object failed, error: %@" ,task.error);
            }
            return nil;
        }];
//        [getTask waitUntilFinished];
        
        [_lock unlock];
        
    }
    
}

-(void)didReceiveData:(NSData*)data
{
    BOOL cancelled = [self isCancelled];
    if (cancelled) return;
    
    if (data) {
        if (_data == nil) {
            _data = [NSMutableData new];
        }
        [_data appendData:data];
    }
    
    // progressive
    /*--------------------------- progressive ----------------------------*/
    BOOL progressive = YES;//(_options & YYWebImageOptionProgressive) > 0;
    BOOL progressiveBlur = YES;//(_options & YYWebImageOptionProgressiveBlur) > 0;
    if (!_completion || !(progressive || progressiveBlur)) return;
    if (data.length <= 16) return;
    if (_expectedSize > 0 && data.length >= _expectedSize * 0.99) return;
    if (_progressiveIgnored) return;
    
    NSTimeInterval min = progressiveBlur ? MIN_PROGRESSIVE_BLUR_TIME_INTERVAL : MIN_PROGRESSIVE_TIME_INTERVAL;
    NSTimeInterval now = CACurrentMediaTime();
    if (now - _lastProgressiveDecodeTimestamp < min) return;
    
    if (!_progressiveDecoder) {
        _progressiveDecoder = [[YYImageDecoder alloc] initWithScale:[UIScreen mainScreen].scale];
    }
    [_progressiveDecoder updateData:_data final:NO];
    if ([self isCancelled]) return;
    
    if (_progressiveDecoder.type == YYImageTypeUnknown ||
        _progressiveDecoder.type == YYImageTypeWebP ||
        _progressiveDecoder.type == YYImageTypeOther) {
        _progressiveDecoder = nil;
        _progressiveIgnored = YES;
        return;
    }
    if (progressiveBlur) { // only support progressive JPEG and interlaced PNG
        if (_progressiveDecoder.type != YYImageTypeJPEG &&
            _progressiveDecoder.type != YYImageTypePNG) {
            _progressiveDecoder = nil;
            _progressiveIgnored = YES;
            return;
        }
    }
    if (_progressiveDecoder.frameCount == 0) return;
    
    if (!progressiveBlur) {
        YYImageFrame *frame = [_progressiveDecoder frameAtIndex:0 decodeForDisplay:YES];
        if (frame.image) {
//            [_lock lock];
            if (![self isCancelled]) {
//                _completion(frame.image, _request.URL, YYWebImageFromRemote, YYWebImageStageProgress, nil);
                _completion(frame.image, _objectKey, YYWebImageFromRemote, YYWebImageStageProgress, nil);
                _lastProgressiveDecodeTimestamp = now;
            }
//            [_lock unlock];
        }
        return;
    } else {
        if (_progressiveDecoder.type == YYImageTypeJPEG) {
            if (!_progressiveDetected) {
                NSDictionary *dic = [_progressiveDecoder framePropertiesAtIndex:0];
                NSDictionary *jpeg = dic[(id)kCGImagePropertyJFIFDictionary];
                NSNumber *isProg = jpeg[(id)kCGImagePropertyJFIFIsProgressive];
                if (!isProg.boolValue) {
                    _progressiveIgnored = YES;
                    _progressiveDecoder = nil;
                    return;
                }
                _progressiveDetected = YES;
            }
            
            NSInteger scanLength = (NSInteger)_data.length - (NSInteger)_progressiveScanedLength - 4;
            if (scanLength <= 2) return;
            NSRange scanRange = NSMakeRange(_progressiveScanedLength, scanLength);
            NSRange markerRange = [_data rangeOfData:JPEGSOSMarker() options:kNilOptions range:scanRange];
            _progressiveScanedLength = _data.length;
            if (markerRange.location == NSNotFound) return;
            if ([self isCancelled]) return;
            
        } else if (_progressiveDecoder.type == YYImageTypePNG) {
            if (!_progressiveDetected) {
                NSDictionary *dic = [_progressiveDecoder framePropertiesAtIndex:0];
                NSDictionary *png = dic[(id)kCGImagePropertyPNGDictionary];
                NSNumber *isProg = png[(id)kCGImagePropertyPNGInterlaceType];
                if (!isProg.boolValue) {
                    _progressiveIgnored = YES;
                    _progressiveDecoder = nil;
                    return;
                }
                _progressiveDetected = YES;
            }
        }
        
        YYImageFrame *frame = [_progressiveDecoder frameAtIndex:0 decodeForDisplay:YES];
        UIImage *image = frame.image;
        if (!image) return;
        if ([self isCancelled]) return;
        
        if (!YYCGImageLastPixelFilled(image.CGImage)) return;
        _progressiveDisplayCount++;
        
        CGFloat radius = 32;
        if (_expectedSize > 0) {
            radius *= 1.0 / (3 * _data.length / (CGFloat)_expectedSize + 0.6) - 0.25;
        } else {
            radius /= (_progressiveDisplayCount);
        }
        image = [image yy_imageByBlurRadius:radius tintColor:nil tintMode:0 saturation:1 maskImage:nil];
        
        if (image) {
//            [_lock lock];
            if (![self isCancelled]) {
                _completion(image, _objectKey, YYWebImageFromRemote, YYWebImageStageProgress, nil);
//                _completion(image, _request.URL, YYWebImageFromRemote, YYWebImageStageProgress, nil);
                _lastProgressiveDecodeTimestamp = now;
            }
//            [_lock unlock];
        }
    }
    
    
}

// runs on network thread, called from outer "cancel"
-(void)_cancelOperation {
    @autoreleasepool {
        if (_getObjectRequest) {
            [_getObjectRequest cancel];
            _getObjectRequest = nil;
        }
        if (_completion) {
            _completion(nil, _objectKey, YYWebImageFromNone, YYWebImageStageCancelled, nil);
            [self _endBackgroundTask];
        }
    }
}

-(void)_didReceiveImageFromDisk:(UIImage*)image {
    @autoreleasepool {
        [_lock lock];
        if (![self isCancelled]) {
            if (_completion) {
                _completion(image, _objectKey, YYWebImageFromDiskCache, YYWebImageStageFinished, nil);
            }
            [self _finish];
        }
        else {
            [self _startRequest:nil];
        }
        [_lock unlock];
    }
}

-(void)_didReceiveImageFromWeb:(UIImage*)image {
    @autoreleasepool {
        [_lock lock];
        if (![self isCancelled]) {
            if (_cache) {
                if (image) {
                    NSData *data = _data;
                    dispatch_async([YYAliyunOSSImageOperation _imageQueue], ^{
                        [_cache setImage:image imageData:data forKey:_cacheKey withType:YYImageCacheTypeAll];
                    });
                }
            }
            _data = nil;
            NSError *error = nil;
            if (!image) {
                error = [NSError errorWithDomain:@"com.ibireme.image" code:-1 userInfo:@{ NSLocalizedDescriptionKey : @"Web image decode fail." }];
            }
            if (_completion) {
                _completion(image, _objectKey, YYWebImageFromRemote, YYWebImageStageFinished, error);
            }
        }
        [_lock unlock];
    }
}

-(void)_didDownloadDataSuccess:(OSSGetObjectResult*)getResult
{
    
    DLog(@"getResult = %@", getResult.objectMeta);
    
    @autoreleasepool {
        [_lock lock];
        BOOL cancelled = [self isCancelled];
        [_lock unlock];
        if (cancelled) {
            return;
        }
        
        if (getResult.downloadedData) {
            _data = [getResult.downloadedData mutableCopy];
        }
        
        if (_progress) {
            
        }
        
        [_lock lock];
        
        _getObjectRequest = nil;
        
        if (![self isCancelled]) {
            __weak typeof(self) _self = self;
            dispatch_async([self.class _imageQueue], ^{
                __strong typeof(_self) self = _self;
                if(!self) return;
                
                CFAbsoluteTime start = CFAbsoluteTimeGetCurrent();
                // do something
                UIImage *image;
                YYImageDecoder *decoder = [YYImageDecoder decoderWithData:self.data scale:[UIScreen mainScreen].scale];
                image = [decoder frameAtIndex:0 decodeForDisplay:YES].image;
                CFAbsoluteTime end = CFAbsoluteTimeGetCurrent();
                DLog(@"_didDownloadDataSuccess: objectKey: %@, YYImageDecoder take time : %f", getResult.objectMeta,end - start);
                
//                CFAbsoluteTime start = CFAbsoluteTimeGetCurrent();
//                UIImage *image = [UIImage sd_imageWithData:self.data];
////                UIImage *image;
//                image = [UIImage decodedAndScaledDownImageWithImage:image];
//                [self.data setData:UIImagePNGRepresentation(image)];
//                CFAbsoluteTime end = CFAbsoluteTimeGetCurrent();
//                DLog(@"YYImageDecoder take time : %f", end - start);
                
                YYImageType imageType = YYImageDetectType((__bridge CFDataRef)self.data);
                switch (imageType) {
                    case YYImageTypeJPEG:
                    case YYImageTypeGIF:
                    case YYImageTypePNG:
                    case YYImageTypeWebP: { // save to disk cache
//                        if (!hasAnimation) {
                            if (imageType == YYImageTypeGIF ||
                                imageType == YYImageTypeWebP) {
                                self.data = nil; // clear the data, re-encode for disk cache
                            }
//                        }
                    } break;
                    default: {
                        self.data = nil; // clear the data, re-encode for disk cache
                    } break;
                }
                
//                CFAbsoluteTime start2 = CFAbsoluteTimeGetCurrent();
//                UIImage *image = [UIImage imageWithData:self.data];
//                CFAbsoluteTime end2 = CFAbsoluteTimeGetCurrent();
//                DLog(@"[UIImage imageWithData]: %f", end2 - start2);
                
//                UIImage *image = [UIImage imageWithData:self.data];
                
                if ([self isCancelled]) {
                    return;
                }
                
                [self performSelector:@selector(_didReceiveImageFromWeb:) onThread:[self.class _networkThread] withObject:image waitUntilDone:NO];
                
            });
        }
        
        
        
        
        [_lock unlock];
    }
}

-(void)_didDownloadDataFailure
{
    @autoreleasepool {
        [_lock lock];
        if (![self isCancelled]) {
            if (_completion) {
                NSError *error ;
//                if (!image) {
                    error = [NSError errorWithDomain:@"com.ibireme.image" code:-2 userInfo:@{ NSLocalizedDescriptionKey : @"Web image downlaod fail." }];
//                }
                _completion(nil, _objectKey, YYWebImageFromNone, YYWebImageStageFinished, error);
            }
            _getObjectRequest = nil;
            _data = nil;
            [self _finish];
        }
        [_lock unlock];
    }
    
}

#pragma mark - Override NSOperation

-(void)start {
    
    DLog(@"start: objectKey = %@", self.objectKey);
    
    @autoreleasepool {
        [_lock lock];
        self.started = YES;
        if ([self isCancelled]) {
            [self performSelector:@selector(_cancelOperation) onThread:[[self class] _networkThread] withObject:nil waitUntilDone:NO modes:@[NSDefaultRunLoopMode]];
            self.finished = YES;
        }
        else if ([self isReady] && ![self isFinished] && ![self isExecuting]) {
            if (!_objectKey) {
                self.finished = YES;
                if (_completion) {
                    NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:@{NSLocalizedDescriptionKey:@"request in nil"}];
                    _completion(nil, _objectKey, YYWebImageFromNone, YYWebImageStageFinished, error);
                }
            }
            else {
                self.executing = YES;
                [self performSelector:@selector(_startOperation) onThread:[[self class] _networkThread] withObject:nil waitUntilDone:NO modes:@[NSDefaultRunLoopMode]];
                
            }
        }
        [_lock unlock];
    }
}

-(void)cancel {
    [_lock lock];
    if (![self isCancelled]) {
        [super cancel];
        self.cancelled = YES;
        if ([self isExecuting]) {
            self.executing = NO;
            [self performSelector:@selector(_cancelOperation) onThread:[[self class] _networkThread] withObject:nil waitUntilDone:NO modes:@[NSDefaultRunLoopMode]];
        }
        if (self.started) {
            self.finished = YES;
        }
    }
    [_lock unlock];
}

- (void)setExecuting:(BOOL)executing {
    [_lock lock];
    if (_executing != executing) {
        [self willChangeValueForKey:@"isExecuting"];
        _executing = executing;
        [self didChangeValueForKey:@"isExecuting"];
    }
    [_lock unlock];
}

-(BOOL)isExecuting
{
    [_lock lock];
    BOOL executing = _executing;
    [_lock unlock];
    return executing;
}

-(void)setFinished:(BOOL)finished
{
    [_lock lock];
    if (_finished != finished) {
        [self willChangeValueForKey:@"isFinished"];
        _finished = finished;
        [self didChangeValueForKey:@"isFinished"];
    }
    [_lock unlock];
}

-(BOOL)isFinished {
    [_lock lock];
    BOOL finished = _finished;
    [_lock unlock];
    return finished;
}

-(void)setCancelled:(BOOL)cancelled
{
    [_lock lock];
    if (_cancelled != cancelled) {
        [self willChangeValueForKey:@"isCancelled"];
        _cancelled = cancelled;
        [self didChangeValueForKey:@"isCancelled"];
    }
    [_lock unlock];
}

-(BOOL)isCancelled
{
    [_lock lock];
    BOOL cancelled = _cancelled;
    [_lock unlock];
    return cancelled;
}

-(BOOL)isConcurrent {
    return YES;
}

-(BOOL)isAsynchronous {
    return YES;
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key {
    if ([key isEqualToString:@"isExecuting"] ||
        [key isEqualToString:@"isFinished"] ||
        [key isEqualToString:@"isCancelled"]) {
        return NO;
    }
    return [super automaticallyNotifiesObserversForKey:key];
}

- (NSString *)description {
    NSMutableString *string = [NSMutableString stringWithFormat:@"<%@: %p ",self.class, self];
    [string appendFormat:@" executing:%@", [self isExecuting] ? @"YES" : @"NO"];
    [string appendFormat:@" finished:%@", [self isFinished] ? @"YES" : @"NO"];
    [string appendFormat:@" cancelled:%@", [self isCancelled] ? @"YES" : @"NO"];
    [string appendString:@">"];
    return string;
}

                            


@end
