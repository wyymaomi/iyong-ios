//
//  PartnerDAO.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DAO.h"

@interface PartnerDAO : DAO

- (NSMutableArray*)getAllPartner:(NSString*)companyId;// 获取对应公司名下的合作伙伴列表
- (BOOL)updatePartnerList:(NSArray*)partnerList;// 更新合作伙伴信息
- (BOOL)deletePartnerById:(NSString*)partnerId companyId:(NSString*)companyId;

@end
