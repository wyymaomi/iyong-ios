//
//  BusinessTypeTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/6.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BusinessTypeTableViewCell.h"
#import "HeaderView.h"

@implementation BusinessTypeTableViewCell

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    static NSString *ID = @"BusinessTypeTableViewCell";
    BusinessTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.userInteractionEnabled = YES;
//        cell.contentView.userInteractionEnabled = YES;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(24, 15, 2, 17)];
        view.backgroundColor = NAVBAR_BGCOLOR;
        [cell.contentView addSubview:view];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, view.top-1, ViewWidth, view.height)];
        titleLabel.font = FONT(17);
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = @"服务类型";
        titleLabel.backgroundColor = [UIColor clearColor];
        [titleLabel sizeToFit];
        [cell.contentView addSubview:titleLabel];
        
        cell.imageArray = [NSMutableArray new];
        cell.labelArray = [NSMutableArray new];
        
//        NSArray *typeNameArray = @[@"logo_commercial", @"logo_travel", @"logo_airport", @"logo_goods", @"logo_wedding", @"logo_chartered", @"logo_truck", @"logo_bus"];
//        
//        NSArray *nameArray = @[@"商务用车", @"旅游用车", @"机场接送", @"货物用车", @"婚庆用车", @"包车服务", @"拖车服务", @"企业班车"];
        
        NSInteger number = 8;
        NSInteger startX = 32;
        NSInteger startY = titleLabel.bottom+17;
        NSInteger imgWidth = 50, imgHeight = 42.5;
        NSInteger separatorX = (ViewWidth-startX * 2 - imgWidth * 4)/3,separatorY = 33;

        for (NSInteger i = 0; i < number; i++) {
            
            NSDictionary *dictionary = serviceTypeList()[i];
 
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.userInteractionEnabled = YES;
            button.enabled = YES;
            button.frame = CGRectMake(startX+(imgWidth+separatorX)*(i%4), startY+(imgHeight+separatorY)*(i/4), imgWidth, imgHeight);
            NSString *imgName = [NSString stringWithFormat:@"%@_gray", dictionary[@"icon"]];
            NSString *selImgName = [NSString stringWithFormat:@"%@_blue", dictionary[@"icon"]];
            [button setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:selImgName] forState:UIControlStateSelected];
            button.tag = /*[dictionary[@"type"] integerValue];//*/i+1;
            [cell.contentView addSubview:button];
 
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(startX+(imgWidth+separatorX)*(i%4), button.bottom+5, imgWidth, 20)];
            label.font = FONT(11);
            label.textAlignment = UITextAlignmentCenter;
            label.text = dictionary[@"title"];//nameArray[i];
            label.highlightedTextColor = NAVBAR_BGCOLOR;
            label.textColor = UIColorFromRGB(0xBEBEBE);
            label.tag = (i+1)*1000;
            [cell.contentView addSubview:label];
            [label setHighlighted:NO];
            
            [cell.imageArray addObject:button];
            [cell.labelArray addObject:label];
        }
    }
    return cell;
}

- (void)onClickButton:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if (!button.isSelected) {
        [button setSelected:YES];
    }
    else {
        [button setSelected:NO];
    }
    
    
    
}


//- (void)onClickImageView:(id)sender
//{
//    UITapGestureRecognizer *tap = (UITapGestureRecognizer*)sender;
//    
//    UIImageView *imageView = (UIImageView*)tap.view;
//    
//    DLog(@"tag = %ld", (long)imageView.tag);
//    
//    //    NSLog(@"image name is %@", [imageView.image accessibilityIdentifier]);
//    
//    if (!imageView.isHighlighted) {
//        [imageView setHighlighted:YES];
//    }
//    else {
//        [imageView setHighlighted:NO];
//    }
//    
//    NSString *tagStr = [NSString stringWithFormat:@"%ld", (long)imageView.tag];
//    BOOL isHighlighted = imageView.isHighlighted;
//    if (isHighlighted) {
//        if (![self.businessTypeArray containsObject:tagStr]) {
//            [self.businessTypeArray addObject:tagStr];
//        }
//    }
//    else {
//        if ([self.businessTypeArray containsObject:tagStr]) {
//            [self.businessTypeArray removeObject:tagStr];
//        }
//    }
//    
//    DLog(@"businessTypeArray = %@", self.businessTypeArray);
//    
//    
//}


- (HeaderView*)headerView
{
    if (!_headerView) {
        _headerView = [HeaderView customView];
        _headerView.titleLabel.text = @"服务类别";
//        [self.contentView addSubview:_headerView];
        
    }
    return _headerView;
}

- (UIView*)lineView
{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xebebeb);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (void)layoutSubviews
{
    DLog(@"layoutSubviews");
    [super layoutSubviews];
    
    self.lineView.frame = CGRectMake(12, self.height-SEPARATOR_LINE_HEIGHT, ViewWidth-24,SEPARATOR_LINE_HEIGHT);
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    DLog(@"awakeFromNib");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
