//
//  UIImageView+Download.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UIImageView+Download.h"
#import "NSData+ImageContentType.h"
#import "NetworkManager.h"
#import "AliyunOSSManager.h"
#import "ImageUtil.h"
#import "YYImageDownloader.h"

@implementation UIImageView (Download)

- (void)downloadAdvImage:(NSString*)imageUrl
        placeholderImage:(UIImage*)placeholderImage
                 success:(SuccessBlock)success
              andFailure:(FailureBlock)failure
{
    if (IsStrEmpty(imageUrl)) {
        return;
    }
    
    // 如果本地已经缓存图片
    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
    
    NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    
    NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
    
    if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
        
        self.image = [UIImage imageWithData:imageData];
        
        if (success) {
            success(imageData);
        }
        
        return;
    }
    else {
        
        if (placeholderImage != nil) {
            
            __weak __typeof(self)wself = self;
            
            dispatch_main_async_safe(^{
                
                wself.image = placeholderImage;
                
            });
        }
        
    }
    
    // 如果本地没有缓存图片 则保存到本地
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, DONWLOAD_IMAGE_API_ENCRYPT ];
    
    NSString *params = [NSString stringWithFormat:@"url=%@", imageUrl];
    
    __weak __typeof(self)wself = self;
    
    [[NetworkManager sharedInstance] downloadImageData:url requestMethod:POST params:params success:^(id responseData) {
        
        if (!wself) return;
        
        if ([responseData isKindOfClass:[NSData class]]) {
            
            if (!responseData) {
                return;
            }
            
            NSData *imageData = responseData;
            
            // 判断是否为jpg或者png格式图片
            NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
            
            if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                
                [[FileCache defaultCache] saveData:imageData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                
//                dispatch_main_async_safe(^{
                
                    wself.image = [UIImage imageWithData:imageData];
                    
//                    [self setNeedsLayout];
                
//                });
                
                if (success) {
                    
                    success(imageData);
                }
                
            }
            else {
                
                if (placeholderImage != nil) {
                    
//                    dispatch_main_async_safe(^{
                    
                        wself.image = placeholderImage;
                        
//                    });
                    
                }
                
            }
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (placeholderImage != nil) {
            
//            dispatch_main_async_safe(^{
            
                wself.image = placeholderImage;
                
//            });
            
        }
        
        if (failure) {
            
            failure(errorDesc);
            
        }
        
    }];
}

//- (void)downloadBanner

- (void)downloadImageFromAliyunOSS:(NSString*)imageUrl
                       isThumbnail:(BOOL)isThumbnail
                  placeholderImage:(UIImage*)placeholderImage
                           success:(SuccessBlock)success
                        andFailure:(FailureBlock)failure
{
#if 0
    
    if (placeholderImage) {
        dispatch_main_async_safe(^{
            self.image = placeholderImage;
        })
    }
    
    
    __weak __typeof(self)wself = self;
    [[YYImageDownloader sharedDownloader] downloadImageWithImageUrl:imageUrl isThumbnail:isThumbnail completed:^(NSData *imageData, NSError *error) {
        
        if (imageData != nil) {
            
//            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            dispatch_main_sync_safe(^{
                wself.image = [UIImage imageWithData:imageData];
                [wself setNeedsLayout];
                //                [wself setNeedsLayout];
                if (success) {
                    success(imageData);
                }
            });
        
//            }];
        
        }
        else {
            
//            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            dispatch_main_sync_safe(^{
                wself.image = placeholderImage;
                [wself setNeedsLayout];
                if (failure) {
                    failure(nil);
                }
            });
            
//            }];
           
        }
    }];
    return;
#endif
    
#if 1
    
    if (IsStrEmpty(imageUrl) || [imageUrl contains:@"null"] /*|| ![imageUrl startsWith:@"img"]*/) {
        return;
    }
    
    // 如果本地已经缓存图片
    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
    if (isThumbnail) {
        convertImgUrl = [convertImgUrl substringToIndex:[convertImgUrl indexOfString:@"."]];
        convertImgUrl = [convertImgUrl stringByAppendingString:@"-thumbnail.png"];
    }
    NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
    if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
        self.image = [UIImage imageWithData:imageData];
        if (success) {
            success(imageData);
        }
        return;
    }
    else {
        if (placeholderImage != nil) {
            self.image = placeholderImage;
        }
    }
    
    __weak __typeof(self)wself = self;
    
    [[AliyunOSSManager sharedInstance] downloadImage:imageUrl isThumbnail:isThumbnail complete:^(DownloadImageState state, NSData *data) {
        
        if (state == DownloadImageSuccess) {
            
            if (!wself) {
                return;
            }
            
            NSString *imageContentType = [NSData sd_contentTypeForImageData:data];
            
            if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                if (isThumbnail) {
                    // 保存缩略图
                    NSInteger maxFileSize = 500 * 1024;
                    UIImage *tempImage = [UIImage imageWithData:data];
                    NSData *compressedData = [ImageUtil compressImageWithData:tempImage toMaxFileSize:maxFileSize];
                    [[FileCache defaultCache] saveData:compressedData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                    dispatch_main_async_safe(^{
                        wself.image = [UIImage imageWithData:data];
                    });
                    
                }
                else {
                    [[FileCache defaultCache] saveData:data forKey:convertImgUrl cacheAge:kCacheAgeForever];
                    dispatch_main_async_safe(^{
                        wself.image = [UIImage imageWithData:data];
                    });
                }
                
                if (success) {
                    success(data);
                }
            }
            else {
                if (placeholderImage != nil) {
                    self.image = placeholderImage;
                }
                if (failure) {
                    failure(nil);
                }
            }
        }
        else if (state == DownloadImageFailed) {
            if (placeholderImage != nil) {
                dispatch_main_async_safe(^{
                    self.image = placeholderImage;
                })
                
            }
            if (failure) {
                failure(nil);
            }
        }
    }];
    
#endif
    
}

- (void)downloadImage:(NSString*)imageUrl
     placeholderImage:(UIImage*)placeholderImage
              success:(SuccessBlock)success
           andFailure:(FailureBlock)failure;
{
#if 1
    
    self.image = placeholderImage;
    
    if (IsStrEmpty(imageUrl) || [imageUrl contains:@"null"]) {
        return;
    }
    
    // 如果本地已经缓存图片
    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
    
    NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    
    NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
    
    if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
        
        WeakSelf
        
        dispatch_main_async_safe(^{
        
            weakSelf.image = [UIImage imageWithData:imageData];
            
            [weakSelf setNeedsLayout];
            
            if (success) {
                success(imageData);
            }
            
        });
        
        
        return;
    }
    else {
        
        if (placeholderImage != nil) {
            
            //            __weak __typeof(self)wself = self;
            
            //            dispatch_main_async_safe(^{
            
            //                self.image = placeholderImage;
            
            //            });
            
            
            WeakSelf
            
            dispatch_main_async_safe(^{
            
                weakSelf.image = placeholderImage;//[UIImage imageWithData:imageData];
                
                [weakSelf setNeedsLayout];
                
                if (success) {
                    success(imageData);
                }
                
            });
            
//            return;
            
        }
        
    }
    
    
    
    // 如果本地没有缓存图片 则保存到本地
    //    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, DONWLOAD_IMAGE_API_ENCRYPT];
    
    //    NSString *params = [NSString stringWithFormat:@"url=%@", imageUrl];
    
    __weak __typeof(self)wself = self;
    
    [[NetworkManager sharedInstance] downloadImageData:imageUrl requestMethod:POST success:^(id responseData) {
        //    [[NetworkManager sharedInstance] downloadImageData:url requestMethod:POST params:params success:^(id responseData) {
        
        if (!wself) return;
        
        if ([responseData isKindOfClass:[NSData class]]) {
            
            if (!responseData) {
                return;
            }
            
            NSData *imageData = responseData;
            
            // 判断是否为jpg或者png格式图片
            NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
            
            if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                
                [[FileCache defaultCache] saveData:imageData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                
                dispatch_main_async_safe(^{
                
                    wself.image = [UIImage imageWithData:imageData];
                    
                    [wself setNeedsLayout];
                    
                    if (success) {
                        
                        success(imageData);
                    }
                    
                });
                
                
                
            }
            else {
                
                if (placeholderImage != nil) {
                    
                    dispatch_main_async_safe(^{
                    
                        wself.image = placeholderImage;
                        
                    });
                    
                }
                
            }
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (placeholderImage != nil) {
            
            dispatch_main_async_safe(^{
            
                wself.image = placeholderImage;
                
            });
            
        }
        
        if (failure) {
            
            failure(errorDesc);
            
        }
        
    }];
    
#endif

    
}

-(void)downloadAvatarImageView:(NSString*)imageUrl
    placeholderImage:(UIImage*)placeholderImage
             success:(SuccessBlock)success
          andFailure:(FailureBlock)failure
{
    
    __weak __typeof(self)wself = self;
    
    [[YYImageDownloader sharedDownloader] downloadImageWithAvatarUrl:imageUrl isThumbnail:YES completed:^(NSData *imageData, NSError *error) {
        
        if (imageData != nil) {
            
            dispatch_main_async_safe(^{
                wself.image = [UIImage imageWithData:imageData];
                [wself setNeedsLayout];
                //                [wself setNeedsLayout];
                if (success) {
                    success(imageData);
                }
            });
            
        }
        else {
            
            dispatch_main_async_safe(^{
                wself.image = placeholderImage;
                [wself setNeedsLayout];
                //                [wself setNeedsLayout];
                if (failure) {
                    failure(nil);
                }
            });
            
        }
        
    }];
    
//    [[YYImageDownloader sharedDownloader] downloadImageWithAvatarUrl:imageUrl isThumbnail:isThumbnail completed:^(NSData *imageData, NSError *error) {
//        
//        if (imageData != nil) {
//            
//            //            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            dispatch_main_async_safe(^{
//                wself.image = [UIImage imageWithData:imageData];
//                [wself setNeedsLayout];
//                //                [wself setNeedsLayout];
//                if (success) {
//                    success(imageData);
//                }
//            });
//            
//            //            }];
//            
//        }
//        else {
//            
//            //            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            dispatch_main_async_safe(^{
//                wself.image = placeholderImage;
//                [wself setNeedsLayout];
//                //                [wself setNeedsLayout];
//                if (failure) {
//                    failure(nil);
//                }
//            });
//            
//            //            }];
//            
//        }
//    }];
    
}

@end
