//
//  VechileTypeVIew.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VechileTypeView : UIView
@property (weak, nonatomic) IBOutlet UILabel *startDistanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *vechileTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *vechilePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *vechileTypeLabel2;
@property (weak, nonatomic) IBOutlet UILabel *vechilePriceLabel2;
@property (weak, nonatomic) IBOutlet UIImageView *vechileTypeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *vechileTypeSmallImageView;
@property (weak, nonatomic) IBOutlet UIView *vechileLargeView;
@property (weak, nonatomic) IBOutlet UIView *vechileSmallView;
@property (weak, nonatomic) IBOutlet UILabel *vechileModelLabel;

@end
