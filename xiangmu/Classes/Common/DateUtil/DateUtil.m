//
//  DateUtil.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DateUtil.h"

@implementation DateUtil

+ (NSInteger)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *oneDayStr = [dateFormatter stringFromDate:oneDay];
    NSString *anotherDayStr = [dateFormatter stringFromDate:anotherDay];
    NSDate *dateA = [dateFormatter dateFromString:oneDayStr];
    NSDate *dateB = [dateFormatter dateFromString:anotherDayStr];
    NSComparisonResult result = [dateA compare:dateB];
//    NSLog(@"oneDay : %@, anotherDay : %@", oneDay, anotherDay);
    if (result == NSOrderedDescending) {
        //oneDay > anotherDay
        return 1;
    }
    else if (result == NSOrderedAscending){
        //oneDay < anotherDay
        return -1;
    }
    //oneDay = anotherDay
    return 0;
}

+ (NSString *)getCurrentDate {
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
//    return dateTime;
    
    NSDateFormatter *formatter = [[NSDate class] sharedDateFormatter];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *date = [formatter stringFromDate:[NSDate date]];
    return date;
}


+(NSDate*)getTenDayAfterDateTime:(NSDate*)dateTime
{
    
    if (IsNilOrNull(dateTime)|| ![dateTime isKindOfClass:[NSDate class]]) {
        return nil;
    }
    
    NSDate *date = [NSDate dateWithTimeInterval:600 sinceDate:dateTime];
    return date;
    
}


+(BOOL)isLessThanSixMonths:(NSString*)dateStr1
                  dateStr2:(NSString*)dateStr2
                     error:(NSString **)errorMsg;
{
    if (IsStrEmpty(dateStr1)) {
        return YES;
    }
    if (IsStrEmpty(dateStr2)) {
        return YES;
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *date1 = [formatter dateFromString:dateStr1];
    NSDate *date2 = [formatter dateFromString:dateStr2];
    
    if ([NSDate dateCompare:date1 secondDate:date2] > 0) {
        *errorMsg = MSG_ENDTIME_MUST_BE_MORE_THAN_STARTTIME;
        return NO;
    }
    // 比较两个日期
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit unit = NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear;
    
    NSDateComponents *components = [calendar components:unit fromDate:date1 toDate:date2 options:0];
    
    if (labs(components.year) == 0) {
        if (labs(components.month) < 6) {
            return YES;
        }
        else if (labs(components.month) == 6) {
            if (labs(components.day) == 0) {
                return YES;
            }
        }
    }

    *errorMsg = MSG_PLS_ENTER_LESS_THANK_SIX_MOTNHS_TIME_INTERVALS;
    return NO;
}




+(NSString*)getYearStr:(NSDate*)date;
{
    if (IsNilOrNull(date) || ![date isKindOfClass:[NSDate class]]) {
        return @"";
    }
    return [NSString stringWithFormat:@"%ld", (long)date.year];
}

+(NSString*)getMonthStr:(NSDate*)date;
{
    if (IsNilOrNull(date) || ![date isKindOfClass:[NSDate class]]) {
        return @"";
    }
    NSInteger month = [date month];
    if (month < 10) {
        return [NSString stringWithFormat:@"0%ld", (long)month];
    }
    return [NSString stringWithFormat:@"%ld", (long)month];
}

+(NSString*)formatDateTime:(id)dateTime dateFomatter:(NSString*)dateFormatter;
{
    if (IsNilOrNull(dateTime)) {
        return @"";
    }
    
    if ([dateTime isKindOfClass:[NSString class]] || [dateTime isKindOfClass:[NSNumber class]]) {
        if ([dateTime doubleValue] > 0) {
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([dateTime doubleValue]/1000)];
            return [date stringWithDateFormat:dateFormatter];
        }
    }
    
    return @"";
}

+(NSString*)timerAgo:(id)dateTime
{
    if (IsNilOrNull(dateTime)) {
        return nil;
    }
    
    if ([dateTime isKindOfClass:[NSString class]] || [dateTime isKindOfClass:[NSNumber class]]) {
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([dateTime doubleValue]/1000)];
        return [date timeAgo];
        
    }
    return nil;
    
}

+ (NSString*)timerLeft:(id)dateTime;
{
    if (IsNilOrNull(dateTime)) {
        return nil;
    }
    
    if ([dateTime isKindOfClass:[NSString class]] || [dateTime isKindOfClass:[NSNumber class]]) {
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([dateTime doubleValue]/1000)];
        return [date timeLeft];
        
    }
    return nil;
}

@end
