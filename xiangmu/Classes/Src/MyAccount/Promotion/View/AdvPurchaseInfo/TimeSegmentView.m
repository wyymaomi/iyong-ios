//
//  TimeSegmentView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "TimeSegmentView.h"

@implementation TimeSegmentView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [_button setBackgroundImage:[UIImage imageNamed:@"img_time_segment_bg"] forState:UIControlStateNormal];
        [_button setBackgroundImage:[UIImage imageNamed:@"icon_time_unable"] forState:UIControlStateDisabled];
        [_button setBackgroundImage:[UIImage imageNamed:@"img_time_segment_bg_selected"] forState:UIControlStateSelected];
        [_button addTarget:self action:@selector(onClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_button];
        
        _startTimeLabel = [UILabel new];
        _startTimeLabel.frame = CGRectMake(5, 4*Scale, self.frame.size.width, 14*Scale);
        //[[UILabel alloc] initWithFrame:CGRectMake(0, 2, self.frame.size.width, 16)];
        _startTimeLabel.font = FONT(14*Scale);
        [self addSubview:_startTimeLabel];
        
        _endTimeLabel = [UILabel new];
        _endTimeLabel.frame = CGRectMake(0, self.frame.size.height-20*Scale, self.frame.size.width-5,20*Scale);
        _endTimeLabel.font = FONT(14*Scale);
        _endTimeLabel.textAlignment = UITextAlignmentRight;
        [self addSubview:_endTimeLabel];
        
        _priceLabel = [UILabel new];
        _priceLabel.frame = CGRectMake(self.width-30*Scale-2, 0, 30*Scale, 10);
        _priceLabel.textColor = [UIColor whiteColor];
        _priceLabel.textAlignment = UITextAlignmentRight;
        _priceLabel.font = FONT(10*Scale);
        [self addSubview:_priceLabel];
        
        
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (void)setStatus:(NSUInteger)status
{
    _status = status;
    
    if (_status == TimeSegmentViewStatusSelected) {
        self.startTimeLabel.textColor = [UIColor whiteColor];
        self.endTimeLabel.textColor = [UIColor whiteColor];
        self.button.enabled = YES;
        self.button.selected = YES;
//        [self setNeedsLayout];
    }
    else if (_status == TimeSegmentViewStatusUnSelected) {
        self.startTimeLabel.textColor = NAVBAR_BGCOLOR;
        self.endTimeLabel.textColor = NAVBAR_BGCOLOR;
        self.button.enabled = YES;
        self.button.selected = NO;
//        [self setNeedsLayout];
    }
    else if (_status == TimeSegmentViewStatusDisabled) {
        self.startTimeLabel.textColor = [UIColor whiteColor];
        self.endTimeLabel.textColor = [UIColor whiteColor];
        self.button.selected = NO;
        self.button.enabled = NO;
//        [self.button setEnabled:NO];
//        [self.button setBackgroundImage:[UIImage imageNamed:@"icon_time_unable"] forState:UIControlStateNormal];
        self.priceLabel.text = @"";
//        [self setNeedsLayout];
    }
    
}

-(void)onClickButton:(UIButton*)button
{
    BOOL isSelected = button.isSelected;
    BOOL isEnabled = button.isEnabled;
    
    if (isEnabled) {
        if (isSelected) {
            self.status = TimeSegmentViewStatusUnSelected;
        }
        else {
            self.status = TimeSegmentViewStatusSelected;
        }
    }
    else {
        self.status = TimeSegmentViewStatusDisabled;
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(onClickTimeView:)]) {
        [_delegate onClickTimeView:button];
    }
}

-(void)onClickTimeView:(UIButton*)button;
{
    if (_delegate && [_delegate respondsToSelector:@selector(onClickTimeView:)]) {
        [_delegate onClickTimeView:button];
    }
}

@end
