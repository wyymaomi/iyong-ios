//
//  InvoiceInfoTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceInfoEditTableViewCell.h"
#import "InvoiceinfoHttpMessage.h"

@implementation InvoiceInfoEditTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"InvoiceInfoEditTableViewCellID";
    InvoiceInfoEditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.titleTextField.tag = InvoiceTextFieldTypeTitle;
        cell.taxpayerNumberTextField.tag = InvoiceTextFieldTypeTaxpayerNumber;
        cell.registAddressTextField.tag = InvoiceTextFieldTypeRegistAddress;
        cell.bankNameTextField.tag = InvoiceTextFieldTypeBankName;
        cell.bankAccountTextField.tag = InvoiceTextFieldTypeBankAccount;
        
        
    }
    return cell;
}

+(CGFloat)getCellHeight
{
    return 406;
}

+(CGFloat)getCloseStatusCellHeight
{
    return 204;
}

-(void)initData:(InvoiceinfoHttpMessage*)invoiceData
{
    if (!invoiceData) {
        return;
    }
    
    _invoiceData = invoiceData;

    self.titleTextField.text = [StringUtil getSafeString:invoiceData.header];
    self.taxpayerNumberTextField.text = [StringUtil getSafeString:invoiceData.taxpayerNumber];
    self.registAddressTextField.text = [StringUtil getSafeString:invoiceData.registerAddress];
    self.bankNameTextField.text = [StringUtil getSafeString:invoiceData.openingBank];
    self.bankAccountTextField.text = [StringUtil getSafeString:invoiceData.bankAccount];
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
