//
//  OnlineServiceTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OnlineServiceModel.h"
#import "ServiceBodyView.h"
#import "ServiceToolBarView.h"
#import "ServiceProviderViewModel.h"
#import "StarView.h"

@protocol ServiceCellDelegate <NSObject>

- (void)onClickImageView:(NSInteger)tag;

@end

@class ServiceProviderViewModel;



@interface OnlineServiceTableViewCell : UITableViewCell


@property (nonatomic, weak) id<ServiceCellDelegate> delegate;

@property (nonatomic, strong) ServiceProviderViewModel *viewModel;

// 主体
@property (nonatomic, strong) ServiceBodyView *bodyView;
// 工具条
@property (nonatomic, strong) ServiceToolBarView *toolView;

+ (instancetype)momentsTableViewCellWithTableView:(UITableView *)tableView;

//@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
//@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
//@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
//@property (weak, nonatomic) IBOutlet UILabel *businessScropLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
//@property (weak, nonatomic) IBOutlet UIImageView *imageView2;
//@property (weak, nonatomic) IBOutlet UIImageView *imageView3;
//@property (weak, nonatomic) IBOutlet UIImageView *imageView4;
//@property (weak, nonatomic) IBOutlet UIButton *telButton;
//@property (weak, nonatomic) IBOutlet UIView *favoriteView;
//@property (weak, nonatomic) IBOutlet UIView *likeView;
//@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
//@property (weak, nonatomic) IBOutlet StarView *starView;
//@property (weak, nonatomic) IBOutlet UIImageView *favoriteImageView;
//
//@property (weak, nonatomic) IBOutlet UIView *companyInfoView;
//@property (weak, nonatomic) IBOutlet UIView *businessScopeView;
//@property (weak, nonatomic) IBOutlet UIView *imagesView;
//@property (weak, nonatomic) IBOutlet UIView *bottomView;

//- (void)initData:(OnlineServiceModel*)data;
//- (CGFloat)cellHeight:(OnlineServiceModel*)data;

//- (CGFloat)cellHeight;


@end
