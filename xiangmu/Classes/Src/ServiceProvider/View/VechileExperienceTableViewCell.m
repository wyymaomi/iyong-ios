//
//  VechileExperienceTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileExperienceTableViewCell.h"

@implementation VechileExperienceTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"VechileExperienceTableViewCell";
    VechileExperienceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //        CGFloat separator = (ViewWidth-12*Scale*2)/2;
        //        CGFloat separator = 5;
        cell.imageView1.width = 105*Scale;
        cell.imageView1.height = 105*Scale;
        cell.imageView2.width = 105*Scale;
        cell.imageView2.height = 105*Scale;
        //        cell.imageView2.left = cell.imageView1.right+separator;
        cell.imageView3.width = 105*Scale;
        cell.imageView3.height = 105*Scale;
        //        cell.imageView3.left = cell.imageView2.right+separator;
    }
    return cell;
}

- (id)initWithStyle:(UITableView*)tableView// style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        
//        self.myimage = [[UIImageView alloc] init];
//        self.myimage.frame = CGRectMake(10, 10, 60, 60);
//        
//        [self addSubview:self.myimage];
//    }
    
    static NSString *ID = @"VechileExperienceTableViewCell";
    
    VechileExperienceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
//        CGFloat separator = (ViewWidth-12*Scale*2)/2;
//        CGFloat separator = 5;
        cell.imageView1.width = 105*Scale;
        cell.imageView1.height = 105*Scale;
        cell.imageView2.width = 105*Scale;
        cell.imageView2.height = 105*Scale;
//        cell.imageView2.left = cell.imageView1.right+separator;
        cell.imageView3.width = 105*Scale;
        cell.imageView3.height = 105*Scale;
//        cell.imageView3.left = cell.imageView2.right+separator;
        
        
    }
    
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    
//    if (self) {
//        
//        
//        
//    }
    
    
    return cell;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.avatarImageView.frame = CGRectMake(12*Scale, 15*Scale, 40*Scale, 40*Scale);
    self.avatarImageView.cornerRadius = self.avatarImageView.width/2;
    self.userNameLabel.frame = CGRectMake(self.avatarImageView.right+10*Scale, 15*Scale, ViewWidth-62.5*Scale-22*Scale, 13*Scale);
    self.contentLabel.frame = CGRectMake(self.userNameLabel.left, self.userNameLabel.bottom+6*Scale, self.userNameLabel.width, 22*Scale);

    CGFloat separator = (ViewWidth-44*Scale - self.imageView1.width*3)/2;
    self.imageView1.top = self.imageView2.top = self.imageView3.top = self.contentLabel.bottom+15*Scale;
    self.imageView1.left = 22*Scale;
    self.imageView2.left = CGRectGetMaxX(self.imageView1.frame) + separator;
    self.imageView3.left = CGRectGetMaxX(self.imageView2.frame) + separator;
    
    self.bottomView.height = 22*Scale;
    self.bottomView.top = self.imageView1.bottom + 15*Scale;

    
}

+(CGFloat)getCellHeight
{
//    return 300;
//    return cell.bottomView.
    return 220 * Scale;
}

//- (void)setViewModel:(UserExpericenViewModel *)viewModel
//{
//    self.avatarImageView.frame = viewModel.bodyIconFrame;
//    self.userNameLabel.frame = viewModel.bodyNameFrame;
//    self.contentLabel.frame = viewModel.bodyCompanyFrame;
//    
//    
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
