//
//  BaseViewController+HandleLoginSuccess.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/11.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController+HandleLoginSuccess.h"
#import "AppDelegate.h"

@implementation BaseViewController (HandleLoginSuccess)

-(void)dismissLoginWindow
{
    
    
    AppDelegate *appDelegate = APP_DELEGATE;
    [appDelegate initResignTime];
    appDelegate.receiveLoginOverTimeMessageCount = 0;
    receiveAuthMessageCount = NO;
    
    if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
        [self.loginDelegate onLoginSuccess];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
    [self dismissToRootViewController:YES completion:nil];
    
}

-(void)handleResetPwdSuccess
{
//    WeakSelf
    
    // 如果未设置手势密码
    
//    if (![[UserManager sharedInstance] gesturePasswordIsExit]) {
//        
//        BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:MSG_SET_GESTURE_PWD delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"前往"];
//        
//        [alertView setConfirmBlock:^{
//           
//            [weakSelf presentGestureSetting:SOURCE_FROM_LOGIN];
//            
//        }];
//        
//        [alertView setCancelBlock:^{
//            
//            [MsgToolBox showToast:@"重设密码成功"];
//            
//            [weakSelf dismissLoginWindow];
//            
//        }];
//        
//        [alertView show];
//  
//    }
//    
//    else {
    
        [MsgToolBox showToast:@"修改密码成功，请重新登录"];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
//    }
}



- (void)handleLoginSuccess
{
    
//    [self hideHUDIndicatorViewAtCenter];
    
    WeakSelf
    // 如果未设置手势密码
    if (![[UserManager sharedInstance] gesturePasswordIsExit]) {
        
        BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:MSG_SET_GESTURE_PWD delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"前往"];
        
        [alertView setConfirmBlock:^{
            
            [weakSelf presentGestureSetting:SOURCE_FROM_LOGIN];
            
        }];
        
        [alertView setCancelBlock:^{
            
            [MsgToolBox showToast:@"登录成功"];
            
            [weakSelf dismissLoginWindow];
            
        }];
        
        [alertView show];
        
        
#if 0 // 产品说要统一弹窗样式
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息" message:@"您尚未设置手势密码\n是否立即设置？" delegate:nil cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [weakSelf presentGestureSetting:SOURCE_FROM_LOGIN];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"登录成功" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        
                        AppDelegate *appDelegate = APP_DELEGATE;
                        [appDelegate initResignTime];
                        appDelegate.receiveLoginOverTimeMessageCount = 0;
                        receiveAuthMessageCount = NO;
                        
                        if (weakSelf.loginDelegate && [weakSelf.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
                            [weakSelf.loginDelegate onLoginSuccess];
                        }
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
                        [weakSelf dismissToRootViewController:YES completion:nil];
                        
                    }
                    
                }];
            }
        }];
#endif
    }
    else {
        
        [self dismissLoginWindow];
        
        //        [MsgToolBox showToast:@"登录成功"];
        //
        //        AppDelegate *appDelegate = APP_DELEGATE;
        //        [appDelegate initResignTime];
        //        appDelegate.receiveLoginOverTimeMessageCount = 0;
        //        receiveAuthMessageCount = NO;
        //
        //        if (weakSelf.loginDelegate && [weakSelf.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
        //            [weakSelf.loginDelegate onLoginSuccess];
        //        }
        //
        //        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
        //        [weakSelf dismissToRootViewController:YES completion:nil];
        
#if 0 // 产品说要统一弹窗样式
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"登录成功" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            
            if (buttonIndex == 0) {
                
                AppDelegate *appDelegate = APP_DELEGATE;
                [appDelegate initResignTime];
                appDelegate.receiveLoginOverTimeMessageCount = 0;
                receiveAuthMessageCount = NO;
                
                if (weakSelf.loginDelegate && [weakSelf.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
                    [weakSelf.loginDelegate onLoginSuccess];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
                [weakSelf dismissToRootViewController:YES completion:nil];
                
            }
            
        }];
#endif
    }
    
    //    AppDelegate *appDelegate = APP_DELEGATE;
    //    [appDelegate initResignTime];
    //    appDelegate.receiveLoginOverTimeMessageCount = 0;
    //    receiveAuthMessageCount = NO;
    //
    //    if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(onLoginSuccess)]) {
    //        [self.loginDelegate onLoginSuccess];
    //    }
    //    
    //    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
    //    [self dismissToRootViewController:YES completion:nil];
    
}


@end
