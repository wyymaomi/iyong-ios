//
//  AdvPurchaseCollectionView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/4/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AdvPurchaseCollectionViewDelegate <NSObject>

- (void)onClickItem:(NSInteger)row tag:(NSInteger)tag;

@end

@interface AdvPurchaseCollectionView : UICollectionView<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) id<AdvPurchaseCollectionViewDelegate> customDelegate;

@end
