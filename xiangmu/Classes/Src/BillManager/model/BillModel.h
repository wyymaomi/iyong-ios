//
//  BillModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol BillOrderModel <NSObject>
@end

@protocol BillRepayModel <NSObject>
@end

@protocol BillRepayDataModel <NSObject>
@end

@interface BillOrderModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *carTime;
@property (nonatomic, strong) NSNumber<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *customerName;
@property (nonatomic, strong) NSString<Optional> *customerMobile;
@property (nonatomic, strong) NSString<Optional> *downAddress;
@property (nonatomic, strong) NSString<Optional> *orderId;
@property (nonatomic, strong) NSString<Optional> *price;
@property (nonatomic, strong) NSString<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *upAddress;
@property (nonatomic, strong) NSString<Optional> *upCompanyName;
@property (nonatomic, strong) NSString<Optional> *upCompanyLogoUrl;

@end

@interface BillResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<BillOrderModel, Optional> *data;

@end

@interface BillRepayModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *carNumber;
@property (nonatomic, strong) NSString<Optional> *carTime;
@property (nonatomic, strong) NSString<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *customerMobile;
@property (nonatomic, strong) NSString<Optional> *customerName;
@property (nonatomic, strong) NSString<Optional> *downAddress;
@property (nonatomic, strong) NSString<Optional> *driver;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *orderId;
@property (nonatomic, strong) NSNumber<Optional> *price;
@property (nonatomic, strong) NSString<Optional> *repaymentTime;
@property (nonatomic, strong) NSString<Optional> *upAddress;
@property (nonatomic, strong) NSNumber<Optional> *checked;// 是否被选中
@property (nonatomic, strong) NSString<Optional> *upCompanyName;
@property (nonatomic, strong) NSString<Optional> *upCompanyLogoUrl;

@end

@interface BillRepayDataModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *totalAmount;
@property (nonatomic, strong) NSArray<BillRepayModel,Optional> *bills;

@end


@interface BillRepayResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) BillRepayDataModel<Optional> *data;

@end

