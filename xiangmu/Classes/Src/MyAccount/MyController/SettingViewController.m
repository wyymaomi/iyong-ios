//
//  SettingViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SettingViewController.h"
#import "PasswordSetViewController.h"
#import "GesturePasswordController.h"
#import "SettingTableView.h"
#import "AppDelegate.h"
#import "BBAlertView.h"

@interface SettingViewController ()<SettingTableViewDelegate>

@property (nonatomic, strong) SettingTableView *settingTableView;

//@property (nonatomic, strong) NSArray *nextVcList;

@end

@implementation SettingViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"设置";
    
    [self.view addSubview:self.settingTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SettingTableView Delegate method

- (void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath;
{
    
    NSInteger section = indexPath.section;
    
    if (section == 0) {
        if (indexPath.row==0) {
            PasswordSetViewController *mimaVC=[[PasswordSetViewController alloc]init];
            mimaVC.type=MimaTypeLoad;
            [self.navigationController pushViewController:mimaVC animated:YES];
        }
        else if (indexPath.row==1)
        {
            GesturePasswordController *viewController = [[GesturePasswordController alloc] init];
            viewController.source_from = SOURCE_FROM_ABOUT_ME;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else
        {
            PasswordSetViewController *mimaVC=[[PasswordSetViewController alloc]init];
            mimaVC.type=MimaTypeJiaoyi;
            [self.navigationController pushViewController:mimaVC animated:YES];
            
        }
    }
    
    if (section == self.settingTableView.cellList.count - 1) {
        
        [[UserManager sharedInstance] doLogout];
        
        AppDelegate *appDelegate = APP_DELEGATE;
        
        [appDelegate backFromLogin];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    else {
        
//        NSArray *list = self.settingTableView.nextVcList[section];
//        [self gotoPage:list[indexPath.row]];

    }
}


-(void)viewDidLayoutSubviews
{
    if ([self.settingTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.settingTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.settingTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.settingTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

#pragma mark -

- (SettingTableView*)settingTableView
{
    if (!_settingTableView) {
        
        _settingTableView = [[SettingTableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        
        _settingTableView.settingDelegate = self;
        
    }
    
    return _settingTableView;
    
}

@end
