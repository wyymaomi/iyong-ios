//
//  ResourceConstants.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef ResourceConstants_h
#define ResourceConstants_h

#pragma mark - plist
#define ORDER_STATUS_HEADER_PLIST   @"OrderStatusHeader.plist"
#define ORDER_STATUS_CELL_PLIST     @"OrderStatusCell.plist"
#define HOME_CELL_PLIST             @"HomeCell.plist"
#define CODE_MSG_PLIST              @"CodeMsg.plist"
#define ORDER_HELP_PLIST            @"OrderHelp.plist"
#define BALANCE_HELP_PLIST          @"BalanceHelp.plist"
#define VECHILE_SERVICE_PLIST       @"VechileService.plist"

#define myDotNumbers     @"0123456789.\n"

#pragma mark - login keyChain
#define kLoginUserNameKey                     @"loginUserNameKeyChain"
#define kLoginPasswdKey                       @"loginPassWordKeyChain"
#define kKeychainServiceNameSuffix            @"SN_66996699_4008365365_LoginServiceName"
//参数加密的key
#define kLoginPasswdParamEncodeKey            @"LoginPassWordEncode"

#define kAliyunOSSImageFolder @"img"
//#define kAliyunOSSImagePath(companyId) [NSString stringWithFormat:@"%@/%@/", kAliyunOSSImageFolder, companyId]
#define kAliyunOSSPublichImagePath(companyId) [NSString stringWithFormat:@"public/%@/%@", kAliyunOSSImageFolder, companyId]
#define kAliyunOSSPrivateImagePath(companyId) [NSString stringWithFormat:@"private/%@/%@", kAliyunOSSImageFolder, companyId]

// 占位图
#define VechilePlaceholderImage [UIImage imageNamed:@"img_vechile_default"] // 占位图
#define AvatarPlaceholderImage  [UIImage imageNamed:@"icon_avatar"] // 头像占位图
#define DefaultPlaceholderImage [UIImage imageNamed:@"img_default"] // 默认占位图
//#define DefaultPlaceholderImage [UIImage imageNamed:@"img_placeholder"] // 默认占位图
//#define DefaultPlaceholderImage [UIImage iamgeWithColor:[UIColor whiteColor]]

#define DefaultImageSize CGSizeMake(400, 400)

// tableview row height
#define DefaultTableRowHeight 50*Scale
#define DefaultTableRowTitleFont FONT(18)
#define DefaultTableRowTitleTextColor UIColorFromRGB(0x333333)
#define DefaultTableRowSubtitleFont FONT(16)
#define DefaultTableRowSubtitleTextColor UIColorFromRGB(0x666666)

// 分割线高度
#define SEPARATOR_LINE_HEIGHT 0.5


#pragma mark - 图片名称
#define IMG_VECHILE_PLACE_HOLDER @"img_vechile_default"

#pragma mark - 错误信息
static inline NSString *getErrorMsg(NSInteger code);
static inline NSString *getErrorMsg(NSInteger code)
{
    for (id obj in codeMsgList()) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = obj;
            if ([dict[@"code"] integerValue] == code) {
                return dict[@"msg"];
            }
        }
    }
    return @"操作失败，请重试";
}

#define CONTACT_SERVICE_PHONENUMBER @"021-60732013"

#pragma mark - STRING
#define STR_OK @"确定"
#define STR_CANCEL @"取消"
#define MSG_NETWORK_FAILED @"网络连接错误，请重试"
#define MSG_FAILED @"操作失败，请重试"
#define MSG_DECRYPT_FAILURE @"解密失败"
#define MSG_LOADING @"正在加载中，请稍等片刻"
#define MSG_SEARCHING @"正在查询，请稍候"
#define MSG_REGISTER_SUCCESS @"注册成功"
#define MSG_REGISTER_FAILURE @"注册失败"
#define MSG_IS_REGISTERING @"正在注册，请稍候..."
#define MSG_IS_BIND_ACCOUNT @"正在绑定帐号，请稍候..."
#define MSG_IS_LOGING @"正在登录，请稍候..."
#define MSG_ERROR_INVALID_MOBILE @"手机号码格式有误，请重新输入"
#define MSG_ERROR_INVALID_REF_MOBILE @"推荐人手机号输入格式有误"
#define MSG_MOBILE_NULL @"请输入手机号"
#define MSG_VERIFY_CODE_NULL @"请输入验证码"
#define MSG_NEW_PWD_NULL @"请输入新密码"
#define MSG_RE_NEW_PWD_NULL @"请输入确认新密码"
#define MSG_TWICE_PWD_NOT_SAME @"两次输入密码不同，请重新输入"
#define MSG_GET_VERIFY_CODING @"验证码正在发送，请稍等"
//#define MSG_GET_VERIFY_CODING @"正在获取验证码，请稍候..."
#define MSG_GET_VERIFY_FAILURE @"获取验证码失败，请重试"
#define MSG_RESET_PWDING @"正在重设密码，请稍候..."
#define MSG_RESET_PWD_SUCCESS @"重设密码成功，请返回"
#define MSG_RESET_PWD_FAILURE @"重设密码失败，请重试"
#define MSG_PAY_PWD_NULL @"请输入支付密码"
#define MSG_INVALID_PAY_PWD @"请输入6位数字支付密码"
#define MSG_SET_PAY_PWD_SUCCESS @"设置支付密码成功"
#define MSG_SET_PAY_PWD_FAILURE @"设置支付密码失败"
#define MSG_UPDATE_LOGIN_PWD @"正在重设登录密码，请稍候..."
#define MSG_SET_GESTURE_PWD @"是否设置手势密码"
#define MSG_UPDATE_LOGIN_PWD_SUCCESS @"重设登录密码成功，请返回"
#define MSG_UPDATE_LOGIN_PWD_FAILURE @"重设登录密码失败，请返回"
#define MSG_SET_PAY_PWDING @"正在设置支付密码，请稍候..."
#define MSG_LOGIN_SUCCESS @"登录成功"
#define MSG_LOGIN_FAILURE @"登录失败"
#define MSG_LOGIN_TIP @"请先登录"
#define MSG_IDCARD_CERTIFICATIONING @"正在查询，请稍候"
#define MSG_GET_CAR_LIST_SUCCESS @"获取汽车信息成功"
#define MSG_GET_CAR_LIST_FAILURE @"获取汽车信息失败"
#define MSG_SAVE_ORDER_SUCCESS @"保存订单成功，请返回"
#define MSG_SAVE_ORDER_FAILURE @"保存订单失败"
#define MSG_DISPATCH_ORDER_FAILURE @"派单失败"
#define MSG_NEW_ORDER @"正在添加订单，请稍候..."
#define MSG_DISPATCH_ORDER @"正在派单，请稍候..."
#define MSG_ORDER_DETAIL @"正在查询订单，请稍候..."

#define MSG_ENDTIME_MUST_BE_MORE_THAN_STARTTIME @"结束日期必须大于开始日期，请重新输入"
#define MSG_PLS_ENTER_LESS_THANK_SIX_MOTNHS_TIME_INTERVALS @"请输入六个月以内日期间隔"
#define ALERT_MSG_CARTIME_NULL @"请选择用车时间"
#define ALERT_MSG_START_LOCATION_NULL @"请输入出发地点"
#define ALERT_MSG_CUSTOMER_NULL @"请输入用车人"
#define ALERT_MSG_CUSTOMER_MOBILE_NULL @"请输入用车人电话"
#define ALERT_MSG_ROUGH_ITINERARY_NULL @"请输入大致行程"
#define ALERT_MSG_ORDER_REMARK_NULL @"请输入备注"
#define ALERT_MSG_DISPATCH_PRICE_NULL @"请输入派单价格"
#define ALERT_MSG_CHOOSE_DISPATCH_OBJECT @"请选择派单单位"
#define ALERT_MSG_CHOOSE_VECHILE @"请选择车辆"
#define ALERT_MSG_LOCATION_NULL @"请输入到达地点"
#define ALERT_MSG_DISTANCE_NULL @"请输入路程距离"
#define ALERT_MSG_ENDTIME_NULL @"请输入结束时间"
#define ALERT_MSG_REMARK_NULL @"请输入备注"
#define ALERT_MSG_SETTLEMENT_PRICE_NULL @"请输入结算价格"
#define ALERT_MSG_RATE_ID_NULL @"请选择付款方式"
#define STR_ORDER_REMARK @"请保持车辆清洁，正装、备水、纸巾、雨伞，路桥费停车费垫付，不签不收"
#define ALERT_MSG_CONTENT_LIMIT @"内容限制在30字以内！"
#define ALERT_MSG_REMAKR_CONTENT_LIMIT @"备注内容限制在100字以内！"
#define ALERT_MSG_ROUGH_ITINERARY_CONTENT_LIMIT @"大致行程内容限制在200字以内！"
#define ALERT_MSG_BUSINESS_CONTENT_LIMIT @"经营范围限制在500字以内！"
#define ALERT_MSG_NO_CONTENT @"没有输入，请重新填写"
#define MSG_SEND_SMS_SUCCESS @"发送短信成功"
#define MSG_SEND_SMS_FAILURE @"发送短信失败"

#pragma mark - 支付结果文字
#define kMsgPayAuthFailure @"授权失败"
#define kMsgPaySuccess @"订单支付成功"
#define kMsgPayFailue @"订单支付失败"
#define kMsgPayUserCancelled @"用户中途取消支付"
#define kMsgPayNetworkFailed @"网络连接错误"
#define kMsgPayProcessing @"正在处理中..."

#pragma mark - 权限提示文字
#define kMsgOpenSystemPushnotification @"你的手机尚未开启消息推送，不能及时收到消息通知，请在系统设置中开启通知服务"
#define kMsgOpenSystemLocation @"无法获取您当前位置!\n请在系统设置中开启定位服务"
#define kMsgOpenSystemAlbumn @"您已关闭照片使用权限，请至手机\"设置->隐私->照片\"中打开"
#define kMsgOpenSystemCamera @"您已关闭相机使用权限，请至手机\"设置->隐私->相机\"中打开"
#define kFailedToFindCameraDevice @"未能找到相机设备"

#pragma mark - 网络连接提示
#define kMsgNotReachable @"网络不可用！请检查您的网络环境"

#pragma mark - 点赞提示文字
#define kMsgLike @"点赞成功"

#pragma mark - 广告订单提示文字
#define kMsgHasUnpayOrder @"有未支付订单，是否前往支付？"
#define kMsgAdvPurchaseOutofLimit @"当前版位今日购买数量已达上限，请重新选择"

/**
 *  tableView
 */
#define iCodeTableviewBgColor [UIColor colorWithHexString:@"#E2EAF2"]                  //tableview背景颜色
#define iCodeTableViewSectionMargin 25

/**
 *  导航条
 */
#define iCodeNavigationBarColor [UIColor colorWithHexString:@"#47B879"]                  //导航条颜色

/**
 *  tableView
 */
#define iCodeTableviewBgColor [UIColor colorWithHexString:@"#E2EAF2"]                  //tableview背景颜色
#define iCodeTableViewSectionMargin 25                                                 //section间距

/**
 *  头像cell高度
 */
#define iconRowHeight 74

/**
 *  普通cell高度
 */
#define nomalRowHeight 44

/**
 *  Code圈
 */
#define circleCellMargin 15  //间距
#define circleCelliconWH 40  //头像高度、宽度
#define circleCellWidth ViewWidth  //cell的宽度
#define circleCellNameattributes @{NSFontAttributeName : [UIFont systemFontOfSize:16]}  //昵称att
#define circleCellNameFont [UIFont systemFontOfSize:16]                                 //昵称字号
#define circleCellTimeattributes @{NSFontAttributeName : [UIFont systemFontOfSize:13]}  //时间att
#define circleCellTimeFont [UIFont systemFontOfSize:13]                                 //时间字号
#define circleCellTextattributes @{NSFontAttributeName : [UIFont systemFontOfSize:13]}  //正文att
#define circleCellTextFont [UIFont systemFontOfSize:13]                                 //正文字号
#define circleCellPhotosWH (circleCellWidth - 2 * (circleCellMargin + circleCellPhotosMargin)) / 3                                                                                   //图片的宽高
#define circleCellPhotosMargin 5                                                        //图片间距
#define circleCellToolBarHeight 35                                                      //cell工具条高度
#define circleCellToolBarTintColor [UIColor colorWithHexString:@"#ffffff"]              //工具条图标、字体颜色
#define circleCellToolBarTittleFont [UIFont systemFontOfSize:14]                        //工具条btn字号



#endif /* ResourceConstants_h */
