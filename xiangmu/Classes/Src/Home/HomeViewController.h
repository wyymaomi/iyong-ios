//
//  HomeViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/27.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "SDCycleScrollView.h"
#import "OnlineServiceModel.h"
#import "UserExpericeModel.h"
#import "HomeViewModel.h"
#import "ProviderDetailViewController.h"
#import "GYZChooseCityController.h"

#define DOWNLOAD_MAX_COUNT 7

#define TABLE_ROW_COUNT 5
//#define TABLE_ROW_COUNT 13//8



@interface HomeViewController : BaseViewController
{
    dispatch_semaphore_t _lock;
    BOOL isFirstIn;
}

@property (nonatomic, assign) BOOL isFirstIn;

@property (nonatomic, assign) AdvDisplayType showAdvStatus; // 0:初始化；1:暂停显示广告，重新进入时继续显示广告；2：广告显示

@property (nonatomic, strong) NSString *currentCityCode;//当前定位城市

@property (nonatomic, strong) NSString *gpsCityShortname;

@property (nonatomic, strong) SDCycleScrollView *bannerView;

@property (nonatomic, strong) HomeViewModel *viewModel ;

-(void)setNavTitleView;

@property (nonatomic, assign) NSInteger downloadCount;

@property (nonatomic, assign) NSInteger requestMaxCount; //

@property (nonatomic, assign) NSInteger tableViewRows; // 表格行数

- (void)fetchAllData;

@end
