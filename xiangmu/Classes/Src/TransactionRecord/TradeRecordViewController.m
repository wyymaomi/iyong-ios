//
//  TradeRecordViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "TradeRecordViewController.h"
#import "PullRefreshTableView.h"
#import "TradeRecordHeaderView.h"
#import "TradeRecordTableViewCell.h"
#import "ActionSheetDatePicker.h"
#import "TradeModel.h"
#import "DateUtil.h"


@interface TradeRecordViewController ()<UITableViewDataSource, UITableViewDelegate, PullRefreshTableViewDelegate>
//@property (strong, nonatomic) PullRefreshTableView *tableView;
@property (strong, nonatomic) NSNumber *expend;
@property (strong, nonatomic) NSNumber *income;
@property (strong, nonatomic) NSDate *date;

@end

@implementation TradeRecordViewController


#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"交易记录";
    self.date = [NSDate date];
    [self.view addSubview:self.pullRefreshTableView];
    self.pullRefreshTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // 去掉导航栏下方的横线
//    UINavigationBar *navigationBar = self.navigationController.navigationBar;
//    [navigationBar setBottomBorderColor:NAVBAR_BGCOLOR height:1];
    
    [self.pullRefreshTableView beginHeaderRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - footer refresh
-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        return;
    }
    
    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    TradeModel *item = self.dataList.lastObject;
    NSString *params = [NSString stringWithFormat:@"txTime=%@&time=%@",  [StringUtil getSafeString:[self.date stringWithDateFormat:@"yyyy-MM"]],[StringUtil getSafeString:item.createTime]];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, TRADE_RECORD_LIST_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        TradeResonseData *tradeRecordResponseData = [[TradeResonseData alloc] initWithDictionary:responseData error:nil];
        if ([tradeRecordResponseData.code integerValue] == STATUS_OK) {
            if (!IsArrEmpty(tradeRecordResponseData.data)) {
                [strongSelf.dataList addObjectsFromArray:tradeRecordResponseData.data];
                [strongSelf.pullRefreshTableView reloadData];
            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[tradeRecordResponseData.code integerValue]]];
        }
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        StrongSelf
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
    }];
    
    
}

#pragma mark - header refresh
-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    [self refreshTransactionSummary];
    [self refreshTransactionList];

}

-(void)refreshTransactionList
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, TRADE_RECORD_LIST_API];
    NSString *params = [NSString stringWithFormat:@"txTime=%@&time=", [StringUtil getSafeString:[self.date stringWithDateFormat:@"yyyy-MM"]]];
    self.loadStatus = LoadStatusLoading;
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        strongSelf.loadStatus = LoadStatusFinish;
        
        TradeResonseData *tradeRecordResponseData = [[TradeResonseData alloc] initWithDictionary:responseData error:nil];
        if ([tradeRecordResponseData.code integerValue] == STATUS_OK) {
            strongSelf.dataList = tradeRecordResponseData.data;
            [strongSelf.pullRefreshTableView reloadData];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[tradeRecordResponseData.code integerValue]]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        [weakSelf.pullRefreshTableView endHeaderRefresh];
        weakSelf.loadStatus = LoadStatusNetworkError;
        
    }];
}

-(void)refreshTransactionSummary
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, TRADE_SUMMARY_API];
    NSString *params = [NSString stringWithFormat:@"txTime=%@", [self.date stringWithDateFormat:@"yyyy-MM"]];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        [weakSelf.pullRefreshTableView endHeaderRefresh];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            weakSelf.income = data[@"income"];
            weakSelf.expend = data[@"expend"];
            [weakSelf.pullRefreshTableView reloadData];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        NSLog(@"response failure");
        [weakSelf.pullRefreshTableView endHeaderRefresh];
        
    }];
}

-(void)onSelectDate:(id)sender
{
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer*)sender;
    UIView *view = tapRecognizer.view;
    WeakSelf
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date]minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        StrongSelf
        if ([selectedDate isKindOfClass:[NSDate class]]) {
            strongSelf.date = selectedDate;
            [strongSelf.pullRefreshTableView beginHeaderRefresh];
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:view];
}

#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    
    TradeRecordHeaderView *headerView = [TradeRecordHeaderView customView];
    headerView.backgroundColor = NAVBAR_BGCOLOR;
    headerView.incomeLabel.text = IsNilOrNull(self.income) ? @"" : [self.income stringValue];
    headerView.outLabel.text = IsNilOrNull(self.expend) ? @"" : [self.expend stringValue];
    headerView.yearLabel.text = [DateUtil getYearStr:self.date];//[[self.date year] stringValue];
    headerView.monthLabel.text = [DateUtil getMonthStr:self.date];//[[self.date month] stringValue];
//    [headerView.dateView addTap]
//    UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showAlert:)];
    [headerView.dateView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelectDate:)]];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"BillTableViewCellID";
    TradeRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([TradeRecordTableViewCell class]) owner:nil options:nil] lastObject];
    }
    TradeModel *item = self.dataList[indexPath.row];
    cell.amountLabel.text = [NSString stringWithFormat:@"%@ %@", item.amountType, item.amount];//item.amount;
    cell.priceLabel.text = item.price;
    cell.dateLabel.text = item.date;
    cell.transactionType.text = item.transactionType;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    [self gotoOrderDetailPage:indexPath.section];
    
}



//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
