//
//  VechileTypeSelectViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileTypeSelectViewController.h"
#import "VechileTypeVIew.h"
#import "VechileOrderManager.h"

@interface VechileTypeSelectViewController ()
{
    UIPageControl *_pageControl;
}

@property (nonatomic, strong) NSArray *carDescList; // 车型具体信息列表
//@property (nonatomic, strong) NSArray *carTypeList;

//@property (nonatomic, strong) UIPageControl *pageControl;

@end

#define Vechile_Page_Count 3

@implementation VechileTypeSelectViewController

-(NSArray*)carDescList
{
    if (!_carDescList) {
        _carDescList = [[VechileOrderManager sharedInstance] getVechileDescTypeList];
    }
    return _carDescList;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"车型选择";

    self.view.backgroundColor = UIColorFromRGB(0xEBEBEB);
    
//    NSArray *carLargeImages = @[@"img_vechile_comfortable",
//                                @"img_vechile_commercial",
//                                @"img_vechile_luxure",];
//    
//    NSArray *carSmallImages = @[@"img_vechile_comfortable_small",
//                                @"img_vechile_commercial_small",
//                                @"img_vechile_luxure_small"];
//    
//    NSArray *carTypeNames = @[@"舒适型", @"商务型", @"豪华型"];
//    NSArray *carModelNames = @[@"别克GL8", @"奥迪A6L", @"奔驰S级"];
//    NSArray *seatNames = @[@"7座", @"5座", @"5座"];
    
//    NSArray *carDescList = carTypeList();
    
    

    
    
    UIScrollView *myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, self.view.frame.size.height)];
    for (NSUInteger i=0; i < self.carTypeList.count; i++) {
        
        CarTypeModel *model = self.carTypeList[i];
        
        __block CarDescModel *carDesc;
        
//        for (CarDescModel *lCarDesc in self.carDescList) {
//            if ([lCarDesc.type integerValue] == [model.carType integerValue]) {
//                carDesc = lCarDesc;
//            }
//        }
        
        [self.carDescList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
           
            if ([obj isKindOfClass:[CarDescModel class]]) {
                
                CarDescModel *carDescModel = obj;
                
                if ([carDescModel.type integerValue] == [model.carType integerValue]) {
                    carDesc = carDescModel;
                    *stop = YES;
                }
            }
            
        }];
        
        VechileTypeView *view = [VechileTypeView customView];
        view.frame = CGRectMake(ViewWidth * i, 0, ViewWidth, self.view.frame.size.height);
        // 上面的文字和图片
        view.vechileTypeLabel.text = [NSString stringWithFormat:@"%@(%@同款以上车型)%@", carDesc.carTypeName, carDesc.carModelName, carDesc.seatName];
        view.vechilePriceLabel.text = [NSString stringWithFormat:@"起步价:%@元(%@公里)，超出部分按%@元每公里计算", [model.startingPrice stringValue], [model.startingMileage stringValue], [model.unitPrice stringValue]];
//        view.vechilePriceLabel.text = [NSString stringWithFormat:@"每公里%@元", [model.unitPrice stringValue]];
        view.vechileTypeImageView.image = [UIImage imageNamed:carDesc.carLargeImage];//[UIImage imageNamed:carLargeImages[i]];
        // 下面的文字和图片
        view.vechileTypeLabel2.text = [NSString stringWithFormat:@"%@%@", carDesc.carTypeName, carDesc.seatName];
        view.vechileTypeSmallImageView.image = [UIImage imageNamed:carDesc.carSmallImage];//[UIImage imageNamed:carSmallImages[i]];
        view.vechileModelLabel.text = carDesc.carModelName;//carModelNames[i];
        view.vechilePriceLabel2.text = [NSString stringWithFormat:@"%@元/公里", [model.unitPrice stringValue]];
        view.tag = i;
        view.userInteractionEnabled = YES;
        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickCarModel:)]];
        [myScrollView addSubview:view];
    }
    myScrollView.bounces = NO;
    myScrollView.pagingEnabled = YES;
    myScrollView.showsHorizontalScrollIndicator = NO;
    myScrollView.contentSize = CGSizeMake(ViewWidth * self.carTypeList.count, self.view.frame.size.height);
    myScrollView.delegate = self;
    myScrollView.backgroundColor = UIColorFromRGB(0xEBEBEB);
    [self.view addSubview:myScrollView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(ViewWidth/3, 20, ViewWidth/3, 12.0*Scale)];
//     设置页数
    _pageControl.numberOfPages = self.carTypeList.count;
    _pageControl.currentPage = 0;
//     设置页码的点的颜色
    _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];//[UIColor whiteColor];
//     设置当前页码的点颜色
    _pageControl.currentPageIndicatorTintColor = NAVBAR_BGCOLOR;//UIColorFromRGB(0xF9BF3C);
    
    [self.view addSubview:_pageControl];
    
    [self setRightNavigationItemWithTitle:@"确定" selMethod:@selector(onClickOKButton:)];
    
    
//    [self.carDescList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        
//        if ([obj isKindOfClass:[CarDescModel class]]) {
//            
//            CarDescModel *carDescModel = obj;
//            
//            if ([carDescModel.type integerValue] == [model.carType integerValue]) {
//                carDesc = carDescModel;
//                *stop = YES;
//            }
//        }
//        
//    }];
    
    __block NSUInteger currentPage = 0;
    
    [self.carTypeList enumerateObjectsUsingBlock:^(CarTypeModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.carType integerValue] == self.carType) {
            *stop = YES;
        }
        else {
            currentPage++;
        }
        
    }];

    
    
    
    _pageControl.currentPage = currentPage;
    CGPoint point = CGPointMake(ViewWidth*_pageControl.currentPage, 0);
    [myScrollView setContentOffset:point animated:YES];
    
    
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    _pageControl.frame = CGRectMake(ViewWidth/3, self.view.frame.size.height-45*Scale, ViewWidth/3, 12*Scale);

}

-(void)onClickOKButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(onSelectVechile:)]) {
        [self.delegate onSelectVechile:_pageControl.currentPage];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onClickCarModel:(UITapGestureRecognizer *)recognizer
{
    
    DLog(@"%ld", recognizer.view.tag);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onSelectVechile:)]) {
        [self.delegate onSelectVechile:_pageControl.currentPage];
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // 计算当前在第几页
    _pageControl.currentPage = (NSInteger)(scrollView.contentOffset.x / [UIScreen mainScreen].bounds.size.width);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
