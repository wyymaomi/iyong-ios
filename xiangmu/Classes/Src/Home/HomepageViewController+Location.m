//
//  HomepageViewController+Location.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HomepageViewController+Location.h"
#import "GYZChooseCityController.h"
#import "AppConfig.h"

@implementation HomepageViewController (Location)


- (void)didSelectCity:(id)sender
{
    self.isAccountInfoShow = NO;
    GYZChooseCityController *cityPickerVC = [[GYZChooseCityController alloc] init];
    [cityPickerVC setDelegate:self];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:cityPickerVC] animated:YES completion:^{
        //
    }];
}

#pragma mark - GYZCityPickerDelegate
- (void) cityPickerController:(GYZChooseCityController *)chooseCityController didSelectCity:(GYZCity *)city
{
    [AppConfig currentConfig].defaultCityName = city.shortName;
    [AppConfig currentConfig].defaultCityCode = city.cityID;
    
    [self setNavTitleView];
    
    //    [self.searchBar.cityButton setTitle:city.cityName forState:UIControlStateNormal];
    //    [LocationService saveCityToConfig:city.cityName];
    //    self.searchBar.searchTextField.text = @"";
    
    [chooseCityController dismissViewControllerAnimated:YES completion:nil];
}

- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController
{
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}



@end
