//
//  ProviderDetailModel.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderDetailModel.h"

@implementation ProviderDetailModel

- (NSString*)displayDescriptionText;
{
    if (_blurb.length==0) {
        return @"该公司暂无简介";
    }
    
    return _blurb;
}

@end
