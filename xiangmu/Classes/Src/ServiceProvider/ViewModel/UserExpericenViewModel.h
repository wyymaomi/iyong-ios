//
//  UserExpericenViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserExpericeModel.h"


@interface UserExpericenViewModel : NSObject

/**
 *  数据模型
 */
@property (nonatomic ,strong) UserExpericeModel *serviceModel;

@property (nonatomic, strong) UIFont *nickNameFont;
@property (nonatomic, strong) UIFont *timeFont;
@property (nonatomic, strong) UIFont *contentFont;

@property (nonatomic, strong) UIFont *browsesFont;
@property (nonatomic, strong) UIFont *commentsFont;
@property (nonatomic, strong) UIFont *likesFont;
/**
 *  主体Frame
 */
@property (nonatomic ,assign) CGRect momentsBodyFrame;

@property (nonatomic, assign) CGRect bodyCompanyFrame;
//昵称Frame
@property (nonatomic ,assign) CGRect bodyNameFrame;
//头像Frame
@property (nonatomic ,assign) CGRect bodyIconFrame;
//时间Frame
@property (nonatomic ,assign) CGRect bodyTimeFrame;
//正文Frame
@property (nonatomic ,assign) CGRect bodyTextFrame;
//图片Frame
@property (nonatomic ,assign) CGRect bodyPhotoFrame;


/**
 *  工具条Frame
 */
@property (nonatomic, assign) CGRect momentsToolBarFrame;
// 查看数
@property (nonatomic, assign) CGRect toolViewNumTextFrame;
@property (nonatomic, assign) CGRect toolViewNumIconFrame;
// 评论数
@property (nonatomic, assign) CGRect toolCommentNumTextFrame;
@property (nonatomic, assign) CGRect toolCommentNumIconFrame;
// 点赞数
@property (nonatomic, assign) CGRect toolLikeTextFrame;
@property (nonatomic, assign) CGRect toolLikeIconFrame;
//点赞Frame
//@property (nonatomic ,assign) CGRect toolLikeFrame;
//收藏Frame
//@property (nonatomic ,assign) CGRect toolFavoriteFrame;
// 通话Frame
//@property (nonatomic, assign) CGRect toolTelFrame;

/**
 *  cell高度
 */
@property (nonatomic ,assign) CGFloat cellHeight;


@end
