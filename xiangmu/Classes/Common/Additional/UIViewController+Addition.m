//
//  UIViewController+Addition.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UIViewController+Addition.h"
#import "LoginViewController.h"
#import "GesturePasswordController.h"
#import "GestureLoginViewController.h"
#import "RealnameCertificationViewController.h"
#import "BBAlertView.h"
#import "BaseWebpageViewController.h"
#import "BaseWebViewController.h"
#import "RealnameCertificationViewController.h"

@implementation UIViewController (Addition)

-(BOOL)openCompanyModePage
//-(void)openCompanyModePage
//-(void)openCompanyModePage
{
    // 判断是否实名认证
    // 判断是否企业认证
    BOOL isRealnameCertification = [UserManager sharedInstance].userModel.isRealnameCertified;
//    BOOL isCompanyCertification = [UserManager sharedInstance].userModel.isCompanyCertified;
    BOOL isCompanyCertification = NO;
    if (!isRealnameCertification) {
        [self showRealnameCertificationAlertView];
//        return;
        return NO;
    }
    if (!isCompanyCertification) {
        
//        [self showHUDIndicatorViewAtCenter:@"正在查询，请稍候..."];
        
//        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        WeakSelf
        
        __block BOOL isCertificationed = NO;
        
        [[UserManager sharedInstance] getAccountInfo:^(NSInteger code_status) {
            
//            [weakSelf hideHUDIndicatorViewAtCenter];
            
            if (code_status == STATUS_OK) {
                
                if ([[UserManager sharedInstance].userModel isCompanyCertified]) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kOnCompanyCertificationedNotification object:nil];
                    
                    isCertificationed = YES;
                    
                }
                else {
                    
                    NSInteger companyCertificationStatus = [[UserManager sharedInstance].userModel.companyCertification integerValue];
                    
                    if (companyCertificationStatus == CompanyCertificationStatusInReview) {
                        
                        BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"企业认证状态待审核" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertView setConfirmBlock:^{
//                            [alertView dismiss];
                        }];
                        [alertView show];
                        
                        isCertificationed = NO;
                        
                    }
                    else {
                        [weakSelf showCompanyCertificationAlertView];
                        
                        isCertificationed = NO;
                        
                    }
                    
                    
                    
                    
//                    return NO;
                    
                }
            }
            else {
                
                [weakSelf showCompanyCertificationAlertView];
                
                isCertificationed = NO;
                
//                return;
                
            }
            
//            dispatch_semaphore_signal(semaphore);
            
        }];
        
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        
        return isCertificationed;
        
    }
    
    
    
    return YES;
//    else {
//        [self gotoPage:@"CompanyModeViewController"];
//    }
    
    
}

-(void)showRealnameCertificationAlertView
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"还未实名认证" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"前往"];
    WeakSelf
    [alertView setConfirmBlock:^{
//        [weakSelf gotoPage:@"RealnameCertificationViewController"];
//        Class cls = NSClassFromString(@"RealnameCertificationViewController");
//        if (!cls) {
//            return;
//        }
//        BaseViewController *vc = [[cls alloc]init];
//        vc.enter_type = ENTER_TYPE_PRESENT;
//        vc.hidesBottomBarWhenPushed = YES;
//        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
//        [weakSelf presentViewController:navigationController animated:YES completion:nil];
        
        RealnameCertificationViewController *viewController = [[RealnameCertificationViewController alloc] init];
        viewController.type = [[UserManager sharedInstance].userModel.certification boolValue]?0:1;
        viewController.hidesBottomBarWhenPushed = YES;
        viewController.enter_type = ENTER_TYPE_PRESENT;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        [weakSelf presentViewController:navigationController animated:YES completion:nil];
//        [self.navigationController pushViewController:viewController animated:YES];
    }];
    
    [alertView show];
}

-(void)showCompanyCertificationAlertView
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"还未企业认证" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"前往"];
    [alertView setConfirmBlock:^{
        
        Class cls = NSClassFromString(@"CompanyCertificationViewController");
        if (!cls) {
            return;
        }
        BaseViewController *vc = [[cls alloc]init];
        vc.enter_type = ENTER_TYPE_PRESENT;
        vc.hidesBottomBarWhenPushed = YES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:navigationController animated:YES completion:nil];
        
//        [self gotoPage:@"CompanyCertificationViewController"];
    }];
    [alertView show];
}

#pragma mark - 判断是否登录

- (void)showLoginAlertView
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleLogin Title:nil message:@"请先进行登录" customView:nil delegate:nil cancelButtonTitle:@"稍后再去" otherButtonTitles:@"立即登录！"];
//    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleLogin Title:nil message:@"对不起，您还没有登录！" customView:nil delegate:nil cancelButtonTitle:@"稍后再去" otherButtonTitles:@"立即登录！"];
    WeakSelf
    [alertView setConfirmBlock:^{
        if (![[UserManager sharedInstance] isLogin])
        {
            [weakSelf presentLogin];
        }
    }];
    [alertView show];
}

- (void)showLoginAlertViewWithMsg:(NSString*)msg
{
    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleLogin Title:nil message:msg customView:nil delegate:nil cancelButtonTitle:@"稍后再去" otherButtonTitles:@"立即登录！"];
    WeakSelf
    [alertView setConfirmBlock:^{
        if (![[UserManager sharedInstance] isLogin])
        {
            [weakSelf presentLogin];
        }
    }];
    [alertView show];
}

#pragma mark - 显示等待弹窗
- (void)showHUDIndicatorViewAtCenter:(NSString*)indiTitle
{
    [self.view showHUDIndicatorViewAtCenter:indiTitle];
}

#pragma mark - 隐藏等待弹窗
- (void)hideHUDIndicatorViewAtCenter;
{
    [self.view hideHUDIndicatorViewAtCenter];
}


#pragma mark -
- (void)showAlertView:(NSString *)msg
{
//    if ([NSThread isMainThread]) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
//        [alertView show];
//    } else {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
//            [alertView show];
//        });
//    }
    [MsgToolBox showToast:msg];
    
}

#pragma mark - 登录

-(void)presentLogin
{
    if ([[UserManager sharedInstance] isGesturePwdLogin]) {
        GestureLoginViewController *gesturePwdController = [[GestureLoginViewController alloc] init];
        gesturePwdController.enter_type = ENTER_TYPE_PRESENT;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:gesturePwdController];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else {
        LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        loginViewController.enter_type = ENTER_TYPE_PRESENT;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginViewController];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

- (void)presentAccountLogin
{
    LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    loginViewController.enter_type = ENTER_TYPE_PRESENT;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark - 手势密码登录
-(void)presentGestureLogin
{
    GestureLoginViewController *viewController = [[GestureLoginViewController alloc] init];
    viewController.title = @"手势密码登录";
    viewController.enter_type = ENTER_TYPE_PRESENT;
//    viewController.source_from = source_from;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - 手势密码设置
-(void)presentGestureSetting:(NSInteger)source_from
{
    GesturePasswordController *viewController = [[GesturePasswordController alloc] init];
    viewController.title = @"设置手势密码";
    viewController.enter_type = ENTER_TYPE_PRESENT;
    viewController.source_from = source_from;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)dismissToRootViewController:(BOOL)flag completion: (void (^ __nullable)(void))completion;
{
    UIViewController *rootVC = self.presentingViewController;
    while (rootVC.presentingViewController) {
        rootVC = rootVC.presentingViewController;
    }
    
    [rootVC dismissViewControllerAnimated:flag completion:completion];
}

#pragma mark - 

-(void)openWebView:(NSString*)url;
{
    BaseWebViewController *viewController = [[BaseWebViewController alloc] init];
    viewController.url = url;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)gotoWebpage:(NSString*)url;
{
    BaseWebpageViewController *viewController = [[BaseWebpageViewController alloc] init];
    viewController.url = url;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - 进入实名认证页面

-(void)gotoRealnameCertificationPage
{
    RealnameCertificationViewController *viewController = [[RealnameCertificationViewController alloc] init];
    viewController.type = [[UserManager sharedInstance].userModel.certification boolValue]?0:1;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
