//
//  CommunityHeaderTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityHeaderTableViewCell.h"
#import "CertifiedView.h"
#import "StarView.h"

@implementation CommunityHeaderTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    
    static NSString *ID =  @"CommunityHeaderTableViewCell";
    CommunityHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.nickNameLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.nickname] ;
    cell.mobileLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.username];
    cell.companyLabel.text = [StringUtil getSafeString:[UserManager sharedInstance].userModel.companyName];
//    LRWeakSelf(cell)
//    [cell.avartarImageView downloadImage:[UserManager sharedInstance].userModel.logoUrl placeholderImage:[UIImage imageNamed:@"icon_avatar"] success:^(id responseData) {
//        weakcell.avartarImageView.cornerRadius = cell.avartarImageView.frame.size.width/2;
//    } andFailure:^(NSString *errorDesc) {
//        weakcell.avartarImageView.cornerRadius = 5;
//    }];
    cell.avartarImageView.cornerRadius = cell.avartarImageView.width/2;
//    [cell.avartarImageView downloadImageFromAliyunOSS:[UserManager sharedInstance].userModel.logoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:nil andFailure:nil];
    
    
    return cell;
}


-(void)initData
{
    BOOL isCompanyCertified = [[UserManager sharedInstance].userModel.companyCertification integerValue] == CompanyCertificationStatusFinished;
    BOOL isPersonCertified = [[UserManager sharedInstance].userModel.certification boolValue];
    BOOL isVechileCertified = [[UserManager sharedInstance].userModel.carCertification boolValue];
    [self.certifiedView initData:isPersonCertified isCompanyCertified:isCompanyCertified isVechileCertified:isVechileCertified];
    
    self.starView.width = kStarFrameWith * 5;
    [self.starView setStarsImages:[[UserManager sharedInstance].userModel.companyScor floatValue]];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
