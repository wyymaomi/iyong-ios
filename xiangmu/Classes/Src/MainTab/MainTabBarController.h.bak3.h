//
//  MainTabBarController.h
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountInfoView.h"

@interface MainTabBarController : UITabBarController

- (void)setCenterButtonSelected:(BOOL)selected;

@property (nonatomic, strong) AccountInfoView *accountInfoView;
@property (nonatomic, assign) BOOL isAccountInfoShow;// 是否显示帐户页面
@property (nonatomic, assign) SEL nextAction;

//@property (nonatomic, strong) UINavigationController *homeNavigation;
//@property (nonatomic, retain) UINavigationController *orderNavigation;
//@property (nonatomic, retain) UINavigationController *billNavigation;
//@property (nonatomic, retain) UINavigationController *meNavigation;
//@property (nonatomic, retain) UINavigationController *communityNavigation;

//@property (nonatomic, assign) NSInteger tabIndex;

@end
