//
//  CompanyCommentTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProviderCommentViewModel.h"
#import "CompanyCommentBodyView.h"

@interface CompanyCommentTableViewCell : UITableViewCell




//@property (nonatomic, weak) id<UserExperienceCellDelegate> delegate;

//@property (nonatomic, strong) UIImageView *

@property (nonatomic, strong) ProviderCommentViewModel *viewModel;

// 主体
@property (nonatomic, strong) CompanyCommentBodyView *bodyView;
// 工具条
//@property (nonatomic, strong) CommentToolBarView *toolView;

+ (instancetype)momentsTableViewCellWithTableView:(UITableView *)tableView;

@end
