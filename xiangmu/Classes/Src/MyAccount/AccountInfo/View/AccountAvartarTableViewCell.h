//
//  AccountAvartarTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountAvartarTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *avatarImageView;// 头像
@property (nonatomic, strong) UILabel *nickNameLabel;
@property (nonatomic, strong) UILabel *mobileLabel;
@property (nonatomic, strong) UILabel *companyLabel;

@end
