//
//  BaseOrderTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseOrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
