//
//  BaseViewController+navigation.m
//  xiangmu
//
//  Created by David kim on 16/9/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController+navigation.h"
//#import "BaseWebViewController.h"
#import "BaseWebpageViewController.h"

@implementation UIViewController (navigation)

-(void)openWebPage:(NSString*)url;
{
    BaseWebpageViewController *viewController = [[BaseWebpageViewController alloc] init];
    viewController.url = url;
    viewController.enter_type = ENTER_TYPE_PUSH;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)gotoPage:(NSString*)vcName
{
    [self gotoPage:vcName animated:YES];
}

- (void)gotoPage:(NSString*)vcName animated:(BOOL)animated
{
    if (IsStrEmpty(vcName)) {
        return;
    }
    Class cls = NSClassFromString(vcName);
    if (!cls) {
        return;
    }
    BaseViewController *vc = [[cls alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:animated];
}

- (void)gotoPage:(NSString*)vcName animated:(BOOL)animated title:(NSString*)title
{
    if (IsStrEmpty(vcName)) {
        return;
    }
    Class cls = NSClassFromString(vcName);
    if (!cls) {
        return;
    }
    BaseViewController *vc = [[cls alloc]init];
    vc.title = title;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:animated];
}

- (void)presentPage:(NSString*)vcName animated:(BOOL)animated
{
    if (IsStrEmpty(vcName)) {
        return;
    }
    Class cls = NSClassFromString(vcName);
    if (!cls) {
        return;
    }
    BaseViewController *vc = [[cls alloc]init];
//    vc.title = title;
    vc.hidesBottomBarWhenPushed = YES;
    [self presentViewController:vc animated:YES completion:nil];
//    [self.navigationController pushViewController:vc animated:animated];
}

@end
