//
//  HyperlinksButton.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HyperlinksButton.h"

@implementation HyperlinksButton

-(void)setLineColor:(UIColor *)lineColor
{
    _lineColor = [lineColor copy];
    [self setNeedsLayout];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGRect textRect = self.titleLabel.frame;
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    CGFloat descender = self.titleLabel.font.descender;
    if ([self.lineColor isKindOfClass:[UIColor class]]) {
        CGContextSetStrokeColorWithColor(contextRef, self.lineColor.CGColor);
    }
    
    CGContextMoveToPoint(contextRef, textRect.origin.x, textRect.origin.y + textRect.size.height + descender+5);
    CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width, textRect.origin.y + textRect.size.height + descender+5);
    
    CGContextClosePath(contextRef);
    CGContextDrawPath(contextRef, kCGPathStroke);
}


@end
