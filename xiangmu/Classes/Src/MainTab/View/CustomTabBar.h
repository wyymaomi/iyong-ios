//
//  CustomTabBar.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CustomTabBar;

@protocol CustomTabBarDelegate <NSObject>

@optional
-(void)tabBarCenterButtonClick:(CustomTabBar*)tabBar;

@end

@interface CustomTabBar : UITabBar

@property (nonatomic, weak) id<CustomTabBarDelegate>  delegate;

@property (nonatomic, weak) UIButton *centerButton;
@property (nonatomic, weak) UIImageView *semiCircleImageView;
@property (nonatomic, weak) UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
