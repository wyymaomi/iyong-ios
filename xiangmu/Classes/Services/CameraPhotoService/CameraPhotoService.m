//
//  CameraPhotoService.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CameraPhotoService.h"
#import "AuthorizationUtil.h"
#import "UIRoute.h"

@interface CameraPhotoService ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UIActionSheet *actionSheet;

@property (nonatomic, strong) UIAlertController *alertController;

@property (nonatomic, weak)  UIViewController *viewCotnroller;

@property (nonatomic, strong) UINavigationController *navigationController;

//@property (nonatomic, weak) UIView *view;

@property (nonatomic, strong) CameraPhotoCompleteBlock onActionSheetDone;

@property (nonatomic, strong) GetImageDataBlock onGetImageData;

@end

@implementation CameraPhotoService

- (id)initWithViewController:(id)navigationController type:(CameraPhotoType)type;
{
    if (self = [super init]) {
        if ([navigationController isKindOfClass:[UIViewController class]]) {
            _viewCotnroller = navigationController;
        }
        if ([navigationController isKindOfClass:[UINavigationController class]]) {
            _navigationController = navigationController;
        }
//        if ([view isKindOfClass:[UIView class]]) {
//            _view = view;
//        }
        _type = type;
        
    }
    return self;
}

- (void)dealloc
{
    _navigationController = nil;
}

- (id)initWithViewController:(id)navigationController
                        type:(CameraPhotoType)type
               completeBlock:(CameraPhotoCompleteBlock)completeBlock
              onGetImageData:(GetImageDataBlock)onGetImageData
{
    self = [super init];
    if (self) {
//        if ([viewController isKindOfClass:[UIViewController class]]) {
//            _viewCotnroller = viewController;
//        }
//        if ([view isKindOfClass:[UIView class]]) {
//            _view = view;
//        }
        
        if ([navigationController isKindOfClass:[UIViewController class]]) {
            _viewCotnroller = navigationController;
        }
        
        if ([navigationController isKindOfClass:[UINavigationController class]]) {
            _navigationController = navigationController;
        }
        _onActionSheetDone = completeBlock;
        _onGetImageData = onGetImageData;
    }
    return self;
}

- (void)show
{
//    if (IOS8_OR_LATER) {
//        if ([self.viewCotnroller isKindOfClass:[UIViewController class]]) {
//            [(UIViewController*)self.viewCotnroller presentViewController:self.alertController animated:YES completion:nil];
//        }
////        [self.viewController presentViewController:self.alertController animated:YES completion:nil];
//    }
//    else {
//        [self.actionSheet showInView:((UIViewController*)self.viewCotnroller).view];
//    }
    
//    [self.actionSheet showInView:self.viewCotnroller.view];
    
    [self.actionSheet showInView:self.navigationController.view];
}

- (void)show:(NSUInteger)tag;
{
//    if (IOS8_OR_LATER) {
//        if ([self.viewCotnroller isKindOfClass:[UIViewController class]]) {
//            self.tag = tag;
//            [(UIViewController*)self.viewCotnroller presentViewController:self.alertController animated:YES completion:nil];
//        }
////        self.alertController
////        [self.viewController presentViewController:self.alertController animated:YES completion:nil];
//    }
//    else {
//    [self.actionSheet showInView:self.viewCotnroller.view];
//        [self.actionSheet showInView:self.viewCotnroller.view];
//        [self.actionSheet showInView:self.window];
    
    [self.actionSheet showInView:self.navigationController.view];
        self.actionSheet.tag = tag;
//    }
}

- (UIAlertController*)alertController
{
    if (!_alertController) {
        WeakSelf
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf LocalPhoto];
        }];
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf takePhoto];
        }];
        
        _alertController = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle: UIAlertControllerStyleActionSheet];
        [_alertController addAction:cancelAction];
        [_alertController addAction:photoAction];
        [_alertController addAction:cameraAction];
        
    }
    
    return _alertController;
}

- (UIActionSheet*)actionSheet
{
    if (!_actionSheet) {
        _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:@"取消"
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:@"拍照", @"从相册选择", nil];
    }
    return _actionSheet;
}

#pragma mark - UIActionSheet Delegate Method

//-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == actionSheet.cancelButtonIndex) {
//        
//    }
//    switch (buttonIndex) {
//        case 0:
////            [self takePhoto];
//            break;
//        case 1:
////            [self LocalPhoto];
//            break;
//        default:
//            break;
//    }
//}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        
    }
    switch (buttonIndex) {
        case 0:
            [self takePhoto];
            break;
        case 1:
            [self LocalPhoto];
            break;
        default:
            break;
    }
}

//开始拍照
-(void)takePhoto
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        if (![AuthorizationUtil isAllowVideo]) {
            [UIRoute openSystemSetting:kMsgOpenSystemCamera];
            return;
        }
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.delegate = self;
        //设置拍照后的图片可被编辑
        cameraPicker.allowsEditing = YES;
        cameraPicker.sourceType = sourceType;
//        UIViewController *viewController = (UIViewController*)self.viewCotnroller;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {        //判断当前设备的系统是否是ios7.0以上
            cameraPicker.edgesForExtendedLayout = UIRectEdgeNone;
        }
        
//        [self.navigationController presentViewController:cameraPicker animated:YES completion:nil];
        [self.viewCotnroller presentViewController:cameraPicker animated:YES completion:nil];
    }
    else
    {
        [UIRoute openSystemSetting:kFailedToFindCameraDevice];
        DLog(@"模拟其中无法打开照相机,请在真机中使用");
    }
}

//打开本地相册
-(void)LocalPhoto
{

    if (![AuthorizationUtil isAllowAlbumn]) {
        [UIRoute openSystemSetting:kMsgOpenSystemAlbumn];
        return;
    }
    
    UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
    photoPicker.view.backgroundColor = [UIColor orangeColor];
    photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    photoPicker.delegate = self;
    photoPicker.navigationBar.barStyle = UIBarStyleDefault;
    photoPicker.navigationBar.barTintColor = [UIColor whiteColor];
//    photoPicker.navigationBar.barTintColor = NAVBAR_BGCOLOR;//[UIColor colorWithRed:20.f/255.0 green:24.0/255.0 blue:38.0/255.0 alpha:1];
    photoPicker.navigationBar.tintColor = [UIColor blackColor];//[UIColor whiteColor];
    //设置选择后的图片可被编辑
    photoPicker.allowsEditing = YES;
//    UIViewController *viewController = (UIViewController*)self.viewCotnroller;
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {        //判断当前设备的系统是否是ios7.0以上
//        photoPicker.edgesForExtendedLayout = UIRectEdgeNone;
//    }
    
    [self.viewCotnroller presentViewController:photoPicker animated:YES completion:nil];
    
//    self.navigationController prese
//    [self.navigationController pushViewController:photoPicker animated:YES];
    
//    [self.navigationController presentViewController:photoPicker animated:YES completion:nil];
    
}

#pragma mark - UIImagePickerControllDelegate method

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    WeakSelf
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
//        @autoreleasepool {
        
            NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        
            if ([mediaType isEqualToString:@"public.image"]) {
                
                UIImage* image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
                
                CGSize imageSize;//= CGSizeMake(640,640);
                NSInteger maxImageFileLength;// = 200 * 1024;
                
                switch (weakSelf.type) {
                    case CameraPhotoLarge:
                        imageSize = CGSizeMake(640, 640);
                        maxImageFileLength = 1024 * 1024;
                        break;
                    case CameraPhotoTypeMedium:
                        imageSize = CGSizeMake(320, 320);
                        maxImageFileLength = 150 * 1024;
                    case CameraPhotoTypeSmall:
                        imageSize = CGSizeMake(100, 100);
                        maxImageFileLength = 5 * 1024;
                        break;
                    default:
                        break;
                }
                DLog(@"type = %lu", (unsigned long)weakSelf.type);
                
                UIImage *tempImage = [ImageUtil imageWithImage:image scaledToSize:imageSize];
                
                NSData *data = [ImageUtil compressImageWithData:tempImage toMaxFileSize:maxImageFileLength];
                
                DLog(@"data.size = %lu", (unsigned long)data.length);
                
                if (weakSelf.onGetImageData) {
                    weakSelf.onGetImageData(data, weakSelf.actionSheet.tag);
                }
                else {
                    if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(onGetImageData:tag:)]) {
                        [weakSelf.delegate onGetImageData:data tag:weakSelf.actionSheet.tag];
                    }
                }
            }
            
//        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [picker dismissViewControllerAnimated:YES completion:nil];
            if (weakSelf.onActionSheetDone) {
                weakSelf.onActionSheetDone(weakSelf.actionSheet.tag);
            }
            
        });
    });
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"您取消了选择图片");
    [picker dismissViewControllerAnimated:YES completion:nil];
}


@end
