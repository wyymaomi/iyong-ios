//
//  CreditLineViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface CreditLineViewController : BaseViewController

@property (nonatomic, strong) NSNumber *creditLine;// 信用额度


@end
