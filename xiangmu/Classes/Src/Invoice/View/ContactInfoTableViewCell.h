//
//  ContactInfoTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *areaView;
@property (weak, nonatomic) IBOutlet UITextField *contactNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *contactPhoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressDetailTextField;
@property (weak, nonatomic) IBOutlet UITextField *contactAreaTextField;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+(CGFloat)getCellHeight;

@end
