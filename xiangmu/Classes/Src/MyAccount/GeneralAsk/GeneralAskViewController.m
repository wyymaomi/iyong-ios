//
//  GeneralAskViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "GeneralAskViewController.h"
#import "GeneralAskHeaderTableViewCell.h"
#import "GeneralAskDetailViewController.h"
#import "WithdrawRecordListViewController.h"

@interface GeneralAskViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, strong) NSArray *helpContents;

@end

@implementation GeneralAskViewController

-(UITableView*)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];   
    }
    return _tableView;
}

-(NSArray*)helpContents
{
    if (IsArrEmpty(_helpContents)) {
        // 加载plist中的字典数据
        NSString *path = [[NSBundle mainBundle] pathForResource:BALANCE_HELP_PLIST ofType:nil];
        _helpContents = [NSArray arrayWithContentsOfFile:path];
    }
    return _helpContents;
}

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"余额提现问题";
    [self.view addSubview:self.tableView];
    self.selectedIndex = 0;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 

-(void)onRechargeHelpClick
{
    self.title = @"余额充值问题";
    self.selectedIndex = 1;
    [self.tableView reloadData];
}

-(void)onWithdrawHelpClick
{
    self.title = @"余额提现问题";
    self.selectedIndex = 0;
    [self.tableView reloadData];
}

#pragma mark - UITableView Delegate method

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 100;
    }
    return 44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *helpList = self.helpContents[_selectedIndex];
    return helpList.count + 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        GeneralAskHeaderTableViewCell *cell = [GeneralAskHeaderTableViewCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.rechargeView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRechargeHelpClick)]];
        [cell.withDrawView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onWithdrawHelpClick)]];
        if (_selectedIndex == 0) {
            cell.withDrawView.borderWidth = 1;
            cell.rechargeView.borderWidth = 0;
        }
        else {
            cell.withDrawView.borderWidth = 0;
            cell.rechargeView.borderWidth = 1;
        }
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"CellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        NSArray *helpList = self.helpContents[_selectedIndex];
        NSDictionary *dict = helpList[indexPath.row - 1];
        cell.textLabel.text = dict[@"cellTitle"];
        cell.textLabel.font = FONT(14);
        
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_selectedIndex == 0 && indexPath.row == 2) {
        WithdrawRecordListViewController *vc = [[WithdrawRecordListViewController alloc] init];
        vc.title = @"余额提现记录";
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        NSArray *helpList = self.helpContents[_selectedIndex];
        GeneralAskDetailViewController *vc = [[GeneralAskDetailViewController alloc] init];
        vc.helpContent = helpList[indexPath.row -1];
        vc.title = vc.helpContent[@"title"];
        [self.navigationController pushViewController:vc animated:YES];
    }

    
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
    }
}


@end
