//
//  PublishSuccessViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/6.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "PublishSuccessViewController.h"

@interface PublishSuccessViewController ()

@end

@implementation PublishSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.backButton greenStyle];
    [self.backButton addTarget:self action:@selector(onClickBackButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;//隐藏为YES，显示为NO
}

- (void)onClickBackButton:(id)sender
{
    [self dismissToRootViewController:YES completion:nil];
    
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
