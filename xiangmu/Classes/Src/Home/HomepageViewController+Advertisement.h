//
//  HomepageViewController+Advertisement.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HomepageViewController.h"
#import "SIDADView.h"

static char const * const kAdView = "kAdView";
static char const * const kAdvList = "kAdvList";
//static char const * const kGuideView = "kGuideView";
//static char const * const kClickIndex = "kClickIndex";

@interface HomepageViewController (Advertisement)<AdvDelegate>

@property (nonatomic, strong) SIDADView *adView;// 广告
@property (nonatomic, strong) NSArray *advList; // 广告链接地址

- (void)showAdv:(BOOL)bAll;
- (void)onClickAdvIcon; // 点击广告图标

@end
