//
//  BusinessCircleTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Enquire.h"
//#import "BusinessCircleModel.h"
#import "CommentView.h"

typedef enum : NSUInteger {
    CommunityBusinessCirlce,
    MyBusinessCircle
} BusinessCircleType;

#define kLineHeight 20      // 每行高度
#define kLineLeftInset 20
#define kLineRightInset 10
#define kTextLabelLeft  106


@interface BusinessCircleTableViewCell : UITableViewCell

@property (nonatomic, assign) NSInteger type;// 类型

@property (nonatomic, strong) UIImageView *avatarImageView; // 头像
@property (nonatomic, strong) UILabel *nicknameLabel; // 昵称
@property (nonatomic, strong) UIView *userInfoView; // 用户信息

@property (nonatomic, strong) UIView *startTimeView;
@property (nonatomic, strong) UILabel *startDateTimeLabel;

@property (nonatomic, strong) UIView *endTimeView;
@property (nonatomic, strong) UILabel *endDateTimeLabel;

@property (nonatomic, strong) UIView *startLocationView;
@property (nonatomic, strong) UILabel *startLocationLabel;

@property (nonatomic, strong) UIView *endLocationView;
@property (nonatomic, strong) UILabel *endLocationLabel;

@property (nonatomic, strong) UIView *vechileModelView;
@property (nonatomic, strong) UILabel *vechileModelLabel;

@property (nonatomic, strong) UIView *vechileNumberView;
@property (nonatomic, strong) UILabel *vechileNumberLabel;

// 大致行程
@property (nonatomic, strong) UIView *roughItineraryView;
@property (nonatomic, strong) UILabel *roughItineraryLabel;

// 是否需要第三方垫付
@property (nonatomic, strong) UILabel *thirdPayLabel;

// 询价发布时间
@property (nonatomic, strong) UILabel *elapseTimeLabel;

@property (nonatomic, strong) UIButton *fullTextButton;

// 竞价按钮
@property (nonatomic, strong) UIButton *publishPriceButton;

// 竞价状态按钮 询价中／已结束
@property (nonatomic, strong) UIButton *statusButton;

// count down View
@property (nonatomic, strong) UIView *countDownView;
@property (nonatomic, strong) UILabel *hourElapseTimeLabel;
@property (nonatomic, strong) UILabel *minuteElapseTimeLabel;
@property (nonatomic, strong) UILabel *secondElapseTimeLabel;

- (void)initData:(EnquireModel*)data;

@property (nonatomic, strong) EnquireModel *enquireModel;

@property (nonatomic, assign) BOOL isOpen;// 状态 0: 关闭；1: 展开

+ (CGFloat)getCellHeight:(EnquireModel*)data;

- (void)adjustBaseInfoPosition;

- (void)adjustCollapsePosition;

//- (void)setSubviewsFrame:(EnquireModel*)enquireModel;



@end
