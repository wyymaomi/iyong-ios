//
//  EnumConstants.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef EnumConstants_h
#define EnumConstants_h

typedef NS_ENUM(NSInteger, SOURCE_FROM) {
    SOURCE_FROM_LOGIN,
    SOURCE_FROM_ABOUT_ME,
    SOURCE_FROM_OTHER,
    SOURCE_FROM_NEW_ORDER, // 订单新建界面
    SOURCE_FROM_ORDER_MANAGER, // 订单管理界面
    SOURCE_FROM_ORDER_LIST,// 订单列表
    SOURCE_FROM_SALES_QUERY, // 营业查询
    SOURCE_FROM_TAB_CONTROL,
    SOURCE_FROM_HOME
};

typedef NS_ENUM(NSInteger, OrderEditMode){
    OrderNew = 0,
    OrderEdit
};

typedef NS_ENUM(NSInteger, OrderMode) {
    OrderModeManager = 0,   // 订单管理
    OrderModeDetail = 1     // 订单详情
};

typedef NS_ENUM(NSInteger, OrderTextFieldTag) {
    OrderTextFieldCarTime = 10000,
    OrderTextFieldStartLocation,
    OrderTextFieldCustomerName,
    OrderTextFieldCustomerMobile,
    OrderTextFieldItinerary,
    OrderTextFieldRemark,
    OrderTextFieldDispatchPrice,
    OrderTextFieldDispatchObject,
    OrderTextFieldArriveLocation,
    OrderTextFieldEndtime,
    OrderTextFieldDistance,
    OrderTextFieldRoadRemark,
    OrderTextFieldSettlementPrice,
};

typedef NS_ENUM(NSInteger, VechileTextFieldFlag) {
    VechileTextFieldModel = 20000,
    VechileTextFieldNumber,
    VechileTextFieldName,
    VechileTextFieldMobile
};

/**
 @enum      OrderSection)
 @abstract  订单详情列表界面section
 */
typedef NS_ENUM(NSInteger, OrderSection) {
    OrderSectionNew = 0,//新建订单
    OrderSectionDispatch,//派单信息
    OrderSectionVechileArrange,// 车辆安排
    OrderSectionWayBill,// 路单信息
    OrderSectionPay// 结算信息
};

/**
 @enum      OrderStatus
 @abstract  订单流程状态
 */
typedef NS_ENUM(NSInteger, OrderStatus) {
    OrderStatusNew = 1,             // 新建订单
    OrderStatusDispatch = 2,        // 派单信息
    OrderStatusVechileArrange = 3,  // 车辆安排
    OrderStatusWayBill = 4,         // 路单信息
    OrderStatusBarginAndPay = 5,    // 结算信息
    OrderStatusFinish = 6,          // 已结束
    OrderStatusTravelling = 11      // 行程中
};

/**
 @enum      OrderInfoType
 @abstract  基础信息
 */
typedef NS_ENUM(NSInteger, OrderInfoType) {
    OrderInfoTypeNew = 1,  // 新建
    OrderInfoTypeBrowsableAndEditable,// 浏览，可编辑
    OrderInfoTypeBrowsableAndNoEditable// 浏览，不可编辑
};

/**
 @enum      OrderDispatchType
 @abstract  派单信息
 */
typedef NS_ENUM(NSInteger, OrderDispatchType) {
    OrderDispatchTypeFirst = 1, // 首次派单
    OrderDispatchTypeBrowsableAndChangable, // 浏览，可改派
    OrderDispatchTypeBrowsableAndNoChangable // 浏览，不可改派
};

/**
 @enum      OrderVechilearrangeType
 @abstract  车辆安排
 */
typedef NS_ENUM(NSInteger, OrderVechilearrangeType) {
    OrderVechilearrangeTypeFirst = 1,// 首次车辆安排
    OrderVechilearrangeTypeBrowsableAndChangable,// 浏览，可变更
    OrderVechilearrangeTypeBrowsableAndNoChangable// 浏览，不可变更
};

/**
 @enum      OrderWaybillType
 @abstract  路单信息
 */
typedef NS_ENUM(NSInteger, OrderWaybillType) {
    OrderWaybillTypeFirst = 1, // 首次填写
    OrderWaybillTypeBrowsableAndEditable, // 浏览，可编辑
    OrderWaybillTypeBrowsableAndNoEditable // 浏览，不可编辑
};

/**
 @enum      OrderSettlementType
 @abstract  结算信息
 */
typedef NS_ENUM(NSInteger, OrderSettlementType) {
    OrderSettlementTypeFirst = 1, // 首次填写
    OrderSettlementTypeBrowsableAndChangable, // 浏览，可变更
    OrderSettlementTypeBrowsableAndNoChangable, // 浏览，不可变更
};

/**
 @enum      OrderPayType
 @abstract  结算信息
 */
typedef NS_ENUM(NSInteger, OrderPayType) {
    OrderPayTypeFirst = 1, // 首次支付确认 同意支付
    OrderPayTypeBrwosable, // 浏览
    OrderPayTypeFirstPay, // 首次付款 付款
    OrderPayTypeHavePay // 已付款
};




#endif /* EnumConstants_h */
