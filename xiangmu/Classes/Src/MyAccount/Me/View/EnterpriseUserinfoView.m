//
//  EnterpriseUserinfoView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/16.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "EnterpriseUserinfoView.h"

@implementation EnterpriseUserinfoView


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.companyPublishButton.frame = CGRectMake(39*Scale, 20*Scale, 75*Scale, 55*Scale);
    
    self.companyPublishLabel.frame = CGRectMake(39*Scale, self.companyPublishButton.bottom+8*Scale, 75*Scale, 16*Scale);
    
    self.companyPublishLabel.textAlignment = UITextAlignmentCenter;
    
    
    // 商家信息
    self.providerInfoButton.frame = CGRectMake(self.companyPublishButton.right+36*Scale, 20*Scale, 75*Scale, 55*Scale);
    
    self.providerInfoLabel.frame = CGRectMake(self.providerInfoButton.left, self.providerInfoButton.bottom+8*Scale, self.providerInfoButton.width, 16*Scale);
    
    self.providerInfoLabel.textAlignment = UITextAlignmentCenter;
    
    
    // 商家推广
    self.providerPromotionButton.frame = CGRectMake(self.providerInfoButton.right+36*Scale, 20*Scale, 75*Scale, 55*Scale);
    
    self.providerPromotionLabel.frame = CGRectMake(self.providerPromotionButton.left, self.providerPromotionButton.bottom+8*Scale, self.providerPromotionButton.width, 16*Scale);
    
    self.providerPromotionLabel.textAlignment = UITextAlignmentCenter;
    
    
    
    
    self.waitButton.frame = CGRectMake(self.companyPublishButton.left, self.companyPublishLabel.bottom+20*Scale, 75*Scale, 55*Scale);
    
    self.waitLabel.frame = CGRectMake(self.waitButton.left, self.waitButton.bottom+8*Scale, self.waitButton.width, 16*Scale);
    
    self.waitLabel.textAlignment = UITextAlignmentCenter;
    
    self.waitLabel.enabled = NO;
    
    self.waitButton.enabled = NO;
    
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

@end
