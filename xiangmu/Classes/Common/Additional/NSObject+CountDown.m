//
//  NSObject+CountDown.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "NSObject+CountDown.h"

@implementation NSObject (CountDown)

- (void)countDownTime:(NSUInteger)time countDownBlock:(CountDownBlock)countDownBlock outTimeBlock:(CountdownFinishBlock)finishBlock;
{
    
    __block NSUInteger second = time;
    
    self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(0, 0));
    
    WeakSelf
    
    dispatch_source_set_timer(self.timer, DISPATCH_TIME_NOW, 1.0f * NSEC_PER_SEC, 1.0f * NSEC_PER_SEC);
    dispatch_source_set_event_handler(self.timer, ^{
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (countDownBlock != nil) {
                countDownBlock(second);
            }
        });
        
        if ((second--) == 0) {
            dispatch_cancel(weakSelf.timer);
            weakSelf.timer = nil;
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (finishBlock != nil) {
                    finishBlock();
                }
            });
        }
    });
    
    dispatch_resume(self.timer);
}

- (void)cancelTimer
{
    if (self.timer) {
        dispatch_cancel(self.timer);
        self.timer = nil;
    }
}

- (void)setTimer:(dispatch_source_t)timer{
    
    objc_setAssociatedObject(self, @selector(timer), timer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (dispatch_source_t)timer{
    return objc_getAssociatedObject(self, @selector(timer));
}


@end
