//
//  NewsListViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewsListViewController+NetworkRequest.h"

@implementation NewsListViewController (NetworkRequest)

#pragma mark - 获取资讯列表

-(void)fetchNewsListByCreateTime:(NSString*)createTime
{
    BOOL isHeaderRefresh = IsStrEmpty(createTime);
    
    if (isHeaderRefresh) {
        [self.dataList removeAllObjects];
    }
    
//    NSString *params = IsStrEmpty(createTime) ? @"": [NSString stringWithFormat:@"time=%@", createTime];
    
    NSDictionary *params = IsStrEmpty(createTime) ? @{@"time":@""} : @{@"time": createTime};
    
    self.loadStatus = LoadStatusLoading;
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startHttpPost:APIWithBaseUrl(NEWS_LIST_API) params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        //        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        if (code_status == STATUS_OK) {
            
            NSError *error;
            
            NewsResponseData *responseData = [[NewsResponseData alloc] initWithDictionary:result error:&error];
            
            if (error) {
                return;
            }
            
            [strongSelf.dataList addObjectsFromArray:responseData.data.list];
            
                    strongSelf.loadStatus = LoadStatusFinish;
                    
                    [strongSelf.pullRefreshTableView reloadData];
                    
                    strongSelf.pullRefreshTableView.footerHidden = NO;
//                });
                
//            });
            
        }
        else if (code_status == NETWORK_FAILED) {
//            [strongSelf resetAction];
            strongSelf.loadStatus = LoadStatusNetworkError;
        }
        else {
//            [strongSelf resetAction];
            strongSelf.loadStatus = LoadStatusFinish;
            //            NSError *error;
            //            OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
            //            if (error) {
            //                return;
            //            }
            //            [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
        }
        
        
        
        
    }];
    
}

-(void)fetchNewsListByPageIndex:(NSInteger)pageIndex newsId:(NSString*)newsId
{
    if (pageIndex < 1) {
        return;
    }
    
    NSString *id = IsStrEmpty(newsId)?@"":newsId;
//    if(IsStrEmpty(newsId)) {
//        return;
//    }
    
    NSDictionary *params = @{@"pageIndex" : [NSString stringWithFormat:@"%ld", (long)pageIndex],
                             @"id": id};
    
    if (pageIndex == 1) {
        [self.dataList removeAllObjects];
    }
    
    self.loadStatus = LoadStatusLoading;
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startHttpPost:APIWithBaseUrl(NEWS_LIST_API) params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        //        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        if (code_status == STATUS_OK) {
            
            NSError *error;
            
            NewsResponseData *responseData = [[NewsResponseData alloc] initWithDictionary:result error:&error];
            
            if (error) {
                return;
            }
            
            if (responseData.data.list.count > 0) {
                
                self.commonPage.pageIndex++;
                
                [strongSelf.dataList addObjectsFromArray:responseData.data.list];
            }
            
            
            
            strongSelf.loadStatus = LoadStatusFinish;
            
            [strongSelf.pullRefreshTableView reloadData];
            
            strongSelf.pullRefreshTableView.footerHidden = NO;
            //                });
            
            //            });
            
        }
        else if (code_status == NETWORK_FAILED) {
            //            [strongSelf resetAction];
            strongSelf.loadStatus = LoadStatusNetworkError;
        }
        else {
            //            [strongSelf resetAction];
            strongSelf.loadStatus = LoadStatusFinish;
            //            NSError *error;
            //            OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
            //            if (error) {
            //                return;
            //            }
            //            [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
        }
        
        
        
        
    }];
    
    
}

#pragma mark - 按页码分页

- (void)fetchExperienceListByPageIndex:(NSInteger)pageIndex
{
    
    if (pageIndex < 1) {
        return;
    }
    
    NSString *url;
    url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_LIST_API];
    
    
    NSDictionary *params = @{@"pageIndex": [NSString stringWithFormat:@"%ld", (long)pageIndex]};
    //                             @"areaCode" : @""};
    //                             @"areaCode":[AppConfig currentConfig].gpsCityCode};
    
    if (pageIndex == 1) {
        [self.dataList removeAllObjects];
    }
    
    //    if (isHeaderRefresh) {
    //        self.pullRefreshTableView.footerHidden = YES;
    //        [self.dataList removeAllObjects];
    //    }
    
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        //        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        if (code_status == STATUS_OK) {
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                
//                UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
//                if (error) {
//                    return;
//                }
//                
//                NSArray<UserExpericeModel> *list = responseData.data.list;
//                if (list.count > 0) {
//                    strongSelf.commonPage.pageIndex++;
//                }
//                for (NSInteger i = 0; i < list.count; i++) {
//                    UserExpericeModel *model = list[i];
//                    UserExpericenViewModel *viewModel = [[UserExpericenViewModel alloc] init];
//                    viewModel.serviceModel = model;
//                    [strongSelf.dataList addObject:viewModel];
//                }
//                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    strongSelf.loadStatus = LoadStatusFinish;
//                    
//                    [strongSelf.pullRefreshTableView reloadData];
//                    
//                    strongSelf.pullRefreshTableView.footerHidden = NO;
//                });
                
            });
            
        }
        else if (code_status == NETWORK_FAILED) {
            strongSelf.loadStatus = LoadStatusNetworkError;
        }
        else {
            strongSelf.loadStatus = LoadStatusFinish;
            //            NSError *error;
            //            OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
            //            if (error) {
            //                return;
            //            }
            //            [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
        }
        
    }];
}

@end
