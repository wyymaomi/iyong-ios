//
//  CarTravelListViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "VechileOrderManager.h"


@interface CarTravelListViewController : BaseViewController

@property (nonatomic, assign) NSUInteger listStatus;

@property (nonatomic, assign) NSUInteger source_from;// 0: 我的账户；1:支付成功

@end
