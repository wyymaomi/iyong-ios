//
//  DiaoduyuanCollectionViewCell.m
//  xiangmu
//
//  Created by David kim on 16/3/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DiaoduyuanCollectionViewCell.h"

@implementation DiaoduyuanCollectionViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *leftImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-15, 18, 23/3, 42/3)];
        leftImageView.image = [UIImage imageNamed:@"icon_disclosure"];
        [self addSubview:leftImageView];
        
        UIImageView *topImageView=[[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 15, 15)];
        topImageView.image=[UIImage imageNamed:@"icon_dispatcher"];
        [self addSubview:topImageView];
        
        UIImageView *bottomImageView1=[[UIImageView alloc]initWithFrame:CGRectMake(20, 32, 15, 15)];
        bottomImageView1.image=[UIImage imageNamed:@"icon_contact_dial"];
        [self addSubview:bottomImageView1];
        
        
        _name=[[UILabel alloc]initWithFrame:CGRectMake(45, 5, 100, 20)];
        _name.font=[UIFont systemFontOfSize:14];
        // _name.text=@"小王";
        _name.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        [self addSubview:_name];
        
        _phoneLabel=[[UILabel alloc]initWithFrame:CGRectMake(45, 30, 100, 20)];
        _phoneLabel.font=[UIFont systemFontOfSize:14];
        // _phoneLabel.text=@"13835555551";
        _phoneLabel.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        [self addSubview:_phoneLabel];
        
        
    }
    return self;
}

@end
