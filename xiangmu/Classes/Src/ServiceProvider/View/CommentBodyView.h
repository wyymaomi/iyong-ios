//
//  CommentBodyView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserExpericenViewModel.h"
#import "UserExperienceTableViewCell.h"
#import "StarView.h"
#import "PYPhotosView.h"

@interface UserExperienceToolBar : UIView



@end

@interface CommentBodyView : UIView

@property (nonatomic, strong) UserExpericenViewModel *viewModel;

//@property (nonatomic, strong) UIView *separatorLineView; // 公司基本信息
//@property (nonatomic, strong) UIView *businessView;// 经营范围

//@property (nonatomic, strong) StarView *starView;

@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UIImageView *browseIcon;
@property (nonatomic, strong) UIImageView *commentIcon;
@property (nonatomic, strong) UIButton *likeButton;

@property (nonatomic, strong) UILabel *browseNumberLabel;
@property (nonatomic, strong) UILabel *commentNumberLabel;
@property (nonatomic, strong) UILabel *likeNumberLabel;

//@property (nonatomic, strong) UIImageView *iconView;
//@property (nonatomic, strong) UILabel *nameLabel;
//@property (nonatomic, strong) UILabel *contentLabel;
//@property (nonatomic, strong) UILabel *companyLabel;

//@property (nonatomic, strong) PYPhotosView *photosView;
//@property (nonatomic, strong) UIView *photosView;
//@property (nonatomic, strong) UIImageView *photoImageView1;
//@property (nonatomic, strong) UIImageView *photoImageView2;
//@property (nonatomic, strong) UIImageView *photoImageView3;
//@property (nonatomic, strong) UIImageView *photoImageView4;

@property (nonatomic, weak) UserExperienceTableViewCell *cell;

@end
