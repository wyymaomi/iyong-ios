//
//  MyInvoiceViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/28.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MyInvoiceViewController.h"
#import "MyInvoiceTableView.h"
#import "InvoiceManager.h"

@interface MyInvoiceViewController ()<MyInvoiceTableViewDelegate>

@property (nonatomic, strong) MyInvoiceTableView *invoiceTableView;

@end

@implementation MyInvoiceViewController

-(MyInvoiceTableView*)invoiceTableView
{
    if (!_invoiceTableView) {
        _invoiceTableView = [[MyInvoiceTableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        _invoiceTableView.customDelegate = self;
    }
    return _invoiceTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的发票";
    [self.view addSubview:self.invoiceTableView];
    
    [self setRightNavigationItemWithTitle:@"开票说明" selMethod:@selector(openInvoiceExplainWebpage)];
    
    [InvoiceManager sharedInstance].myInvoiceController = self;
    
}
-(void)dealloc
{
    self.invoiceTableView.customDelegate = nil;
    [InvoiceManager sharedInstance].myInvoiceController = nil;
}

//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([self.invoiceTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.invoiceTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.invoiceTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.invoiceTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    switch (index) {
        case MyInvoiceRowDrawInvoice:
            [InvoiceManager sharedInstance].invoiceinfoHttpMessage = nil;
            [self gotoPage:@"AddNewInvoiceViewController"];
            break;
        case MyInvoiceRowInvoiceHistory:
            [self gotoPage:@"InvoiceHistoryListViewController"];
            break;
        case MyInvoiceRowInvoiceSetting:
            [self gotoPage:@"InvoiceSettingListViewController"];
            break;
        default:
            break;
    }
}

-(void)openInvoiceExplainWebpage
{
    NSString *url = [NSString stringWithFormat:@"%@%@", RESOURSE_WEB_PAGE, INVOICE_EXPLAIN_HTML];
    [self openWebView:url];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
