//
//  LaunchImageViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/9.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^LaunchImageCompleteBlock)(void);

typedef void(^LaunchInitBlock)(void);

@interface LaunchImageViewController : UIViewController

@property (nonatomic, strong) LaunchInitBlock initBlock;

@property (nonatomic, strong) LaunchImageCompleteBlock completeBlock;

@end
