//
//  NetworkReach.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface NetworkReach : NSObject

@property (nonatomic, readonly) BOOL isNetReachable;
@property (nonatomic, readonly) BOOL isHostReach;
@property (nonatomic, readonly) NSInteger reachableCount;
@property (nonatomic, strong) Reachability *hostReach;
@property (nonatomic, strong) Reachability *routerReachability;
@property (nonatomic, assign) NetworkStatus lastNetworkStatus;

- (void)initNetwork;
//- (void)showNetworkAlertMessage;

@end
