//
//  SalesModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"


@interface SalesSummaryModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *count;
@property (nonatomic, strong) NSString<Optional> *totalPrice;

@end

@protocol SalesModel <NSObject>
@end


@interface SalesModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *price;
@property (nonatomic, strong) NSString<Optional> *carTime;
@property (nonatomic, strong) NSString<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *customerMobile;
@property (nonatomic, strong) NSString<Optional> *customerName;
@property (nonatomic, strong) NSString<Optional> *downAddress;
@property (nonatomic, strong) NSString<Optional> *upAddress;
@property (nonatomic, strong) NSString<Optional> *orderId;
@property (nonatomic, strong) NSString<Optional> *upCompanyId;
@property (nonatomic, strong) NSString<Optional> *upCompanyName;
@property (nonatomic, strong) NSString<Optional> *upCompanyLogoUrl;

@end


@interface SalesResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<SalesModel, Optional> *data;

@end
