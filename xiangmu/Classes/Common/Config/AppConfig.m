//
//  Config.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/11.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AppConfig.h"
#import "UserDefaults.h"


@implementation AppConfig

@dynamic carLastUpdateStr;
@dynamic partnerLastUpdateStr;
@dynamic databaseVersion;
@dynamic isFirstLocateCity;
@dynamic locationCity;
@dynamic businessCircleNumber;
@dynamic IsEnquireShowIntro;
@dynamic advLastUpdateStr;
@dynamic defaultCityName;
@dynamic defaultCityCode;

-(id) init {
    
    if(!(self = [super init]))
        return self;
    
    self.defaults = [NSUserDefaults standardUserDefaults];
    
    [self.defaults registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:
                                    @"",        dCarLastUpdateStr,
                                    @"",        dPartnerLastUpdateStr,
                                    @"",        dDatabaseVersion,
                                    @0,         dIsFirstLocateCity,
                                    @"",        dLocationCity,
                                    @0,         dBusinessCircleNumber,
                                    @NO,        dIsEquireShowIntro,
                                    @"",        dAdvLastUpdateStr,
                                    @"",        dDefaultCityCode,
                                    @"",        dDefaultCityName,
                                    @0,         dServiceType,
                                    @"",        dDeviceId,
                                    @"",        dGpsCityName,
                                    @"",        dGpsCityCode,
                                    @YES,       dSoundOn,
                                     nil]];
    
    return self;

}

+(AppConfig *) currentConfig {
    
    static AppConfig *instance;
    
    if(!instance)
        
        instance = [[AppConfig alloc] init];
    
    return instance;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    
    if ([NSStringFromSelector(aSelector) hasPrefix:@"set"]){
        
        return [NSMethodSignature signatureWithObjCTypes:"v@:@"];
    }
    //DLog(@"methodSignatureForSelector 2\n");
    return [NSMethodSignature signatureWithObjCTypes:"@@:"];
}


- (void)forwardInvocation:(NSInvocation *)anInvocation {
    
    NSString *selector = NSStringFromSelector(anInvocation.selector);
    if ([selector hasPrefix:@"set"]) {
        NSRange firstChar, rest;
        firstChar.location  = 3;
        firstChar.length    = 1;
        rest.location       = 4;
        rest.length         = selector.length - 5;
        
        selector = [NSString stringWithFormat:@"%@%@",
                    [[selector substringWithRange:firstChar] lowercaseString],
                    [selector substringWithRange:rest]];
        
        __unsafe_unretained id value;
        [anInvocation getArgument:&value atIndex:2];
        
        //DLog(@"forwardInvocation 1\n");
        
        if ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSDictionary class]])
        {
            [self.defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:value] forKey:selector];
        }
        else
        {
            [self.defaults setObject:value forKey:selector];
        }
        
    }
    
    else {
        //DLog(@"forwardInvocation 2\n");
        id value = [self.defaults objectForKey:selector];
        
        if ([value isKindOfClass:[NSData class]])
        {
            void *cfValue = (__bridge void *)[NSKeyedUnarchiver unarchiveObjectWithData:value];
            [anInvocation setReturnValue:&cfValue];
        }
        else
        {
            [anInvocation setReturnValue:&value];
        }
        
    }
}



@end
