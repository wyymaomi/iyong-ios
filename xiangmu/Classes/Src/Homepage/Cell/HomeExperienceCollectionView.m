//
//  HomeExperienceCollectionView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomeExperienceCollectionView.h"
#import "HomeExperienceCollectionViewCell.h"
#import "UserExpericeModel.h"

@implementation HomeExperienceCollectionView

-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    
    if (self) {
        
        [self initViews];
        
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self initViews];
        
    }
    
    return self;
}

- (void)initViews
{
    
    [self registerNib:[UINib nibWithNibName:@"HomeExperienceCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"HomeExperienceCollectionViewCell"];
    //    [self registerClass:[HomepageCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    self.delegate=self;
    self.dataSource=self;
    self.scrollEnabled=NO;
}

#pragma mark - UICollectionView Delegate method

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //    return self.subviewCellList.count;
        return self.dataList.count;
    
//    return 10;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    HomeExperienceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeExperienceCollectionViewCell" forIndexPath:indexPath];
    
    UserExpericeModel *data = self.dataList[indexPath.row];
    
    NSString *imgName = data.imgs[0];
    
//    [cell.imageView downloadImageFromAliyunOSS:imgName isThumbnail:YES placeholderImage:[UIImage imageNamed:@"img_placeholder"] success:^(id responseData) {
    
        
//    } andFailure:^(NSString *errorDesc) {
//        
//        
//    }];
    
    [cell.imageView yy_setImageWithObjectKey:imgName placeholder:DefaultPlaceholderImage imageSize:CGSizeMake(400, 400) manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
        
    }];
    
    cell.commentLabel.text = data.content;
    
    cell.viewnumLabel.text = data.browseQuantity;
    
    cell.commentNumLabel.text = data.commentQuantity;
    
    
    return cell;
}

////cell点击变色
//-(BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
///cell点击变色
//-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_customDelegate && [_customDelegate respondsToSelector:@selector(onClickExperienceItem:)]) {
        [_customDelegate onClickExperienceItem:indexPath.row];
    }
    //    NSDictionary *cellDict = self.subviewCellList[indexPath.row];
    //    NSString *viewController = cellDict[@"vc"];
    //
    //    if ([viewController isEqualToString:@""]) {
    //        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该系统暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    //        [alertView show];
    //    }
    //    else {
    //        if (_customDelegate && [_customDelegate respondsToSelector:@selector(onGotoPage:title:)]) {
    //            [_customDelegate onGotoPage:viewController title:cellDict[@"text"]];
    //        }
    //    }
    
}


@end
