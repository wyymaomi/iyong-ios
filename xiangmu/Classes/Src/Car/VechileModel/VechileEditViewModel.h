//
//  VechileEditViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewModel.h"
#import "CarModel.h"

@interface VechileEditViewModel : BaseViewModel

@property (nonatomic, strong) CarModel *vechileModel;

- (void)setVechileCertificationStatus:(NSString**)statusText btnBgColor:(UIColor**)btnBgColor;

- (BOOL)isVechileCertCanEdit;


@end
