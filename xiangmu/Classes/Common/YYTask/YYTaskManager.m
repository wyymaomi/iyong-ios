//
//  YYTaskManager.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/16.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "YYTaskManager.h"

@interface YYTaskManager ()

@property (strong) NSMutableSet *taskSets;

@end

@implementation YYTaskManager

+ (instancetype)sharedInstance
{
    static YYTaskManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[YYTaskManager alloc] init];
    });
    return instance;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _taskSets = [[NSMutableSet alloc] init];
        _taskOperationQueue = [[NSOperationQueue alloc] init];
    }
    return self;
}

- (void)addTask:(YYTask *)task
{
    @synchronized (self) {
        NSParameterAssert([task isKindOfClass:[YYTask class]]);
        
        if (![self.taskSets containsObject:task]) {
            [self.taskSets addObject:task];
        }
    }
}

- (void)removeTask:(YYTask *)task
{
    @synchronized (self) {
        NSParameterAssert([task isKindOfClass:[YYTask class]]);
        
        if ([self.taskSets containsObject:task]) {
            [self.taskSets removeObject:task];
        }
    }
}

- (YYTask*)containTask:(Class)taskName observer:(id)observer
{
    @synchronized (self) {
        
        __block YYTask *task = nil;
        
        [self.taskSets enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
            task = obj;
            *stop = YES;
        }];
        
        return task;
    }
}

- (NSSet*)tasksForObserver:(id)observer
{
    @synchronized (self) {
        
        NSMutableSet *taskSet = [[NSMutableSet alloc] init];
        
        [self.taskSets enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
           
            if ([obj delegate] == observer) {
                [taskSet addObject:obj];
            }
            
        }];
        
        return [taskSet count] ? taskSet : nil;
    }
}

@end
