//
//  CommunityCarTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HyperlinksButton.h"

@interface CommunityCarTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *carImageView;

@property (weak, nonatomic) IBOutlet UIView *driverInfoVIew;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *carNumberLabel;
@property (weak, nonatomic) IBOutlet HyperlinksButton *driverMobileButton;

@property (weak, nonatomic) IBOutlet UIView *vechileSelectView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

@end
