//
//  MyTableViewCell.h
//  xiangmu
//
//  Created by David kim on 16/3/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *disclosureImageView;
@property (nonatomic,strong)UILabel *name;
@property (nonatomic,strong)UILabel *detailLabel;
@property (nonatomic, strong) UIButton *verifyButton;

@end
