//
//  VechileArrangeTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "VechileArrangeTableViewCell.h"

@implementation VechileArrangeTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"CellIdentifier";
    VechileArrangeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0xDEDEDE);
        cell.driverMobileButton.lineColor = UIColorFromRGB(0x00A04E);
    }
    return cell;
}


+(CGFloat)getHeight:(VechileArrangeResponseModel*)model
{
    if (IsNilOrNull(model)) {
        return 0;
    }
    
    switch ([model.type integerValue]) {
        case OrderVechilearrangeTypeFirst:
            return 176;
            break;
        case OrderVechilearrangeTypeBrowsableAndChangable:
        case OrderVechilearrangeTypeBrowsableAndNoChangable:
            if ([model.confirm integerValue] == 1) {
                return 100;
            }
            break;
        default:
            break;
    }
    return 176;
}

-(void)initData:(VechileArrangeResponseModel*)model;
{
    if (IsNilOrNull(model)) {
        self.carSelView.hidden = NO;
        self.carDetailView.hidden = YES;
        return;
    }
    
    self.carSelView.hidden = YES;
    self.carDetailView.hidden = NO;
    
    self.carNumberLabel.text = model.number.trim;
    self.carNumberLbl.text = model.number.trim;
    self.driverNameLabel.text = model.driver.trim;
    [self.driverMobileButton setTitle:model.mobile forState:UIControlStateNormal];
//    self.driverMobileButton.titleLabel.text = model.mobile.trim;
//    self.driverMobileLabel.text = model.mobile.trim;
    

    
    if (IsNilOrNull(model.number) || IsStrEmpty(model.number) ||
        IsNilOrNull(model.driver) || IsStrEmpty(model.driver) ||
        IsNilOrNull(model.mobile) || IsStrEmpty(model.mobile)) {
        
        self.carSelView.hidden = NO;
        self.carDetailView.hidden = YES;
        
    }
    
    switch ([model.type integerValue]) {
        case OrderVechilearrangeTypeFirst:
            self.changeButton.hidden  = YES;
//            self.carIdView.frame = CGRectMake(0, 0, self.carIdView.frame.size.width, self.carIdView.frame.size.height);
//            self.carIdView.hidden = NO;
            self.vechileInfoView.frame = CGRectMake(0, 0, self.vechileInfoView.frame.size.width, self.vechileInfoView.frame.size.height);
            self.vechileInfoView.hidden = NO;
            self.driverContactView.frame = CGRectMake(0, 97, self.driverContactView.frame.size.width, self.driverContactView.frame.size.height);
            self.driverContactView.hidden = NO;
            break;
        case OrderVechilearrangeTypeBrowsableAndChangable:
            self.changeButton.hidden = NO;
            self.carIdView.hidden = YES;
            self.vechileInfoView.frame = CGRectMake(0, 0, self.vechileInfoView.frame.size.width, self.vechileInfoView.frame.size.height);
            self.driverContactView.hidden = ([model.confirm integerValue] == 1) ? YES : NO ;
            
            break;
        case OrderVechilearrangeTypeBrowsableAndNoChangable:
            self.changeButton.hidden = YES;
            self.carIdView.hidden = YES;
            self.vechileInfoView.hidden = NO;
            self.vechileInfoView.frame = CGRectMake(0, 0, self.vechileInfoView.frame.size.width, self.vechileInfoView.frame.size.height);
            self.driverContactView.hidden = ([model.confirm integerValue] == 1) ? YES : NO ;
            break;
        default:
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
