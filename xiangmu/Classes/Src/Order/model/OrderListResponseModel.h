//
//  OrderListResponseModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/4.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"


@protocol OrderListItem
@end

@interface OrderListResponseData : JSONModel

@property (nonatomic, assign) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<OrderListItem> *data;

@end

@interface OrderListItem : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *carTime;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSNumber<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *customerMobile;
@property (nonatomic, strong) NSString<Optional> *customerName;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *itinerary;
@property (nonatomic, strong) NSString<Optional> *location;
@property (nonatomic, strong) NSString<Optional> *price;
@property (nonatomic, strong) NSString<Optional> *remark;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSNumber<Optional> *valid;
@property (nonatomic, strong) NSString<Optional> *downAddress;
@property (nonatomic, strong) NSString<Optional> *upCompanyLogoUrl;
@property (nonatomic, strong) NSString<Optional> *upCompanyName;


@end
