//
//  MyTableViewCell.m
//  xiangmu
//
//  Created by David kim on 16/3/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyTableViewCell.h"
#import "MyUtil.h"

@implementation MyTableViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryNone;
//        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        self.disclosureImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-15, (self.frame.size.height-42/3)/2, 23/3, 42/3)];
//        self.disclosureImageView.image = [UIImage imageNamed:@"icon_disclosure"];
//        [self addSubview:self.disclosureImageView];
        
        self.name = [[UILabel alloc]initWithFrame:CGRectMake(12*Scale, 0, (ViewWidth-24*Scale), DefaultTableRowHeight)];
        self.name.textAlignment = UITextAlignmentLeft;
//        self.name.textColor=[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
//        self.name.font=[UIFont systemFontOfSize:14];
        self.name.textColor = DefaultTableRowTitleTextColor;
        self.name.font = DefaultTableRowTitleFont;
        self.name.textColor = UIColorFromRGB(0x242424);
        
        [self addSubview:self.name];
        
        self.detailLabel = [MyUtil createLabelFrame:CGRectMake(12*Scale, 0, ViewWidth-12*Scale-12*Scale, DefaultTableRowHeight) title:nil font:[UIFont systemFontOfSize:14] textAlignment:NSTextAlignmentRight numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
        self.detailLabel.font = DefaultTableRowSubtitleFont;
        self.detailLabel.textColor = DefaultTableRowSubtitleTextColor;
        self.detailLabel.frame = CGRectMake(12*Scale, 0, ViewWidth-12*Scale-12*Scale, DefaultTableRowHeight);
        [self addSubview:self.detailLabel];
//        self.detailLabel.backgroundColor = [UIColor yellowColor];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = 1000;
        button.hidden = YES;
        button.width = 45;
        NSInteger buttonWidth = 45;
        NSInteger buttonHeight = 20;
        NSInteger buttonLeft = self.detailLabel.right - buttonWidth;
        button.frame = CGRectMake(buttonLeft, (self.contentView.frame.size.height - buttonHeight)/2, buttonWidth, buttonHeight);
        [self.contentView addSubview:button];
        self.verifyButton = button;
    
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
//    self.name.frame = CGRectMake(20, (self.frame.size.height-20)/2, 100, 20);
    
//    self.name.frame = CGRectMake(12*Scale, (D), <#CGFloat width#>, <#CGFloat height#>)
    
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
