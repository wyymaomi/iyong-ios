//
//  ContactListService.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "ABRootViewController.h"
//#import "CNContactRootViewController.h"
#import <ContactsUI/ContactsUI.h>
#import <AddressBookUI/AddressBookUI.h>
//#import 

@protocol ContactListServiceDelegate <NSObject>

- (void)onGetMobile:(NSString*)mobile;

@end


@interface ContactListService : NSObject

@property (nonatomic, assign) id<ContactListServiceDelegate> delegate;

- (id) initWithViewController:(UIViewController*)viewController;

- (void)showContactList;

@end

@interface CNContactRootViewController : CNContactPickerViewController<CNContactPickerDelegate,UINavigationControllerDelegate>

@property (nonatomic, weak) id<ContactListServiceDelegate> customDelegate;

@end


@interface ABRootViewController : ABPeoplePickerNavigationController <ABPeoplePickerNavigationControllerDelegate>

@property (nonatomic, assign) id<ContactListServiceDelegate> customDelegate;

@end


