//
//  FFScrollView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FFScrollViewDelegate <NSObject>

@optional
- (void)scrollViewDidClickedAtPage:(NSInteger)pageNumber;

@end

@interface FFScrollView : UIView
//{
//    NSTimer *timer;
//    NSArray *sourceArr;
//    
//    NSString * tagStr;
//    
//    UILabel * lable1;
//    UILabel * lable2;
//    UILabel * lable3;
//}

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *sourceArr;

@property(strong,nonatomic) UIScrollView *scrollView;
@property(strong,nonatomic) UIPageControl *pageControl;
//@property(assign,nonatomic) FFScrollViewSelecttionType selectionType;
@property(assign,nonatomic) id <FFScrollViewDelegate> pageViewDelegate;
- (id)initPageViewWithFrame:(CGRect)frame views:(NSArray *)views tag:(NSString *)tag;

@end
