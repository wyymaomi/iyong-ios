//
//  BillManageViewController.h
//  xiangmu
//
//  Created by David kim on 16/3/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"

//typedef NS_ENUM(NSInteger,BillType)
//{
//    BillTypeHome=10,
//    BillTypeFirst,
//};
typedef NS_ENUM(NSInteger,ZhangType)
{
    ZhangTypeAdd=10,
    ZhangTypeEdit,
};
@interface BillManageViewController : LFNavController

@property (nonatomic,assign)ZhangType type;
@end
