//
//  SIDADView.m
//  SIDAdView
//
//  Created by XU JUNJIE on 13/7/15.
//  Copyright (c) 2015 ISNC. All rights reserved.
//

#import "SIDADView.h"
#import "AdvModel.h"
#import "SDWebImageDownloader.h"
#import "UIImageView+WebCache.h"
#import "AliyunOSSManager.h"

#define superid_ad_color_title          HEXRGB(0x0099cc)
#define superid_ad_color_tips           HEXRGB(0x333333)



//RGB Color transform（16 bit->10 bit）
#define HEXRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//Screen Height and width
#define Screen_height   [[UIScreen mainScreen] bounds].size.height
#define Screen_width    [[UIScreen mainScreen] bounds].size.width
#define VIEW_BX(view) (view.frame.origin.x + view.frame.size.width)
#define VIEW_BY(view) (view.frame.origin.y + view.frame.size.height)
//Get View size:
#define VIEW_W(view)  (view.frame.size.width)
#define VIEW_H(view)  (view.frame.size.height)

#define SCROLL_VIEW_WIDTH 0.825 * ViewWidth

//iPhone4
#define   isIphone4  [UIScreen mainScreen].bounds.size.height < 500

@interface SIDADView()<UIScrollViewDelegate>

@property (retain, nonatomic) IBOutletCollection(NSObject) NSArray* superid_ad_closeTargets;
@property (retain,nonatomic) UIButton        *closeBtn;
@property (retain,nonatomic) UIButton       *otherBtn;
@property (retain,nonatomic) UIView          *bgView;
@property (retain,nonatomic) UILabel         *titleLable;
@property (retain,nonatomic) UILabel         *subTitleLable;
@property (retain,nonatomic) UIView          *titleBgView;
@property (retain,nonatomic) UIImageView     *adImageView;
@property (nonatomic, strong) NSArray *imageList;
@property (retain,nonatomic) NSDictionary    *characterDitionary;

@property (nonatomic, strong) UIWindow      *maskWindow;
@property (nonatomic, strong) UIView        *maskView;

// 创建页码控制器
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation SIDADView

- (UIPageControl*)pageControl
{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(ViewWidth / 3, ViewHeight * 15 / 16, ViewWidth / 3, ViewHeight / 16)];
        // 设置页数
//        _pageControl.numberOfPages = 3;
        // 设置页码的点的颜色
        _pageControl.pageIndicatorTintColor = [UIColor darkGrayColor];
        // 设置当前页码的点颜色
        _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];//UIColorFromRGB(0xF9BF3C);
        
//        [self.view addSubview:pageControl];
    }
    return _pageControl;
}


-(UIScrollView*)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCROLL_VIEW_WIDTH, 1.46*0.825*Screen_width)];
        _scrollView.bounces = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
//        _scrollView.contentSize = CGSizeMake(0.8*Screen_width * 3, 1.46*0.825*Screen_width);
        _scrollView.canCancelContentTouches = YES;
        _scrollView.delaysContentTouches = NO;
        
        //对srcollView添加点击响应
        UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
        sigleTapRecognizer.numberOfTapsRequired = 1;
        [_scrollView addGestureRecognizer:sigleTapRecognizer];
        _scrollView.delegate = self;
        
    }
    return _scrollView;
}

- (UIWindow *)maskWindow
{
    if (!_maskWindow) {
        
        _maskWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        //edited by gjf 修改alertview leavel
        _maskWindow.windowLevel = UIWindowLevelStatusBar + BBAlertLeavel;
        _maskWindow.backgroundColor = [UIColor clearColor];
        _maskWindow.hidden = YES;
        
        UIView *maskView = ({
            
            UIView *view = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
            view.backgroundColor = [UIColor blackColor];
            view.alpha = 0.7;
            view;
        });
        [_maskWindow addSubview:maskView];
        _maskView = maskView;
        
        
        
        _bgView = ({
            
            UIView *view =[[UIView alloc]initWithFrame: CGRectMake(0, 0, SCROLL_VIEW_WIDTH, 1.46*0.825*Screen_width)];
//            view.frame = CGRectMake(0, 0, 0.8*Screen_width, 1.46*0.825*Screen_width);
            view.center = CGPointMake(Screen_width/2, Screen_height/2);
//            if (isIphone4) {
//                self.center = CGPointMake(Screen_width/2, Screen_height/2+20);
//            }
            view.backgroundColor = [UIColor whiteColor];
            view.layer.cornerRadius = 6.0;
            view.clipsToBounds = YES;
            view.layer.borderWidth=1;
            view.layer.borderColor=superid_ad_color_title.CGColor;
            view;
            
        });
        
        [_maskWindow addSubview:_bgView];
        

//        _bgView.delegate = self;
        
//        [_bgView addSubview:self.scrollView];


        
//        _closeBtn = ({
//            
//            UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];;
//            btn.frame = CGRectMake(VIEW_BX(_bgView)-32, 30, 33, 33);
//            if (isIphone4) {
//                
//                btn.frame = CGRectMake(VIEW_BX(_bgView)-32, 12, 33, 33);
//                
//            }
//            [btn setBackgroundImage:[self imageOfSuperid_ad_close] forState:normal];
//            [btn addTarget:self action:@selector(closeBtnClickEventHandle) forControlEvents:UIControlEventTouchUpInside];
//            
//            btn;
//        });
//        
//        [_maskWindow addSubview:_closeBtn];
        
        _otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _otherBtn.frame = CGRectMake(VIEW_BX(_bgView) - 115, _bgView.top - 40, 115, 30);
        [_otherBtn setCornerRadius:5];
        [_otherBtn setTitle:@"查看更多活动" forState:UIControlStateNormal];
        [_otherBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _otherBtn.titleLabel.font = FONT(14);
        [_otherBtn setBackgroundImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateNormal];
        [_maskWindow addSubview:_otherBtn];
        
        
//        _titleBgView = ({
//            
//            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_W(_bgView), 0.2105*VIEW_H(_bgView))];
//            view.backgroundColor = [UIColor whiteColor];
//            view;
//        });
//        [_bgView addSubview:_titleBgView];
//        
//        _titleLable = ({
//            
//            UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0.3*VIEW_H(_titleBgView), VIEW_W(_titleBgView), 18)];
//            lable.backgroundColor = [UIColor clearColor];
//            lable.textAlignment = NSTextAlignmentCenter;
//            lable.font = [UIFont systemFontOfSize:16];
//            lable.textColor = superid_ad_color_title;
//            lable;
//        });
//        [_titleBgView addSubview:_titleLable];
//        
//        _subTitleLable = ({
//            
//            UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, VIEW_BY(_titleLable)+0.075*VIEW_H(_titleBgView), VIEW_W(_titleBgView), 14)];
//            lable.backgroundColor = [UIColor clearColor];
//            lable.textAlignment = NSTextAlignmentCenter;
//            lable.font = [UIFont systemFontOfSize:12];
//            lable.textColor = superid_ad_color_tips;
//            lable.text = @"一登权威鉴定";
//            lable;
//        });
//        
//        [_titleBgView addSubview:_subTitleLable];
//        
//        _adImageView = ({
//            
//            UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, VIEW_BY(_titleBgView), VIEW_W(_bgView), VIEW_H(_bgView)-VIEW_H(_titleBgView))];
//            
//            view;
//            
//        });
//        [_bgView addSubview:_adImageView];
        
    }
    return _maskWindow;
}

- (id)init{
    
    self = [super init];
    
    if (self) {

        self.userInteractionEnabled = YES;
        
//        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor clearColor];
        
        
//        NSArray *textArray = [[NSArray alloc]initWithObjects:@"含蓄",@"活泼",@"成熟",@"风趣",@"严肃",@"和蔼", nil];
//        NSArray *keyArray  = [[NSArray alloc]initWithObjects:@"reserved",@"lively",@"mature",@"humorous",@"serious",@"kindly", nil];
        
//        _characterDitionary = [[NSDictionary alloc]initWithObjects:textArray forKeys:keyArray];
    }
    
    return self;
}

- (void)groupSync:(NSArray*)images
{
    
    dispatch_group_t group = dispatch_group_create();
    
    for (NSInteger i = 0; i < images.count; i++) {
        
        dispatch_group_enter(group);
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSLog(@"任务%ld完成", (long)i);
            NSLog(@"current thread = %@", [NSThread currentThread]);
            for (NSInteger i = 0; i < 10000; i++) {
                NSLog(@"i=%ld", (long)i);
            }
            dispatch_group_leave(group);
        });
    }
    
    dispatch_group_notify(group, dispatch_get_global_queue(0, 0), ^{
        NSLog(@"current thread = %@", [NSThread currentThread]);
        NSLog(@"任务完成");
        
        // 回到主线程显示图片
        dispatch_async(dispatch_get_main_queue(), ^{
            // 4.将新图片显示出来
            //            self.imageView.image = image;
            NSLog(@"current thread = %@", [NSThread currentThread]);
        });
        
    });
}


- (void)show:(NSArray*)images borderColor:(UIColor*)color
{

    
//    for (NSInteger i = 0; i < images.count; i++) {
//        UIImage *image = [UIImage imageNamed:images[i]];
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.width * i, 0, self.scrollView.width, self.scrollView.height)];
//        imageView.image = image;
//        [self.scrollView addSubview:imageView];
//    }

#if 0
//    dispatch_queue_t queue = dispatch_get_global_queue(0, 0);
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_group_t group = dispatch_group_create();
    
    __block NSMutableArray *errorIndexs = [NSMutableArray array];
    __block NSMutableArray *successIndexs = [NSMutableArray array];
    
    
    
    for (NSInteger i = 0; i < images.count; i++) {
        
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        dispatch_group_enter(group);
        WeakSelf
        dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
           
//            DLog(@"i = %d", i);
            AdvModel *advModel = images[i];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.width * i, 0, self.scrollView.width, self.scrollView.height)];
            [imageView downloadImageFromAliyunOSS:advModel.imageUrl isThumbnail:NO placeholderImage:nil success:^(id responseData) {
                
                StrongSelf
                NSLog(@"current thread = %@", [NSThread currentThread]);
                imageView.image = [UIImage imageWithData:responseData];
                [strongSelf.scrollView addSubview:imageView];
                DLog(@"第%ld张下载成功", (long)i);
                [successIndexs addObject:[NSNumber numberWithInteger:i]];
//                dispatch_semaphore_signal(semaphore);
//                dispatch_group_leave(group);
                
            } andFailure:^(NSString *errorDesc) {
                
//                dispatch_group_leave(group);
                DLog(@"第%ld张下载失败，errorDesc=%@", (long)i, errorDesc);
                [errorIndexs addObject:[NSNumber numberWithInteger:i]];
//                dispatch_semaphore_signal(semaphore);
                
            }];
            
//            dispatch_group_leave(group);
 
        });
        
        
        
    }
    
    WeakSelf
    dispatch_notify(group, dispatch_get_main_queue(), ^(){
        DLog(@"updateUI");
        DLog(@"下载成功后通知更新UI");
        StrongSelf
//        if (successIndexs.count == 0) {
//            [strongSelf dismiss];
//            return;
//        }
        
//        [strongSelf.maskWindow makeKeyAndVisible];
//        strongSelf.maskView.userInteractionEnabled = YES;
//        UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAdvDismiss:)];
//        sigleTapRecognizer.numberOfTapsRequired = 1;
//        [storngSelf.maskView addGestureRecognizer:sigleTapRecognizer];
        
        
        
        
        
        [strongSelf.maskWindow makeKeyAndVisible];
        
        strongSelf.scrollView.contentSize = CGSizeMake(SCROLL_VIEW_WIDTH * images.count, 1.46*0.825*Screen_width);
        
        [strongSelf.bgView addSubview:strongSelf.scrollView];
        
        [strongSelf.maskWindow addSubview:strongSelf.pageControl];
        strongSelf.pageControl.numberOfPages = images.count;
        strongSelf.pageControl.currentPage = 0;
        strongSelf.pageControl.top = strongSelf.bgView.bottom + 10;
        
        if (color) {
            strongSelf.bgView.layer.borderColor = color.CGColor;
        }
        
        strongSelf.maskView.userInteractionEnabled = YES;
        UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAdvDismiss:)];
        sigleTapRecognizer.numberOfTapsRequired = 1;
        [strongSelf.maskView addGestureRecognizer:sigleTapRecognizer];
        
    });
    

#endif
    
//    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
//
//        DLog(@"下载成功后通知更新UI");
//        
//        StrongSelf
//        
//        if (errorIndexs.count > 0) {
//            [strongSelf dismiss];
//            return;
//        }
//    
//        strongSelf.scrollView.contentSize = CGSizeMake(0.8*Screen_width * images.count, 1.46*0.825*Screen_width);
//        [strongSelf.bgView addSubview:self.scrollView];
//        
//        [strongSelf.maskWindow addSubview:self.pageControl];
//        strongSelf.pageControl.numberOfPages = images.count;
//        strongSelf.pageControl.currentPage = 0;
//        strongSelf.pageControl.top = _bgView.bottom + 10;
//        
//        if (color) {
//            strongSelf.bgView.layer.borderColor = color.CGColor;
//        }
//        
//    });
    
    
#if 1
    dispatch_group_t group = dispatch_group_create();
    
    for (NSInteger i = 0; i < images.count; i++) {
        
        dispatch_group_enter(group);
        
        //网络请求
        AdvModel *advModel = images[i];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.width * i, 0, self.scrollView.width, self.scrollView.height)];
//        [imageView downloadImageFromAliyunOSS:advModel.imageUrl isThumbnail:YES placeholderImage:nil success:^(id responseData) {
//            
//            imageView.image = [UIImage imageWithData:responseData];
//            [self.scrollView addSubview:imageView];
//            DLog(@"第%d张下载成功", i+1);
        
//            dispatch_group_leave(group);
//
//        } andFailure:^(NSString *errorDesc) {
//            
//            DLog(@"第%ld张下载失败，errorDesc=%@", i+1, errorDesc);
//            dispatch_group_leave(group);
//
//        }];
        
        [imageView downloadImageFromAliyunOSS:advModel.imageUrl isThumbnail:YES placeholderImage:nil success:^(id responseData) {
            
            imageView.image = responseData;
//            imageView.image = [UIImage imageWithData:responseData];
            [self.scrollView addSubview:imageView];
            DLog(@"第%d张下载成功", i+1);
            
            dispatch_group_leave(group);
            
        } andFailure:^(NSString *errorDesc) {
            
            DLog(@"第%d张下载失败，errorDesc=%@", i+1, errorDesc);
            dispatch_group_leave(group);
            
        }];

    }
    
    WeakSelf
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        StrongSelf
        
        DLog(@"下载成功后通知更新UI");
        
        [strongSelf.maskWindow makeKeyAndVisible];
        strongSelf.scrollView.contentSize = CGSizeMake(0.825*Screen_width * images.count, 1.46*0.825*Screen_width);
        [strongSelf.bgView addSubview:strongSelf.scrollView];
        
        [strongSelf.maskWindow addSubview:strongSelf.pageControl];
        strongSelf.pageControl.numberOfPages = images.count;
        strongSelf.pageControl.currentPage = 0;
        strongSelf.pageControl.top = _bgView.bottom + 10;
        
        if (color) {
            strongSelf.bgView.layer.borderColor = color.CGColor;
        }
        
        strongSelf.maskView.userInteractionEnabled = YES;
        UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:strongSelf action:@selector(onAdvDismiss:)];
        sigleTapRecognizer.numberOfTapsRequired = 1;
        [strongSelf.maskView addGestureRecognizer:sigleTapRecognizer];
        

    });
#endif

    
#if 0
    for (NSInteger i = 0 ; i < images.count; i++) {
        AdvModel *advModel = images[i];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.width * i, 0, self.scrollView.width, self.scrollView.height)];
        NSString *imgUrl = [NSString stringWithFormat:@"%@%@%@", HOST_NAME, @"/iyong-web/ad/download?url=", advModel.imageUrl];
        DLog(@"imgUrl = %@", imgUrl);
        [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        
        [self.scrollView addSubview:imageView];
    }
    
    self.scrollView.contentSize = CGSizeMake(0.8*Screen_width * images.count, 1.46*0.825*Screen_width);
    [_bgView addSubview:self.scrollView];
    
    [_maskWindow addSubview:self.pageControl];
    self.pageControl.numberOfPages = images.count;
    self.pageControl.currentPage = 0;
    self.pageControl.top = _bgView.bottom + 10;
    
    if (color) {
        _bgView.layer.borderColor = color.CGColor;
    }
#endif
}

- (void)showInView:(UIView *)view withFaceInfo: (NSDictionary *)info advertisementImage: (UIImage *)image borderColor: (UIColor *)color{
    
//    if (!info) {
//
//        return;
//    }
    
    
    [self.maskWindow makeKeyAndVisible];
    
    _titleLable.text  = @"title";//[self featureTransform:info];
    _adImageView.image = image;
    if (color) {
        
        _bgView.layer.borderColor = color.CGColor;
        
    }
    
    
    
}

- (void)showWithFaceInfo: (NSDictionary *)info advertisementImage: (UIImage *)image borderColor: (UIColor *)color{
    
    if (!info) {
        
        return;
    }

    _titleLable.text  =[self featureTransform:info];
    _adImageView.image = image;
    if (color) {
        
        _bgView.layer.borderColor = color.CGColor;

    }
    [[self getCurrentVC].view addSubview:self];
    
}

- (void)dismiss
{
    [_maskWindow removeAllSubviews];
    _maskWindow.hidden = YES;
    _maskWindow = nil;
    _scrollView = nil;
    _pageControl = nil;
}

- (void)closeBtnClickEventHandle{
    
    [_maskWindow removeAllSubviews];
//    [self removeFromSuperview];
    _maskWindow = nil;
//    _adImageView.image = nil;
//    _titleLable.text = nil;

}

- (void)dealloc{
    
//    [_maskWindow removeAllSubviews];
    _maskWindow = nil;
    _titleLable = nil;
    _scrollView = nil;
    
    _adImageView = nil;
    _subTitleLable = nil;
    _bgView = nil;
    _titleBgView = nil;
    _closeBtn = nil;
}

- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

- (void)drawSuperid_ad_close
{
    //// Color Declarations
    UIColor* white = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 1];
    
    //// yuan Drawing
    UIBezierPath* yuanPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(2, 2, 60, 61)];
    [white setStroke];
    yuanPath.lineWidth = 2;
    [yuanPath stroke];
    
    
    //// mid Drawing
    UIBezierPath* midPath = UIBezierPath.bezierPath;
    [midPath moveToPoint: CGPointMake(30.01, 32.5)];
    [midPath addLineToPoint: CGPointMake(18.74, 43.96)];
    [midPath addCurveToPoint: CGPointMake(18.74, 45.98) controlPoint1: CGPointMake(18.2, 44.51) controlPoint2: CGPointMake(18.19, 45.42)];
    [midPath addCurveToPoint: CGPointMake(20.73, 45.98) controlPoint1: CGPointMake(19.29, 46.54) controlPoint2: CGPointMake(20.18, 46.54)];
    [midPath addLineToPoint: CGPointMake(32, 34.52)];
    [midPath addLineToPoint: CGPointMake(43.27, 45.98)];
    [midPath addCurveToPoint: CGPointMake(45.26, 45.98) controlPoint1: CGPointMake(43.82, 46.54) controlPoint2: CGPointMake(44.71, 46.54)];
    [midPath addCurveToPoint: CGPointMake(45.26, 43.96) controlPoint1: CGPointMake(45.81, 45.42) controlPoint2: CGPointMake(45.8, 44.51)];
    [midPath addLineToPoint: CGPointMake(33.99, 32.5)];
    [midPath addLineToPoint: CGPointMake(45.26, 21.04)];
    [midPath addCurveToPoint: CGPointMake(45.26, 19.02) controlPoint1: CGPointMake(45.8, 20.49) controlPoint2: CGPointMake(45.81, 19.58)];
    [midPath addCurveToPoint: CGPointMake(43.27, 19.02) controlPoint1: CGPointMake(44.71, 18.46) controlPoint2: CGPointMake(43.82, 18.46)];
    [midPath addLineToPoint: CGPointMake(32, 30.48)];
    [midPath addLineToPoint: CGPointMake(20.73, 19.02)];
    [midPath addCurveToPoint: CGPointMake(18.74, 19.02) controlPoint1: CGPointMake(20.18, 18.46) controlPoint2: CGPointMake(19.29, 18.46)];
    [midPath addCurveToPoint: CGPointMake(18.74, 21.04) controlPoint1: CGPointMake(18.19, 19.58) controlPoint2: CGPointMake(18.2, 20.49)];
    [midPath addLineToPoint: CGPointMake(30.01, 32.5)];
    [midPath closePath];
    midPath.miterLimit = 4;
    
    midPath.usesEvenOddFillRule = YES;
    
    [white setFill];
    [midPath fill];
}

#pragma mark Generated Images
static UIImage* _imageOfSuperid_ad_close = nil;

- (UIImage*)imageOfSuperid_ad_close
{
    if (_imageOfSuperid_ad_close)
        return _imageOfSuperid_ad_close;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(64, 64), NO, 0.0f);
    [self drawSuperid_ad_close];
    
    _imageOfSuperid_ad_close = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return _imageOfSuperid_ad_close;
}

#pragma mark Customization Infrastructure

- (void)setSuperid_ad_closeTargets: (NSArray*)superid_ad_closeTargets
{
    _superid_ad_closeTargets = superid_ad_closeTargets;
    
    for (id target in self.superid_ad_closeTargets)
        [target setImage: self.imageOfSuperid_ad_close];
}

- (NSString *)featureTransform:(NSDictionary *)info{
    
    NSString *str = [NSString stringWithFormat:@"我是一个%@%@的%@",[self getUserCharacterStr:info],[self getUserAppearanceStrFromInfo:info],[self getGenerationStr:info]];
    return str;
    
}

- (NSString *)getUserCharacterStr:(NSDictionary *)dict{
    
    NSString *key = [dict objectForKey:@"character"];

    if (dict && key ) {
        
        if ([_characterDitionary objectForKey:key]) {
            
            return [_characterDitionary objectForKey:key];
        }else{
            
            return @"幸运";
        }
        
        
    }else{
        
        return @"幸运";
    }
}

- (NSString *)getUserAppearanceStrFromInfo: (NSDictionary *)dict{
    
    NSArray *tagsArray = [dict objectForKey:@"tags"];

    if (!tagsArray) {
        
        return @"";
    }
    BOOL isGoodLooking = NO;
    
    for (int i = 0; i<[tagsArray count]; i++) {
        
        if ([[tagsArray objectAtIndex:i]isEqualToString:@"goodLooking"]) {
            
            isGoodLooking = YES;
            
        }
    }
    if (isGoodLooking == YES) {
        
        if ([[dict objectForKey:@"gender"] isEqualToString:@"male"]) {
            
            return @"帅气";
        }else{
            
            return @"漂亮";
        }
        
    }else{
        
        return @"";
    }

}

- (NSString *)getGenerationStr:(NSDictionary *)info{

    NSString *str = [info objectForKey:@"generation"];
    if (str) {
        
        NSString *ageStr = [str substringToIndex:2];
        ageStr = [NSString stringWithFormat:@"%@后",ageStr];
        return ageStr;
    }else{
        
        return @"人";
    }
    
}

#pragma mark - scrollView delegate method

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // 计算当前在第几页
    double scrollViewWidth = SCROLL_VIEW_WIDTH;
    DLog(@"scrollViewWidth = %.5f", scrollViewWidth);
    DLog(@"self.scrollView.contentOffset.x = %.5f", self.scrollView.contentOffset.x);
//    self.pageControl.currentPage = (NSInteger)(self.scrollView.contentOffset.x / scrollViewWidth);
    NSInteger index=self.scrollView.contentOffset.x/(scrollViewWidth+0.5);//fabs(self.scrollView.contentOffset.x)/scrollViewWidth;
    self.pageControl.currentPage = index;
    
//    double mod = self.scrollView.contentOffset.x % scrollViewWidth;

//    CGFloat mod = ((CGFloat)self.scrollView.contentOffset.x) % ((CGFloat)scrollViewWidth);
    
    
    
//    if ((CGFloat)((CGFloat)self.scrollView.contentOffset.x % scrollViewWidth) > 0) {
//        self.pageControl.currentPage ++;
//    }
//    double scrollOffsetX = self.scrollView.contentOffset.x;
//    double mod = (double)scrollOffsetX % scrollViewWidth;
//    if (mod > 0) {
//        self.pageControl.currentPage++;
//    }
}

- (void)handleTapGesture:(id)sender
{
    DLog(@"clicked");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onClickAdv:)]) {
        [self.delegate onClickAdv:self.pageControl.currentPage];
    }
}

- (void)onAdvDismiss:(id)sender
{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(onClickAdv:)]) {
//        [self.delegate onClickAdv:self.pageControl.currentPage];
//    }
    
//    self.scrollView = nil;
//    self.pageControl = nil;
//    self.maskWindow = nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onDismiss)]) {
        [self.delegate onDismiss];
    }
}

@end
