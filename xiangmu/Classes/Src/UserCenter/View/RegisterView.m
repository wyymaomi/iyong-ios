//
//  RegisterView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RegisterView.h"
#import "MyUtil.h"

@implementation RegisterView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.logoImageView.frame = CGRectMake(15, 16, 30, 30);
    self.logoTextLabel.frame = CGRectMake(16, self.logoImageView.bottom+5*Scale, 50*Scale, 20*Scale);
    self.logoTextLabel.font = FONT(10);
    self.logoTextLabel.textColor = [UIColor whiteColor];
    self.logoTextLabel.text = @"iYONG";
    
    self.backButton.frame = CGRectMake(ViewWidth-60, self.logoImageView.top, 60, 30);
    [self.backButton setTitle:@"登录" forState:UIControlStateNormal];
    [self.backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backButton.titleLabel.font = FONT(20);
    
    self.registerTextLabel.frame = CGRectMake(30, 110, ViewWidth-60,22);
    self.registerTextLabel.textColor = [UIColor whiteColor];
    self.registerTextLabel.font = FONT(20);
    
    self.accountView.frame = CGRectMake(60*Scale, self.registerTextLabel.bottom+65*Scale, ViewWidth-120*Scale, 32*Scale);
    self.accountImageView.frame = CGRectMake(0, 0, 22, 22);
    self.accountTextField.textColor = [UIColor whiteColor];
    self.accountTextField.font = FONT(16);
    self.accountTextField.placeholder = @"请输入手机号";
    self.accountTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.accountTextField.frame = CGRectMake(self.accountImageView.right+40*Scale, 0, self.accountView.width-self.accountImageView.right-40*Scale, self.accountView.height);
    self.accountLineView.frame = CGRectMake(0, self.accountView.height-0.5, self.accountView.width, 0.5);
    self.accountLineView.backgroundColor = [UIColor whiteColor];
    
    self.verifyCodeView.frame = CGRectMake(self.accountView.left, self.accountView.bottom+32*Scale, self.accountView.width, 32*Scale);
    self.verifyCodeIconImageView.frame = self.accountImageView.frame;
    self.verifyCodeTextField.frame = self.accountTextField.frame;
    self.verifyCodeTextField.textColor = [UIColor whiteColor];
    self.verifyCodeTextField.font = FONT(16);
    self.verifyCodeTextField.placeholder = @"请输入验证码";
    self.verifyCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.verifyCodeLineView.frame = self.accountLineView.frame;
    self.accountLineView.backgroundColor = [UIColor whiteColor];
    self.verifyCodeButton.frame = CGRectMake(self.verifyCodeView.width-70*Scale, 0, 70*Scale, 25*Scale);
    [self.verifyCodeButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.verifyCodeButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    [self.verifyCodeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
    self.verifyCodeButton.cornerRadius = 10;
    [self.verifyCodeButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    [self.verifyCodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    // 密码输入界面
    self.pwdView.frame = CGRectMake(self.accountView.left, self.verifyCodeView.bottom+25*Scale, self.accountView.width, self.accountView.height);
    self.pwdIconImageView.frame = self.accountImageView.frame;
    self.pwdTextField.frame = self.accountTextField.frame;
    self.pwdTextField.textColor = [UIColor whiteColor];
    self.pwdTextField.font = FONT(16);
    self.pwdTextField.textColor = [UIColor whiteColor];
    self.pwdTextField.placeholder = @"请输入密码";
    self.pwdLineView.frame = CGRectMake(0, self.pwdView.height-0.5,self.pwdView.width, 0.5);
    self.pwdLineView.backgroundColor = [UIColor whiteColor];
    
    // 推荐码
    self.recommendCodeView.frame = CGRectMake(self.accountView.left, self.pwdView.bottom+25*Scale, self.accountView.width, self.accountView.height);
    self.recommendCodeIconImageView.frame = self.accountImageView.frame;
    self.recommendCodeTextField.frame = self.accountTextField.frame;
    self.recommendCodeTextField.textColor = [UIColor whiteColor];
    self.recommendCodeTextField.font = FONT(16);
    self.recommendCodeTextField.placeholder = @"推荐码(可选择)";
    self.recommendCodeLineView.frame = CGRectMake(0, self.recommendCodeView.height-0.5, self.recommendCodeView.width, 0.5);
    self.recommendCodeLineView.backgroundColor = [UIColor whiteColor];
    self.recommendCodeView.hidden = YES;
    
    
    // 注册按钮
    self.registerButton.frame = CGRectMake(36*Scale, self.recommendCodeView.bottom+80*Scale, ViewWidth-72*Scale, 42*Scale);
    [self.registerButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.registerButton.titleLabel.font = FONT(20);
    [self.registerButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.registerButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    self.registerButton.cornerRadius = 20;
    [self.registerButton setTitle:@"立即注册" forState:UIControlStateNormal];
    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.registerButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    
    self.registerProtocolView.top = self.registerButton.bottom + 20*Scale;
    
    
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.pwdTextField.secureTextEntry = YES;
    
    [self addProtocolView];
    
    
}

- (void)addProtocolView
{
    //    NSString *cotnent = @"注册账号即表示同意尊重
    
    UIView *view = [UIView new];
    
    NSInteger labelY = 0;//self.registerView.registerButton.bottom+20*Scale;
    NSString *contentStr = @"注册帐号即表示同意遵守《 爱佣服务条款 》";
    CGSize contentSize = [contentStr sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
    UILabel *label=[MyUtil createLabelFrame:CGRectMake((ViewWidth-contentSize.width)/2, labelY, contentSize.width, 20) title:contentStr font:[UIFont systemFontOfSize:12] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor whiteColor]];
    
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:contentStr];
    //设置：在0-3个单位长度内的内容显示成红色
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(12, 7)];
    label.attributedText = str;
    [view addSubview:label];
    //    [self.registerView.tpkeyboardAvoidingScrollView addSubview:label];
    
    
    NSString *subContent =  @"注册帐号即表示同意遵守《 ";
    CGSize subContentSize = [StringUtil sizeForString:subContent fontSize:12 andWith:MAXFLOAT];
    NSString *buttonText = @"爱佣服务条款";
    CGFloat buttonWidth = [StringUtil widthForString:buttonText fontSize:12 andWidth:MAXFLOAT];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(label.left+subContentSize.width, labelY, buttonWidth, 20)];
    [button setBackgroundColor:[UIColor clearColor]];
//    [button addTarget:self action:@selector(clickMember:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    self.registerProtocolButton = button;
    //    [self.registerView.tpkeyboardAvoidingScrollView addSubview:button];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor whiteColor];
    lineView.frame = CGRectMake(label.left, label.bottom+5, label.width, 0.5);
    [view addSubview:lineView];
    
    view.frame = CGRectMake(0, self.registerButton.bottom+20*Scale, ViewWidth, 20);
    [self addSubview:view];
    
    self.registerProtocolView = view;
    
//    [self.registerView.tpkeyboardAvoidingScrollView addSubview:view];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
