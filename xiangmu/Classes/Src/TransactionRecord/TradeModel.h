//
//  TradeModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol TradeModel <NSObject>
@end

@interface TradeModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *amount;
@property (nonatomic, strong) NSString<Optional> *amountType;
@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *date;
@property (nonatomic, strong) NSString<Optional> *price;
@property (nonatomic, strong) NSString<Optional> *transactionType;
@property (nonatomic, strong) NSString<Optional> *createTime;

@end

@interface TradeResonseData:JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<TradeModel, Optional> *data;

@end
