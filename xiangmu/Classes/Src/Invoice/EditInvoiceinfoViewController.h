//
//  EditInvoiceinfoViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "InvoiceinfoHttpMessage.h"
#import "InvoiceData.h"

@interface EditInvoiceinfoViewController : BaseViewController

@property (nonatomic, strong) InvoiceinfoHttpMessage *invoiceinfoHttpMessage;

@property (nonatomic, strong) NSMutableArray<InvoiceData*> *amountList;

@end
