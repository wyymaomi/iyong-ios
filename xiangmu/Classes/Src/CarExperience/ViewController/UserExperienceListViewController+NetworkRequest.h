//
//  UserExperienceListViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/6.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "UserExperienceListViewController.h"

@interface UserExperienceListViewController (NetworkRequest)

- (void)fetchExperienceListByPageIndex:(NSInteger)pageIndex;

- (void)addLikeNumber:(UserExpericenViewModel*)viewModel likeLabel:(UILabel*)likeLabel likeImageView:(UIImageView*)likeImageView;

- (void)addLikeNumber:(UserExpericenViewModel*)viewModel;

@end
