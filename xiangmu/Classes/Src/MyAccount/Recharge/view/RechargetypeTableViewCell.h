//
//  RechargetypeTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RechargetypeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;
//@property (weak, nonatomic) IBOutlet UILabel *limitAmountDetailLabel;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

@end
