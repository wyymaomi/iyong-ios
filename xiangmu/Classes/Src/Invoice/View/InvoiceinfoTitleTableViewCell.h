//
//  InvoiceinfoTitleTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomTableViewCell.h"

@interface InvoiceinfoTitleTableViewCell : CustomTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *invoiceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceTotalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceTotalQuanityLabel;

@end
