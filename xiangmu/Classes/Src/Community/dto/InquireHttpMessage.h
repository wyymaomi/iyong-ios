//
//  InquireHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface InquireHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *startTime;//用车时间
@property (nonatomic, strong) NSString *endTime;// 结束时间
@property (nonatomic, strong) NSString *startPlace;// 出发地点
@property (nonatomic, strong) NSString *endPlace; // 结束地点
@property (nonatomic, strong) NSString *model; // 车型
@property (nonatomic, strong) NSString *quantity; // 数量
//@property (nonatomic, strong) NSString *location;//出发地点
@property (nonatomic, strong) NSString *itinerary;//大致行程
@property (nonatomic, strong) NSNumber *releaseTime; // 发布时间段
@property (nonatomic, strong) NSString *customerName;//用车联系人
@property (nonatomic, strong) NSString *customerMobile;//用车人电话
//@property (nonatomic, strong) NSString *remark;//备注
//@property (nonatomic, strong) NSString *price;

@property (nonatomic, strong) NSNumber *advance;
@property (nonatomic, strong) NSString *areaCode;// 城市编码
@property (nonatomic, strong) NSNumber *type;// 用车服务类型

@end
