//
//  OrderHelper.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderHelper.h"

@implementation OrderHelper

+(void)checkIsSelfDispatchOrder:(NSString*)orderId success:(SuccessBlock)success andFailure:(FailureBlock)failure;
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_IS_SELF_DISPATCH_API];
    NSString *param = [NSString stringWithFormat:@"orderId=%@", orderId];
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        
        if (success) {
            success(responseData);
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        if (failure) {
            failure(errorDesc);
        }
    }];
}

+(NewOrderHttpMessage*)getOrderHttpMessage:(OrderBaseInfoResponseModel*)orderBaseInfoModel price:(NSString*)price;
{
    NewOrderHttpMessage *orderHttpMessage = [NewOrderHttpMessage new];
    orderHttpMessage.location = [StringUtil getSafeString:orderBaseInfoModel.location];//orderBaseInfoModel.location;
    orderHttpMessage.customerName = [StringUtil getSafeString:orderBaseInfoModel.customerName];//orderBaseInfoModel.customerName;
    orderHttpMessage.customerMobile = [StringUtil getSafeString:orderBaseInfoModel.customerMobile];//orderBaseInfoModel.customerMobile;
    orderHttpMessage.itinerary = [StringUtil getSafeString:orderBaseInfoModel.itinerary];//orderBaseInfoModel.itinerary;
    orderHttpMessage.remark = [StringUtil getSafeString:orderBaseInfoModel.remark];
    orderHttpMessage.model = [StringUtil getSafeString:orderBaseInfoModel.model];
//    orderHttpMessage.carTime = [StringUtil getSafeString:[orderBaseInfoModel.carTime stringValue]];
    orderHttpMessage.carTime = [[orderBaseInfoModel.carTime stringValue] getDateTime:@"yyyy-MM-dd HH:mm"];
    orderHttpMessage.price = price;

    return orderHttpMessage;
}



@end
