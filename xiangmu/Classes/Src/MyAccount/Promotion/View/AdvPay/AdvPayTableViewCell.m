//
//  AdvPayTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPayTableViewCell.h"

@implementation AdvPayTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"AdvPayTableViewCellID";
    AdvPayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        [cell.radioButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_radio_unselected"] forState:UIControlStateNormal];
        [cell.radioButton setBackgroundImage:[UIImage imageNamed:@"icon_btn_radio_selected"] forState:UIControlStateSelected];
        
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"支付宝";
            cell.iconImageView.image = [UIImage imageNamed:@"icon_alipay"];
            
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"微信支付";
            cell.iconImageView.image = [UIImage imageNamed:@"icon_wechat"];
        }
        
        
    }
    return cell;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSInteger titleTextWidth = [self.titleLabel.text textSize:CGSizeMake(200, 16) font:self.titleLabel.font].width;
    self.hotIconImageView.left = self.titleLabel.origin.x + titleTextWidth + 5;
    
//    cell.hotIconImageView
    
//    NSInteger = 
}

+(CGFloat)getCellHeight
{
    return 60;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
