//
//  InvoicinfoContactTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"

@interface InvoicinfoContactTableViewCell : CustomTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactAreaLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactRemarkLabel;


@end
