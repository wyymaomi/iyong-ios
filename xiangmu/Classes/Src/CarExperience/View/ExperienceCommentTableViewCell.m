//
//  ExperienceCommentTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ExperienceCommentTableViewCell.h"

@implementation ExperienceCommentTableViewCell

+ (instancetype)momentsTableViewCellWithTableView:(UITableView *)tableView;
{
    static NSString *reuseID = @"ExperienceCommentTableViewCell";
    ExperienceCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[ExperienceCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseID];

    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xDEDEDE);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.bodyView];
        //        [self addSubview:self.toolView];
    }
    
    return self;
}


//- (id)initWithStyle:(UITableView*)tableView// style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    
//    NSString *ID = @"ExperienceCommentTableViewCell";
//    ExperienceCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
//    if (!cell) {
////        cell = [[ExperienceCommentTableViewCell alloc] initWithStyle:UITableView]
//        cell = [[ExperienceCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.backgroundColor = [UIColor yellowColor];
//        
//        
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 50)];
//        label.textColor = [UIColor blackColor];
//        label.text = @"test";
//        [self.contentView addSubview:label];
//        
//        [self.contentView addSubview:self.iconView];
//        [self.contentView addSubview:self.nameLabel];
//        [self.contentView addSubview:self.companyLabel];
//        //        [self addSubview:self.starView];
//        [self.contentView addSubview:self.contentLabel];
//        //        [self addSubview:self.photosView];
//        
//    }
//    return cell;
//}

//设置cell的frame
//-(void)setFrame:(CGRect)frame{
////    frame.origin.x += circleCellMargin;
////    frame.size.width -= circleCellMargin * 2;
////    [super setFrame:frame];
//    [super setFrame:frame];
//}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (_viewModel) {
        self.bodyView.frame = _viewModel.momentsBodyFrame;
        //        self.toolView.frame = _viewModel.momentsToolBarFrame;
        self.lineView.frame = CGRectMake(12, ViewWidth-24, self.bodyView.height-SEPARATOR_LINE_HEIGHT, SEPARATOR_LINE_HEIGHT);
    }
}


- (void)setViewModel:(ExperienceCommentViewModel *)viewModel
{
    _viewModel = viewModel;
    
    // 设置子控件的frame
    self.bodyView.frame = _viewModel.momentsBodyFrame;
    self.bodyView.viewModel = viewModel;
    
}

- (UIView*)lineView
{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xEBEBEB);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}


- (FollowCommentBodyView*)bodyView
{
    if (_bodyView == nil) {
        _bodyView = [FollowCommentBodyView new];
//        _bodyView.cell = self;
        
    }
    
    return _bodyView;
}
@end
