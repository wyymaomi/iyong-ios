//
//  AppDelegate.m
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+IFlySpeech.h"
#import "AppDelegate+Notification.h"
#import "AppDelegate+NavigationBar.h"
#import "AppDelegate+Location.h"
#import "AppDelegate+Bugly.h"
#import "AliyunOSSManager.h"
#import "AppDelegate+UMeng.h"
#import "AppDelegate+EaseMob.h"
#import "TalkingData.h"
#import "LaunchImageViewController.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import "AppDelegate+JPush.h"
#import "BaiduTraceSDK/BaiduTraceSDK.h"
//#import "NewMainTabBarController.h"
//#import <ShareSDKConnector/ShareSDKConnector.h>


//#define EaseMobAppKey @"easemob-demo#chatdemoui"


@interface AppDelegate ()<IMLSDKRestoreDelegate,BMKGeneralDelegate>


//@property (nonatomic, strong) HomepageViewController *homepageViewController;


@end

@implementation AppDelegate


#pragma mark - life cycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//    [[PLeakSniffer sharedInstance] installLeakSniffer];
    
//    DLog(@"openUDID = %@", [SystemInfo deviceUUID]);
    
    DLog(@"application didFinishLaunchingWithOption");
    
    // 启动时延迟两秒
#if RELEASE
    
#endif
    
    DLog(@"didFinishLaunchingWithOptions");
    
    DLog(@"device model = %@", [SystemInfo deviceVersion]);
    DLog(@"OSType = %@", [SystemInfo OSVersion]);
    

    // 使用SDK的任何功能前，都需要先调用initInfo:方法设置基础信息。
    BTKServiceOption *sop = [[BTKServiceOption alloc] initWithAK:AK mcode:mcode serviceID:serviceID keepAlive:0];
    [[BTKAction sharedInstance] initInfo:sop];
    
    // 要使用百度地图，请先启动BaiduMapManager
    _mapManager = [[BMKMapManager alloc]init];
    BOOL ret = [_mapManager start:AK generalDelegate:self];
    if (!ret) {
        DLog(@"manager start failed!");
//        NSLog(@"manager start failed!");
#if DEBUG
        [MsgToolBox showToast:@"manager start failed!"];
#endif
    }

    
    // 注册环信
    WeakSelf


    // 注册阿里云
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        [[AliyunOSSManager sharedInstance] setupEnvironment];
        
    });
    
    //other code
//#endif
    
//    [self initShareSDK];

    // 注册消息通知
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//        [self setupNotification:application didFinishLaunchingWithOptions:launchOptions];
//    });
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self setupJPush:application didFinishLaunchingWithOptions:launchOptions];
    });


//    [self.networkReach initNetwork];
    
    // 进入主界面
    [self setNavigationBarAppearance];
    //    [application setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    // 判断是否第一次启动app，如果是，就进入引导页
    _isNotFirstLaunchApp = [[[UserDefaults sharedInstance] objectForKey:kNotFirstLaunchApp] boolValue];
    if (!_isNotFirstLaunchApp) {
        [AppConfig currentConfig].isSoundOn = @YES;
        //        // 如果是第一次进入引导页
        //        _window.rootViewController = [[GuideViewController alloc] init];
        [NSThread sleepForTimeInterval:SPLASH_TIME];
        [self initGuideView];
    }
    else {
        // 判断是否有推送通知权限
        if (![AuthorizationUtil isAllowPushNotification]) {
            [UIRoute openSystemSetting:kMsgOpenSystemPushnotification];
        }
        
        //        [self initViews];
        
        [self setupLaunchView];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        // 初始化EaseMob
        [weakSelf setupEaseMob:application didFinishLaunchingWithOptions:launchOptions];
        
    });


    
    //#ifdef PRODUCT
    // 注册umeng
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        [self setupUmeng];
        
    });
    //#endif
    
    //#ifdef PRODUCT
#ifdef RELEASE
    // 注册talkdata
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND,0), ^{
        [TalkingData sessionStarted:@"12F8F53635A54E28BEC45508740CE3BA" withChannelId:@"App Store"];
    });
    
#endif
    

    // 注册科大讯飞
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self initIFlySpeech];
    });
    
    // 最新版本监测
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self checkUpdate];
    });
    
    // 注册第三方SDK，比如支付宝
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [SDKManager registerPlatforms];
    });
    
    // 初始化MoblinkSDK
    // 测试环境Key:d9b58a07cbf4  发布环境Key:d9b9102d3e79 线上：1b8898cb51ccb
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [MobLink registerApp:@"1c13b017dc9f0"];
        [MobLink setDelegate:self];
    });
    
    // 初始化bugly
    //#ifdef RELEASE
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self setupBugly];
    });
    
    //#endif

    


    return YES;

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    DLog(@"applicationDidBecomeActive");
    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [JPUSHService resetBadge];
    
    //当应用进入后台时间超过10分钟，重新进入弹出登录界面
    self.currentTime = CFDateGetAbsoluteTime((CFDateRef)[NSDate date]);
    
    if ((self.receiveLoginOverTimeMessageCount == 0) && (self.resignTime != 0) && (self.currentTime - self.resignTime > LOGIN_OVER_TIME)&& [[UserManager sharedInstance] isLogin])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
    }
    
    if (self.timer == nil) {
        self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
    
    // app进入前台时判断是否需要强制升级,如果需要强制升级，则弹窗
    if ([self.checkUpdateService isForceUpgrade]) {
        [self.checkUpdateService popupUpgradeAlert];
    }

    
}

//#pragma mark - applicationWillResignActive

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    
    //记录进入后台的时间
    [self initResignTime];
    [self.timer invalidate];
    self.timer = nil;
    
}

//#pragma mark - applicationDidEnterBackground

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
    
    DLog(@"applicationDidEnterBackground");
}

//#pragma mark - applicationWillEnterForeground

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    DLog(@"applicationWillEnterForeground");
    [[NSNotificationCenter defaultCenter] postNotificationName:kDisplayAdvNotification object:nil];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 禁止横屏

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window
{
    return UIInterfaceOrientationMaskPortrait;
}



#pragma mark - NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{

//    DLog(@"url = %@", [url ])
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响。
    DLog(@"options = %@", options);
    BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    if (!result) {
        // 其他如支付等SDK的回调
        result = [SDKManager handleOpenURL:url];
        if (!result) {
            return  [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
        }
    }


    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
        result = [SDKManager handleOpenURL:url];
        if (!result) {
            return  [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
        }
    }

    return result;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    [[IFlySpeechUtility getUtility] handleOpenURL:url];
    
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        BOOL result = [SDKManager handleOpenURL:url];
        if (!result) {
            return  [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
        }
    }
    
    return YES;
}

-(void)handleGetRedpackSuccess
{
    NSString *params = [NSString stringWithFormat:@"amount=%@", self.redpackMoney];
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(SAVE_RED_PACK_API) requestMethod:POST params:params success:^(id responseData) {
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSString *message = [NSString stringWithFormat:@"您的红包金额%@元，已放入您的账户",self.redpackMoney];
            [MsgToolBox showToast:message];
            
        }
        else {
            
            [MsgToolBox showToast:getErrorMsg(code_status)];
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        [MsgToolBox showToast:errorDesc];
    }];
}

#pragma mark - Universal Lins

//- (BOOL)application:(UIApplication*)application continueUserActivity:(nonnull NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray * _Nullable))restorationHandler
//{
//    if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb]) {
//        NSURL *webpageURL = userActivity.webpageURL;
//        NSString *host = webpageURL.host;
//        if ([host isEqualToString:@"iyong.com"]) {
//            NSString *url = [userActivity.webpageURL.description substringFromIndex:23];
//            NSArray *mat = [url componentsSeparatedByString:@"/"];
//            if (mat.count == 2) {
//                NSString *type = [mat objectAtIndex:0];
//                NSString *key = [mat objectAtIndex:1];
//                if ([type isEqualToString:@"goods"]) {
//                    [self openGoodsView:key];
//                }
//            }
//        }
//        else {
//            [[UIApplication sharedApplication] openURL:webpageURL];
//        }
//    }
//    
//    return YES;
//}

//- (void)openGoodsView:(NSString*)goodsId
//{
//    UIViewController *rootVc = self.window.rootViewController;
//    
//    
//}



#pragma mark - 系统消息、通知

//- (void)setupNotification:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
//{
//    // 向苹果注册推送，获取deviceToken并上报
//    [self registerAPNS:application];
//    // 初始化SDK
//    [self initCloudPush];
//    // 监听推送通道打开动作
////        [self listenerOnChannelOpened];
//    // 监听推送消息到来
//    [self registerMessageReceive];
//    // 点击通知将App从关闭状态启动时，将通知打开回执上报
//    [CloudPushSDK handleLaunching:launchOptions];
//}

-(void)initGuideView
{
    [[UITextField appearance] setTintColor:NAVBAR_BGCOLOR];
    
    [self _startLocation];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    self.window = [[UIWindow alloc] initWithFrame:rect];
    
    if (!_mainTabbarController) {
        TabbarManager *tabbarManager = [TabbarManager new];
        _mainTabbarController  = [tabbarManager createTabBarController];
//        _mainTabbarController = [[NewMainTabBarController alloc] init];
//        _mainTabbarController = [[MainTabBarController alloc] init];
    }
    
    
    
    self.window.rootViewController = [[GuideViewController alloc] init];
    [self.window makeKeyAndVisible];
//    [[UserDefaults sharedInstance] setObject:@1 forKey:kNotFirstLaunchApp];
}

//-(id<TabbarDelegate>)mainTabbarController
-(CustomTabBarViewController*)mainTabbarController
{
    if (!_mainTabbarController) {
        TabbarManager *tabbarManager = [TabbarManager new];
        _mainTabbarController  = [tabbarManager createTabBarController];
    }
    return _mainTabbarController;
}



-(void)initViewsFromGuidePage
{
    
//    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.window.rootViewController = self.mainTabbarController;
    
    [[UserDefaults sharedInstance] setObject:@1 forKey:kNotFirstLaunchApp];
    
//    [self.window makeKeyAndVisible];
    
}

-(void)setupLaunchView
{

    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    if (!_mainTabbarController) {
        TabbarManager *tabbarManager = [TabbarManager new];
        _mainTabbarController  = [tabbarManager createTabBarController];
        //        _mainTabbarController = [[NewMainTabBarController alloc] init];
        //        _mainTabbarController = [[MainTabBarController alloc] init];
    }
    
//    [self _startLocation];
    
    LaunchImageViewController *launchViewController = [[LaunchImageViewController alloc] init];
    
    
    self.window.rootViewController = launchViewController;
    
    [self.window makeKeyAndVisible];
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        [self _startLocation];
        
//    });
    
}



-(void)initViews
{


    
    CGRect rect = [UIScreen mainScreen].bounds;
    self.window = [[UIWindow alloc] initWithFrame:rect];
    
    
    [self _startLocation];
    
//    HomepageViewController *homepageViewController = [HomepageViewController new];
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homepageViewController];
//    self.window.rootViewController = navigationController;
    
//    if (!_mainTabbarController) {
//        _mainTabbarController = [[MainTabBarController alloc] init];
//    }
//    _mainTabbarController = [[MainTabBarController alloc] init];
    self.window.rootViewController = self.mainTabbarController;
    
    
    [[UserDefaults sharedInstance] setObject:@1 forKey:kNotFirstLaunchApp];
    
//    [NSThread sleepForTimeInterval:SPLASH_TIME];
//    
//    [self.window makeKeyAndVisible];

//    dispatch_main_sync_safe(^{
//        
////        DLog(@"===============================WYY============================");
//        DLog(@"WYY 启动页面开始暂时停留2秒============");
//        
////        [NSThread sleepForTimeInterval:SPLASH_TIME];
//        
////        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(SPLASH_TIME * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        
////            [self.window makeKeyAndVisible];
////        });
//        
//        [NSTimer scheduledTimerWithTimeInterval:SPLASH_TIME target:self selector:@selector(makeKeyWindowVisible) userInfo:nil repeats:NO];
//
//        
//    });
    
    [NSThread sleepForTimeInterval:SPLASH_TIME];
    
    [self.window makeKeyAndVisible];
    

    
//    dispatch_after(sleepTime, dispatch_get_main_queue(), ^(void){
//        [self.window makeKeyAndVisible];
//    });
    
//    [self performSelector:@selector(makeKeyWindowVisible) withObject:nil afterDelay:SPLASH_TIME];
    
    
    
    // 2. 再创建一个窗口
//    UIWindow *w2 = [[UIWindow alloc] initWithFrame:CGRectMake(100, 100, 200, 200)];
//    w2.backgroundColor = [UIColor yellowColor];
//    [w2 makeKeyAndVisible];
    
}

-(void)makeKeyWindowVisible
{
    DLog(@"WYY 显示主界面");
    [self.window makeKeyAndVisible];
}

#pragma mark - 检测升级

-(CheckUpdateService*)checkUpdateService
{
    if (!_checkUpdateService) {
        _checkUpdateService = [CheckUpdateService new];
    }
    return _checkUpdateService;
}

- (void)checkUpdate
{
    [self.checkUpdateService checkVersion];
}



#pragma mark - auto login when app is laucnhed

-(void)autoLogin
{
//    NSString *loginRsaBody = [[UserManager sharedInstance] loginRsaBody];
//    NSString *loginShaBody = [[UserManager sharedInstance] loginShaBody];
    
//    NSString *userName = 
    
//    WeakSelf
    if ([[UserManager sharedInstance] isLogin] /*&& !IsStrEmpty(loginRsaBody) && !IsStrEmpty(loginShaBody)*/) {
//        NSString *thirdPartyLogin = [UserManager sharedInstance].thirdPartyLoginParams;
//        if (IsStrEmpty(thirdPartyLogin)) {
//            
//        }
        [[UserManager sharedInstance] doAutoLogin:^(NSInteger code_status) {
//            StrongSelf
            if (code_status == STATUS_OK) {
//                [strongSelf doOrderCountQuery];
//                [strongSelf doGetWaitPassedPartner];
            }
            
        } failure:^(NSInteger code_status) {
            [self.window makeKeyAndVisible];
//            if (code_status ==  1001) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
////                [self showWaiting]
//            }
//            else {
//                
//            }
        }];
    }
}



#pragma mark - APP进入前台运行时的操作

-(BOOL)isLoginOvertime; // 判断是否超时登录
{
    self.currentTime = CFDateGetAbsoluteTime((CFDateRef)[NSDate date]);
    
    return [[UserManager sharedInstance] isLogin] && (self.receiveLoginOverTimeMessageCount == 0) && (self.resignTime != 0) && (self.currentTime - self.resignTime > LOGIN_OVER_TIME);
}

-(void)timerAction:(NSTimer*)timer
{
    //    [self handleCurrentTime];
    
    //当应用进入后台时间超过10分钟，重新进入弹出登录界面
    self.currentTime = CFDateGetAbsoluteTime((CFDateRef)[NSDate date]);
//    DLog(@"receiveLoginOverTimeMessageCount = %ld", (long)self.receiveLoginOverTimeMessageCount);
    
    if ([self isLoginOvertime])
    {
//        self.hasNotificationed  = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
    }
}

-(void)initResignTime
{
    self.resignTime = CFDateGetAbsoluteTime((CFDateRef)[NSDate date]);
}

-(void)handleCurrentTime
{
    //当应用进入后台时间超过10分钟，重新进入弹出登录界面
    self.currentTime = CFDateGetAbsoluteTime((CFDateRef)[NSDate date]);
    
    if (/*[[UserManager sharedInstance] isLogin] && (self.receiveLoginOverTimeMessageCount == 0) && (self.resignTime != 0) && (self.currentTime - self.resignTime > LOGIN_OVER_TIME)*/[self isLoginOvertime])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
    }
}

-(void)backFromLogin
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kOnLogoutNotification object:nil];
    
    [_mainTabbarController backFromLogin];
    
    
//    [_mainTabbarController setSelectedIndex:0];
//    [_mainTabbarController setCenterButtonSelected:NO];
//    self.mainTabbarController set
//    [self.tabBarController setSelectedViewController:_hallNavigation];
}

-(void)backToOrderList
{
//    [_mainTabbarController setSelectedIndex:1];
//    [_mainTabbarController setCenterButtonSelected:NO];
//    tabBar.centerButton.selected = NO;
}

#pragma mark - getter and setter

- (NetworkReach *)networkReach
{
    if (!_networkReach) {
        _networkReach = [[NetworkReach alloc] init];
    }
    return _networkReach;
}

#pragma mark - dealloc

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_timer invalidate];
}

#pragma mark -

- (void)IMLSDKStartCheckScene
{
    NSLog(@"Start Check Scene");
}

- (void)IMLSDKEndCheckScene
{
    NSLog(@"End Check Scene");
}

- (void) IMLSDKWillRestoreScene:(MLSDKScene *)scene Restore:(void (^)(BOOL))restoreHandler
{
//    NSLog(@"Will Restore Scene - Path:%@",scene.path);
    
//    if ([scene.path hasSuffix:@"provider.html"])
//    if ([scene.path contains:@"provider"])
//    {
//        [[MLDTool shareInstance] showAlertWithTitle:nil
//                                            message:@"是否进行场景恢复？"
//                                        cancelTitle:@"否"
//                                         otherTitle:@"是"
//                                         clickBlock:^(MLDButtonType type) {
//                                             type == MLDButtonTypeSure ? restoreHandler(YES) : restoreHandler (NO);
//                                         }];
//    }
//    else
//    {
//        restoreHandler(YES);
//    }
}

- (void)IMLSDKCompleteRestore:(MLSDKScene *)scene
{
//    NSLog(@"Complete Restore -Path:%@",scene.path);
}

- (void)IMLSDKNotFoundScene:(MLSDKScene *)scene
{
//    NSLog(@"Not Found Scene - Path :%@",scene.path);
    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"没有找到路径"
//                                                        message:[NSString stringWithFormat:@"Path:%@",scene.path]
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//    [alertView show];
    
}

#pragma  mark - BMKGeneralDelegate delegate method

- (void)onGetNetworkState:(int)iError
{
    if (0 == iError) {
        DLog(@"联网成功");
    }
    else{
        DLog(@"onGetNetworkState %d",iError);
    }
    
}

- (void)onGetPermissionState:(int)iError
{
    if (0 == iError) {
        DLog(@"授权成功");
    }
    else {
        DLog(@"onGetPermissionState %d",iError);
    }
}

@end
