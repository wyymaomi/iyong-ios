//
//  UITextView+MaxLength.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (MaxLength)

@property (nonatomic, assign) NSInteger maxLength;

@end
