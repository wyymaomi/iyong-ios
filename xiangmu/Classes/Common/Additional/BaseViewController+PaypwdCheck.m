//
//  BaseViewController+PaypwdCheck.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController+PaypwdCheck.h"
#import "PasswordBuild.h"
#import "PasswordSetViewController.h"
#import "EncryptUtil.h"

@implementation BaseViewController (PaypwdCheck)

#pragma mark - 弹出支付密码窗口
- (void)reCreatePassword {
    PasswordBuild * pwdCreate = [[PasswordBuild alloc] init];
    pwdCreate.pwdCount = 6;
    pwdCreate.pwdOperationType = PwdOperationTypeCreate;
    [pwdCreate show];
    
    __weak typeof(self)weakSelf = self;
    pwdCreate.PwdInit = ^(NSString *pwd){
        [weakSelf checkPayPwd:pwd];
    };
}


#pragma mark - 查询是否有支付密码
-(void)checkHasPayPwd
{
    
    self.nextAction = @selector(checkHasPayPwd);
    self.object1 = nil;
    self.object2 = nil;
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_HAS_API];
    [self showHUDIndicatorViewAtCenter:@"正在加载中"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            //            if (!IsStrEmpty(responseData[@"data"])) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                // 已经设置支付密码发送支付请求 弹出支付密码输入窗口验证 验证OK再进行支付
                [self reCreatePassword];
            }
            else if([data integerValue] == 0){
                // 未设置支付密码
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请设置支付密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                WeakSelf
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        PasswordSetViewController *viewController = [[PasswordSetViewController alloc] init];
                        viewController.type = MimaTypeJiaoyi;
                        [weakSelf.navigationController pushViewController:viewController animated:YES];
                    }
                }];
            }
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];
}

#pragma mark - 验证支付密码是否正确
-(void)checkPayPwd:(NSString*)pwd
{
    self.nextAction = @selector(checkPayPwd:);
    self.object1 = pwd;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_VALID_API];
    NSString *param = [NSString stringWithFormat:@"password=%@", [EncryptUtil getRSAEncryptPassword:pwd]];
    [self showHUDIndicatorViewAtCenter:@"正在校验中"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                if (strongSelf.payDelegate && [self.payDelegate respondsToSelector:@selector(onPaySuccess)]) {
                    [strongSelf.payDelegate onPaySuccess];
                }
//                if (strongSelf.payHttpMessage.rateId == nil) {
//                    [strongSelf showAlertView:ALERT_MSG_RATE_ID_NULL];
//                    return;
//                }
//                NSString *orderId = _orderMode == 1 ? strongSelf.orderId : strongSelf.orderBaseInfoModel.id;
//                strongSelf.payHttpMessage.orderId = orderId;
//                strongSelf.payHttpMessage.bargainId = strongSelf.barginAndPayModel.pay.id;
//                [strongSelf onSubmit:strongSelf.payHttpMessage];
//                if (completeBlock) {
//                    completeBlock();
//                }
            }
            else if([data integerValue] == 0){
                // 校验支付密码失败
//                [strongSelf showAlertView:@"校验支付密码失败"];
//                [MsgToolBox showToast:@"校验支付密码失败"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"支付密码错误，请重新输入" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
            //            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];
}



@end
