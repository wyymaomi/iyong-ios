//
//  RechargeTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RechargeTableViewCell.h"

@interface RechargeTableViewCell ()<UITextFieldDelegate>
@end

@implementation RechargeTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath
{
    static NSString *ID = @"RechargeTableViewCell";
    RechargeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        if (indexPath.section == 0) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//            cell.selectionStyle = UITableViewSelectionStyleGray;
            cell.titleLabel.text = @"储蓄卡";
            cell.valueTextField.placeholder = @"请选择银行卡号";
            cell.valueTextField.enabled = NO;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.titleLabel.text = @"金   额";
            cell.valueTextField.placeholder = @"请输入充值金额";
            cell.valueTextField.enabled = YES;
            cell.valueTextField.keyboardType = UIKeyboardTypeNumberPad;
        }
//        cell.valueTextField.delegate = self;
    }
    return cell;
}

//#pragma mark - UITextFieldDelegate
//#define myDotNumbers     @"0123456789.\n"
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    
//    if (textField.keyboardType != UIKeyboardTypeDecimalPad) {
//        return NO;
//    }
//    //输入字符限制
//    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:myDotNumbers]invertedSet];
//    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//    if (filtered.length == 0) {
//        //支持删除键
//        return [string isEqualToString:@""];
//    }
//    if (textField.text.length == 0) {
//        return ![string isEqualToString:@"."];
//    }
//    //第一位为0，只能输入.
//    else if (textField.text.length == 1){
//        if ([textField.text isEqualToString:@"0"]) {
//            return [string isEqualToString:@"."];
//        }
//    }
//    else{//只能输入一个.
//        if ([textField.text rangeOfString:@"."].length) {
//            if ([string isEqualToString:@"."]) {
//                return NO;
//            }
//            //两位小数
//            NSArray *ary =  [textField.text componentsSeparatedByString:@"."];
//            if (ary.count == 2) {
//                if ([ary[1] length] == 2) {
//                    return NO;
//                }
//            }
//        }
//    }
//    
//    return YES;
//}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
