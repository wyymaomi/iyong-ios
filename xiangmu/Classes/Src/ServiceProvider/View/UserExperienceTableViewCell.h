//
//  UserExperienceTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CommentBodyView.h"
//#import "CommentToolBarView.h"

@class UserExpericenViewModel;
//@class ServiceBodyView;
//@class ServiceToolBarView;

@class CommentBodyView;
//@class CommentToolBarView;

@protocol UserExperienceCellDelegate <NSObject>

- (void)onClickLikeButton:(id)sender;

@end



@interface UserExperienceTableViewCell : UITableViewCell

@property (nonatomic, weak) id<UserExperienceCellDelegate> cellDelegate;

@property (nonatomic, strong) UserExpericenViewModel *viewModel;

@property (nonatomic, strong) UIView *lineView;

// 主体
@property (nonatomic, strong) CommentBodyView *bodyView;

+ (instancetype)momentsTableViewCellWithTableView:(UITableView *)tableView;

@end
