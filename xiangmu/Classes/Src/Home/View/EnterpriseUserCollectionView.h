//
//  EnterpriseUserCollectionView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EnterpriseUserCollectionViewDelegate <NSObject>

- (void)onGotoPage:(NSString*)vcPage title:(NSString*)title;

@end

@interface EnterpriseUserCollectionView : UICollectionView

@property (nonatomic, assign) id<EnterpriseUserCollectionViewDelegate> customDelegate;

@property (nonatomic, assign) NSInteger orderCount;
@property (nonatomic, assign) NSInteger applyPartnerBadgeNumber;

@end
