//
//  BussinessSearchHttpMessage.h
//  xiangmu
//
//  Created by David kim on 16/9/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface BussinessSearchHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *endTime;
@property (nonatomic, strong) NSString *startPlace;
@property (nonatomic, strong) NSString *areaCode;// 城市代码
@property (nonatomic, strong) NSNumber *type; // 用车服务类型

@end
