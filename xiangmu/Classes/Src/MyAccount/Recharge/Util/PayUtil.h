//
//  PayUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayUtil : NSObject

+(BOOL)validatePayInfo:(NSString*)amount bankcardNo:(NSString*)bankcardNo payType:(NSInteger)payType error:(NSString**)errorMsg;

@end
