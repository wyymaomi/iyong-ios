//
//  ABRootViewController.m
//  Nav
//
//  Created by zhuo li on 11-5-10.
//  Copyright 2011 chiefree. All rights reserved.
//

#import "ABRootViewController.h"

@implementation ABRootViewController


#pragma mark - life cycle



#pragma mark - IOS8_OR_LATER

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    
    if (IsStrEmpty(phoneNO)) {
        return;
    }
    
    if ([phoneNO hasPrefix:@"+"]) {
        phoneNO = [phoneNO substringFromIndex:3];
    }
    if ([phoneNO startsWith:@"86"]) {
        phoneNO = [phoneNO substringFromIndex:2];
    }
    
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"%@", phoneNO);
    
    if (self.controllerDelegate && [self.controllerDelegate respondsToSelector:@selector(onGetContactPhone:)]) {
        [self.controllerDelegate onGetContactPhone:phoneNO];
    }
//    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person
{
    ABPersonViewController *personViewController = [[ABPersonViewController alloc] init];
    personViewController.displayedPerson = person;
    [peoplePicker pushViewController:personViewController animated:YES];
}



#pragma mark - IOS7_OR_BEFORE

#pragma mark ABPeoplePickerNavigationControllerDelegate methods 
// Displays the information of a selected person 
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    
    if ([phoneNO hasPrefix:@"+"]) {
        phoneNO = [phoneNO substringFromIndex:3];
    }
    if ([phoneNO startsWith:@"86"]) {
        phoneNO = [phoneNO substringFromIndex:2];
    }
    
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"%@", phoneNO);
    
    if (self.controllerDelegate && [self.controllerDelegate respondsToSelector:@selector(onGetContactPhone:)]) {
        [self.controllerDelegate onGetContactPhone:phoneNO];
    }
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
    return NO;
}

//#pragma mark -
//#pragma mark ABPeoplePickerNavigationControllerDelegate
//- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
//{
//    return NO;
//}

// Dismisses the people picker and shows the application when users tap Cancel.  
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:nil];;
}

#pragma mark -
#pragma mark ABPersonViewControllerDelegate methods 
// Does not allow users to perform default actions such as dialing a phone number, when they select a contact property. 
- (BOOL)personViewController:(ABPersonViewController *)personViewController shouldPerformDefaultActionForPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifierForValue
{
    return NO;
} 


@end
