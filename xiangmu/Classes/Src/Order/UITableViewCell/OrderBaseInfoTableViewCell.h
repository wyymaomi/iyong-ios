//
//  OrderBaseInfoTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderBaseInfoResponseModel.h"
#import "HyperlinksButton.h"

//IB_DESIGNABLE
@interface OrderBaseInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *upCompanyLabel;// 派单方
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel; // 订单号
@property (weak, nonatomic) IBOutlet UIView *carDateView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UILabel *carDateLabel; // 用车日期
//@property (weak, nonatomic) IBOutlet UITextField *carDateTextField; // 用车日期
@property (weak, nonatomic) IBOutlet UITextField *startLocationTextField;// 出发地点
@property (weak, nonatomic) IBOutlet UITextField *customerTextField;// 用车人
//@property (weak, nonatomic) IBOutlet UITextField *customerMobileTextField;// 用户电话
@property (weak, nonatomic) IBOutlet UILabel *carModelLabel;

@property (weak, nonatomic) IBOutlet UITextView *itineraryTextView; //大致行程
@property (weak, nonatomic) IBOutlet UITextView *memoTextView; // 备注
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet HyperlinksButton *telephoneButton;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

-(void)initData:(OrderBaseInfoResponseModel*)model;

-(void)doSelectCarDate;

@end
