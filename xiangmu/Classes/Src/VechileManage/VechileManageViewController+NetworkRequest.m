 //
//  VechileManageViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileManageViewController+NetworkRequest.h"

@implementation VechileManageViewController (NetworkRequest)

-(void)fetchDriverList:(NSInteger)status createTime:(NSString*)createTime
{
    if (IsStrEmpty(createTime)) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    NSString *params = [NSString stringWithFormat:@"status=%ld&time=%@", (long)status, [StringUtil getSafeString:createTime]];
    
    self.loadStatus = LoadStatusLoading;
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(BIND_DRIVER_LIST_API) requestMethod:POST params:params success:^(NSDictionary * responseData) {
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        strongSelf.loadStatus = LoadStatusFinish;
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            NSArray *dataList = responseData[@"data"];
            for (NSDictionary *dictionary in dataList) {
                BindDriverModel *model = [[BindDriverModel alloc] initWithDictionary:dictionary error:nil];
                [strongSelf.dataList addObject:model];
            }
            
            [strongSelf.pullRefreshTableView reloadData];
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        [weakSelf resetAction];
        [MsgToolBox showToast:errorDesc];
    }];
    
}

-(void)unbindDriver:(id)sender
{
    
    UIButton *button = (UIButton*)sender;
    NSInteger tag = button.tag;
    
    
    BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"是否注销" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定"];
    WeakSelf
    [alertView setConfirmBlock:^{
        [weakSelf unBindDriver:tag];
    }];
    [alertView show];
    

    

}

-(void)unBindDriver:(NSInteger)tag;
{
    
    BindDriverModel *model = self.dataList[tag];
    
    NSString *params = [NSString stringWithFormat:@"id=%@", model.id];
    
    [self showHUDIndicatorViewAtCenter:@"正在注销，请稍候"];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(UNBIND_DRIVER_API) requestMethod:POST params:params success:^(NSDictionary * responseData) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            [MsgToolBox showToast:@"注销成功"];
            [weakSelf.dataList removeObject:model];
            [weakSelf.pullRefreshTableView reloadData];
            
        }
        else {
            
            [MsgToolBox showToast:@"注销失败，请重试"];
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        [MsgToolBox showToast:errorDesc];
        
    }];
}

@end
