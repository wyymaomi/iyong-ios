//
//  InquireViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "InquireViewController.h"
#import "BaseOrderTableViewCell.h"
#import "InquireHttpMessage.h"
//#import "ActionSheetPicker.h"
#import "ActionSheetStringPicker.h"
#import "CustomActionSheetDatePicker.h"
#import "InputHelperViewController.h"
#import "TextInputHelperViewController.h"
#import "LocationSearchViewController.h"
#import "ThirdPayNoteViewController.h"
#import "CommunityHelper.h"
#import "AppConfig.h"

@interface InquireViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) InquireHttpMessage *httpMessage;
@property (nonatomic, strong) NSArray *releaseTimePeriods;

@end

@implementation InquireViewController


- (NSArray*)releaseTimePeriods
{
    if (!_releaseTimePeriods) {
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"ReleaseTimePeriod.plist" ofType:nil];
        _releaseTimePeriods = [NSArray arrayWithContentsOfFile:path];
    
    }
    
    return _releaseTimePeriods;
}


- (InquireHttpMessage*)httpMessage;
{
    if (_httpMessage == nil) {
        _httpMessage = [[InquireHttpMessage alloc] init];
        _httpMessage.type = [AppConfig currentConfig].serviceType;
        _httpMessage.releaseTime = @30;
        _httpMessage.advance = @0;
    }
    return _httpMessage;
}

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"询价";
    
    [self.inquireButton blueStyle];
    
    UIView *view = [[UIView alloc] initWithFrame:(CGRectMake(0,0,ViewWidth,0.1))];
//    view.backgroundColor = [UIColor blueColor];
    self.tableView.tableHeaderView = view;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 业务逻辑

#pragma mark - 设置时间

- (void)setStartTime:(id)sender
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@""
                                           doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
                                               StrongSelf
                                               strongSelf.httpMessage.startTime = selectDateTime;
                                               [strongSelf.tableView reloadData];
                                           } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                               
                                           } origin:sender];
}

-(void)setEndTime:(id)sender
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@""
                                           doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
                                               StrongSelf
                                               
                                               strongSelf.httpMessage.endTime = selectDateTime;
                                               
                                               NSDate *date1 = [NSDate dateFromString:strongSelf.httpMessage.startTime dateFormat:@"yyyy-MM-dd HH:mm"];
                                               NSDate *date2 = [NSDate dateFromString:strongSelf.httpMessage.endTime dateFormat:@"yyyy-MM-dd HH:mm"];
                                               NSInteger compareResult = [NSDate dateCompare:date1 secondDate:date2];
                                               if (compareResult >=0) {
                                                   
                                                   strongSelf.httpMessage.endTime = @"";
                                                   
                                                   [strongSelf showAlertView:@"结束时间必须大于开始时间"];
                                               }
                                               else {
                                                   
                                                   [strongSelf.tableView reloadData];
                                               }
                                               
                                           } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                               
                                               
                                           } origin:sender];
}


#pragma mark - 设置发布时间段



- (void)setReleaseTime:(id)sender
{
    
    NSMutableArray *releaseTimeStrList = [NSMutableArray new];
    
    for (NSInteger i = 0; i < self.releaseTimePeriods.count; i++) {
        NSDictionary *dict = self.releaseTimePeriods[i];
        [releaseTimeStrList addObject:dict[@"key"]];
    }
    
    WeakSelf
    [ActionSheetStringPicker showPickerWithTitle:@"请选择时间段" rows:releaseTimeStrList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        NSDictionary *dict = weakSelf.releaseTimePeriods[selectedIndex];
        weakSelf.httpMessage.releaseTime = dict[@"value"];
        [weakSelf.tableView reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender];
    
}

- (IBAction)onInquire:(id)sender {
    
    NSString *errorMsg;
    if (![CommunityHelper isValidEnquire:self.httpMessage errorMsg:&errorMsg]) {
        [MsgToolBox showToast:errorMsg];
        return;
    }
    
    
    self.nextAction = @selector(onInquire:);
    self.object1 = sender;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_ENQUIRY_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在发布询价，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:self.httpMessage.description success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"发布成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
                
            }];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
}

#pragma mark - 第三方包含垫付费用注意事项

- (void)openThirdpayNote:(UITapGestureRecognizer*)tapGestureRecognizer
{
    ThirdPayNoteViewController *viewController = [[ThirdPayNoteViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - 

- (void)onCheckButtonClick:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    button.selected = !button.isSelected;
    
    self.httpMessage.advance = [NSNumber numberWithInteger:button.selected];
    
}



#pragma mark - UITableView Delegate Method

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width,10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView]-1) {
        
        return 50;
    }
    return 0.001f;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == [self numberOfSectionsInTableView:tableView]-1) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20, 0, ViewWidth-40, 50)];
        
        UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [checkButton setBackgroundImage:[UIImage imageNamed:@"icon_check_button"] forState:UIControlStateNormal];
        [checkButton setBackgroundImage:[UIImage imageNamed:@"icon_check_button_sel"] forState:UIControlStateSelected];
        checkButton.frame = CGRectMake(15, 12, 15, 15);
        [checkButton addTarget:self action:@selector(onCheckButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        checkButton.selected = [self.httpMessage.advance boolValue];
        [view addSubview:checkButton];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(37, 12, ViewWidth-37, 15)];
        label.backgroundColor = [UIColor clearColor];
        label.text = @"报价是否需要第三方包含垫付费用";
        label.textColor = UIColorFromRGB(0x4a7292);
        label.font = FONT(14);
        label.userInteractionEnabled = YES;
        [label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openThirdpayNote:)]];
        [view addSubview:label];

        return view;
        
    }
    
    return [UIView new];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }
    if (section == 1) {
        return 2;
    }
    if (section == 2) {
        return 2;
    }
    if (section == 3) {
        return 2;
    }
    if (section == 4) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BaseOrderTableViewCell *cell = [BaseOrderTableViewCell cellWithTableView:tableView];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"出发时间";
            cell.textField.placeholder = @"请选择出发时间";
            cell.textField.tag = InquireStartDateTime;
            cell.textField.text = self.httpMessage.startTime;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"结束时间";
            cell.textField.placeholder = @"请选择结束时间";
            cell.textField.tag = InquireEndDateTime;
            cell.textField.text = self.httpMessage.endTime;
        }
        
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"出发地点";
            cell.textField.placeholder = @"请输入出发地点";
            cell.textField.tag = InquireStartLocation;
            cell.textField.text = self.httpMessage.startPlace;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"结束地点";
            cell.textField.placeholder = @"请输入结束地点";
            cell.textField.tag = InquireEndLocation;
            cell.textField.text = self.httpMessage.endPlace;
        }
    }
    
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"用车人";
            cell.textField.placeholder = @"请输入用车人";
            cell.textField.tag = OrderTextFieldCustomerName;
            cell.textField.text = self.httpMessage.customerName.trim;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"用车人电话";
            cell.textField.placeholder = @"请输入用车人电话";
            cell.textField.tag = OrderTextFieldCustomerMobile;
            cell.textField.text = self.httpMessage.customerMobile.trim;
        }
        
    }
    if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"车       型";
            cell.textField.placeholder = @"请输入车型";
            cell.textField.tag = InquireVechileModel;
            cell.textField.text = self.httpMessage.model.trim;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"数       量";
            cell.textField.placeholder = @"请输入用车数量";
            cell.textField.tag = InquireVechileNumber;
            cell.textField.text = self.httpMessage.quantity.trim;
        }
    }
    if (indexPath.section == 4) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"大致行程";
            cell.textField.placeholder = @"请输入大致行程";
            cell.textField.tag = InquireRoughtTravel;
            cell.textField.text = self.httpMessage.itinerary.trim;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"发布时间段";
            cell.textField.placeholder = @"选择时间段";
            cell.textField.tag = InquirePbulishTimeSegment;
            cell.textField.text = @"";
            NSInteger releaseTimeInteger = [self.httpMessage.releaseTime integerValue];
            if (releaseTimeInteger > 0) {
                if (releaseTimeInteger/60 > 0) {
                    cell.textField.text = [NSString stringWithFormat:@"%ld小时", (long)releaseTimeInteger/60];
                }
                else {
                    cell.textField.text = [NSString stringWithFormat:@"%ld分钟", (long)releaseTimeInteger];
                }
            }
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            [self setStartTime:cell.textField];
            
        }
        
        if (indexPath.row == 1) {
            
            [self setEndTime:cell.textField];
            
        }
    }
    
    if (indexPath.section == 1) {
        
        BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (cell) {
            
            LocationSearchViewController *vc = [[LocationSearchViewController alloc] init];
            
            vc.title = cell.titleLabel.text;
            
            if (indexPath.row == 0) {
                
                WeakSelf
                vc.selectLocationHandle = ^(NSString *text, NSString *cityId) {
                    weakSelf.httpMessage.startPlace = text;
                    weakSelf.httpMessage.areaCode = cityId;
                    [weakSelf.tableView reloadData];
                };
                
            }
            if (indexPath.row == 1) {
                
                WeakSelf 
                vc.selectLocationHandle = ^(NSString *text, NSString *cityId) {
                    weakSelf.httpMessage.endPlace = text;
                    [weakSelf.tableView reloadData];
                };
                
            }
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        
    }
    
    if (indexPath.section == 2) {
        
        
        BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (cell) {
            
            InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
            viewController.title = cell.titleLabel.text;
            viewController.text = cell.textField.text;
            viewController.tag = cell.textField.tag;
            
            if (indexPath.row == 0) {
                WeakSelf
                viewController.completeHandle = ^(NSInteger tag, NSString *text) {
                    weakSelf.httpMessage.customerName = text.trim;
                    [weakSelf.tableView reloadData];
                };
            }
            
            if (indexPath.row == 1) {
                WeakSelf
                viewController.completeHandle = ^(NSInteger tag, NSString *text) {
                    weakSelf.httpMessage.customerMobile = text.trim;
                    [weakSelf.tableView reloadData];
                };
            }
            
            [self.navigationController pushViewController:viewController animated:YES];
            
        }
        
    }
    
    if (indexPath.section == 3) {
        
        BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (cell) {
            
            InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
            viewController.title = cell.titleLabel.text;
            viewController.text = cell.textField.text;
            viewController.tag = cell.textField.tag;
            
            if (indexPath.row == 0) {
                WeakSelf
                viewController.completeHandle = ^(NSInteger tag, NSString *text) {
                    weakSelf.httpMessage.model = text.trim;
                    [weakSelf.tableView reloadData];
                };
                
            }
            
            else if (indexPath.row == 1){
                WeakSelf
                viewController.completeHandle = ^(NSInteger tag, NSString *text) {
                    weakSelf.httpMessage.quantity = text.trim;
                    [weakSelf.tableView reloadData];
                };
            }
            [self.navigationController pushViewController:viewController animated:YES];
        }
        
    }
    
    if (indexPath.section == 4) {
        
        if (indexPath.row == 0) {
            
            BaseOrderTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            if (cell) {
                
                TextInputHelperViewController *viewController = [[TextInputHelperViewController alloc] init];
                viewController.tag = cell.textField.tag;
                viewController.title = cell.titleLabel.text;
                viewController.text = cell.textField.text;
                WeakSelf
                viewController.completeHandle = ^(NSInteger tag, NSString *text) {
                    weakSelf.httpMessage.itinerary = text.trim;
                    [weakSelf.tableView reloadData];
                };
                
                [self.navigationController pushViewController:viewController animated:YES];
                
            }
            
            
        }
        
        if (indexPath.row == 1) {
            
            [self setReleaseTime:cell.textField];
            
        }
        
    }
    
    if (indexPath.section == 5) {
        
        
        
    }
    
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}


@end
