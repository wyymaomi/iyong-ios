//
//  PostContentTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "PostContentTableViewCell.h"
#import "UITextView+MaxLength.h"


@implementation PostContentTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"PostContentTableViewCell";
    PostContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
//        cell.textView = [[UIPlaceHolderTextView alloc] initWithFrame:CGRectMake(22, 43, ViewWidth-44, 130*Scale)];
//        
//        cell.textView.placeholder = @"是不是想说点什么";
//        
//        cell.textView.borderColor = [UIColor lightGrayColor];
//        
//        cell.textView.borderWidth = 1;
//        
//        cell.textView.cornerRadius = 5;
        
        cell.textView.maxLength = TextViewMaxLength;
        
//        [cell.contentView addSubview:cell.textView];
        
//        cell.textView = 
        
    }
    return cell;
}



- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.headerView.frame = CGRectMake(0, 0, ViewWidth, 48);
    
    self.textView.top = self.headerView.bottom;
}

- (HeaderView*)headerView
{
    if (_headerView == nil) {
        _headerView = [HeaderView customView];
        _headerView.titleLabel.text = @"评论上传";
        [self.contentView addSubview:_headerView];
    }
    return _headerView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
