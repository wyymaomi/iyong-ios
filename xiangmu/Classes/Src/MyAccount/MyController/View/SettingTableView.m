//
//  SettingTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SettingTableView.h"
#import "MyTableViewCell.h"
#import "AppConfig.h"

@interface SettingTableView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UISwitch *pushDisplaySwitch;

@end

@implementation SettingTableView

- (NSArray*)cellList
{
    if (!_cellList) {
        NSInteger userLevel = [[UserManager sharedInstance] userLevel];
//        if (userLevel == 1) {
//            _cellTitleList = @[@[@"银行卡", @"密码管理"], @[@"调度员管理"], @[@"退出登录"]];
//        }
//        else {
//            _cellTitleList = @[@[@"密码管理"], @[@"退出登录"]];
//        }
        _cellList = @[@[@{@"title": @"设置登录密码", @"vc": @""},
                        @{@"title": @"设置手势密码", @"vc": @""},
                        @{@"title": @"设置交易密码", @"vc": @""}],
                      @[@{@"title":@"消息提示",@"vc":@""}],
                      @[@{@"title":@"退出登录",@"vc":@""}]];
//        if (userLevel == 1) {
        
//            _cellList = @[@[@{@"title":@"银行卡",@"vc":@"BankcardViewController"},
//                            @{@"title":@"密码管理", @"vc":@"PasswordManagerTableViewController"}],
//                          @[@{@"title":@"退出登录",@"vc":@""}]];
//        }
//        else {
        
//        }
    }
    return _cellList;
}

- (void)setup {
    
    [super setup];
    
    self.delegate = self;
    self.dataSource = self;
    
    self.tableFooterView = [[UIView alloc] init];
    self.rowHeight = DefaultTableRowHeight;
    
}

- (UISwitch *)pushDisplaySwitch
{
    if (_pushDisplaySwitch == nil) {
        _pushDisplaySwitch = [[UISwitch alloc] init];
        [_pushDisplaySwitch addTarget:self action:@selector(pushDisplayChanged:) forControlEvents:UIControlEventValueChanged];
    }
    
    return _pushDisplaySwitch;
}

- (void)pushDisplayChanged:(UISwitch *)pushDisplaySwitch
{
    if (pushDisplaySwitch.isOn) {
        [AppConfig currentConfig].isSoundOn = [NSNumber numberWithBool:YES];
//#warning Set the nickname for the details，such as _nickName = @"环信";
//        _pushDisplayStyle = EMPushDisplayStyleMessageSummary;
    }
    else{
        [AppConfig currentConfig].isSoundOn = [NSNumber numberWithBool:NO];
//        _pushDisplayStyle = EMPushDisplayStyleSimpleBanner;
    }
}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.cellList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *rowList = self.cellList[section];
    return rowList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0001f;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellId";
    
    MyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    
    if (!cell) {
        
        cell = [[MyTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        if (indexPath.section == 1 && indexPath.row == 0) {
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            self.pushDisplaySwitch.frame = CGRectMake(ViewWidth - self.pushDisplaySwitch.frame.size.width - 10, (DefaultTableRowHeight - self.pushDisplaySwitch.frame.size.height) / 2, self.pushDisplaySwitch.frame.size.width, self.pushDisplaySwitch.frame.size.height);
            [cell.contentView addSubview:self.pushDisplaySwitch];
            
            BOOL isSoundOn = [[AppConfig currentConfig].isSoundOn boolValue];
            
            [self.pushDisplaySwitch setOn:isSoundOn];
            
        }
        else if (indexPath.section == self.cellList.count - 1) {
            
        }
        
    }
    
    NSInteger section = indexPath.section;
    
    NSArray *rowList = self.cellList[section];
    
    NSDictionary *dict = rowList[indexPath.row];
    
    cell.name.text = dict[@"title"];
    
    // 如果是最后一行  文字居中
    if (section == self.cellList.count - 1) {

        cell.name.hidden = YES;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, tableView.rowHeight)];
        label.text = cell.name.text;
        label.font = cell.name.font;
        label.textColor = cell.name.textColor;
        label.textAlignment = UITextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:label];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    return cell;
    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.settingDelegate && [self.settingDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
        [self.settingDelegate didSelectRowAtIndexPath:indexPath];
    }
    
    
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

@end
