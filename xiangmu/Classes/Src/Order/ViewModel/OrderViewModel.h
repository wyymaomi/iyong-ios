//
//  OrderViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewModel.h"
#import "NewOrderHttpMessage.h"

@interface OrderViewModel : BaseViewModel

+(BOOL)validateNewOrder:(NewOrderHttpMessage*)orderHttpMessage error:(NSString**)errorMsg;

@end
