//
//  LoginViewController.m
//  xiangmu
//
//  Created by David kim on 16/4/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "MyUtil.h"
#import "RegisterViewController.h"
#import "ForgetPasswordViewController.h"
#import "GesturePasswordController.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "SaveAndTake.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UserDefaults.h"
#import "UserManager.h"
#import "HttpConstants.h"
#import "UserHelper.h"

#import "LoginView.h"
#import "LoginViewController+NetworkRequest.h"

@interface LoginViewController ()<UITextFieldDelegate>


@end

@implementation LoginViewController

//- (QQLoginHttpMessage*)qqLoginHttpMessage
//{
//    if (!_qqLoginHttpMessage) {
//        _qqLoginHttpMessage = [QQLoginHttpMessage new];
//    }
//    return _qqLoginHttpMessage;
//}
//
//- (WechatLoginHttpMessage*)wechatLoginHttpMessage
//{
//    if (!_wechatLoginHttpMessage) {
//        _wechatLoginHttpMessage = [WechatLoginHttpMessage new];
//    }
//    return _wechatLoginHttpMessage;
//}
//
//-(WeiboLoginHttpMessage*)weiboLoginHttpMessage
//{
//    if (!_weiboLoginHttpMessage) {
//        _weiboLoginHttpMessage = [WeiboLoginHttpMessage new];
//    }
//    return _weiboLoginHttpMessage;
//}

-(ThirdpartyLoginHttpMessage*)thirdPartyLoginHttpMessage
{
    if (!_thirdPartyLoginHttpMessage) {
        _thirdPartyLoginHttpMessage = [ThirdpartyLoginHttpMessage new];
    }
    return _thirdPartyLoginHttpMessage;
}


-(id)init
{
    if (self = [super init]) {
        
        self.title = @"登录";
        
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    bPopupLoginView = NO;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    bPopupLoginView = YES;
    
     [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _loginView = [LoginView customView];
    [self.view addSubview:_loginView];
    
    // Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
    [self.loginView.closeButton addTarget:self action:@selector(onClickCloseButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.loginView.qqLoginImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getAuthWithUserInfoFromQQ)]];
    [self.loginView.wechatLoginImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getAuthWithUserInfoFromWechat)]];
    [self.loginView.weiboLoginImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getAuthWithUserInfoFromSina)]];
    
    self.userNameTextField = self.loginView.accountTextField;
    self.userPasswordTextField = self.loginView.pwdTextField;
//    self.loginBu
    [self.loginView.loginButton addTarget:self action:@selector(doLogin) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.registerButton addTarget:self action:@selector(gotoRegister) forControlEvents:UIControlEventTouchUpInside];
    
    [self.loginView.forgetPwdButton addTarget:self action:@selector(gotoReset) forControlEvents:UIControlEventTouchUpInside];
    
}


- (void)onClickCloseButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;//隐藏为YES，显示为NO
}

//- (void) viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
//}
//- (void) viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
//}


-(void)backFromGesturePwd
{
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)createInterface
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth/3+5, ViewHeight/5-70, ViewHeight/6-15 ,ViewHeight/6-15)];
    imageView.image=[UIImage imageNamed:@"登录"];
    
    UIImageView *fuimageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth/3, ViewHeight/5-70+ViewHeight/6, ViewHeight/6 ,20)];
    fuimageView.image=[UIImage imageNamed:@"登录副标题"];
//    [self.scrollview addSubview:fuimageView];
//    [self.scrollview addSubview:imageView];
}
-(void)createTextfield
{
    //    self.scrollview.frame=CGRectMake(0, 0, ViewWidth, ViewHeight);
    self.scrollview.backgroundColor=[UIColor whiteColor];
    
//    [self.view addSubview:self.scrollview];
    
    UITextField *phoneTextfield=[[UITextField alloc]initWithFrame:CGRectMake(20, ViewHeight/5*2, ViewWidth-40, ViewHeight/14)];
    phoneTextfield.borderStyle=UITextBorderStyleRoundedRect;
    phoneTextfield.layer.cornerRadius=20.0f;
    phoneTextfield.layer.borderWidth=1.0f;
    phoneTextfield.font=[UIFont systemFontOfSize:13];
    phoneTextfield.tag=100;
    //phoneTextfield.textAlignment=UITextAlignmentLeft;
    //数字键盘
    phoneTextfield.keyboardType=UIKeyboardTypeNumberPad;
    phoneTextfield.layer.borderColor=[[UIColor colorWithRed:190/255.0f green:203/255.0f blue:220/255.0f alpha:1.0f] CGColor];
    phoneTextfield.placeholder=@"请输入手机号";
    
    UIImageView *leftView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"输入框图标"]];
    leftView.frame=CGRectMake(10, 10, 23*2, 38/4);
    leftView.contentMode=UIViewContentModeCenter;
    phoneTextfield.leftView=leftView;
    phoneTextfield.leftViewMode=UITextFieldViewModeAlways;
    self.userNameTextField = phoneTextfield;
//    [self.scrollview addSubview:phoneTextfield];
    
    UITextField *password=[[UITextField alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(phoneTextfield.frame)+20, ViewWidth-40, ViewHeight/14)];
    password.borderStyle=UITextBorderStyleRoundedRect;
    password.layer.cornerRadius=20.0f;
    password.layer.borderWidth=1.0f;
    password.tag=101;
    // password.textAlignment=UITextAlignmentCenter;
    password.font=[UIFont systemFontOfSize:13];
    password.layer.borderColor=[[UIColor colorWithRed:190/255.0f green:203/255.0f blue:220/255.0f alpha:1.0f] CGColor];
    password.secureTextEntry=YES;
    password.placeholder=@"请输入密码";
    
    UIImageView *leftView1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"输入框图标-1"]];
    leftView1.frame=CGRectMake(10, 10, 23*2, 38/4);;
    leftView1.contentMode=UIViewContentModeCenter;
    password.leftView=leftView1;
    password.leftViewMode=UITextFieldViewModeAlways;
    self.userPasswordTextField = password;
//    [self.scrollview addSubview:password];
    
    UIImageView *lineView=[[UIImageView alloc]initWithFrame:CGRectMake(43, 0, 2, ViewHeight/14)];
    lineView.image=[UIImage imageNamed:@"渐变"];
    [password addSubview:lineView];
    
    UIImageView *lineView1=[[UIImageView alloc]initWithFrame:CGRectMake(43, 0, 2, ViewHeight/14)];
    lineView1.image=[UIImage imageNamed:@"渐变"];
    [phoneTextfield addSubview:lineView1];
    
    UIButton *btn=[MyUtil createBtnFrame:CGRectMake(20, CGRectGetMaxY(password.frame)+40, ViewWidth-40, ViewHeight/15) title:@"登  录" bgImageName:nil target:self action:@selector(doLogin)];
    btn.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:150.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
    btn.layer.cornerRadius=18.0f;
    [btn setTitle:@"登  录" forState:UIControlStateNormal];
//    [self.scrollview addSubview:btn];
    
    UIButton *btn1=[MyUtil createBtnFrame:CGRectMake(20, ViewHeight/5*4, 40, 20) title:@"注册" bgImageName:nil target:self action:@selector(gotoRegister)];
    btn1.titleLabel.font=[UIFont systemFontOfSize:16];
    [btn1 setTitleColor:[UIColor colorWithRed:71.0f/255.0f green:107.0f/255.0f blue:154.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    UIButton *btn2=[MyUtil createBtnFrame:CGRectMake(ViewWidth-100, ViewHeight/5*4, 80, 20) title:@"忘记密码" bgImageName:nil target:self action:@selector(gotoReset)];
    btn2.titleLabel.font=[UIFont systemFontOfSize:16];
    [btn2 setTitleColor:[UIColor colorWithRed:71.0f/255.0f green:107.0f/255.0f blue:154.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
//    [self.scrollview addSubview:btn1];
//    [self.scrollview addSubview:btn2];
    
//    UIButton *sinaLoginButton = [MyUtil createBtnFrame:CGRectMake(20, 0, 40, 20) title:@"新浪登录" bgImageName:nil target:self action:@sel]
    
    UIButton *sinaLoginButton = [MyUtil createBtnFrame:CGRectMake(20, 10, 100, 30) title:@"新浪登录" bgImageName:nil target:self action:@selector(getAuthWithUserInfoFromSina)];
    sinaLoginButton.titleLabel.font = FONT(16);
    [sinaLoginButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [self.scrollview addSubview:sinaLoginButton];
    
    UIButton *qqLoginButton = [MyUtil createBtnFrame:CGRectMake(120, 10, 100, 30) title:@"QQ登陆" bgImageName:nil target:self action:@selector(getAuthWithUserInfoFromQQ)];
    qqLoginButton.titleLabel.font = FONT(16);
    [qqLoginButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [self.scrollview addSubview:qqLoginButton];
    
    UIButton *wechatButton = [MyUtil createBtnFrame:CGRectMake(220, 10, 100, 30) title:@"微信登录" bgImageName:nil target:self action:@selector(getAuthWithUserInfoFromWechat)];
    wechatButton.titleLabel.font = FONT(16);
    [wechatButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [self.scrollview addSubview:wechatButton];
}

- (void)gotoThirdpartyRegister:(ThirdpartyLoginHttpMessage*)thirdPartyLoginHttpMessage
{
    RegisterViewController *viewController = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    viewController.delegate = self;
    viewController.enter_type = ENTER_TYPE_PRESENT;
    viewController.registerType = RegisterTypeThirdparty;
    viewController.thirdPartyLoginHttpMessage = thirdPartyLoginHttpMessage;
    viewController.thirdPartyId = self.thirdPartyId;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:nav animated:YES completion:nil];
}


-(void)gotoRegister
{
    RegisterViewController *viewController = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    viewController.delegate=self;
    viewController.enter_type = ENTER_TYPE_PRESENT;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:nav animated:YES completion:nil];
    
    //    [self.navigationController pushViewController:registerVC animated:YES];
    //    UITabBarController *tabbar = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    //    tabbar.tabBar.hidden = YES;
    //    UINavigationController *nav = tabbar.viewControllers[0];
    //    [nav pushViewController:registerVC animated:YES];
    
}
-(void)writeUsername:(NSString *)username WithPassword:(NSString *)password
{
    UITextField *textfield=[self.view viewWithTag:100];
    textfield.text=username;
    paword=password;
}

-(void)gotoReset
{
    ForgetPasswordViewController *cpasswordVC=[[ForgetPasswordViewController alloc] initWithNibName:@"ForgetPasswordViewController" bundle:nil];
    cpasswordVC.type=Changepassword;
    cpasswordVC.enter_type = ENTER_TYPE_PRESENT;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cpasswordVC];
    [self presentViewController:nav animated:YES completion:nil];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


//-(BOOL)checkGesturePassword
//{
//    return IsStrEmpty([[UserDefaults sharedInstance] objectForKey:KEY_GESTURE_PWD]);
//}

//-(void)checkGesturePasswordFromServer
//{
//    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, HAS_GESTURE_PWD_API];
//    WeakSelf
//    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary *responseData) {
//        StrongSelf
//        [strongSelf hideHUDIndicatorViewAtCenter];
//        if (![responseData isKindOfClass:[NSDictionary class]]) {
//            return;
//        }
//        
//        id codeStatus = responseData[@"code"];
//        if (IsNilOrNull(codeStatus)) {
//            return;
//        }
//        
//        NSInteger code = [responseData[@"code"] integerValue];
//        if (code == STATUS_OK) {
//            NSString *data = responseData[@"data"];
//            if ([data integerValue] == 1) {
//                [strongSelf presentGestureSetting:SOURCE_FROM_LOGIN];
//            }
//            else {
//                [strongSelf dismissViewControllerAnimated:YES completion:nil];
//                //                [self presentGestureSetting:SOURCE_FROM_LOGIN];
//            }
//        }
//        else {
//            [strongSelf dismissViewControllerAnimated:YES completion:nil];
//            //            [self presentGestureSetting:SOURCE_FROM_LOGIN];
//        }
//        
//    } andFailure:^(NSString *errorDesc) {
//        [weakSelf hideHUDIndicatorViewAtCenter];
//        [weakSelf showAlertView:errorDesc];
//        
//    }];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

