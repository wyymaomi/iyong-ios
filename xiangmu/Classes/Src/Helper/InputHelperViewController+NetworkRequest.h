//
//  InputHelperViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InputHelperViewController.h"

@interface InputHelperViewController (NetworkRequest)

-(void)updateUserInfo:(NSInteger)tag text:(NSString*)text;

@end
