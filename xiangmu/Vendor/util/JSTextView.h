//
//  JSTextView.h
//  xiangmu
//
//  Created by David kim on 16/4/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSTextView : UITextView
@property(nonatomic,copy)NSString *myPlaceholder;//文字
@property(nonatomic,copy)UIColor *myPlaceholderColor;//文字颜色
@end
