//
//  CarTableViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CarModel.h"

@protocol CarTableViewControllDelegate <NSObject>


-(void)onGetCarData:(CarModel*)carModel;
@optional
-(void)onGetVechileData:(NSDictionary*)carVo;// carImageData:(NSData*)carImageData;
//-(void)onGetVechileData:(NSDictionary*)carVo carData:(CarModel*)carData;

@end

typedef NS_ENUM(NSInteger, VechileListType)
{
    
    VechileListTypeAll = 0,
    VechileListTypecertified
    
};



@interface CarTableViewController : BaseViewController

@property (nonatomic, weak) id<CarTableViewControllDelegate> carDelegate;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, assign) NSInteger listType; // 列表类型

@end
