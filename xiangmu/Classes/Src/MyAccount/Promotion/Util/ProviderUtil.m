//
//  ProviderUtil.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderUtil.h"
#import "UserManager.h"
#import "StringUtil.h"

@implementation ProviderUtil

+(NSString*)displayAdvTitle:(NSUInteger)advType;
{
    if (advType == AdvTypeBanner) {
        return @"顶部广告专区";
    }
    else if (advType == AdvTypeOnlineProvider) {
        return @"全国商户广告专区";
    }
    else if (advType == AdvTypeLocalArea) {
        NSString *areaName = [StringUtil getSafeString:[UserManager sharedInstance].userModel.areaName];
        return [NSString stringWithFormat:@"%@地区广告专区", areaName];
        
//        return [NSString stringWithFormat:@"%@地区广告专区", [StringUtil getSafeString:[UserManager sharedIn]]]
//        return @"专属地区广告专区";
//        return [NSString stringWithFormat:@"%@地区广告专区", [StringUtil getSafe][UserManager sharedInstance].userModel.areaName];
    }
    return nil;
}

+(NSString*)displayAdOrderStatusText:(NSUInteger)status;
{
    switch (status) {
        case AdOrderStatusWaitPay:
            return @"待支付";
        case AdOrderStatusPaySuccess:
            return @"支付成功";
        case AdOrderStatusPayFailure:
            return @"支付失败";
        case AdOrderStatusExpire:
            return @"已过期";
        case AdOrderStatusCancelled:
            return @"已取消";
        case AdOrderStatusPendingRefund:
            return @"待退款";
        case AdOrderStatusRefunded:
            return @"已退款";
            
        default:
            break;
    }
    
    return nil;
}

+(NSString*)getAdOrderDate:(NSString*)timemillions;
{

    NSString *dateStr = [NSDate getDateAccordingTime:timemillions formatStyle:@"MM/dd"];
    return dateStr;

}

+(NSString*)getAdOrderTimeSegment:(NSString*)startTime endTime:(NSString*)endTime;
{
    NSString *startDateTime = [NSDate getDateAccordingTime:startTime formatStyle:@"HH:mm"];
    NSString *endDateTime = [NSDate getDateAccordingTime:endTime formatStyle:@"HH:mm"];
    return [NSString stringWithFormat:@"%@/%@", startDateTime,endDateTime];
}

@end
