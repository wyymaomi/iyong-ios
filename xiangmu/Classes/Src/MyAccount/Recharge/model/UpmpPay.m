//
//  UpmpPay.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UpmpPay.h"
#import <IPSUPMPSDK/IPSUpmpViewController.h>

@implementation UpmpPay

- (NSString*)generateOrderString:(NSDictionary*)dict;
{
    return nil;
}

@end

@implementation UpmppayOrder

- (NSString*)generateOrderString:(NSDictionary*)dict;
{
    NSString *transAmount = dict[kRechargeAmount];
    NSString *bankcardNo = dict[kBankcardNo];
    
    UpmpPay *upmpPay = [self getUpmpData:nil tranAmt:transAmount bankcardNo:bankcardNo];
    
    return upmpPay.description;
}

-(UpmpPay*)getUpmpData:(NSString*)billNo tranAmt:(NSString*)tranAmt bankcardNo:(NSString*)bankcardNo
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *dateStr = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:date]];
    
    UpmpPay *upmpPay = [[UpmpPay alloc] init];
    upmpPay.merCode = UPMP_MER_CODE;
    upmpPay.accCode = UPMP_ACC_CODE;
    upmpPay.merBillNo = billNo;
    upmpPay.tranAmt = tranAmt;
    upmpPay.ordPerVal = @"1";
    upmpPay.requestTime = dateStr;
    upmpPay.orderDesc = @"recharge";
    upmpPay.bankCard = bankcardNo;
//    upmpPay.merNoticeUrl = UPMP_NOTICE_URL;
    upmpPay.ccyCode = @"156";
    upmpPay.prdCode = @"2301";
    
    return upmpPay;
}

-(NSString*)generateSignString:(UpmpPay*)upmpPay merRequestDes:(NSString**)merRequestDes
{
    NSString* bankCardStr = [IPSUpmpViewController TripleDES:upmpPay.bankCard encryptOrDecrypt:kCCEncrypt key:UPMP_DES_KEY desIv:UPMP_DES_IV];
    
    NSMutableDictionary* merRequestDict = [[NSMutableDictionary alloc]init];
    [merRequestDict setValue:upmpPay.accCode forKey:@"accCode"];
    [merRequestDict setValue:upmpPay.merBillNo forKey:@"merBillNo"];
    [merRequestDict setValue:upmpPay.ccyCode forKey:@"ccyCode"];
    [merRequestDict setValue:upmpPay.prdCode forKey:@"prdCode"];
    [merRequestDict setValue:upmpPay.tranAmt forKey:@"tranAmt"];
    [merRequestDict setValue:upmpPay.requestTime forKey:@"requestTime"];
    [merRequestDict setValue:upmpPay.ordPerVal forKey:@"ordPerVal"];
    [merRequestDict setValue:upmpPay.merNoticeUrl forKey:@"merNoticeUrl"];
    [merRequestDict setValue:upmpPay.orderDesc forKey:@"orderDesc"];
    [merRequestDict setValue:bankCardStr forKey:@"bankCard"];
    
    DLog(@"merRequestDict:%@",merRequestDict);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:merRequestDict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *requestJason =[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    DLog(@"requestJason=%@", requestJason);
    /*NSString* */*merRequestDes = [IPSUpmpViewController TripleDES:requestJason encryptOrDecrypt:kCCEncrypt key:UPMP_DES_KEY desIv:UPMP_DES_IV];
    DLog(@"加密数据:%@",*merRequestDes);
    
    NSString* merRequest = [NSString stringWithFormat:@"%@%@%@",UPMP_MER_CODE,*merRequestDes,UPMP_MD5_CERT];
    DLog(@"md5字段：%@",merRequest);
    
    NSString* merRequestMd5 = [IPSUpmpViewController md5:merRequest];
    DLog(@"md5:%@",merRequestMd5);
    
    return merRequestMd5;
}

@end
