//
//  AdvPurchaseTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/4/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPurchaseTableViewCell.h"


@implementation AdvPurchaseTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _headerView = [HeaderView customView];
//        _headerView.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:_headerView];
        
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(130*Scale, 130*Scale);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        self.collectionView = [[AdvPurchaseCollectionView alloc] initWithFrame:CGRectMake(12*Scale, 40/**Scale*/, ViewWidth-12*Scale, 130*Scale) collectionViewLayout:flowLayout];
        self.collectionView.scrollEnabled = YES;
        self.collectionView.backgroundColor = [UIColor whiteColor];
        self.collectionView.showsHorizontalScrollIndicator = NO;
        
        [self.contentView addSubview:self.collectionView];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.headerView.frame = CGRectMake(0, 20*Scale, ViewWidth, 48);
    
    self.collectionView.top = self.headerView.bottom;
    
}

- (HeaderView*)headerView
{
    if (_headerView == nil) {
        _headerView = [HeaderView customView];
        [self.contentView addSubview:_headerView];
    }
    return _headerView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
