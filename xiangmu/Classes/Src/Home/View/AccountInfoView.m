//
//  AccountInfoView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AccountInfoView.h"


@interface AccountInfoView ()<EnterpriseUserCollectionViewDelegate>







@end

//static CGFloat accountInfoViewWidth = ViewWidth - ViewWidth/3

@implementation AccountInfoView

//- (NSArray *)subviewCellList
//{
//    if (IsNilOrNull(_subviewCellList)) {
//        // 加载plist中的字典数组
//        NSString *path = [[NSBundle mainBundle] pathForResource:HOME_CELL_PLIST ofType:nil];
//        _subviewCellList = [NSArray arrayWithContentsOfFile:path];
//    }
//    return _subviewCellList;
//}

- (EnterpriseUserinfoView*)enterpriseUserinfoView
{
    if (_enterpriseUserinfoView == nil) {
        _enterpriseUserinfoView = [EnterpriseUserinfoView customView];
        _enterpriseUserinfoView.frame = CGRectMake(0, self.bottomButton.bottom, ViewWidth, 245*Scale);
        [_enterpriseUserinfoView.companyPublishButton addTarget:self action:@selector(onClickCompanyPublish:) forControlEvents:UIControlEventTouchUpInside];
        [_enterpriseUserinfoView.providerInfoButton addTarget:self action:@selector(onClickProviderInfoButton:) forControlEvents:UIControlEventTouchUpInside];
//        _enterpriseUserinfoView.providerInfoButton addTarget:self action:@selector(onClickProviderInfo) forControlEvents:<#(UIControlEvents)#>
        [_enterpriseUserinfoView.providerPromotionButton addTarget:self action:@selector(onClickProviderPromotionButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _enterpriseUserinfoView;
}

- (EnterpriseUserCollectionView*)collectionView
{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *flowlayout=[[UICollectionViewFlowLayout alloc]init];
//        [flowlayout setItemSize:CGSizeMake((ViewWidth-(ViewWidth/3)-2)/3, 78)];
        [flowlayout setItemSize:CGSizeMake((ViewWidth-90*Scale-2)/3, 84*Scale)];
        [flowlayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        flowlayout.minimumLineSpacing = 0.0f;
        flowlayout.minimumInteritemSpacing = 0.0f;
        //flowlayout.sectionInset=UIEdgeInsetsMake(0, 0, 0, 0);
//        flowlayout.minimumInteritemSpacing = 0.5f ;//最小行间距,(当垂直布局时是行间距,当水平布局时是列间距)
//        flowlayout.minimumLineSpacing = 0.5f ;//两个单元格之间的最小间距
        
        _collectionView = [[EnterpriseUserCollectionView alloc] initWithFrame:CGRectMake(45*Scale, self.titleLabel.bottom+15*Scale, ViewWidth-90*Scale, 84*Scale*4) collectionViewLayout:flowlayout];
//        _collectionView = [[EnterpriseUserCollectionView alloc]initWithFrame:CGRectMake(45*Scale, self.titleLabel.bottom + 15*Scale, ViewWidth-(ViewWidth/3), 78*4) collectionViewLayout:flowlayout];
        _collectionView.backgroundColor = [UIColor clearColor];//[UIColor clearColor];
        _collectionView.customDelegate = self;

    }
    
    return _collectionView;
}

- (UIButton*)bottomButton
{
    if (!_bottomButton) {
        _bottomButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _bottomButton.frame = CGRectMake(0, 0, ViewWidth, 50*Scale);
        [_bottomButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateNormal];
        [_bottomButton setTitle:@"切换企业模式" forState:UIControlStateNormal];
        [_bottomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _bottomButton.titleLabel.font = FONT(20);
    }
    return _bottomButton;
}

- (UIButton*)collapseButton
{
    if (!_collapseButton) {
        
        _collapseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _collapseButton.frame = CGRectMake((ViewWidth-(ViewWidth/3)-20)/2,12, 20, 20);
        [_collapseButton setBackgroundImage:[UIImage imageNamed:@"icon_up_arrow"] forState:UIControlStateNormal];
        
    }
    
    return _collapseButton;
}

-(UILabel*)titleLabel
{
    if (!_titleLabel) {
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.collapseButton.bottom + 10, ViewWidth-(ViewWidth/3), 20)];
        _titleLabel.text = @"企业用户";
        _titleLabel.textColor = [UIColor darkGrayColor];
        _titleLabel.textAlignment = UITextAlignmentCenter;
        _titleLabel.font = FONT(12);
        _titleLabel.backgroundColor = [UIColor clearColor];
    }
    
    return _titleLabel;
}

-(UIView*)subBgView
{
    if (!_subBgView) {
        _subBgView = ({
            UIView *view = [[UIView alloc] initWithFrame: CGRectMake(0, ViewHeight-78-72, /*ViewWidth-(ViewWidth/3)*/ViewWidth, 4 * 78 + 100)];
            view.backgroundColor = [UIColor whiteColor];//UIColorFromRGB(0xF7F7F7);
            view.clipsToBounds = YES;
            view;
            
        });
        
//        _collapseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        
    }
    return _subBgView;
}

- (AccountInfoTableView*)accountTableView
{
    if (_accountTableView == nil) {
        _accountTableView = [[AccountInfoTableView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, self.frame.size.height) style:UITableViewStylePlain];
//        _accountTableView = [[AccountInfoTableView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth - (ViewWidth/3), ViewHeight) style:UITableViewStylePlain];
    }
    return _accountTableView;
}

- (UIWindow *)maskWindow
{
    if (!_maskWindow) {
        
        _maskWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        //edited by gjf 修改alertview leavel
        _maskWindow.windowLevel = UIWindowLevelStatusBar + BBAlertLeavel;
        _maskWindow.backgroundColor = [UIColor clearColor];
        _maskWindow.hidden = YES;
        
        UIView *maskView = ({
            
            UIView *view = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
            view.backgroundColor = [UIColor clearColor];
//            view.alpha = 0.9;
            view;
        });
        [_maskWindow addSubview:maskView];
        _maskView = maskView;
        
        
        _bgView = ({
            
            UIView *view =[[UIView alloc]initWithFrame: CGRectMake(ViewWidth/3, 0, ViewWidth-(ViewWidth/3), ViewHeight)];
            view.backgroundColor = UIColorFromRGB(0xF7F7F7);
            view.clipsToBounds = YES;
            view.layer.borderWidth=1;
            view;
            
        });
        
        [_maskWindow addSubview:_bgView];
        
    }
    return _maskWindow;
}


#pragma mark - life cycle

-(id)init
{
    self = [super init];
    
    if (self) {
        _isNeedAnimation = YES;
        _isVisible = NO;
//        _isBottom = YES;
    }
    
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self addSubview:self.accountTableView];
        
#if 0 // 我的界面改版
        [self addSubview:self.subBgView];
        [self.subBgView addSubview:self.bottomButton];
        [self.subBgView addSubview:self.enterpriseUserinfoView];
        
        self.subBgView.top = ViewHeight-220;//self.frame.size.height-50*Scale;//0;//ViewHeight-78-72;
#endif

        
    }
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.accountTableView.frame = CGRectMake(0, 0, ViewWidth, self.frame.size.height);
    
}


- (void)initViews
{
    [self addSubview:self.accountTableView];
    
}

- (void)show
{
//    if (_isVisible) {
//        return;
//    }
    
    _isVisible = YES;
    
    [self.maskWindow makeKeyAndVisible];
    
    _maskView.userInteractionEnabled = YES;
    UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDismiss:)];
//    sigleTapRecognizer.numberOfTapsRequired = 1;
    [_maskView addGestureRecognizer:sigleTapRecognizer];
    
    [_bgView addSubview:self.accountTableView];
    
    [self.subBgView addSubview:self.collapseButton];
//    self.collapseButton.top = ViewHeight-78-40;
    [self.subBgView addSubview:self.titleLabel];
    [self.subBgView addSubview:self.collectionView];
    
    [_bgView addSubview:_subBgView];
    
    _bgView.left = ViewWidth;
    
//    popupView.frame = popupStartRect;
//    popupView.alpha = 1.0f;
    if (self.isNeedAnimation) {
        WeakSelf
        [UIView animateWithDuration:0.35 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            weakSelf.bgView.left = ViewWidth/3;
        } completion:^(BOOL finished) {
            //        [self.mj_popupViewController viewDidAppear:NO];
        }];
    }
    else {
        self.bgView.left = ViewWidth/3;
    }

    
}

- (void)dismiss
{
    
    if (!_isVisible) {
        return;
    }
    
    [_subBgView removeAllSubviews];
    
    [_maskView removeFromSuperview];
    [_bgView removeFromSuperview];
    _maskWindow = nil;
}


//- (void)show:(NSArray*)images borderColor:(UIColor*)color
//{
//    [self.maskWindow makeKeyAndVisible];
//    
//    _maskView.userInteractionEnabled = YES;
//    UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDismiss:)];
//    sigleTapRecognizer.numberOfTapsRequired = 1;
//    [_maskView addGestureRecognizer:sigleTapRecognizer];
//}

- (void)onDismiss:(id)sender
{

    if (!_isVisible) {
        return;
    }
    
    [_collectionView removeAllSubviews];
    [_collectionView removeFromSuperview];
    _collectionView = nil;
    
    [self.subBgView removeAllSubviews];
    [_maskView removeFromSuperview];
    [_bgView removeFromSuperview];
    _maskWindow = nil;
}

-(void)refreshOrderCount:(NSInteger)orderCount
{
    self.accountTableView.orderCount = orderCount;
    [self.accountTableView reloadData];
    self.collectionView.orderCount = orderCount;
    [self.collectionView reloadData];
}

- (void)refreshPartnerNumber:(NSInteger)partnerNumber
{
    self.collectionView.applyPartnerBadgeNumber = partnerNumber;
    [self.collectionView reloadData];
}

- (void)onGotoPage:(NSString*)vcPage title:(NSString*)title;
{
    if (_delegate && [_delegate respondsToSelector:@selector(onGotoPage:title:)]) {
        [_delegate onGotoPage:vcPage title:title];
    }
}

- (void)onClickCompanyPublish:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(openCompanyPublish)]) {
        [_delegate openCompanyPublish];
    }
}

- (void)onClickProviderPromotionButton:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(openAdvPurchase)]) {
        [_delegate openAdvPurchase];
    }
}

- (void)onClickProviderInfoButton:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(openProviderInfo)]) {
        [_delegate openProviderInfo];
    }
}



@end
