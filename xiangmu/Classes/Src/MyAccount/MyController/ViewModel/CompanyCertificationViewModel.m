//
//  CompanyCertificationViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CompanyCertificationViewModel.h"
#import "NetworkManager.h"
#import "UserManager.h"

@implementation CompanyCertificationViewModel

- (BOOL)canEditAreaName
{
    NSInteger status = [[UserManager sharedInstance].userModel.companyCertification integerValue];
    if (status == CompanyCertificationStatusInit ||
        status == CompanyCertificationStatusRejected) {
        return YES;
    }
    return NO;
}

- (BOOL)canEditPhoto
{
    NSInteger status = [[UserManager sharedInstance].userModel.companyCertification integerValue];
    if (status == CompanyCertificationStatusInit ||
        status == CompanyCertificationStatusRejected) {
        return YES;
    }
    return NO;
}

- (BOOL)canEditCompanyName
{
    NSInteger status = [[UserManager sharedInstance].userModel.companyCertification integerValue];
    if (status == CompanyCertificationStatusInit ||
        status == CompanyCertificationStatusRejected) {
        return YES;
    }
    return NO;
}

- (BOOL) canEditCompanyAddress
{
    NSInteger status = [[UserManager sharedInstance].userModel.companyCertification integerValue];
    if (status == CompanyCertificationStatusInit ||
        status == CompanyCertificationStatusFinished ||
        status == CompanyCertificationStatusRejected)
    {
        return YES;
    }
    return NO;
}



-(id)init
{
    self = [super init];
    
    if (self = [super init]) {
        
    }
    
    return self;
}

- (void)getCompanyCertificationInfo:(SuccessBlock)success failure:(FailureBlock)failure
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_COMPANY_CERTIFICATION_API];
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(id responseData) {
        if (success) {
            success(responseData);
        }
        
    } andFailure:^(NSString *errorDesc) {
        if (failure) {
            failure(errorDesc);
        }
        
    }];
}

- (BOOL)validateCompanyCertificationInfo:(CompanyCertifcationHttpMessage*)httpMessage errorMsg:(NSString**)errorMsg;
{
//    if (IsStrEmpty(httpMessage.fileContent)) {
//        *errorMsg = @"请上传营业执照";
//        return NO;
//    }
    
    if (IsStrEmpty(httpMessage.areaName) || IsStrEmpty(httpMessage.areaCode)) {
        *errorMsg = @"请输入所在地区";
        return NO;
    }
    
//    if (IsStrEmpty(httpMessage.areaName) || IsStrEmpty(httpMessage.areaCode)) {
//        *errorMsg = @"请输入所在地区";
//        return NO;
//    }
    if (IsStrEmpty(httpMessage.name)) {
        *errorMsg = @"请输入公司名称";
        return NO;
    }
    
    
    if (IsStrEmpty(httpMessage.address)) {
        *errorMsg = @"请输入企业经营地址";
        return NO;
    }
    
    return YES;
}

@end
