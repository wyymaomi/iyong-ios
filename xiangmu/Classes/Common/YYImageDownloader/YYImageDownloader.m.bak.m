//
//  YYImageDownloader.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "YYImageDownloader.h"
#import "AliyunOSSManager.h"

@interface YYImageDownloader()

@property (strong, nonatomic) NSOperationQueue *downloadQueue;
//@property (strong, nonatomic) NSMutableArray *errorUrl;

@end

@implementation YYImageDownloader

+(YYImageDownloader*)sharedDownloader {
    static YYImageDownloader *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (NSMutableDictionary*)operations
{
    if (!_operations) {
        _operations = [NSMutableDictionary new];
    }
    return _operations;
}

- (instancetype)init {
    if (self = [super init]) {
//        _queue = dispatch_queue_create("com.iyong.YYImageDownloaderBarrierQueue", DISPATCH_QUEUE_CONCURRENT);
        _downloadQueue = [NSOperationQueue new];
        _downloadQueue.maxConcurrentOperationCount = 6;
        _downloadQueue.name = @"com.iyong.YYImageDownloadQueue";
        
//        _errorUrl = [NSMutableArray new];
        
        
    }
    return self;
}


- (void)dealloc
{
    [self.downloadQueue cancelAllOperations];
}

- (void)downloadImageWithAvatarUrl:(NSString*)imageUrl
                       isThumbnail:(BOOL)isThumbnail
                         completed:(YYImageDonwloadCompleteBlock)completeBlock
{
    
    NSBlockOperation *blockOperation = [NSBlockOperation new];
    [blockOperation addExecutionBlock:^{
        
        if (IsStrEmpty(imageUrl)) {
            if (completeBlock) {
                completeBlock(nil, nil);
            }
            return;
        }
        
        // 如果本地已经缓存图片
        NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
        if (isThumbnail) {
            convertImgUrl = [convertImgUrl substringToIndex:[convertImgUrl indexOfString:@"."]];
            convertImgUrl = [convertImgUrl stringByAppendingString:@"-thumbnail.png"];
        }
        
//        if ([self.errorUrl containsObject:convertImgUrl]) {
//            if (completeBlock) {
//                completeBlock(nil,nil);
//            }
//            return;
//        }
        
        NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
        if (imageData != nil && imageData.length > 0) {
            NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
            if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                if (completeBlock) {
                    completeBlock(imageData, nil);
                }
                return;
            }
        }
        
        __weak __typeof(self)wself = self;
        
        [[NetworkManager sharedInstance] downloadImageData:imageUrl requestMethod:POST success:^(id responseData) {
            //    [[NetworkManager sharedInstance] downloadImageData:url requestMethod:POST params:params success:^(id responseData) {
            
            if (!wself) return;
            
            if ([responseData isKindOfClass:[NSData class]]) {
                
                if (!responseData) {
                    return;
                }
                
                NSData *imageData = responseData;
                
                // 判断是否为jpg或者png格式图片
                NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
                
                if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                    
                    [[FileCache defaultCache] saveData:imageData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                    
//                    dispatch_main_async_safe(^{
                    
                        if (completeBlock) {
                            completeBlock(imageData, nil);
                        }
                        
//                        wself.image = [UIImage imageWithData:imageData];
                        
//                        [wself setNeedsLayout];
                        
//                        if (success) {
                        
//                            success(imageData);
//                        }
                        
//                    });
                    
                    
                    
                }
                else {
                    
//                    [self.errorUrl addObject:convertImgUrl];
                    
                    if (completeBlock) {
                        completeBlock(nil, nil);
                    }
                    
//                    if (placeholderImage != nil) {
//                        
//                        dispatch_main_async_safe(^{
//                            
//                            wself.image = placeholderImage;
//                            
//                        });
//                        
//                    }
                    
                }
                
            }
            
        } andFailure:^(NSString *errorDesc) {
            
//            if (completeBlock) {
//                completeBlock(nil, nil);
//            }
            
//            [self.errorUrl addObject:convertImgUrl];
            if (completeBlock) {
                completeBlock(nil, nil);
            }
            
//            if (placeholderImage != nil) {
//                
//                dispatch_main_async_safe(^{
//                    
//                    wself.image = placeholderImage;
//                    
//                });
//                
//            }
            
//            if (failure) {
//                
//                failure(errorDesc);
//                
//            }
            
        }];
        
        
        
        
    }];
    
    [_downloadQueue addOperation:blockOperation];
}


- (void)downloadImageWithImageUrl:(NSString*)imageUrl
                      isThumbnail:(BOOL)isThumbnail
                        completed:(YYImageDonwloadCompleteBlock)completeBlock
{
    // Invoking this method without a completedBlock is pointless
    NSAssert(completeBlock != nil, @"completeBlock could not be nil");
    
    
//    if (IsStrEmpty(imageUrl) /*|| [imageUrl contains:@"null"] || ![imageUrl startsWith:@"img"]*/) {
//        
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        
//            if (completeBlock) {
//                completeBlock(nil,nil);
//            }
//        }];
//        
//        
////        [wself.operations removeObjectForKey:imageUrl];
//        
//        return;
//    }


    
//    NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
//    if (isThumbnail) {
//        convertImgUrl = [convertImgUrl substringToIndex:[convertImgUrl indexOfString:@"."]];
//        convertImgUrl = [convertImgUrl stringByAppendingString:@"-thumbnail.png"];
//    }
//    
//    NSData *imageData;

#if 0
    // 如果本地已经缓存图片
    imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
    if (imageData != nil && imageData.length > 0) {
        NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
        if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
                if (completeBlock) {
                    completeBlock(imageData, nil);
                }
                
            }];
            
            
//            [wself.operations removeObjectForKey:imageUrl];
            
            return;
        }
    }
#endif

    
    
    // 取出当前图片url对应的下载操作（operation对象）
//    NSBlockOperation *blockOperation = self.operations[imageUrl];
//    if (blockOperation) {
//        return;
//    }
    
    
    __weak __typeof(self)wself = self;
    
    NSBlockOperation *blockOperation = [NSBlockOperation new];
    [blockOperation addExecutionBlock:^{
       
        if (IsStrEmpty(imageUrl) /*|| [imageUrl contains:@"null"] || ![imageUrl startsWith:@"img"]*/) {
            
            [wself.operations removeObjectForKey:imageUrl];
            
//            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            
//                if (completeBlock) {
//                    completeBlock(nil,nil);
//                }
//            }];
            
            return;
        }
            
            

        // 如果本地已经缓存图片
        NSString *convertImgUrl = [imageUrl replaceAll:@"/" with:@"-"];
        if (isThumbnail) {
            convertImgUrl = [convertImgUrl substringToIndex:[convertImgUrl indexOfString:@"."]];
            convertImgUrl = [convertImgUrl stringByAppendingString:@"-thumbnail.png"];
        }
            
            NSData *imageData;

        
        imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
        if (imageData != nil && imageData.length > 0) {
            NSString *imageContentType = [NSData sd_contentTypeForImageData:imageData];
            if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                
                [wself.operations removeObjectForKey:imageUrl];
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                    if (completeBlock) {
                        completeBlock(imageData, nil);
                    }
                    
                }];
                
                
                
                
                return;
            }
        }

        
        
        
        [[AliyunOSSManager sharedInstance] downloadImage:imageUrl isThumbnail:isThumbnail complete:^(DownloadImageState state, NSData *data) {
            
            if (state == DownloadImageSuccess) {
                
                if (!wself) {
                    return;
                }
                
                NSString *imageContentType = [NSData sd_contentTypeForImageData:data];
                // 是否为图片数据
                if ([imageContentType isEqualToString:@"image/jpeg"] || [imageContentType isEqualToString:@"image/png"]) {
                    if (isThumbnail) {
                        // 保存缩略图
                        NSInteger maxFileSize = 300 * 1024;
                        UIImage *tempImage = [UIImage imageWithData:data];
                        NSData *compressedData = [ImageUtil compressImageWithData:tempImage toMaxFileSize:maxFileSize];
                        [[FileCache defaultCache] saveData:compressedData forKey:convertImgUrl cacheAge:kCacheAgeForever];
                    }
                    else {
                        [[FileCache defaultCache] saveData:data forKey:convertImgUrl cacheAge:kCacheAgeForever];
                    }
                
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                        if (completeBlock) {
                            completeBlock(imageData, nil);
                        }
                        
                    }];
                    
                    [self.operations removeObjectForKey:imageUrl];
                }
                else {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                        if (completeBlock) {
                            completeBlock(nil, nil);
                        }
                        
                    }];
                    [self.operations removeObjectForKey:imageUrl];
                }
            }
            // 下载失败
            else if (state == DownloadImageFailed) {
//                [self.errorUrl addObject:convertImgUrl];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                    if (completeBlock) {
                        completeBlock(nil, nil);
                    }
                    
                }];
                [self.operations removeObjectForKey:imageUrl];
                
            }
        }];

        
    }];
    
    // 将当前下载操作添加到下载操作缓存中 (为了解决重复下载)
    self.operations[imageUrl] = blockOperation;
    
    // 添加下载操作到队列中
    [self.downloadQueue addOperation:blockOperation];
    
    
    
}

@end
