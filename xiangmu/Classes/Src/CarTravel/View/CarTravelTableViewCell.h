//
//  CarTravelTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TravelOrderModel.h"

typedef NS_ENUM(NSUInteger, CarTravelCellHeight)
{
    CarTravelWaitReceiveCellHeight=210,
    CarTravelPendingPayCellHeight=238,
    CarTravelWaitStartCellHeight=238,
    CarTravelStartTravelCellHeight=238,
    CarTravelCompleteTravelCellHeight=238,
};

@interface CarTravelTableViewCell : UITableViewCell

@property (nonatomic, strong) TravelOrderModel *model;

@property (weak, nonatomic) IBOutlet UILabel *startDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *startAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *endAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@property (weak, nonatomic) IBOutlet UIButton *contactButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelOrderButton;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UILabel *vechileInfoLabel;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

-(void)initData:(TravelOrderModel*)model;

+(CGFloat)getCellHeight;

@end
