//
//  DatabaseManager.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>


#define kDatabaseFileName       @"iYong.sqlite"

@class FMDatabaseQueue;
@interface DatabaseManager : NSObject
{
    BOOL _isInitializeSuccess;
}

@property (nonatomic, assign) BOOL isDataBaseOpened;

@property (nonatomic, strong) NSString *writablePath;

@property (nonatomic, strong) FMDatabaseQueue *databaseQueue;

+ (DatabaseManager*)currentManager;

- (BOOL)isDatabaseOpened;

- (void)openDataBase;

- (void)closeDataBase;

+ (void)releaseManager;


@end
