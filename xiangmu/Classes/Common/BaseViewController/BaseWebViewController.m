//
//  WebViewController.m
//  lottery
//
//  Created by WYY on 14-6-5.
//
//

#import "BaseWebViewController.h"
#import <AFNetworking/UIWebView+AFNetworking.h>
#import <WebKit/WebKit.h>
//#import "WKWebView.h"
//#import "MyWalletWebViewController.h"
//#import "BallBetViewController.h"
//#import "NewJczqBetViewController.h"
//#import "WithDrawWebViewController.h"
//#import "RecordDetailViewController.h"
//#import "WXApi.h"
//#import "BaseNavigationController.h"
//#import "lotteryAppDelegate.h"
//#import "SfcMatchViewController.h"
//#import "lotteryAppDelegate.h"
//#import "IMYWebView.h"


@interface BaseWebViewController ()



@property (nonatomic, strong) NSMutableURLRequest *reqeust;

@end

@implementation BaseWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _hasLeftBarButtonItem = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self initNavigationBar];
    
    self.view.backgroundColor = UIColorFromRGB(0xF5F5F5);
    
    DLog(@"self.url = %@", self.url);
    
//    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.bounds];
//    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
//    [self.view addSubview:webView];
    
    
//    _webview = [[IMYWebView alloc] initWithFrame:self.view.bounds];
//    _webview reloadRequest
//    [self.view addSubview:_webview];
    
//    if(_webview.usingUIWebView)
//    {
//        self.title = @"ClickRefresh-UIWebView";
//    }
//    else
//    {
//        self.title = @"ClickRefresh-WKWebView";
//    }
//
//    
////    NSString *url = @"http://auto.sina.com.cn/newcar/2017-06-12/detail-ifyfzfyz3448440.shtml";
//    
//        [_webView loadRequest:[NSURLRequest reqeustWithURL:[NSURL URLWithString:url]]];
//
////    NSString *url = @"http://www.taobao.com";
//    
//    
//    DLog(@"self.url = %@", self.url);
//    
//    [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
//
//    [_webview reload];
    
    
    _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    _webView.autoresizesSubviews = YES;
    _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _webView.backgroundColor = [UIColor grayColor];
    _webView.delegate = self;
    self.view = self.webView;
    
    /// set proxy and progresssView
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigationBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigationBarBounds.size.height - progressBarHeight, navigationBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
//    _progressView.backgroundColor = NAVBAR_BGCOLOR;
//    [_progressView setBackgroundColor:[UIColor yellowColor]];
//    _progressView setBackgroundColor
//    _progressView.progressTintColor=NAVBAR_BGCOLOR;
    _progressView.tintColor = NAVBAR_BGCOLOR;
    [_progressView setProgress:0.1f];
    
    [self startRequest];
}

-(void)startRequest
{
//    
//    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.bounds];
//    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
////    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.baidu.com"]]];
//    [self.view addSubview:webView];
    
    DLog(@"self.url = %@", self.url);
    
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_url] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60];
    
    [_webView loadRequest:urlRequest];
    
//    self.reqeust = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    
//    NSURLConnection *conn = [NSURLConnection connectionWithRequest:self.reqeust delegate:self];
    
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
//    NSMutableURLRequest *requestUrl = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.url]];
//    requestUrl
//    [_webView loadRequest:request];
    
//    [_webView loadReques:reqeustUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60]];
    
//    NSMutableURLRequest *request_url = [NSMutableURLRequest URLWithString:self.url];
    
//    [_webView loadRequest:requestUrl progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//        
//        
//    } success:^NSString * _Nonnull(NSHTTPURLResponse * _Nonnull response, NSString * _Nonnull HTML) {
//        
//        return HTML;
//        
//    } failure:^(NSError * _Nonnull error) {
//        
//        
//    }];
    
    
}

//- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
//    if ([challenge previousFailureCount]== 0) {
//        //NSURLCredential 这个类是表示身份验证凭据不可变对象。凭证的实际类型声明的类的构造函数来确定。
//        NSURLCredential* cre = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
//        [challenge.sender useCredential:cre forAuthenticationChallenge:challenge];
//    }
//}
//
//
//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
////    [self.webView loadRequest:self.reqeust];
////    [connection cancel];
//}

-(void)initNavigationBar
{
    if (_hasLeftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_back"] style:UIBarButtonItemStylePlain target:self action:@selector(pressedBackbut:)];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_progressView != nil) {
        [self.navigationController.navigationBar addSubview:_progressView];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Remove progress view
    // because UINavigationBar is shared with other ViewControllers
    if (_progressView != nil) {
        [_progressView removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidUnload {
//    _webView = nil;
    _url = nil;
}

//-(void)setUserUrl:(NSString*)url
//{
//    if ([self checkLogin]) {
//        self.url  = [NSString stringWithFormat:@"%@?uid=%@&uname=%@&token=%@&pf=2&channel=%@", url, [self getUid], [[self getLoginName] encodeToPercentEscapeString], [[self getToken] encodeToPercentEscapeString], CHANNEL_ID/*, [NSDate getTimeStamp]*/];
//    }
//    else {
//        self.url = url;
//    }
//    NSLog(@"url = %@", _url);
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - navigation back method
-(void)pressedBackbut:(UIButton*)button
{
    if ([self.webview canGoBack]) {
        [self.webview goBack];
        return;
    }
    if (self.enter_type == ENTER_TYPE_PRESENT) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
//    self.title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}


#pragma mark - webview delegate method

- (void)webViewDidStartLoad:(UIWebView *)webView;
{
//    [self showWaiting:@""];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [_progressView setProgress:0.0f];
//    [self dismissNotificationMessage];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
//    [self hideWaiting];
    DLog(@"");
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//    [self dismissNotificationMessage];
    
    if (self.title == nil || self.title.length == 0) {
        self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
{
    DLog(@"didFailLoadWithError:%@", error.userInfo);
    
//    [self hideWaiting];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//    DebugLog(@"error.status = %ld\nerror.localizedDescription = %@", (long)error.code, [error localizedDescription]);
    if ([error code] != NSURLErrorCancelled) {
        //show error alert, etc.
//        [self showTipMessage:[NSString stringWithFormat:@"亲，网络开小差，请重试"]];
//#ifdef DEBUG
//        [self showTipMessage:[NSString stringWithFormat:@"错误码:%d", [error code]]];
//#endif
    }
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    DLog(@"shouldStartLoadWithRequest");
    
    
//    [self dismissNotificationMessage];
//    [self hideWaiting];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    NSURL * url = [request URL];
    
    DLog("url.scheme = %@, url = %@， navigationType = %d", url.scheme, url.absoluteString, navigationType);
    
    if (IsStrEmpty(url.absoluteString)) {
        return NO;
    }
    
//    if (navigationType == -1) {
//        navigationType = UIWebViewNavigationTypeOther;
//    }
    
//    if ([url.absoluteString startsWith:@"http://h5.51caixiang.com/award?lid"] || [url.absoluteString startsWith:@"http://h5.51caixiangtest.com/award?lid"]) {
//        if([self.title isEqualToString:@"开奖信息"]) {
//            NSDictionary *dict = @{@"url":url.absoluteString};
//            [self pushViewController:VIEW_ID_AWARD_DETAIL withObject:dict];
//            return NO;
//        }
//    }
//    
//    NSString *urlScheme = url.scheme.lowercaseString;
//    if ([urlScheme isEqualToString:@"loginsuccess"] /*|| [urlScheme isEqualToString:@"registersuccess"]*/) {
//        // loginSuccess:username password uid token
//        NSLog(@"url.absoluteString = %@\nafter decode = %@", url.absoluteString, [url.absoluteString decodeFromPercentEscapeString]);
//        NSString *decodeString = [url.absoluteString decodeFromPercentEscapeString];
//        NSArray *array = [decodeString componentsSeparatedByString:@":"];
//        NSArray *array2 = [array[1] componentsSeparatedByString:@" "];
//        if (array2.count >= 4) {
//            NSString *username = array2[0];
//            NSString *password = array2[1];
//            NSString *uid = array2[2];
//            NSString *token = array2[3];
//            // 保存到NSUserDefaults中
//            [[UserDefaults sharedInstance] setObject:token forKey:KEY_USER_TOKEN];
//            [[UserDefaults sharedInstance] setObject:uid forKey:KEY_USER_UID];
//            [[UserDefaults sharedInstance] setObject:username forKey:KEY_USER_NAME];
//            [[UserDefaults sharedInstance] setObject:password forKey:KEY_USER_PASSWORD];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kOnGetUserInfoEvent object:nil userInfo:nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginRegisterSuccessEvent object:nil];
//        }
//        return NO;
//    }
//    // 注册成功
//    if ([urlScheme isEqualToString:@"registersuccess"]) {
//        NSLog(@"url.absoluteString = %@\nafter decode = %@", url.absoluteString, [url.absoluteString decodeFromPercentEscapeString]);
//        NSString *decodeString = [url.absoluteString decodeFromPercentEscapeString];
//        NSArray *array = [decodeString componentsSeparatedByString:@":"];
//        NSArray *array2 = [array[1] componentsSeparatedByString:@" "];
//        NSString *username = array2[0];
//        NSString *password = array2[1];
//        NSString *uid = array2[2];
//        NSString *token = array2[3];
//        // 保存到NSUserDefaults中
//        [[UserDefaults sharedInstance] setObject:token forKey:KEY_USER_TOKEN];
//        [[UserDefaults sharedInstance] setObject:uid forKey:KEY_USER_UID];
//        [[UserDefaults sharedInstance] setObject:username forKey:KEY_USER_NAME];
//        [[UserDefaults sharedInstance] setObject:password forKey:KEY_USER_PASSWORD];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kOnGetUserInfoEvent object:nil userInfo:nil];
//        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//        return NO;
//
//    }
//    // 设置用户名成功
//    if ([urlScheme isEqualToString:@"setnicknamesuccess"]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kOnGetUserInfoEvent object:nil userInfo:nil];
//        [self.navigationController popViewControllerAnimated:YES];
//        return NO;
//    }
//    // 忘记密码
//    if ([urlScheme isEqualToString:@"forgetpassword"]) {
//        [self pushViewController:VIEW_ID_FORGETPASSWORD withObject:nil];
//        return NO;
//    }
//    // 密码设置成功
//    if ([urlScheme isEqualToString:@"resetpasswordsuccess"]) {
//        if ([self.title isEqualToString:@"修改密码"]) {
//            NSArray *array = [url.absoluteString componentsSeparatedByString:@":"];
//            NSString *password = array[1];
//            NSLog(@"password=%@", password);
//            [[UserDefaults sharedInstance] setObject:password forKey:KEY_USER_PASSWORD];
//            lotteryAppDelegate *delegate = (lotteryAppDelegate*)APP_DELEGATE;
//            [delegate autoLogin];
//            [self showTipMessage:@"修改密码成功"];
//            [self.navigationController popViewControllerAnimated:YES];
//            return NO;
//        }
//        else {
//            [self showTipMessage:@"重置密码成功"];
//            [self.navigationController popViewControllerAnimated:YES];
//            return NO;
//        }
//    }
//    // 绑定手机号成功
//    if ([urlScheme isEqualToString:@"bindmobilesuccess"]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kOnBindMobileSuccessEvent object:nil];
//        [self.navigationController popViewControllerAnimated:YES];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"mywallet"]) {
//        NSLog(@"mywallet");
//        if ([self checkLogin]) {
//            [self pushViewController:VIEW_ID_MY_WALLET withObject:nil];
//        }
//        else {
//            [self doLogin];
//        }
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"mylottery"]) {
//        NSLog(@"mylottery");
//        [self gotoMyLottery];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"orderdetail"]) {
//        NSArray *array = [url.absoluteString componentsSeparatedByString:@":"];
////        NSString *orderId = array[1];
//        RecordDetailViewController *recordDetailViewController = [[RecordDetailViewController alloc] init];
////        recordDetailViewController.orderId = orderId;
//        [self.navigationController pushViewController:recordDetailViewController animated:YES];
////        BaseWebViewController *viewController = [[BaseWebViewController alloc] init];
////        viewController.title = @"订单详情";
////        [viewController setUserUrl:[NSString stringWithFormat:@"%@/%@", ORDER_DETAIL_URL/*, ORDER_DETAIL*/, orderId]];
////        viewController.enter_type = ENTER_TYPE_PUSH;
////        [self.navigationController pushViewController:viewController animated:YES];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"billdetail"]) {
//        NSArray *array = [url.absoluteString componentsSeparatedByString:@":"];
//        NSString *orderId = array[1];
//        BaseWebViewController *viewController = [[BaseWebViewController alloc] init];
//        viewController.title = @"账单详情";
//        [viewController setUserUrl:[NSString stringWithFormat:@"%@/%@/%@", API_URL, BILL_DETAIL, orderId]];
//        viewController.enter_type = ENTER_TYPE_PRESENT;
//        BaseNavigationController *localNav = [[BaseNavigationController alloc] initWithRootViewController:viewController];
//        [self presentViewController:localNav animated:YES completion:nil];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"withdraw"]) {
//        WithDrawWebViewController *viewController = [[WithDrawWebViewController alloc] init];
//        viewController.title = @"提款";
//        [viewController setUserUrl:[NSString stringWithFormat:@"%@/%@", API_URL, WITHDRAW_API]];
//        viewController.enter_type = ENTER_TYPE_PUSH;
//        [self.navigationController pushViewController:viewController animated:YES];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"withdrawsuccess"]) {
//        [self showTipMessage:@"提款成功"];
//        [self.navigationController popViewControllerAnimated:YES];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kOnRefreshMyWalletEvent object:nil];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"recharge"]) {
//        [self doRecharge];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"bindbankcard"]) {
//        [self pushViewController:VIEW_ID_BIND_BANKCARD withObject:nil];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"bindbankcardsuccess"]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kOnBindBankcardSuccessEvent object:nil];
//        [self.navigationController popViewControllerAnimated:YES];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"bindalipaysuccess"]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kOnBindAlipaySuccessEvent object:nil];
//        [self.navigationController popViewControllerAnimated:YES];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"bindalipay"]) {
//        [self pushViewController:VIEW_ID_BIND_ALIPAY withObject:nil];
//        return NO;
//    }
//    if ([urlScheme isEqualToString:@"continuepayresult"]) {
//        NSArray *array = [url.absoluteString componentsSeparatedByString:@":"];
//        NSString *result = array[1];
//        [self showTipMessage:[result decodeFromPercentEscapeString]];
//        return NO;
//    }
    
    return YES;
    
}

@end
