//
//  CertifiedView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CertifiedView.h"

#define imageWidth 13
#define imageHeight 13

@implementation CertifiedView

- (instancetype)init
{
    if (self = [super init]) {
        [self initialize];
        self.width = (imageWidth+5)*2;
//        self.width = (imageWidth+5) * 3;// + imageWidth;
        self.height = imageWidth;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self initialize];
        
    }
    return self;
}

- (void)initialize
{
    self.companyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.personImageView.right+5, 0, imageWidth, imageHeight)];
    self.companyImageView.image = [UIImage imageNamed:@"icon_company"];
    [self addSubview:self.companyImageView];
    
//    self.vechileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.companyImageView.right + 5, -2, 18, 18)];
//    self.vechileImageView.image = [UIImage imageNamed:@"icon_vechile"];
//    [self addSubview:self.vechileImageView];
    
    
}

- (void)initData:(BOOL)isRealnameCertified isCompanyCertified:(BOOL)isCompanyCertified isVechileCertified:(BOOL)isVechileCertified
{
    if (isRealnameCertified) {
        self.personImageView.image = [UIImage imageNamed:@"icon_person"];
    }
    else {
        self.personImageView.image = [UIImage imageNamed:@"icon_person_gray"];
    }
    
    if (isCompanyCertified) {
        self.companyImageView.image = [UIImage imageNamed:@"icon_company"];
    }
    else {
        self.companyImageView.image = [UIImage imageNamed:@"icon_company_gray"];
    }
    
//    if (isVechileCertified) {
//        self.vechileImageView.image = [UIImage imageNamed:@"icon_vechile"];
//    }
//    else {
//        self.vechileImageView.image = [UIImage imageNamed:@"icon_vechile_gray"];
//    }
}

- (UIImageView*)personImageView
{
    if (!_personImageView) {
        _personImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageWidth, imageHeight)];
        _personImageView.image = [UIImage imageNamed:@"icon_person"];
        [self addSubview:_personImageView];
    }
    return _personImageView;
}

- (UIImageView*)companyImageView
{
    if (!_companyImageView) {
        _companyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageWidth, imageHeight)];
        _companyImageView.image = [UIImage imageNamed:@"icon_company"];
        [self addSubview:_companyImageView];
    }
    return _companyImageView;
}

- (UIImageView*)vechileImageView
{
    if (!_vechileImageView) {
        _vechileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageWidth, imageHeight)];
        _vechileImageView.image = [UIImage imageNamed:@"icon_vechile"];
        [self addSubview:_vechileImageView];
    }
    return _vechileImageView;
}

@end
