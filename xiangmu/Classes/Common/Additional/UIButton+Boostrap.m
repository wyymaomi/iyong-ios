//
//  UIButton+HighlitedImage.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UIButton+Boostrap.h"

@implementation UIButton (Boostrap)

-(void)bootstrapStyle{
    self.layer.borderWidth = 1;
    self.layer.cornerRadius = 20;
    self.layer.masksToBounds = YES;
    [self setAdjustsImageWhenHighlighted:NO];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self.titleLabel setFont:[UIFont fontWithName:@"FontAwesome" size:self.titleLabel.font.pointSize]];
}

- (void)okStyle;
{
    [self bootstrapStyle];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.layer.cornerRadius = 18;
    self.layer.borderWidth = 0.f;
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    [self setBackgroundImage:[self buttonImageFromColor:NAVBAR_BGCOLOR] forState:UIControlStateNormal];
    [self setBackgroundImage:[self buttonImageFromColor:BUTTON_HIGHLIGHTED_COLOR] forState:UIControlStateHighlighted];
    
}

- (void)cancelStyle;
{
    [self bootstrapStyle];
//    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    self.layer.cornerRadius = 18;
    self.layer.borderWidth = 0.f;
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
//    [self setBackgroundImage:[self buttonImageFromColor:] forState:UIControlStateHighlighted];

}

- (void)greenStyle;
{
//    [self bootstrapStyle];
//    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//    self.backgroundColor = UIColorFromRGB(0x09a892);
//    self.layer.cornerRadius = 5;
//    self.layer.borderWidth=0;
    
    [self bootstrapStyle];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.backgroundColor = UIColorFromRGB(0x09a892);
    self.layer.cornerRadius = 18;
    self.layer.borderWidth = 0.f;
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    [self setBackgroundImage:[self buttonImageFromColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    [self setBackgroundImage:[self buttonImageFromColor:BUTTON_HIGHLIGHTED_COLOR] forState:UIControlStateHighlighted];
}

- (void)grayStyle;
{
    [self bootstrapStyle];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.backgroundColor = [UIColor lightGrayColor];
    self.layer.cornerRadius = 5;
    self.layer.borderWidth = 0.f;
}

- (void)yellowStyle
{
    [self bootstrapStyle];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.backgroundColor = UIColorFromRGB(0xE3C148);
    self.layer.cornerRadius = 5;
    self.layer.borderWidth = 0.f;
//    [self setBackgroundImage:[self buttonImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
//    [self setBackgroundImage:[self buttonImageFromColor:UIColorFromRGB(0x0094E7)] forState:UIControlStateHighlighted];

}

-(void)blueStyle
{
    [self bootstrapStyle];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.backgroundColor = NAVBAR_BGCOLOR;//UIColorFromRGB(0x30A6DE);
    self.layer.cornerRadius = 18;
    self.layer.borderWidth = 0.f;
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
//    [self setBackgroundImage:[self buttonImageFromColor:UIColorFromRGB(0x0094E7)] forState:UIControlStateHighlighted];
    [self setBackgroundImage:[self buttonImageFromColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    [self setBackgroundImage:[self buttonImageFromColor:BUTTON_HIGHLIGHTED_COLOR] forState:UIControlStateHighlighted];
}

-(void)whiteStyle
{
    [self bootstrapStyle];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self setBackgroundColor:[UIColor whiteColor]];
    self.layer.cornerRadius = 18;
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    [self setBackgroundImage:[self buttonImageFromColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
}

-(void)alertStyle
{
    [self bootstrapStyle];
    [self setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 5;
    self.layer.borderWidth = 1;
    self.layer.borderColor = UIColorFromRGB(0xF0F0F0).CGColor;
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
//    [self setBackgroundImage:[self buttonImageFromColor:UIColorFromRGB(0x0094E7)] forState:UIControlStateHighlighted];
    [self setBackgroundImage:[self buttonImageFromColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
}

-(void)darkRedStyle
{
//    [self bootstrapStyle];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setBackgroundColor:UIColorFromRGB(0xD21A22)];
    [self setBackgroundImage:[self buttonImageFromColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    [self setBackgroundImage:[self buttonImageFromColor:UIColorFromRGB(0xb81c25)] forState:UIControlStateHighlighted];
    
}


- (UIImage *) buttonImageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
