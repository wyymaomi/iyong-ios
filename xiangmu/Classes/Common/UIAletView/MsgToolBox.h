//
//  MsgToolBox.h
//  UserExperienceDemo
//
//  Created by liuzhilong on 15/3/31.
//  Copyright (c) 2015年 alibaba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MsgToolBox : NSObject

//+(void)showToastOnView:(NSString*)message view:(UIView*)view;
+ (void) showAlert:(NSString *)title content:(NSString *)content;
+ (void) showToast:(NSString*)message;

@end
