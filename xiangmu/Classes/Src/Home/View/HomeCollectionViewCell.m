//
//  HomeCollectionViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HomeCollectionViewCell.h"

@implementation HomeCollectionViewCell

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        //self.frame = CGRectMake(0, 0, (ViewWidth-40)/3, 90);

        CGFloat imageWidth = 80;
        CGFloat imageHeight = 80;
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-imageWidth, (self.frame.size.height-imageHeight)/2, imageWidth, imageHeight)];
//        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-46, (self.frame.size.height-46)/2, 46, 46)];
        self.iconImageView.tag = 100;
        [self addSubview:self.iconImageView];

        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-self.iconImageView.width-5, self.frame.size.height)];
        self.textLabel.textAlignment = UITextAlignmentRight;
        self.textLabel.font = BOLD_FONT(15);
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.tag = 101;
        [self addSubview:self.textLabel];
        
//        self.textLabel.userInteractionEnabled = YES;
//        self.iconImageView.userInteractionEnabled = YES;
        self.userInteractionEnabled = YES;
        
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

@end
