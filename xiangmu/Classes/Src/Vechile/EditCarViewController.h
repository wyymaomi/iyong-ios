//
//  EditCarViewController.h
//  xiangmu
//
//  Created by David kim on 16/3/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "AddCarDetailViewController.h"

@protocol Cheliang <NSObject>

-(void)cheliang;

@end
@interface EditCarViewController : LFNavController<Get,Sex,Date,Age>
{
    //头像
    UIImageView *avatarImg;
}
@property (nonatomic,strong)NSMutableArray *arr;
@property (nonatomic,strong)NSString *uid;

@property (nonatomic,weak) id<Cheliang>delegate;

@end
