//
//  AssociationalWordDisplayController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AssociationalWordDisplayController.h"


@implementation AssociationalWordDisplayController

-(NSMutableArray*)keywordList
{
    if (!_keywordList) {
        _keywordList = [NSMutableArray array];
    }
    return _keywordList;
}

- (UITableView *)displayTableView
{
    if(!_displayTableView){
        
        _displayTableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                         style:UITableViewStylePlain];
        
//        [_displayTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        
        [_displayTableView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
        
        _displayTableView.scrollEnabled = YES;
        
        _displayTableView.userInteractionEnabled = YES;
        
        _displayTableView.delegate =self;
        
        _displayTableView.dataSource =self;
        
        _displayTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        _displayTableView.tableFooterView = [[UIView alloc] init];
        
    }
    
    return _displayTableView;
}

//- (id)initWithController:(UIViewController *)controller
-(id)init
{
    self = [super init];
    if (self) {
        
//        _tableTopPosY = 191;
//        _distanceToTop = 95-25;
//        _contentController = controller;
        
        self.displayTableView.frame = CGRectMake(0, 0, ViewWidth, ViewHeight);
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
    }
    return self;
}

#pragma mark - receive keyboardWillHide Notification
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.displayTableView.frame = CGRectMake(0, STATUS_BAR_HEIGHT + 44, ViewWidth, ViewHeight - STATUS_BAR_HEIGHT - 44);
}

#pragma mark - receive keyboardWillShow Notification
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    
//    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    
//    WeakSelf
//    [UIView animateWithDuration:0.5 animations:^{
    
        NSInteger tableViewTop = self.displayTableView.top;
        self.displayTableView.frame = CGRectMake(0, tableViewTop, ViewWidth, ViewHeight-tableViewTop-keyboardRect.size.height);
        
//    }];
//    CGFloat height = ViewWidth - _distanceToTop - keyboardRect.size.height;
    
//    self.displayTableView.frame = CGRectMake(0, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    
//    if (_tableTopPosY != 50) {
//        self.displayTableView.frame = CGRectMake(0, _tableTopPosY, ViewWidth, height);
        
//        [UIView animateWithDuration:0.5 animations:^{
//            _tableTopPosY = 50;
//            self.displayTableView.frame = CGRectMake(0, _tableTopPosY, ViewWidth, height);
//        }];
//    }
//    else
//    {
//        self.displayTableView.frame = CGRectMake(0, _tableTopPosY, ViewWidth, height);
//    }
}

#pragma mark - UITableView Delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.keywordList && self.keywordList.count > 0) {
        return self.keywordList.count;
    }
    return 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"keywordsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];

        cell.textLabel.font = FONT(15);
        cell.textLabel.textColor = UIColorFromRGB(0x7D7D7D);
        cell.detailTextLabel.font = FONT(13);
        cell.detailTextLabel.textColor = UIColorFromRGB(0xC3C3C3);
    }
    LocationSearchModel *data = self.keywordList[indexPath.row];
//    cell.textLabel.text = data.title;
//    cell.detailTextLabel.text = data.address;
//    cell.textLabel.text = self.keywordList[indexPath.row];
    
    cell.textLabel.text = data.name;
    cell.detailTextLabel.text = data.address;
//    cell.detailTextLabel.text = data.
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectAssociationalWord:)]) {
        LocationSearchModel *data = self.keywordList[indexPath.row];
        [self.delegate didSelectAssociationalWord:data];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didScrollView)]) {
        [self.delegate didScrollView];
    }
//    [_inputTextView resignFirstResponder];
}











@end
