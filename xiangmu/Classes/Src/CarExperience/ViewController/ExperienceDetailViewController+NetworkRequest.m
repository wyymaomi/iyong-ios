//
//  ExperienceDetailViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/7.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ExperienceDetailViewController+NetworkRequest.h"

@implementation ExperienceDetailViewController (NetworkRequest)

-(void)fetchExperienceDetail
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_DETAIL_API];
//    NSString *params = [NSString stringWithFormat:@"experienceId=%@", self.experienceViewModel.serviceModel.id];
    NSDictionary *params = @{@"experienceId": self.experienceViewModel.serviceModel.id};
    
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        [strongSelf resetAction];
        
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        
        if (code_status == STATUS_OK) {
            
            strongSelf.experienceViewModel.serviceModel = [[UserExpericeModel alloc] initWithDictionary:result[@"data"] error:nil];
            [strongSelf.pullRefreshTableView reloadData];
            
//            [strongSelf.shareImageView downloadImageFromAliyunOSS:strongSelf.experienceViewModel.serviceModel.imgs[0] isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
            
            [strongSelf.shareImageView yy_setImageWithObjectKey:strongSelf.experienceViewModel.serviceModel.imgs[0] placeholder:DefaultPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                
                
            } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
                
                
            }];
            
        }
        else {
            
        }
        
    }];
//    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
//        StrongSelf
//        [strongSelf resetAction];
//        [strongSelf.pullRefreshTableView endHeaderRefresh];
//        NSInteger code_status = [responseData[@"code"] integerValue];
//        if (code_status == STATUS_OK) {
//            
//            strongSelf.experienceViewModel.serviceModel = [[UserExpericeModel alloc] initWithDictionary:responseData[@"data"] error:nil];
//            [strongSelf.pullRefreshTableView reloadData];
//
////            UserExpericeModel *model = [[UserExpericeModel alloc] initWithDictionary:responseData[@"data"] error:nil];
//            
//            
////            strongSelf.experienceViewModel.serviceModel = [[UserExpericeModel alloc] initWithDictionary:responseData[@"data"] error:nil];
////            [strongSelf.pullRefreshTableView reloadData];
//            
////            ProviderDetailModel *detailModel = [[ProviderDetailModel alloc] initWithDictionary:result[@"data"] error:nil];
////            //            [strongSelf.viewModel setDetailModel:detailModel];
////            strongSelf.viewModel.detailModel = detailModel;
////            [strongSelf downloadImagesFromAliyun];
////            [strongSelf.tableView reloadData];
////            [strongSelf setNavigationView];
//            
//        }
//        else {
//            
//        }
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//        StrongSelf
//        [strongSelf resetAction];
//        [strongSelf.pullRefreshTableView endHeaderRefresh];
//        
//    }];
    
}

- (void)addLikeNumber
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_LIKE_API];
    NSString *params = [NSString stringWithFormat:@"experienceId=%@", self.experienceViewModel.serviceModel.id];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            NSInteger likeNumbers = [strongSelf.experienceViewModel.serviceModel.thumbsQuantity integerValue];
            likeNumbers++;
            strongSelf.experienceViewModel.serviceModel.thumbsUp = @1;
            strongSelf.experienceViewModel.serviceModel.thumbsQuantity = [NSString stringWithFormat:@"%ld", (long)likeNumbers];
            
            [MsgToolBox showToast:kMsgLike];
            [strongSelf.pullRefreshTableView reloadData];
            
        }
        else {
            if (code_status == 1852) {
                [MsgToolBox showToast:@"已点过赞"];
            }
            else {
                [MsgToolBox showToast:getErrorMsg(code_status)];
            }
        }
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}


- (void)fetchFollowCommentList:(NSString*)time
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_COMMENT_LIST_API];
    NSDictionary *params = @{@"experienceId": self.experienceViewModel.serviceModel.id,
                             @"time": time};
    
    if (IsStrEmpty(time)) {
        [self.followCommentList removeAllObjects];
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        if (code_status == STATUS_OK) {
            
            //            ProviderCommentViewModel *viewModel = [[ProviderCommentViewModel alloc] init]
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                ExperienceCommentModelResponseData *responseData = [[ExperienceCommentModelResponseData alloc] initWithDictionary:result error:nil];
                for (NSInteger i = 0; i < responseData.data.count; i++) {
                    ExperienceCommentModel *model = responseData.data[i];
                    ExperienceCommentViewModel *viewModel = [[ExperienceCommentViewModel alloc] init];
                    viewModel.commentModel = model;
                    [strongSelf.followCommentList addObject:viewModel];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [strongSelf.pullRefreshTableView reloadData];
                    
                    [strongSelf.pullRefreshTableView endHeaderRefresh];
                    [strongSelf.pullRefreshTableView endFooterRefresh];
                    
                    //                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:ProviderCellCompanyDiscuss];
                    //                    [strongSelf.pullRefreshTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                    
                });
                
                
            });
            
            
        }
        else {
            
            [weakSelf.pullRefreshTableView endHeaderRefresh];
            [weakSelf.pullRefreshTableView endFooterRefresh];
            
        }
    }];
    
}

- (void)uploadComment:(NSString*)content
{
    //    NSString *errorMsg;
    //    if (![CommunityHelper isValidEnquire:self.httpMessage errorMsg:&errorMsg]) {
    //        [MsgToolBox showToast:errorMsg];
    //        return;
    //    }
    
    
    //    self.nextAction = @selector(onInquire:);
    //    self.object1 = sender;
    //    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_COMMENT_PUBLISH_API];
    NSString *params = [NSString stringWithFormat:@"topicId=%@&content=%@", self.experienceViewModel.serviceModel.id, content];
    
    [self showHUDIndicatorViewAtCenter:@"正在发布评论，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"发布成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [strongSelf fetchFollowCommentList:@""];
                }
                
            }];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
}



@end
