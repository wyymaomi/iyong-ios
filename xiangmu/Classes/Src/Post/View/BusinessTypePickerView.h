//
//  BusinessTypePickerView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BusinessTypePickerViewDelegate <NSObject>

- (void)onGetBusinessType:(NSInteger)tag isHighLighted:(BOOL)isHighLighted;

@end

@interface BusinessTypePickerView : UIView

@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIImageView *commercialImageView;
@property (weak, nonatomic) IBOutlet UIImageView *travelImageView;
@property (weak, nonatomic) IBOutlet UIImageView *airportImageView;
@property (weak, nonatomic) IBOutlet UIImageView *weddingImageView;
@property (weak, nonatomic) IBOutlet UIImageView *charteredImageView;
@property (weak, nonatomic) IBOutlet UIImageView *truckImageView;
@property (weak, nonatomic) IBOutlet UIImageView *busImageView;
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;

@property (nonatomic, strong) NSMutableArray<UIImageView*> *imageArray;

@property (nonatomic, strong) NSArray *typeNameArray;

@property (nonatomic, weak) id<BusinessTypePickerViewDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *businessTypeArray;

@end
