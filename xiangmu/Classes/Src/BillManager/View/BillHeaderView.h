//
//  BillHeaderView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *moenyLabel;

@end
