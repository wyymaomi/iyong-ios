//
//  BBRSACryptor.h
//  BBRSACryptor-ios
//
//  Created by liukun on 14-3-21.
//  Copyright (c) 2014年 liukun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <openssl/rsa.h>
#import <openssl/pem.h>

// 服务器公钥
//#define SERVER_RSA_PUBLIC_KEY @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDomFdXNdo8l6+0Mc9fapk1HV4btL2r1wzrzB0rH01mtoJtj26rTonw9VhBD7QpahGIy46qIRn9S/xPvTImfA3OJnFDB/mBVXKwUxsBu/XqPYKLxTom1NNq9qovAfX1Mb0DkTd6A+obo2f0u6WYYjhmLj3Dc146yb7xSBVMyEPlOQIDAQAB"

//#define SERVER_RSA_PUBLIC_KEY @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCT7H313yhd81bNYtsWzZuklJZswqO2badzMIQcoXJRek3U6oqh9FE2nAvnFjIYqqpNdbiKBH2cRjsoBOyy3Pktdw0PVRkUeFoKbF38ELOO76Er6VzIXJ2dHsnjX83IX49XA6BdEiFcAaL9SgHkToChc+5AvGcP+P36cAG96HrArwIDAQAB"

#define SERVER_RSA_PUBLIC_KEY @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCJ6/mf94z919p0I6ZtLbAG8zHBvl2MwVDTJXyVhVZga1K3qFzZxNOtDVNxCG9bfSmphNtl833QY2MO+X6Uh7To+UXNBi+P2GlF4w9OFkgdEkRDRJfFhKPN7TjN6yXzSIl8gxccJIteMXWF/iHbscKEI+us8nn49RHrFBWJAYM4CwIDAQAB"

// 服务器私钥
#define SERVER_RSA_PRIVATE_KEY @"MIICXAIBAAKBgQDomFdXNdo8l6+0Mc9fapk1HV4btL2r1wzrzB0rH01mtoJtj26rTonw9VhBD7QpahGIy46qIRn9S/xPvTImfA3OJnFDB/mBVXKwUxsBu/XqPYKLxTom1NNq9qovAfX1Mb0DkTd6A+obo2f0u6WYYjhmLj3Dc146yb7xSBVMyEPlOQIDAQABAoGAYlg3ZbpxYflI40nEQrXRvbGDHvfW0peE8RZGpvr/QRSeJTDmPCxjCqzR+gBnzrL4Yvyef7SrRnwgXJ7EW51MQDWKeqsF43UB6Md4HcnauU6Jr2CszofMjUw/WlN2MlI9oAwgz0NilcC4kndznXHivF8UzZVcFnZvXdb0hooDCTkCQQD1GUS6xYia7nC9BHVhpXzUQTTRpAqpy0uu+gvcWzYeOtBRpg1ypKAnnUlShbiFyHkzAWv1wzWGV9WsI8cwZh97AkEA8vC0XGLTNjn9ryvzml6WrWO57g9dLvBZu/CZOfYeGY0UnZcL96w9y4RtXUeqcs1TMsRYbnUWuCR+0//iGl+12wJAR+6sjyweFWpZSAAg+xF4e0rP158PShnSEn5sMLXMQj78mJnlZHoQTtAIjuGT8B+QAnouVyh9cvogSa8ObJPsNQJBAMGF+Ub2+mViALHAA0ffPYxVutDmsEYp9HGAEa9YXVXhSqDWqBPdw0QR88SwyBi3cyUnv4IOB9EcUWOznHuB7q0CQDepF6qcfaTzB/ZFxWv+1y5XBGubCWgTW6XDGhj3WIgIUs3DvDLmhZjjTHB6KaBR0+CAUj4CDHY9oo4CFQReDTg="

#define CLIENT_RSA_PUBLIC_KEY @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDMB+8mx+YmcKIgym+LER9kOLPccredAQtzsgg8cpJqIY1RurikHSekdOuD28fEZkOuIUoyA1/iffVrF+NVIUG1Jtm2KD1AC9vHcsKsjKS8CDGz/hmHWrJvLKbIK+IGohitYPcdWBTsgeGWUVpBmjb2KRwt9BOEbcgm97CYAjcQewIDAQAB"

#pragma mark - pkcs8 编码
#define CLIENT_RSA_PRIVATE_KEY @"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMwH7ybH5iZwoiDKb4sRH2Q4s9xyt50BC3OyCDxykmohjVG6uKQdJ6R064Pbx8RmQ64hSjIDX+J99WsX41UhQbUm2bYoPUAL28dywqyMpLwIMbP+GYdasm8spsgr4gaiGK1g9x1YFOyB4ZZRWkGaNvYpHC30E4RtyCb3sJgCNxB7AgMBAAECgYEAja3wxuhHDGhoXQbV4Cb6uniZhTpwAGmKT0PQblFNFL/Jx3g24VeRgKTkKoNCvEIbO17Ii+RGABxhgIVtCl92pRvjbND1gP35EJ/ToPJLuZbmvFPrNPJJVzG/xZAkB2uorEJTFrWqrOz9L55YpdubGOEGZj1LwdyQNVfjNk9siPECQQDvGkOtheifirunPIJ3hko26xEnshE9vce+QKI3bFgsxZ8HeafsdoN/UHTFGtEREVx4HFYqJeX06uumLjMFJXVzAkEA2nMrmVJIsYj8ZjejE5/cKNY/abiZ1Kth3mdd//ilSQZlPMP6DG0Lkhm271XVJdD3MCx45eFQ0bhtGIW3yH722QJBAIh0us8bLyluKa5F0EeaclxpRz8aVtCfsJEZnfp3cNMMbbf8bCN9d4p/RkT/CvXsrJM3yM0BF/3w3E/Y6fEz8ZUCQAPVTYiNNxVWcAdjJotrnLNivw4d3SUsNLVwXufk9xGG+TPNxI9/Qlq39MvSV8EoH45K2Xw/LengMJnjjswH3lECQDSQpALnitklIhadJcKRdzqfl8utUVwIcLAOeKXRRvPcnhNomKE7LBCnMGCpkwPk42oTgxAUR2dqw9cchmFyr5Y="

//#define CLIENT_RSA_PRIVATE_KEY @"MIICXQIBAAKBgQDMB+8mx+YmcKIgym+LER9kOLPccredAQtzsgg8cpJqIY1RurikHSekdOuD28fEZkOuIUoyA1/iffVrF+NVIUG1Jtm2KD1AC9vHcsKsjKS8CDGz/hmHWrJvLKbIK+IGohitYPcdWBTsgeGWUVpBmjb2KRwt9BOEbcgm97CYAjcQewIDAQABAoGBAI2t8MboRwxoaF0G1eAm+rp4mYU6cABpik9D0G5RTRS/ycd4NuFXkYCk5CqDQrxCGzteyIvkRgAcYYCFbQpfdqUb42zQ9YD9+RCf06DyS7mW5rxT6zTySVcxv8WQJAdrqKxCUxa1qqzs/S+eWKXbmxjhBmY9S8HckDVX4zZPbIjxAkEA7xpDrYXon4q7pzyCd4ZKNusRJ7IRPb3HvkCiN2xYLMWfB3mn7HaDf1B0xRrRERFceBxWKiXl9Orrpi4zBSV1cwJBANpzK5lSSLGI/GY3oxOf3CjWP2m4mdSrYd5nXf/4pUkGZTzD+gxtC5IZtu9V1SXQ9zAseOXhUNG4bRiFt8h+9tkCQQCIdLrPGy8pbimuRdBHmnJcaUc/GlbQn7CRGZ36d3DTDG23/GwjfXeKf0ZE/wr17KyTN8jNARf98NxP2OnxM/GVAkAD1U2IjTcVVnAHYyaLa5yzYr8OHd0lLDS1cF7n5PcRhvkzzcSPf0Jat/TL0lfBKB+OStl8Py3p4DCZ447MB95RAkA0kKQC54rZJSIWnSXCkXc6n5fLrVFcCHCwDnil0Ubz3J4TaJihOywQpzBgqZMD5ONqE4MQFEdnasPXHIZhcq+W"
/**
 @abstract  padding type
 */
typedef NS_ENUM(NSInteger, RSA_PADDING_TYPE) {
    
    RSA_PADDING_TYPE_NONE       = RSA_NO_PADDING,
    RSA_PADDING_TYPE_PKCS1      = RSA_PKCS1_PADDING,
    RSA_PADDING_TYPE_SSLV23     = RSA_SSLV23_PADDING
};

@interface BBRSACryptor : NSObject
{
    RSA *_rsaPublic;
    RSA *_rsaPrivate;
    RSA *_serverRsaPublic;
    RSA *_serverRsaPrivate;
    
@public
    RSA *_rsa;
}

/**
 Generate rsa key pair by the key size.
 @param keySize RSA key bits . The value could be `512`,`1024`,`2048` and so on.
 Normal is `1024`.
 */
- (BOOL)generateClientRSAKeyPairWithKeySize:(int)keySize;

/**
 @abstract  import server public key, call before 'encryptWithServerPublicKey'
 @param     publicKey with base64 encoded
 @return    Success or not.
 */
- (BOOL)importServerRSAPublicKeyBase64:(NSString*)publicKey;

/**
 @abstract  import server private key, call before 'encryptWithServerPrivateKey'
 @param     privateKey with base64 encoded
 @return    Success or not.
 */
- (BOOL)importServerRSAPrivateKeyBase64:(NSString*)privateKey;

/**
 @abstract  import public key, call before 'encryptWithPublicKey'
 @param     publicKey with base64 encoded
 @return    Success or not.
 */
- (BOOL)importClientRSAPublicKeyBase64:(NSString *)publicKey;

/**
 @abstract  import private key, call before 'decryptWithPrivateKey'
 @param privateKey with base64 encoded
 @return Success or not.
 */
- (BOOL)importClientRSAPrivateKeyBase64:(NSString *)privateKey;

/**
 @abstract  export server public key, 'importServerRSAPublicKeyBase64' should call before this method
 @return    server public key base64 encoded
 */
- (NSString*)base64EncodeServerPublicKey;

/**
 @abstract  export server private key, 'importServerRSAPrivateKeyBase64' should call before this method
 @return    server private key base64 encoded
 */
- (NSString*)base64EncodeServerPrivateKey;

/**
 @abstract  export public key, 'generateRSAKeyPairWithKeySize' or 'importClientRSAPublicKeyBase64' should call before this method
 @return    public key base64 encoded
 */
- (NSString *)base64EncodedPublicKey;

/**
 @abstract  export public key, 'generateRSAKeyPairWithKeySize' or 'importClientRSAPrivateKeyBase64' should call before this method
 @return    private key base64 encoded
 */
- (NSString *)base64EncodedPrivateKey;

/**
 @abstract  encrypt text using server RSA public key
 @param     padding type add the plain text
 @return    encrypted data
 */
-(NSData*)encryptWithServerPublicKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                       plainData:(NSData*)plainData;

/**
 @abstract  encrypt text using server RSA private key
 @param     padding type add the plain text
 @return    encrypted data
 */
-(NSData*)encryptWithServerPrivateKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                        plainData:(NSData*)plainData;

/**
 @abstract  encrypt text using RSA public key
 @param     padding type add the plain text
 @return    encrypted data
 */
- (NSData *)encryptWithPublicKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                   plainData:(NSData *)plainData;

/**
 @abstract  encrypt text using RSA private key
 @param     padding type add the plain text
 @return    encrypted data
 */
- (NSData *)encryptWithPrivateKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                    plainData:(NSData *)plainData;

/**
 @abstract  decrypt text using Server RSA public key
 @param     padding type add the plain text
 @return    encrypted data
 */
- (NSData *)decryptWithServerPublicKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                        cipherData:(NSData *)cipherData;

/**
 @abstract  decrypt text using Server RSA private key
 @param     padding type add the plain text
 @return    encrypted data
 */
- (NSData *)decryptWithServerPrivateKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                         cipherData:(NSData *)cipherData;


/**
 @abstract  decrypt text using RSA private key
 @param     padding type add the plain text
 @return    encrypted data
 */
- (NSData *)decryptWithPrivateKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                   cipherData:(NSData *)cipherData;

/**
 @abstract  decrypt text using RSA public key
 @param     padding type add the plain text
 @return    encrypted data
 */
- (NSData *)decryptWithPublicKeyUsingPadding:(RSA_PADDING_TYPE)padding
                                  cipherData:(NSData *)cipherData;
@end
