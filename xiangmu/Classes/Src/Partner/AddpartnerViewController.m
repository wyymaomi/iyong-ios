//
//  AddpartnerViewController.m
//  xiangmu
//
//  Created by David kim on 16/3/24.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AddpartnerViewController.h"
#import "MyUtil.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "UserManager.h"
#import "HttpConstants.h"
#import "LoginViewController.h"
//#import "ABRootViewController.h"
//#import "CNContactRootViewController.h"
#import "ContactListService.h"

@interface AddpartnerViewController ()<UITextFieldDelegate, ContactListServiceDelegate>
{
    UILabel *nicheng;
    NSString *userid;
}
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation AddpartnerViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.title = @"添加合作伙伴";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_phone_address"] style:UIBarButtonItemStyleDone target:self action:@selector(phoneContact)];
    [self createInterface];
}

-(void)createInterface
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, 160)];
    view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:view];
    
    //    UILabel *name=[MyUtil createLabelFrame:CGRectMake(view.frame.size.width/2-30, 40, 60, 40) title:@"手机号" font:[UIFont systemFontOfSize:18] textAlignment:NSTextAlignmentCenter numberOfLines:1 textColor:[UIColor blackColor]];
    //    [view addSubview:name];
    
    UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(20, 10, view.frame.size.width-40, 40)];
    textField.placeholder=@"输入手机号添加好友";
    textField.font=[UIFont systemFontOfSize:15];
    textField.textAlignment=NSTextAlignmentCenter;
    textField.keyboardType=UIKeyboardTypeNumberPad;
    textField.tag=100;
    textField.layer.borderWidth=0.5;
    textField.layer.borderColor=[[UIColor colorWithRed:229.0f/255.0f green:229.0f/255.0f blue:229.0f/255.0f alpha:1.0f]CGColor];
    textField.layer.cornerRadius=8.0f;
    [view addSubview:textField];
    textField.delegate = self;
    
    
    
    
    UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(view.frame.size.width-100, 0, 60, 40)];
//    rightView.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:193.0f/255.0f blue:65.0f/255.0f alpha:1.0f];
    rightView.backgroundColor = UIColorFromRGB(0x01C216);
    UIButton *button=[MyUtil createBtnFrame:CGRectMake(5, 5, 40, 30) title:@"搜索" bgImageName:nil target:self action:@selector(gotoSearch)];
    button.titleLabel.font=[UIFont boldSystemFontOfSize:18];//[UIFont systemFontOfSize:16];
    [rightView addSubview:button];
    textField.rightView = rightView;
    textField.rightViewMode = UITextFieldViewModeAlways;
    
    nicheng=[MyUtil createLabelFrame:CGRectMake(view.frame.size.width/2-75, 60, 150, 40) title:@"昵称:" font:[UIFont systemFontOfSize:15] textAlignment:NSTextAlignmentCenter numberOfLines:1 textColor:[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f]];
    [view addSubview:nicheng];
    
    UIButton *btn=[MyUtil createBtnFrame:CGRectMake(20, 110,ViewWidth-40, 40) title:@"申请" bgImageName:nil target:self action:@selector(gotoApply)];
    btn.titleLabel.font=[UIFont systemFontOfSize:15];
    btn .backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:179.0f/255.0f blue:249.0f/255.0f alpha:1.0f];
    btn.titleLabel.textColor=[UIColor whiteColor];
    btn.layer.cornerRadius=18.0f;
    [view addSubview:btn];
}

-(void)phoneContact
{
    ContactListService *contactListService = [[ContactListService alloc] initWithViewController:self];
    contactListService.delegate = self;
    [contactListService showContactList];
    
//    if (isIOS10 || isIOS9) {
//        
//        CNContactRootViewController *contactRootViewController = [CNContactRootViewController new];
//        contactRootViewController.delegate = contactRootViewController;
//        contactRootViewController.controllerDelegate = self;
//        [self presentViewController:contactRootViewController animated:YES completion:nil];
//        
//    }
//    else {
//        ABRootViewController *abRootViewController = [[ABRootViewController alloc] init];
//        abRootViewController.controllerDelegate = self;
//        abRootViewController.peoplePickerDelegate = abRootViewController;
//        //    NSArray *displayedItems = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonPhoneProperty],nil];
//        //    abRootViewController.displayedProperties = displayedItems;
//        if(IOS8_OR_LATER){
//            abRootViewController.predicateForSelectionOfPerson = [NSPredicate predicateWithValue:false];
//        }
//        [self presentViewController:abRootViewController animated:YES completion:nil];
//    }

}

- (void)onGetMobile:(NSString*)mobile;
{
    userid = nil;
    UITextField *textField = [self.view viewWithTag:100];
    textField.text = mobile;
}

//-(void)onGetContactPhone:(NSString *)phone
//{
//    userid = nil;
//    UITextField *textField = [self.view viewWithTag:100];
//    textField.text = phone;
//}
//
//-(void)onGetContactMobile:(NSString*)phone;
//{
//    [self onGetContactPhone:phone];
//}


-(void)gotoSearch
{
    NSLog(@"搜索");

    UITextField *textField=[self.view viewWithTag:100];
    
    
    
    NSString *searchText = textField.text;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^1[3|4|5|7|8][0-9]\\d{8}$" options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *result = [regex firstMatchInString:searchText options:0 range:NSMakeRange(0, [searchText length])];
    if (result) {
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Partner_Query_API];
        NSString *parmas = [NSString stringWithFormat:@"mobile=%@", textField.text];
        
        WeakSelf
        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
            NSArray *array=[responseData valueForKey:@"data"];
            NSLog(@"response success");
            nicheng.text=[NSString stringWithFormat:@"昵称:%@",[array valueForKey:@"nickname"]];
            userid=[array valueForKey:@"companyId"];
            NSLog(@"%@",userid);
            
            //                if ([array isKindOfClass:[NSNull class]])
            if (array==nil)
            {
                UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"提示" message:@"该用户不存在" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                nicheng.text=[NSString stringWithFormat:@"昵称:%@",@""];
                [alertController addAction:alertAction];
                [weakSelf presentViewController:alertController animated:YES completion:nil];
            }
            
//            DLog(@"result = %@", responseData);
            
        } andFailure:^(NSString *errorDesc) {
            
            DLog(@"response failure");
            
        }];
    }
    else
    {
        DLog(@"手机号错误");
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入11位手机号"preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertView addAction:alertAction];
        [self presentViewController:alertView animated:YES completion:nil];
    }
}

-(void)gotoBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)gotoApply
{


    if (userid.length == 0) {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先搜索好友" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertView addAction:alertAction];
        [self presentViewController:alertView animated:YES completion:nil];
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Partner_Apply_API];
    NSString *parmas = [NSString stringWithFormat:@"dstCompanyId=%@", userid];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        NSString *number=[responseData valueForKey:@"code"];
        NSLog(@"%@",number);
        NSLog(@"response success");
        if ([number isEqualToString:@"1000"]) {
            [weakSelf showAlertView:@"申请添加伙伴成功"];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:[number integerValue]]];
        }
        
//        DLog(@"result = %@", responseData);
        
    } andFailure:^(NSString *errorDesc) {
        
//        NSLog(@"response failure");
        
    }];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    UITextField *text = [self.view viewWithTag:100];
    [text resignFirstResponder];
    
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
