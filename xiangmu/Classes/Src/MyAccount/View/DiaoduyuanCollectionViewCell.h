//
//  DiaoduyuanCollectionViewCell.h
//  xiangmu
//
//  Created by David kim on 16/3/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiaoduyuanCollectionViewCell : UITableViewCell
@property (nonatomic,strong)UILabel *name;
@property (nonatomic,strong)UILabel *phoneLabel;
@end
