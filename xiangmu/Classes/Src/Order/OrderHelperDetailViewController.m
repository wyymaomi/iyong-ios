//
//  OrderHelperDetailViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderHelperDetailViewController.h"
#import "OrderHelpDetailTableViewCell.h"

@interface OrderHelperDetailViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *helpContents;

@end

@implementation OrderHelperDetailViewController


-(NSArray*)helpContents
{
    if (IsArrEmpty(_helpContents)) {
        // 加载plist中的字典数据
        NSString *path = [[NSBundle mainBundle] pathForResource:ORDER_HELP_PLIST ofType:nil];
        NSArray *array = [NSArray arrayWithContentsOfFile:path];
        _helpContents = array[self.selectedIndex];
    }
    return _helpContents;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.helpContents.count;
//    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderHelpDetailTableViewCell *cell = [OrderHelpDetailTableViewCell cellWithTableView:tableView];
    
    NSDictionary *dictioanry = self.helpContents[indexPath.section];
    
    cell.titleLabel.text = dictioanry[@"title"];
    cell.detailLabel.text = dictioanry[@"detail"];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictioanry = self.helpContents[indexPath.section];
    NSString *detailText = dictioanry[@"detail"];
    CGFloat height = [StringUtil heightForString:detailText fontSize:14 andWidth:(tableView.frame.size.width-30)] + 45 + 30;
    return height;
}

@end
