//
//  BillTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BillTableViewCell.h"

@implementation BillTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"BillTableViewCellID";
    BillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
//        cell.backgroundColor = UIColorFromRGB(0xF7F7F7);
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.orderInfoView.tag = indexPath.section;
        
//        cell.tag = indexPath.row;
//        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectRow:)];
//        cell.orderInfoView
    }
    return cell;
}

+(CGFloat)getCellHeight
{
    return 174;
}

-(void)initData:(BillOrderModel*)data type:(NSString*)typeName
{
    self.upCompanyNameLabel.text = IsStrEmpty(data.upCompanyName)?@"自派":data.upCompanyName;
    self.orderStatusLabel.text = typeName;
    self.carDateLabel.text = [data.carTime getDateTime];
    self.customerInfoLabel.text = [NSString stringWithFormat:@"%@ %@", data.customerName, data.customerMobile.trim];
    self.startLocationLabel.text = data.upAddress;
    self.arriveLocationLabel.text = data.downAddress;
    self.orderPriceLabel.text = [NSString stringWithFormat:@"结算金额：¥%@", data.price];
    [self.orderPriceLabel setTextColor:UIColorFromRGB(0xD51618) text:[NSString stringWithFormat:@"¥%ld", (long)[data.price integerValue]]];
    
    [self.orderInfoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doSelectCell:)]];
}

-(void)initSalesData:(SalesModel*)data
{
//    self.upCompanyNameLabel.text = data.upCompanyName;
    if ([[UserManager sharedInstance] isSelfDispatch:data.upCompanyId] || IsStrEmpty(data.upCompanyId)) {
        self.upCompanyNameLabel.text = @"自派";
    }
    else {
        self.upCompanyNameLabel.text = [StringUtil getSafeString:data.upCompanyName];
        //IsStrEmpty(data.upCompanyName)?@"自派":data.upCompanyName;
    }
    
    self.carDateLabel.text = [data.carTime getDateTime];
    self.startLocationLabel.text = data.upAddress;
    self.arriveLocationLabel.text = data.downAddress;
    self.orderPriceLabel.text = [NSString stringWithFormat:@"已结算金额：¥%@", data.price];
    self.orderStatusLabel.text = @"已完成";
    self.customerInfoLabel.text = [NSString stringWithFormat:@"%@ %@", data.customerName, data.customerMobile];
    [self.orderPriceLabel setTextColor:UIColorFromRGB(0xD51618) text:[NSString stringWithFormat:@"¥%ld", (long)[data.price integerValue]]];
    
    [self.orderInfoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doSelectCell:)]];
    
}

-(void)doSelectCell:(UIView*)view
{
    self.orderInfoView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
        
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(onSelectRowAtIndex:)]) {
            [weakSelf.delegate onSelectRowAtIndex:weakSelf.orderInfoView.tag];
        }
        weakSelf.orderInfoView.backgroundColor = [UIColor whiteColor];
        
    });
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
