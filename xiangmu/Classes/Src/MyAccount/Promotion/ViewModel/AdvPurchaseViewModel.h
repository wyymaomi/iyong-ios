//
//  AdvPurchaseViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewModel.h"
#import "AdvSumModel.h"
#import "AdvDetailModel.h"
#import "AdPurchaseOrder.h"

@interface AdvPurchaseViewModel : BaseViewModel

@property (nonatomic, strong) NSString *currentSelectDate;// 当前选中的日期
@property (nonatomic, strong) NSString *currentSelectTImeSegment; // 当前选中的时间段
@property (nonatomic, strong) NSNumber *countLeft; // （剩余可购买数量）

@property (nonatomic, strong) NSMutableArray<AdvSumModel*> *saleAdvSumList;
@property (nonatomic, strong) NSMutableArray<AdvDetailModel*> *saleAdvDetailList;
@property (nonatomic, strong) NSMutableArray<AdPurchaseOrder*> *advOrderList;
@property (nonatomic, strong) NSMutableDictionary *advSumDictionary; // 可售广告字典

@end
