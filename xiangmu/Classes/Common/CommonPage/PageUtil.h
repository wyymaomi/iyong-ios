//
//  PageUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/27.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PageUtil : NSObject

@property (nonatomic, assign) NSInteger pageIndex;

@end
