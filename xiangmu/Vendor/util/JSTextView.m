//
//  JSTextView.m
//  xiangmu
//
//  Created by David kim on 16/4/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSTextView.h"
#import <UIKit/UIKit.h>

@interface JSTextView()
//先拿出这个label以方便我们后面的使用
@property(nonatomic,weak)UILabel *placeholderLabel;

@end

@implementation JSTextView

-(instancetype)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor clearColor];
        
        UILabel *placeholderLabel=[[UILabel alloc]init];//添加一个占位label
        placeholderLabel.backgroundColor=[UIColor clearColor];
        placeholderLabel.numberOfLines=0;//设置可以输入多行文字时可以自动换行
        [self addSubview:placeholderLabel];
        
        self.placeholderLabel=placeholderLabel;//赋值保存
        self.myPlaceholderColor=[UIColor lightGrayColor];//设置占位文字默认颜色
        self.font=[UIFont systemFontOfSize:15];//设置默认的字体
        
        //通知监听文字的改变
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:self];
    }
    return self;
}

//监听文字改变
-(void)textDidChange
{
    //这个hasText是一个系统的BOOL属性,如果UItextview输入文字hastext就是YES,反之就为NO
    self.placeholderLabel.hidden=self.hasText;
}


//在-(void)layoutSubviews里面设置uilabel的frame
-(void)layoutSubviews
{
    [super layoutSubviews];
   //设置UIlabel的y值
    
    //设置UIlabel的x值
//    self.placeholderLabel.widthAnchor=self.widthAnchor-self.placeholderLabel.centerXAnchor*2.0;
    //self.placeholderLabel.frame.origin.y=8;
    CGRect frame=self.placeholderLabel.frame;
    frame.origin.y=8;
    frame.origin.x=5;
    frame.size.width=self.frame.size.width-frame.origin.x*2.0;
    CGSize maxSize=CGSizeMake(frame.size.width, MAXFLOAT);
    frame.size.height=[self.myPlaceholder boundingRectWithSize:maxSize options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.placeholderLabel.font} context:nil].size.height;
    self.placeholderLabel.frame=frame;
   // self.placeholderLabel.frame=CGRectMake(5, 8, self.frame.size.width-10, 10);
    
}

-(void)setMyPlaceholder:(NSString *)myPlaceholder
{
    _myPlaceholder=[myPlaceholder copy];
    //设置文字
    self.placeholderLabel.text=myPlaceholder;
    //重新计算子控件frame
    [self setNeedsLayout];
}


-(void)setMyPlaceholderColor:(UIColor *)myPlaceholderColor
{
    _myPlaceholderColor=myPlaceholderColor;
    //设置颜色
    self.placeholderLabel.textColor=myPlaceholderColor;
}



-(void)setFont:(UIFont *)font
{
    [super setFont:font];
    self.placeholderLabel.font=font;
    //重新计算子控件
    [self setNeedsLayout];
}


-(void)setText:(NSString *)text
{
    [super setText:text];
    //这里调用的是uitextviewtextdidchangenotification通知的回调
    [self textDidChange];
}


-(void)setAttributedText:(NSAttributedString *)attributedText
{
    [super setAttributedText:attributedText];
    
    ///这里调用的是uitextviewtextdidchangenotification通知的回调
    [self textDidChange];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:UITextViewTextDidChangeNotification];
}













@end
