//
//  HttpConstants.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef HttpConstants_h
#define HttpConstants_h

#pragma mark HTTP服务器域名

#if DEBUG
#define HTTP_BASE_URL @"http://192.168.1.199:7070/iyong-web"
//#define HTTP_BASE_URL @"http://app.chinaiyong.com:8080/iyong-web"
#else
//#define HTTP_BASE_URL @"http://192.168.1.199:7070/iyong-web"
#define HTTP_BASE_URL @"http://app.chinaiyong.com:8080/iyong-web"
#endif

#pragma mark - HTTP response code
typedef NS_ENUM(NSInteger, HTTP_RESPONSE_CODE) {
    STATUS_COPY       =1302,
    STATUS_OK         = 1000,//数据下载成功
    STATUS_FAIL       = 1001,//失败
    NETWORK_FAILED    = 1//网络问题
};

//#define REQUEST_TIME_OUT 30

#pragma mark - 接口API
//登录
#define LOGIN_API @"login/login"
//注册
#define Register_API @"login/register"
#define REGISTER_VERIFY_CODE_API @"sms/get_register_captcha" // 注册获取验证码
#define VERIFY_CODE_API @"sms/get_captcha" // 获取验证码
#define RESET_PWD_API @"login/reset_password" // 忘记密码
#pragma mark - 手势密码相关API
#define SET_GESTURE_PWD_API @"user/set_signPwd" // 设置手势密码
#define HAS_GESTURE_PWD_API @"user/has_signPwd" // 是否已设置手势密码
#define VALIDATE_GESTURE_PWD_API @"user/validate_signPwd" // 验证手势密码(请求模式与登录相同)
#define GET_FINANCE_API @"company/get_finance" // 获取财务信息接口
//车辆列表
#define CAR_LIST_API @"car/list"
//添加车辆
#define CAR_SAVE_API @"car/save"
//删除车辆
#define CAR_Delete_API @"car/delete"
//编辑车辆里面的数据显示
#define CAR_Query_API @"car/query"
//编辑车辆
#define CAR_Update_API @"car/update"
//添加合作伙伴 搜索
#define Partner_Query_API @"company/query"
//添加合作伙伴 申请
#define Partner_Apply_API @"partner/apply"
//添加合作伙伴 待通过列表
#define Partner_ApplyList_API @"partner/applylist"
//添加合作伙伴 待通过中通过
#define Partner_Accept_API @"partner/accept"
//添加合作伙伴 待通过中不通过
#define Partner_Ignore_API @"partner/ignore"
//我的合作伙伴 列表
#define Partner_List_API @"partner/list"
//账单
#define Order_Partner_list_API @"partner/partner_list"
//我的信息
#define My_Detail_API @"user/query"
//调度员列表
#define Diaodu_List_API @"dispatcher/list"
//添加调度员
#define Diaodu_Add_API @"dispatcher/add_dsp"
//添加调度员
#define Diaodu_Add_API @"dispatcher/add_dsp"
//删除调度员
#define Diaodu_Delete_API @"dispatcher/delete"
//编辑调度员
#define Diaodu_Update_API @"dispatcher/update"
//修改用户信息
#define Yonghu_Update_API @"user/update"
//添加推荐人
#define Company_Add_Inviter @"company/add_inviter"
//推荐人列表
#define Company_Inviter_List @"company/inviter_list"

#pragma mark - 下载图片接口
#define DONWLOAD_IMAGE_API @"transfer/download"

#pragma mark - 营业查询接口
#define SALES_LIST_API @"business/list" // 营业查询列表
#define SALES_SUMMARY_API @"business/summary" // 营业查询

#pragma mark - 交易记录接口
#define TRADE_RECORD_LIST_API @"transaction/list" // 交易列表
#define TRADE_SUMMARY_API @"transaction/summary" // 月度交易汇总

#pragma mark - 登录密码更改接口
#define LOGIN_PWD_CHANGE_API @"user/update_psd" // 登录密码更新

#pragma mark - 支付密码接口
#define PAY_PWD_HAS_API @"user/has_payPwd"// 是否有支付密码接口
#define PAY_PWD_SET_API @"user/set_payPwd" // 设置支付密码接口
#define PAY_PWD_VALID_API @"user/validate_payPwd" // 验证支付密码接口

#pragma mark - 账单管理接口
#define BILL_LIST_COLLECTED_API @"bill/collected_list" //已收款账单列表接口
#define BILL_LIST_UNCOLLECTED_API @"bill/uncollected_list" // 未收款账单列表接口
#define BILL_LIST_PAID_API @"bill/alreadyPaid_list" // 已付款账单列表
#define BILL_LIST_UNPAID_API @"bill/unpaid_list" // 未付款账单列表
#define CREDIT_UNREPAYMENT_LIST_API @"bill/norepayment_list" // 未还款信用账单列表接口

#pragma mark - 订单接口
#define ORDER_PARTNER_LIST_API @"partner/list"// 派单对象接口
#define ORDER_SMS_API @"order/sms_send" // 订单发送短信接口
#define ORDER_COPY_API @"order/order_copy" // 复制订单信息接口

#define ORDER_SUBMIT_API @"order/submit"
#define ORDER_SAVE_API @"order/save_order" // 保存订单信息
#define ORDER_DISPATCH_API @"order/submit_chain" // 保存派单信息
#define ORDER_SAVE_CARINFO_API @"order/save_carinfo" // 安排车辆：选择车辆
#define ORDER_ARRANGE_VECHILE_API @"order/submit_carinfo" // 安排车辆：司机已确认
#define ORDER_SAVE_WAYBILL_API @"order/submit_waybill" // 保存路单信息
#define ORDER_BARGAIN_API @"order/submit_bargain" // 请求结算
#define ORDER_AGREE_PAY_API @"order/submit_agreePay" // 同意付款
#define ORDER_PAY_API @"order/submit_pay" // 付款


#define ORDER_UPDATE_ORDER @"order/update_order" // 修改订单信息
#define ORDER_ROLLBACK_CHAIN @"order/rollback_chain" // 派单改派
#define ORDER_ROLLBACK_CARINFO @"order/rollback_carinfo" // 派单改派
#define ORDER_UPDATE_WAYBILL @"order/update_waybill" // 修改路单

#define ORDER_LIST_API @"order/list" // 订单列表查询（车辆安排中：3，填写路单中：4）参数：status，time
#define ORDER_LIST_UNTREATED_API @"order/list/untreated" // 未处理订单列表查询
#define ORDER_LIST_STROKE_API @"order/list/stroke" // 行程中订单列表查询
#define ORDER_LIST_UNBARGAIN_API @"order/list/unbargain" // 待结算订单列表查询
#define ORDER_LIST_UNPAID_API @"order/list/unpaid" // 待付款订单列表查询
#define ORDER_LIST_COMPLETED_API @"order/list/completed" // 已完成订单列表查询

#define ORDER_DETAIL_API @"order/detail" // 订单详情

#define ORDER_DETAIL_BASE_API @"order/detail_order" // 订单基础信息
#define ORDER_DETAIL_CHAIN_API @"order/detail_chain"// 派单
#define ORDER_DETAIL_CARINFO_API @"order/detail_carinfo" // 安排车辆
#define ORDER_DETAIL_WAYBILL_API @"order/detail_waybill" // 路单
#define ORDER_DETAIL_BARGIN_API @"order/detail_bargain" // 结算

#endif /* HttpConstants_h */
