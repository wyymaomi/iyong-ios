//
//  AuthorizationUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthorizationUtil : NSObject

+ (BOOL)isAllowPushNotification;

+ (BOOL)isAllowAlbumn;

+ (BOOL)isAllowVideo;

+(BOOL)isAllowContact;

@end
