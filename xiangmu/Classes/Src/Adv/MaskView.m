//
//  MaskView.m
//  
//
//  Created by 湛思科技 on 2017/5/26.
//
//

#import "MaskView.h"

@interface MaskView ()

@property (nonatomic, strong) UIWindow      *maskWindow;
@property (nonatomic, strong) UIView        *maskView;

@end

@implementation MaskView

- (UIWindow *)maskWindow
{
    if (!_maskWindow) {
        
        _maskWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        //edited by gjf 修改alertview leavel
        _maskWindow.windowLevel = UIWindowLevelStatusBar + BBAlertLeavel;
        //        _maskWindow.windowLevel = UIWindowLevelStatusBar + BBAlertLeavel;
        _maskWindow.backgroundColor = [UIColor clearColor];
        _maskWindow.hidden = YES;
        
        _maskView= ({
            UIView *view = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
            view.backgroundColor = [UIColor blackColor];
            view.alpha = 0.7;
            view;
        });
        [_maskWindow addSubview:_maskView];
        
        _maskView.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissWindow:)];
        sigleTapRecognizer.numberOfTapsRequired = 1;
        [_maskView addGestureRecognizer:sigleTapRecognizer];
        //        _maskView = maskView;
        
        
//        _bgView = ({
//            
//            UIView *view =[[UIView alloc]initWithFrame: CGRectMake(0, 0, ViewWidth, /*1.46*0.825*Screen_width*/SCROLL_VIEW_HEIGHT)];
//            //            view.frame = CGRectMake(0, 0, 0.8*Screen_width, 1.46*0.825*Screen_width);
//            view.center = CGPointMake(Screen_width/2, Screen_height/2);
//            //            if (isIphone4) {
//            //                self.center = CGPointMake(Screen_width/2, Screen_height/2+20);
//            //            }
//            view.backgroundColor = [UIColor clearColor];
//            //            view.backgroundColor = [UIColor whiteColor];
//            view.layer.cornerRadius = 6.0;
//            view.clipsToBounds = YES;
//            //            view.layer.borderWidth=1;
//            //            view.layer.borderColor=superid_ad_color_title.CGColor;
//            view;
//            
//        });
        
//        [_maskWindow addSubview:_bgView];
        
        
        //        _otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        _otherBtn.frame = CGRectMake(VIEW_BX(_bgView) - 115, _bgView.top - 40, 115, 30);
        //        [_otherBtn setCornerRadius:5];
        //        [_otherBtn setTitle:@"查看更多活动" forState:UIControlStateNormal];
        //        [_otherBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //        _otherBtn.titleLabel.font = FONT(14);
        //        [_otherBtn setBackgroundImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateNormal];
        //        [_maskWindow addSubview:_otherBtn];
        
//        _otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        _otherBtn.frame = CGRectMake(ViewWidth-38*Scale, 142.5*Scale, 15*Scale, 15*Scale);
//        [_otherBtn setBackgroundImage:[UIImage imageNamed:@"icon_exit"] forState:UIControlStateNormal];
//        [_otherBtn addTarget:self action:@selector(onAdvDismiss:) forControlEvents:UIControlEventTouchUpInside];
//        
//        [_maskWindow addSubview:_otherBtn];
        
    }
    return _maskWindow;
}

- (id)init{
    
    self = [super init];
    
    if (self) {
        
        self.userInteractionEnabled = YES;
        
        self.backgroundColor = [UIColor clearColor];
        
        //        [UMSocialUIManager setShareMenuViewDelegate:self];
        
    }
    
    return self;
}

//-(void)showAdv:(NSArray<AdvModel*>*)advList
-(void)showMaskView
{
    // 显示在主界面上
    
    [self.maskWindow makeKeyAndVisible];
    
//    self.scrollView.contentSize = CGSizeMake(ViewWidth * pageCount, SCROLL_VIEW_HEIGHT);
//    //    self.scrollView.contentSize = CGSizeMake(SCROLL_VIEW_WIDTH * pageCount, SCROLL_VIEW_HEIGHT);
//    //    self.scrollView.contentSize = CGSizeMake(0.825*Screen_width, 1.46*0.825*Screen_width);
//    //    self.scrollView.contentSize = CGSizeMake(0.825*Screen_width * images.count, 1.46*0.825*Screen_width);
//    [self.bgView addSubview:self.scrollView];
//    
//    [self.maskWindow addSubview:self.pageControl];
//    
//    self.pageControl.numberOfPages = pageCount;
//    //    self.pageControl.numberOfPages = 1;
//    //    self.pageControl.numberOfPages = images.count;
//    self.pageControl.currentPage = 0;
//    self.pageControl.top = self.bgView.bottom + 10;
    
    //    if (color) {
    //        strongSelf.bgView.layer.borderColor = color.CGColor;
    //    }
    
    
    
    
}

-(void)dismissWindow:(id)sender
{
    [self.maskWindow removeFromSuperview];
    self.maskWindow = nil;
}


@end
