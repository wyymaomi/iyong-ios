//
//  BargainResponseModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"


@protocol RateModel
@end

@interface RateModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;// rateID
@property (nonatomic, strong) NSString<Optional> *price;// 实际支付价格
@property (nonatomic, strong) NSString<Optional> *name;// 还款方式name
@property (nonatomic, strong) NSNumber<Optional> *payRate;// 费率
@property (nonatomic, strong) NSNumber<Optional> *payType; // 付款方式 1:信用支付；2:余额支付

@end



@interface BarginModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *companyInfo;
@property (nonatomic, strong) NSNumber<Optional> *confirm;
@property (nonatomic, strong) NSNumber<Optional> *dstPrice;// 结算价格
@property (nonatomic, strong) NSString<Optional> *editDstPrice;
@property (nonatomic, strong) NSNumber<Optional> *estimatePrice;// 预计价格
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *orderId;
@property (nonatomic, strong) NSArray<RateModel,Optional> *rates; // 结算费率
@property (nonatomic, strong) NSNumber<Optional> *srcPrice;
@property (nonatomic, strong) NSString<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *type;

@end






@interface PayModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *companyId;
@property (nonatomic, strong) NSString<Optional> *companyInfo;
@property (nonatomic, strong) NSNumber<Optional> *confirm;
@property (nonatomic, strong) NSNumber<Optional> *dstPrice;// 结算价格
@property (nonatomic, strong) NSNumber<Optional> *estimatePrice;// 预计价格
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *orderId;
@property (nonatomic, strong) NSNumber<Optional> *srcPrice;
@property (nonatomic, strong) NSString<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSArray<RateModel,Optional> *rates; // 结算费率
@property (nonatomic, strong) NSString<Optional> *payType;
@property (nonatomic, strong) NSString<Optional> *payPrice;

@end



@interface BarginAndPayResponseModel : JSONModel

@property (nonatomic, strong) BarginModel<Optional> *bargain;
@property (nonatomic, strong) PayModel<Optional> *pay;
@property (nonatomic, strong) NSString<Optional> *selected;
@property (nonatomic, strong) NSString<Optional> *unselected;

@end
