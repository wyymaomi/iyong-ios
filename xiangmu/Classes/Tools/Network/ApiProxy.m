//
//  ApiProxy.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ApiProxy.h"

@implementation ApiProxy

#pragma mark - life cycle
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static ApiProxy *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ApiProxy alloc] init];
    });
    return sharedInstance;
}

- (void)callApiWithParams:(NSString*)url params:(NSDictionary*)params requestType:(RequestType)requestType success:(ApiSuccessHandle)success fail:(ApiFailHandle)fail;
{
    AFHTTPRequestOperationManager *manager = self.requestManager;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;

    if (requestType == RequestTypePost) {
        
        [manager POST:url parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
            if (success) {
                success(responseObject);
            }
            
        } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
            if (fail) {
                fail(error);
            }
            
        }];
        
    }
    
}



- (AFHTTPRequestOperationManager*)requestManager
{
    if (_requestManager == nil) {
        _requestManager = [AFHTTPRequestOperationManager manager];
    }
    
    return _requestManager;
}


@end
