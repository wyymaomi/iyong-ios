//
//  EnumConstants.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#ifndef EnumConstants_h
#define EnumConstants_h

typedef NS_ENUM(NSUInteger, InvoiceTextFieldType)
{
    InvoiceTextFieldTypeTitle=0,
    InvoiceTextFieldTypeTaxpayerNumber,
    InvoiceTextFieldTypeRegistAddress,
    InvoiceTextFieldTypeBankName,
    InvoiceTextFieldTypeBankAccount,
    InvoiceTextFieldTypeContacterName,
    InvoiceTextFieldTypeContactPhoneNumber,
    InvoiceTextFieldTypeLocationArea,
    InvoiceTextFieldTypeDetailAddress,
    InvoiceTextFieldTypeCustomMemo
};

//订单状态
//1=待接单，2=待支付,(3=支付确认中,4=支付失败，5=已过期)，6=行程待开始，7=行程已开始，8=已完成，9=已取消
typedef NS_ENUM(NSUInteger, CarOrderStatus)
{
    CarOrderStatusWaitReceive=1,
    CarOrderStatusPendingPay=2,
    CarOrderStatusPayInConfirming=3,
    CarOrderStatusPayFailure=4,
    CarOrderStatusPayExpire=5,
    CarOrderStatusTravelWaitStart=6,
    CarOrderStatusTravelStarted=7,
    CarOrderStatusTravelComplete=8,
    CarOrderStatusTravelCancelled=9
};

typedef NS_ENUM(NSInteger, SpecialAdvType)
{
    SpecialAdvTypeRedpack = 1
};

//1.信用余额，2=账户余额，3=推广余额，4=支付宝, 5=iyong，6=微信支付
typedef NS_ENUM(NSInteger, AdPayType)
{
    AdPayTypeBalanceAccount = 1,
    AdPayTypeMyAccount,
    AdPayTypePromotionAccount,
    //    AdPayTypeMyAccount,
    AdPayTypeAlipay,
    AdPayTypeiYONG,
    AdPayTypeWechat
};

typedef NS_ENUM(NSUInteger, AdPurchaseFlag)
{
    AdPurchaseFlagCanbuy = 1,// 可购买
    AdPurchaseFlagCannotBuy = 0 // 不可购买
};

typedef NS_ENUM(NSUInteger, AdOrderStatus)
{
    AdOrderStatusWaitPay=1,// 待支付
    AdOrderStatusPaySuccess,// 支付成功
    AdOrderStatusPayFailure,// 支付失败
    AdOrderStatusExpire,// 过期
    AdOrderStatusCancelled,// 取消
    AdOrderStatusPendingRefund,// 待退款
    AdOrderStatusRefunded//已退款
};

typedef NS_ENUM(NSUInteger, AdvType)
{
    AdvTypeBanner=1,
    AdvTypeOnlineProvider,
    AdvTypeLocalArea
};

typedef NS_ENUM(NSUInteger, AdPurchaseStatus)
{
    AdPurchaseStatusNotBuy=1,// 未购买
    AdPurchaseStatusHaveBuyed,// 已购买
    AdPurchaseStatusLocked// 已锁定
};

//typedef NS_ENUM(NSUInteger, AdPurchaseType)
//{
//    AdPurchaseTypePromotionAccount,
//    AdPurchaseTypeMyAccount,
//    AdPurchaseTypeWechat,
//    AdPurchaseTypeAlipay
//};

typedef NS_ENUM(NSUInteger, RegisterType)
{
    RegisterTypeMobile=0,     // 手机号注册
    RegisterTypeThirdparty  // 第三方注册
};

typedef NS_ENUM(NSUInteger, ThirdpartyId)
{
    ThirdpartyIdWechat=0,   // 微信
    ThirdpartyIdQQ,         // QQ
    ThirdpartyIdWeibo      // 微博
};

typedef NS_ENUM(NSInteger, AdvPurchaseRow)
{
    AdvPurchaseRowBanner=0,// 轮播图
    AdvPurchaseRowOnlienProvider,// 在线商户
    AdvPurchaseRowLocalArea// 所属地区
};

typedef NS_ENUM(NSInteger, HomeTableRow)
{
    HomeTableRowBanner,// 轮播图
    HomeTableRowBusinessType, // 业务范围
    HomeTableRowRecommend,// 推荐
    HomeTableRowServicer,// 在线服务商
//    HomeTableRowExperience,// 用车体验
    HomeTableRowGpsCity, // gps定位城市
//    HomeTableRowBeijing,// 北京
//    HomeTableRowShanghai,// 上海
//    HomeTableRowHangzhou,// 杭州
//    HomeTableRowShenzhen// 深圳
    HomeTableRowWedding,
    HomeTableRowTravel,
    HomeTableRowCommercial,
    HomeTableRowAirport,
    HomeTableRowBus,
    HomeTableRowCharter,
    HomeTableRowGoods,
    HomeTableRowTruck
};

typedef NS_ENUM(NSInteger, BusinessType)
{
    BusinessTypeNone = 0,
    BusinessTypeWedding=1,
    BusinessTypeTravel,
    BusinessTypeCharter,
    BusinessTypeCommercial,
    BusinessTypeAirport,
    BusinessTypeCompanyBus,
    BusinessTypeGoods,
    BusinessTypeTruck
};

//typedef NS_ENUM(NSInteger, BusinessType)
//{
//    BusinessTypeNone=0,
//    BusinessTypeCommercial,
//    BusienssTypeTravel,
//    BusinessTypeAirport,
//    BusienssTypeGoods,
//    BusinessTypeWedding,
//    BusinessTypeCharter,
//    BusinessTypeTruck,
//    BusienssTypeBus
//};

//typedef NS_ENUM(NSInteger, ProviderType)
//{
//    ProviderTypeCommercial = 1,// 商务用车
//    ProviderTypeTravel,     // 旅游用车
//    ProviderTypeAirport,    // 机场接送
//    ProviderTypeGoods,      // 货车服务
//    ProviderTypeWedding,    // 婚车服务
//    ProviderTypeCharter,    // 包车服务
//    ProviderTypeTruck,      // 拖车服务
//    ProviderTypeBus         // 企业班车
//};

typedef NS_ENUM(NSInteger, RechargeType) {
    RechargeTypeAlipay = 4,
    RechargeTypeWechat = 6,
    RechargeTypeIPS = 8
};

typedef NS_ENUM(NSInteger, VechileServiceType) {
    VechileServiceTypeWedding=1,
    VechileServiceTypeTravel,
    VechileServiceTypeChartered,
    VechileServiceTypeCommericial,
    VechileServiceTypeAirport,
    VechileServiceTypeRegularBus,
    VechileServiceTypetransport,
    VechileServiceTypeTruck
};

typedef NS_ENUM(NSInteger, CompanyCertificationStatus) {
    CompanyCertificationStatusInit = 1,
    CompanyCertificationStatusInReview,
    CompanyCertificationStatusFinished,
    CompanyCertificationStatusRejected
};

// 编辑状态：增加 修改
typedef NS_ENUM(NSInteger, EditMode){
    EditModeAdd,
    EditModeEdit
};

typedef NS_ENUM(NSInteger, SOURCE_FROM) {
    SOURCE_FROM_LOGIN,
    SOURCE_FROM_ABOUT_ME,
    SOURCE_FROM_OTHER,
    SOURCE_FROM_NEW_ORDER, // 订单新建界面
    SOURCE_FROM_ORDER_MANAGER, // 订单管理界面
    SOURCE_FROM_ORDER_LIST,// 订单列表
    SOURCE_FROM_SALES_QUERY, // 营业查询
    SOURCE_FROM_COMMUNITY_PUBLISH, // 社区发布
    SOURCE_FROM_MY_PARTNER,
    SOURCE_FROM_TAB_CONTROL,
    SOURCE_FROM_HOME,
    SOURCE_FROM_REGISTER, // 注册页面
    SOURCE_FROM_COMPANY_COMMUNITY // 社区发布
};

typedef NS_ENUM(NSInteger, OrderEditMode){
    OrderNew = 0,
    OrderEdit
};

typedef NS_ENUM(NSInteger, OrderMode) {
    OrderModeManager = 0,   // 订单管理
    OrderModeDetail = 1     // 订单详情
};

typedef NS_ENUM(NSInteger, TextFieldTag) {
    OrderTextFieldCarTime = 10000,
    OrderTextFieldCarModel,
    OrderTextFieldStartLocation,
    OrderTextFieldCustomerName,
    OrderTextFieldCustomerMobile,
    OrderTextFieldItinerary,
    OrderTextFieldRemark,
    OrderTextFieldOriginPrice,              // 新建订单／编辑订单 接单价格
    OrderTextFieldDispatchPrice,
    OrderTextFieldDispatchObject,
    OrderTextFieldArriveLocation,
    OrderTextFieldEndtime,
    OrderTextFieldDistance,
    OrderTextFieldRoadRemark,
    OrderTextFieldSettlementPrice,
    MyNicknameTextField,                    // 昵称输入框
    MyCompanyTextField,                     // 公司名称
    CreditApplyUserName,                    // 申请人姓名
    CreditApplyCompanyName,                 // 申请公司名称
    CreditApplyMobile,                      // 申请手机号
    CreditApplyQuota,                       // 申请额度
    CommunityTextFieldLocation,             // 当前位置
    CompanyTextFieldBusinessAddress,        // 企业经营地址
    CompanyTextFieldCompanyName,            // 公司名称
    DriverLicenseNoTextField,               // 驾驶证件号
    DriverLicenseGetDateTextField,          // 驾驶证领取日期
    DriverLicenseValidateDateTextField,     // 驾驶证有效日期
    VehicleLicenseGetDateTextField,         // 行驶证领取日期
    VechileLicenseValidateDateTextField,    // 行驶证有效日期
    IDNumberTextField,                      // 证件号
    IDTypeTextField,                        // 证件类型
    IDValidateDateTextField,                // 证件有效时间
    InquireStartDateTime,                   // 询价出发时间
    InquireEndDateTime,                     // 询价结束时间
    InquireStartLocation,                   // 询价出发地点
    InquireEndLocation,                     // 询价结束地点
    InquireVechileModel,                    // 询价车型
    InquireVechileNumber,                   // 询价数量
    InquireRoughtTravel,                    // 询价大致形成
    InquirePbulishTimeSegment,              // 询价发布时间段
    UseCarTelephone                         // 用车电话号码
};

typedef NS_ENUM(NSInteger, VechileTextFieldFlag) {
    VechileTextFieldModel = 20000,
    VechileTextFieldNumber,
    VechileTextFieldName,
    VechileTextFieldMobile
};

/**
 @enum      OrderSection)
 @abstract  订单详情列表界面section
 */
typedef NS_ENUM(NSInteger, OrderSection) {
    OrderSectionNew = 0,//新建订单
    OrderSectionDispatch,//派单信息
    OrderSectionVechileArrange,// 车辆安排
    OrderSectionWayBill,// 路单信息
    OrderSectionPay// 结算信息
};

/**
 @enum      OrderStatus
 @abstract  订单流程状态
 */
typedef NS_ENUM(NSInteger, OrderStatus) {
    OrderStatusNew = 1,             // 新建订单
    OrderStatusDispatch = 2,        // 派单信息
    OrderStatusVechileArrange = 3,  // 车辆安排
    OrderStatusWayBill = 4,         // 路单信息
    OrderStatusBarginAndPay = 5,    // 结算信息
    OrderStatusFinish = 6,          // 已结束
    OrderStatusTravelling = 11      // 行程中
};

/**
 @enum      OrderInfoType
 @abstract  基础信息
 */
typedef NS_ENUM(NSInteger, OrderInfoType) {
    OrderInfoTypeNew = 1,  // 新建
    OrderInfoTypeBrowsableAndEditable,// 浏览，可编辑
    OrderInfoTypeBrowsableAndNoEditable// 浏览，不可编辑
};

/**
 @enum      OrderDispatchType
 @abstract  派单信息
 */
typedef NS_ENUM(NSInteger, OrderDispatchType) {
    OrderDispatchTypeFirst = 1, // 首次派单
    OrderDispatchTypeBrowsableAndChangable, // 浏览，可改派
    OrderDispatchTypeBrowsableAndNoChangable // 浏览，不可改派
};

/**
 @enum      OrderVechilearrangeType
 @abstract  车辆安排
 */
typedef NS_ENUM(NSInteger, OrderVechilearrangeType) {
    OrderVechilearrangeTypeFirst = 1,// 首次车辆安排
    OrderVechilearrangeTypeBrowsableAndChangable,// 浏览，可变更
    OrderVechilearrangeTypeBrowsableAndNoChangable// 浏览，不可变更
};

/**
 @enum      OrderWaybillType
 @abstract  路单信息
 */
typedef NS_ENUM(NSInteger, OrderWaybillType) {
    OrderWaybillTypeFirst = 1, // 首次填写
    OrderWaybillTypeBrowsableAndEditable, // 浏览，可编辑
    OrderWaybillTypeBrowsableAndNoEditable // 浏览，不可编辑
};

/**
 @enum      OrderSettlementType
 @abstract  结算信息
 */
typedef NS_ENUM(NSInteger, OrderSettlementType) {
    OrderSettlementTypeFirst = 1, // 首次填写
    OrderSettlementTypeBrowsableAndChangable, // 浏览，可变更
    OrderSettlementTypeBrowsableAndNoChangable, // 浏览，不可变更
};

/**
 @enum      OrderPayType
 @abstract  结算信息
 */
typedef NS_ENUM(NSInteger, OrderPayType) {
    OrderPayTypeFirst = 1, // 首次支付确认 同意支付
    OrderPayTypeBrwosable, // 浏览
    OrderPayTypeFirstPay, // 首次付款 付款
    OrderPayTypeHavePay // 已付款
};

/**
 @enum      OrderRollbackStatus
 @abstract  改派状态
 */
typedef NS_ENUM(NSInteger, OrderRollbackStatus) {
    OrderRollbackStatusInit = 0, // 
    OrderRollbackStatusStart, // 已发起
    OrderRollbackNotAgree, // 未同意
    OrderRollbackHaveAgree // 已同意
};




#endif /* EnumConstants_h */
