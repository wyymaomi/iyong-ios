//
//  ProviderDescriptionTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderDescriptionTableViewCell.h"

@implementation ProviderDescriptionTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"ProviderDescriptionTableViewCell";
    ProviderDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(24, 15, 2, 17)];
        view.backgroundColor = NAVBAR_BGCOLOR;
        [cell.contentView addSubview:view];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, view.top-1, ViewWidth, view.height)];
        titleLabel.font = FONT(17);
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = @"公司简介";
        titleLabel.backgroundColor = [UIColor clearColor];
        [titleLabel sizeToFit];
        [cell.contentView addSubview:titleLabel];
        cell.titleLabel = titleLabel;
        
        cell.companyDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(24, 45, ViewWidth, cell.height)];
        cell.companyDescriptionLabel.numberOfLines = 0;
        cell.companyDescriptionLabel.font = FONT(15);
        cell.companyDescriptionLabel.textColor = UIColorFromRGB(0x666666);
        cell.companyDescriptionLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:cell.companyDescriptionLabel];
        cell.companyDescriptionLabel.text = @"该公司暂无简介";
        
        
    }
    return cell;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
}

- (void)setViewModel:(ProviderDetailViewModel *)viewModel
{
    if (!viewModel) {
        return;
    }
    
    _viewModel = viewModel;
    
    [_viewModel initCompanyDescription];
    
    self.companyDescriptionLabel.frame = _viewModel.bodyFrame;
    
    self.companyDescriptionLabel.text = [_viewModel.detailModel displayDescriptionText];
    
//    self.lineView.frame = CGRectMake(12, viewModel.companyDescriptionSectionHeight-SEPARATOR_LINE_HEIGHT, ViewWidth-24, SEPARATOR_LINE_HEIGHT);
    
}

- (UIView*)lineView
{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xEBEBEB);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.companyDescriptionLabel.frame = CGRectMake(24, 45, ViewWidth-48, self.height);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
