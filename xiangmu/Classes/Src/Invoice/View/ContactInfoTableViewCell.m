//
//  ContactInfoTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ContactInfoTableViewCell.h"

@implementation ContactInfoTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"ContactInfoTableViewCellIdentifier";
    ContactInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.contactNameTextField.tag = InvoiceTextFieldTypeContacterName;
        cell.contactPhoneTextField.tag = InvoiceTextFieldTypeContactPhoneNumber;
        cell.addressDetailTextField.tag = InvoiceTextFieldTypeDetailAddress;
    }
    return cell;
}

+(CGFloat)getCellHeight
{
    return 200;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
