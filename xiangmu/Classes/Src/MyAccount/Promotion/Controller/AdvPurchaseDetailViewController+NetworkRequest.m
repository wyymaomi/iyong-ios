//
//  AdvPurchaseDetailViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/13.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdvPurchaseDetailViewController+NetworkRequest.h"
#import "AdvSumModel.h"
#import "AdvDetailModel.h"




@implementation AdvPurchaseDetailViewController (NetworkRequest)



//areaCode(地区代码)
//position(广告版位1到10数字)
//type（广告类型：1banner广告；2在线广告；3区域广告）
//startTime（开始时间：yyyy-MM-dd）
//endTime（结束时间：yyyy-MM-dd）

- (void)fetchAdvSumList:(AdvSumHttpMessage*)httpMessage
{
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ADMAIN_SUM_LIST_API];
//    NSString *params = httpMessage.description;
//    NSString *params = @"areaCode=&type=1&position=1&startTime=2017-04-17&endTime=";
//    NSString *dateStr = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
//    NSString *params = [NSString stringWithFormat:@"areaCode=&type=%lu&position=%ld&startTime=%@&endTime=", (unsigned long)self.advType,(long)self.position,dateStr];
    NSString *params = httpMessage.description;
    
    
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary * responseData) {
        StrongSelf
        
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            [strongSelf.viewModel.saleAdvSumList removeAllObjects];
            [strongSelf.viewModel.advSumDictionary removeAllObjects];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                AdvSumResponseData *advSumResponseData = [[AdvSumResponseData alloc] initWithDictionary:responseData error:nil];
                
                if (advSumResponseData.data.count > 0) {
                    for (NSInteger i = 0; i < advSumResponseData.data.count; i++) {
                        AdvSumModel *model = advSumResponseData.data[i];
                        [strongSelf.viewModel.saleAdvSumList addObject:model];
                        [strongSelf.viewModel.advSumDictionary setObject:model.count forKey:model.date];
                    }
                }
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    strongSelf.loadStatus = LoadStatusFinish;
                    
                    [strongSelf.groupTableView reloadData];
                    
//                    [strongSelf.groupTableView reloadSections:[NSIndexSet indexSetWithIndex:AdvPurchaseDetailRowCalendar] withRowAnimation:UITableViewRowAnimationNone];
                    
                    
                });
                
            });
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

- (void)fetchAdvDetailList:(NSString*)date
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ADMIN_LIST_API];
    NSString *areaCode = (self.advType == AdvTypeLocalArea)?[UserManager sharedInstance].userModel.areaCode:@"";
    NSString *params = [NSString stringWithFormat:@"areaCode=%@&type=%lu&position=%ld&date=%@", areaCode,  (unsigned long)self.advType, (long)self.position, date];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSDictionary *data = responseData[@"data"];
                NSArray *dataList = data[@"adList"];
                
                strongSelf.viewModel.countLeft = data[@"countLeft"];
                [strongSelf.viewModel.saleAdvDetailList removeAllObjects];
                
                for (NSUInteger i = 0; i < dataList.count; i++) {
                    NSDictionary *dict = dataList[i];
                    AdvDetailModel *detailModel = [[AdvDetailModel alloc] initWithDictionary:dict error:nil];
                    [strongSelf.viewModel.saleAdvDetailList addObject:detailModel];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (self.viewModel.advOrderList.count > 0) {
                        self.tableViewRowNums = 4;
                    }
                    else {
                        self.tableViewRowNums = 3;
                    }
                    
                    [strongSelf.groupTableView reloadData];
                    
                });
                
            });
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        
        
    }];
}




@end
