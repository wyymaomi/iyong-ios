//
//  CommunityHelper.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityHelper.h"

@implementation CommunityHelper

+ (BOOL) isValidPublisher:(NSString**)errorMsg; // 是否有询价权限
{
    
    //    BOOL isCompanyCertified = [[UserManager sharedInstance].userModel.companyCertification integerValue] == CompanyCertificationStatusFinished;
    BOOL isRealnameCertified = [[[UserManager sharedInstance].userModel certification] boolValue];
    //    BOOL isVechileCertified = [[UserManager sharedInstance].userModel.carCertification boolValue];
    
    if (!isRealnameCertified) {
        *errorMsg = @"未实名认证，无询价权限";
        return NO;
    }
    
    //    if (!isCompanyCertified) {
    //        *errorMsg = @"未企业认证，无报价权限";
    //        return NO;
    //    }
    
    //    if (!isVechileCertified) {
    //        *errorMsg = @"未车辆认证，无报价权限";
    //    }
    
    return YES;
    
}

+ (BOOL) isValidBusienssCircleBrowser:(NSString**)errorMsg; // 是否有业务圈浏览权限
{
    //    BOOL isCompanyCertified = [[UserManager sharedInstance].userModel.companyCertification integerValue] == CompanyCertificationStatusFinished;
    //    BOOL isRealnameCertified = [[[UserManager sharedInstance].userModel certification] boolValue];
    //    BOOL isVechileCertified = [[UserManager sharedInstance].userModel.carCertification boolValue];
    //
    //    if (!isRealnameCertified) {
    //        *errorMsg = @"未实名认证，无浏览权限";
    //        return NO;
    //    }
    //
    //    if (!isCompanyCertified) {
    //        *errorMsg = @"未企业认证，无浏览权限";
    //        return NO;
    //    }
    //
    //    if (!isVechileCertified) {
    //        *errorMsg = @"未车辆认证，无浏览权限";
    //        return NO;
    //    }
    
    if (![[UserManager sharedInstance] isLogin]) {
        *errorMsg = @"请先登录";
        return NO;
    }
    
    return YES;
}

+(BOOL)isCertifcationed:(NSString**)errorMsg; // 是否三项认证信息满足
{
    BOOL realnameCertificationed = [[UserManager sharedInstance].userModel.certification boolValue];
    BOOL companyCertificationed = [[UserManager sharedInstance].userModel.companyCertification integerValue] == CompanyCertificationStatusFinished;
    BOOL vechileCertificationed = [[UserManager sharedInstance].userModel.carCertification boolValue];
    
    if (realnameCertificationed && companyCertificationed /*&& vechileCertificationed*/) {
        return YES;
    }

    return NO;
}

+ (BOOL) isValidBusinessCircleBider:(NSString**)errorMsg; // 是否有业务圈报价权限
{
    BOOL isBidPermission = [[UserManager sharedInstance].userModel.bidPermisson boolValue];

    if (!isBidPermission) {
        *errorMsg = @"亲！没有报价权限，请与客服联系！";
        return NO;
    }

    
    
    return YES;
    
//    BOOL isCompanyCertified = [[UserManager sharedInstance].userModel.companyCertification integerValue] == CompanyCertificationStatusFinished;
//    BOOL isRealnameCertified = [[[UserManager sharedInstance].userModel certification] boolValue];
//    BOOL isVechileCertified = [[UserManager sharedInstance].userModel.carCertification boolValue];
//    
//    if (!isRealnameCertified) {
//        *errorMsg = @"未实名认证，无报价权限";
//        return NO;
//    }
////    else {
//        if (!isCompanyCertified) {
//             *errorMsg = @"未企业认证，无报价权限";
//            return NO;
//        }
//        if (!isVechileCertified) {
//            *errorMsg = @"未车辆认证，无报价权限";
//            return NO;
//        }
//    
//    if (isRealnameCertified && (isCompanyCertified && isVechileCertified)) {
//        return YES;
//    }
//    
//    return NO;
    
}

+ (BOOL) isValidEnquire:(InquireHttpMessage*)httpMessage errorMsg:(NSString**)errorMsg;
{
    if (IsStrEmpty(httpMessage.startTime)) {
        *errorMsg = @"请输入开始时间";
        return NO;
    }
    
    if (IsStrEmpty(httpMessage.endTime)) {
        *errorMsg = @"请输入结束时间";
        return NO;
    }
    
    if (IsStrEmpty(httpMessage.startPlace)) {
        *errorMsg = @"请输入开始地点";
        return NO;
    }
    
    if (IsStrEmpty(httpMessage.endPlace)) {
        *errorMsg = @"请输入结束地点";
        return NO;
    }
    
    if (IsStrEmpty(httpMessage.customerName)) {
        *errorMsg = @"请输入用车人";
        return NO;
    }
    
    if (IsStrEmpty(httpMessage.customerMobile.trim)) {
        *errorMsg = @"请输入用车人电话";
        return NO;
    }
    
    if (IsStrEmpty(httpMessage.model)) {
        *errorMsg = @"请输入车型";
        return NO;
    }
    
    if (IsStrEmpty(httpMessage.quantity)) {
        *errorMsg = @"请输入数量";
        return NO;
    }
    
//    if (IsStrEmpty(httpMessage.itinerary)) {
//        *errorMsg = @"请输入大致行程";
//        return NO;
//    }

    
    return YES;
}

+(BOOL)isValidBidContent:(BidHttpMessage*)bidHttpMesage errorMsg:(NSString**)errorMsg;
{
    if (IsStrEmpty(bidHttpMesage.model.trim)) {
        *errorMsg = @"请输入车型";
        return NO;
    }
    
    if (IsStrEmpty(bidHttpMesage.quantity.trim) || [bidHttpMesage.quantity integerValue] == 0) {
        *errorMsg = @"请输入数量";
        return NO;
    }
    
    if (IsStrEmpty(bidHttpMesage.price.trim) || [bidHttpMesage.price integerValue] == 0) {
        *errorMsg = @"请输入价格";
        return NO;
    }
    
    return YES;
}

- (NSString*)getCityIdFromCityName:(NSString*)cityName
{
    for (GYZCity *city in self.cityDatas) {
        if ([city.cityName isEqualToString:cityName] || [city.shortName isEqualToString:cityName]) {
            return city.cityID;
        }
    }
    return nil;
}

- (NSString *)getCityNameFromAreacode:(NSString*)areaCode
{
    for (GYZCity *city in self.cityDatas) {
        if ([city.cityID isEqualToString:areaCode]) {
            return city.cityName;
        }
    }
    return nil;
}

-(NSMutableArray *) cityDatas{
    if (_cityDatas == nil) {
        NSArray *array = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CityData" ofType:@"plist"]];
        _cityDatas = [NSMutableArray new];//[[NSMutableArray alloc] init];
        for (NSDictionary *groupDic in array) {
            //            GYZCityGroup *group = [[GYZCityGroup alloc] init];
            //            group.groupName = [groupDic objectForKey:@"initial"];
            for (NSDictionary *dic in [groupDic objectForKey:@"citys"]) {
                GYZCity *city = [[GYZCity alloc] init];
                city.cityID = [dic objectForKey:@"city_key"];
                city.cityName = [dic objectForKey:@"city_name"];
                city.shortName = [dic objectForKey:@"short_name"];
                city.pinyin = [dic objectForKey:@"pinyin"];
//                city.initials = [dic objectForKey:@"initials"];
                
                [_cityDatas addObject:city];
                
                //                _cityDatas arrayByAddingObjectsFromArray:<#(nonnull NSArray *)#>
                //                [group.arrayCitys addObject:city];
                //                [self.recordCityData addObject:city];
            }
            //            [self.arraySection addObject:group.groupName];
            //            [_cityDatas addObject:group];
        }
    }
    return _cityDatas;
}

- (NSArray *)vechileServiceList
{
    if (IsNilOrNull(_vechileServiceList)) {
        // 加载plist中的字典数组
        NSString *path = [[NSBundle mainBundle] pathForResource:VECHILE_SERVICE_PLIST ofType:nil];
        _vechileServiceList = [NSArray arrayWithContentsOfFile:path];
    }
    return _vechileServiceList;
}

- (NSString*)getServicetypeNameFromServiceId:(NSInteger)serviceId
{
    NSDictionary *serviceDict = self.vechileServiceList[serviceId-1];
    return serviceDict[@"title"];
}

-(NSArray*)getServicetypeNameList
{
    NSMutableArray *array = [NSMutableArray new];
    for (NSInteger i = 0; i < self.vechileServiceList.count; i++) {
        NSDictionary *dict = self.vechileServiceList[i];
        [array addObject:dict[@"title"]];
    }
    return array;
}



@end
