//
//  SettlementAndPayTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SettlementAndPayTableViewCell.h"

@implementation SettlementAndPayEditView

+(instancetype)customView
{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([SettlementAndPayTableViewCell class]) owner:nil options:nil] lastObject];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.companyInfoLabel.text = @"";
    self.srcPriceLabel.text = @"";
    self.settlementPriceTextField.text = @"";
    self.settlementPriceView.backgroundColor = [UIColor whiteColor];
}

@end


@implementation SettlementAndPayTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"CellIdentifier";
    SettlementAndPayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0xDEDEDE);
    }
    return cell;
}

-(SettlementAndPayEditView*)settlementEditView
{
    if (_settlementEditView == nil) {
        _settlementEditView = [SettlementAndPayEditView customView];
        _settlementEditView.frame = CGRectMake(0, 36, self.contentView.frame.size.width + 1, _settlementEditView.frame.size.height);
        _settlementEditView.hidden = YES;
        [self.contentView addSubview:_settlementEditView];
    }
    return _settlementEditView;
}



- (void)initData:(BarginAndPayResponseModel*)model
{
    
    if (IsNilOrNull(model)) {
        return;
    }
    
    self.settlementButton.enabled = YES;
    self.settlementButton.userInteractionEnabled = YES;
    self.payButton.enabled = YES;
    self.payButton.userInteractionEnabled = YES;
    [self.settlementButton addTarget:self action:@selector(doSelectSettlementTab) forControlEvents:UIControlEventTouchUpInside];
    [self.payButton addTarget:self action:@selector(doSelectPayTab) forControlEvents:UIControlEventTouchUpInside];
    
    self.paymentView.hidden = YES;
    self.payInfoView.hidden = YES;
    
    self.model = model;
    
    if ([self.model.selected isEqualToString:@"bargain"]) {
        [self doRefreshSettlementTab];
    }
    else {
        [self doRefreshPayTab];
    }
    
    BarginModel *barginModel = model.bargain;
    self.settlementStatusLabel.text = @"";
    if (NotNilAndNull(barginModel) && (self.status == 0 || self.status == 3)) {
        self.settlementStatusLabel.text = barginModel.status.trim;
        switch ([barginModel.type integerValue]) {
            case OrderSettlementTypeFirst:
            {
                self.browseInfoView.hidden = YES;
                self.settlementEditView.hidden = NO;
                self.settlementEditView.companyInfoLabel.text = barginModel.companyInfo.trim;
                self.settlementEditView.srcPriceLabel.text = [barginModel.srcPrice integerValue] > 0 ? [NSString stringWithFormat:@"¥%@", barginModel.srcPrice] : @"";
                
                self.settlementEditView.settlementPriceTextField.text = barginModel.editDstPrice;
                self.settlementEditView.settlementPriceView.userInteractionEnabled = YES;
                self.settlementEditView.settlementPriceView.backgroundColor = [UIColor whiteColor];
                [self.settlementEditView.settlementPriceView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doGetSettlementPrice:)]];
                
            }
                break;
            case OrderSettlementTypeBrowsableAndChangable:
            {
                self.browseInfoView.hidden = NO;
                self.settlementEditView.hidden = YES;
                self.partnerLabel.text = barginModel.companyInfo.trim;
                self.estimatePriceLabel.text = [NSString stringWithFormat:@"¥%ld",(long)[barginModel.srcPrice integerValue]];
                if ([barginModel.dstPrice integerValue] > 0) {
                    self.settlementPriceTextField.text = [NSString stringWithFormat:@"¥%@", [barginModel.dstPrice stringValue]];
                }
                else {
                    self.settlementPriceTextField.text = @"";
                }
                self.changeButton.hidden = NO;
            }
                break;
            case OrderSettlementTypeBrowsableAndNoChangable:
            {
                self.browseInfoView.hidden = NO;
                self.settlementEditView.hidden = YES;
                self.partnerLabel.text = barginModel.companyInfo.trim;
                self.estimatePriceLabel.text = [NSString stringWithFormat:@"¥%ld",(long)[barginModel.srcPrice integerValue]];
                if ([barginModel.dstPrice integerValue] > 0) {
                    self.settlementPriceTextField.text = [NSString stringWithFormat:@"¥%@", [barginModel.dstPrice stringValue]];
                }
                else {
                    self.settlementPriceTextField.text = @"";
                }
                self.changeButton.hidden = YES;
                break;
            }
            default:
                self.estimatePriceLabel.text = @"";
                self.settlementPriceTextField.text = @"";
                self.partnerLabel.text = @"";
                self.changeButton.hidden = YES;
                break;
        }
    }
    
    PayModel *payModel = model.pay;
    self.payStatusLabel.text = @"";
    if (NotNilAndNull(payModel) && self.status > 0) {
        self.payStatusLabel.text = payModel.status;
        self.partnerLabel.text = payModel.companyInfo.trim;
        self.estimatePriceLabel.text = [NSString stringWithFormat:@"¥%ld", (long)[payModel.srcPrice integerValue]];
//        self.estimatePriceLabel.text = [NSString stringWithFormat:@"%ld元", (long)[payModel.srcPrice integerValue]];
//        self.settlementPriceTextField.text = [payModel.dstPrice integerValue] > 0 ? [payModel.dstPrice stringValue].trim : @"";
        if ([payModel.dstPrice integerValue] > 0) {
            self.settlementPriceTextField.text = [NSString stringWithFormat:@"¥%@", [payModel.dstPrice stringValue]];
        }
        else {
            self.settlementPriceTextField.text = @"";
        }
        self.payStatusLabel.text = payModel.status.trim;
        self.changeButton.hidden = YES;
        
        // 移除所有子view
        if (!IsArrEmpty(payModel.rates)) {
            self.status = CompleteStatusCheckPay;
            self.paymentView.hidden = NO;
            self.payInfoView.hidden = YES;
            self.paymentTermView.hidden = NO;
            for (UIView *view in self.paymentTermView.subviews) {
                [view removeFromSuperview];
            }
            
            // 重新计算显示高度
            NSInteger count = payModel.rates.count;
            NSInteger row = count%ROW_COUNT==0?count/ROW_COUNT:count/ROW_COUNT+1;// 多少行
            NSInteger groupHeight = 12 + (BUTTON_HEIGHT + MARGIN_COLUMN) * row;
            
            CustomRadioButtonGroup *customRadioButtonGroup = [[CustomRadioButtonGroup alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, groupHeight) data:payModel.rates];
            customRadioButtonGroup.delegate = self;
            [self.paymentTermView addSubview:customRadioButtonGroup];
        }
        else {
            // 如果payPrice 和 payType不空 显示payInfoView
            if (NotNilAndNull(self.model.pay.payPrice) && !IsStrEmpty(self.model.pay.payPrice) &&
                NotNilAndNull(self.model.pay.payType) && !IsStrEmpty(self.model.pay.payType)) {
                self.paymentView.hidden = NO;
                self.payInfoView.hidden = NO;
                self.paymentTermView.hidden = YES;
                self.payPriceLabel.text = [NSString stringWithFormat:@"¥%@", model.pay.payPrice.trim];
                self.payTermLabel.text = model.pay.payType.trim;
                self.height = 183;
            }
        }
        

    }
}

#pragma mark - 

-(void)doGetSettlementPrice:(id)sender
{
    self.settlementEditView.settlementPriceView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    WeakSelf
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(NSEC_PER_SEC * 0.1)), dispatch_get_main_queue(), ^{
        if (weakSelf.refreshDelegate && [weakSelf.refreshDelegate respondsToSelector:@selector(doGetSettlementPrice)]) {
            [weakSelf.refreshDelegate doGetSettlementPrice];
        }

        weakSelf.settlementEditView.settlementPriceView.backgroundColor = [UIColor whiteColor];
    });
}

#pragma mark -

-(void)onClickButton:(NSInteger)index;
{
//    NSArray *rates = self.model.pay.rates;
//    RateModel *rateModel = rates[index];
//    self.settlementPriceTextField.text = rateModel.price;
    if (self.refreshDelegate && [self.refreshDelegate respondsToSelector:@selector(onClickRate:)]) {
        [self.refreshDelegate onClickRate:index];
    }

}

#pragma mark - 选择结算TAB
- (void)doSelectSettlementTab {
    BarginModel *model = self.model.bargain;
    if (model == nil) {
        return;
    }
    [self doRefreshSettlementTab];
    self.model.selected = @"bargain";
    if (self.refreshDelegate && [self.refreshDelegate respondsToSelector:@selector(doRefreshSettlementAndPayModule:)]) {
        [self.refreshDelegate doRefreshSettlementAndPayModule:self.status];
    }
}

-(void)doRefreshSettlementTab
{
    BarginModel *barginModel = self.model.bargain;
    if (NotNilAndNull(barginModel)) {
        
        
        if ([barginModel.type integerValue] == OrderSettlementTypeFirst) {
            self.height = 157;
            self.status = CompleteStatusWaitSettle;
        }
        else {
            self.height = 97;
            self.status = CompleteStatusWaitPay;
        }

        
//        [UIView animateWithDuration:0.4 animations:^{
            self.bottomLine.frame = CGRectMake(0, self.bottomLine.frame.origin.y, ViewWidth/2, self.bottomLine.frame.size.height);
//        }];
        
    }
}

-(void)doRefreshPayTab
{
    PayModel *payModel = self.model.pay;
    if (NotNilAndNull(payModel)) {
        self.status = CompleteStatusFinishSettle;
        self.height = 97;
//        [UIView animateWithDuration:0.4 animations:^{
            self.bottomLine.frame = CGRectMake(ViewWidth/2, self.bottomLine.frame.origin.y, ViewWidth/2, self.bottomLine.frame.size.height);
//        }];
        
        if (!IsArrEmpty(payModel.rates)) {
            self.status = CompleteStatusCheckPay;
            NSInteger count = payModel.rates.count;
            NSInteger row = count%ROW_COUNT==0?count/ROW_COUNT:count/ROW_COUNT+1;// 多少行
            NSInteger groupHeight = 12 + (BUTTON_HEIGHT + MARGIN_COLUMN) * row;
            self.height += groupHeight + 30;
        }
        if ([payModel.type integerValue] == OrderPayTypeHavePay)
        {
            self.status = CompleteStatusHavePay;
        }
    }
}

#pragma mark - 选择支付TAB
- (void)doSelectPayTab {
    PayModel *pay = self.model.pay;
    if (pay == nil) {
        return;
    }
    self.model.selected = @"pay";
    if (self.refreshDelegate && [self.refreshDelegate respondsToSelector:@selector(doRefreshSettlementAndPayModule:)]) {
        [self.refreshDelegate doRefreshSettlementAndPayModule:self.status];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
