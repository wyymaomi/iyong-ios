//
//  TravelOrderModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/1.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface TravelOrderModel : JSONModel

//@property (nonatomic, strong) NSNumber *amount;
//@property (nonatomic, strong) NSString<Optional> *areaCode;
//@property (nonatomic, strong) NSNumber<Optional> *carType;
//@property (nonatomic, strong) NSNumber<Optional> *createTime;
//@property (nonatomic, strong) NSString<Optional> *endLocation;
//@property (nonatomic, strong) NSString<Optional> *id;
//@property (nonatomic, strong) NSNumber<Optional> *mileage;
//@property (nonatomic, strong) NSNumber<Optional> *orderStatus;
//@property (nonatomic, strong) NSNumber<Optional> *payMode;
//@property (nonatomic, strong) NSNumber<Optional> *receiveCutoffTime;
//@property (nonatomic, strong) NSString<Optional> *receiveOrderTime;
//@property (nonatomic, strong) NSString<Optional> *startLocation;
//@property (nonatomic, strong) NSNumber<Optional> *startTime;
//@property (nonatomic, strong) NSNumber<Optional> *unitPrice;
//@property (nonatomic, strong) NSString<Optional> *userId;

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *userId;
@property (nonatomic, strong) NSNumber<Optional> *useCarMode;// 用车类型
@property (nonatomic, strong) NSString<Optional> *startTime;
@property (nonatomic, strong) NSString<Optional> *startLocation;
@property (nonatomic, strong) NSString<Optional> *endLocation;
@property (nonatomic, strong) NSNumber<Optional> *carType;
@property (nonatomic, strong) NSString<Optional> *carModel;
@property (nonatomic, strong) NSNumber<Optional> *unitPrice;
@property (nonatomic, strong) NSNumber<Optional> *mileage;
@property (nonatomic, strong) NSNumber<Optional> *amount;
@property (nonatomic, strong) NSNumber<Optional> *orderStatus;
@property (nonatomic, strong) NSNumber<Optional> *payStatus;
@property (nonatomic, strong) NSNumber<Optional> *payMode;
@property (nonatomic, strong) NSString<Optional> *areaCode;
@property (nonatomic, strong) NSNumber<Optional> *receiveOrderTime;
@property (nonatomic, strong) NSNumber<Optional> *receiveCutoffTime;
@property (nonatomic, strong) NSNumber<Optional> *createTime;
@property (nonatomic, strong) NSString<Optional> *driverMobile;
@property (nonatomic, strong) NSString<Optional> *driverName;
@property (nonatomic, strong) NSString<Optional> *carNumber;
@property (nonatomic, strong) NSString<Optional> *companyName; // 公司名称
//@property (nonatomic, strong) NSString<Optional> *driverMobile;
//@property (nonatomic, strong) NSString<Optional> *carModel;

-(NSString*)getFormatDriverName;

@end
