//
//  InvoiceManager.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InvoiceinfoHttpMessage.h"
#import "MyInvoiceViewController.h"

@interface InvoiceManager : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic, strong) InvoiceinfoHttpMessage *invoiceinfoHttpMessage;

@property (nonatomic, strong) MyInvoiceViewController *myInvoiceController;

+(BOOL)validateInvoiceInfo:(InvoiceinfoHttpMessage*)invoiceinfo error:(NSString**)errorMsg;

+(BOOL)validateInvoiceSettinginfo:(InvoiceSettingHttpMessage*)invoiceSettingInfo error:(NSString**)errorMsg;

@end
