//
//  DispatchOrderDTO.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface DispatchOrderHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *orderId; // 订单ID
//@property (nonatomic, strong) NSString *srcCompanyId; // 发单公司ID
@property (nonatomic, strong) NSString *dstCompanyId; // 接单公司ID
//@property (nonatomic, strong) NSString *partnerCompanyId;// 接单公司ID
@property (nonatomic, strong) NSString *dstPrice; // 发单价

@end
