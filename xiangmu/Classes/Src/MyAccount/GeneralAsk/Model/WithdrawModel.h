//
//  WithdrawModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/15.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol WithdrawModel <NSObject>
@end

@interface WithdrawModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *createTime;
@property (nonatomic, strong) NSNumber<Optional> *price;
@property (nonatomic, strong) NSNumber<Optional> *realPrice;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSString<Optional> *statusZn;

@end

@interface WithdrawResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<WithdrawModel,Optional> *data;

@end
