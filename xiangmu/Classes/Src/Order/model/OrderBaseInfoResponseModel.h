//
//  OrderBaseInfoModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@interface OrderBaseInfoResponseModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *upCompanyName;
@property (nonatomic, strong) NSString<Optional> *upCompanyLogoUrl;
@property (nonatomic, strong) NSNumber<Optional> *carTime; // 用车日期
@property (nonatomic, strong) NSString<Optional> *companyId; // 公司ID（旧的订单号）
@property (nonatomic, strong) NSNumber<Optional> *createTime; // 创建订单时间
@property (nonatomic, strong) NSString<Optional> *customerName;// 用车人
@property (nonatomic, strong) NSString<Optional> *customerMobile;// 用车人电话
@property (nonatomic, strong) NSString<Optional> *id; // 订单号
@property (nonatomic, strong) NSString<Optional> *itinerary;// 大致行程
@property (nonatomic, strong) NSString<Optional> *location;// 出发地点
@property (nonatomic, strong) NSString<Optional> *price; // 价格
@property (nonatomic, strong) NSString<Optional> *remark; // 备注
@property (nonatomic, strong) NSNumber<Optional> *status; // 订单状态
@property (nonatomic, strong) NSNumber<Optional> *type;// 1:新建；2:浏览，可编辑；3:浏览，不可编辑
@property (nonatomic, strong) NSNumber<Optional> *valid;
@property (nonatomic, strong) NSString<Optional> *model;//车型

@property (nonatomic, strong) NSString<Optional> *editCarTime;
@property (nonatomic, strong) NSString<Optional> *editCustomerName;
@property (nonatomic, strong) NSString<Optional> *editCustomerMobile;
@property (nonatomic, strong) NSString<Optional> *editRemark;
@property (nonatomic, strong) NSString<Optional> *editItinerary;
@property (nonatomic, strong) NSString<Optional> *editLocation;
@property (nonatomic, strong) NSString<Optional> *editModel;
@property (nonatomic, strong) NSString<Optional> *code;// 显示的订单号

@end
