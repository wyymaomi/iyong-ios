//
//  BillTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BillModel.h"
#import "SalesModel.h"

@protocol BillTableViewCellDelegate <NSObject>

//-(void)onCheckButton:(NSUInteger)tag checked:(BOOL)checked;
-(void)onSelectRowAtIndex:(NSUInteger)rowIndex;

@end

@interface BillTableViewCell : UITableViewCell

@property (weak, nonatomic) id<BillTableViewCellDelegate> delegate;



@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *upCompanyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *carDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *startLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *arriveLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerInfoLabel;
@property (weak, nonatomic) IBOutlet UIView *orderInfoView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
-(void)initData:(BillOrderModel*)billOrderModel type:(NSString*)typeName;
-(void)initSalesData:(SalesModel*)model;
+(CGFloat)getCellHeight;

@end
