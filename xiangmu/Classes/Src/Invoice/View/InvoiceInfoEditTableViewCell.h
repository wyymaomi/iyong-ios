//
//  InvoiceInfoTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InvoiceinfoHttpMessage.h"



@interface InvoiceInfoEditTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic, strong) InvoiceinfoHttpMessage* invoiceData;

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *taxpayerNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *registAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *bankNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *bankAccountTextField;


@property (weak, nonatomic) IBOutlet UIView *invoiceIdentifierView;
@property (weak, nonatomic) IBOutlet UIView *registAddressView;
@property (weak, nonatomic) IBOutlet UIView *bankNameView;
@property (weak, nonatomic) IBOutlet UIView *bankcardNumberView;
@property (weak, nonatomic) IBOutlet UIView *companyInvoiceView;

@property (weak, nonatomic) IBOutlet UIButton *updownButton;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (nonatomic, assign) BOOL isSpred; // 是否展开

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+(CGFloat)getCellHeight;

+(CGFloat)getCloseStatusCellHeight;

-(void)initData:(InvoiceinfoHttpMessage*)invoiceData;

@end
