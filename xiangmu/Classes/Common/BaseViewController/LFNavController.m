//
//  LFNavController.m
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "LFNavController.h"
#import "MyUtil.h"

@interface LFNavController ()

@end

@implementation LFNavController

//-(id)init
//{
//    self = [super init];
//    if (self) {
//        self.hasLeftBackButton = YES;
//        self.enter_type = ENTER_TYPE_PUSH;
//    }
//    return self;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(UIButton *)addNavBtn:(CGRect)frame title:(NSString *)title target:(id)target action:(SEL)action isLeft:(BOOL)isLeft
{
    UIButton *btn=[MyUtil createBtnFrame:frame title:title bgImageName:@"" target:target action:action];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    //设置自定义的item
    UIBarButtonItem *item=[[UIBarButtonItem alloc]initWithCustomView:btn];
    if (isLeft) {
        self.navigationItem.leftBarButtonItem=item;
    }
    else
    {
        self.navigationItem.rightBarButtonItem=item;
    }
    return btn;
}

-(UILabel *)addNavTitle:(CGRect)frame title:(NSString *)title
{
    UIFont *font=[UIFont boldSystemFontOfSize:20];
    UIColor *color=[UIColor blackColor];
    UILabel *label=[MyUtil createLabelFrame:frame title:title font:font textAlignment:NSTextAlignmentCenter numberOfLines:1 textColor:color];
    //设置导航栏的标题视图
    self.navigationItem.titleView=label;
    return label;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
