//
//  VechileEditViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "VechileEditViewController.h"
#import "VechileTableViewCell.h"
#import "VechileEditTableViewCell.h"

#import "InputHelperViewController.h"
#import "VechileCertificationViewController.h"

#import "ActionSheetPicker.h"
#import "VechileUtils.h"
#import "ImageUtil.h"
#import "GTMBase64.h"
#import "DateUtil.h"
#import "VechileEditViewModel.h"
#import "CameraPhotoService.h"

@interface VechileEditViewController ()<UITableViewDelegate, UITableViewDataSource, InputHelperDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, retain) UIImageView *avatarImageView;

@property (nonatomic, strong) VechileHttpMessage *vechileHttpMessage;
@property (nonatomic, strong) CameraPhotoService *cameraPhotoService;


@end

@implementation VechileEditViewController

- (CameraPhotoService *)cameraPhotoService
{
    if (!_cameraPhotoService) {
        WeakSelf
        _cameraPhotoService = [[CameraPhotoService alloc] initWithViewController:self.navigationController type:CameraPhotoLarge completeBlock:^(NSInteger tag){
            [weakSelf.groupTableView reloadData];
        } onGetImageData:^(NSData *imageData, NSInteger tag) {
            weakSelf.vechileHttpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imageData] encoding:NSUTF8StringEncoding];
        }];
    }
    return _cameraPhotoService;
}

- (VechileEditViewModel *)vechileViewModel
{
    if (!_vechileViewModel) {
        
        _vechileViewModel = [[VechileEditViewModel alloc] init];
    }
    
    return _vechileViewModel;
}

-(VechileHttpMessage*)vechileHttpMessage
{
    if (IsNilOrNull(_vechileHttpMessage)) {
        _vechileHttpMessage = [[VechileHttpMessage alloc] init];
        _vechileHttpMessage.sex = @"1";
        _vechileHttpMessage.age = @"60后";
    }
    return _vechileHttpMessage;
}

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = (self.editType == VechileEditModeAdd) ? @"添加车辆" : @"编辑车辆";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveVechile)];
    [self.view addSubview:self.groupTableView];
    [self initData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.editType == VechileEditModeEdit) {
        
        [self getCarInfo];
        
    }
}

-(void)initData
{
    if (self.editType == VechileEditModeEdit && !IsStrEmpty(self.vechileViewModel.vechileModel.id)) {
        self.vechileHttpMessage.id = self.vechileViewModel.vechileModel.id;
        self.vechileHttpMessage.model = self.vechileViewModel.vechileModel.model;
        self.vechileHttpMessage.number = self.vechileViewModel.vechileModel.number;
        self.vechileHttpMessage.driver = self.vechileViewModel.vechileModel.driver;
        self.vechileHttpMessage.mobile = self.vechileViewModel.vechileModel.mobile;
        self.vechileHttpMessage.sex = [self.vechileViewModel.vechileModel.sex stringValue];
        self.vechileHttpMessage.age = self.vechileViewModel.vechileModel.age;
        self.vechileHttpMessage.insuranceDate = [DateUtil formatDateTime:self.vechileViewModel.vechileModel.insuranceDate dateFomatter:@"yyyy-MM-dd"];
        self.vechileHttpMessage.inspectionDate = [DateUtil formatDateTime:self.vechileViewModel.vechileModel.inspectionDate dateFomatter:@"yyyy-MM-dd"];
        
        // 如果有图片，判断图片是否已经下载
        if (!IsStrEmpty(self.vechileViewModel.vechileModel.imgUrl)) {
            if (self.imageData == nil) {
                // 如果图片没有下载
                WeakSelf
                [[NetworkManager sharedInstance] downloadImage:self.vechileViewModel.vechileModel.imgUrl success:^(id responseData) {
                    StrongSelf
                    NSData *imageData = responseData;
                    if (imageData != nil && imageData.length > 0) {
                        strongSelf.imageData = imageData;
                        [strongSelf.groupTableView reloadData];
                    }
                } andFailure:^(NSString *errorDesc) {
                    
                    
                }];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - business method
-(void)saveVechile
{
    NSString *errorMsg;
    
    if (IsStrEmpty(self.vechileHttpMessage.fileContent)) {
        if (self.imageData == nil || self.imageData.length == 0) {
            [self showAlertView:@"请先上传车辆照片"];
            return;
        }
    }
    
    if (IsStrEmpty(self.vechileHttpMessage.model)) {
        [self showAlertView:@"请输入车型"];
        return;
    }
    if (![VechileUtils validateCarId:self.vechileHttpMessage.number error:&errorMsg] && self.editType == 0) {
        [self showAlertView:errorMsg];
        return;
    }
    if (IsStrEmpty(self.vechileHttpMessage.driver)) {
        [self showAlertView:@"请输入司机姓名"];
        return;
    }
    if (IsStrEmpty(self.vechileHttpMessage.mobile)) {
        [self showAlertView:@"请输入电话"];
        return;
    }
    
    if (self.editType == 0) {
        [self addVechileToServer];
    }
    else if (self.editType == 1) {
        [self editVechileToServer];
    }
}

-(void)addVechileToServer
{
//    DLog(@"fileData.length = %lu", (unsigned long)self.vechileHttpMessage.fileContent.length);
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_SAVE_API];
    NSString *params = self.vechileHttpMessage.description;
    
    [self showHUDIndicatorViewAtCenter:@"正在保存车辆，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            weakSelf.vechileViewModel.vechileModel = [[CarModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"添加车辆成功，是否要进行车辆认证？" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"确定", nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
                else if (buttonIndex == 1) {
                    [weakSelf gotoVechileCertificationPage:YES carId:weakSelf.vechileViewModel.vechileModel.id];
                    weakSelf.editType = 1;
                    [weakSelf.groupTableView reloadData];
                }
            }];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf showAlertView:errorDesc];
        
    }];
}

-(void)editVechileToServer
{
    DLog(@"fileData.length = %lu", (unsigned long)self.vechileHttpMessage.fileContent.length);
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_Update_API];
    NSString *params = self.vechileHttpMessage.description;
    
    [self hideHUDIndicatorViewAtCenter];
    [self showHUDIndicatorViewAtCenter:@"正在保存车辆，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"编辑车辆成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
    }];
}

- (void)getCarInfo
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_Query_API];
    NSString *params = [NSString stringWithFormat:@"id=%@", self.vechileViewModel.vechileModel.id];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary * responseData) {
        
        StrongSelf
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            strongSelf.vechileViewModel.vechileModel = [[CarModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            [strongSelf.groupTableView reloadData];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}


#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    if (self.editType == 0) {
    //        return 2;
    //    }
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 7;
    }
    else if (section == 1) {
        return 2;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0 && indexPath.row == 0) {
        return 80;
    }
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section==0 || section == 1) {
        return 20;
    }
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *ID = @"CellIdentifier";
    VechileEditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([VechileEditTableViewCell class]) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.backgroundColor = [UIColor whiteColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        if (indexPath.section == 0 && indexPath.row == VechileEditRowAvartar) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [cell.textField removeFromSuperview];
            _avatarImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-100, 10, 80, 60)];
            [_avatarImageView setImage:[UIImage imageNamed:IMG_VECHILE_PLACE_HOLDER]];
            _avatarImageView.cornerRadius = 18;
            [cell.contentView addSubview:self.avatarImageView];
        }
    }
    if (indexPath.section == 0) {
        if (indexPath.row == VechileEditRowAvartar) {
            cell.titleLabel.text = @"头    像";
            
            if (IsStrEmpty(self.vechileHttpMessage.fileContent)) {
                if (self.imageData != nil && self.imageData.length > 0) {
                    _avatarImageView.image = [UIImage imageWithData:self.imageData];
                }
            }
            else {
                NSData *decodeImageData = [GTMBase64 decodeString:self.vechileHttpMessage.fileContent];
                _avatarImageView.image = [UIImage imageWithData:decodeImageData];
            }
        }
        if (indexPath.row == VechileEditRowModel) {
            cell.titleLabel.text = @"车    型";
            cell.textField.placeholder = @"请填写车";
            cell.textField.tag = VechileTextFieldModel;
            cell.textField.text = self.vechileHttpMessage.model.trim;
        }
        if (indexPath.row == VechileEditRowNumber) {
            cell.titleLabel.text = @"车    牌";
            cell.textField.placeholder = @"请填车牌号";
            cell.textField.tag = VechileTextFieldNumber;
            cell.textField.text = self.vechileHttpMessage.number.trim;
        }
        if (indexPath.row == VechileEditRowName) {
            cell.titleLabel.text = @"姓    名";
            cell.textField.placeholder = @"请填姓名";
            cell.textField.tag = VechileTextFieldName;
            cell.textField.text = self.vechileHttpMessage.driver.trim;
        }
        if (indexPath.row == VechileEditRowAge) {
            cell.titleLabel.text = @"年    龄";
            cell.textField.placeholder = @"请输入年龄";
            cell.textField.text = self.vechileHttpMessage.age.trim;
        }
        if (indexPath.row == VechileEditRowSex) {
            cell.titleLabel.text = @"性    别";
            cell.textField.placeholder = @"请输入性别";
            cell.textField.text = [self.vechileHttpMessage.sex isEqualToString:@"1"]?@"男":@"女";
        }
        if (indexPath.row == VechileEditRowMobile) {
            cell.titleLabel.text = @"电    话";
            cell.textField.placeholder = @"请填手机号";
            cell.textField.tag = VechileTextFieldMobile;
            cell.textField.text = self.vechileHttpMessage.mobile.trim;
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"年检日期";
            cell.textField.placeholder = @"选择日期";
            cell.textField.text = self.vechileHttpMessage.inspectionDate.trim;
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"保险日期";
            cell.textField.placeholder = @"选择日期";
            cell.textField.text = self.vechileHttpMessage.insuranceDate.trim;
        }
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"车辆认证";
            cell.textField.hidden = YES;
            cell.verifyImageView.hidden = NO;
            
            NSString *statusText;
            UIColor *btnBgColor;
            [self.vechileViewModel setVechileCertificationStatus:&statusText btnBgColor:&btnBgColor];
            
            cell.verifyButton.hidden = NO;
            [cell.verifyButton setBackgroundImage:[UIImage imageWithColor:btnBgColor] forState:UIControlStateNormal];
            [cell.verifyButton setTitle:statusText forState:UIControlStateNormal];
            cell.verifyButton.cornerRadius = 5;
            cell.verifyButton.titleLabel.font = FONT(10);
            [cell.verifyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
        }
    }
    
    return cell;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VechileEditTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section == 0) {
        if (indexPath.row == VechileEditRowModel ||
            indexPath.row == VechileEditRowName ||
            (indexPath.row == VechileEditRowNumber && self.editType == VechileEditModeAdd) ||
            indexPath.row == VechileEditRowMobile) {
            
            InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
            viewController.title = cell.titleLabel.text;
            viewController.text = cell.textField.text.trim;
            viewController.tag = cell.textField.tag;
            viewController.delegate = self;
            [self.navigationController pushViewController:viewController animated:YES];
            
        }
        if (indexPath.row == VechileEditRowAge) {
            [self selectAge:cell.textField];
        }
        if (indexPath.row == VechileEditRowSex) {
            [self selectSex:cell.textField];
        }
        if (indexPath.row == VechileEditRowAvartar) {
//            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
//                                                                     delegate:self
//                                                            cancelButtonTitle:@"取消"
//                                                       destructiveButtonTitle:nil
//                                                            otherButtonTitles:@"拍照", @"从相册选择", nil];
//            [actionSheet showInView:self.view];
            [self.cameraPhotoService show];
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [self selectInspectiveDate:cell.textField];
        }
        if (indexPath.row == 1) {
            [self selectInsuranceDate:cell.textField];
        }
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            if (self.editType == VechileEditModeAdd) {
                // 如果没有保存车辆信息
                if (IsStrEmpty(self.vechileViewModel.vechileModel.id)) {
                    [self showAlertView:@"请先保存车辆，再进行认证"];
                    return;
                }
            }
            BOOL isEditMode = [self.vechileViewModel isVechileCertCanEdit];
            [self gotoVechileCertificationPage:isEditMode carId:self.vechileViewModel.vechileModel.id];
            
            
            
        }
    }
    
}

- (void)gotoVechileCertificationPage:(BOOL)isEditMode carId:(NSString*)carId
{
    VechileCertificationViewController *viewController = [[VechileCertificationViewController alloc] init];
    //    NSInteger vechileCertificationStatus = [self.vechileViewModel.vechileModel.certificationStatus integerValue];
    viewController.bEditMode = isEditMode;
    viewController.carId = carId;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - 日期选择、年龄选择、性别选择
-(void)selectSex:(id)sender
{
    NSArray *sexList = @[@"男", @"女"];
    WeakSelf
    [ActionSheetStringPicker showPickerWithTitle:@"请选择性别" rows:sexList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        if (selectedIndex == 0) {
            weakSelf.vechileHttpMessage.sex = @"1";
        }
        else if (selectedIndex == 1) {
            weakSelf.vechileHttpMessage.sex = @"0";
        }
        [weakSelf.groupTableView reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
        
    } origin:sender];
}

-(void)selectAge:(id)sender
{
    NSArray *ageList = @[@"50后",@"60后",@"70后",@"80后",@"90后",@"00后"];
    WeakSelf
    [ActionSheetStringPicker showPickerWithTitle:@"请选择年龄" rows:ageList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        weakSelf.vechileHttpMessage.age = ageList[selectedIndex];
        [weakSelf.groupTableView reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
        
    } origin:sender];
}

-(void)selectInspectiveDate:(id)sender
{
    WeakSelf
    
    [ActionSheetDatePicker showPickerWithTitle:@"请选择年检日期" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        StrongSelf
        if ([selectedDate isKindOfClass:[NSDate class]]) {
            NSDate *date = selectedDate;
            strongSelf.vechileHttpMessage.inspectionDate = [date stringWithDateFormat:@"yyyy-MM-dd"];
            [strongSelf.groupTableView reloadData];
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:sender];
    

}


-(void)selectInsuranceDate:(id)sender
{
    WeakSelf
    [ActionSheetDatePicker showPickerWithTitle:@"请选择保险日期" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        StrongSelf
        if ([selectedDate isKindOfClass:[NSDate class]]) {
            NSDate *date = selectedDate;
            strongSelf.vechileHttpMessage.insuranceDate = [date stringWithDateFormat:@"yyyy-MM-dd"];
            [strongSelf.groupTableView reloadData];
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:sender];
}

#pragma mark - UITextField Delegate method

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
//{
//    if (textField.tag == OrderTextFieldCarTime) {
//        [self setCarDate];
//        return NO;
//    }
//
//    return YES;
//}

#pragma mark - Text Input Delegate method
-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
{
    if (text.trim.length > 0) {
        if (tag == VechileTextFieldModel) {
            self.vechileHttpMessage.model = text.trim;
        }
        if (tag == VechileTextFieldNumber) {
            self.vechileHttpMessage.number = text.trim;
        }
        if (tag == VechileTextFieldName) {
            self.vechileHttpMessage.driver = text.trim;
        }
        if (tag == VechileTextFieldMobile) {
            self.vechileHttpMessage.mobile = text.trim;
        }
        [self.groupTableView reloadData];
    }
}

//#pragma mark - UIActionSheet Delegate Method
//
//-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == actionSheet.cancelButtonIndex) {
//        
//    }
//    switch (buttonIndex) {
//        case 0:
//            [self takePhoto];
//            break;
//        case 1:
//            [self LocalPhoto];
//            break;
//        default:
//            break;
//    }
//}
//
////开始拍照
//-(void)takePhoto
//{
//    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
//    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
//    {
//        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
//        cameraPicker.delegate = self;
//        //设置拍照后的图片可被编辑
//        cameraPicker.allowsEditing = YES;
//        cameraPicker.sourceType = sourceType;
//        [self presentViewController:cameraPicker animated:YES completion:nil];
//    }
//    else
//    {
//        NSLog(@"模拟其中无法打开照相机,请在真机中使用");
//    }
//}
//
////打开本地相册
//-(void)LocalPhoto
//{
//    UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
//    
//    photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    photoPicker.delegate = self;
//    //设置选择后的图片可被编辑
//    photoPicker.allowsEditing = YES;
//    [self presentViewController:photoPicker animated:YES completion:nil];
//}
//
//#pragma mark - UIImagePickerControllDelegate method
//
//-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//{
//    
//    
//    WeakSelf
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//    
//        @autoreleasepool {
//        
//            NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
//            if ([mediaType isEqualToString:@"public.image"]) {
//                
//                UIImage* image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
////                UIImage *image = info[UIImagePickerControllerOriginalImage];
//            
//                CGSize imageSize = CGSizeMake(320,320);
//                NSInteger maxImageFileLength = 200*1024;//320*240;
//                
//                UIImage *tempImage = [ImageUtil imageWithImage:image scaledToSize:imageSize];
//                
////                tempImage = [ImageUtil compressImage:image toMaxFileSize:maxImageFileLength];
////                NSData *data = UIImageJPEGRepresentation(tempImage, 1);
//                
//                NSData *data = [ImageUtil compressImageWithData:tempImage toMaxFileSize:maxImageFileLength];
//                
//                weakSelf.vechileHttpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:data] encoding:NSUTF8StringEncoding];
//                
////                weakSelf.avatarImageView.image = [UIImage imageWithData:data];
//                DLog(@"data.size = %lu", (unsigned long)data.length);
// 
//            }
//            
//        }
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//    
//            [picker dismissViewControllerAnimated:YES completion:nil];
//            [weakSelf.groupTableView reloadData];
//            
//        });
//    });
//    
//    
//}
//
//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
//{
//    NSLog(@"您取消了选择图片");
//    [picker dismissViewControllerAnimated:YES completion:nil];
//}



@end
