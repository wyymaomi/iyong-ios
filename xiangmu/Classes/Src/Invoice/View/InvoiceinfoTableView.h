//
//  InvoiceinfoTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseTableView.h"
#import "InvoiceManager.h"

@protocol InvoiceinfoTableViewDelegate <NSObject>

-(void)onClickSubmitButton;

@end

@interface InvoiceinfoTableView : BaseTableView<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) id<InvoiceinfoTableViewDelegate> customDelegate;

@property (nonatomic, strong) InvoiceinfoHttpMessage *invoiceInfoHttpMessage;

@property (nonatomic, assign) NSUInteger mode;

@end
