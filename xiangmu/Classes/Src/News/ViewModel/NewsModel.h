//
//  NewsModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol NewsModel <NSObject>

@end

@protocol NewsResponseListModel <NSObject>

@end

@interface NewsModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *title;//标题
@property (nonatomic, strong) NSString<Optional> *imgUrl;// 图片地址
@property (nonatomic, strong) NSString<Optional> *linkUrl;// 资讯链接地址
@property (nonatomic, strong) NSString<Optional> *source;// 资讯来源
@property (nonatomic, strong) NSString<Optional> *createTime;// 生成时间
@property (nonatomic, strong) NSNumber<Optional> *ad;

@end

@interface NewsResponseListModel : JSONModel

@property (nonatomic, assign) NSNumber<Optional> *pageIndex;// 当前页码
@property (nonatomic, strong) NSMutableArray<NewsModel, Optional> *list;// 列表

@end

@interface NewsResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;

@property (nonatomic, strong) NewsResponseListModel *data;

//@property (nonatomic, )
//@property (nonatomic, strong) NSMutableArray<Optional, NewsModel> *data;

@end
