//
//  NewRechargeTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableView.h"
#import "RechargeAmountTableViewCell.h"
#import "NewBankTableViewCell.h"

@protocol NewRechargeTableViewDelegate <NSObject>

-(void)selectBankcard;

-(void)selectChargeAmount:(NSInteger)amount;

-(void)doRecharge;

@end

typedef NS_ENUM(NSUInteger, RechargeTypeRow)
{
    RechargeTypeRowAlipay=0,
    RechargeTypeRowWechat,
    RechargeTypeRowHuanxun
};

#define DEFAULT_RECHARGE_AMOUNT 1000

@interface NewRechargeTableView : BaseTableView<UITableViewDelegate, UITableViewDataSource,RechargeAmountTableViewCellDelegate>

@property (nonatomic, weak) id<NewRechargeTableViewDelegate> customDelegate;

@property (nonatomic, assign) NSUInteger rechargeType; // 充值方式

@property (nonatomic, assign) NSUInteger rechargeAmount; // 充值金额

@end
