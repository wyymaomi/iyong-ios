//
//  AppDelegate+Notification.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AppDelegate+Notification.h"

@implementation AppDelegate (Notification)

- (void)setupNotification:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    DLog(@"SDK Version = %@", [CloudPushSDK getVersion]);
    // APNs注册，获取deviceToken并上报
    [self registerAPNS:application];
    // 初始化SDK
    [self initCloudPush];
    // 监听推送通道打开动作
    [self listenerOnChannelOpened];
    // 监听推送消息到达
    [self registerMessageReceive];
    // 点击通知将App从关闭状态启动时，将通知打开回执上报
    // [CloudPushSDK handleLaunching:launchOptions];(Deprecated from v1.8.1)
    [CloudPushSDK sendNotificationAck:launchOptions];
}

#pragma mark Channel Opened
/**
 *	注册推送通道打开监听
 */
- (void)listenerOnChannelOpened {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onChannelOpened:)
                                                 name:@"CCPDidChannelConnectedSuccess"
                                               object:nil];
}

/**
 *	推送通道打开回调
 *
 *	@param 	notification
 */
- (void)onChannelOpened:(NSNotification *)notification {
//    [MsgToolBox showAlert:@"温馨提示" content:@"消息通道建立成功"];
}


#pragma mark - APNs
/**
 *    注册苹果推送，获取deviceToken用于推送
 *
 *    @param     application
 */
- (void)registerAPNS:(UIApplication *)application {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        // iOS 8 Notifications
        UIUserNotificationSettings *settingsType = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil];
        [application registerUserNotificationSettings:settingsType];
        [application registerForRemoteNotifications];
    }
    else {
        // iOS < 8 Notifications
        UIRemoteNotificationType type = UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:type];
    }
}

#pragma mark SDK Init
- (void)initCloudPush {
    // 正式上线建议关闭
#ifdef DEBUG
    [CloudPushSDK turnOnDebug];
#endif
    // SDK初始化
    [CloudPushSDK asyncInit:AliyunPushNotificationAppKey appSecret:AliyunPushNotificationAppSecret callback:^(CloudPushCallbackResult *res) {
        if (res.success) {
            
            DLog(@"Push SDK init success, deviceId: %@.", [CloudPushSDK getDeviceId]);
            
//            [MsgToolBox showAlert:@"" content:[CloudPushSDK getDeviceId]];
            
            [AppConfig currentConfig].deviceId = [CloudPushSDK getDeviceId];
            
            // 如果用户token为空，则不上传
            
            if ([[UserManager sharedInstance] isLogin] /*&& !IsStrEmpty(loginRsaBody) && !IsStrEmpty(loginShaBody)*/) {
                [[UserManager sharedInstance] doAutoLogin:^(NSInteger code_status) {
                    if (code_status == STATUS_OK) {
                        [self.window makeKeyAndVisible];
                    }
                } failure:^(NSInteger code_status) {
                    [self.window makeKeyAndVisible];
                }];
            }
            
        } else {
            DLog(@"Push SDK init failed, error: %@", res.error);
            //            NSString *content = [NSString stringWithFormat:@"Push SDK init failed, error: %@", res.error];
            //            [MsgToolBox showAlert:@"提示" content:content];
            
            if ([[UserManager sharedInstance] isLogin]) {
                [[UserManager sharedInstance] doAutoLogin:^(NSInteger code_status) {
                    [self.window makeKeyAndVisible];
                } failure:^(NSInteger code_status) {
                    [self.window makeKeyAndVisible];
                }];
            }
        }
    }];
}

#pragma mark - 苹果推送注册成功回调

/*
 *  苹果推送注册成功回调，将苹果返回的deviceToken上传到CloudPush服务器
 */
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    DLog(@"Upload deviceToken to CloudPush server.");
    //    [MsgToolBox showAlert:@"提示" content:@"Upload deviceToken to CloudPush server."];
    [CloudPushSDK registerDevice:deviceToken withCallback:^(CloudPushCallbackResult *res) {
        if (res.success) {
            DLog(@"Register deviceToken success.");
            //            [MsgToolBox showAlert:@"提示" content:@"Upload deviceToken to CloudPush server."];
        } else {
            DLog(@"Register deviceToken failed, error: %@", res.error);
            //            NSString *content = [NSString stringWithFormat:@"Register deviceToken failed, error: %@", res.error];
            //            [MsgToolBox showAlert:@"提示" content:content];
        }
    }];
}

/*
 *  苹果推送注册失败回调
 */
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error);
    //    NSString *errorMsg = [NSString stringWithFormat:@"didFailToRegisterForRemoteNotificationsWithError %@", error];
    //    [MsgToolBox showAlert:@"ErrorMsg" content:errorMsg];
}

#pragma mark Notification Open
/*
 *  App处于启动状态时，通知打开回调
 */
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    
    DLog(@"receive RemoteNotification: %@", userInfo);
    
    if (application.applicationState == UIApplicationStateInactive)
    {
        [self performSelector:@selector(receiveRemoteNotification:) withObject:userInfo afterDelay:1];
    }
    else if (application.applicationState == UIApplicationStateActive)
    {
        [self receiveRemoteNotification:userInfo isActive:YES];
    }
}

- (void)receiveRemoteNotification:(NSDictionary *)userInfo
{
    [self receiveRemoteNotification:userInfo isActive:NO];
}

- (void)receiveRemoteNotification:(NSDictionary *)userInfo isActive:(BOOL)active
{
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications] ;
    
    // 取得APNS通知内容
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    // 内容
    NSString *content = [aps valueForKey:@"alert"];
    // badge数量
    NSInteger badge = [[aps valueForKey:@"badge"] integerValue];
    // 播放声音
    NSString *sound = [aps valueForKey:@"sound"];
    // 取得Extras字段内容
    NSString *Extras = [userInfo valueForKey:@"Extras"]; //服务端中Extras字段，key是自己定义的
    DLog(@"content = [%@], badge = [%ld], sound = [%@], Extras = [%@]", content, (long)badge, sound, Extras);
    // iOS badge 清0
    //    application.applicationIconBadgeNumber = 0;
    // 通知打开回执上报
    [CloudPushSDK sendNotificationAck:userInfo];
    
    [MsgToolBox showAlert:@"提示" content:[StringUtil getSafeString:content]];
    
//    [CloudPushSDK handleReceiveRemoteNotification:userInfo];
//    
//    if (active) {
//        
//        [MsgToolBox showAlert:@"提示" content:content];
//        
//        return;
//        
//    }
    
}

#pragma mark - 接收消息

- (void) registerMessageReceive {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onMessageReceived:)
                                                 name:@"CCPDidReceiveMessageNotification"
                                               object:nil];
}

- (void)onMessageReceived:(NSNotification *)notification {
    
    CCPSysMessage *message = [notification object];
    NSString *title = [[NSString alloc] initWithData:message.title encoding:NSUTF8StringEncoding];
    NSString *body = [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];
    DLog(@"Receive message title: %@, content: %@.", title, body);
    
    if (IsStrEmpty(body)) {
        return;
    }
    
    NSData *data = [body dataUsingEncoding:NSUTF8StringEncoding];
    id result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    if (![result isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    NSDictionary *messageDict = result;
    
    NSString *code = messageDict[@"code"];
    
    if ([code isEqualToString:BUSINESS_MSG_CODE]) {
        
        NSInteger count = [[AppConfig currentConfig].businessCircleNumber integerValue];
        count++;
        [AppConfig currentConfig].businessCircleNumber = [NSNumber numberWithInteger:count];
        [[NSNotificationCenter defaultCenter] postNotificationName:kBussinessCircleNewMessageNotification object:nil];
        
    }
}




@end
