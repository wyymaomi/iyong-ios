//
//  MyInvoiceTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/28.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableView.h"

typedef NS_ENUM(NSUInteger, MyInvoiceRow)
{
    MyInvoiceRowDrawInvoice,
    MyInvoiceRowInvoiceSetting,
    MyInvoiceRowInvoiceHistory
};

@protocol MyInvoiceTableViewDelegate <NSObject>

- (void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface MyInvoiceTableView : TPKeyboardAvoidingTableView<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSArray *cellTitleList;

@property (nonatomic, weak) id<MyInvoiceTableViewDelegate> customDelegate;

@end
