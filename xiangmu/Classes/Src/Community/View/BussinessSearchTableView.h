//
//  BussinessSearchTableView.h
//  xiangmu
//
//  Created by David kim on 16/9/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableView.h"

@class BussinessSearchHttpMessage;
@class BusinessCircleListViewController;

typedef NS_ENUM(NSInteger,BussinessCircleSearchRow)
{
    SearchRowCity = 0,
    SearchRowStartTime,
    SearchRowEndTime,
    SearchRowStartLocation,
    SearchRowServiceType
};

@protocol BussinessSearchTableViewDelegate <NSObject>

- (void)onSettingStartLocation;
- (void)search;
- (void)selectCity;

@end

@interface BussinessSearchTableView : BaseTableView<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) id<BussinessSearchTableViewDelegate> searchDelegate;
@property (nonatomic, strong) NSArray *titleList;
@property (nonatomic, strong) NSArray *iconList;
@property (nonatomic, strong) BussinessSearchHttpMessage *httpMessage;
//@property (nonatomic, weak) BusinessCircleListViewController *previousViewController;

//- (NSString *)getCityNameFromAreacode:(NSString*)areaCode;
//- (NSString*)getCityIdFromCityName:(NSString*)cityName;

@end
