//
//  ForgetPaypasswordViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ForgetPaypasswordViewController.h"
#import "MyUtil.h"
#import "MsgToolBox.h"
#import "ForgetPaypwdViewModel.h"
#import "EncryptUtil.h"

@interface ForgetPaypasswordViewController ()

@property (nonatomic, strong) ForgetPaypwdViewModel *viewModel;
@property (nonatomic, strong) UIButton *verifyCodeButton;

@property (nonatomic, strong) UITextField *verifyCodeTextField;
@property (nonatomic, strong) UITextField *payPasswordTextField;
@property (nonatomic, strong) UITextField *repayPasswordTextField;

@end

@implementation ForgetPaypasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"重置交易密码";
    
    [self.view addSubview:self.tpkeyboardAvoidingGroupTableView];
    self.tpkeyboardAvoidingGroupTableView.rowHeight = DefaultTableRowHeight;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(resetPaypwd)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.verifyCodeButton cancelTimer];
}

#pragma mark - 重置支付验证码

- (void)resetPaypwd
{

    
    // 获取验证码和新的支付密码
//    NSString *verifyCode =
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:ForgetPaypwdRowVerifyCode];
//    UITableViewCell *cell = (UITableViewCell*)[self.tpkeyboardAvoidingGroupTableView cellForRowAtIndexPath:indexPath];
    
    
    self.viewModel.rePayPassword = self.repayPasswordTextField.text.trim;
    self.viewModel.payPassword = self.payPasswordTextField.text.trim;
    self.viewModel.verificationCode = self.verifyCodeTextField.text.trim;
    
    NSString *errorMsg;
    
    if (![self.viewModel validateHttpDTO:&errorMsg]) {
        [MsgToolBox showAlert:@"" content:errorMsg];
        return;
    }
    
    self.nextAction = @selector(resetPaypwd);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_RESET_API];
    NSString *params = [NSString stringWithFormat:@"captcha=%@&password=%@", self.viewModel.verificationCode, [EncryptUtil getRSAEncryptPassword:self.viewModel.payPassword]];
    [self showHUDIndicatorViewAtCenter:@"正在重置交易密码，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary * responseData) {
//        StrongSelf
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"重置交易密码成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
                
            }];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf showAlertView:errorDesc];
        [weakSelf resetAction];
        
    }];
    
    
}

#pragma makr - 获取验证码

- (void)doGetVerifyCode
{
    
    self.nextAction = @selector(doGetVerifyCode);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    NSString *mobile = [UserManager sharedInstance].userModel.username;
    
    NSString *error;
    if (![UserHelper validatePhoneNum:mobile error:&error]) {
        [self showAlertView:error];
        return;
    }

    
    [self showHUDIndicatorViewAtCenter:MSG_GET_VERIFY_CODING];
//    [self showHUDIndicatorViewAtCenter:mobile];
    WeakSelf
    [[UserManager sharedInstance] doGetVerifyCode:mobile success:^(NSInteger code_status) {
//        StrongSelf
        [NSThread sleepForTimeInterval:2];
        [weakSelf hideHUDIndicatorViewAtCenter];
        if (code_status == STATUS_OK) {
            [weakSelf.verifyCodeButton countDownTime:60 countDownBlock:^(NSUInteger timer) {
                weakSelf.verifyCodeButton.enabled = NO;
                [weakSelf.verifyCodeButton setTitle:[NSString stringWithFormat:@"%d秒后重发", timer] forState:UIControlStateNormal];
                weakSelf.verifyCodeButton.titleLabel.textColor = [UIColor whiteColor];
            } outTimeBlock:^{
                weakSelf.verifyCodeButton.enabled = YES;
                [weakSelf.verifyCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
            }];
        }
        else {
            [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
//            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
        
    } failure:^(NSInteger code_status) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        if (code_status == NETWORK_FAILED) {
            [strongSelf showAlertView:MSG_GET_VERIFY_FAILURE];
        }
        else{
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    }];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 25*Scale;
    }
    return 0.0001f;
}


- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 2) {
//        return 80;
        return 0.00001f;
    }
    return 0.0001f;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 25*Scale)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, ViewWidth, 25*Scale)];
        NSString *encodeMobile = [self.viewModel encryptMobileText:[UserManager sharedInstance].userModel.username];
        label.text = [NSString stringWithFormat:@"您预留的手机号为%@", encodeMobile];
        label.font = FONT(12);
        label.textColor = [UIColor lightGrayColor];
        label.backgroundColor = [UIColor clearColor];
        [view addSubview:label];
        return view;
        
    }
    
    return [UIView new];
}

//- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    if (section == 2) {
//        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 50)];
//        
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        button.frame = CGRectMake(10, 20, ViewWidth-20, 50);
//        [button blueStyle];
//        [button setTitle:@"确定" forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(resetPaypwd) forControlEvents:UIControlEventTouchUpInside];
//        [view addSubview:button];
//        
//        return view;
//    }
//    
//    return [UIView new];
//
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellid=@"cellid";
    NSArray *nameArray=@[@"验证码",@"交易密码",@"确认密码"];
    NSArray *placeholderArray = @[@"请输入验证码", @"请输入6位交易密码", @"请再次输入以确认"];
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, DefaultTableRowHeight)];
        [cell.contentView addSubview:textField];
        textField.keyboardType=UIKeyboardTypeNumberPad;
        textField.tag = 999;
        textField.backgroundColor = [UIColor whiteColor];
        textField.font = DefaultTableRowSubtitleFont;//FONT(14);
        textField.placeholder = placeholderArray[indexPath.section];
        
        if (indexPath.section == ForgetPaypwdRowVerifyCode) {
            self.verifyCodeTextField = textField;
        }
        if (indexPath.section == ForgetPaypwdRowNewPassword) {
            self.payPasswordTextField = textField;
        }
        if (indexPath.section == ForgetPaypwdRowRenewPassword) {
            self.repayPasswordTextField = textField;
        }
        
        UILabel *titleLabel = [MyUtil createLabelFrame:CGRectMake(20, 0, 100, DefaultTableRowHeight) title:@"" font:DefaultTableRowTitleFont textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:DefaultTableRowTitleTextColor];
        titleLabel.text = nameArray[indexPath.section];
        
        UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, DefaultTableRowHeight)];
        [leftView addSubview:titleLabel];
        textField.leftView = leftView;
        textField.leftViewMode = UITextFieldViewModeAlways;
        
        if (indexPath.section == ForgetPaypwdRowVerifyCode) {
            UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(ViewWidth-80*Scale, (DefaultTableRowHeight-30*Scale)/2, 80*Scale, 30*Scale)];
            UIButton *verifyCodeButton=[UIButton buttonWithType:UIButtonTypeCustom];
            [verifyCodeButton setFrame:CGRectMake(0,0,70*Scale,30*Scale)];
            [verifyCodeButton addTarget:self action:@selector(doGetVerifyCode) forControlEvents:UIControlEventTouchUpInside];
            [verifyCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
            verifyCodeButton.titleLabel.font = FONT(13*Scale);
            verifyCodeButton.titleLabel.textAlignment = UITextAlignmentCenter;
//            verifyCodeButton.backgroundColor = UIColorFromRGB(0x27558c);
            [verifyCodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [verifyCodeButton blueStyle];
            verifyCodeButton.layer.cornerRadius = 5;
            [verifyCodeButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
            [verifyCodeButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateNormal];
            [rightView addSubview:verifyCodeButton];
            textField.rightView = rightView;
            textField.rightViewMode = UITextFieldViewModeAlways;
            self.verifyCodeButton = verifyCodeButton;
        }
    }
    
//    UITextField *textField = [cell viewWithTag:999];
//    if (indexPath.section == ForgetPaypwdRowVerifyCode) {
//        textField.text = self.viewModel.verificationCode;
//    }
//    if (indexPath.section == ForgetPaypwdRowNewPassword) {
//        textField.text = self.viewModel.payPassword;
//    }
//    if (indexPath.section == ForgetPaypwdRowRenewPassword) {
//        textField.text = self.viewModel.rePayPassword;
//    }

    return cell;
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];

//    if (indexPath.row==0) {
//        PasswordSetViewController *mimaVC=[[PasswordSetViewController alloc]init];
//        mimaVC.type=MimaTypeLoad;
//        [self.navigationController pushViewController:mimaVC animated:YES];
//    }
//    else if (indexPath.row==1)
//    {
//        GesturePasswordController *viewController = [[GesturePasswordController alloc] init];
//        viewController.source_from = SOURCE_FROM_ABOUT_ME;
//        [self.navigationController pushViewController:viewController animated:YES];
//    }
//    else
//    {
//        
//        PasswordSetViewController *mimaVC=[[PasswordSetViewController alloc]init];
//        mimaVC.type=MimaTypeJiaoyi;
//        [self.navigationController pushViewController:mimaVC animated:YES];
//        
//    }
    //    [self.navigationController pushViewController:mimaVC animated:YES];
//}

#pragma mark - getter and setter

- (ForgetPaypwdViewModel*)viewModel
{
    if (!_viewModel) {
        _viewModel = [ForgetPaypwdViewModel new];
    }
    return _viewModel;
}


@end
