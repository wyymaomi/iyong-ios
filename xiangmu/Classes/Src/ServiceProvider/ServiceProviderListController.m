//
//  ServiceProviderListController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ServiceProviderListController.h"
#import "OnlineServiceTableViewCell.h"
#import "OnlineServiceModel.h"
#import "ServiceProviderViewModel.h"
#import "ProviderDetailViewController.h"
#import "ProviderCollectionView.h"

#import "CustomRadioButtonGroup.h"
//#import "YYPhotoGroupView.h"
#import "YYStatusView.h"
#import "AppConfig.h"

#import "ServiceProviderListController+NetworkRequest.h"


@interface ServiceProviderListController ()<UIActionSheetDelegate,ServiceCellDelegate, PullRefreshCollectionViewDelegate, ProviderCollectionViewDelegate>



@end

@implementation ServiceProviderListController

- (ProviderCollectionView*)collectionView
{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat itemWidth = (ViewWidth-15*3)/2;
        CGFloat itemHeight = itemWidth+34*Scale;
        flowLayout.itemSize = CGSizeMake(itemWidth, itemHeight);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.minimumLineSpacing = 15;
        flowLayout.minimumInteritemSpacing = 15;
        
        _collectionView = [[ProviderCollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:flowLayout];
        _collectionView.scrollEnabled = YES;
        _collectionView.customDelegate = self;
//        _collectionView.
    }
    return _collectionView;
}

- (PageUtil*)commonPage
{
    if (_commonPage == nil) {
        _commonPage = [PageUtil new];
        _commonPage.pageIndex = 1;
    }
    return _commonPage;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.collectionView];
    self.collectionView.pullRefreshDelegate = self;
    self.collectionView.customDelegate = self;
    
    [self.collectionView beginHeaderRefresh];
 
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    if (_fpsLabel) {
//        _fpsLabel 
//    }
    
    
}

- (void)initViews
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onSelectItem:(NSInteger)row;
{
    OnlineServiceModel *data = self.dataList[row];
    ProviderDetailViewController *viewController = [[ProviderDetailViewController alloc] init];
    viewController.companyId = data.companyId;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - 跳转到服务商编辑页面

#pragma mark - PullRefresh delegate method

-(void)onHeaderRefresh
{

    
    if ([self.title isEqualToString:@"我的收藏"]) {
        self.nextAction = @selector(onHeaderRefresh);
        self.object1 = nil;
        self.object2 = nil;
        
        if ([self needLogin]) {
            return;
        }
        
        [self fetchMyFavoriteList:nil];
    }
    else {
        self.commonPage.pageIndex = 1;
        [self fetchServiceProviderByPageIndex:self.commonPage.pageIndex];
    }

    
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {

        [self onHeaderRefresh];
        return;
    }
    
    if (self.dataList.count > 0) {

        
        if ([self.title isEqualToString:@"我的收藏"]) {
            
            self.nextAction = @selector(onFooterRefresh);
            self.object1 = nil;
            self.object2 = nil;
            
            if ([self needLogin]) {
                return;
            }
            
            OnlineServiceModel *model = [self.dataList lastObject];
            [self fetchMyFavoriteList:model.collectionTime];
        }
        else {
            [self fetchServiceProviderByPageIndex:self.commonPage.pageIndex];
        }
        
    }
    
}




@end
