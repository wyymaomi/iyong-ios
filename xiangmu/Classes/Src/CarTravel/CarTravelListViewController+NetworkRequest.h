//
//  CarTravelListViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/27.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CarTravelListViewController.h"
#import "TravelOrderModel.h"

@interface CarTravelListViewController (NetworkRequest)

-(void)fetchOrderList:(NSUInteger)listStatus time:(NSString*)time;

@end
