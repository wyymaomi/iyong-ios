//
//  SettingTableView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableView.h"

@protocol SettingTableViewDelegate <NSObject>

- (void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface SettingTableView : BaseTableView

@property (nonatomic, weak) id<SettingTableViewDelegate> settingDelegate;

@property (nonatomic, strong) NSArray *cellList;

//@property (nonatomic, strong) NSArray *nextVcList;

@end
