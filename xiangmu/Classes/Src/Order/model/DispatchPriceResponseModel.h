//
//  DispatchPriceModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@interface DispatchPriceResponseModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *dstCompanyId;
@property (nonatomic, strong) NSString<Optional> *companyInfo;// 公司名称
@property (nonatomic, strong) NSString<Optional> *editCompanyInfo;
@property (nonatomic, strong) NSString<Optional> *dstPrice;// 发单价
@property (nonatomic, strong) NSString<Optional> *editDstPrice;// 编辑的派单价
@property (nonatomic, strong) NSString<Optional> *srcPrice;// 接单价
@property (nonatomic, strong) NSNumber<Optional> *end;
@property (nonatomic, strong) NSString<Optional> *id;// 派单信息ID
@property (nonatomic, strong) NSString<Optional> *orderId;
@property (nonatomic, strong) NSString<Optional> *srcCompanyId;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSNumber<Optional> *rollbackStatus;// 改派状态

@end
