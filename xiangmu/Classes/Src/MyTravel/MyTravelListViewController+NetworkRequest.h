//
//  MyTravelListViewController+NetworkRequest.h
//  iYONGDriver
//
//  Created by 湛思科技 on 2017/7/7.
//  Copyright © 2017年 Shanghai Zhansi Tech. All rights reserved.
//

#import "MyTravelListViewController.h"
#import "TravelModel.h"

@interface MyTravelListViewController (NetworkRequest)

-(void)fetchTraveList:(NSString*)time driverId:(NSString*)driverId;

@end
