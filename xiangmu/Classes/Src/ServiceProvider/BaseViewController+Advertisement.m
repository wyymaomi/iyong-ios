//
//  ServiceProviderRootViewController+Advertisement.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController+Advertisement.h"
//#import "BaseWebViewController.h"
#import "BaseWebpageViewController.h"
#import "AdvModel.h"
#import "AdvDAO.h"
#import "AppConfig.h"
#import "ProviderDetailViewController.h"

@protocol BaseViewController <NSObject>

@end

@implementation BaseViewController (Advertisement)

#pragma mark - 广告页面

- (void)showAdvFirstTime
{
    // 一天只显示一次广告
    NSString *currentDate = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
    NSString *lastAdvTime = [AppConfig currentConfig].advLastUpdateStr;
#if 0
    if ([currentDate isEqualToString:lastAdvTime]) {
        return;
    }
#endif
//#ifdef DEBUG
//    [self showAdv:NO];
//    return;
//#endif
    
    // 只显示一次广告
    if (IsStrEmpty(lastAdvTime)) {
        [self showAdv:NO];
    }
    

    
}

-(void)showMaskView
{
    if (self.maskView == nil) {
        self.maskView = [[MaskView alloc] init];
    }
    
    [self.maskView showMaskView];
}

- (void)showAdv:(BOOL)bAll
{
#if 0
    if (self.adView == nil) {
        self.adView = [SIDADView new];
    }
    [self.adView show];
    self.adView.delegate = self;
    self.advDisplayType = AdvDisplayTypeInDisplay;
    
    NSString *currentDate = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
    DLog(@"currentDate = %@", currentDate);
    [AppConfig currentConfig].advLastUpdateStr = currentDate;
    
    return;
#endif
    
    // 一天只显示一次广告
    
    WeakSelf
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ADV_LIST_URL];
    NSDictionary *params = @{/*@"time":time,*/
                             @"type":@1,
                             @"areaCode": @"",
//                             @"areaCode":[AppConfig currentConfig].defaultCityCode,
                             @"clientType": @1};
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        StrongSelf
        if (code_status == STATUS_OK) {
            
            AdvResponseData *responseData = [[AdvResponseData alloc] initWithDictionary:result error:nil];
            
            if (IsArrEmpty(responseData.data)) {
                return;
            }
            
            // 如果没有广告
            if (responseData.data.count == 0) {
                return;
            }
            
            NSString *currentDate = [[NSDate date] stringWithDateFormat:@"yyyy-MM-dd"];
            DLog(@"currentDate = %@", currentDate);
            [AppConfig currentConfig].advLastUpdateStr = currentDate;
            
            if (strongSelf.adView == nil) {
                strongSelf.adView = [SIDADView new];
                strongSelf.adView.delegate = strongSelf;
            }
            
            strongSelf.advDisplayType = AdvDisplayTypeInDisplay;
            
            strongSelf.advList = responseData.data;
            
            [strongSelf.adView showFirstMaskView];
            
            [strongSelf.adView showAdv:responseData.data];
            
//            [strongSelf.adView show:responseData.data borderColor:[UIColor clearColor]];
//            [strongSelf.adView show:responseData.data borderColor:[UIColor clearColor]];
            
        }
        else if (code_status == NETWORK_FAILED)
        {
            //            [weakSelf showAlertView:MSG_GET_VERIFY_FAILURE];
        }
        else {
            //            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
        
    }];
    
    //    }
}

-(double)getMaxUpdateTime:(NSArray*)dataList
{
    
    double maxUpdateTime = 0L;
    for (AdvModel *adv in dataList) {
        if ([adv.updateTime doubleValue] > maxUpdateTime) {
            maxUpdateTime = [adv.updateTime doubleValue];
        }
    }
    return maxUpdateTime;
}

- (void)onClickAdvIcon;
{
    
    AdvDAO *advDAO = [AdvDAO new];
    NSArray *localAdvList = [advDAO getAllAdv];
    
    if (localAdvList.count == 0) {
        [self showAdv:YES];
        return;
    }
    
    if (self.adView == nil) {
        self.adView = [SIDADView new];
        self.adView.delegate = self;
    }
    [self.adView showAdv:localAdvList];
//    [self.adView show:localAdvList borderColor:[UIColor clearColor]];
    self.advList = localAdvList;
    
}




- (void)onClickAdv:(NSInteger)index;
{
    
    AdvModel *advModel = self.advList[index];
    
    if (advModel.isRedpack) {
        return;
    }
    
    if (IsStrEmpty(advModel.linkUrl.trim)) {
        if (IsStrEmpty(advModel.companyId)) {
            return;
        }
        else {
            NSString *companyId = advModel.companyId;
            ProviderDetailViewController *viewController = [[ProviderDetailViewController alloc] init];
            viewController.companyId = companyId;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        return;
    }
    
    [self gotoWebpage:advModel.linkUrl];
    
//    BaseWebpageViewController *viewController = [[BaseWebpageViewController alloc] init];
//    viewController.url = advModel.linkUrl;
//    viewController.enter_type = ENTER_TYPE_PUSH;
//    viewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:viewController animated:YES];
    
    self.advDisplayType = AdvDisplayTypeInit;
//    self.showAdv  = NO;
    //    self.isFirstIn = NO;
    [self.adView dismiss];
}


- (void)onDismiss;
{
//    self.showAdv = NO;
    //    self.isFirstIn = NO;
    self.advDisplayType = AdvDisplayTypeInit;
    [self.adView dismiss];
}

#pragma mark - getter and setter

-(void)setMaskView:(MaskView *)maskView
{
    objc_setAssociatedObject(self, &kMaskView, maskView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(MaskView*)maskView
{
    return objc_getAssociatedObject(self, &kMaskView);
}

-(void)setAdView:(SIDADView *)adView
{
    objc_setAssociatedObject(self, &kAdView, adView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(SIDADView*)adView
{
    return objc_getAssociatedObject(self, &kAdView);
}

-(void)setAdvList:(NSArray *)advList
{
    objc_setAssociatedObject(self, &kAdvList, advList, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSArray*)advList
{
    return objc_getAssociatedObject(self, &kAdvList);
}


@end
