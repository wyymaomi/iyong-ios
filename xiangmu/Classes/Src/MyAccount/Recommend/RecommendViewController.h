//
//  TuijianViewController.h
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "TuijianDetailViewController.h"

@interface RecommendViewController : LFNavController<Tuijian>

@end
