//
//  OrderHelpViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderHelpViewController.h"
#import "OrderStatusTableViewCell.h"
#import "OrderHelperDetailViewController.h"

@interface OrderHelpViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation OrderHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"订单常见问题";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderStatusTableViewCell *cell = [OrderStatusTableViewCell cellWithTableView:tableView];
    
    cell.badgeButton.hidden = YES;
    
    switch (indexPath.row) {
        case 0:
            cell.titleLabel.text = @"关于下单";
            break;
        case 1:
            cell.titleLabel.text = @"关于支付";
            break;
        case 2:
            cell.titleLabel.text = @"关于结算";
            break;
    }

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OrderHelperDetailViewController *viewController = [[OrderHelperDetailViewController alloc] init];
    viewController.selectedIndex = indexPath.row;
    OrderStatusTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    viewController.title = cell.titleLabel.text;
    [self.navigationController pushViewController:viewController animated:YES];
    
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}





@end
