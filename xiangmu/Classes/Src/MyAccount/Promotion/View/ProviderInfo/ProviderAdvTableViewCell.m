//
//  ProviderAdvTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderAdvTableViewCell.h"
#import "ProviderUtil.h"

@implementation ProviderAdvDetailView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, frame.size.height)];
        _dateLabel.textColor = NAVBAR_BGCOLOR;
        _dateLabel.font = FONT(12);
        [self addSubview:_dateLabel];

        _timeSegmentLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width, 0, 30, frame.size.height)];
        _timeSegmentLabel.textColor = NAVBAR_BGCOLOR;
        _timeSegmentLabel.textAlignment=UITextAlignmentCenter;
        _timeSegmentLabel.font = FONT(12);
        [self addSubview:_timeSegmentLabel];
        
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width-60, 0, 60, frame.size.height)];
        _priceLabel.textColor = NAVBAR_BGCOLOR;
        _priceLabel.textAlignment = UITextAlignmentRight;
        _priceLabel.font = FONT(12);
        [self addSubview:_priceLabel];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _dateLabel.frame = CGRectMake(0, 0, 70, self.height);
    _priceLabel.frame = CGRectMake(self.width-60, 0, 60, self.height);
    _timeSegmentLabel.frame = CGRectMake(30, 0, self.width-60, self.height);
}

- (void)initData:(NSString*)date timeSegment:(NSString*)timeSegment price:(NSNumber*)price
{
    _dateLabel.text = date;
    _timeSegmentLabel.text = timeSegment;
    _priceLabel.text = [NSString stringWithFormat:@"¥%@", [price stringValue]];
}


@end

@implementation ProviderAdvTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"ProviderAdvTableViewCell";
    ProviderAdvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return cell;

}

- (void)initData:(ProviderOrderModel*)order;
{
    if (!order) {
        return;
    }
    
    self.positionLabel.text = [NSString stringWithFormat:@"第%@版位", order.position];
    self.titleLabel.text = [ProviderUtil displayAdvTitle:[order.type integerValue]];
    [self.statusButton setTitle:[ProviderUtil displayAdOrderStatusText:[order.orderStatus integerValue]] forState:UIControlStateNormal];
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%@元", order.price];

    NSUInteger i = 0;
    for (ProviderOrderDetailModel *detailModel in order.details) {
        CGFloat height = 18;
        ProviderAdvDetailView *detailView = [[ProviderAdvDetailView alloc] initWithFrame:CGRectMake(0, i*height , ViewWidth-142.5-34, height)];
        detailView.dateLabel.text = [ProviderUtil getAdOrderDate:detailModel.startTime];
        detailView.priceLabel.text = [NSString stringWithFormat:@"¥%@", detailModel.price];
        detailView.timeSegmentLabel.text = [ProviderUtil getAdOrderTimeSegment:detailModel.startTime endTime:detailModel.endTime];
        [self.orderDetailView addSubview:detailView];
        i++;
        
    }
    
    if ([order.orderStatus integerValue] == AdOrderStatusPaySuccess) {
        self.statusButton.hidden = NO;
        self.deleteButton.hidden = YES;
        
        self.cancelButton.hidden = YES;
        self.payButton.hidden = YES;
        self.cancelledButton.hidden = YES;
        self.expireButton.hidden = YES;
        
        self.statusButton.left = self.deleteButton.left;
    }
    else if ([order.orderStatus integerValue] == AdOrderStatusWaitPay || [order.orderStatus integerValue] == AdOrderStatusPayFailure) {
        
        self.payButton.hidden = NO;
        self.cancelButton.hidden = NO;
        
        self.statusButton.hidden = YES;
        self.deleteButton.hidden = YES;
        self.cancelledButton .hidden = YES;
        self.expireButton.hidden = YES;
        
        self.payButton.frame = self.deleteButton.frame;
        self.cancelButton.frame = self.statusButton.frame;
    }
    else if ([order.orderStatus integerValue] == AdOrderStatusCancelled) {
        self.cancelledButton.hidden = NO;
        self.deleteButton.hidden = NO;
        
        self.payButton.hidden = YES;
        self.cancelButton.hidden = YES;
        self.expireButton.hidden = YES;
        self.statusButton.hidden = YES;
        
//        self.cancelledButton.frame = self.statusButton.frame;
        
    }
    else if([order.orderStatus integerValue] == AdOrderStatusExpire){
        
        self.expireButton.hidden = NO;
        self.deleteButton.hidden = NO;
        
        self.statusButton.hidden = YES;
        self.cancelledButton.hidden = YES;
        self.payButton.hidden = YES;
        self.cancelButton.hidden = YES;
        
//        self.expireButton.frame = self.statusButton.frame;
    }
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.cancelButton.frame = self.statusButton.frame;
    
    self.payButton.frame = self.deleteButton.frame;
    
    self.cancelButton.frame = self.statusButton.frame;
    
    self.expireButton.frame = self.statusButton.frame;
    
    self.cancelledButton.frame = self.statusButton.frame;
    
}

+(CGFloat)getCellHeight
{
    return 204;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
