//
//  ZhanghuYueViewController.h
//  xiangmu
//
//  Created by David kim on 16/5/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"

@interface AccountBalanceViewController : LFNavController
@property (nonatomic, assign) NSUInteger mode;
@property (nonatomic, strong) NSString *accountBalance;
@end
