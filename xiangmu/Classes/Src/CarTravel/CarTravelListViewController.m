//
//  CarTravelListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CarTravelListViewController.h"
#import "CarTravelListViewController+NetworkRequest.h"
#import "CarTravelTableViewCell.h"
#import "CustomSegment.h"

@interface CarTravelListViewController ()<CustomSegmentDelegate>

@property (nonatomic, strong) CustomSegment *segment;


@end

@implementation CarTravelListViewController

-(CustomSegment*)segment
{
    if (!_segment) {
        _segment = [[CustomSegment alloc] initWithFrame:CGRectZero];
    }
    return _segment;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"我的行程";
    
    self.segment.frame = CGRectMake(0, 0, ViewWidth, 40*Scale);
    self.segment.items = @[@"预约中", @"行程中", @"已完成"];
    self.segment.delegate = self;
    [self.view addSubview:self.segment];
    
    self.listStatus = 1;
    
    if (self.source_from == 1) {
        self.listStatus = 2;
        self.segment.currentIndex = 1;
    }

    self.pullRefreshTableView.top = self.segment.bottom+10*Scale;
    self.pullRefreshTableView.height = self.view.frame.size.height-self.pullRefreshTableView.top;
    [self.view addSubview:self.pullRefreshTableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveCancelOrderMessage:) name:kOnCancelCarOrderSuccessNotification object:nil];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]]
    
    [self setRightNavigationItemWithTitle:@"开发票" selMethod:@selector(gotoMyInvoicePage)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.pullRefreshTableView beginHeaderRefresh];
}

-(void)pressedBackButton
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kOnCancelCarOrderSuccessNotification object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    
    if ([self needLogin]) {
        return;
    }
    
    [self fetchOrderList:self.listStatus time:nil];
    
}

-(void)onFooterRefresh
{
    self.nextAction = @selector(onFooterRefresh);
    
    if ([self needLogin]) {
        return;
    }
    
    TravelOrderModel *model = [self.dataList lastObject];
    [self fetchOrderList:self.listStatus time:[model.createTime stringValue]];
    
//    GrabOrderModel *model = [self.dataList lastObject];
//    [self fetchCanreceiveOrderList:[model.createTime stringValue]];
}

#pragma mark - 

-(void)didSelectAtIndex:(NSInteger)index;
{
    self.listStatus = index+1;
    [self.pullRefreshTableView beginHeaderRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return 1;
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    return 236;
//    return [CarTravelTableViewCell getCellHeight];
    TravelOrderModel *model = self.dataList[indexPath.row];
    NSUInteger orderStatus = [model.orderStatus integerValue];
    switch(orderStatus) {
        case CarOrderStatusWaitReceive:// 待接单
        case CarOrderStatusTravelComplete:// 已经完成
        case CarOrderStatusTravelCancelled:// 已取消
        case CarOrderStatusPayInConfirming: // 支付确认中
        case CarOrderStatusPayExpire:// 支付已过期
            return CarTravelWaitReceiveCellHeight;
            break;
        case CarOrderStatusPendingPay:// 待支付
        case CarOrderStatusTravelStarted:// 已开始行程
        case CarOrderStatusTravelWaitStart:
        
            return CarTravelPendingPayCellHeight;
    }
    
    
    return 0.00001f;
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarTravelTableViewCell *cell = [CarTravelTableViewCell cellWithTableView:tableView indexPath:indexPath];
    
    TravelOrderModel *model = self.dataList[indexPath.row];
    [cell initData:model];
    cell.cancelOrderButton.tag = indexPath.row;
    [cell.cancelOrderButton addTarget:self action:@selector(onCancelOrder:) forControlEvents:UIControlEventTouchUpInside];
    cell.payButton.tag = indexPath.row;
    [cell.payButton addTarget:self action:@selector(goPay:) forControlEvents:UIControlEventTouchUpInside];
    cell.contactButton.tag = indexPath.row;
    [cell.contactButton addTarget:self action:@selector(onContactCustom:) forControlEvents:UIControlEventTouchUpInside];
    
    NSUInteger orderStatus = [model.orderStatus integerValue];
    if (orderStatus == CarOrderStatusWaitReceive) {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TravelOrderModel *model = self.dataList[indexPath.row];
    NSUInteger orderStatus = [model.orderStatus integerValue];
    if (orderStatus == CarOrderStatusWaitReceive) {
        
        VechileOrderHttpMessage *vechileOrder = [VechileOrderManager sharedInstance].vechileOrderHttpMessage;
        vechileOrder.orderId = model.id;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[model.startTime doubleValue]/1000.0f];
        vechileOrder.carDateTime = [date stringWithDateFormat:kNSDateHelperFormatSQLDateWithMonth];
        vechileOrder.startLocation = model.startLocation;
        vechileOrder.endLocation = model.endLocation;
        vechileOrder.carType = [model.carType integerValue];
        vechileOrder.distance = [model.mileage floatValue];
        vechileOrder.carUnitPrice = [model.unitPrice floatValue];
        vechileOrder.carPrice = [model.amount floatValue];
        
        [self gotoPage:@"StartOrderViewController"];
        
        
    }
    
    //    TraceModel *model = self.dataList[indexPath.row];
//    TravelModel *model = self.dataList[indexPath.row];
    
    //    VechileLocationViewController *viewController = [VechileLocationViewController new];
    //    viewController.entityName = model.id;
    //    [self.navigationController pushViewController:viewController animated:YES];
    
//    self.pageIndex = 1;
//    //    _curTraceModel = model;
//    _curTravelModel = model;
//    [self.trackResultList removeAllObjects];
//    [self showHUDIndicatorViewAtCenter:@"正在查询，请稍候..."];
//    NSUInteger startTime = ([_curTravelModel.startTime integerValue])/1000;
//    NSUInteger endTime = ([_curTravelModel.endTime integerValue])/1000;
//    [self queryTrackHistory:startTime endTimeStamp:endTime pageIndex:self.pageIndex];
    
    
}

-(void)onCancelOrder:(UIButton*)button
{
    NSUInteger tag = button.tag;
    
    TravelOrderModel *model = self.dataList[tag];
    
    NSInteger carOrderStatus = [model.orderStatus integerValue];
    NSInteger carMode = [model.useCarMode integerValue];
    if (carOrderStatus == CarOrderStatusTravelWaitStart) {
        
        [[VechileOrderManager sharedInstance] cancelOrder:model.id carOrderMode:carMode viewController:self isCompletePay:YES];
//        [[VechileOrderManager sharedInstance] cancelOrder]
//        [[VechileOrderManager sharedInstance] cancelOrder:model.id viewController:self isCompletePay:YES];
    }
    else if (carOrderStatus == CarOrderStatusWaitReceive || carOrderStatus == CarOrderStatusPendingPay) {
        [[VechileOrderManager sharedInstance] cancelOrder:model.id carOrderMode:carMode viewController:self isCompletePay:NO];
        
//        [[VechileOrderManager sharedInstance] cancelOrder:model.id viewController:self isCompletePay:NO];
    }
    
}

-(void)onReceiveCancelOrderMessage:(id)sender
{
    [self.pullRefreshTableView beginHeaderRefresh];
}

-(void)goPay:(UIButton*)button
{
    NSUInteger tag = button.tag;
    
    TravelOrderModel *model = self.dataList[tag];
    
    VechileOrderHttpMessage *vechileOrder = [VechileOrderManager sharedInstance].vechileOrderHttpMessage;
    
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:[model.startTime doubleValue]/1000.0f];
    vechileOrder.carDateTime = [startDate stringWithDateFormat:kNSDateHelperFormatSQLDateWithMonth];
    
    vechileOrder.orderId = model.id;
    vechileOrder.carPrice = [model.amount floatValue];
    vechileOrder.carUnitPrice = [model.unitPrice floatValue];
    vechileOrder.distance = [model.mileage floatValue];
    vechileOrder.driverMobile = model.driverMobile;
    NSString *surname = [model.driverName substringFromIndex:0 toIndex:1];
    vechileOrder.driverName = [NSString stringWithFormat:@"%@师傅", surname];
    vechileOrder.startLocation = model.startLocation;
    vechileOrder.endLocation = model.endLocation;
    vechileOrder.carModel = model.carModel;
    vechileOrder.carType = [model.carType integerValue];
    vechileOrder.carNumber = model.carNumber;
    vechileOrder.receiveOrderTime = [model.receiveOrderTime doubleValue];
    
    if ([[VechileOrderManager sharedInstance] isPendingPayOvertime]) {
        [MsgToolBox showToast:@"支付超时，请重新下单"];
        return;
    };
    
    
    [self gotoPage:@"OrderPayViewController"];
    
}

-(void)onContactCustom:(UIButton*)button
{
    NSUInteger tag = button.tag;
    
    TravelOrderModel *model = self.dataList[tag];
    
    if (IsStrEmpty(model.driverMobile)) {
        return;
    }
    
    [[VechileOrderManager sharedInstance] contactDriver:model.driverMobile];

    
}

-(void)gotoMyInvoicePage
{
    [self gotoPage:@"MyInvoiceViewController"];
}




@end
