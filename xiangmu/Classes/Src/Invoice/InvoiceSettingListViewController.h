//
//  InvoiceSettingListViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/30.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "InvoiceinfoHttpMessage.h"

typedef NS_ENUM(NSUInteger, InvoiceListSourceFrom)
{
    InvoiceListSourceFromMyInvoice=0,
    InvoiceListSourceFromEditInfo
};

@interface InvoiceSettingListViewController : BaseViewController

@property (nonatomic, assign) NSUInteger sourceFrom;

@property (nonatomic, copy) void (^completeHandle)(InvoiceSettingHttpMessage *settingHttpMessage);

@end
