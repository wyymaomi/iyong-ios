//
//  MyInvoiceTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/28.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MyInvoiceTableView.h"
#import "MyTableViewCell.h"

@implementation MyInvoiceTableView

-(NSArray*)cellTitleList
{
    if (!_cellTitleList) {
        _cellTitleList = @[@"金额开票",@"开票设置",@"开票历史"];
    }
    return _cellTitleList;
}

-(id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    
    if (self) {
        [self setupView];
    }
    
    return self;
}



- (void)setupView {
    
    self.delegate = self;
    self.dataSource = self;
    
    self.tableFooterView = [[UIView alloc] init];
    self.rowHeight = DefaultTableRowHeight;
    
}



#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return self.cellTitleList.count;
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSArray *list = self.cellList[section];
//    return list.count;
    return self.cellTitleList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellId";
    
    MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    
    if (!cell) {
        
        cell = [[MyTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.detailLabel.hidden = YES;
        cell.verifyButton.hidden = YES;
        
        
    }
    
    cell.name.left = 12;
    cell.name.text = self.cellTitleList[indexPath.row];
    
    return cell;
    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
        [self.customDelegate didSelectRowAtIndexPath:indexPath];
    }
    
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
