//
//  ImageCache.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "FileCache.h"

@interface FileCache ()

@property (strong, nonatomic) NSCache *memCache;
@property (assign, nonatomic) BOOL shouldCacheImagesInMemory;

@end

@implementation FileCache

+(instancetype)defaultCache;
{
    static FileCache *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[FileCache alloc] init];
    });
    
    return _instance;
}

-(id)init
{
    self = [super init];
    if (self) {
        
        _shouldCacheImagesInMemory = YES;
        _memCache = [[NSCache alloc] init];
        _memCache.name = @"com.iyong.cache";
        
        NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        self.cachePath = [NSString stringWithFormat:@"%@/Cache/", cachesDirectory];
        // Create IO serial queue
        _ioQueue = dispatch_queue_create("com.iyong.cache", DISPATCH_QUEUE_SERIAL);
        
        NSDictionary* dict = [NSDictionary dictionaryWithContentsOfFile:[self cachePathForKey:@"Cache.plist"]];
        
        if([dict isKindOfClass:[NSDictionary class]]) {
            _cacheDictionary = [dict mutableCopy];
        } else {
            _cacheDictionary = [[NSMutableDictionary alloc] init];
        }
        
        [self cleanDisk];
        
//        //清除过期的缓存
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(cleanDisk)
//                                                     name:UIApplicationWillTerminateNotification
//                                                   object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(applicationDidBecomeActive:)
//                                                     name:UIApplicationDidBecomeActiveNotification
//                                                   object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(applicationWillResignActive:)
//                                                     name:UIApplicationDidBecomeActiveNotification
//                                                   object:nil];
    }
    return self;
}

-(NSString*)cachePathForKey:(NSString *)key
{
    NSString *pathName = self.cachePath;
    
    if (NO == [[NSFileManager defaultManager] fileExistsAtPath:pathName]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:pathName
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
    }
    return [pathName stringByAppendingString:key];
}

//- (BOOL)hasMemCached:(NSString*)key
//{
//    NSData *
//}

-(BOOL)hasCached:(NSString *)key
{
    NSDate* date = [_cacheDictionary objectForKey:key];
    if (!date) {
        return NO;
    }
    if ([date isKindOfClass:[NSNumber class]]) {
        if ([(NSNumber*)date integerValue] == kCacheAgeForever) {
            return YES;
        }
        else {
            return NO;
        }
    }
    else {
        if([[[NSDate date] earlierDate:date] isEqualToDate:date]) return NO;
        return [[NSFileManager defaultManager] fileExistsAtPath:[self cachePathForKey:key]];
    }
}

- (NSData *)dataForKey:(NSString *)key
{
    
    if (!key) {
        return nil;
    }

    // First check the in-memory cache...
    NSData *memData = [self.memCache objectForKey:key];
    if (!IsNilOrNull(memData)) {
        return memData;
    }
    
    if([self hasCached:key])
    {
        NSData *data = [NSData dataWithContentsOfFile:[self cachePathForKey:key]];
        [self.memCache setObject:data forKey:key];
        return data;
//        self.memCache setObject:
//        return [NSData dataWithContentsOfFile:[self cachePathForKey:key]
//                                      options:0
//                                        error:NULL];
    }
    else
    {
        return nil;
    }
}

- (void)saveData:(NSData *)data forKey:(NSString *)key
{
    [self saveData:data forKey:key cacheAge:kCacheAgeDefault];
}

- (void)saveData:(NSData *)data forKey:(NSString *)key cacheAge:(NSTimeInterval)age
{
    if (!data || !key)
    {
        return;
    }
    
    // if memory cache is enabled
    if (self.shouldCacheImagesInMemory) {
        [self.memCache setObject:data forKey:key cost:data.length];
    }
    
    // 异步函数 + 串行队列：会开启新的线程，但是任务是串行的，执行完一个任务，再执行下一个任务
    dispatch_async(_ioQueue, ^
                   {
                       NSFileManager *fileManager = NSFileManager.new;
                       
                       if (![fileManager fileExistsAtPath:self.cachePath])
                       {
                           [fileManager createDirectoryAtPath:self.cachePath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
                       }
                       
                       [fileManager createFileAtPath:[self cachePathForKey:key]
                                            contents:data
                                          attributes:nil];
                       
                       if (age < 0)
                       {
                           [_cacheDictionary setObject:@(age)
                                                forKey:key];
                       }
                       else
                       {
                           [_cacheDictionary setObject:[NSDate dateWithTimeIntervalSinceNow:age]
                                                forKey:key];
                       }
                       [_cacheDictionary writeToFile:[self cachePathForKey:@"Cache.plist"]
                                          atomically:YES];
                   });
}

//string
- (NSString *)stringForKey:(NSString *)key
{
    NSData *data = [self dataForKey:key];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (void)saveString:(NSString *)string forKey:(NSString *)key
{
    [self saveString:string forKey:key cacheAge:kCacheAgeDefault];
}

- (void)saveString:(NSString *)string forKey:(NSString *)key cacheAge:(NSTimeInterval)age
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    [self saveData:data forKey:key cacheAge:age];
}

//array
- (NSArray *)arrayForKey:(NSString *)key
{
    NSData *data = [self dataForKey:key];
    id array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if ([array isKindOfClass:[NSArray class]])
    {
        return array;
    }
    return nil;
}

- (void)saveArray:(NSArray *)array forKey:(NSString *)key
{
    [self saveArray:array forKey:key cacheAge:kCacheAgeDefault];
}

- (void)saveArray:(NSArray *)array forKey:(NSString *)key cacheAge:(NSTimeInterval)age
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:array];
    [self saveData:data forKey:key cacheAge:age];
}

- (void)removeDataForKey:(NSString *)key
{
    dispatch_async(_ioQueue, ^
                   {
                       [[NSFileManager defaultManager] removeItemAtPath:[self cachePathForKey:key] error:nil];
                   });
}

- (void)clearDisk
{
    dispatch_async(_ioQueue, ^
                   {
                       [_cacheDictionary removeAllObjects];
                       [[NSFileManager defaultManager] removeItemAtPath:self.cachePath error:nil];
                       [[NSFileManager defaultManager] createDirectoryAtPath:self.cachePath
                                                 withIntermediateDirectories:YES
                                                                  attributes:nil
                                                                       error:NULL];
                   });
}

- (void)cleanDisk
{
    dispatch_async(_ioQueue, ^
                   {
                       NSDictionary *cacheDic = [NSDictionary dictionaryWithDictionary:_cacheDictionary];
                       for(NSString* key in cacheDic)
                       {
                           id date = [cacheDic objectForKey:key];
                           if(([date isKindOfClass:[NSDate class]] && [[[NSDate date] earlierDate:date] isEqualToDate:date]) ||
                              ([date isKindOfClass:[NSNumber class]] && [date intValue] == kCacheAgeOneLifeCycle))
                           {
                               [[NSFileManager defaultManager] removeItemAtPath:[self cachePathForKey:key]
                                                                          error:NULL];
                               [_cacheDictionary removeObjectForKey:key];
                           }
                       }
                       [_cacheDictionary writeToFile:[self cachePathForKey:@"Cache.plist"]
                                          atomically:YES];
                   });
}

- (unsigned long long)cacheSize
{
    unsigned long long size = 0;
    NSDirectoryEnumerator *fileEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:self.cachePath];
    for (NSString *fileName in fileEnumerator)
    {
        NSString *filePath = [self.cachePath stringByAppendingPathComponent:fileName];
        NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        size += [attrs fileSize];
    }
    return size;
}



@end
