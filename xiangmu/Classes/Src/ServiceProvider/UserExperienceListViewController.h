//
//  UserExperienceListViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "PageUtil.h"
#import "UserExpericenViewModel.h"

@interface UserExperienceListViewController : BaseViewController

@property (nonatomic, strong) PageUtil *commonPage; // 分页处理

@end
