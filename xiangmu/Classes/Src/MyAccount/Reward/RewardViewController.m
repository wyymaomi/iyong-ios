//
//  BonusViewController.m
//  xiangmu
//
//  Created by David kim on 16/6/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RewardViewController.h"
#import "MyUtil.h"

@interface RewardViewController ()

@end

@implementation RewardViewController

- (void)viewDidLoad {
    self.view.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    [super viewDidLoad];
    [self createMyNav];
    [self createInterface];
}

-(void)createMyNav
{
    UILabel *titleLabel=[self addNavTitle:CGRectMake(0, 0, 335, 49) title:@"奖励"];
    titleLabel.textColor=[UIColor whiteColor];
    
    UIButton *backBtn=[self addNavBtn:CGRectMake(0, 0, 9, 15) title:nil target:self action:@selector(gotoBack) isLeft:YES];
    [backBtn setImage:[UIImage imageNamed:@"icon_back"] forState:UIControlStateNormal];
   
    self.navigationController.navigationBar.translucent=NO;
}
-(void)gotoBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)createInterface
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 10, ViewWidth, 150)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    
    UILabel *label = [MyUtil createLabelFrame:CGRectMake(20, 10, 80, 20) title:@"奖励等级" font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
    [view addSubview:label];
    
    UILabel *label1 = [MyUtil createLabelFrame:CGRectMake(0, 40, ViewWidth, 1) title:nil font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
    label1.backgroundColor = [UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
    [view addSubview:label1];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 50, ViewWidth-40, 90)];
    imageView.image = [UIImage imageNamed:@"奖励"];
    [view addSubview:imageView];
    
    
    UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 180, ViewWidth, 150)];
    view1.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view1];
    
    UILabel *label2 = [MyUtil createLabelFrame:CGRectMake(20, 10, 80, 20) title:@"奖励规制" font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
    [view1 addSubview:label2];
    
    UILabel *label3 = [MyUtil createLabelFrame:CGRectMake(0, 40, ViewWidth, 1) title:nil font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
    label3.backgroundColor = [UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
    [view1 addSubview:label3];
    
    UILabel *label4 = [MyUtil createLabelFrame:CGRectMake(10, 50, ViewWidth-20, 100) title:@"订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付订单支付" font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:0 textColor:[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f]];
    //自动折行
    label4.lineBreakMode = UILineBreakModeWordWrap;
    [view1 addSubview:label4];
    
    
}
@end
