//
//  BankcardTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BankcardTableViewCell.h"

@implementation BankcardTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"BankcardCellIdentifier";
    BankcardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
