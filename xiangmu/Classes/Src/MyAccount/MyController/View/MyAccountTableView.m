//
//  MyAccountTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyAccountTableView.h"
//#import "MyTableViewCell.h"
#import "MyAccountTableViewCell.h"

#define AccountCellHeight 55

@interface MyAccountTableView ()<UITableViewDelegate, UITableViewDataSource>



@end

@implementation MyAccountTableView

- (NSArray*)cellList
{
    if (!_cellList) {
        
        _cellList = @[@[/*@{@"title":@"信用额度", @"icon":@"icon_account_creditLine", @"vc":@"CreditLineViewController"},
                        @{@"title":@"信用余额", @"icon":@"icon_credit_amount",@"vc":@"BillPayViewController"},*/
                          /*@{@"title":@"金融服务", @"icon":@"icon_finance", @"vc":@"http://iyong-public.oss-cn-shanghai.aliyuncs.com/iyong/finance/finance.html"},*/
                        @{@"title":@"账户余额", @"icon" : @"icon_account_balance", @"vc":@"AccountBalanceViewController"},
                        @{@"title": @"奖      励", @"icon": @"icon_account_bonus",@"vc":@""},
                        @{@"title": @"交易记录", @"icon": @"icon_trade_record",@"vc":@"TradeRecordViewController"}],
                      @[@{@"title": @"银行卡", @"icon":@"icon_bankcard",@"vc":@"BankcardViewController"},
                        @{@"title": @"密码管理", @"icon":@"icon_pwd_manage",@"vc":@"PasswordManagerTableViewController"}]];
    }
    return _cellList;
}

- (void)setup {
    [super setup];
    
    self.delegate = self;
    self.dataSource = self;
    
    self.tableFooterView = [[UIView alloc] init];
    self.rowHeight = AccountCellHeight;
    
}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.cellList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *list = self.cellList[section];
    return list.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellId";
    
    MyAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    
    if (!cell) {
        
        cell = [[MyAccountTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
//        if (indexPath.section == 0) {
        
//            CGFloat iconImageWidth = 20;
//            CGFloat iconImageHeight = 14;
        
            CGFloat iconImageWidth = 22*Scale;
            CGFloat iconImageHeight = 22*Scale;
        
//            if (indexPath.row == MyAccountRowBonus) {
//                iconImageHeight = 20;
//            }
            UIImageView *iconImageView = [[UIImageView alloc]initWithFrame:CGRectMake(12*Scale,(tableView.rowHeight-iconImageHeight)/2 ,iconImageWidth,iconImageHeight)];
            iconImageView.tag=900;
            cell.name.left = iconImageView.right + 16*Scale;
            [cell.contentView addSubview:iconImageView];
        
//        UIImageView *disclosureImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ViewWidth-26, (AccountCellHeight-10.5*Scale)/2-5, 10.5*Scale, 21*Scale)];
//        disclosureImageView.image = [UIImage imageNamed:@"icon_disclosure2"];
//        [cell.contentView addSubview:disclosureImageView];
        
//        }
    }
    
    NSArray *list = self.cellList[indexPath.section];
    
    NSDictionary *dict = list[indexPath.row];
    
    cell.name.text = dict[@"title"];
    
    // 设置图标
    UIImageView *iconImageView = [cell viewWithTag:900];
    if (iconImageView != nil) {
        iconImageView.image = [UIImage imageNamed:dict[@"icon"]];
        
    }
    
    
    
    return cell;
    
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.accountDelegate && [self.accountDelegate respondsToSelector:@selector(didSelectRowAtIndexPath:)]) {
        [self.accountDelegate didSelectRowAtIndexPath:indexPath];
    }
    
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}





@end
