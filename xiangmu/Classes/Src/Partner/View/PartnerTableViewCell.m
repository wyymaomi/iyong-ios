//
//  PartnerTableViewCell.m
//  xiangmu
//
//  Created by David kim on 16/3/24.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PartnerTableViewCell.h"
#define ViewWidth [[UIScreen mainScreen]bounds].size.width

@implementation PartnerTableViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.frame = CGRectMake(0, 0, ViewWidth, 50);
        
//        self.disclosureImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-15, 18, 23/3, 42/3)];
//        self.disclosureImageView.image = [UIImage imageNamed:@"icon_disclosure"];
//        [self addSubview:self.disclosureImageView];
        
        self.title=[[UILabel alloc]initWithFrame:CGRectMake(30, 10, 150, 30)];
        self.title.textColor=[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
        self.title.font=[UIFont systemFontOfSize:14];
        [self addSubview:self.title];
        
        [self addSubview:self.badgeValueBtn];
        
        
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIButton *)badgeValueBtn
{
    if (!_badgeValueBtn)
    {
        _badgeValueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_badgeValueBtn setBackgroundImage:[UIImage imageNamed:@"icon_msg_circle"] forState:UIControlStateNormal];
        [_badgeValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _badgeValueBtn.alpha = 0;
        _badgeValueBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        CGFloat btnWidth = 22;
        CGFloat btnHeight = 22;
        _badgeValueBtn.frame = CGRectMake(self.frame.size.width- 15 -  btnWidth - 5, (self.frame.size.height - btnHeight)/2,  btnWidth, btnHeight);
        [_badgeValueBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        _badgeValueBtn.hidden = YES;
//        _badgeValueBtn.layer.zPosition = 1;
        [_badgeValueBtn setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [_badgeValueBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    }
    return _badgeValueBtn;
}

-(void)hideBadge
{
    self.badgeValueBtn.hidden = YES;
    self.badgeValueBtn.alpha = 0;
}

-(void)showBadgeValue:(NSString*)number
{
    self.badgeValueBtn.hidden = NO;
    self.badgeValueBtn.alpha = 1;
    if ([number isEqualToString:@"0"] || IsStrEmpty(number))
    {
        self.badgeValueBtn.alpha = 0;
        self.badgeValueBtn.hidden = NO;
        number = @"0";
        //        self.badgeValueBtn.hidden = YES;
        return;
    }
    
    NSUInteger length = [number length];
    
    CGSize size = [@"9" textSize:CGSizeMake(64, 20) font:FONT(12)];
//    CGSize size = [@"9" sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(64, 20)];
    
    CGRect rect = self.badgeValueBtn.frame;
    rect.size.width = size.width * (length + 0.5)>22?size.width*(length+0.5):22;
    
    self.badgeValueBtn.frame = rect;
    [self.badgeValueBtn setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.badgeValueBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    self.badgeValueBtn.alpha = 1.0;
    [self.badgeValueBtn setTitle:number forState:UIControlStateNormal];
    [self.badgeValueBtn setTitle:number forState:UIControlStateDisabled];
    //    }
}


@end
