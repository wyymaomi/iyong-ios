//
//  ForgetPaypasswordViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger, ForgetPaypwdRow) {
    ForgetPaypwdRowVerifyCode = 0,
    ForgetPaypwdRowNewPassword,
    ForgetPaypwdRowRenewPassword
};

@interface ForgetPaypasswordViewController : BaseViewController

@end
