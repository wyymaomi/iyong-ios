//
//  PathInfoTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/20.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WayBillResponseModel.h"
#import "CustomComboBoxView.h"

@protocol WayBillTableViewCellDelegate

-(void)doGetArriveLocation;
-(void)doGetEndTime;
-(void)doGetDistance;
-(void)doGetRemark;

@end


@interface WayBillTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *editArriveLocationView;
@property (weak, nonatomic) IBOutlet UITextField *editArriveLocationTextField;
@property (weak, nonatomic) IBOutlet UIView *editEndTimeView;
@property (weak, nonatomic) IBOutlet UITextField *editEndTimeTextField;
@property (weak, nonatomic) IBOutlet UIView *editDistanceView;
@property (weak, nonatomic) IBOutlet UITextField *editDistanceTextField;
@property (weak, nonatomic) IBOutlet UIView *editRemarkView;
@property (weak, nonatomic) IBOutlet UITextField *editRemarkTextField;


@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UILabel *arriveLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;


@property (weak, nonatomic) id<WayBillTableViewCellDelegate> delegate;

+ (instancetype)cellWithTableView:(UITableView *)tableView wayBillModel:(WayBillResponseModel*)wayBillModel;
-(void)initData:(WayBillResponseModel*)model;

@end
