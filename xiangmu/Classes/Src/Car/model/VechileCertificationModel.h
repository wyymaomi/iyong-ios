//
//  VechileCertificationModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@interface VechileCertificationModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *carId;
@property (nonatomic, strong) NSString<Optional> *driverLicenseExpiryDate;
@property (nonatomic, strong) NSString<Optional> *driverLicenseNumber;
@property (nonatomic, strong) NSString<Optional> *driverLicenseReceiveDate;
@property (nonatomic, strong) NSString<Optional> *driverLicenseUrl;
@property (nonatomic, strong) NSString<Optional> *drivingLicenseExpiry;
@property (nonatomic, strong) NSString<Optional> *drivingLicenseReceiveDate;
@property (nonatomic, strong) NSString<Optional> *drivingLicenseUrl;
@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *idBackUrl;
@property (nonatomic, strong) NSString<Optional> *idExpiryDate;
@property (nonatomic, strong) NSString<Optional> *idFrontUrl;
@property (nonatomic, strong) NSString<Optional> *idNumber;
@property (nonatomic, strong) NSNumber<Optional> *idType;
@property (nonatomic, strong) NSString<Optional> *status;

@end
