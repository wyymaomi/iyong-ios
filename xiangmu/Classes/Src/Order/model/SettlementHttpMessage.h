//
//  SettlementHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/5.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface SettlementHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *price;

@end
