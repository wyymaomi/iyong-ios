//
//  ShareUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UShareUI/UShareUI.h>

@interface ShareUtil : NSObject



+(void)startShare:(NSString*)url title:(NSString*)title desc:(NSString*)desc icon:(UIImage*)icon webpageUrl:(NSString*)webpageUrl;

+ (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType title:(NSString*)title desc:(NSString*)desc icon:(UIImage*)icon webpageUrl:(NSString*)webpageUrl;

+(void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
                            title:(NSString *)title
                             desc:(NSString *)desc
                             icon:(UIImage *)icon
                       webpageUrl:(NSString *)webpageUrl
                     shareSuccess:(void(^)(id data))shareSuccess
                     shareFailure:(void(^)(id data, NSError *error))shareFailure;

@end
