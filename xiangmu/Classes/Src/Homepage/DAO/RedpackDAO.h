//
//  RedpackDAO.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "DAO.h"

@interface RedpackDAO : DAO

//- (NSMutableArray*)getAllCar:(NSString*)companyId;// 获取对应用户名下的车辆列表
//- (NSMutableArray*)getCertifiedCarList:(NSString*)companyId; // 获取已认证车辆列表
//- (BOOL)updateCarList:(NSArray*)carList;// 更新车辆信息
//- (BOOL)deleteCarById:(NSString*)carId;

-(NSString*)getRedpackMoney;

-(NSString*)getTodayRedpackMoney;

-(BOOL)isExistRedpack:(NSInteger)flag;

-(BOOL)isExistTodayRedpack:(NSInteger)flag;

-(BOOL)updateRedpackStatus:(NSInteger)flag;

//-(BOOL)updateRedpackStatu:(NSInteger)flag;

//-(BOOL)updateRedpack:(NSString*)amount time:(NSString*)time;

-(BOOL)insertRedpack:(NSString*)amount flag:(NSInteger)flag;

@end
