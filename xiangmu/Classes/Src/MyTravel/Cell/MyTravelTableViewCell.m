//
//  MyTravelTableViewCell.m
//  iYONGDriver
//
//  Created by 湛思科技 on 2017/6/21.
//  Copyright © 2017年 Shanghai Zhansi Tech. All rights reserved.
//

#import "MyTravelTableViewCell.h"

@implementation MyTravelTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"MyTravelTableViewCell";
    MyTravelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    return cell;
}

-(void)setTravelModel:(TravelModel *)travelModel
{
    if (!travelModel) {
        return;
    }
    
    _travelModel = travelModel;
    
    self.startLocation.text = travelModel.startLocation;
    self.endLocation.text = travelModel.endLocation;
    
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(long)([travelModel.startTime doubleValue]/1000)];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:(long)([travelModel.endTime doubleValue]/1000)];
    
    self.dateTimeLabel.text = [startDate stringWithDateFormat:@"MM月dd日"];
    self.startTimeLabel.text = [startDate stringWithDateFormat:@"HH:mm"];
    self.endTimeLabel.text = [endDate stringWithDateFormat:@"HH:mm"];
    
    self.timeLabel.text = [NSString stringWithFormat:@"耗时：%@", [endDate timeLeft:startDate]];
    
//    double distance = [travelModel.travel doubleValue]/1000;
//    self.distanceLabel.text = [NSString stringWithFormat:@"共计：%.3f公里", distance];
    self.distanceLabel.text = [NSString stringWithFormat:@"共计：%@公里", [travelModel.mileage stringValue]];
    
}

//-(TraceModel*)setTrace;
//-(void)setTraceModel:(TraceModel *)traceModel
//{
//    if (!traceModel) {
//        return;
//    }
//    
////    self.dateTimeLabel =
//    
//    _traceModel = traceModel;
//    
//    self.startLocation.text = traceModel.startLocation;
//    self.endLocation.text = traceModel.endLocation;
//    
//    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:[traceModel.startTimestamp doubleValue]];
//    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:[traceModel.endTimestamp doubleValue]];
//    
//    self.dateTimeLabel.text = [startDate stringWithDateFormat:@"MM月dd日"];
//    self.startTimeLabel.text = [startDate stringWithDateFormat:@"HH:mm"];
//    self.endTimeLabel.text = [endDate stringWithDateFormat:@"HH:mm"];
//    
//    self.timeLabel.text = [NSString stringWithFormat:@"耗时：%@", [endDate timeLeft:startDate]];
//    
//    
//    
//    double distance = [traceModel.distance doubleValue]/1000;
//    self.distanceLabel.text = [NSString stringWithFormat:@"共计：%.3f公里", distance];
//}

+(CGFloat)getCellHeight
{
    return 160;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
