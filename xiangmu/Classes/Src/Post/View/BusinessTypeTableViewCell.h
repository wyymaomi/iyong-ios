//
//  BusinessTypeTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/6.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"

@interface BusinessTypeTableViewCell : UITableViewCell

//+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@property (nonatomic, strong) HeaderView *headerView;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) NSMutableArray *businessTypeArray;

//@property (nonatomic, strong) NSMutableArray *imageArray;
//@property (nonatomic, strong) NSMutableArray *typeNameArray;

@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, strong) NSMutableArray *labelArray;

//@property (weak, nonatomic) UIImageView *commercialImageView;
//@property (weak, nonatomic) UIImageView *travelImageView;
//@property (weak, nonatomic) UIImageView *airportImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *weddingImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *charteredImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *truckImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *busImageView;
//@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;

//- (void)onClickImageView:(id)sender;

@end
