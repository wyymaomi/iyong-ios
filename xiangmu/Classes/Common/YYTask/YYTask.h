//
//  YYTask.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/16.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YYTask;

#undef YYTaskCancel
#define YYTaskCancel(_task) ({ if ([(_task) isExecuting]) { [(_task) cancel];} })

/**
 *  YYTask 的状态枚举
 */
typedef NS_ENUM(NSUInteger, YYTaskState)
{
    /**
     *  准备完成的状态
     */
    YYTaskStatusReady,
    /**
     *  task正在运行中
     */
    YYTaskStatusExecuting,
    /**
     *  task执行成功
     */
    YYTaskStatusSuccess,
    /**
     *  task执行失败
     */
    YYTaskStateFailed,
    /**
     *  task被取消
     */
    YYTaskStateCanceled
};

/**
 *  YYTask类，在executeOperation方法中实现任务即可，自动管理runLoop和operation的状态
 */
@interface YYTask : NSOperation

@property (readonly) YYTaskState state;
@property (readonly) BOOL        isSuccessed;
@property (readonly) BOOL        isExecutingOrSuccessed;


@end
