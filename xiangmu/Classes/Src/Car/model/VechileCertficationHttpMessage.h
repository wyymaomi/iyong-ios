//
//  VechileCertficationHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface VechileCertficationHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString *carId;  // 车辆ID

//@property (nonatomic, strong) NSString *driverLicenseNumber;
//@property (nonatomic, strong) NSString *driverLicenseReceiveDate;
//@property (nonatomic, strong) NSString *driverLicenseExpiryDate;
@property (nonatomic, strong) NSString *driverLicenseContent;

//@property (nonatomic, strong) NSString *drivingLicenseReceiveDate;  // 领取行驶证日期
//@property (nonatomic, strong) NSString *drivingLicenseExpiry;       // 行驶证有效日期
@property (nonatomic, strong) NSString *drivingLicenseContent;      // 行驶证文件内容

@property (nonatomic, strong) NSNumber *idType;             // 身份证件类型
//@property (nonatomic, strong) NSString *idNumber;           // 身份证件号
//@property (nonatomic, strong) NSString *idExpiryDate;     // 身份证件有效期
@property (nonatomic, strong) NSString *idFrontContent;     // 身份证件正面图片内容
@property (nonatomic, strong) NSString *idBackContent;      // 身份证件背面图片内容

- (NSString*)getIdTypeString;

- (NSString*)getErrorMsg;


@end
