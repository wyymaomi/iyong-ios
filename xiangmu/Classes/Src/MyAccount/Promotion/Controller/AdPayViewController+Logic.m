//
//  AdPayViewController+Logic.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdPayViewController+Logic.h"
#import "AlipayOrder.h"
#import "SDKManager.h"
#import "SDKRegisterService.h"
#import "SDKPayService.h"
#import "WXApiObject.h"
#import "WXApi.h"
#import "BaseViewController+PaypwdCheck.h"
//#import "WXApiRequestHandler.h"

@implementation AdPayViewController (Logic)

- (void)doAdPurchase
{

    if (IsStrEmpty(self.adOrderId)) {
        if ([self.httpMessage.payMode integerValue] == AdPayTypeMyAccount ||
            [self.httpMessage.payMode integerValue] == AdPayTypePromotionAccount) {
            [self checkHasPayPwd];
        }
        else {
            [self submitPurchaseReqeust:self.httpMessage];
        }
    }
    else {
        if ([self.httpMessage.payMode integerValue] == AdPayTypeMyAccount ||
            [self.httpMessage.payMode integerValue] == AdPayTypePromotionAccount) {
            [self checkHasPayPwd];
        }
        else {
            [self submitPurchaseReqeust:self.httpMessage];
//            [self submitPay:self.adOrderId];
        }
        
    }
}

#pragma mark - Pay Delegate method
-(void)onPaySuccess;
{
    if (IsStrEmpty(self.adOrderId)) {
        [self submitPurchaseReqeust:self.httpMessage];
    }
    else {
        [self submitPay:self.adOrderId];
    }

}

-(void)getFinanceInfo
{
    self.nextAction = @selector(getFinanceInfo);
    self.object1 = nil;
    self.object2 = nil;
    
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_FINANCE_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            strongSelf.myAccountMoney = data[@"accountAmount"];
            strongSelf.promotionAccountMoney = data[@"extensionAmount"];
            [strongSelf.groupTableView reloadData];
//            strongSelf.myAccountTableView.creditLine = data[@"creditLine"];
//            strongSelf.myAccountTableView.creditAmount = data[@"creditAmount"];
//            strongSelf.myAccountTableView.accountAmount = data[@"accountAmount"];
        }
        else {
//            strongSelf.myAccountTableView.creditLine = nil;
//            strongSelf.myAccountTableView.creditAmount = nil;
//            strongSelf.myAccountTableView.accountAmount = nil;
        }
//        [strongSelf.myAccountTableView reloadData];
        
    } andFailure:^(NSString *errorDesc) {
        [weakSelf resetAction];
        DLog(@"response failure");
    }];
    
}

- (void)submitPay:(NSString*)adOrderId
{
    NSString *params = [NSString stringWithFormat:@"%@&orderId=%@", self.httpMessage.description, adOrderId];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_PAY_ORDER_API) requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            
            if (strongSelf.httpMessage.payMode == [NSNumber numberWithInteger:AdPayTypeAlipay]) {
                NSString *data = responseData[@"data"];
                if (!IsStrEmpty(data)) {
                    [strongSelf doAlipayPay:data];
                }
                
            }
            else if (strongSelf.httpMessage.payMode == [NSNumber numberWithInteger:AdPayTypeWechat]) {
                
                NSDictionary *data = responseData[@"data"];
                if (data != nil) {
                    
                    [strongSelf bizPay:data];
                }
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"支付成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        [self gotoPage:@"ProviderInfoViewController"];
                    }
                    
                }];
            }
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}


//adMainIds（广告ID数组，可以以英文逗号隔开）
//price（总价）
//payMode（支付方式：1余额支付；2推广余额支付；3支付宝支付；4微信支付）

- (void)submitPurchaseReqeust:(AdPurchaseHttpMessage*)httpMessage
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, AD_PURCHASE_API];
    NSString *params = httpMessage.description;
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary * responseData) {
        StrongSelf
        
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            
                if (httpMessage.payMode == [NSNumber numberWithInteger:AdPayTypeAlipay]) {
                    id data = responseData[@"data"];
                    if ([data isKindOfClass:[NSString class]]) {
                        NSString *dataString = (NSString*)data;
                        if (!IsStrEmpty(dataString)) {
                            [strongSelf doAlipayPay:dataString];
                        }
                    }
                    else {
                        [MsgToolBox showToast:@"支付失败"];
                    }
                }
                else if (httpMessage.payMode == [NSNumber numberWithInteger:AdPayTypeWechat]) {
                    id data = responseData[@"data"];
                    if ([data isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dict = responseData[@"data"];
                        if (dict != nil) {
                            [strongSelf bizPay:dict];
                        }
                    }
                    else {
                        [MsgToolBox showToast:@"支付失败"];
                    }
                }
                else {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"支付成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                       
                        if (buttonIndex == 0) {
                            [self gotoPage:@"ProviderInfoViewController"];
//                            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
                        }
                        
                    }];
                }
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

//
//选中商品调用支付宝极简支付
//

#pragma mark - 支付宝充值/支付

- (void)doAlipayPay:(NSString*)encryptOrderString
{
    
//    NSDictionary *dict = @{kTradeNo:@"1234567890",//[self generateTradeNO],
//                           kRechargeAmount:self.transAmountTextField.text.trim,
//                           kPayTime:[[NSDate date] stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"]};
//    id<PayModelAdapterProtocol> alipay = [[AlipayOrder alloc] init];
    
    [[SDKManager getPayService:SDKPlatformAlipay] payOrder:/*[alipay generateOrderString:dict]*/encryptOrderString callBack:^(NSString *signString, NSError *error) {
        DLog(@"signString = %@", signString);
//        NSInteger resultStatus = [signString integerValue];
//        if (resultStatus == 6001) {
//            [MsgToolBox showAlert:@"" content:@"用户取消"];
////                        [self showTipMessage:@"用户取消"];
//            //            [self ]
//        }
//        else if (resultStatus == 6002){
//            [MsgToolBox showAlert:@"" content:@"网络连接错误"];
////                        [self showTipMessage:@"网络连接错误"];
//        }
//        else if (resultStatus == 4000){
//            [MsgToolBox showAlert:@"" content:@"订单支付失败"];
//            //            [self showTipMessage:@"订单支付失败"];
//        }
//        else if (resultStatus == 8000){
//            [MsgToolBox showAlert:@"" content:@"正在处理中"];
//            //            [self showTipMessage:@"正在处理中"];
//        }
//        else if (resultStatus == 9000){
//            [MsgToolBox showAlert:@"" content:@"订单支付成功"];
//            //            [self showTipMessage:@"订单支付成功"];
//        }
        
    }];
    
}

#pragma mark - 微信支付

- (void)bizPay:(NSDictionary*)dict
{
//    NSString *res = [WXApiRequestHandler jumpToBizPay];
//    if( ![@"" isEqual:res] ){
//        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"支付失败" message:res delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        
//        [alter show];
////        [alter release];
//    }
    
    NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
    
    //调起微信支付
    PayReq *req             = [[PayReq alloc] init];
    req.partnerId           = dict[@"partnerid"];
    req.prepayId            = [dict objectForKey:@"prepayid"];
    req.nonceStr            = [dict objectForKey:@"noncestr"];
    req.timeStamp           = stamp.intValue;
    req.package             = [dict objectForKey:@"package"];
    req.sign                = [dict objectForKey:@"sign"];
    [WXApi sendReq:req];
    
}


@end
