//
//  InvoiceSettingEditViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/30.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "InvoiceinfoHttpMessage.h"

typedef NS_ENUM(NSUInteger, InvoiceEditMode)
{
    InvoiceEditModeAddNew,
    InvoiceEditModeUpdate
};

@interface InvoiceSettingEditViewController : BaseViewController

@property (nonatomic, assign) NSUInteger editMode;// 编辑模式

@property (nonatomic, strong) InvoiceSettingHttpMessage *invoiceSettingHttpMessage;

@end
