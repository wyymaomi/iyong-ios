//
//  BindBnkcardViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BindBankcardViewController.h"
#import "BankcardViewController.h"

@interface BindBankcardViewController ()<UITextFieldDelegate>

@end

@implementation BindBankcardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"绑定银行卡";
    [self.okButton blueStyle];
    
    self.bankcardNameTextField.text = self.bankcardName;
//    self.bankcardNameTextField.text = [[UserManager sharedInstance].userModel realName];
    self.bankcardNoTextField.delegate = self;
    
}

#pragma makr - 绑定银行卡

- (IBAction)onOKButtonClick:(id)sender {
    
    [self.bankcardNoTextField resignFirstResponder];
    
    NSString *bankcardStr = [self.bankcardNoTextField.text replaceAll:@" " with:@""].trim;
    
    NSString *errorMsg;
    if (![UserHelper validateBankcardNo:bankcardStr error:&errorMsg]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:errorMsg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        WeakSelf
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                StrongSelf
                strongSelf.bankcardNoTextField.text = @"";
                [strongSelf.bankcardNoTextField becomeFirstResponder];
            }
        }];
        return;
    }
    
    self.nextAction = @selector(onOKButtonClick:);
    self.object1 = sender;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ADD_BANKCARD_API];
    NSString *params = [NSString stringWithFormat:@"cardNumber=%@", bankcardStr];
    
    [self showHUDIndicatorViewAtCenter:@"正在绑定银行卡，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"添加银行卡成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    
                    for (UIViewController *viewController in strongSelf.navigationController.viewControllers) {
                        
                        if ([viewController isKindOfClass:[BankcardViewController class]]) {
                            
                            [strongSelf.navigationController popToViewController:viewController animated:YES];
                            
                        }
                        
                    }
                }
                
            }];
        }
        else if (code_status ==STATUS_FAIL) {
            NSString *msg = responseData[@"data"];
            if (IsStrEmpty(msg)) {
                msg = @"银行卡添加失败，请重新输入";
            }
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    strongSelf.bankcardNoTextField.text = @"";
                    [strongSelf.bankcardNoTextField becomeFirstResponder];
                }
                
            }];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.bankcardNoTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegate method
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [textField text];
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
        return NO;
    }
    
    text = [text stringByReplacingCharactersInRange:range withString:string];
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *newString = @"";
    while (text.length > 0) {
        NSString *subString = [text substringToIndex:MIN(text.length, 4)];
        newString = [newString stringByAppendingString:subString];
        if (subString.length == 4) {
            newString = [newString stringByAppendingString:@" "];
        }
        text = [text substringFromIndex:MIN(text.length, 4)];
    }
    
    newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
    
    if (newString.length >= 24) {
        return NO;
    }
    
    [textField setText:newString];
    
    return NO;
}

-(void)pressedBackButton
{
    WeakSelf
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"是否取消绑定银行卡？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
       
        if (buttonIndex == 1) {
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }
        
    }];
}


@end
