//
//  AddressTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressTableViewCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;

//@property (nonatomic,)

+(CGFloat)getCellHeight;

@end
