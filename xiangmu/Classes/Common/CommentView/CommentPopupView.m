//
//  CommentView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/28.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CommentPopupView.h"
#import "UIPlaceHolderTextView.h"

#define DialogViewHeight  190*Scale//ViewHeight*0.3

@interface CommentPopupView ()

@property (nonatomic, strong) UIWindow      *maskWindow;
@property (nonatomic, strong) UIView        *maskView;
@property (nonatomic, strong) UIView        *dialogView;
@property (nonatomic, strong) UIPlaceHolderTextView    *commentView;

@end

@implementation CommentPopupView

- (id)init{
    
    self = [super init];
    
    if (self) {
        
        self.userInteractionEnabled = YES;
        
        //        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor clearColor];
        
//        [self initViews];
        
    }
    
    return self;
}



//- (void)initViews
//{
//    [self addSubview:self.maskWindow];
//}

- (UIWindow *)maskWindow
{
    if (!_maskWindow) {
        
        _maskWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        //edited by gjf 修改alertview leavel
//        _maskWindow.windowLevel = UIWindowLevelStatusBar + BBAlertLeavel;
        _maskWindow.backgroundColor = [UIColor clearColor];
//        _maskWindow.hidden = YES;
        
        UIView *maskView = ({
            
            UIView *view = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
            view.backgroundColor = [UIColor blackColor];
            view.alpha = 0.7;
            view;
        });
        [_maskWindow addSubview:maskView];
        _maskView = maskView;
        
        _dialogView=[[UIView alloc] init];
        _dialogView.frame=CGRectMake(0, ViewHeight, ViewWidth, DialogViewHeight);
        _dialogView.backgroundColor = UIColorFromRGB(0xEEEEEE);
        [_maskWindow addSubview:_dialogView];

        
        _commentView=[[UIPlaceHolderTextView alloc] init];
        _commentView.frame=CGRectMake(22, 20, ViewWidth-44, DialogViewHeight-55);
        _commentView.backgroundColor=[UIColor whiteColor];
//        _commentView.center=CGPointMake(ViewWidth/2, DialogViewHeight/2);
        _commentView.cornerRadius = 5;
        _commentView.borderWidth = 0.5;
        _commentView.borderColor = UIColorFromRGB(0x9a9a9a);
        [_dialogView addSubview:_commentView];
        
        
        UIButton * confirmButton=[[UIButton alloc] initWithFrame:CGRectMake(ViewWidth-45-10, DialogViewHeight-30, 45, 25)];
        [confirmButton setTitle:@"发送" forState:UIControlStateNormal];
//        confirmButton.center=CGPointMake(ViewWidth- 28, DialogViewHeight-2);
        confirmButton.right = _commentView.right;
        confirmButton.titleLabel.font = FONT(11);
        confirmButton.backgroundColor = NAVBAR_BGCOLOR;
//        confirmButton.backgroundColor = UIColorFromRGB(0x5d9cec);//UIColorFromRGB(0xCCCCCC);
        confirmButton.cornerRadius = 5;
        confirmButton.borderWidth=0.5;
        confirmButton.borderColor = UIColorFromRGB(0x333333);
        [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [confirmButton addTarget:self action:@selector(confrimClick) forControlEvents:UIControlEventTouchUpInside];
        [_dialogView addSubview:confirmButton];
        
        
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fingerTapped)];
        [maskView addGestureRecognizer:singleTap];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        
    }
    return _maskWindow;
}

-(void)addComment
{
    
    [self.maskWindow makeKeyAndVisible];
    [_commentView becomeFirstResponder];
    _commentView.text = @"";
    
}


-(void)confrimClick
{
    NSLog(@"确定按钮被点击 评论内容%@",_commentView.text);
    
    NSString *text = _commentView.text.trim;
    if (text.length == 0) {
        return;
    }
    
    [self fingerTapped];
    if (_delegate && [_delegate respondsToSelector:@selector(onGetText:)]) {
        [_delegate onGetText:text];
    }
    
}


-(void)cancleClick
{
    
//    [_blackView removeFromSuperview];
//    [_mainView removeFromSuperview];
    [_commentView resignFirstResponder];
    _dialogView.frame=CGRectMake(0, ViewHeight, ViewWidth, DialogViewHeight);
    
    
    
}

-(void)fingerTapped
{
    
//    [_blackView removeFromSuperview];
//    [_mainView removeFromSuperview];
    [_commentView resignFirstResponder];
    _dialogView.frame=CGRectMake(0, ViewHeight, ViewWidth, DialogViewHeight);
    self.maskWindow.hidden = YES;
}

- (void)removeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification{
    
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSLog(@"keyboadr  %@",NSStringFromCGRect(keyboardBounds));
    int  keyBoardHeight=keyboardBounds.size.height;
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    _dialogView.center=CGPointMake(ViewWidth/2, ViewHeight-keyBoardHeight-DialogViewHeight/2);
    NSLog(@"dialogView  frame %@",NSStringFromCGRect(_dialogView.frame));
    NSLog(@"comment  frame %@",NSStringFromCGRect(_commentView.frame));
    [UIView commitAnimations];
    
    
    
    
}




@end
