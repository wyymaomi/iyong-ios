//
//  CustomCalendarCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/13.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "FSCalendarCell.h"

@interface CustomCalendarCell : FSCalendarCell

@property (nonatomic, strong) UILabel *badgeLabel;

@end
