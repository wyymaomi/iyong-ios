//
//  HomepageCollectionView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, HomeCollectionViewType)
{
    HomeCollectionViewTypeRecommend = 0,
    HomeCollectionViewTypeServicer,
    HomeCollectionViewTypeExperience,
    HomeCollectionViewTypeBeijing,
    HomeCollectionViewTypeShanghai,
    HomeCollectionViewTypeHangzhou,
    HomeCollectionViewTypeShenzhen
};

@protocol HomepageCollectionViewDelegate <NSObject>

- (void)onClickItem:(NSInteger)row tag:(NSInteger)tag;

@end

@interface HomepageCollectionView : UICollectionView<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, strong) NSArray *dataList;

@property (nonatomic, weak) id<HomepageCollectionViewDelegate> customDelegate;

@end
