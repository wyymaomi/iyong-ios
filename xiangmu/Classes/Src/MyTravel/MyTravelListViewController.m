//
//  MyTravelListViewController.m
//  iYONGDriver
//
//  Created by 湛思科技 on 2017/6/27.
//  Copyright © 2017年 Shanghai Zhansi Tech. All rights reserved.
//

#import "MyTravelListViewController.h"
#import "MyTravelTableViewCell.h"
//#import "TraceModel.h"
//#import "TraceDAO.h"
#import "DrvierPathViewController.h"
#import "MyTravelListViewController+NetworkRequest.h"
#import "BaiduTraceSDK/BaiduTraceSDK.h"
#import "VechileLocationVIewCOntroller.h"

@interface MyTravelListViewController ()<PullRefreshTableViewDelegate,BTKTrackDelegate>
{
//    BMKMapView *_mapView;
//    BMKPointAnnotation *_annotation;
//    LocationAnnotationView *_locationAnnotationView;
//    BMKLocationService *_locService;
//    BMKGeoCodeSearch *_geocodeSearch;
//    CLLocationCoordinate2D _currentCoordinate ;
    
}

@property (nonatomic, assign) NSUInteger pageIndex;
@property (nonatomic, assign) NSUInteger totalPages;

//@property (nonatomic, strong) TraceModel *curTraceModel;
@property (nonatomic, strong) TravelModel *curTravelModel;

@end

@implementation MyTravelListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的行程";
    
//    TraceDAO *dao = [[TraceDAO alloc] init];
//    self.dataList = [dao getAllTrace:self.entityName];
    
    // 使用SDK的任何功能前，都需要先调用initInfo:方法设置基础信息。
    BTKServiceOption *sop = [[BTKServiceOption alloc] initWithAK:AK mcode:mcode serviceID:serviceID keepAlive:1];
    [[BTKAction sharedInstance] initInfo:sop];
//    [self startService];
    
    [self.view addSubview:self.pullRefreshTableView];
    self.pullRefreshTableView.pullRefreshDelegate = self;
    

    [self.pullRefreshTableView beginHeaderRefresh];
    
}

//-(void)startService
//{
//    if ([self needLogin]) {
//        return;
//    }
//    
//    //    self.entityName = [UserManager sharedInstance].userModel.id;
//    //    self.entityName = [StringUtil createUDID];
//    //    DLog(@"self.entityName = %@", self.entityName);
//    self.entityName = self.driverId;
//    
//    BTKStartServiceOption *op = [[BTKStartServiceOption alloc] initWithEntityName:self.entityName];
//    [[BTKAction sharedInstance] startService:op delegate:self];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    
}



#pragma mark - PullRefreshTableViewDelegate method

-(void)onHeaderRefresh;
{
    self.nextAction = @selector(onHeaderRefresh);
    
    if ([self needLogin]) {
        return;
    }
    
    [self fetchTraveList:nil driverId:self.driverId];
}

-(void)onFooterRefresh;
{
    self.nextAction = @selector(onFooterRefresh);
    
    if ([self needLogin]) {
        return;
    }
    
    TravelModel *model = [self.dataList lastObject];
    [self fetchTraveList:[model.startTime stringValue] driverId:self.driverId];
    
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [MyTravelTableViewCell getCellHeight];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTravelTableViewCell *cell = [MyTravelTableViewCell cellWithTableView:tableView indexPath:indexPath];
//    cell.traceModel =
//    TraceModel *model = self.dataList[indexPath.row];
//    cell.traceModel = model;
    TravelModel *model = self.dataList[indexPath.row];
    cell.travelModel = model;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    TraceModel *model = self.dataList[indexPath.row];
    TravelModel *model = self.dataList[indexPath.row];
    
//    VechileLocationViewController *viewController = [VechileLocationViewController new];
//    viewController.entityName = model.id;
//    [self.navigationController pushViewController:viewController animated:YES];
    
    self.pageIndex = 1;
//    _curTraceModel = model;
    _curTravelModel = model;
    [self.trackResultList removeAllObjects];
    [self showHUDIndicatorViewAtCenter:@"正在查询，请稍候..."];
    NSUInteger startTime = ([_curTravelModel.startTime doubleValue])/1000;
    NSUInteger endTime = ([_curTravelModel.endTime doubleValue])/1000;
    [self queryTrackHistory:startTime endTimeStamp:endTime pageIndex:self.pageIndex];
    
    
}
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{

#pragma mark - 查询轨迹

//-(void)queryTest
//{
//    _isEnd = YES;
//    NSUInteger startTimeStamp = 1498134720;
//    NSUInteger endTimeStamp = 1498141290;
//    [self queryTrackHistory:startTimeStamp endTimeStamp:endTimeStamp];
//}

-(void)queryTrackHistory:(long)startTimeStamp endTimeStamp:(long)endTimeStamp pageIndex:(NSUInteger)pageIndex
{
    BTKQueryTrackProcessOption *option = [[BTKQueryTrackProcessOption alloc] init];
    option.denoise = 1;
    option.vacuate = 1;
    option.mapMatch = 1;
    option.radiusThreshold = 100;
    
    
    BTKQueryHistoryTrackRequest *request = [[BTKQueryHistoryTrackRequest alloc] initWithEntityName:self.driverId startTime:startTimeStamp endTime:endTimeStamp isProcessed:YES processOption:option supplementMode:BTK_TRACK_PROCESS_OPTION_SUPPLEMENT_MODE_DRIVING outputCoordType:BTK_COORDTYPE_BD09LL sortType:BTK_TRACK_SORT_TYPE_ASC pageIndex:pageIndex pageSize:TrackPageSize serviceID:serviceID tag:12];
    [[BTKTrackAction sharedInstance] queryHistoryTrackWith:request delegate:self];
}

-(void)onQueryHistoryTrack:(NSData *)response {
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"track history response: %@", dict);
    
    
    // track history test
    if ([dict[@"status"] integerValue] == 0) {
        if ([dict[@"tag"] integerValue] == 12) {
            
            NSUInteger total = [dict[@"total"] integerValue];
//            NSUInteger size = [dict[@"size"] integerValue];
            
            if (total == 0) {
                dispatch_main_sync_safe(^{
                    [self hideHUDIndicatorViewAtCenter];
                    [MsgToolBox showToast:@"没有数据"];
                })

                return;
            }
            
            self.totalPages = total/TrackPageSize;
            if (total % TrackPageSize != 0) {
                self.totalPages++;
            }
            
            if (self.pageIndex < self.totalPages) {
                
                NSArray *points = dict[@"points"];
                if (points.count > 0) {
                    [self.trackResultList addObjectsFromArray:points];
                }
                
                self.pageIndex++;

                
                NSUInteger startTime = ([_curTravelModel.startTime doubleValue])/1000;
                NSUInteger endTime = ([_curTravelModel.endTime doubleValue])/1000;
                [self queryTrackHistory:startTime endTimeStamp:endTime pageIndex:self.pageIndex];
                
            }
            else {
                
                // 进入轨迹
                
                NSArray *points = dict[@"points"];
                
                if (points.count > 0) {
                    [self.trackResultList addObjectsFromArray:points];
                }
                
                if (self.trackResultList.count > 0 && self.pageIndex == self.totalPages) {
                    
                    WeakSelf
                    dispatch_main_sync_safe(^{
                        
                        [weakSelf hideHUDIndicatorViewAtCenter];
                        
                        DrvierPathViewController *viewController = [[DrvierPathViewController alloc] init];
                        viewController.trackHistoryList = weakSelf.trackResultList;
                        [weakSelf.navigationController pushViewController:viewController animated:YES];
                        
                    });
                    
                }
                
            }
        }
    }
    else {
        [self hideHUDIndicatorViewAtCenter];
        [MsgToolBox showToast:dict[@"message"]];
    }
    
}

-(NSMutableArray*)trackResultList
{
    if (!_trackResultList) {
        _trackResultList = [NSMutableArray new];
    }
    return _trackResultList;
}





@end
