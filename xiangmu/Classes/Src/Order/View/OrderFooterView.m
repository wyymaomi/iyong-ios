//
//  OrderFooterView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderFooterView.h"

@implementation OrderFooterView

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorFromRGB(0xF7F7F7);
        [self addSubview:self.rechargeButton];
        [self addSubview:self.okButton];
    }
    return self;
}

- (UIButton*)rechargeButton
{
    if (_rechargeButton == nil) {
        _rechargeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rechargeButton.frame = CGRectMake(10, 10, ViewWidth/2-20, 40);
        [_rechargeButton setTitle:@"充值" forState:UIControlStateNormal];
        [_rechargeButton blueStyle];
//        _rechargeButton.cornerRadius = 5;
    }
    return _rechargeButton;
}

- (UIButton*)okButton
{
    if (!_okButton) {
        _okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _okButton.frame = CGRectMake(10, 10, ViewWidth-20, 40);
        [_okButton setTitle:@"确定" forState:UIControlStateNormal];
        [_okButton blueStyle];
//        _okButton.cornerRadius = 5;
    }
    return _okButton;
}

//- (void)awakeFromNib
//{
//    [super awakeFromNib];
//}

@end
