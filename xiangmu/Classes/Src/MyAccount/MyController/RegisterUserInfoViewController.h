//
//  RegisterUserInfoViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "AvatarViewController.h"

@protocol MyDetailViewControllerDelegate <NSObject>

@optional

//刷新个人信息
- (void)refreshMyInfo:(NSIndexPath *)indexpath;

@end

typedef NS_ENUM(NSInteger, RegisterUserInfoRow){
    RegisterUserInfoRowAvatar = 0,
    RegisterUserInfoRowMobile,
    RegisterUserInfoRowIdCertification,
    RegisterUserInfoRowNickName,
    RegisterUserInfoRowCompanyName,
    RegisterUserInfoRowCompanyCertification,
    RegisterUserInfoRowUsrInfoBindEmail
};

@interface RegisterUserInfoViewController : BaseViewController<AvatarDelegate>
{
    //头像
    UIImageView *avatarImg;
}

@property (nonatomic,weak) id<MyDetailViewControllerDelegate> personDelegate;


@property (nonatomic,strong) NSIndexPath *indexpath;

@property (nonatomic, strong) NSData *avatarImageData;

@property (nonatomic, strong) NSString *imageFileContents;// BASE64编码后的图片数据

@end
