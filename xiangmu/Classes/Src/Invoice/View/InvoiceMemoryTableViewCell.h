//
//  InvoiceMemoryTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/29.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UIPlaceHolderTextView.h"
#import "XXTextView.h"

@interface InvoiceMemoryTableViewCell : UITableViewCell

@property (strong, nonatomic) XXTextView *memoTextView;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

+(CGFloat)getCellHeight;

@end
