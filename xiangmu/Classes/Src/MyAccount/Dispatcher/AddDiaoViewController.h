//
//  AddDiaoViewController.h
//  xiangmu
//
//  Created by David kim on 16/3/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "AddDiaoDetailViewController.h"

@protocol Diaodu <NSObject>

-(void)diaodu;

@end
@interface AddDiaoViewController : LFNavController<DiaoDu>

@property (nonatomic,weak) id <Diaodu>delegate;
@end
