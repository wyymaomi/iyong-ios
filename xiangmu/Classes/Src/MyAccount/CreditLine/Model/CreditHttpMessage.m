//
//  CreditHttpMessage.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CreditHttpMessage.h"
#import <objc/message.h>

@implementation CreditHttpMessage


-(NSString*)description;
{
    unsigned int propertyCount;
    objc_property_t *properties = class_copyPropertyList([self class], &propertyCount);
    NSMutableArray *propNameAndValueArray = [NSMutableArray new];
    for (unsigned int i = 0; i < propertyCount; i++)
    {
        // get property name
        objc_property_t property = properties[i];
        const char *char_f = property_getName(property);
        NSString *propertyName = @(char_f);
        if ([propertyName isEqualToString:@"amount"]) {
            continue;
        }
        id propertyValue = [self valueForKey:(NSString *)propertyName];
        if (propertyValue) {
            if ([propertyValue isKindOfClass:[NSString class]]) {
                //                [propNameAndValueArray addObject:[NSString stringWithFormat:@"%@=%@", propertyName,  [propertyValue stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                [propNameAndValueArray addObject:[NSString stringWithFormat:@"%@=%@", propertyName, propertyValue]];
            }
            else if ([propertyValue isKindOfClass:[NSNumber class]]) {
                NSNumber *value = (NSNumber*)propertyValue;
                [propNameAndValueArray addObject:[NSString stringWithFormat:@"%@=%@", propertyName, [value stringValue]]];
            }
        }
        else {
            [propNameAndValueArray addObject:[NSString stringWithFormat:@"%@=", propertyName]];
        }
    }
    free(properties);
    
    NSString *result = [propNameAndValueArray componentsJoinedByString:@"&"];
    //    DLog(@"request parmas = %@", result);
    return result;
}

@end
