//
//  PartnerTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PartnerModel.h"

@interface MyPartnerTableViewCell : UITableViewCell

//@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UIButton *dialButton;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
-(void)initData:(PartnerModel*)partnerModel;

@end
