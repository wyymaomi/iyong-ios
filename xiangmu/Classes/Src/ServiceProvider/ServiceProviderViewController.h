//
//  ServiceProviderViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

#import "UIPlaceHolderTextView.h"

typedef NS_ENUM(NSInteger, ImageViewIndex)
{
    ImageViewIndex1 = 1000,
    ImageViewIndex2 = 2000,
    ImageViewIndex3 = 3000,
    ImageViewIndex4 = 4000
};

typedef NS_ENUM(NSInteger, PublishType)
{
    PublishTypeBusiness = 0,  // 经营范围
    PublishTypeUserExperience // 用户体验
};

@interface ServiceProviderViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *textView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIImageView *imageView2;
@property (weak, nonatomic) IBOutlet UIImageView *imageView3;
@property (weak, nonatomic) IBOutlet UIImageView *imageView4;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (nonatomic, assign) NSInteger type;
@end
