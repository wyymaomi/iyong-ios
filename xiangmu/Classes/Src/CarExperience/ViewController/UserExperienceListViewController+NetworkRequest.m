//
//  UserExperienceListViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/6.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "UserExperienceListViewController+NetworkRequest.h"
#import "AppConfig.h"
#import "UserExpericeModel.h"

@implementation UserExperienceListViewController (NetworkRequest)

#pragma mark - 按页码分页

- (void)fetchExperienceListByPageIndex:(NSInteger)pageIndex
{
    
    if (pageIndex < 1) {
        return;
    }
    
    NSString *url;
    url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_LIST_API];
    
    
    NSDictionary *params = @{@"pageIndex": [NSString stringWithFormat:@"%ld", (long)pageIndex]};
//                             @"areaCode" : @""};
//                             @"areaCode":[AppConfig currentConfig].gpsCityCode};
    
    if (pageIndex == 1) {
        [self.dataList removeAllObjects];
    }
    
    //    if (isHeaderRefresh) {
    //        self.pullRefreshTableView.footerHidden = YES;
    //        [self.dataList removeAllObjects];
    //    }
    
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        //        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        if (code_status == STATUS_OK) {
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                
                UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
                if (error) {
                    return;
                }
                
                NSArray<UserExpericeModel> *list = responseData.data.list;
                if (list.count > 0) {
                    strongSelf.commonPage.pageIndex++;
                }
                for (NSInteger i = 0; i < list.count; i++) {
                    UserExpericeModel *model = list[i];
                    UserExpericenViewModel *viewModel = [[UserExpericenViewModel alloc] init];
                    viewModel.serviceModel = model;
                    [strongSelf.dataList addObject:viewModel];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    strongSelf.loadStatus = LoadStatusFinish;
                    
                    [strongSelf.pullRefreshTableView reloadData];
                    
                    strongSelf.pullRefreshTableView.footerHidden = NO;
                });
                
            });
            
        }
        else if (code_status == NETWORK_FAILED) {
            strongSelf.loadStatus = LoadStatusNetworkError;
        }
        else {
            strongSelf.loadStatus = LoadStatusFinish;
//            NSError *error;
            //            OnlineServiceResponseData *responseData = [[OnlineServiceResponseData alloc] initWithDictionary:result error:&error];
            //            if (error) {
            //                return;
            //            }
            //            [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
        }
        
    }];
}

- (void)addLikeNumber:(UserExpericenViewModel*)viewModel
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_LIKE_API];
    NSString *params = [NSString stringWithFormat:@"experienceId=%@", viewModel.serviceModel.id];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSInteger likeNumbers = [viewModel.serviceModel.thumbsQuantity integerValue];
            likeNumbers++;
            viewModel.serviceModel.thumbsUp = @1;
            viewModel.serviceModel.thumbsQuantity = [NSString stringWithFormat:@"%ld", likeNumbers];
            [strongSelf.pullRefreshTableView reloadData];
        }
        else {
            if (code_status == 1852) {
                [MsgToolBox showToast:@"已点过赞"];
            }
            else {
                [MsgToolBox showToast:getErrorMsg(code_status)];
            }
        }
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

- (void)addLikeNumber:(UserExpericenViewModel*)viewModel likeLabel:(UILabel*)likeLabel likeImageView:(UIImageView*)likeImageView
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_LIKE_API];
    NSString *params = [NSString stringWithFormat:@"experienceId=%@", viewModel.serviceModel.id];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            NSInteger likeNumbers = [viewModel.serviceModel.thumbsQuantity integerValue];
            likeNumbers++;
            likeLabel.text = [NSString stringWithFormat:@"%ld", (long)likeNumbers];
            likeImageView.image = [UIImage imageNamed:@"icon_heart"];
            viewModel.serviceModel.thumbsUp = @1;
        }
        else {
            if (code_status == 1852) {
                [MsgToolBox showToast:@"已点过赞"];
            }
            else {
                [MsgToolBox showToast:getErrorMsg(code_status)];
            }
        }
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}




@end
