//
//  AccountInfoView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountInfoTableView.h"
#import "EnterpriseUserCollectionView.h"
#import "AccountInfoTableView.h"
#import "FirstCollectionViewCell.h"
#import "EnterpriseUserinfoView.h"
#import "BBAlertView.h"

@protocol AccountInfoViewDelegate <NSObject>

- (void)onDismiss;
- (void)onGotoPage:(NSString*)vcPage title:(NSString*)title;
//- (void)presentPage:(NSString*)vcPage;
- (void)openCompanyPublish;
- (void)openAdvPurchase;
- (void)openProviderInfo;

@end

@interface AccountInfoView : UIView

@property (nonatomic, strong) UIView        *bgView;
@property (nonatomic, assign) double        *bgViewWidth;


@property (nonatomic, strong) UIWindow      *maskWindow;
@property (nonatomic, strong) UIView        *maskView;

@property (nonatomic, strong) UIView        *subBgView;
@property (nonatomic, strong) UIButton      *collapseButton;
@property (nonatomic, strong) UILabel       *titleLabel;
//@property (nonatomic, strong) NSArray       *subviewCellList;
@property (nonatomic, assign) BOOL          isBottom;// 是否处于底部
@property (nonatomic, strong) UIButton      *bottomButton; // 底部按钮
@property (nonatomic, strong) EnterpriseUserinfoView *enterpriseUserinfoView; // 企业模式视图
//@property (nonatomic, strong) UIView        *enterpriseUsrView; // 企业模式视图

@property (nonatomic, strong) AccountInfoTableView *accountTableView;
@property (nonatomic, strong) EnterpriseUserCollectionView *collectionView;

@property (nonatomic, assign) id<AccountInfoViewDelegate> delegate;

@property (nonatomic, assign) BOOL isNeedAnimation;// 是否需要动画效果
@property (nonatomic, assign) BOOL isVisible;

- (void)dismiss;

- (void)show;
//- (void)show:(NSArray*)images borderColor:(UIColor*)color;

-(void)refreshOrderCount:(NSInteger)orderCount;

- (void)refreshPartnerNumber:(NSInteger)partnerNumber;

@end
