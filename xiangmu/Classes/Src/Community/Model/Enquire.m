//
//  Communtiy.m
//  xiangmu
//
//  Created by 湛思科技 on 16/9/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "Enquire.h"

@implementation BidModel

@end

@implementation EnquireModel

- (BOOL)checkIsEnd;
{
    NSTimeInterval inquireEndTime = [self.createTime doubleValue] + [self.releaseTime doubleValue] * 60 * 1000;
    BOOL isEnd = [self.end boolValue] || (inquireEndTime/1000 < lround([[NSDate now] timeIntervalSince1970]));
    return isEnd;
}

@end



@implementation EnquireResponse

@end
