//
//  TextInputView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextInputView : UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *vechileModelTextField;
@property (weak, nonatomic) IBOutlet UITextField *vechileQuantityTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UITextField *remarkTextField;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

//@property (nonatomic, copy) void(^clickBlock)();

@property (nonatomic, copy) void (^completeHandle)(NSString *vechileModel, NSString *vechileQuantity, NSString *price, NSString *remark);

//@property (nonatomic, copy) void (^cancelHandle)

@end
