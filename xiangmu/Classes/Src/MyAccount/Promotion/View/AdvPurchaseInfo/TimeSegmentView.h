//
//  TimeSegmentView.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, TImeSegmentViewStatus)
{
    TimeSegmentViewStatusSelected,
    TimeSegmentViewStatusUnSelected,
    TimeSegmentViewStatusDisabled
};

@protocol TimeViewDelegate <NSObject>

-(void)onClickTimeView:(UIButton*)button;

@end

@interface TimeSegmentView : UIView
@property (strong, nonatomic) UIButton *button;
@property (strong, nonatomic) UILabel *priceLabel;
@property (strong, nonatomic) UILabel *startTimeLabel;
@property (strong, nonatomic) UILabel *endTimeLabel;

@property (nonatomic, assign) NSUInteger status;

@property (nonatomic, assign) id<TimeViewDelegate> delegate;

//- (void)initButtonStatus;

@end
