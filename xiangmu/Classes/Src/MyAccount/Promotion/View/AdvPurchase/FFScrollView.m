//
//  FFScrollView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "FFScrollView.h"

@implementation FFScrollView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(id)initPageViewWithFrame:(CGRect)frame views:(NSArray *)views tag:(NSString *)tag
{
    self = [super initWithFrame:frame];
    if (self) {
        self.sourceArr = views;
        self.userInteractionEnabled = YES;
        [self initSubviewsWithFrame:frame];
    }
    return self;
}

- (void)initSubviewsWithFrame:(CGRect)frame
{
    CGFloat width = frame.size.width;
    CGFloat height = frame.size.height;
    CGRect fitRect = CGRectMake(0, 0, width, height);
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:fitRect];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(width*self.sourceArr.count, height);
    self.scrollView.delegate = self;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    [self addSubview:self.scrollView];
}

@end
