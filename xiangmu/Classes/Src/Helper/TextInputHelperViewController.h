//
//  TextInputHelperViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceHolderTextView.h"
#import "BaseViewController.h"
#import "InputHelperViewController.h"

@interface TextInputHelperViewController : BaseViewController

@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) NSInteger tag;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *textView;
@property (weak, nonatomic) id<InputHelperDelegate> delegate;

@property (nonatomic, copy) void (^completeHandle)(NSInteger tag, NSString *text);

@end
