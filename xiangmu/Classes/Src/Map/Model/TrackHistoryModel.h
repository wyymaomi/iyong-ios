//
//  TrackHistoryModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol EndPoint <NSObject>

@end

@protocol StartPoint <NSObject>

@end

@protocol TrackPoint <NSObject>

@end

@interface EndPoint : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *latitude;
@property (nonatomic, strong) NSNumber<Optional> *loc_time;
@property (nonatomic, strong) NSNumber<Optional> *longitude;

@end

@interface StartPoint : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *latitude;
@property (nonatomic, strong) NSNumber<Optional> *loc_time;
@property (nonatomic, strong) NSNumber<Optional> *longitude;

@end

@interface TrackPoint : JSONModel

@property (nonatomic, strong) NSString<Optional> *coord_type;
@property (nonatomic, strong) NSString<Optional> *create_time;
@property (nonatomic, strong) NSNumber<Optional> *direction;
@property (nonatomic, strong) NSNumber<Optional> *height;
@property (nonatomic, strong) NSNumber<Optional> *latitude;
@property (nonatomic, strong) NSNumber<Optional> *loc_time;
@property (nonatomic, strong) NSNumber<Optional> *radius;
@property (nonatomic, strong) NSNumber<Optional> *speed;


@end





@interface TrackHistoryModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *distance;
@property (nonatomic, strong) EndPoint<Optional> *end_point;
@property (nonatomic, strong) NSString<Optional> *message;
@property (nonatomic, strong) NSArray<TrackPoint*> *points;
@property (nonatomic, strong) NSNumber<Optional> *size;
@property (nonatomic, strong) StartPoint<Optional> *start_point;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *tag;
@property (nonatomic, strong) NSNumber<Optional> *toll_distance;
@property (nonatomic, strong) NSNumber<Optional> *total;

@end
