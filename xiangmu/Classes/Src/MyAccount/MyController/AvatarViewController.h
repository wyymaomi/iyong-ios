//
//  MyTouxiangViewController.h
//  xiangmu
//
//  Created by David kim on 16/5/27.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"

@protocol AvatarDelegate<NSObject>

-(void)onGetAvatarImage:(NSData*)imageData;

@end

@interface AvatarViewController : LFNavController
@property (nonatomic,strong) UITableView *avatarTableView;
//@property (nonatomic, strong) NSData *avatar
@property (nonatomic,strong) UIImageView *avatarImageView;
@property (nonatomic,assign)id<AvatarDelegate>delegate;
@property (nonatomic, strong) NSData *avatarImageData; // 头像图片数据
@property (nonatomic, assign) BOOL changeAvatarImage; // 是否已经改变头像
@end
