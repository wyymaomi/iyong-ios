//
//  CompanyCommentBodyView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProviderCommentViewModel.h"

@interface CompanyCommentBodyView : UIView

@property (nonatomic, strong) ProviderCommentViewModel *viewModel;

@property (nonatomic, strong) UIView *separatorLineView; // 公司基本信息
@property (nonatomic, strong) UIView *businessView;// 经营范围

//@property (nonatomic, strong) StarView *starView;

@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *companyLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIView *lineView;


@end
