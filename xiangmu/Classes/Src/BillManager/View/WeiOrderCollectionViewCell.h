//
//  WeiOrderCollectionViewCell.h
//  xiangmu
//
//  Created by David kim on 16/4/19.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeiOrderCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *dateLabel;
@property(nonatomic,strong)UILabel *jichangLabel;
@property(nonatomic,strong)UILabel *daodaLabel;
@end
