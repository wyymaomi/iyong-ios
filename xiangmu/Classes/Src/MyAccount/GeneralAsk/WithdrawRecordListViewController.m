//
//  WithdrawRecordListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "WithdrawRecordListViewController.h"
#import "PullRefreshTableView.h"
#import "WithdrawRecordInreviewTableViewCell.h"
#import "WithdrawRecordRejectTableViewCell.h"
#import "WithdrawCompleteTableViewCell.h"
#import "WithdrawModel.h"

@interface WithdrawRecordListViewController ()<EmptyDataDelegate>

@end

@implementation WithdrawRecordListViewController


#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view addSubview:self.pullRefreshTableView];
    [self.pullRefreshTableView reloadData];
    self.emptyDataDelegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.pullRefreshTableView beginHeaderRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PullRefresh delegate method

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;

    [self refreshData:nil isHeaderRefresh:YES];
    
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        [self.pullRefreshTableView endFooterRefresh];
        return;
    }
    
    if (self.dataList.count > 0) {
        self.nextAction = @selector(onFooterRefresh);
        self.object1 = nil;
        self.object2 = nil;
        
        WithdrawModel *model = [self.dataList lastObject];
        [self refreshData:[model.createTime stringValue] isHeaderRefresh:NO];
    }

}

-(void)refreshData:(NSString*)createTime isHeaderRefresh:(BOOL)isHeaderRefresh
{
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:createTime]];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, WITHDRAW_LIST_API];
    
    if (isHeaderRefresh) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseObject) {
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        strongSelf.pullRefreshTableView.footerHidden = NO;
        strongSelf.loadStatus = LoadStatusFinish;

        WithdrawResponseData *responseData = [[WithdrawResponseData alloc] initWithDictionary:responseObject error:nil];
        if ([responseData.code integerValue] == STATUS_OK) {
            
            [strongSelf.dataList addObjectsFromArray:responseData.data];
            [strongSelf.pullRefreshTableView reloadData];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[responseData.code integerValue]]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
//        [strongSelf showAlertView:errorDesc];
        strongSelf.loadStatus = LoadStatusNetworkError;
        
    }];
}

#pragma mark - UITableView Delegate method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.00001f;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.00001f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WithdrawModel *model = self.dataList[indexPath.section];
    if ([model.status integerValue] == 1 || [model.status integerValue] == 2) {
        WithdrawRecordInreviewTableViewCell *cell = [WithdrawRecordInreviewTableViewCell  cellWithTableView:tableView];
        cell.amountLabel.text = [NSString stringWithFormat:@"¥%@", [model.price stringValue]];
        
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([model.createTime doubleValue] / 1000.0f)];
        cell.startTimeLabel.text = [startDate stringWithDateFormat:@"MM-dd HH:mm"];
        
        NSDate *endDate = [NSDate dateWithTimeInterval:24*60*60 sinceDate:startDate];
        cell.endTimeLabel.text = [endDate stringWithDateFormat:@"MM-dd HH:mm"];
        
        return cell;
    }
    else if ([model.status integerValue] == 3) {
        WithdrawRecordRejectTableViewCell *cell = [WithdrawRecordRejectTableViewCell cellWithTableView:tableView];
        cell.amountLabel.text = [NSString stringWithFormat:@"¥%@", [model.price stringValue]];
        
        return cell;
    }
    else {
        WithdrawCompleteTableViewCell *cell = [WithdrawCompleteTableViewCell cellWithTableView:tableView];
        cell.amountLabel.text = [NSString stringWithFormat:@"¥%@", [model.price stringValue]];
        
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([model.createTime doubleValue] / 1000.0f)];
        cell.startTimeLabel.text = [startDate stringWithDateFormat:@"MM-dd HH:mm"];
        
        NSDate *endDate = [NSDate dateWithTimeInterval:24*60*60 sinceDate:startDate];
        cell.endTimeLabel.text = [endDate stringWithDateFormat:@"MM-dd HH:mm"];
        
        return cell;
    }
    return nil;
    
    

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WithdrawModel *model = self.dataList[indexPath.section];
    if ([model.status integerValue] == 1 || [model.status integerValue] == 2) {
        return 170;
    }
    else if ([model.status integerValue] == 3) {
        return 130;
    }
    return 125;
}

#pragma mark - EmptyDataDelegate methods

- (NSString*)emptyDetailText;
{
    return @"暂时没有余额提现记录";
}


@end
