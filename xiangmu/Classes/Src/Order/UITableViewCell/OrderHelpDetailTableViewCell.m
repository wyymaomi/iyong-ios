//
//  OrderHelpDetailTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderHelpDetailTableViewCell.h"

@implementation OrderHelpDetailTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"CellIdentifier";
    OrderHelpDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.backgroundColor = UIColorFromRGB(0xDEDEDE);
//        cell.driverMobileButton.lineColor = UIColorFromRGB(0x00A04E);
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
