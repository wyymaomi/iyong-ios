//
//  AddBankcardNumberTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBankcardNumberTableViewCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

@end
