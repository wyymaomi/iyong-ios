//
//  NewCustomTabBar.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewCustomTabBar.h"

@implementation NewCustomTabBar

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        
        // 去掉顶部阴影
        [[UITabBar appearance] setShadowImage:[UIImage new]];
        [[UITabBar appearance] setBackgroundImage:[[UIImage alloc]init]];
        [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 0.5)];
        lineView.backgroundColor = UIColorFromRGB(0xCCCCCC);
        [self addSubview:lineView];
//        self.topLineView = lineView;
        
        //        UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 1, ViewWidth, self.height)];
        //        bgImageView.image = [UIImage imageNamed:@"img_tab_bar"];
        //        [self addSubview:bgImageView];
//        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 1)];
//        lineView.backgroundColor = UIColorFromRGB(0xCCCCCC);
//        [self addSubview:lineView];
//        self.topLineView = lineView;
//        
//        UIButton *plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [plusBtn setBackgroundImage:[UIImage imageNamed:@"icon_tab_plus"] forState:UIControlStateNormal];
//        [plusBtn setBackgroundImage:[UIImage imageNamed:@"icon_tab_plus_focused"] forState:UIControlStateHighlighted];
//        [plusBtn setBackgroundImage:[UIImage imageNamed:@"icon_tab_plus_focused"] forState:UIControlStateSelected];
//        //        plusBtn.backgroundColor = [UIColor grayColor];
//        self.centerButton = plusBtn;
//        [plusBtn addTarget:self action:@selector(plusBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:plusBtn];
//        
//        UILabel *centerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//        centerLabel.text = @"发布";
//        centerLabel.font = FONT(12);
//        centerLabel.textColor = UIColorFromRGB(0x333333);
//        [centerLabel sizeToFit];
//        [self addSubview:centerLabel];
//        self.titleLabel = centerLabel;
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //    self.semiCircleImageView.centerX = self.centerX;
    //    self.semiCircleImageView.centerY = self.height * 0.5 - 3 * LBMagin;//self.height * 0.5 - 2 * LBMagin;
    ////    DLog(@"self.semiCircleImageView.centerY = %f", self.semiCircleImageView.centerY);
    //    self.semiCircleImageView.size = CGSizeMake(self.semiCircleImageView.image.size.width, self.semiCircleImageView.image.size.height);
    
//    self.topLineView.top = 0;
//    
//    self.centerButton.left = (ViewWidth-47)/2;//ViewWidth/2-self.centerButton.currentBackgroundImage.size.width/2;
//    self.centerButton.top = -15;//self.height*0.5-2*LBMagin;//self.height * 0.5 - 2 * LBMagin;
//    self.centerButton.size = CGSizeMake(47, 47);
//    
//    self.titleLabel.centerX = self.centerButton.centerX;
//    self.titleLabel.centerY = CGRectGetMaxY(self.centerButton.frame) + 8 ;
    
    //    self.titleLabel.left = ViewWidth-
    //    self.titleLabel.left = self.centerButton.left;
    //    self.titleLabel.top = self.height * 0.5;
    //    self.titleLabel.width = 30;
    //    self.titleLabel.height = 30;
    
    //    self.titleLabel.frame = CGRectMake(<#CGFloat x#>, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    
    //    self.centerButton.centerX = 20;//self.frame.size.width/2-self.centerButton.currentImage.size.width;
    //    self.centerButton.centerY = self.height * 0.5 - 1.5 * LBMagin ;
    //    self.centerButton.size = CGSizeMake(self.centerButton.currentBackgroundImage.size.width, self.centerButton.currentBackgroundImage.size.height);
    
    
    //    centerLabel.frame = CGRectMake(ViewWidth/2-, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    //    centerLabel.frame = CGRectMake(, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    //    centerLabel.centerX = self.centerButton.centerX;
    //    centerLabel.centerY = CGRectGetMaxY(self.centerButton.frame) + LBMagin;
    
    //    DLog(@"self.plusBtn.centerY = %f", self.centerButton.centerY);
    
    //    self.titleLabel.centerX = self.centerButton.centerX;
    //    self.titleLabel.centerY = CGRectGetMaxY(self.centerButton.frame) + LBMagin ;
    
    //    int btnIndex = 0;
    //    Class class = NSClassFromString(@"UITabBarButton");
    
    //    for (UIView *btn in self.subviews) {//遍历tabbar的子控件
    //
    //        if ([btn isKindOfClass:class]) {
    //
    //            btn.width = self.width / 5;
    //
    //            btn.left = btn.width * btnIndex;
    //
    //            btnIndex++;
    //
    //            //如果是索引是2(从0开始的)，直接让索引++，目的就是让消息按钮的位置向右移动，空出来发布按钮的位置
    //            if (btnIndex == 2) {
    //                btnIndex++;
    //            }
    //
    //        }
    //    }
    
//    NSUInteger btnIndex = 0;
    Class class = NSClassFromString(@"UITabBarButton");
    NSUInteger btnIndex = 0;
//    for (UIView *button in self.subviews) {// 遍历tabbar的子控件
//        if ([button isKindOfClass:class]) {
//            button.width = self.width/3;
//            button.left = btnIndex * button.width;
//            btnIndex++;
//        }
//    }
    
//    [self bringSubviewToFront:self.centerButton];
    
}

@end
