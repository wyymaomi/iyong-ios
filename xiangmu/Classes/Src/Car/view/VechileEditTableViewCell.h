//
//  VechileEditTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VechileEditTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIImageView *verifyImageView;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
