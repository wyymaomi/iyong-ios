//
//  ForgetPaypwdViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ForgetPaypwdViewModel.h"
#import "UserHelper.h"

@implementation ForgetPaypwdViewModel

-(NSString*)encryptMobileText:(NSString*)mobile
{
    if ([mobile isEqualToString:@""] || mobile == nil) {
        return @"";
    }
    
    NSString *mobileEncode = [NSString stringWithFormat:@"%@*******%@", [mobile substringToIndex:2], [mobile substringFromIndex:(mobile.length - 2)]];
    return mobileEncode;
}

- (BOOL)validateHttpDTO:(NSString**)errorMsg;
{
    
    if (![UserHelper validateCodeString:self.verificationCode error:errorMsg]) {
        return NO;
    }
    
    if (![UserHelper verifyPaypwd:self.payPassword error:errorMsg]) {
        return NO;
    };
    
    if (![self.payPassword isEqualToString:self.rePayPassword]) {
        *errorMsg = @"两次输入密码不同，请重新输入";
        return NO;
    }
    
    
    return YES;
}

@end
