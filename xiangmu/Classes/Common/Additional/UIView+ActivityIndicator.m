//
//  UIView+ActivityIndicator.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UIView+ActivityIndicator.h"
#import "MBProgressHUD.h"

@implementation UIView (ActivityIndicator)

#pragma mark -
#pragma mark hud

- (void)showHUDIndicatorViewAtCenter:(NSString *)indiTitle
{
    MBProgressHUD *hud = [self getHUDIndicatorViewAtCenter];
    
    if (hud == nil){
        
        hud = [self createHUDIndicatorViewAtCenter:indiTitle yOffset:-10];
        [hud show:YES];
    }else{
        hud.labelText = indiTitle;
    }
}

- (void)hideHUDIndicatorViewAtCenter
{
    MBProgressHUD *hud = [self getHUDIndicatorViewAtCenter];
    
    [hud hide:YES];
}

- (MBProgressHUD *)createHUDIndicatorViewAtCenter:(NSString *)indiTitle yOffset:(CGFloat)y
{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self];
    hud.layer.zPosition = 10;
    hud.yOffset = y;
//    hud.minShowTime = 3;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = indiTitle;
    [self addSubview:hud];
    hud.tag = hudViewTag;
    return hud;
}

- (MBProgressHUD *)getHUDIndicatorViewAtCenter
{
    UIView *view = [self viewWithTagNotDeepCounting:hudViewTag];
    
    if (view != nil && [view isKindOfClass:[MBProgressHUD class]]){
        
        return (MBProgressHUD *)view;
    }
    else
    {
        return nil;
    }
}

- (UIView *)viewWithTagNotDeepCounting:(NSInteger)tag
{
    for (UIView *view in self.subviews)
    {
        if (view.tag == tag) {
            return view;
            break;
        }
    }
    return nil;
}

@end
