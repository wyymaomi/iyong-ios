//
//  LaunchImageViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/9.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "LaunchImageViewController.h"
#import "AppDelegate.h"

@interface LaunchImageViewController ()

@property (nonatomic,strong) UIImageView *launchImageView; //图片视图

@property (nonatomic,strong) UIButton *disMissBtn; //跳过按钮

@property (nonatomic,strong) NSTimer *timer; //计时器

@property(nonatomic,copy)dispatch_source_t waitDataTimer;

@end

#define DISPATCH_SOURCE_CANCEL_SAFE(time) if(time)\
{\
dispatch_source_cancel(time);\
time = nil;\
}

@implementation LaunchImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.realLaunchImageView = [[UIImageView alloc] init];
//    _realLaunchImageView.image = [UIImage imageNamed:@"launchImageForJiaZai"];
//    [self.view addSubview:_realLaunchImageView];
//    [_realLaunchImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.top.right.and.bottom.equalTo(weakSelf.view);
//    }];
//    [self.view bringSubviewToFront:_realLaunchImageView];
    
    _launchImageView = [[UIImageView alloc] init];
    _launchImageView.image = [UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"splash"]];
    _launchImageView.frame = CGRectMake(0, 0, ViewWidth, ViewHeight);
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.launchImageView];
    
//    self.disMissBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//    self.disMissBtn.frame = CGRectMake(ViewWidth-100, 10, 50, 20);
//    [_disMissBtn setTitle:@"跳过" forState:(UIControlStateNormal)];
//    [_disMissBtn addTarget:self action:@selector(clickDisMissBtn:) forControlEvents:(UIControlEventTouchUpInside)];
//    [self.view addSubview:_disMissBtn];
//    [_disMissBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.launchImageView).offset(-20);
//        make.top.equalTo(weakSelf.launchImageView).offset(20);
//    }];
    

    
//    if (_initBlock) {
//        _initBlock();
//    }
    
    DLog(@"WYY sleep for two millions");
    
    //计时器
//    self.timer = [NSTimer scheduledTimerWithTimeInterval:SPLASH_TIME target:self selector:@selector(swtichTabController) userInfo:nil repeats:YES];
    
//    [NSThread sleepForTimeInterval:SPLASH_TIME];
    
//
    
//    [self startWaitDataDispathTiemr];

    dispatch_time_t delayInSeconds = dispatch_time(DISPATCH_TIME_NOW,(int64_t)(2 * NSEC_PER_SEC));
    dispatch_after(delayInSeconds, dispatch_get_main_queue(), ^{
        //.....
        [self swtichTabController];
    });

}

-(void)dealloc
{
    DLog(@"dealloc");
}

-(void)startWaitDataDispathTiemr
{
    __block NSInteger duration = SPLASH_TIME;
//    if(_waitDataDuration) duration = _waitDataDuration;
    NSTimeInterval period = 1.0;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _waitDataTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_waitDataTimer, dispatch_walltime(NULL, 0), period * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_waitDataTimer, ^{
        
//        dispatch_main_sync_safe(dispatch_get_main_queue(), ^{
//        dispatch_main_sync_safe(^{
            if(duration==0)
            {
                DISPATCH_SOURCE_CANCEL_SAFE(_waitDataTimer);
                dispatch_main_sync_safe(^{
                    AppDelegate *appDelegate = APP_DELEGATE;
                    [appDelegate initViewsFromGuidePage];
                });

//                [self remove];
                return ;
            }
            duration--;
//        });
    });
    
    dispatch_resume(_waitDataTimer);
}

-(void)viewDidDisappear:(BOOL)animated
{
    _launchImageView = nil;
    
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

//-(void)

-(void)clickDisMissBtn:(id)sender
{
    [self swtichTabController];
}

-(void)swtichTabController
{
//    if (_completeBlock) {
//        _completeBlock();
//    }
    
    AppDelegate *appDelegate = APP_DELEGATE;
    [appDelegate initViewsFromGuidePage];
}

-(UIImageView*)launchImageView
{
    if (!_launchImageView) {
        _launchImageView = [[UIImageView alloc] init];
        _launchImageView.image = [UIImage imageNamed:@"LaunchImage"];
    }
    return _launchImageView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
