//
//  LoginView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/7.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "LoginView.h"

#import "WXApi.h"

@implementation LoginView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.closeButton.frame = CGRectMake(15, 18, 15, 15);
    
    self.logoImageView.frame = CGRectMake((ViewWidth-65*Scale)/2, 86*Scale, 65*Scale, 65*Scale);
    
    if (iPhone4) {
        self.logoImageView.top = 50;
    }
    
    
    self.logoTextLabel.frame = CGRectMake(0, self.logoImageView.bottom+17*Scale, ViewWidth, 18*Scale);
    self.logoTextLabel.textAlignment = UITextAlignmentCenter;
    self.logoTextLabel.textColor = [UIColor whiteColor];
    
    self.accountInputView.frame = CGRectMake(60*Scale, self.logoTextLabel.bottom+65*Scale, ViewWidth-120*Scale, 32*Scale);
    self.accountIcon.frame = CGRectMake(0, 0, 22, 22);
    self.accountTextField.textColor = [UIColor whiteColor];
    self.accountTextField.font = FONT(16);
    self.accountTextField.placeholder = @"请输入手机号";
    self.accountTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.accountTextField.frame = CGRectMake(self.accountIcon.right+40, 0, self.accountInputView.width-self.accountIcon.right-40, self.accountInputView.height);
    self.accountLine.frame = CGRectMake(0, self.accountInputView.height-0.5, self.accountInputView.width, 0.5);
    self.accountLine.backgroundColor = [UIColor whiteColor];
    
    if (iPhone4) {
        self.accountInputView.top = self.logoTextLabel.bottom+40;
    }
    
    self.pwdInputView.frame = CGRectMake(60*Scale, self.accountInputView.bottom+25*Scale, self.accountInputView.width, self.accountInputView.height);
    self.pwdIcon.frame = self.accountIcon.frame;
    self.pwdTextField.frame = self.accountTextField.frame;
    self.pwdTextField.textColor = [UIColor whiteColor];
    self.pwdTextField.font = FONT(16);
    self.pwdTextField.textColor = [UIColor whiteColor];
    self.pwdTextField.placeholder = @"请输入密码";
    self.pwdTextField.keyboardType = UIKeyboardTypeDefault;
    self.pwdLine.frame = CGRectMake(0, self.pwdInputView.height-0.5,self.pwdInputView.width, 0.5);
    self.pwdLine.backgroundColor = [UIColor whiteColor];
    
    NSString *forgetText = @"忘记密码？";
    CGSize forgetTextSize = [forgetText textSize:CGSizeMake(ViewWidth, MAXFLOAT) font:FONT(10)];
    self.forgetPwdButton.frame = CGRectMake(self.pwdInputView.right-forgetTextSize.width, self.pwdInputView.bottom+5*Scale, forgetTextSize.width, 12*Scale);
    [self.forgetPwdButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.forgetPwdButton setTitle:@"忘记密码？" forState:UIControlStateNormal];
    self.forgetPwdButton.titleLabel.font = FONT(10);
    self.forgetPwdButton.backgroundColor = [UIColor clearColor];
//    self.forgetPwdButton.titleLabel.textAlignment
//    self.forgetPwdButton.titleLabel.textAlignment = UITextAlignmentRight;
    
    self.loginButton.frame = CGRectMake(36*Scale, self.pwdInputView.bottom+50*Scale, ViewWidth-72*Scale, 42*Scale);
    [self.loginButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.loginButton.titleLabel.font = FONT(20);
    [self.loginButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.loginButton setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR] forState:UIControlStateHighlighted];
    self.loginButton.cornerRadius = 20;
    [self.loginButton setTitle:@"登录" forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.loginButton setTitleColor:NAVBAR_BGCOLOR forState:UIControlStateNormal];
    
    self.thirdPartLoginTextLabel.frame = CGRectMake(0, self.loginButton.bottom+75*Scale, ViewWidth, 15*Scale);
    self.thirdPartLoginTextLabel.textAlignment = UITextAlignmentCenter;
    self.thirdPartLoginTextLabel.textColor = [UIColor whiteColor];
    self.thirdPartLoginTextLabel.text = @"您还可以选择第三方登录";
    self.thirdPartLoginTextLabel.font = FONT(15);
    
    if (iPhone4) {
        self.thirdPartLoginTextLabel.top = self.loginButton.bottom+30;
    }
    
    self.lineView.frame = CGRectMake(60*Scale, self.thirdPartLoginTextLabel.bottom+10*Scale, ViewWidth-120*Scale, 0.5*Scale);
    self.lineView.backgroundColor = [UIColor whiteColor];
    
    self.wechatLoginImageView.frame = CGRectMake(62.5*Scale, self.lineView.bottom+35*Scale, 40*Scale, 40*Scale);
    self.qqLoginImageView.frame = CGRectMake(self.wechatLoginImageView.right+62.5*Scale, self.wechatLoginImageView.top, 40*Scale, 40*Scale);
    self.weiboLoginImageView.frame = CGRectMake(self.qqLoginImageView.right+62.5*Scale, self.wechatLoginImageView.top, 40*Scale, 40*Scale);
    
    if (iPhone4) {
        self.wechatLoginImageView.top = self.lineView.bottom+20;
        self.qqLoginImageView.top = self.wechatLoginImageView.top;
        self.weiboLoginImageView.top = self.wechatLoginImageView.top;
    }
    
    self.registerView.top = ViewHeight-55*Scale;
    self.qqLoginImageView.userInteractionEnabled = YES;
    self.wechatLoginImageView.userInteractionEnabled = YES;
    self.weiboLoginImageView.userInteractionEnabled = YES;
    
    self.noAccountTipLabel.width = ViewWidth/2-5;
    self.noAccountTipLabel.textAlignment = UITextAlignmentRight;
    self.registerButton.left = ViewWidth/2+5;
    
    self.pwdTextField.secureTextEntry = YES;
    
    //判断安装微信
    if ([WXApi isWXAppInstalled]){
        //安装了微信的处理
        self.wechatLoginImageView.hidden = NO;
    } else {
        //没有安装微信的处理
        self.wechatLoginImageView.hidden = YES;
        self.qqLoginImageView.left = ViewWidth/2-40*Scale-35*Scale;
        self.weiboLoginImageView.left = ViewWidth/2+35*Scale;
    }
    
    
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // CUSTOM INITIALIZATION HERE
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
//    [[NSBundle mainBundle] loadNibNamed:@"LoginView" owner:self options:nil];
    
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
