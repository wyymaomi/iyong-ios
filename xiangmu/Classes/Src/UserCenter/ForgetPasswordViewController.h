//
//  CpasswordViewController.h
//  xiangmu
//
//  Created by David kim on 16/4/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "TPKeyboardAvoidingScrollView.h"
typedef NS_ENUM(NSInteger,PasswordType)
{
    Changepassword=10,
    Resetpassword,
};

typedef NS_ENUM(NSInteger, UITextFieldType)
{
    TextFieldTypeMobile = 0,
    TextFieldTypeVerifyCode,
    TextFieldTypeNewPassword,
    TextFieldTypeReNewPassword
};

@interface ForgetPasswordViewController : BaseViewController
{
   UIButton *sendYanZheng;
}
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollview;
@property (nonatomic,assign)PasswordType type;
@end

