//
//  AdvDetailModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface AdvDetailModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *id;
@property (nonatomic, strong) NSString<Optional> *areaCode;
@property (nonatomic, strong) NSString<Optional> *areaName;
@property (nonatomic, strong) NSNumber<Optional> *defaultType;
@property (nonatomic, strong) NSString<Optional> *endTime;
@property (nonatomic, strong) NSNumber<Optional> *position;
@property (nonatomic, strong) NSNumber<Optional> *price;
@property (nonatomic, strong) NSString<Optional> *startTime;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, strong) NSNumber<Optional> *type;
@property (nonatomic, strong) NSString<Optional> *userId;
@property (nonatomic, strong) NSNumber<Optional> *purchaseFlag;

-(BOOL)canBuy;

@end
