//
//  CustomComboBoxView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CustomComboBoxView.h"

@implementation CustomComboBoxView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - 25, 0, 25, 25)];
        _imageView.image = [UIImage imageNamed:@"icon_white_arrow"];
        _imageView.backgroundColor = UIColorFromRGB(0x0094FF);
        _imageView.contentMode = UIViewContentModeCenter;
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self addSubview:_imageView];
        
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width - 25, frame.size.height)];
        _textLabel.font = [UIFont systemFontOfSize:14];
        _textLabel.backgroundColor = [UIColor whiteColor];
        _textLabel.userInteractionEnabled = YES;
        [self addSubview:_textLabel];
        

        
        self.userInteractionEnabled = YES;
        self.borderColor = UIColorFromRGB(0xDEDEDE);
        self.borderWidth = 1;
    }
    return self;
}

@end
