//
//  OrderHelper.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/18.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NewOrderHttpMessage.h"
#import "OrderBaseInfoResponseModel.h"

@interface OrderHelper : NSObject

+(void)checkIsSelfDispatchOrder:(NSString*)orderId success:(SuccessBlock)success andFailure:(FailureBlock)failure;

+(NewOrderHttpMessage*)getOrderHttpMessage:(OrderBaseInfoResponseModel*)orderBaseInfoModel price:(NSString*)price;

@end
