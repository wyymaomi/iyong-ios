//
//  InvoiceHistoryTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/1.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"

@interface InvoiceHistoryTableViewCell : CustomTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLabel;

@end
