//
//  ServiceProviderListController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "ProviderCollectionView.h"
#import "PullRefreshCollectionView.h"
#import "PageUtil.h"

typedef NS_ENUM(NSInteger, ProviderListType)
{
    ProviderListTypeAllList=0,
    ProviderListTypeMyFavorite
};

@interface ServiceProviderListController : BaseViewController

@property (nonatomic, assign) ProviderListType type;

@property (nonatomic, strong) ProviderCollectionView *collectionView;

@property (nonatomic, strong) NSString *areaCode;

@property (nonatomic, assign) NSUInteger businessType; // 服务类型

@property (nonatomic, strong) PageUtil *commonPage; // 分页处理


@end
