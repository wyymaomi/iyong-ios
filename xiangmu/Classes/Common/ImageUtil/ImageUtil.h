//
//  ImageUtil.h
//  lottery
//
//  Created by wyy on 15-1-8.
//  Copyright (c) 2015年 ___QIHUITEL___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageUtil : NSObject

+ (NSString *)dataFilePath;
+ (NSString *)documentFolderPath;
+ (void)saveImage:(UIImage *)image WithName:(NSString *)imageName;
+ (void)saveImageData:(NSData *)imageData WithName:(NSString *)imageName;
+ (UIImage*)loadImageData:(NSString*)directoryPath
               imageName:(NSString*)imageName;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+ (UIImage *)compressImage:(UIImage *)image toMaxFileSize:(NSInteger)maxFileSize;
+ (void)removeLocalImage:(NSString*)imageName;
+ (NSData*)compressImageWithData:(UIImage*)image toMaxFileSize:(NSInteger)maxFileSize;
+ (UIImage *)streImageNamed:(NSString *)imageName;

@end
