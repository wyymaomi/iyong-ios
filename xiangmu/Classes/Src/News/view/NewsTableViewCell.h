//
//  NewsTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserExpericeModel;

@interface NewsTableViewCell : UITableViewCell

//@property (nonatomic, strong) UITextField *titleLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *sourceFromLabel;
@property (nonatomic, strong) UIImageView *imgView;

+(CGFloat)getCellHeight;

-(void)initData:(id)model;

//-(void)initData:(UserExpericeModel*)model;

//@property (nonatomic, strong) 

@end
