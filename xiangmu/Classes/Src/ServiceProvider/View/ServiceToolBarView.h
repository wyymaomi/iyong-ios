//
//  ServiceToolBarView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ServiceProviderViewModel;

@interface ServiceToolBarView : UIView

@property (nonatomic, strong) UIView *likeView;
@property (nonatomic, strong) UIImageView *likeImageView;
@property (nonatomic, strong) UILabel *likeNumsLabel;

@property (nonatomic, strong) UIView *favoriteView;
@property (nonatomic, strong) UIImageView *favoriteImageView;
@property (nonatomic, strong) UILabel *favoriteLabel;

@property (nonatomic, strong) UIView *telephoneView;
@property (nonatomic, strong) UIButton *telephoneButton;

@property (nonatomic, strong) ServiceProviderViewModel *viewModel;

@end
