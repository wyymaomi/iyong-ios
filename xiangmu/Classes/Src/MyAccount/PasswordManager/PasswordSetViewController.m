//
//  MimaViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PasswordSetViewController.h"
#import "MyUtil.h"
#import "UserHelper.h"
#import "EncryptUtil.h"
#import "NSString+Encrypt.h"

@interface PasswordSetViewController ()
{
    NSString *title;
}

@property (nonatomic, assign) BOOL hasPayPassword;// 是否已经设置支付密码
@end

@implementation PasswordSetViewController

-(UITextField*)oldPwdTextField
{
    if (_oldPwdTextField == nil) {
        
        UITextField *oldPwdTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, ViewWidth, DefaultTableRowHeight)];
        oldPwdTextField.backgroundColor = [UIColor whiteColor];
        oldPwdTextField.font = DefaultTableRowSubtitleFont;
        oldPwdTextField.textColor = DefaultTableRowSubtitleTextColor;
        oldPwdTextField.textAlignment = UITextAlignmentRight;
        if (self.type == MimaTypeJiaoyi) {
            oldPwdTextField.placeholder=@"请输入旧交易密码";
            oldPwdTextField.keyboardType=UIKeyboardTypeNumberPad;
        }
        else if(self.type == MimaTypeLoad) {
            oldPwdTextField.placeholder = @"请输入旧登录密码";
            oldPwdTextField.keyboardType = UIKeyboardTypeDefault;
        }
//        self.oldPwdTextField = oldPwdTextField;
//        [self.view addSubview:self.oldPwdTextField];
        
        UILabel *oldPwdLabel = [MyUtil createLabelFrame:CGRectMake(20, 10, 100, 30) title:@"" font:DefaultTableRowTitleFont textAlignment:UITextAlignmentLeft numberOfLines:1 textColor:DefaultTableRowTitleTextColor];
        if (self.type == MimaTypeJiaoyi) {
            oldPwdLabel.text=@"旧交易密码";
        }
        else if(self.type == MimaTypeLoad) {
            oldPwdLabel.text = @"旧登录密码";
        }
        UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 30, 100, 50)];
        [leftView addSubview:oldPwdLabel];
        oldPwdTextField.leftView = leftView;
        oldPwdTextField.leftViewMode = UITextFieldViewModeAlways;
        
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(ViewWidth-20, 0, 20, 30)];
        rightView.backgroundColor = [UIColor whiteColor];
        oldPwdTextField.rightView = rightView;
        oldPwdTextField.rightViewMode = UITextFieldViewModeAlways;
        
//        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(oldPwdTextFIel, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)]
        
        _oldPwdTextField = oldPwdTextField;
        
    }
    return _oldPwdTextField;
}

-(UITextField*)pwdTextField
{
    if (_pwdTextField == nil) {
        
        
        UITextField *textfield = [[UITextField alloc]initWithFrame:CGRectMake(0, 10+DefaultTableRowHeight+1, ViewWidth, DefaultTableRowHeight)];
        textfield.backgroundColor = [UIColor whiteColor];
        textfield.font = DefaultTableRowSubtitleFont;
        textfield.textColor = DefaultTableRowSubtitleTextColor;
        textfield.textAlignment = UITextAlignmentRight;
        if (self.type == MimaTypeJiaoyi) {
            textfield.placeholder=@"请输入6位交易密码";
            textfield.keyboardType=UIKeyboardTypeNumberPad;
        }
        else if(self.type == MimaTypeLoad) {
            textfield.placeholder = @"请输入6-16位登录密码";
            textfield.keyboardType = UIKeyboardTypeDefault;
        }
        
        _pwdTextField = textfield;
//        self.pwdTextField = textfield;
//        [self.view addSubview:textfield];
        
        
        UILabel *textfieldLabel = [MyUtil createLabelFrame:CGRectMake(20, 10, 100, 30) title:@"登录密码" font:DefaultTableRowTitleFont textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:DefaultTableRowTitleTextColor];
        if (self.type == MimaTypeJiaoyi) {
            textfieldLabel.text = @"新交易密码";
            
        }
        else if(self.type == MimaTypeLoad) {
            textfieldLabel.text = @"新登录密码";
        }
        UIView *left = [[UIView alloc]initWithFrame:CGRectMake(0, 30, 100, 50)];
        [left addSubview:textfieldLabel];
        textfield.leftView = left;
        textfield.leftViewMode = UITextFieldViewModeAlways;
        
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(ViewWidth-20, 0, 20, 30)];
        rightView.backgroundColor = [UIColor whiteColor];
        textfield.rightView = rightView;
        textfield.rightViewMode = UITextFieldViewModeAlways;
        
    }
    return _pwdTextField;
}

-(UITextField*)rePwdTextField
{
    
    
    if (_rePwdTextField == nil) {
        
        _rePwdTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10+DefaultTableRowHeight*2+2, ViewWidth, DefaultTableRowHeight)];
        _rePwdTextField.backgroundColor = [UIColor whiteColor];
        _rePwdTextField.font = DefaultTableRowSubtitleFont;//FONT(14);
        _rePwdTextField.textColor = DefaultTableRowSubtitleTextColor;
        _rePwdTextField.textAlignment = UITextAlignmentRight;
        if (self.type == MimaTypeJiaoyi) {
            _rePwdTextField.placeholder = @"再次输入以确认";
            _rePwdTextField.keyboardType = UIKeyboardTypeNumberPad;
        }
        else if (self.type == MimaTypeLoad) {
            _rePwdTextField.placeholder = @"请再次输入以确认";
            _rePwdTextField.keyboardType = UIKeyboardTypeDefault;
        }
//        [self.view addSubview:_rePwdTextField];
        
        UILabel *reTetFieldLabel = [MyUtil createLabelFrame:CGRectMake(20, 10, 100, 30) title:@"再次输入" font:DefaultTableRowTitleFont textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:DefaultTableRowTitleTextColor];
        if (self.type == MimaTypeJiaoyi) {
            reTetFieldLabel.text=@"再次输入";
        }
        else if(self.type == MimaTypeLoad) {
            reTetFieldLabel.text = @"再次输入";
        }
        
        UIView *reLeftView = [[UIView alloc]initWithFrame:CGRectMake(0, 30, 100, 50)];
        [reLeftView addSubview:reTetFieldLabel];
        _rePwdTextField.leftView = reLeftView;
        //    textfield.keyboardType=UIKeyboardTypeNumberPad;
        //   [textfield.leftView addSubview:textfieldLabel];
        _rePwdTextField.leftViewMode = UITextFieldViewModeAlways;
        
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(ViewWidth-20, 0, 20, 30)];
        rightView.backgroundColor = [UIColor whiteColor];
        _rePwdTextField.rightView = rightView;
        _rePwdTextField.rightViewMode = UITextFieldViewModeAlways;
        
        
    }
    return _rePwdTextField;
    
}


#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createMyNav];
    [self createInterface];
    // Do any additional setup after loading the view.
}
-(void)createMyNav
{
    if (self.type==MimaTypeLoad) {
        title=@"设置登录密码";
    }
    else if(self.type==MimaTypeShoushi)
    {
        title=@"设置手势密码";
    }
    else
    {
        title=@"设置交易密码";
    }
    self.title = title;
}

-(void)viewDidLayoutSubviews
{
    
}

-(void)createInterface
{
    if (self.type == MimaTypeLoad) {
        [self.view addSubview:self.oldPwdTextField];
        [self.view addSubview:self.pwdTextField];
        [self.view addSubview:self.rePwdTextField];
        
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(doSetPwd)];

    }
    if (self.type == MimaTypeJiaoyi) {
        [self checkHasPayPwd];
        
        
        
//        UIButton *btn=[MyUtil createBtnFrame:CGRectMake(38*Scale, 200*Scale, ViewWidth-76*Scale, 41*Scale) title:@"确定" bgImageName:nil target:self action:@selector(doSetPwd)];
//        btn.titleLabel.font=[UIFont systemFontOfSize:16];
//        btn.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:150.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
//        btn.layer.cornerRadius=18.0f;
//        [btn whiteStyle];
//        
//        [self.view addSubview:btn];
    }


    

    
}


-(void)checkHasPayPwd
{
    
    self.nextAction = @selector(checkHasPayPwd);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_HAS_API];
    [self showHUDIndicatorViewAtCenter:@"正在加载中"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                // 已经设置支付密码
                strongSelf.hasPayPassword = YES;
                dispatch_main_async_safe(^{
                
                    [strongSelf.view addSubview:strongSelf.oldPwdTextField];
                    [strongSelf.view addSubview:strongSelf.pwdTextField];
                    [strongSelf.view addSubview:strongSelf.rePwdTextField];
                    
                    strongSelf.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"重置" style:UIBarButtonItemStyleDone target:self action:@selector(gotoForgetpaypwdViewController)];
                    
                    UIButton *btn=[MyUtil createBtnFrame:CGRectMake(38*Scale, 200*Scale, ViewWidth-76*Scale, 41*Scale) title:@"确定" bgImageName:nil target:self action:@selector(doSetPwd)];
                    btn.titleLabel.font=[UIFont systemFontOfSize:16];
                    btn.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:150.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
                    btn.layer.cornerRadius=18.0f;
                    [btn whiteStyle];
                    
                    [strongSelf.view addSubview:btn];
                    
                    
                });
                
            }
            else if([data integerValue] == 0){
                // 未设置支付密码
                strongSelf.hasPayPassword = NO;
                dispatch_main_async_safe(^{
                    
                    [strongSelf.view addSubview:strongSelf.pwdTextField];
                    [strongSelf.view addSubview:strongSelf.rePwdTextField];
                    strongSelf.pwdTextField.top = 10;
                    strongSelf.rePwdTextField.top = strongSelf.pwdTextField.bottom+1;
                    
                    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(doSetPwd)];
                    
                });
                
            }
            
        }
        else {
            strongSelf.hasPayPassword = NO;
            dispatch_main_async_safe(^{
                [strongSelf.view addSubview:strongSelf.oldPwdTextField];
                [strongSelf.view addSubview:strongSelf.pwdTextField];
                [strongSelf.view addSubview:strongSelf.rePwdTextField];
                
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(doSetPwd)];
            })
            
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
//        [strongSelf showAlertView:errorDesc];

        strongSelf.hasPayPassword = NO;
        dispatch_main_async_safe(^{
            [strongSelf.view addSubview:strongSelf.oldPwdTextField];
            [strongSelf.view addSubview:strongSelf.pwdTextField];
            [strongSelf.view addSubview:strongSelf.rePwdTextField];
        })
        
    }];
}

- (void)gotoForgetpaypwdViewController
{
    [self gotoPage:@"ForgetPaypasswordViewController"];
}


-(void)doSetPwd
{
    if (self.type == MimaTypeLoad) {
        [self doUpdateLoginPwd];
    }
    else if (self.type == MimaTypeJiaoyi) {
        if (self.hasPayPassword) {
            [self doUpdatePayPwd];
        }
        else {
            [self doSetPayPwd];
        }
        
    }
}

-(void)doUpdatePayPwd
{
    if ([self.oldPwdTextField canResignFirstResponder]) {
        [self.oldPwdTextField resignFirstResponder];
    }
    if ([self.pwdTextField canResignFirstResponder]) {
        [self.pwdTextField resignFirstResponder];
    }
    if ([self.rePwdTextField canResignFirstResponder]) {
        [self.rePwdTextField resignFirstResponder];
    }
    
    NSString *oldPassword = self.oldPwdTextField.text.trim;
    NSString *newPassword = self.pwdTextField.text.trim;
    NSString *rePassword = self.rePwdTextField.text.trim;
    
    NSString *psdErrorMsg = nil;
    if (![UserHelper verifyPaypwd:oldPassword error:&psdErrorMsg] ||
        ![UserHelper verifyPaypwd:newPassword error:&psdErrorMsg] ||
        ![UserHelper verifyPaypwd:rePassword error:&psdErrorMsg]) {
        [self showAlertView:psdErrorMsg];
        return;
    }
    if (![UserHelper validateTwicePwd:newPassword pwd2:rePassword error:&psdErrorMsg]) {
        [self showAlertView:psdErrorMsg];
        return;
    }
    
    
    self.nextAction = @selector(doUpdatePayPwd);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_UPDATE_API];
//    NSString *param = [NSString stringWithFormat:@"password=%@", [EncryptUtil getRSAEncryptPassword:password]];
    NSString *param = [NSString stringWithFormat:@"newPassword=%@&oldPassword=%@", [EncryptUtil getRSAEncryptPassword:newPassword], [EncryptUtil getRSAEncryptPassword:oldPassword]];
    [self showHUDIndicatorViewAtCenter:MSG_SET_PAY_PWDING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
    
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:MSG_SET_PAY_PWD_SUCCESS delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    if (IsStrEmpty(strongSelf.nextViewController)) {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                    else {
                        Class cls = NSClassFromString(self.nextViewController);
                        BaseViewController *viewController = [[cls alloc] init];
                        [strongSelf.navigationController pushViewController:viewController animated:YES];
                    }
                }
            }];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf showAlertView:errorDesc];
    }];
    
    
}

-(void)doSetPayPwd
{

    if ([self.pwdTextField canResignFirstResponder]) {
        [self.pwdTextField resignFirstResponder];
    }
    if ([self.rePwdTextField canResignFirstResponder]) {
        [self.rePwdTextField resignFirstResponder];
    }
    /*密码校验*/
    NSString *password = self.pwdTextField.text.trim;
    NSString *rePassword = self.rePwdTextField.text.trim;
    NSString *psdErrorMsg = nil;
    if (![UserHelper verifyPaypwd:password error:&psdErrorMsg] || ![UserHelper verifyPaypwd:rePassword error:&psdErrorMsg]) {
        [self showAlertView:psdErrorMsg];
        return;
    }
    if (![UserHelper validateTwicePwd:password pwd2:rePassword error:&psdErrorMsg]) {
        [self showAlertView:psdErrorMsg];
        return;
    }
    
    self.nextAction = @selector(doSetPayPwd);
    self.object1 = nil;
    self.object2 = nil;
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_SET_API];
    NSString *param = [NSString stringWithFormat:@"password=%@", [EncryptUtil getRSAEncryptPassword:password]];
    [self showHUDIndicatorViewAtCenter:MSG_SET_PAY_PWDING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:MSG_SET_PAY_PWD_SUCCESS delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    if (IsStrEmpty(strongSelf.nextViewController)) {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                    else {
                        Class cls = NSClassFromString(self.nextViewController);
                        BaseViewController *viewController = [[cls alloc] init];
                        [strongSelf.navigationController pushViewController:viewController animated:YES];
                    }
                }
            }];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf showAlertView:errorDesc];
    }];
}

-(void)doUpdateLoginPwd
{
    if ([self.pwdTextField canResignFirstResponder]) {
        [self.pwdTextField resignFirstResponder];
    }
    if ([self.rePwdTextField canResignFirstResponder]) {
        [self.rePwdTextField resignFirstResponder];
    }
    /*密码校验*/
    NSString *oldPassword = self.oldPwdTextField.text.trim;
    NSString *password = self.pwdTextField.text.trim;
    NSString *rePassword = self.rePwdTextField.text.trim;
    NSString *psdErrorMsg = nil;
    if (![UserHelper validatePassWord:oldPassword error:&psdErrorMsg] ||
        ![UserHelper validatePassWord:password error:&psdErrorMsg] ||
        ![UserHelper validatePassWord:rePassword error:&psdErrorMsg]) {
        [self showAlertView:psdErrorMsg];
        return;
    }
    
    if (![UserHelper validateTwicePwd:password pwd2:rePassword error:&psdErrorMsg]) {
        [self showAlertView:psdErrorMsg];
        return;
    }
    
    
    self.nextAction = @selector(doUpdateLoginPwd);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, LOGIN_PWD_CHANGE_API];
//    NSString *param = [NSString stringWithFormat:@"password=%@", [EncryptUtil getRSAEncryptPassword:password]];
    NSString *param = [NSString stringWithFormat:@"oldPassword=%@&newPassword=%@", [EncryptUtil getRSAEncryptPassword:oldPassword],[EncryptUtil getRSAEncryptPassword:password]];
    [self showHUDIndicatorViewAtCenter:MSG_UPDATE_LOGIN_PWD];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            [UserManager sharedInstance].storePassword = strongSelf.pwdTextField.text.trim;

//            NSString *param = [NSString stringWithFormat:@"username=%@&password=%@", [[UserManager sharedInstance].userModel username], strongSelf.pwdTextField.text.trim];
//            [[UserManager sharedInstance] setLoginRsaBody:[EncryptUtil getRSALoginParams:param]];
//            [[UserManager sharedInstance] setLoginShaBody:[param sha1]];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:MSG_UPDATE_LOGIN_PWD_SUCCESS delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    if (IsStrEmpty(strongSelf.nextViewController)) {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                    else {
                        Class cls = NSClassFromString(self.nextViewController);
                        BaseViewController *viewController = [[cls alloc] init];
                        [strongSelf.navigationController pushViewController:viewController animated:YES];
                    }
                }
            }];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf showAlertView:errorDesc];
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
