//
//  AddDiaoDetailViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AddDiaoDetailViewController.h"

@interface AddDiaoDetailViewController ()

@end

@implementation AddDiaoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    [self createMyNav];
    [self createInterface];
    // Do any additional setup after loading the view.
}
-(void)createMyNav
{
    
    UILabel *titleLabel=[self addNavTitle:CGRectMake(0, 0, 335, 49) title:self.name];
    titleLabel.textColor=[UIColor whiteColor];
    
    UIButton *saveBtn=[self addNavBtn:CGRectMake(0, 0, 40, 20) title:@"保存" target:self action:@selector(gotoSave) isLeft:NO];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    self.navigationController.navigationBar.translucent=NO;
}
-(void)createInterface
{
    
    UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(0, 10, ViewWidth, 40)];
    textField.backgroundColor=[UIColor whiteColor];
    textField.clearButtonMode=UITextFieldViewModeAlways;
    textField.font=[UIFont systemFontOfSize:13];
    textField.textAlignment=NSTextAlignmentCenter;
    textField.tag=600+self.row;
    if (self.row==0) {
        textField.placeholder=@"请填写姓名";
    }
    else if (self.row==1) {
        textField.keyboardType=UIKeyboardTypeNumberPad;
        textField.placeholder=@"请填写手机号";
    }
    
    if (self.type==DiaoTypeEdit) {
        if ([self.rightname isEqualToString:@"请填写姓名"]||[self.rightname isEqualToString:@"请填写手机号"])
        {
            textField.placeholder=self.rightname;
        }
        else
        {
            textField.text=self.rightname;
        }
    }
    
    else if (self.type==DiaoTypeAdd)
    {
        if ([self.rightname isEqualToString:@"请填写姓名"]||[self.rightname isEqualToString:@"请填写电话号码"])
        {
            textField.placeholder=self.rightname;
        }
        else
        {
            textField.text=self.rightname;
        }
    }
    [self.view addSubview:textField];
}
-(void)gotoSave
{
    
    UITextField *textfield=[self.view viewWithTag:600+self.row];
    if (self.row == 0) {
        if ([_delegate respondsToSelector:@selector(getRow:WithTitle:)]) {
            [_delegate getRow:self.row WithTitle:textfield.text];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
    NSString *searchText = textfield.text;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^1[3|4|5|7|8][0-9]\\d{8}$" options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *result = [regex firstMatchInString:searchText options:0 range:NSMakeRange(0, [searchText length])];
    if (result) {
        NSLog(@"手机号正确");
        if ([_delegate respondsToSelector:@selector(getRow:WithTitle:)]) {
            [_delegate getRow:self.row WithTitle:textfield.text];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        NSLog(@"手机号错误");
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入11位手机号"preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertView addAction:alertAction];
        [self presentViewController:alertView animated:YES completion:nil];
    }

    }
    
    
}
-(void)gotoBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
