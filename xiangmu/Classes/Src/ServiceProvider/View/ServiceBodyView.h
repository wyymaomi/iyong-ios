//
//  ServiceBodyView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarView.h"
#import "PYPhotosView.h"


//@protocol ServiceCellDelegate <NSObject>
//
//- (void)onClickImage:(NSInteger)index;
//
//@end

@class OnlineServiceTableViewCell;
@class ServiceProviderViewModel;

@interface ServiceBodyView : UIView

@property (nonatomic, strong) ServiceProviderViewModel *viewModel;

@property (nonatomic, strong) UIView *separatorLineView; // 公司基本信息
@property (nonatomic, strong) UIView *businessView;// 经营范围

@property (nonatomic, strong) StarView *starView;

@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *businessLabel;
@property (nonatomic, strong) UILabel *companyLabel;

@property (nonatomic, strong) PYPhotosView *photosView;
//@property (nonatomic, strong) UIView *photosView;
//@property (nonatomic, strong) UIImageView *photoImageView1;
//@property (nonatomic, strong) UIImageView *photoImageView2;
//@property (nonatomic, strong) UIImageView *photoImageView3;
//@property (nonatomic, strong) UIImageView *photoImageView4;

@property (nonatomic, weak) OnlineServiceTableViewCell *cell;

@end
