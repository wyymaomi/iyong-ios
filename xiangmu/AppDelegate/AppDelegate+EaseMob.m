/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */

#import "AppDelegate+EaseMob.h"
//#import "AppDelegate+EaseMobDebug.h"
//#import "AppDelegate+Parse.h"

#import "EMNavigationController.h"
#import "LoginViewController.h"
#import "ChatDemoHelper.h"
#import "MBProgressHUD.h"

/**
 *  本类中做了EaseMob初始化和推送等操作
 */

@implementation AppDelegate (EaseMob)

-(void)setupEaseMob:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //#warning Init SDK，detail in AppDelegate+EaseMob.m
    //#warning SDK注册 APNS文件的名字, 需要与后台上传证书时的名字一一对应
    NSString *apnsCertName = nil;
#if DEBUG
    apnsCertName = @"aps_ios_developer";
#else
    apnsCertName = @"aps_ios_distribution";
#endif
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *appkey = [ud stringForKey:@"identifier_appkey"];
    if (!appkey) {
        appkey = EaseMobAppKey;
        [ud setObject:appkey forKey:@"identifier_appkey"];
    }
    
    [self easemobApplication:application
didFinishLaunchingWithOptions:launchOptions
                      appkey:appkey
                apnsCertName:apnsCertName
                 otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:NO]}];
}

//- (void)initEaseMob
//{
//#warning Init SDK，detail in AppDelegate+EaseMob.m
//#warning SDK注册 APNS文件的名字, 需要与后台上传证书时的名字一一对应
//    NSString *apnsCertName = nil;
//#if DEBUG
//    apnsCertName = @"chatdemoui_dev";
//#else
//    apnsCertName = @"chatdemoui";
//#endif
//    
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    NSString *appkey = [ud stringForKey:@"1122170328115534#iyong"];
//    if (!appkey) {
//        appkey = EaseMobAppKey;
//        [ud setObject:appkey forKey:@"1122170328115534#iyong"];
//    }
//    
//    [self easemobApplication:application
//didFinishLaunchingWithOptions:launchOptions
//                      appkey:appkey
//                apnsCertName:apnsCertName
//                 otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES]}];
//}

- (void)easemobApplication:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
                    appkey:(NSString *)appkey
              apnsCertName:(NSString *)apnsCertName
               otherConfig:(NSDictionary *)otherConfig
{
    //注册登录状态监听
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginStateChange:)
                                                 name:KNOTIFICATION_LOGINCHANGE
                                               object:nil];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL isHttpsOnly = [ud boolForKey:@"identifier_httpsonly"];
    
    [[EaseSDKHelper shareHelper] hyphenateApplication:application
                    didFinishLaunchingWithOptions:launchOptions
                                           appkey:appkey
                                     apnsCertName:apnsCertName
                                      otherConfig:@{@"httpsOnly":[NSNumber numberWithBool:isHttpsOnly], kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:NO],@"easeSandBox":/*[NSNumber numberWithBool:[self isSpecifyServer]]*/@1}];
    
    [ChatDemoHelper shareHelper];
    
    BOOL isAutoLogin = [EMClient sharedClient].isAutoLogin;
    if (isAutoLogin){
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@YES];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
    }
    
//    [self savePushOptions];
}

//- (void)savePushOptions
//{
////    BOOL isUpdate = NO;
////    EMPushOptions *options = [[EMClient sharedClient] pushOptions];
////    if (_pushDisplayStyle != options.displayStyle) {
////        options.displayStyle = _pushDisplayStyle;
////        isUpdate = YES;
////    }
////    
////    if (_nickName && _nickName.length > 0 && ![_nickName isEqualToString:options.displayName])
////    {
////        options.displayName = _nickName;
////        isUpdate = YES;
////    }
////    if (options.noDisturbingStartH != _noDisturbingStart || options.noDisturbingEndH != _noDisturbingEnd){
////        isUpdate = YES;
////        options.noDisturbStatus = _noDisturbingStatus;
////        options.noDisturbingStartH = _noDisturbingStart;
////        options.noDisturbingEndH = _noDisturbingEnd;
////    }
//    
//    EMPushOptions *options = [[EMClient sharedClient] pushOptions];
//    options.displayStyle = EMPushDisplayStyleMessageSummary;
////    options.displayStyle =EMPushOptions *options = [[EMClient sharedClient] pushOptions];
////    options.displayStyle == EMPushDisplayStyleMessageSummary;// 显示消息内容
//    // options.displayStyle == EMPushDisplayStyleSimpleBanner // 显示“您有一条新消息”
//    EMError *error = [[EMClient sharedClient] updatePushOptionsToServer]; // 更新配置到服务器，该方法为同步方法，如果需要，请放到单独线程
//    if(!error) {
//        DLog(@"updateEMPushOptions success");
//        // 成功
//    }else {
//        DLog(@"updateEMPushOptions failure");
//        // 失败
//    }
//
//    
////    __weak typeof(self) weakself = self;
////    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
////        EMError *error = nil;
////        if (isUpdate) {
////            error = [[EMClient sharedClient] updatePushOptionsToServer];
////        }
////        dispatch_async(dispatch_get_main_queue(), ^{
////            if (!error) {
////                [weakself.navigationController popViewControllerAnimated:YES];
////            } else {
////                [weakself showHint:[NSString stringWithFormat:@"保存失败-error:%@",error.errorDescription]];
////            }
////        });
////    });
//}

- (void)easemobApplication:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[EaseSDKHelper shareHelper] hyphenateApplication:application didReceiveRemoteNotification:userInfo];
}

#pragma mark - App Delegate

// 将得到的deviceToken传给SDK
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[EMClient sharedClient] bindDeviceToken:deviceToken];
    });
}

// 注册deviceToken失败，此处失败，与环信SDK无关，一般是您的环境配置或者证书配置有误
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"apns.failToRegisterApns", Fail to register apns)
                                                    message:error.description
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - login changed

- (void)loginStateChange:(NSNotification *)notification
{
    BOOL loginSuccess = [notification.object boolValue];
    EMNavigationController *navigationController = nil;
    if (loginSuccess) {//登陆成功加载主窗口控制器
        //加载申请通知的数据
//        [[ApplyViewController shareController] loadDataSourceFromLocalDB];
//        if (self.mainController == nil) {
//            self.mainController = [[MainViewController alloc] init];
//            navigationController = [[EMNavigationController alloc] initWithRootViewController:self.mainController];
//        }else{
//            navigationController  = (EMNavigationController *)self.mainController.navigationController;
//        }
        // 环信UIdemo中有用到Parse，您的项目中不需要添加，可忽略此处
//        [self initParse];
        
        
        [ChatDemoHelper shareHelper].mainVC = self.mainTabbarController;
        
//        [[ChatDemoHelper shareHelper] asyncGroupFromServer];
        [[ChatDemoHelper shareHelper] asyncConversationFromDB];
        [[ChatDemoHelper shareHelper] asyncPushOptions];
    }
    else{//登陆失败加载登陆页面控制器
//        if (self.mainController) {
//            [self.mainController.navigationController popToRootViewControllerAnimated:NO];
//        }
//        self.mainController = nil;
//        [ChatDemoHelper shareHelper].mainVC = nil;
        
//        LoginViewController *loginController = [[LoginViewController alloc] init];
//        navigationController = [[EMNavigationController alloc] initWithRootViewController:loginController];
//        [self clearParse];
    }
    
    //设置7.0以下的导航栏
//    if ([UIDevice currentDevice].systemVersion.floatValue < 7.0){
//        navigationController.navigationBar.barStyle = UIBarStyleDefault;
//        [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titleBar"]
//                                                 forBarMetrics:UIBarMetricsDefault];
//        [navigationController.navigationBar.layer setMasksToBounds:YES];
//    }
//    
//    navigationController.navigationBar.accessibilityIdentifier = @"navigationbar";
//    self.window.rootViewController = navigationController;
}

#pragma mark - EMPushManagerDelegateDevice

// 打印收到的apns信息
-(void)didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSError *parseError = nil;
    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                        options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *str =  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"apns.content", @"Apns content")
                                                    message:str
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                                          otherButtonTitles:nil];
    [alert show];
    
}

@end
