//
//  DispatchOrderHeaderSectionView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderHeaderSectionView;
@class OrderStatusHeader;

@protocol OrderHeaderSectionViewDelegate <NSObject>

@optional
-(void)sectionHeaderView:(OrderHeaderSectionView*)sectionHeaderView sectionClosed:(NSInteger)section;
-(void)sectionHeaderView:(OrderHeaderSectionView*)sectionHeaderView sectionOpened:(NSInteger)section;

@end


@interface OrderHeaderSectionView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIImageView *indicatorImageView;
@property (weak, nonatomic) id<OrderHeaderSectionViewDelegate> delegate;

@property (nonatomic, assign) BOOL isOpen;

//+ (instancetype)customView;
-(void)initDataWithHeaderStatus:(NSInteger)section
                   headerStatus:(OrderStatusHeader*)headerStatus;
@end


