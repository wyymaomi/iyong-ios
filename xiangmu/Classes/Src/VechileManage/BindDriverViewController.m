//
//  BindDriverViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BindDriverViewController.h"
#import "UserHelper.h"

@interface BindDriverViewController ()

@end

@implementation BindDriverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"绑定司机";
    self.mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    [self.mobileTextField becomeFirstResponder];
    // Do any additional setup after loading the view from its nib.
//    [self.okButton okStyle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBindDriver:(id)sender {
    
    NSString *mobile = self.mobileTextField.text.trim;
    NSString *errorMsg;
    
    if (![UserHelper validatePhoneNum:mobile error:&errorMsg]) {
        [MsgToolBox showToast:errorMsg];
        return;
    }
    
    NSString *params = [NSString stringWithFormat:@"mobile=%@", mobile];
    [self showHUDIndicatorViewAtCenter:@"正在绑定，请稍候"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(BIND_DRIVER_API) requestMethod:POST params:params success:^(NSDictionary * responseData) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            [MsgToolBox showToast:@"绑定请求已发送，请等待该司机处理"];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
    } andFailure:^(NSString *errorDesc) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        [MsgToolBox showToast:errorDesc];
        
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
