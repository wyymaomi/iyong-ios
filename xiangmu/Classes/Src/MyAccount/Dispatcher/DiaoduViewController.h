//
//  DiaoduViewController.h
//  xiangmu
//
//  Created by David kim on 16/3/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "AddDiaoViewController.h"
#import "EditDiaoduViewController.h"


@interface DiaoduViewController : LFNavController<Diaodu,EditDiaodu>
@property(nonatomic,strong)UITableView *orderTabelView;
@end
