//
//  ServiceToolBarView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ServiceToolBarView.h"
#import "ServiceProviderViewModel.h"
#import "OnlineServiceModel.h"

@interface ServiceToolBarView ()



@end

@implementation ServiceToolBarView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.likeView];
        [self addSubview:self.favoriteView];
        [self addSubview:self.telephoneView];
        
    }
    return self;
}

//- (void)setViewModel:(ServiceProviderViewModel *)viewModel
//{
//    _viewModel = viewModel;
//    
//    self.likeView.frame = _viewModel.toolLikeFrame;
//    self.favoriteView.frame = _viewModel.toolFavoriteFrame;
//    self.telephoneView.frame = _viewModel.toolTelFrame;
//}

#pragma mark - getter and setter

- (UIView*)telephoneView
{
    if (_telephoneView == nil) {
        _telephoneView = [UIView new];
        _telephoneView.backgroundColor = [UIColor whiteColor];
        [_telephoneView addSubview:self.telephoneButton];
    }
    return _telephoneView;
}

-(UIButton*)telephoneButton
{
    if (_telephoneButton == nil) {
        _telephoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_telephoneButton setBackgroundImage:[UIImage imageNamed:@"icon_call"] forState:UIControlStateNormal];
//        [_telephoneButton setTitle:@"通话" forState:UIControlStateNormal];
//        [_telephoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        _telephoneButton.titleLabel.font = FONT(14);
//        [_telephoneButton setImage:[UIImage imageNamed:@"icon_partner_dial"] forState:UIControlStateNormal];
//        _telephoneButton.cornerRadius = 5;
    }
    return _telephoneButton;
}

// 收藏

- (UIView*)favoriteView
{
    if (_favoriteView == nil) {
        _favoriteView = [UIView new];
        _favoriteView.backgroundColor = [UIColor whiteColor];
        
        [_favoriteView addSubview:self.favoriteImageView];
        [_favoriteView addSubview:self.favoriteLabel];
        
    }
    return _favoriteView;
}

- (UIImageView *)favoriteImageView
{
    if (_favoriteImageView == nil) {
        _favoriteImageView = [UIImageView new];
        _favoriteImageView.image = [UIImage imageNamed:@"icon_gray_star"];
    }
    return _favoriteImageView;
}

- (UILabel*)favoriteLabel
{
    if (_favoriteLabel == nil) {
        _favoriteLabel = [[UILabel alloc] init];
        
        _favoriteLabel.font = FONT(10);
        _favoriteLabel.textColor = [UIColor blackColor];
        _favoriteLabel.textAlignment = UITextAlignmentCenter;
        
    }
    
    return _favoriteLabel;
}

// 点赞

- (UIView*)likeView
{
    if (_likeView == nil) {
        _likeView = [[UIView alloc] init];
        
        _likeView.backgroundColor = [UIColor whiteColor];
        
        [_likeView addSubview:self.likeImageView];
        [_likeView addSubview:self.likeNumsLabel];
        
    }
    return _likeView;
}

- (UIImageView*)likeImageView
{
    if (_likeImageView == nil) {
        _likeImageView = [[UIImageView alloc] init];
        _likeImageView.image = [UIImage imageNamed:@"icon_heart"];
    }
    
    return _likeImageView;
}

- (UILabel*)likeNumsLabel
{
    if (_likeNumsLabel == nil) {
        _likeNumsLabel = [[UILabel alloc] init];
//        _likeNumsLabel.textColor = [UIColor blueColor];
        _likeNumsLabel.font = FONT(13);
        _likeNumsLabel.textAlignment = UITextAlignmentCenter;
    }
    
    return _likeNumsLabel;
}


- (void)setViewModel:(ServiceProviderViewModel *)viewModel
{
    _viewModel = viewModel;

    self.favoriteImageView.tag = 1000;
    self.favoriteView.frame = viewModel.toolFavoriteFrame;
    self.favoriteImageView.frame = CGRectMake((self.favoriteView.frame.size.width-18)/2, 0, 18, 18);
    self.favoriteLabel.frame = CGRectMake(0, 17, self.favoriteView.frame.size.width, 21);
    self.favoriteLabel.text = @"收藏";
    NSString *favoriteImageName = [viewModel.serviceModel.collection boolValue]?@"icon_star":@"icon_gray_star";
    self.favoriteImageView.image = [UIImage imageNamed:favoriteImageName];
    
    self.telephoneView.frame = viewModel.toolTelFrame;
    NSInteger telButtonWidth = 55;
    NSInteger telButtonHeight = 20;
    self.telephoneButton.frame = CGRectMake((self.telephoneView.frame.size.width-telButtonWidth)/2, (self.telephoneView.frame.size.height-telButtonHeight)/2, telButtonWidth, telButtonHeight);
    
    self.likeView.frame = viewModel.toolLikeFrame;
    self.likeImageView.frame = CGRectMake((self.likeView.frame.size.width-18)/2, 17, 18, 18);
    self.likeNumsLabel.frame = CGRectMake(0,0,self.likeView.frame.size.width,21);
    self.likeNumsLabel.text = _viewModel.serviceModel.thumbsQuantity;
    self.likeNumsLabel.tag = 2000;
    self.likeImageView.tag = 2001;
    self.likeImageView.image = [_viewModel.serviceModel.thumbsUp boolValue] ? [UIImage imageNamed:@"icon_heart"] : [UIImage imageNamed:@"icon_heart2"];
//    NSDate *lastLikeDateTime = [NSDate dateWithTimeIntervalSince1970:[viewModel.serviceModel.thumbsTime doubleValue]/1000];
//    NSTimeInterval interval = [lastLikeDateTime timeIntervalSinceDate:[NSDate date]];
//    if (interval <  -24 * 60 * 60) {
//        self.likeImageView.image = [UIImage imageNamed:@"icon_heart2"];
//    }
//    else {
//        self.likeImageView.image = [UIImage imageNamed:@"icon_heart"];
//    }
    
    
    
}

@end
