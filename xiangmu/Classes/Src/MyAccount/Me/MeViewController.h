//
//  MeViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/9.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface MeViewController : BaseViewController

//-(void)updateUnreadMessage:(NSUInteger)count;

- (void)setupUnreadMessageCount;

@property (nonatomic, strong) NSString *financeUrl;

@end
