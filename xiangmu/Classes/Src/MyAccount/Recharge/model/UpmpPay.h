//
//  UpmpPay.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"
#import "PayModelAdapterProtocol.h"

#pragma mark - UPMP支付相关信息
static NSString *const UPMP_MD5_CERT = @"vsFDYzlBkbh61FkkJ6gZwVQORxHD5Jq79Oz54Q93s9v7H13Z3QH61SUqSdWGbST91n4oaEbAGs8eDQUceKNY48Vug3CNr83t2bOlNG0plpAZKL2WNTx11YaaBpbxsd7a";
static NSString *const UPMP_DES_KEY = @"wxqlmy75s0awSyZUPo2eqTPN";
static NSString *const UPMP_DES_IV = @"jxhVb06L";
static NSString *const UPMP_MER_CODE = @"181046"; // 商户号
static NSString *const UPMP_ACC_CODE = @"1810460012"; // 交易账户号


@interface UpmpPay : BaseHttpMessage

@property(nonatomic,strong)NSString* merCode;
@property(nonatomic,strong)NSString* accCode;
@property(nonatomic,strong)NSString* merBillNo;
@property(nonatomic,strong)NSString* ccyCode;
@property(nonatomic,strong)NSString* tranAmt;
@property(nonatomic,strong)NSString* requestTime;
@property(nonatomic,strong)NSString* ordPerVal;
@property(nonatomic,strong)NSString* orderEncodeType;
@property(nonatomic,strong)NSString* mobileDeviceType;
@property(nonatomic,strong)NSString* sign;
@property(nonatomic,strong)NSString* merNoticeUrl;
@property(nonatomic,strong)NSString* orderDesc;
@property(nonatomic,strong)NSString* bankCard;
@property(nonatomic,strong)NSString* prdCode;


@end

@protocol UpmpPayProtocol <NSObject>

-(NSString*)generateSignString:(UpmpPay*)upmpPay merRequestDes:(NSString**)merRequestDes;

@end

@interface UpmppayOrder : NSObject<PayModelAdapterProtocol, UpmpPayProtocol>

-(UpmpPay*)getUpmpData:(NSString*)billNo tranAmt:(NSString*)tranAmt bankcardNo:(NSString*)bankcardNo;

@end
