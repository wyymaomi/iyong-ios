//
//  RecommendViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RecommendViewController.h"
#import "ServiceProviderCollectionViewCell.h"
#import "VechileExperienceCollectionViewCell.h"

@interface RecommendViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionView *servicerCollectionView;
@property (nonatomic, strong) UICollectionView *experienceCollectionView;
@property (weak, nonatomic) IBOutlet UIView *serviceProviderLoadMoreView;
@property (weak, nonatomic) IBOutlet UIView *experienceLoadMoreView;

@end

static NSString *const VechileExperienceCellIdentifier = @"VechileExperienceCollectionViewCell";
static NSString *const ServiceProviderCellIdentifier = @"serviceProviderCollectionViewCell";

@implementation RecommendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    [self initViews];
    
    [self.serviceProviderLoadMoreView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(serviceProviderLoadMore)]];
    [self.experienceLoadMoreView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(experienceLoadMore)]];
}

- (void)initViews
{
//    self.servicerCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 50, ViewWidth, 125) collectionViewLayout:<#(nonnull UICollectionViewLayout *)#>]
    
    [self initCollectionView];

    
}

- (void)initCollectionView
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(200, 120);
    flowLayout.headerReferenceSize = CGSizeMake(0, 30);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    _servicerCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(12, 50, ViewWidth-24, 130) collectionViewLayout:flowLayout];
    _servicerCollectionView.backgroundColor = [UIColor whiteColor];
    _servicerCollectionView.showsHorizontalScrollIndicator = NO;
    _servicerCollectionView.delegate = self;
    _servicerCollectionView.dataSource = self;
    _servicerCollectionView.tag = RecommendTypeServiceProvider;
    [_servicerCollectionView registerNib:[UINib nibWithNibName:@"ServiceProviderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:ServiceProviderCellIdentifier];
    [self.view addSubview:_servicerCollectionView];
    
    
    _experienceCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(12, 216, ViewWidth-24, 130) collectionViewLayout:flowLayout];
    _experienceCollectionView.backgroundColor = [UIColor whiteColor];
    _experienceCollectionView.showsHorizontalScrollIndicator = NO;
    _experienceCollectionView.delegate = self;
    _experienceCollectionView.dataSource = self;
    _experienceCollectionView.tag = RecommendTypeUserExperience;
    [_experienceCollectionView registerNib:[UINib nibWithNibName:@"VechileExperienceCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:VechileExperienceCellIdentifier];
    [self.view addSubview:_experienceCollectionView];
}

- (void)serviceProviderLoadMore
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"serviceProviderLoadMoreNotification" object:nil];
}

- (void)experienceLoadMore
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"experienceLoadMoreNotification" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionView Delegate method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == RecommendTypeServiceProvider) {
        ServiceProviderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ServiceProviderCellIdentifier forIndexPath:indexPath];
        return cell;
    }
    if (collectionView.tag == RecommendTypeUserExperience) {
        VechileExperienceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VechileExperienceCellIdentifier forIndexPath:indexPath];
        return cell;
    }
    
    return nil;
    
}

@end
