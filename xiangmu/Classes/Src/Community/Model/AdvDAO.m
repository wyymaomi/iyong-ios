//
//  AdvDAO.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AdvDAO.h"
#import "AdvModel.h"

@implementation AdvDAO

- (NSMutableArray*)getAllAdv;
{
    __block NSMutableArray *array = nil;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db) {
        NSString *sql = [NSString stringWithFormat:@"select * from info_adv ORDER BY updateTime DESC"];
        FMResultSet *rs = [db executeQuery:sql];
        if (!rs) {
            [rs close];
            return;
        }
        
        array = [NSMutableArray new];
        while ([rs next]) {
            AdvModel *advModel = [AdvModel new];
            advModel.id = [rs stringForColumn:@"id"];
            advModel.linkUrl = [rs stringForColumn:@"linkUrl"];
            advModel.imageUrl = [rs stringForColumn:@"imageUrl"];
            advModel.sort = [NSNumber numberWithInteger:[rs intForColumn:@"sort"]];
            advModel.type = [NSNumber numberWithInteger:[rs intForColumn:@"type"]];
            advModel.updateTime = [NSNumber numberWithDouble:[rs doubleForColumn:@"updateTime"]];
            [array addObject:advModel];
        }
        
        
    }];
    return array;
}

- (BOOL)updateAdvList:(NSArray*)advList;
{
    __block BOOL isSuccess = NO;
    
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        // 删除本地所有数据库中的广告
        NSString *sql = [NSString stringWithFormat:@"delete from info_adv"];
        isSuccess = [db executeUpdate:sql];
        
        if (isSuccess) {
            
            for (AdvModel *advModel in advList) {
                
                if (advModel == nil || IsStrEmpty(advModel.id)) {
                    continue;
                }
                
                NSString *sql = @"insert into info_adv(id,imageUrl,linkUrl,sort,type,updateTime) values(?,?,?,?,?,?)";
                isSuccess = [db executeUpdate:sql, advModel.id, advModel.imageUrl, advModel.linkUrl, advModel.sort, advModel.type, advModel.updateTime];
                
                
            }

        }
        
    }];
    return isSuccess;
}

@end
