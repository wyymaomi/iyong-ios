//
//  ProviderDetailTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProviderDetailViewModel.h"
#import "StarView.h"
//@class StarView;

@interface ProviderDetailTableViewCell : UITableViewCell

@property (nonatomic, strong) ProviderDetailViewModel *viewModel;

@property (nonatomic, strong) UILabel *companyLabel;
@property (nonatomic, strong) UILabel *browserNumberLabel;
@property (nonatomic, strong) UILabel *commentNumberLabel;
@property (nonatomic, strong) UILabel *likeNumberLabel;
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UIImageView *browserNumberImageView;
@property (nonatomic, strong) UIImageView *commentNumberImageView;

@property (nonatomic, strong) UIButton *likeButton;
@property (nonatomic, strong) StarView *scoreView;
//@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
//@property (weak, nonatomic) IBOutlet UILabel *browserNumberLabel;
//@property (weak, nonatomic) IBOutlet UILabel *favoriteNumberLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
//@property (weak, nonatomic) IBOutlet StarView *scoreView;
//@property (weak, nonatomic) IBOutlet UIImageView *likeImageView;

//- (instancetype) initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@property (nonatomic, strong) UIView *lineView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+(CGFloat)getCellHeight;

@end
