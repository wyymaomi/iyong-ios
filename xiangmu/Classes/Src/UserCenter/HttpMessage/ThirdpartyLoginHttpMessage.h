//
//  ThirdpartyLoginHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface ThirdpartyLoginHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * nickname;// 用户名(昵称？)
@property (nonatomic, strong) NSString * logoUrl;// 头像地址
@property (nonatomic, strong) NSString * openid;
@property (nonatomic, strong) NSString * accessToken;
@property (nonatomic, strong) NSString * refreshToken;
@property (nonatomic, strong) NSString * gender;

@end
