//
//  DispatchCompanyTableViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"
#import "PartnerModel.h"

@protocol MyPartnerDelegate <NSObject>

-(void)onGetDispatchCompany:(PartnerModel*)partnerModel;// 外派
-(void)onSelfDispatch;// 自派

@end


@interface MyPartnerListViewController : BaseTableViewController

@property (nonatomic, weak) id<MyPartnerDelegate> companyDelegate;

@end
