//
//  NewRechargeViewController+ThirdPay.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/26.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "NewRechargeViewController+ThirdPay.h"
#import "RechargeService.h"

@implementation NewRechargeViewController (ThirdPay)

-(void)saveRechargeOrder:(NSUInteger)rechargeMode rechargeAmount:(CGFloat)rechargeAmount;
{
    NSString *params = [NSString stringWithFormat:@"payMode=%lu&amount=%.f", (unsigned long)rechargeMode, rechargeAmount];
    
    [self showHUDIndicatorViewAtCenter:@"正在充值，请稍候..."];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(SAVE_RECHARGE_ORDER) requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSUInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            if (rechargeMode == RechargeTypeAlipay) {
                
                if ([responseData[@"data"] isKindOfClass:[NSString class]]) {
                    NSString *data = responseData[@"data"];
                    [[RechargeService sharedInstance] doAlipayPay:data];
                }
                
            }
            else if (rechargeMode == RechargeTypeWechat) {
                
                if ([responseData[@"data"] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *data = responseData[@"data"];
                    [[RechargeService sharedInstance] bizPay:data];
                }
            }
            else if(rechargeMode == RechargeTypeIPS){
                
                if ([responseData[@"data"] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *data = responseData[@"data"];
                    NSString *notifyUrl = data[@"notifyUrl"];
                    NSString *orderCode = data[@"orderCode"];
                    [weakSelf upmpPay:orderCode notifyUrl:notifyUrl];
                }
                
            }
            
        }
        else {
            [MsgToolBox showToast:@"充值失败，请重试"];
        }
        
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        [MsgToolBox showToast:errorDesc];
        
    }];
}

@end
