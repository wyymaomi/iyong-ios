//
//  SalesFilterQueryViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SalesFilterQueryViewController.h"
#import "SalesCarFilterViewController.h"
#import "SalesFilterListViewController.h"
#import "ActionSheetDatePicker.h"
#import "DateUtil.h"
#import "SalesModel.h"

@interface SalesFilterQueryViewController ()<UITableViewDelegate, UITableViewDataSource, SalesCarFilterDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *endTime;
//@property (strong, nonatomic) NSString *carNumber;
@property (strong, nonatomic) CarModel *carData;
@property (assign, nonatomic) BOOL isOuterDispatch;
@property (assign, nonatomic) BOOL isAll;// 是否全部；
@end

@implementation SalesFilterQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"营业查询";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.okButton blueStyle];
    [self.okButton addTarget:self action:@selector(onButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self initData];
    
//    NSDate *date = [NSDate date];
//    self.startTime
}

-(void)initData
{
    self.isAll = YES;
    self.startTime = nil;
    self.endTime = nil;
//    NSDate *date = [NSDate date];
//    self.startTime = [date stringWithDateFormat:@"yyyy-MM-dd"];
//    self.endTime = [date stringWithDateFormat:@"yyyy-MM-dd"];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onButtonClick:(UIButton*)button
{
    if (IsStrEmpty(self.startTime) || IsStrEmpty(self.endTime)) {
        [self showAlertView:@"请输入开始日期和结束日期"];
        return;
    }
    SalesFilterListViewController *vc = [[SalesFilterListViewController alloc] init];
    vc.isAll = self.isAll;
    vc.carData = self.carData;
    vc.isOuterDispatch = self.isOuterDispatch;
    vc.startDateTime = self.startTime;
    vc.endDateTime = self.endTime;
    [self.navigationController pushViewController:vc animated:YES];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
//    return self.dataList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0001f;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
////    if (section == self.dataList.count -1) {
//        return 0.00001f;
////    }
////    return 10.0f;
//}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    return [[UIView alloc] init];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    BillTableViewCell *cell = [BillTableViewCell cellWithTableView:tableView];
//    SalesModel *model = self.dataList[indexPath.section];
//    [cell initSalesData:model];
//    return cell;
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = FONT(14);
    }
    if (indexPath.row == 0) {
        if (IsStrEmpty(self.startTime)) {
            cell.textLabel.text = @"请输入开始日期";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
        else {
            cell.textLabel.text = self.startTime;
            cell.textLabel.textColor = [UIColor blackColor];
        }
        cell.imageView.image = [UIImage imageNamed:@"icon_startTime"];
    }
    if (indexPath.row == 1) {
        if (IsStrEmpty(self.endTime)) {
            cell.textLabel.text = @"请输入结束日期";
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
        else {
            cell.textLabel.text = self.endTime;
            cell.textLabel.textColor = [UIColor blackColor];
        }
        cell.imageView.image = [UIImage imageNamed:@"icon_endTime"];
    }
    if (indexPath.row == 2) {
        if (self.isAll) {
            cell.textLabel.text = @"全部";
        }
        else {
            if (self.isOuterDispatch) {
                cell.textLabel.text = @"外派";
            }
            else {
                if (self.carData == nil) {
                    cell.textLabel.text = @"自派";
                }
                else {
                    cell.textLabel.text = self.carData.number;
                }
            }
        }
        cell.imageView.hidden = YES;
        CGFloat textLabelLeft = cell.imageView.frame.origin.x;
        cell.textLabel.frame = CGRectMake(textLabelLeft, cell.textLabel.origin.y, cell.contentView.size.width-textLabelLeft*2, cell.textLabel.frame.size.height);
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        [self selectStartTime:cell.textLabel];
    }
    if (indexPath.row == 1) {
        [self selectEndTime:cell.textLabel];
    }
    if (indexPath.row == 2) {
        [self selectCar];
    }
}

//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

//行将显示的时候调用
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}

#pragma mark - 选择起shi
-(void)selectStartTime:(id)sender
{
    WeakSelf
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        StrongSelf
        if ([selectedDate isKindOfClass:[NSDate class]]) {
            NSDate *date = selectedDate;
            strongSelf.startTime = [date stringWithDateFormat:@"yyyy-MM-dd"];
            [strongSelf.tableView reloadData];
            
            NSString *dateStr1 = strongSelf.startTime;
            NSString *dateStr2 = strongSelf.endTime;
            NSString *errorMsg;
            if (![DateUtil isLessThanSixMonths:dateStr1 dateStr2:dateStr2 error:&errorMsg]) {
                [strongSelf showAlertView:errorMsg];
            }
//            else {
////                [self refreshData:NO];
////                [self refreshSummaryData:@0];
//            }
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:sender];
}

-(void)selectEndTime:(id)sender
{
    WeakSelf
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date]minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        StrongSelf
        if ([selectedDate isKindOfClass:[NSDate class]]) {
            NSDate *date = selectedDate;
            strongSelf.endTime = [date stringWithDateFormat:@"yyyy-MM-dd"];
            [strongSelf.tableView reloadData];
//            if (IsStrEmpty(strongSelf.startTimeLabel.text)) {
//                
//            }
//            else {
                NSString *dateStr1 = strongSelf.startTime;
                NSString *dateStr2 = strongSelf.endTime;
                NSString *errorMsg;
                if (![DateUtil isLessThanSixMonths:dateStr1 dateStr2:dateStr2 error:&errorMsg]) {
                    [strongSelf showAlertView:errorMsg];
                    return;
                }
//                else {
//                    [self refreshData:NO];
//                    [self refreshSummaryData:@0];
//                }
//            }
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:sender];
}

-(void)selectCar
{
//    if (IsStrEmpty(self.startTime) || IsStrEmpty(self.endTime)) {
//        [self showAlertView:@"请输入开始日期和结束日期"];
//        return;
//    }
    SalesCarFilterViewController *viewController = [[SalesCarFilterViewController alloc] init];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)onGetCarData:(BOOL)isOuterDispatch carData:(CarModel*)carData;
{
    self.isAll = NO;
    self.isOuterDispatch = isOuterDispatch;
    self.carData = carData;
    [self.tableView reloadData];
}


@end
