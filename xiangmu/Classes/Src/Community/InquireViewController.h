//
//  InquireViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/12.
//  Copyright © 2016年 湛思科技. All rights reserved.
//
#import "BaseViewController.h"


static const NSString *KEY_INQUIRE_TITLE = @"title";
static const NSString *KEY_INQUIRE_TEXTFIELD_PLACEHOLDER = @"placeholder";
static const NSString *KEY_INQUIRE_TEXTFIELD_TAG = @"tag";
static const NSString *KEY_INQUIRE_TEXTFIELD_TEXT = @"text";

@interface InquireViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *inquireButton;

@end
