//
//  BaseTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "PullRefreshTableView.h"
#import "MJRefresh.h"

@implementation PullRefreshTableView

-(id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    if (self) {
        
        [self setupView];
        
    }
    return self;
}

- (void)setupView
{
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.backgroundColor = [UIColor clearColor];
    self.showsVerticalScrollIndicator = YES;
    self.showsHorizontalScrollIndicator = NO;
    self.bounces = YES;
    self.scrollEnabled = YES;
    self.tableFooterView = [[UIView alloc] init];
    
    //        self.hasHeaderRefresh = YES;
    //        self.hasFooterRefresh = YES;
    
    [self setupHeaderView];
    [self setupFooterView];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
     
        [self setupView];
        
    }
    
    return self;
}

//-(void)setHasFooterRefresh:(BOOL)hasFooterRefresh
//{
//    if (hasFooterRefresh) {
//        [self setupFooterView];
//    }
//}

//-(void)setHasHeaderRefresh:(BOOL)hasHeaderRefresh
//{
//    if (hasHeaderRefresh) {
//        [self setupHeaderView];
//    }
//}

- (void)setupHeaderView {
//    if (!_hasHeaderRefresh) {
//        return;
//    }
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefreshing)];
    //    header.lastUpdatedTimeLabel.hidden = YES;
    //    header.stateLabel.font = [UIFont fontWithName:FontName size:13];
    self.mj_header = header;
}

-(void)headerRefreshing
{
    if (self.pullRefreshDelegate && [self.pullRefreshDelegate respondsToSelector:@selector(onHeaderRefresh)]) {
        [self.pullRefreshDelegate onHeaderRefresh];
    }
}

- (void)setupFooterView {
//    if (!_hasFooterRefresh) {
//        return;
//    }
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.mj_footer = footer;
    self.mj_footer.automaticallyHidden = YES;
}

-(void)loadMoreData
{
    if (self.pullRefreshDelegate && [self.pullRefreshDelegate respondsToSelector:@selector(onFooterRefresh)]) {
        [self.pullRefreshDelegate onFooterRefresh];
    }
}

-(void)beginHeaderRefresh
{
    [self.mj_header beginRefreshing];
}

-(void)endHeaderRefresh
{
    [self.mj_header endRefreshing];
}

-(void)beginFooterRefresh;
{
    [self.mj_footer beginRefreshing];
}

-(void)endFooterRefresh;
{
    [self.mj_footer endRefreshing];
}

-(void)setFooterHidden:(BOOL)footerHidden
{
    self.mj_footer.hidden = footerHidden;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
