//
//  AppDelegate.h
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "GesturePasswordController.h"
#import "OrderCountModel.h"
#import "CheckUpdateService.h"
#import "NetworkReach.h"
//#import "WXApi.h"
#import "WXApiManager.h"

#import <CloudPushSDK/CloudPushSDK.h>

// iOS 10 notification
#import <UserNotifications/UserNotifications.h>

#import "TabbarManager.h"
//#import "NewMainTabBarController.h"
//#import "MainTabBarController.h"
#import "GuideViewController.h"
#import "GestureLoginViewController.h"
#import "LoginViewController.h"
#import "GuideViewController.h"
#import "PartnerModel.h"
#import "MsgToolBox.h"
#import <Foundation/Foundation.h>
#import "AppConfig.h"
#import "LocationService.h"
#import "MyUtil.h"
#import "HomepageViewController.h"
#import "AccountInfoViewController.h"
#import "UIRoute.h"
#import "AuthorizationUtil.h"
#import "SDKManager.h"
#import <Bugly/Bugly.h>
#import "SystemInfo.h"
#import <BaiduMapAPI_Base/BMKBaseComponent.h>



static NSString const *kNotFirstLaunchApp = @"notFirst";


@interface AppDelegate : UIResponder <UIApplicationDelegate, LocationServiceDelegate>
{
    // iOS 10通知中心
    UNUserNotificationCenter *_notificationCenter;
    BMKMapManager* _mapManager;
}

@property /*(strong, nonatomic)*/ NSTimer *timer;
//@property (nonatomic, assign) NSInteger receiveLoginOverTimeMessageCount;

@property (nonatomic, strong) NetworkReach *networkReach;

@property (nonatomic, assign) BOOL isNotFirstLaunchApp; // 是否第一次启动app
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) LoginViewController *loginViewController;
@property (strong, nonatomic) GesturePasswordController *gesturePasswordController;

@property (nonatomic, assign) NSInteger counter;
//@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, assign) NSInteger receiveLoginOverTimeMessageCount; // 登录超时通知次数
@property (nonatomic, assign) CFAbsoluteTime resignTime;  //记录进入后台的时间
@property (nonatomic, assign) CFAbsoluteTime currentTime;  //记录进入后台的时间

-(BOOL)isLoginOvertime; // 判断是否超时登录
-(void)initResignTime;// 初始化初始时间
-(void)handleCurrentTime;// 处理当前时间，如果超时通知弹出登录界面

-(void)backFromLogin;
-(void)backToOrderList;

-(void)handleGetRedpackSuccess;

@property (nonatomic, strong) OrderCountModel *orderCountModel;
@property (nonatomic, assign) NSUInteger applyPartnerBadgeNumber;

@property (nonatomic, strong) CheckUpdateService *checkUpdateService;

//-(void)initData;

//-(void)doOrderCountQuery;

@property (nonatomic, strong) CustomTabBarViewController *mainTabbarController;
//@property (nonatomic, weak) id<TabbarDelegate> mainTabbarController;
//@property (nonatomic, strong) NewMainTabBarController *mainTabbarController;
//@property (nonatomic, strong) MainTabBarController *mainTabbarController;

-(void)initViewsFromGuidePage;
-(void)initViews;

@property (nonatomic, strong) NSString *redpackMoney;// 红包金额



@end

