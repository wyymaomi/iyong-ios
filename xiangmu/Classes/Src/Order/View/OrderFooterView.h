//
//  OrderFooterView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderFooterView : UIView

@property (strong, nonatomic) UIButton *rechargeButton;
@property (strong, nonatomic) UIButton *okButton;


//@property (weak, nonatomic) IBOutlet UIButton *rechargeButton;

//@property (weak, nonatomic) IBOutlet UIButton *okButton;

//+ (instancetype)customView;

@end
