//
//  RequestData.m
//  发送验证码
//
//  Created by bww on 15/9/22.
//  Copyright © 2015年 bww. All rights reserved.
//

#import "RequestData.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"


@implementation RequestData

//验证码请求
+(void)requestVerificationCodeWithDic:(NSDictionary *)dic CallBack:(MyCallBack)callBack
{
    NSString *url =@"http://lgjt.hzgames.cn:8080/lgjt2_tvpartner/account2/smsauth?";
    NSString *path =[[url stringByAppendingString:@"username="] stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"username"]]];
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:path]];
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        id result =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        callBack(result);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"请求数据失败");
    }];
    [operation start];
}

//注册请求
+(void)requestRegisterWithDic:(NSDictionary *)dic CallBack:(MyCallBack)callBack
{
    NSString *username = [dic objectForKey:@"username"];
    NSString *password1 = [dic objectForKey:@"password1"];
    NSString *nicheng = [dic objectForKey:@"nicheng"];
    NSString *yanzheng = [dic objectForKey:@"yanzheng"];
    NSString *yaoqingma = [dic objectForKey:@"yaoqingma"];
    NSString *post = [[[[[[[[[[NSString stringWithFormat:@"username="]stringByAppendingString:username]stringByAppendingString:@"&password="]stringByAppendingString:password1]stringByAppendingString:@"&nickname="]stringByAppendingString:nicheng]stringByAppendingString:@"&authcode="]stringByAppendingString:yanzheng]stringByAppendingString:@"&smsauth="]stringByAppendingString:yaoqingma];
    NSData *postData =[post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *connectionRequest =[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://lgjt.hzgames.cn:8080/lgjt2_tvpartner/account2/regist"]];
    [connectionRequest setHTTPMethod:@"POST"];
    [connectionRequest setTimeoutInterval:100];
    [connectionRequest setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    //设置包体(字典)
    [connectionRequest setHTTPBody:postData];
    NSData *data =[NSURLConnection sendSynchronousRequest:connectionRequest returningResponse:nil error:nil];
    NSError *error;
    NSDictionary *aa =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

    callBack(aa);
}
@end
