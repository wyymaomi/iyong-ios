//
//  SIDADView.m
//  SIDAdView
//
//  Created by XU JUNJIE on 13/7/15.
//  Copyright (c) 2015 ISNC. All rights reserved.
//

#import "SIDADView.h"
#import "AdvModel.h"
#import "SDWebImageDownloader.h"
#import "UIImageView+WebCache.h"
#import "AliyunOSSManager.h"
#import "RedpackView.h"
#import "ShareUtil.h"

#define superid_ad_color_title          HEXRGB(0x0099cc)
#define superid_ad_color_tips           HEXRGB(0x333333)



//RGB Color transform（16 bit->10 bit）
#define HEXRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//Screen Height and width
#define Screen_height   [[UIScreen mainScreen] bounds].size.height
#define Screen_width    [[UIScreen mainScreen] bounds].size.width
#define VIEW_BX(view) (view.frame.origin.x + view.frame.size.width)
#define VIEW_BY(view) (view.frame.origin.y + view.frame.size.height)
//Get View size:
#define VIEW_W(view)  (view.frame.size.width)
#define VIEW_H(view)  (view.frame.size.height)

//#define
//#define SCROLL_VIEW_WIDTH (ViewWidth)
#define ADV_VIEW_WIDTH (ViewWidth-80*Scale)
#define SCROLL_VIEW_HEIGHT (ViewHeight - 220*Scale)
//#define SCROLL_VIEW_WIDTH 0.825 * ViewWidth

//iPhone4
#define   isIphone4  [UIScreen mainScreen].bounds.size.height < 500

@interface SIDADView()<UIScrollViewDelegate,UMSocialShareMenuViewDelegate>

@property (retain, nonatomic) IBOutletCollection(NSObject) NSArray* superid_ad_closeTargets;
@property (retain,nonatomic) UIButton        *closeBtn;
@property (retain,nonatomic) UIButton       *otherBtn;
@property (retain,nonatomic) UIView          *bgView;
@property (retain,nonatomic) UILabel         *titleLable;
@property (retain,nonatomic) UILabel         *subTitleLable;
@property (retain,nonatomic) UIView          *titleBgView;
@property (retain,nonatomic) UIImageView     *adImageView;
@property (nonatomic, strong) NSArray *imageList;
@property (retain,nonatomic) NSDictionary    *characterDitionary;

@property (nonatomic, strong) UIWindow      *maskWindow;
@property (nonatomic, strong) UIView        *maskView;

// 创建页码控制器
@property (nonatomic, strong) UIView        *advMaskView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UIScrollView *scrollView;


@property (nonatomic, strong) UIView *topMaskView;
@property (nonatomic, strong) UIImageView *maskLayer1ImageView;
@property (nonatomic, strong) UIImageView *maskLayer2ImageView;

@property (nonatomic, strong) NSArray *advList;

@end

@implementation SIDADView

- (UIPageControl*)pageControl
{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(ViewWidth / 3, ViewHeight * 15 / 16, ViewWidth / 3, ViewHeight / 16)];
        // 设置页数
//        _pageControl.numberOfPages = 3;
        // 设置页码的点的颜色
        _pageControl.pageIndicatorTintColor = [UIColor darkGrayColor];
        // 设置当前页码的点颜色
        _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];//UIColorFromRGB(0xF9BF3C);
        
        [_pageControl setValue:[UIImage imageNamed:@"icon_cycle_dot_selected"] forKeyPath:@"_currentPageImage"];
        [_pageControl setValue:[UIImage imageNamed:@"icon_cycle_dot_unselected"] forKeyPath:@"_pageImage"];
        
    }
    return _pageControl;
}


-(UIScrollView*)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, /*1.46*0.825*Screen_width)*/SCROLL_VIEW_HEIGHT)];
        _scrollView.bounces = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
//        _scrollView.contentSize = CGSizeMake(0.8*Screen_width * 3, 1.46*0.825*Screen_width);
        _scrollView.canCancelContentTouches = YES;
        _scrollView.delaysContentTouches = NO;
        
        //对srcollView添加点击响应
//        UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
//        sigleTapRecognizer.numberOfTapsRequired = 1;
//        [_scrollView addGestureRecognizer:sigleTapRecognizer];
        _scrollView.delegate = self;
        
    }
    return _scrollView;
}

- (UIWindow *)maskWindow
{
    if (!_maskWindow) {
        
        _maskWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        //edited by gjf 修改alertview leavel
        _maskWindow.windowLevel = UIWindowLevelNormal+1;
//        _maskWindow.windowLevel = UIWindowLevelStatusBar + BBAlertLeavel;
        _maskWindow.backgroundColor = [UIColor clearColor];
        _maskWindow.hidden = YES;
        
        _maskView= ({
            UIView *view = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
            view.backgroundColor = [UIColor blackColor];
            view.alpha = 0.7;
//            view.alpha = 0;
            view;
        });
        [_maskWindow addSubview:_maskView];
        
        _maskView.userInteractionEnabled = YES;
        UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAdvDismiss:)];
        sigleTapRecognizer.numberOfTapsRequired = 1;
        [_maskView addGestureRecognizer:sigleTapRecognizer];
//        _maskView = maskView;
        
        _advMaskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _advMaskView.backgroundColor = [UIColor clearColor];
        [_maskWindow addSubview:_advMaskView];

        // 广告图背景层
        _bgView = ({
            
            UIView *view =[[UIView alloc]initWithFrame: CGRectMake(0, 0, ViewWidth, /*1.46*0.825*Screen_width*/SCROLL_VIEW_HEIGHT)];
//            view.frame = CGRectMake(0, 0, 0.8*Screen_width, 1.46*0.825*Screen_width);
            view.center = CGPointMake(Screen_width/2, Screen_height/2);
//            if (isIphone4) {
//                self.center = CGPointMake(Screen_width/2, Screen_height/2+20);
//            }
            view.backgroundColor = [UIColor clearColor];
//            view.backgroundColor = [UIColor whiteColor];
            view.layer.cornerRadius = 6.0;
            view.clipsToBounds = YES;
//            view.layer.borderWidth=1;
//            view.layer.borderColor=superid_ad_color_title.CGColor;
            view;
            
        });
        
        [_advMaskView addSubview:_bgView];
        
        [_advMaskView addSubview:self.pageControl];
        
//        [_maskWindow addSubview:self.pageControl];
        
//        _otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        _otherBtn.frame = CGRectMake(VIEW_BX(_bgView) - 115, _bgView.top - 40, 115, 30);
//        [_otherBtn setCornerRadius:5];
//        [_otherBtn setTitle:@"查看更多活动" forState:UIControlStateNormal];
//        [_otherBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        _otherBtn.titleLabel.font = FONT(14);
//        [_otherBtn setBackgroundImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateNormal];
//        [_maskWindow addSubview:_otherBtn];
        
        _otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _otherBtn.frame = CGRectMake(ViewWidth-38*Scale, 142.5*Scale, 15*Scale, 15*Scale);
        [_otherBtn setBackgroundImage:[UIImage imageNamed:@"icon_exit"] forState:UIControlStateNormal];
        [_otherBtn addTarget:self action:@selector(onAdvDismiss:) forControlEvents:UIControlEventTouchUpInside];
        
        [_advMaskView addSubview:_otherBtn];
        
//        NSString *kNotFirstLaunchApp = @"notFirst";
        NSString *kNotFirstDisplayMaskView = @"kFirstDisplayMaskView";
        BOOL isNotFirstDisplayMaskView = [[[UserDefaults sharedInstance] objectForKey:kNotFirstDisplayMaskView] boolValue];
        if (!isNotFirstDisplayMaskView) {
            _topMaskView= ({
                UIView *view = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
                view.backgroundColor = [UIColor blackColor];
                view.alpha = 0.5;
//                [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissMaskView:)]];
                view;
            });
        
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(ViewWidth-231*Scale, 0, 231*Scale, 204*Scale)];
//        imageView.image = [UIImage imageNamed:@"img_red_pack"];
//        [_topMaskView addSubview:imageView];

            _maskLayer1ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, ViewHeight)];
            _maskLayer1ImageView.userInteractionEnabled = YES;
            _maskLayer1ImageView.image = [UIImage imageNamed:@"img_mask_layer1"];
            [_maskLayer1ImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMaskLayer2:)]];
            [_topMaskView addSubview:_maskLayer1ImageView];
            
            _maskLayer2ImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
            _maskLayer2ImageView.image = [UIImage imageNamed:@"img_mask_layer2"];
            [_maskLayer2ImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissMaskView:)]];
            _maskLayer2ImageView.userInteractionEnabled = YES;
            _maskLayer2ImageView.hidden = YES;
            [_topMaskView addSubview:_maskLayer2ImageView];
            
            [_maskWindow addSubview:_topMaskView];

            self.advMaskView.hidden = YES;
            
//            _bgView.hidden = YES;
//            _pageControl.hidden = YES;
//            _otherBtn.hidden = YES;
            
            NSUserDefaults *useDef = [NSUserDefaults standardUserDefaults];
            [useDef setBool:YES forKey:kNotFirstDisplayMaskView];
            [useDef synchronize];
        }

        
    }
    return _maskWindow;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    
}

-(void)dismissMaskView:(id)sender
{
    [_topMaskView removeFromSuperview];
    _topMaskView = nil;
    
    if (self.advList.count == 0) {
        [self removeFromSuperview];
        return;
    }

    self.advMaskView.hidden = NO;
//    _bgView.hidden = NO;
//    _pageControl.hidden = NO;
//    _otherBtn.hidden = NO;
}

-(void)showMaskLayer2:(id)sender
{
    _maskLayer2ImageView.hidden = NO;
    _maskLayer1ImageView.hidden = YES;
}

- (id)init{
    
    self = [super init];
    
    if (self) {

        self.userInteractionEnabled = YES;
        
        self.backgroundColor = [UIColor clearColor];
        
//        [UMSocialUIManager setShareMenuViewDelegate:self];
        
    }
    
    return self;
}

//- (UIView*)UMSocialParentView:(UIView*)defaultSuperView;
//{
//    return _maskWindow;
//}

//- (void)groupSync:(NSArray*)images
//{
//    
//    dispatch_group_t group = dispatch_group_create();
//    
//    for (NSInteger i = 0; i < images.count; i++) {
//        
//        dispatch_group_enter(group);
//        
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{
//            NSLog(@"任务%ld完成", (long)i);
//            NSLog(@"current thread = %@", [NSThread currentThread]);
//            for (NSInteger i = 0; i < 10000; i++) {
//                NSLog(@"i=%ld", (long)i);
//            }
//            dispatch_group_leave(group);
//        });
//    }
//    
//    dispatch_group_notify(group, dispatch_get_global_queue(0, 0), ^{
//        NSLog(@"current thread = %@", [NSThread currentThread]);
//        NSLog(@"任务完成");
//        
//        // 回到主线程显示图片
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // 4.将新图片显示出来
//            //            self.imageView.image = image;
//            NSLog(@"current thread = %@", [NSThread currentThread]);
//        });
//        
//    });
//}


-(void)show
{
//    self.scrollView addSubview:<#(nonnull UIView *)#>
    
//    self.scrollView addSub
    
    NSInteger pageCount = 3;
    
    CGFloat separatorLeft = 40*Scale;
    
    UIView *redpackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, self.scrollView.height)];
    redpackView.backgroundColor = [UIColor clearColor];
    RedpackView *redPackView = [[RedpackView alloc] initWithFrame:CGRectMake(separatorLeft, 0, ViewWidth-separatorLeft * 2, self.scrollView.height)];
    [redpackView addSubview:redPackView];
    [self.scrollView addSubview:redpackView];
    
    UIView *advView2 = [[UIView alloc] initWithFrame:CGRectMake(ViewWidth, 0, ViewWidth, self.scrollView.height)];
    advView2.backgroundColor = [UIColor clearColor];
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(separatorLeft, 0, ViewWidth-separatorLeft * 2, SCROLL_VIEW_HEIGHT)];
    imageView1.image = [UIImage imageNamed:@"guide0"];
    [advView2 addSubview:imageView1];
    [self.scrollView addSubview:advView2];
    
    UIView *advView3 = [[UIView alloc] initWithFrame:CGRectMake(ViewWidth*2, 0, ViewWidth, SCROLL_VIEW_HEIGHT)];
    advView3.backgroundColor = [UIColor clearColor];
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(separatorLeft, 0, ViewWidth-separatorLeft * 2, SCROLL_VIEW_HEIGHT)];
    imageView2.image = [UIImage imageNamed:@"guide1"];
    [advView3 addSubview:imageView2];
    [self.scrollView addSubview:advView3];
    
    [self.maskWindow makeKeyAndVisible];
    self.scrollView.contentSize = CGSizeMake(ViewWidth * pageCount, SCROLL_VIEW_HEIGHT);
//    self.scrollView.contentSize = CGSizeMake(0.825*Screen_width, 1.46*0.825*Screen_width);
//    self.scrollView.contentSize = CGSizeMake(0.825*Screen_width * images.count, 1.46*0.825*Screen_width);
    [self.bgView addSubview:self.scrollView];
    
    [self.maskWindow addSubview:self.pageControl];
    
    self.pageControl.numberOfPages = pageCount;
//    self.pageControl.numberOfPages = 1;
//    self.pageControl.numberOfPages = images.count;
    self.pageControl.currentPage = 0;
    self.pageControl.top = self.bgView.bottom + 10;
    
//    if (color) {
//        strongSelf.bgView.layer.borderColor = color.CGColor;
//    }
    
//    self.maskView.userInteractionEnabled = YES;
//    UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAdvDismiss:)];
//    sigleTapRecognizer.numberOfTapsRequired = 1;
//    [self.maskView addGestureRecognizer:sigleTapRecognizer];
    
    
}

-(void)showAdv:(NSArray<AdvModel*>*)advList
{
    self.advList = advList;
    
    if (advList.count == 0) {
        return;
    }
    
    NSInteger pageCount = advList.count;
    
    CGFloat separatorLeft = 40*Scale;
    
    for (NSUInteger i = 0; i < pageCount; i++) {
        
        AdvModel *advModel = advList[i];
        
        if ([advModel isRedpack]) {
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(ViewWidth*i, 0, ViewWidth, SCROLL_VIEW_HEIGHT)];
            view.backgroundColor = [UIColor clearColor];
            RedpackView *redPackView = [[RedpackView alloc] initWithFrame:CGRectMake(separatorLeft, 0, ViewWidth-separatorLeft * 2, SCROLL_VIEW_HEIGHT)];
            redPackView.shareUrl = advModel.linkUrl;
            [view addSubview:redPackView];
            [self.scrollView addSubview:view];
            
        }
        
        else {
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(ViewWidth*i, 0, ViewWidth, SCROLL_VIEW_HEIGHT)];
            view.backgroundColor = [UIColor clearColor];
            
            NSInteger startY = 46*Scale;
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(separatorLeft, startY, ViewWidth-separatorLeft * 2, SCROLL_VIEW_HEIGHT-startY)];
            
            NSString *imgPath = advModel.imageUrl;
            
            imageView.userInteractionEnabled = YES;
            
            [imageView yy_setImageWithObjectKey:imgPath placeholder:[UIImage new] manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                
                
            } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
                
                
                
            }];
            
            //对srcollView添加点击响应
            UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
            sigleTapRecognizer.numberOfTapsRequired = 1;
            [imageView addGestureRecognizer:sigleTapRecognizer];
//            _scrollView.delegate = self;
            
            [view addSubview:imageView];

            [self.scrollView addSubview:view];
            
        }
        
        
    }
    
    // 显示在主界面上
    
//    [self.maskWindow makeKeyAndVisible];
    
    self.scrollView.contentSize = CGSizeMake(ViewWidth * pageCount, SCROLL_VIEW_HEIGHT);
//    self.scrollView.contentSize = CGSizeMake(SCROLL_VIEW_WIDTH * pageCount, SCROLL_VIEW_HEIGHT);
    //    self.scrollView.contentSize = CGSizeMake(0.825*Screen_width, 1.46*0.825*Screen_width);
    //    self.scrollView.contentSize = CGSizeMake(0.825*Screen_width * images.count, 1.46*0.825*Screen_width);
    [self.bgView addSubview:self.scrollView];
    

    
    self.pageControl.numberOfPages = pageCount;
    //    self.pageControl.numberOfPages = 1;
    //    self.pageControl.numberOfPages = images.count;
    self.pageControl.currentPage = 0;
    self.pageControl.top = self.bgView.bottom + 10;
    
    //    if (color) {
    //        strongSelf.bgView.layer.borderColor = color.CGColor;
    //    }
    


    
}

-(void)showFirstMaskView
{
    [self.maskWindow makeKeyAndVisible];
}


- (void)showInView:(UIView *)view withFaceInfo: (NSDictionary *)info advertisementImage: (UIImage *)image borderColor: (UIColor *)color{
    
//    if (!info) {
//
//        return;
//    }
    
    
    [self.maskWindow makeKeyAndVisible];
    
    _titleLable.text  = @"title";//[self featureTransform:info];
    _adImageView.image = image;
    if (color) {
        
        _bgView.layer.borderColor = color.CGColor;
        
    }
    
    
    
}

- (void)showWithFaceInfo: (NSDictionary *)info advertisementImage: (UIImage *)image borderColor: (UIColor *)color{
    
    if (!info) {
        
        return;
    }

    _titleLable.text  =[self featureTransform:info];
    _adImageView.image = image;
    if (color) {
        
        _bgView.layer.borderColor = color.CGColor;

    }
    [[self getCurrentVC].view addSubview:self];
    
}

- (void)dismiss
{
    [_maskWindow removeAllSubviews];
    _maskWindow.hidden = YES;
    _maskWindow = nil;
    _scrollView = nil;
    _pageControl = nil;
}

- (void)closeBtnClickEventHandle{
    
    [_maskWindow removeAllSubviews];
//    [self removeFromSuperview];
    _maskWindow = nil;
//    _adImageView.image = nil;
//    _titleLable.text = nil;

}

- (void)dealloc{
    
//    [_maskWindow removeAllSubviews];
    _maskWindow = nil;
    _titleLable = nil;
    _scrollView = nil;
    
    _adImageView = nil;
    _subTitleLable = nil;
    _bgView = nil;
    _titleBgView = nil;
    _closeBtn = nil;
}

- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

- (void)drawSuperid_ad_close
{
    //// Color Declarations
    UIColor* white = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 1];
    
    //// yuan Drawing
    UIBezierPath* yuanPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(2, 2, 60, 61)];
    [white setStroke];
    yuanPath.lineWidth = 2;
    [yuanPath stroke];
    
    
    //// mid Drawing
    UIBezierPath* midPath = UIBezierPath.bezierPath;
    [midPath moveToPoint: CGPointMake(30.01, 32.5)];
    [midPath addLineToPoint: CGPointMake(18.74, 43.96)];
    [midPath addCurveToPoint: CGPointMake(18.74, 45.98) controlPoint1: CGPointMake(18.2, 44.51) controlPoint2: CGPointMake(18.19, 45.42)];
    [midPath addCurveToPoint: CGPointMake(20.73, 45.98) controlPoint1: CGPointMake(19.29, 46.54) controlPoint2: CGPointMake(20.18, 46.54)];
    [midPath addLineToPoint: CGPointMake(32, 34.52)];
    [midPath addLineToPoint: CGPointMake(43.27, 45.98)];
    [midPath addCurveToPoint: CGPointMake(45.26, 45.98) controlPoint1: CGPointMake(43.82, 46.54) controlPoint2: CGPointMake(44.71, 46.54)];
    [midPath addCurveToPoint: CGPointMake(45.26, 43.96) controlPoint1: CGPointMake(45.81, 45.42) controlPoint2: CGPointMake(45.8, 44.51)];
    [midPath addLineToPoint: CGPointMake(33.99, 32.5)];
    [midPath addLineToPoint: CGPointMake(45.26, 21.04)];
    [midPath addCurveToPoint: CGPointMake(45.26, 19.02) controlPoint1: CGPointMake(45.8, 20.49) controlPoint2: CGPointMake(45.81, 19.58)];
    [midPath addCurveToPoint: CGPointMake(43.27, 19.02) controlPoint1: CGPointMake(44.71, 18.46) controlPoint2: CGPointMake(43.82, 18.46)];
    [midPath addLineToPoint: CGPointMake(32, 30.48)];
    [midPath addLineToPoint: CGPointMake(20.73, 19.02)];
    [midPath addCurveToPoint: CGPointMake(18.74, 19.02) controlPoint1: CGPointMake(20.18, 18.46) controlPoint2: CGPointMake(19.29, 18.46)];
    [midPath addCurveToPoint: CGPointMake(18.74, 21.04) controlPoint1: CGPointMake(18.19, 19.58) controlPoint2: CGPointMake(18.2, 20.49)];
    [midPath addLineToPoint: CGPointMake(30.01, 32.5)];
    [midPath closePath];
    midPath.miterLimit = 4;
    
    midPath.usesEvenOddFillRule = YES;
    
    [white setFill];
    [midPath fill];
}

#pragma mark Generated Images
static UIImage* _imageOfSuperid_ad_close = nil;

- (UIImage*)imageOfSuperid_ad_close
{
    if (_imageOfSuperid_ad_close)
        return _imageOfSuperid_ad_close;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(64, 64), NO, 0.0f);
    [self drawSuperid_ad_close];
    
    _imageOfSuperid_ad_close = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return _imageOfSuperid_ad_close;
}

#pragma mark Customization Infrastructure

- (void)setSuperid_ad_closeTargets: (NSArray*)superid_ad_closeTargets
{
    _superid_ad_closeTargets = superid_ad_closeTargets;
    
    for (id target in self.superid_ad_closeTargets)
        [target setImage: self.imageOfSuperid_ad_close];
}

- (NSString *)featureTransform:(NSDictionary *)info{
    
    NSString *str = [NSString stringWithFormat:@"我是一个%@%@的%@",[self getUserCharacterStr:info],[self getUserAppearanceStrFromInfo:info],[self getGenerationStr:info]];
    return str;
    
}

- (NSString *)getUserCharacterStr:(NSDictionary *)dict{
    
    NSString *key = [dict objectForKey:@"character"];

    if (dict && key ) {
        
        if ([_characterDitionary objectForKey:key]) {
            
            return [_characterDitionary objectForKey:key];
        }else{
            
            return @"幸运";
        }
        
        
    }else{
        
        return @"幸运";
    }
}

- (NSString *)getUserAppearanceStrFromInfo: (NSDictionary *)dict{
    
    NSArray *tagsArray = [dict objectForKey:@"tags"];

    if (!tagsArray) {
        
        return @"";
    }
    BOOL isGoodLooking = NO;
    
    for (int i = 0; i<[tagsArray count]; i++) {
        
        if ([[tagsArray objectAtIndex:i]isEqualToString:@"goodLooking"]) {
            
            isGoodLooking = YES;
            
        }
    }
    if (isGoodLooking == YES) {
        
        if ([[dict objectForKey:@"gender"] isEqualToString:@"male"]) {
            
            return @"帅气";
        }else{
            
            return @"漂亮";
        }
        
    }else{
        
        return @"";
    }

}

- (NSString *)getGenerationStr:(NSDictionary *)info{

    NSString *str = [info objectForKey:@"generation"];
    if (str) {
        
        NSString *ageStr = [str substringToIndex:2];
        ageStr = [NSString stringWithFormat:@"%@后",ageStr];
        return ageStr;
    }else{
        
        return @"人";
    }
    
}

#pragma mark - scrollView delegate method

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // 计算当前在第几页
    double scrollViewWidth = ViewWidth;
//    DLog(@"scrollViewWidth = %.5f", scrollViewWidth);
//    DLog(@"self.scrollView.contentOffset.x = %.5f", self.scrollView.contentOffset.x);
//    self.pageControl.currentPage = (NSInteger)(self.scrollView.contentOffset.x / scrollViewWidth);
//    NSInteger index=self.scrollView.contentOffset.x + 0.5)/(scrollViewWidth+0.5);//fabs(self.scrollView.contentOffset.x)/scrollViewWidth;
//    NSInteger mode = self.scrollView.contentOffset.x % (scrollViewWidth+0.5);
//    double mode = self.scrollView.contentOffset.x % scrollViewWidth;
    
    NSInteger index = (self.scrollView.contentOffset.x + 0.5) / scrollViewWidth;
    
    self.pageControl.currentPage = index;
//    if (index == 0) {
//        self.pageControl.currentPage = 0;
//    }
//    else {
//    
//    }
    
    
    
//    double mod = self.scrollView.contentOffset.x % scrollViewWidth;

//    CGFloat mod = ((CGFloat)self.scrollView.contentOffset.x) % ((CGFloat)scrollViewWidth);
    
    
    
//    if ((CGFloat)((CGFloat)self.scrollView.contentOffset.x % scrollViewWidth) > 0) {
//        self.pageControl.currentPage ++;
//    }
//    double scrollOffsetX = self.scrollView.contentOffset.x;
//    double mod = (double)scrollOffsetX % scrollViewWidth;
//    if (mod > 0) {
//        self.pageControl.currentPage++;
//    }
}

- (void)handleTapGesture:(id)sender
{
    DLog(@"clicked");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onClickAdv:)]) {
        [self.delegate onClickAdv:self.pageControl.currentPage];
    }
}

- (void)onAdvDismiss:(id)sender
{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(onClickAdv:)]) {
//        [self.delegate onClickAdv:self.pageControl.currentPage];
//    }
    
//    self.scrollView = nil;
//    self.pageControl = nil;
//    self.maskWindow = nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onDismiss)]) {
        [self.delegate onDismiss];
    }
}

@end
