//
//  ImageUtil.m
//  lottery
//
//  Created by wyy on 15-1-8.
//  Copyright (c) 2015年 ___QIHUITEL___. All rights reserved.
//

#import "ImageUtil.h"

@implementation ImageUtil

+ (NSString *)dataFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"avatarImage"];
    return path;
}

//+(void)saveLocalImage:(NSData*)imageData fileName:(NSString*)fileName
//{
//    //图片保存的路径
//    //这里将图片放在沙盒的documents文件夹中
//    NSString * DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//    
//    //文件管理器
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    
//    //把刚刚图片转换的data对象拷贝至沙盒中 并保存为image.png
//    [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
//    [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:fileName] contents:imageData attributes:nil];
//    
//    //得到选择后沙盒中图片的完整路径
////    _filePath = [[NSString alloc]initWithFormat:@"%@%@",DocumentsPath,  fileName];
//
//}

#pragma mark 保存图片到document
+ (void)saveImage:(UIImage *)image WithName:(NSString *)imageName
{
    NSData* imageData = UIImagePNGRepresentation(image);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    // Now we get the full path to the file
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    // and then we write it out
    [imageData writeToFile:fullPathToFile atomically:NO];
}

#pragma mark 保存图片到document
+ (void)saveImageData:(NSData *)imageData WithName:(NSString *)imageName
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    // Now we get the full path to the file
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    // and then we write it out
    [imageData writeToFile:fullPathToFile atomically:NO];
}

#pragma mark 从文档目录下获取Documents路径
+ (NSString *)documentFolderPath
{
    return [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
}

+(UIImage*)loadImageData:(NSString*)directoryPath
              imageName:(NSString*)imageName
{
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL dirExisted = [fileManager fileExistsAtPath:directoryPath isDirectory:&isDir];
    if (isDir && dirExisted) {
        NSString *imagePath = [NSString stringWithFormat:@"%@/%@", directoryPath, imageName];//[directoryPath stringByAppendingString:imageName];
        BOOL fileExisted = [fileManager fileExistsAtPath:imagePath];
        if (fileExisted) {
            NSData *imageData = [NSData dataWithContentsOfFile:imagePath];
            UIImage *image = [UIImage imageWithData:imageData];
            return image;
        }
    }
    return nil;
}

+(void)removeLocalImage:(NSString*)imageName
{
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL dirExisted = [fileManager fileExistsAtPath:[self documentFolderPath] isDirectory:&isDir];
    if (isDir && dirExisted) {
        NSString *imagePath = [NSString stringWithFormat:@"%@/%@", [self documentFolderPath], imageName];//[directoryPath stringByAppendingString:imageName];
        BOOL fileExisted = [fileManager fileExistsAtPath:imagePath];
        if (fileExisted) {
            [fileManager removeItemAtPath:imagePath error:nil];
//            NSData *imageData = [NSData dataWithContentsOfFile:imagePath];
//            UIImage *image = [UIImage imageWithData:imageData];
//            return image;
        }
    }
}

+ (UIImage *)streImageNamed:(NSString *)imageName
{
    if (imageName == nil) {
        return nil;
    }
    UIImage *image = [UIImage imageNamed:imageName];
    UIImage *streImage = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    return streImage;
}


+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    DLog(@"width = %.f, height = %.f", newSize.width, newSize.height);
    
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}

+ (UIImage *)compressImage:(UIImage *)image toMaxFileSize:(NSInteger)maxFileSize {
    
    DLog(@"maxFileSize = %ld", (long)maxFileSize);
    
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    NSData *imageData = UIImageJPEGRepresentation(image, compression);
    while ([imageData length] > maxFileSize && compression > maxCompression) {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(image, compression);
    }
    
    UIImage *compressedImage = [UIImage imageWithData:imageData];
    return compressedImage;
}

+(NSData*)compressImageWithData:(UIImage*)image toMaxFileSize:(NSInteger)maxFileSize
{
    
    DLog(@"maxFileSize = %ld", (long)maxFileSize);
    
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    NSData *imageData = UIImageJPEGRepresentation(image, compression);
    while ([imageData length] > maxFileSize && compression > maxCompression) {
        compression -= 0.05;
        imageData = UIImageJPEGRepresentation(image, compression);
        DLog(@"=======compression = %.1f", compression);
        DLog(@"=======imageData.length = %lu", (unsigned long)imageData.length);
    }

    return imageData;
    
}



@end
