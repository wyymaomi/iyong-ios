//
//  AppDelegate+IFlySpeech.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AppDelegate.h"
#import "iflyMSC/IFlyMSC.h"

#define APPID_VALUE @"59c34e7a"

@interface AppDelegate (IFlySpeech)

-(void)initIFlySpeech;

@end
