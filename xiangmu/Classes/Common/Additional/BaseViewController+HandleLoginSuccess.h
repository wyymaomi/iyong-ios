//
//  BaseViewController+HandleLoginSuccess.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/11.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController (HandleLoginSuccess)

- (void)handleLoginSuccess;

-(void)handleResetPwdSuccess;

@end
