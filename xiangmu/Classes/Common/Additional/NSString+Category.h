//
//  NSString+Category.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/3.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)

/**
 * Compares two strings lexicographically.
 * the value 0 if the argument string is equal to this string;
 * a value less than 0 if this string is lexicographically less than the string argument;
 * and a value greater than 0 if this string is lexicographically greater than the string argument.
 */
- (NSInteger) compareTo:(NSString*) anotherString;
- (NSInteger) compareToIgnoreCase:(NSString*) str;
- (BOOL) contains:(NSString*) str;
- (BOOL) startsWith:(NSString*)prefix;
- (BOOL) endsWith:(NSString*)suffix;
- (BOOL) equals:(NSString*) anotherString;
- (BOOL) equalsIgnoreCase:(NSString*) anotherString;
- (NSInteger) indexOfChar:(unichar)ch;
- (NSInteger) indexOfChar:(unichar)ch fromIndex:(NSUInteger)index;
- (NSInteger) indexOfString:(NSString*)str;
- (NSInteger) indexOfString:(NSString*)str fromIndex:(NSUInteger)index;
- (NSInteger) lastIndexOfChar:(unichar)ch;
- (NSInteger) lastIndexOfChar:(unichar)ch fromIndex:(NSUInteger)index;
- (NSInteger) lastIndexOfString:(NSString*)str;
- (NSInteger) lastIndexOfString:(NSString*)str fromIndex:(NSUInteger)index;
- (NSString *) substringFromIndex:(NSUInteger)beginIndex toIndex:(NSUInteger)endIndex;
- (NSString *) toLowerCase;
- (NSString *) toUpperCase;
- (NSString *) trim;
- (NSString *) replaceAll:(NSString*)origin with:(NSString*)replacement;
//- (NSString *) replaceAllByRegex:(NSString*)pattern with:(NSString*)replacement;
- (NSArray *) split:(NSString*) separator;

-(NSString*)getDateTime;
-(NSString*)getDateTime:(NSString*)dateFormat;
+(NSString*)getTimeStamp;

- (CGSize)textSize:(CGSize)size font:(UIFont*)font;
//- (CGSize)textSize:(CGFloat)width font:(UIFont*)font;
- (NSString *)replaceStringWithAsterisk:(NSInteger)startLocation length:(NSInteger)length;

@end

