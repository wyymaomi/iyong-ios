//
//  HomeBottomView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/9.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeBottomView : UIView

@property (nonatomic, strong) UIButton *inquiryButton;
@property (nonatomic, strong) UIButton *bidButton;

@end
