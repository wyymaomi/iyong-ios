//
//  MyDetailTableViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UserInfoViewController.h"

#import "MyTableViewCell.h"
#import "MyTableViewCell+Additional.h"
#import "MyUtil.h"
#import "NetworkManager.h"
#import "MsgToolBox.h"
#import "GTMBase64.h"
#import "UserInfoHttpMessage.h"

#import "AvatarViewController.h"
#import "InputHelperViewController.h"
#import "RealnameCertificationViewController.h"
#import "BindMailboxViewController.h"
//#import "GestureLoginViewController.h"
#import "GesturePasswordController.h"
#import "CompanyCertificationViewController.h"
#import "AliyunOSSManager.h"



@interface UserInfoViewController ()<UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate, InputHelperDelegate,RealnameCertificationDelegate, MailboxDelegate>
{
    BOOL _isFirstIn;// 第一次进入界面
}

//@property (nonatomic, strong) UIButton *okButton;
//@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSString *realName;
@property (nonatomic, assign) NSInteger companyCertification;
@property (nonatomic, assign) BOOL idCertification;
@property (nonatomic, strong) NSString *email;

@property (nonatomic, strong) UserInfoHttpMessage *userInfoHttpMessage;

@end

@implementation UserInfoViewController

- (UserInfoHttpMessage*)userInfoHttpMessage
{
    if (!_userInfoHttpMessage) {
        _userInfoHttpMessage = [UserInfoHttpMessage new];
    }
    
    return _userInfoHttpMessage;
}

#pragma mark - life cycle

- (id)init
{
    self = [super init];
    
    if (self) {
        
        _isFirstIn = YES;
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"个人信息";
    if (self.source_from == SOURCE_FROM_REGISTER) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else {
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveUserInfo)];
    }
    
    NSInteger level = [[UserManager sharedInstance].userModel.level integerValue];
    if (level != 1) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    [self.view addSubview:self.groupTableView];
    self.groupTableView.rowHeight = DefaultTableRowHeight;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.source_from != SOURCE_FROM_REGISTER) {
        [self initData];
    }
    
}


#pragma mark - 刷新数据

- (void)initData {
    
    self.nextAction = @selector(initData);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }

    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, My_Detail_API];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        
        NSThread *current = [NSThread currentThread];
        NSLog(@"currentThread = %@", current);
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            [UserManager sharedInstance].userModel = [[UserModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            
            if (_isFirstIn) {
                
                _isFirstIn = NO;
                
                strongSelf.userInfoHttpMessage.nickname = [StringUtil getSafeString:[UserManager sharedInstance].userModel.nickname.trim];
                strongSelf.userInfoHttpMessage.companyName = [StringUtil getSafeString:[UserManager sharedInstance].userModel.companyName.trim];
                
                NSString *logoUrl = [StringUtil getSafeString:[UserManager sharedInstance].userModel.logoUrl.trim];
                [strongSelf downloadAvatarImage:logoUrl];

            }
            
            [strongSelf.groupTableView reloadData];
            
        }
        
    } andFailure:^(NSString *errorDesc) {
//        NSLog(@"response failure");
        StrongSelf
        [strongSelf resetAction];
    }];
    
}

- (void)downloadAvatarImage:(NSString*)logoUrl
{
    if (IsStrEmpty(logoUrl)) {
        return;
    }
    // 如果有图片，判断图片是否已经下载
//    if (!IsStrEmpty(logoUrl)) {
//        WeakSelf
//        [[NetworkManager sharedInstance] downloadImage:logoUrl success:^(id responseData) {
//            StrongSelf
//            NSData *imgData = responseData;
//            if (imgData != nil && imgData.length > 0) {
////                strongSelf.userInfoHttpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imgData] encoding:NSUTF8StringEncoding];
//                _avatarImageData = imgData;
//                [strongSelf.groupTableView reloadData];
//            }
//        } andFailure:^(NSString *errorDesc) {
//            
//            
//        }];

    WeakSelf
    [[AliyunOSSManager sharedInstance] downloadImage:logoUrl isThumbnail:NO complete:^(DownloadImageState state, NSData *data) {
        StrongSelf
        if (state == DownloadImageSuccess) {
            //                strongSelf.userInfoHttpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imgData] encoding:NSUTF8StringEncoding];
            _avatarImageData = data;
            [strongSelf.groupTableView reloadData];
        }
        
    }];
}


//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([self.groupTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.groupTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.groupTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.groupTableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

-(void)saveUserInfo
{
    self.nextAction = @selector(saveUserInfo);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
//    NSData *avatarImageData =
    NSArray *array = @[_avatarImageData];
    [self showHUDIndicatorViewAtCenter:@"正在保存用户信息，请稍候..."];
    WeakSelf
    [[AliyunOSSManager sharedInstance] uploadImages:array isAsync:YES folderName:kAliyunOSSPublichImagePath([UserManager sharedInstance].userModel.companyId) complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        StrongSelf
        
        if (state == UploadImageSuccess) {
            
            strongSelf.userInfoHttpMessage.logoUrl = names[0];
            
            [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(UserInfo_Update_API) requestMethod:POST params:strongSelf.userInfoHttpMessage.description success:^(id responseData) {
                
                [strongSelf hideHUDIndicatorViewAtCenter];
                
                NSInteger code_status = [responseData[@"code"] integerValue];
                
                if (code_status == STATUS_OK) {

                    [[NSNotificationCenter defaultCenter] postNotificationName:kOnUserinfoUpdateNotification object:nil userInfo:nil];
                    
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                    
                    UserModel *userModel = [[UserModel alloc] initWithDictionary:responseData[@"data"] error:nil];
                    
                    [UserManager sharedInstance].userModel = userModel;//[userModel mutableCopy];
                    
                }
                
            } andFailure:^(NSString *errorDesc) {
                
//                StrongSelf
                [strongSelf hideHUDIndicatorViewAtCenter];
                [strongSelf showAlertView:errorDesc];
                [strongSelf resetAction];
                
            }];
            
        }
        else {
            [strongSelf hideHUDIndicatorViewAtCenter];
            [strongSelf resetAction];
            [MsgToolBox showAlert:@"" content:@"上传头像失败，请重新上传"];
        }
        
        
    }];
//    [[AliyunOSSManager sh]]
    
    // 个人信息上传
//    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, UserInfo_Update_API];
//    NSString *params = self.userInfoHttpMessage.description;
//    
//    [self showHUDIndicatorViewAtCenter:@"正在保存用户信息，请稍候..."];
//    
//    WeakSelf
//    
//    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
//        
//        StrongSelf
//        [strongSelf hideHUDIndicatorViewAtCenter];
//        [strongSelf resetAction];
//        
//        NSInteger code_status = [responseData[@"code"] integerValue];
//        
//        if (code_status == STATUS_OK) {
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:kOnUserinfoUpdateNotification object:nil userInfo:nil];
//            
//            [strongSelf.navigationController popViewControllerAnimated:YES];
//            
//            UserModel *userModel = [[UserModel alloc] initWithDictionary:responseData[@"data"] error:nil];
//            
//            [UserManager sharedInstance].userModel = userModel;//[userModel mutableCopy];
//        }
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//        StrongSelf
//        [strongSelf hideHUDIndicatorViewAtCenter];
//        [strongSelf showAlertView:errorDesc];
//        [strongSelf resetAction];
//        
//    }];
    
    
}

#pragma mark - AvatarViewController Delegate method
-(void)onGetAvatarImage:(NSData*)imageData;
{
    _avatarImageData = imageData;
//    self.userInfoHttpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imageData] encoding:NSUTF8StringEncoding];
    [self.groupTableView reloadData];
}


#pragma mark - InputHelperTextDelegate
-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
{
    if (tag == MyNicknameTextField) {
        self.userInfoHttpMessage.nickname = text;
        [self.groupTableView reloadData];
    }
    else if (tag == MyCompanyTextField) {
        self.userInfoHttpMessage.companyName = text;
        [self.groupTableView reloadData];
    }
}

#pragma mark - Bind email delegate method

-(void)onBindMailboxSuccess:(NSString*)emailBox;
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:UsrInfoBindEmail inSection:0];
    MyTableViewCell *cell = (MyTableViewCell*)[self.groupTableView cellForRowAtIndexPath:indexPath];
    cell.detailLabel.text = emailBox;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return UserInfoRowNumber;
    }
    return MeCertificationNumber;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0 && indexPath.row==0) {
//        return 60;
//    }
//    return 44;
    
    if (indexPath.section == 0 && indexPath.row == UserInfoRowCompanyName) {
        return 0.0001f;
    }
    return DefaultTableRowHeight;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    if (section == 1) {
//        return 100;
//    }
//    return 0.00001f;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.00001f;
    }
    return 15.0f;
}

//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    if (section == 1) {
//        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 100)];
////        if (self.source_from == SOURCE_FROM_REGISTER) {
////            UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
////            [okButton setTitle:@"下一步" forState:UIControlStateNormal];
////            okButton.frame = CGRectMake(15, 20, ViewWidth-30, 40);
////            [okButton blueStyle];
////            [okButton addTarget:self action:@selector(onNextButtonClick) forControlEvents:UIControlEventTouchUpInside];
////            
////            [view addSubview:okButton];
////        }
//        
//        
//        return view;
//        
//    }
//    
//    return [[UIView alloc] init];
//    
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellid=@"cellid";
    MyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[MyTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            UIImageView *avatarImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-47.5*Scale, 10*Scale ,35*Scale,35*Scale)];
            avatarImageView.cornerRadius = avatarImageView.width/2;
            avatarImageView.tag=900;
            [cell.contentView addSubview:avatarImageView];
        }

        
        NSInteger level = [[UserManager sharedInstance].userModel.level integerValue];
        if (level != 1) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
    }
    
    
    if (indexPath.section == 0) {
        NSArray *nameArray=@[@"头像", @"昵称",@"手机号", @"公司名称", @"邮箱账号"];
        cell.name.text=nameArray[indexPath.row];
        
        if (indexPath.row==UserInfoRowAvatar) {
            UIImageView *headView=(UIImageView *)[cell.contentView viewWithTag:900];
//            cell.disclosureImageView.frame = CGRectMake(cell.disclosureImageView.origin.x, (cell.contentView.size.height-42/3)/2, 23/3, 42/3);
            if (self.avatarImageData && self.avatarImageData.length > 0) {
                headView.image = [UIImage imageWithData:self.avatarImageData];
            }
            else {
                headView.image = [UIImage imageNamed:@"icon_avatar"];
            }
//            cell.name.top = (cell.contentView.height - 20)/2;
            
        }
        else if(indexPath.row==UseerInfoRowMobile) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.detailLabel.text = [UserManager sharedInstance].userModel.username;
        }
        if (indexPath.row == UserInfoRowNickName) {
            cell.detailLabel.text = self.userInfoHttpMessage.nickname;
        }
        if (indexPath.row == UserInfoRowCompanyName) {
            cell.detailLabel.text = self.userInfoHttpMessage.companyName ;
            cell.hidden = YES;
        }
        if (indexPath.row == UsrInfoBindEmail) {
            cell.name.text = @"邮箱帐号";
            cell.detailLabel.text = [UserManager sharedInstance].userModel.email;
        }
        
    }
    
    if (indexPath.section == 1) {
        if (indexPath.row == MeCertificationRowLevel) {
            [cell initLevelData];
        }
        if (indexPath.row == MeCertificationRowCertificationInfo) {
            [cell initCertificationData];
        }
        if (indexPath.row == MeCertificationRowRealname) {
            [cell initRealnameCertifcationData];
        }
        if (indexPath.row == MeCertificationRowCompany) {
            [cell initCompanyCertificationData];
        }
//        if (indexPath.row == MeCertificationRowVechile) {
//            [cell initVechileCertificationData];
//        }
//        if (indexPath.row == MeCertificationServiceProvier) {
//            [cell initServiceProviderCertificationData];
//        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // 调度员不可编辑 只有老板才可以编辑
    
    NSInteger level = [[UserManager sharedInstance].userModel.level integerValue];
    
    if (level == 1) {
        
        MyTableViewCell *cell=(MyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (indexPath.section == 0) {
            if (indexPath.row == UserInfoRowAvatar) {
                AvatarViewController *viewController=[[AvatarViewController alloc]init];
                viewController.delegate=self;
                viewController.avatarImageData = self.avatarImageData;
//                viewController.avatarImageData = [GTMBase64 decodeString:self.userInfoHttpMessage.fileContent];
                [self.navigationController pushViewController:viewController animated:YES];
            }
            if (indexPath.row == UserInfoRowNickName) {
                
                InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
                viewController.text = cell.detailLabel.text;
                viewController.tag = MyNicknameTextField;
                viewController.delegate = self;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            if (indexPath.row == UserInfoRowCompanyName) {
//                NSString *companyName = [UserManager sharedInstance].userModel.companyName;
//                if (IsStrEmpty(companyName)) {
//                    InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
//                    viewController.text = cell.detailLabel.text;
//                    viewController.tag = MyCompanyTextField;
//                    viewController.delegate = self;
//                    [self.navigationController pushViewController:viewController animated:YES];
//                }
                
            }
            if (indexPath.row == UsrInfoBindEmail) {
                MyTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                BindMailboxViewController *viewController = [[BindMailboxViewController alloc] init];
                viewController.emailbox = cell.detailLabel.text;//[UserManager sharedInstance].userModel.email;
                viewController.delegate = self;
                [self.navigationController pushViewController:viewController animated:YES];

            }
            
        }
        if (indexPath.section == 1) {
            if (indexPath.row == MeCertificationRowRealname) {
                RealnameCertificationViewController *viewController = [[RealnameCertificationViewController alloc] init];
                viewController.type = [[UserManager sharedInstance].userModel.certification boolValue]?0:1;
                viewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            if (indexPath.row == MeCertificationRowCompany) {
                if (![[UserManager sharedInstance].userModel isRealnameCertified]) {
                    [YYAlertView showAlertView:@"" message:@"请先进行实名认证" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                        
                        
                    }];
                    
                    return;
                }
                [self gotoPage:@"CompanyCertificationViewController"];
            }
            if (indexPath.row == MeCertificationRowVechile) {
                [self gotoPage:@"CarTableViewController"];
            }
            if (indexPath.row == MeCertificationServiceProvier) {
//                [self gotoPage:@"ServiceProviderViewController"];
                
                // 服务商权限校验
                if ([[UserManager sharedInstance].userModel isRealnameCertified] && [[UserManager sharedInstance].userModel isCompanyCertified]) {
                    [self resetAction];
                    [self gotoPage:@"ServiceProviderViewController" animated:YES title:@"发布服务商"];
                }
                else {
                    [self resetAction];
//                    WeakSelf
                    [YYAlertView showAlertView:@"" message:@"请先进行实名认证和企业认证" cancleButtonTitle:nil okButtonTitle:@"确定" completeBlock:^(NSInteger buttonIndex) {
                        
//                        if (buttonIndex == 0) {
//                            [weakSelf gotoPage:@"UserInfoViewController"];
//                        }
                        
                    }];
                }
            }
        }

    }
}



//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}


@end
