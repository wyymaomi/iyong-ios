//
//  OnlineServiceTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/26.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OnlineServiceTableViewCell.h"


@interface OnlineServiceTableViewCell ()



@end

@implementation OnlineServiceTableViewCell

+ (instancetype)momentsTableViewCellWithTableView:(UITableView *)tableView;
{
    static NSString *reuseID = @"cell";
    OnlineServiceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[OnlineServiceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseID];
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = UIColorFromRGB(0xDEDEDE);
//        self.contentView.backgroundColor = UIColorFromRGB(0xEDEDED);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.bodyView];
        [self addSubview:self.toolView];
    }
    
    return self;
}

//设置cell的frame
//-(void)setFrame:(CGRect)frame{
////    frame.origin.x += circleCellMargin;
////    frame.size.width -= circleCellMargin * 2;
////    [super setFrame:frame];
//    [super setFrame:frame];
//}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (_viewModel) {
        self.bodyView.frame = _viewModel.momentsBodyFrame;
        self.toolView.frame = _viewModel.momentsToolBarFrame;
    }
}


- (void)setViewModel:(ServiceProviderViewModel *)viewModel
{
    _viewModel = viewModel;
    
    // 设置子控件的frame
    self.bodyView.frame = _viewModel.momentsBodyFrame;
    self.bodyView.viewModel = viewModel;
    self.toolView.frame = _viewModel.momentsToolBarFrame;
    self.toolView.viewModel = viewModel;
    
}


- (ServiceBodyView*)bodyView
{
    if (_bodyView == nil) {
        _bodyView = [ServiceBodyView new];
        _bodyView.cell = self;
        
    }
    
    return _bodyView;
}

- (ServiceToolBarView *)toolView
{
    if (_toolView == nil){
        _toolView = [ServiceToolBarView new];
    }
    return _toolView;
}

@end
