//
//  LocationViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "LocationSearchViewController.h"
#import "SearchModel.h"
#import "HistoryLocationDAO.h"
#import "GYZChooseCityController.h"
#import "GYZChooseCityDelegate.h"
#import "LocationService.h"
#import "MsgToolBox.h"
#import "CommunityHelper.h"
#import "AppConfig.h"
#import "IATConfig.h"

@interface LocationSearchViewController ()<UINavigationBarDelegate,GYZChooseCityDelegate,LocationServiceDelegate,UINavigationControllerDelegate>

@property(nonatomic,weak) UINavigationController *navController;
@property (nonatomic, strong) HistoryLocationDAO *historySearchDAO;
@property (nonatomic, strong) LocationService *locationService;

@end

@implementation LocationSearchViewController

-(HistoryLocationDAO*)historySearchDAO
{
    if (!_historySearchDAO) {
        _historySearchDAO = [[HistoryLocationDAO alloc] init];
    }
    return _historySearchDAO;
}

- (LocationSearchBar*)searchBar
{
    if (!_searchBar) {
        _searchBar = [LocationSearchBar customView];
        _searchBar.delegate = self;
        [_searchBar.speechButton addTarget:self action:@selector(startVoiceRecognize:) forControlEvents:UIControlEventTouchUpInside];
        [_searchBar.cancelButton addTarget:self action:@selector(onCancelSearch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchBar;
}

-(AssociationalWordDisplayController*)keywordDisplayController
{
    if (!_keywordDisplayController) {
        _keywordDisplayController = [[AssociationalWordDisplayController alloc] init];
//        _keywordDisplayController = [[AssociationalWordDisplayController alloc] initWithController:self];
        _keywordDisplayController.delegate = self;
    }
    return _keywordDisplayController;
}


#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.searchBar];
    
    [self.searchBar.cityButton addTarget:self action:@selector(didSelectCity:) forControlEvents:UIControlEventTouchUpInside];
    
//    [self addChildViewController:self.keywordDisplayController];
    [self.view addSubview:self.keywordDisplayController.displayTableView];
    
    if (IsStrEmpty([AppConfig currentConfig].defaultCityName)) {
        [self.searchBar.cityButton setTitle:DEFAULT_CITY_NAME forState:UIControlStateNormal];
    }
    else {
        [self.searchBar.cityButton setTitle:[AppConfig currentConfig].defaultCityName forState:UIControlStateNormal];
    }
//    [LocationService sharedInstance].delegate = self;
//    [[LocationService sharedInstance] startLocation];
    
    self.isSearch = NO;
    
    
    [self.searchBar.searchTextField becomeFirstResponder];
    
}


//- (void)loadView
//{
//    [super loadView];
//    
//    [self.view addSubview:self.searchBar];
//    
//    [self.searchBar.cityButton addTarget:self action:@selector(didSelectCity:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.view addSubview:self.keywordDisplayController.displayTableView];
//    
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    WeakSelf
    [self.historySearchDAO getLatestTwentyKeywordsWithCompletionBlock:^(NSArray *dataList) {
        weakSelf.keywordDisplayController.keywordList = [dataList mutableCopy];
        [weakSelf.keywordDisplayController.displayTableView reloadData];
        
    }];
    
    self.navigationController.delegate = self;
    _navController = self.navigationController;
    
    self.searchBar.frame = CGRectMake(0, STATUS_BAR_HEIGHT, ViewWidth, 44);
    
    self.keywordDisplayController.displayTableView.frame = CGRectMake(0, STATUS_BAR_HEIGHT + 44, ViewWidth, self.view.frame.size.height - STATUS_BAR_HEIGHT - 44);
    
    self.searchBar.searchTextField.text = @"";
    self.searchBar.searchTextField.placeholder = @"请输入搜索关键词";
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_iflyRecognizerView cancel]; //取消识别
    [_iflyRecognizerView setDelegate:nil];
    [_iflyRecognizerView setParameter:@"" forKey:[IFlySpeechConstant PARAMS]];

}


#pragma mark - LocationSearchBar target method

/**
 设置识别参数
 ****/
-(void)initRecognizer
{
    DLog(@"%s",__func__);
    
//    if ([IATConfig sharedInstance].haveView == NO) {//无界面
//        
//        //单例模式，无UI的实例
//        if (_iFlySpeechRecognizer == nil) {
//            _iFlySpeechRecognizer = [IFlySpeechRecognizer sharedInstance];
//            
//            [_iFlySpeechRecognizer setParameter:@"" forKey:[IFlySpeechConstant PARAMS]];
//            
//            //设置听写模式
//            [_iFlySpeechRecognizer setParameter:@"iat" forKey:[IFlySpeechConstant IFLY_DOMAIN]];
//        }
//        _iFlySpeechRecognizer.delegate = self;
//        
//        if (_iFlySpeechRecognizer != nil) {
//            IATConfig *instance = [IATConfig sharedInstance];
//            
//            //设置最长录音时间
//            [_iFlySpeechRecognizer setParameter:instance.speechTimeout forKey:[IFlySpeechConstant SPEECH_TIMEOUT]];
//            //设置后端点
//            [_iFlySpeechRecognizer setParameter:instance.vadEos forKey:[IFlySpeechConstant VAD_EOS]];
//            //设置前端点
//            [_iFlySpeechRecognizer setParameter:instance.vadBos forKey:[IFlySpeechConstant VAD_BOS]];
//            //网络等待时间
//            [_iFlySpeechRecognizer setParameter:@"20000" forKey:[IFlySpeechConstant NET_TIMEOUT]];
//            
//            //设置采样率，推荐使用16K
//            [_iFlySpeechRecognizer setParameter:instance.sampleRate forKey:[IFlySpeechConstant SAMPLE_RATE]];
//            
//            //设置语言
//            [_iFlySpeechRecognizer setParameter:instance.language forKey:[IFlySpeechConstant LANGUAGE]];
//            //设置方言
//            [_iFlySpeechRecognizer setParameter:instance.accent forKey:[IFlySpeechConstant ACCENT]];
//            
//            //设置是否返回标点符号
//            [_iFlySpeechRecognizer setParameter:instance.dot forKey:[IFlySpeechConstant ASR_PTT]];
//            
//        }
//        
//        //初始化录音器
//        if (_pcmRecorder == nil)
//        {
//            _pcmRecorder = [IFlyPcmRecorder sharedInstance];
//        }
//        
//        _pcmRecorder.delegate = self;
//        
//        [_pcmRecorder setSample:[IATConfig sharedInstance].sampleRate];
//        
//        [_pcmRecorder setSaveAudioPath:nil];    //不保存录音文件
//        
//    }else  {//有界面
    
        //单例模式，UI的实例
        if (_iflyRecognizerView == nil) {
            //UI显示剧中
            _iflyRecognizerView= [[IFlyRecognizerView alloc] initWithCenter:self.view.center];
            
            [_iflyRecognizerView setParameter:@"" forKey:[IFlySpeechConstant PARAMS]];
            
            //设置听写模式
            [_iflyRecognizerView setParameter:@"iat" forKey:[IFlySpeechConstant IFLY_DOMAIN]];
            
        }
        _iflyRecognizerView.delegate = self;
        
        if (_iflyRecognizerView != nil) {
            IATConfig *instance = [IATConfig sharedInstance];
            //设置最长录音时间
            [_iflyRecognizerView setParameter:instance.speechTimeout forKey:[IFlySpeechConstant SPEECH_TIMEOUT]];
            //设置后端点
            [_iflyRecognizerView setParameter:instance.vadEos forKey:[IFlySpeechConstant VAD_EOS]];
            //设置前端点
            [_iflyRecognizerView setParameter:instance.vadBos forKey:[IFlySpeechConstant VAD_BOS]];
            //网络等待时间
            [_iflyRecognizerView setParameter:@"20000" forKey:[IFlySpeechConstant NET_TIMEOUT]];
            
            //设置采样率，推荐使用16K
            [_iflyRecognizerView setParameter:instance.sampleRate forKey:[IFlySpeechConstant SAMPLE_RATE]];
            if ([instance.language isEqualToString:[IATConfig chinese]]) {
                //设置语言
                [_iflyRecognizerView setParameter:instance.language forKey:[IFlySpeechConstant LANGUAGE]];
                //设置方言
                [_iflyRecognizerView setParameter:instance.accent forKey:[IFlySpeechConstant ACCENT]];
            }else if ([instance.language isEqualToString:[IATConfig english]]) {
                //设置语言
                [_iflyRecognizerView setParameter:instance.language forKey:[IFlySpeechConstant LANGUAGE]];
            }
            //设置是否返回标点符号
            [_iflyRecognizerView setParameter:instance.dot forKey:[IFlySpeechConstant ASR_PTT]];
            
        }
//    }
}

-(void)startVoiceRecognize:(id)sender
{
    NSLog(@"%s[IN]",__func__);
    
//    if ([IATConfig sharedInstance].haveView == NO) {//无界面
//        
////        [_textView setText:@""];
////        [_textView resignFirstResponder];
//        self.searchBar.searchTextField.text = @"";
//        [self.searchBar resignFirstResponder];
//        
//        self.isCanceled = NO;
//        self.isStreamRec = NO;
//        
//        if(_iFlySpeechRecognizer == nil)
//        {
//            [self initRecognizer];
//        }
//        
//        [_iFlySpeechRecognizer cancel];
//        
//        //设置音频来源为麦克风
//        [_iFlySpeechRecognizer setParameter:IFLY_AUDIO_SOURCE_MIC forKey:@"audio_source"];
//        
//        //设置听写结果格式为json
//        [_iFlySpeechRecognizer setParameter:@"json" forKey:[IFlySpeechConstant RESULT_TYPE]];
//        
//        //保存录音文件，保存在sdk工作路径中，如未设置工作路径，则默认保存在library/cache下
//        [_iFlySpeechRecognizer setParameter:@"asr.pcm" forKey:[IFlySpeechConstant ASR_AUDIO_PATH]];
//        
//        [_iFlySpeechRecognizer setDelegate:self];
//        
//        BOOL ret = [_iFlySpeechRecognizer startListening];
//        
//        if (ret) {
//            [_audioStreamBtn setEnabled:NO];
//            [_upWordListBtn setEnabled:NO];
//            [_upContactBtn setEnabled:NO];
//            
//        }else{
//            [_popUpView showText: @"启动识别服务失败，请稍后重试"];//可能是上次请求未结束，暂不支持多路并发
//        }
//    }else {
    
        if(_iflyRecognizerView == nil)
        {
            [self initRecognizer ];
        }
        
//        [_textView setText:@""];
//        [_textView resignFirstResponder];
    
        self.searchBar.searchTextField.text = @"";
        [self.searchBar.searchTextField resignFirstResponder];
    
        //设置音频来源为麦克风
        [_iflyRecognizerView setParameter:IFLY_AUDIO_SOURCE_MIC forKey:@"audio_source"];
        
        //设置听写结果格式为json
        [_iflyRecognizerView setParameter:@"plain" forKey:[IFlySpeechConstant RESULT_TYPE]];
        
        //保存录音文件，保存在sdk工作路径中，如未设置工作路径，则默认保存在library/cache下
        [_iflyRecognizerView setParameter:@"asr.pcm" forKey:[IFlySpeechConstant ASR_AUDIO_PATH]];
        
        BOOL ret = [_iflyRecognizerView start];
        if (ret) {
//            [_startRecBtn setEnabled:NO];
//            [_audioStreamBtn setEnabled:NO];
//            [_upWordListBtn setEnabled:NO];
//            [_upContactBtn setEnabled:NO];
        }
//    }
}

/*!
 *  回调返回识别结果
 *
 *  @param resultArray 识别结果，NSArray的第一个元素为NSDictionary，NSDictionary的key为识别结果，sc为识别结果的置信度
 *  @param isLast      -[out] 是否最后一个结果
 */
- (void)onResult:(NSArray *)resultArray isLast:(BOOL) isLast;
{
    NSMutableString *result = [[NSMutableString alloc] init];
    NSDictionary *dic = [resultArray objectAtIndex:0];
    
    for (NSString *key in dic) {
        [result appendFormat:@"%@",key];
    }
    self.searchBar.searchTextField.text = [NSString stringWithFormat:@"%@%@",self.searchBar.searchTextField.text,result];
    NSString *searchCity = self.searchBar.cityButton.titleLabel.text;
    [self searchKeywords:self.searchBar.searchTextField.text.trim city:searchCity];
}

/*!
 *  识别结束回调
 *
 *  @param error 识别结束错误码
 */
- (void)onError: (IFlySpeechError *) error;
{
//    [MsgToolBox showToast:@"识别结束"];
}

- (void)onCancelSearch:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)navigationController:(UINavigationController*)navigationController willShowViewController:(UIViewController*)viewController animated:(BOOL)animated
{
    if(viewController ==self){
        [_navController setNavigationBarHidden:YES animated:YES];
    }else{
        //不在本页时，显示真正的nav bar
        [_navController setNavigationBarHidden:NO animated:YES];
        //当不显示本页时，要么就push到下一页，要么就被pop了，那么就将delegate设置为nil，防止出现BAD ACCESS
        //之前将这段代码放在viewDidDisappear和dealloc中，这两种情况可能已经被pop了，self.navigationController为nil，这里采用手动持有navigationController的引用来解决
        if(_navController.delegate == self){
            //如果delegate是自己才设置为nil，因为viewWillAppear调用的比此方法较早，其他controller如果设置了delegate就可能会被误伤
            _navController.delegate=nil;
        }
    }
}

#pragma mark - LocationSearchBar delegate methods

- (void)searchBar:(UITextField *)searchField textDidChange:(NSString *)searchText
{
    if (IsStrEmpty(searchText)) {
        
//        WeakSelf
//        [self.historySearchDAO getLatestTwentyKeywordsWithCompletionBlock:^(NSArray *dataList) {
//            weakSelf.keywordDisplayController.keywordList = dataList;
//            [weakSelf.keywordDisplayController.displayTableView reloadData];
//            weakSelf.isSearch = NO;
//        }];
    }
    else {
        
        NSString *searchCity = self.searchBar.cityButton.titleLabel.text;
        [self searchKeywords:searchField.text city:searchCity];
    }
}

-(void)searchDetailAddress:(NSArray*)uidList
{

//    uid	是	无	‘8ee4560cf91d160e6cc02cd7’	poi的uid
//    uids	是	无	‘8ee4560cf91d160e6cc02cd7’,‘5ffb1816cf771a226f476058’	uid的集合，最多可以传入10个uid，多个uid之间用英文逗号分隔。
//    output	否	xml	json或xml	请求返回格式
//    scope	是	1	1、2	检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息
//    ak	是	无	您的ak	用户的访问密钥，必填项。v2之前该属性为key。
//    sn	否	无		用户的权限签名。
//    timestamp	否	无		设置sn后该值必填。
    
    NSString *url = [NSString stringWithFormat:@"%@?uids=%@&output=json&scope=1&ak=%@", BAIDU_PLACE_API, [uidList componentsJoinedByString:@","], Baidu_Web_AK];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startHttpGet:url params:nil withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        if (code_status == 0) {
            
            NSArray *placeResults = result[@"result"];
            for (NSDictionary *dict in placeResults) {
                NSString *uid = dict[@"uid"];
                [weakSelf.keywordDisplayController.keywordList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
//                    if (!*stop) {
                        LocationSearchModel *model = obj;
                        if ([model.uid isEqualToString:uid]) {
                            model.address = dict[@"address"];
                            *stop = YES;
                        }
//                    }

                    
                }];
            }
            
            [weakSelf.keywordDisplayController.displayTableView reloadData];
            weakSelf.isSearch = YES;
            
        }
        
        
    }];
    
}

-(void)searchKeywords:(NSString*)keyword city:(NSString*)city
{
    
    NSString *url = [NSString stringWithFormat:@"%@?query=%@&region=%@&city_limit=true&output=json&ak=%@", BAIDU_SUGGESTION_PLACE_API, keyword, city, Baidu_Web_AK];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self.keywordDisplayController.keywordList removeAllObjects];
    
    WeakSelf
    [[NetworkManager sharedInstance] startHttpGet:url params:nil withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
//        StrongSelf
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        if (code_status == 0) {
            
            LocationSearchResponseData *responseData = [[LocationSearchResponseData alloc] initWithDictionary:result error:nil];
            
            __block NSMutableArray *uidList = [NSMutableArray new];
            
            for (LocationSearchModel *data in responseData.result) {
                
                if (data.location != nil) {
                    if ([data.location[@"lat"] doubleValue] > 0.0f && [data.location[@"lng"] doubleValue] > 0.0f) {
                        [weakSelf.keywordDisplayController.keywordList addObject:data];
                        [uidList addObject:data.uid];
                    }
                }
                
            }
            
            // 查询详细地址
            [weakSelf searchDetailAddress:uidList];
            
        }
        else if (code_status == NETWORK_FAILED) {
//            [weakSelf hideHUDIndicatorViewAtCenter];
            [MsgToolBox showToast:error];
        }
        else {
            NSString *msg = result[@"message"];
            [MsgToolBox showToast:msg];
        }
        
        
    }];
    
#if 0

    NSDictionary *dict = @{@"region":city,
                           @"region_fix":@1,
                           @"keyword": keyword,
                           @"key": TENCENT_MAP_APP_KEY,
                           @"policy": @1};
    WeakSelf
    [self showHUDIndicatorViewAtCenter:MSG_SEARCHING];
    [[NetworkManager sharedInstance] startHttpGet:TENCENT_MAP_API_URL params:dict withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (code_status == 0) {
            LocationSearchResponseData *responseData = [[LocationSearchResponseData alloc] initWithDictionary:result error:nil];
            strongSelf.keywordDisplayController.keywordList = responseData.data;
            [strongSelf.keywordDisplayController.displayTableView reloadData];
            strongSelf.isSearch = YES;
        }
        else if (code_status == NETWORK_FAILED) {
            [weakSelf showAlertView:error];
        }
        else {
            NSString *msg = result[@"message"];
            if (!IsStrEmpty(msg)) {
                [weakSelf showAlertView:msg];
            }
        }
    }];
    
#endif
}

#pragma mark -

-(void)didSelectAssociationalWord:(LocationSearchModel*)data;
{
    CommunityHelper *communityHelper = [CommunityHelper new];
    if (self.isSearch) {
        data.areaCode = [communityHelper getCityIdFromCityName:self.searchBar.cityButton.titleLabel.text];
    }
    
    [self.historySearchDAO insertHistoyrLocation:data time:[NSNumber numberWithDouble:[NSDate timeStamp]]];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectLocation:)]) {
        [self.delegate didSelectLocation:data.title];
    }
    else {
        if (self.selectLocationHandle) {
            self.selectLocationHandle(data.title, data.areaCode);
        }
        else if (self.selectAddressHandle) {
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([data.location[@"lat"] doubleValue], [data.location[@"lng"] doubleValue]);
            self.selectAddressHandle(data.name, data.address, data.city, data.areaCode, coordinate);
            
//            self.selectAddressHandle(data.name, data.city, data.areaCode, coordinate);
        }
    }

    [self dismissViewControllerAnimated:YES completion:nil];
    
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didScrollView
{
    [self.searchBar.searchTextField resignFirstResponder];
}

- (void)didSelectCity:(id)sender
{
    GYZChooseCityController *cityPickerVC = [[GYZChooseCityController alloc] init];
    [cityPickerVC setDelegate:self];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:cityPickerVC] animated:YES completion:^{
//
    }];
}

#pragma mark - GYZCityPickerDelegate
- (void) cityPickerController:(GYZChooseCityController *)chooseCityController didSelectCity:(GYZCity *)city
{
    [self.searchBar.cityButton setTitle:city.cityName forState:UIControlStateNormal];
//    [LocationService saveCityToConfig:city.cityName];
    self.searchBar.searchTextField.text = @"";
    [chooseCityController dismissViewControllerAnimated:YES completion:nil];
}

- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController
{
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - Location Service Delegate methods

- (void)onLocateCityServiceUnable;
{
    
    [self.searchBar.cityButton setTitle:DEFAULT_CITY_NAME forState:UIControlStateNormal];
    [MsgToolBox showAlert:@"提示" content:@"无法获取您当前位置!"];
}

- (void)onLocateCityUserDenied;
{
    [self.searchBar.cityButton setTitle:DEFAULT_CITY_NAME forState:UIControlStateNormal];
    [MsgToolBox showAlert:@"定位服务未开启" content:@"请在系统设置中开启定位服务"];
}

- (void)onLocateCitySuccess:(NSString*)city;
{
    [self.searchBar.cityButton setTitle:city forState:UIControlStateNormal];
    [LocationService saveCityToConfig:city];
}

- (void)onLocateCityFail;
{
    [MsgToolBox showAlert:@"提示" content:@"定位城市失败，请重试!"];
}

- (void)onLocateCityAddressFail;
{
    [MsgToolBox showAlert:@"提示" content:@"获取地址失败，请重试!"];
}

#pragma mark - didReceiveMemoryWarning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
