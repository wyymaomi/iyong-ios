//
//  MainTabBarController.m
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MainTabBarController.h"
#import "BaseViewController.h"
#import "UserManager.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "GestureLoginViewController.h"
#import "CommunityListViewController.h"
#import "CommunityCategoryViewController.h"
#import "CustomTabBar.h"
#import "OrderCountModel.h"
#import "PublishViewController.h"
#import "MainTabBarController+AccountInfo.h"



@interface MainTabBarController ()<CustomTabBarDelegate, UITabBarControllerDelegate, LoginDelegate, GesturePasswordControllerDelegate,AccountInfoViewDelegate>

//@property (nonatomic, strong) NSArray *tabConfigArray;

@end

@implementation MainTabBarController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLogout) name:kOnLogoutNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLoginSuccess) name:kLoginSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUnreadMessageCount) name:@"setupUnreadMessageCount" object:nil];
    
//    [self createControllers];
    
    [self setupControllers];

    [self initTabBar];
    
    
    self.delegate = self;
    // Do any additional setup after loading the view.
    
//    [self initIntroduceView];
    
    
}



- (void)customTabBarUI {

}

//- (void)initIntroduceView
//{
//    //    if (![USERDEFAULT objectForKey:@"IsShowIntro"]) {
//    UIImageView *introImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, ViewHeight)];
//    //        introImg.tag = INTRO_TAG;
//    //        introImg.userInteractionEnabled = YES;
//    //        if (iPhone4S) {
//    //            introImg.image = [UIImage imageNamed:@"explanation_960"];
//    //        }
//    //        else
//    //        {
//    introImg.image = [UIImage imageNamed:@"function_guide1"];
//    introImg.contentMode = UIViewContentModeScaleToFill;
//    //        }
//    [self.tabBarController.view addSubview:introImg];
//    
//    //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeIntroImg)];
//    //        tap.numberOfTapsRequired = 1;
//    
//    //        [introImg addGestureRecognizer:tap];
//    //    }
//    
//}

-(void)initTabBar
{
    CustomTabBar *tabbar = [[CustomTabBar alloc] init];
    tabbar.delegate = self;
    [self setValue:tabbar forKeyPath:@"tabBar"];
    
//    CustomTabBar *customTabBar = [[CustomTabBar alloc] init];
//    customTabBar.frame = self.tabBar.bounds;
//    customTabBar.delegate = self;
//    self.tabBar addSub
    
    UITabBar *tabBar = [self tabBar];
    self.selectedIndex = 0;
//    [self tabBarController:self shouldSelectViewController:_communityNavigation];
    
//    [tabBar setShadowImage:[UIImage imageNamed:@"img_tab_bar"]];
    
//    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, tabBar.frame.size.height)];
//    bgView.backgroundColor = [UIColor whiteColor];//NAVBAR_BGCOLOR;
//    [tabBar insertSubview:bgView atIndex:0];
    
    //设置tabbar的颜色
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x333333), NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica" size:12.0f],NSFontAttributeName,nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x09a892), NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica" size:12.0f],NSFontAttributeName,nil] forState:UIControlStateSelected];
    
    //通过这两个参数来调整badge位置
    [tabBar setTabIconWidth:33.33];
//    [tabBar setTabIconWidth:self.tabBar.frame.size.width/5-10];
    [tabBar setBadgeTop:9];
    
}

- (void)onLoginSuccess
{
    self.selectedIndex = self.tabIndex;
    
    //    self.selectedIndex = _tabIndex;
    
//    self.selectedIndex = _tabIndex;
//    if (self.selectedIndex == 2) {
//        //        CustomTabBar *tabBar = (CustomTabBar*)[self tabBar];
//        //        tabBar.centerButton.selected = YES;
//        [self tabBarController:self shouldSelectViewController:_communityNavigation];
//    }
    
    // 读取订单数量
//    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_COUNT_API];
//    WeakSelf
//    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary *responseDict) {
//        StrongSelf
//        
//        NSInteger code_status = [responseDict[@"code"] integerValue];
//        if (code_status == STATUS_OK) {
//            
//            NSDictionary *data = responseDict[@"data"];
//            //            strongSelf.orderCountModel = [[OrderCountModel alloc] initWithDictionary:data error:nil];
//            
//            OrderCountModel *orderCountModel = [[OrderCountModel alloc] initWithDictionary:data error:nil];
//            NSInteger arrangeCarCount = [orderCountModel.arrangeCar integerValue];
//            NSInteger fillWaybillCount = [orderCountModel.fillWaybill integerValue];
//            NSInteger notBargainCount = [orderCountModel.notBargain integerValue];
//            NSInteger strokeCount = [orderCountModel.stroke integerValue];
//            NSInteger unpaidCount = [orderCountModel.unpaid integerValue];
//            NSInteger untreatedCount = [orderCountModel.untreated integerValue];
//            
//            NSInteger count = arrangeCarCount + fillWaybillCount + notBargainCount + strokeCount + unpaidCount + untreatedCount;
//            if (count > 0) {
//                [strongSelf.tabBar setBadgeStyle:kCustomBadgeStyleRedDot value:count atIndex:1];
//            }
//            else {
//                [strongSelf.tabBar setBadgeStyle:kCustomBadgeStyleNone value:0 atIndex:1];
//            }
//            
//        }
//        
//    } andFailure:^(NSString *errorDesc) {
//        
//    }];
    
}

- (void)onLogout
{
    [self.tabBar setBadgeStyle:kCustomBadgeStyleNone value:0 atIndex:1];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    // 设置社区按钮和文字为默认状态
    CustomTabBar *tabBar = (CustomTabBar*)[self tabBar];
    
    if (viewController == _homeNavigation) {
        tabBar.centerButton.selected = NO;
        tabBar.titleLabel.textColor = UIColorFromRGB(0x333333);
        return YES;
    }
    
    
    AppDelegate *appDelegate = APP_DELEGATE;
    
    if (![[UserManager sharedInstance] isLogin]) {
        [self showLoginAlertView];
        return NO;
    }
    
    if ([appDelegate isLoginOvertime]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
        return YES;
    }
    
    
    if (viewController == _communityNavigation) {
        return NO;
    }
    
    if (viewController == _homeNavigation || viewController == _orderNavigation || viewController == _orderNavigation || viewController == self.meNavigation) {
//        tabBar.centerButton.isSelected = NO;
        tabBar.centerButton.selected = NO;
        tabBar.titleLabel.textColor = UIColorFromRGB(0x333333);
    }
    
    return YES;
}

-(void)onGestureLoginSuccess;
{
    self.selectedIndex = self.tabIndex;
}

//-(void)onLoginSuccess
//{
//
//}

- (void)setCenterButtonSelected:(BOOL)selected
{
    CustomTabBar *tabBar = (CustomTabBar*)[self tabBar];
    tabBar.centerButton.selected = selected;
}

- (void)setupControllers
{
    
    for (int i=0; i<self.tabConfigArray.count; i++) {
        
        NSDictionary *dictionary = self.tabConfigArray[i];
        
        Class cls = NSClassFromString(dictionary[@"viewController"]);
//        Class cls=NSClassFromString(ctrlArray[i]);
        
        BaseViewController *vc=[[cls alloc]init];
        vc.source_from = SOURCE_FROM_TAB_CONTROL;
        vc.hasLeftBackButton = NO;
        vc.tabBarItem.image = [[UIImage imageNamed:dictionary[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:dictionary[@"icon_focused"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        vc.tabBarController.tabBar.barTintColor = NAVBAR_BGCOLOR;
        vc.tabBarItem.title = dictionary[@"name"];
        
        UINavigationController *navCtrl=[[UINavigationController alloc] initWithRootViewController:vc];
        
        if (i==0) {
            _homeNavigation = navCtrl;
        }
        if (i==1) {
            _communityNavigation = navCtrl;
        }
        if (i==2) {
            self.meNavigation = navCtrl;
            _meViewController = (MeViewController*)vc;
        }
        
        [self addChildViewController:navCtrl];
    }
}

#if 0
-(void)createControllers
{
    NSArray *ctrlArray=@[@"ServiceProviderRootViewController",
                         @"InquireViewController",
                         /*@"CommunityListViewController",@"CommunityCategoryViewController",*/
                         /*@"BillManageViewController",*/@"BusinessCircleListViewController",
                         @"MyViewController"];
    ////////////////////
    NSArray *nameArray=@[@"首页",@"询价",/*@"发布",*/@"报价",@"我"];
    NSArray *imageArray=@[@"icon_tab_home",@"icon_tab_enquiry",/*@"icon_tab_plus",*/ @"icon_tab_offer",@"icon_tab_me"];
    NSArray *selectedArray=@[@"icon_tab_home_focused",@"icon_tab_enquiry_focused",/*@"icon_tab_plus",*/@"icon_tab_offer_focused",@"icon_tab_me_focused"];
    
//    NSMutableArray *navArray=[[NSMutableArray alloc]init];
    for (int i=0; i<ctrlArray.count; i++) {
        
        Class cls=NSClassFromString(ctrlArray[i]);
        
        BaseViewController *vc=[[cls alloc]init];
        vc.source_from = SOURCE_FROM_TAB_CONTROL;
        vc.hasLeftBackButton = NO;
        vc.tabBarItem.image=[[UIImage imageNamed:imageArray[i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage=[[UIImage imageNamed:selectedArray[i]]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarController.tabBar.barTintColor = NAVBAR_BGCOLOR;
        vc.tabBarItem.title = nameArray[i];
//        [vc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(2, -5)];
        
        UINavigationController *navCtrl=[[UINavigationController alloc] initWithRootViewController:vc];
        
//        if (i == 0) {
//            _homeNavigation = navCtrl;
//        }
//        if (i == 1) {
//            _orderNavigation = navCtrl;
//            
//        }
//        if (i == 2) {
//            _communityNavigation = navCtrl;
////            vc.tabBarItem.image = nil;
////            vc.tabBarItem.selectedImage = nil;
//        }
//        if (i == 3) {
//            _billNavigation = navCtrl;
//        }
//        if (i == 4) {
//            _meNavigation = navCtrl;
//        }
        
        if (i == 3){
            _meNavigation = navCtrl;
        }
        
        [self addChildViewController:navCtrl];
    }
}
#endif


#pragma mark - CustomTabBarDelegate
-(void)tabBarCenterButtonClick:(CustomTabBar*)tabBar;
{
#if 0
    if ([self canLogin]) {
        
        PublishViewController *publishViewController = [[PublishViewController alloc] init];
         [self presentViewController:publishViewController animated:YES completion:nil];
        
    }
#endif
    
    if ([self canLogin]) {
        
//        PublishViewController *viewController = [[PublishViewController alloc] init];
//        [self presentViewController:viewController animated:YES completion:nil];
        
        
        PostExperienceViewController *viewController = [[PostExperienceViewController alloc] init];
        viewController.type = TableTypeUserExperience;
        viewController.enter_type = ENTER_TYPE_PRESENT;
        viewController.title = @"发布体验";
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
//        self.navigationController pushViewController:navigation animated:<#(BOOL)#>
        [self presentViewController:navigationController animated:YES completion:nil];
        
    }
    
    return;
    
    //    if ([self canLogin]) {
    //
    //        CommunityCategoryViewController *viewController = [[CommunityCategoryViewController alloc] init];
    //        viewController.enter_type = ENTER_TYPE_PRESENT;
    //
    ////        CommunityListViewController *viewController = [[CommunityListViewController alloc] init];
    ////        viewController.enter_type = ENTER_TYPE_PRESENT;
    //        UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:viewController];
    //        [self presentViewController:navVc animated:YES completion:nil];
    //
    //    }
    
        self.selectedIndex = 1;
    //    if ([self canLogin]) {
    
//    [self tabBarController:self shouldSelectViewController:_communityNavigation];
//        }

    
    //    [self presentViewController:_communityNavigation animated:YES completion:nil];
    
    //    LBpostViewController *plusVC = [[LBpostViewController alloc] init];
    //    plusVC.view.backgroundColor = [self randomColor];
    //
    //    LBNavigationController *navVc = [[LBNavigationController alloc] initWithRootViewController:plusVC];
    //
    //    [self presentViewController:navVc animated:YES completion:nil];
    
    
    
}

-(BOOL)canLogin
{
    //当应用进入后台时间超过10分钟，重新进入弹出登录界面
    AppDelegate *appDelegate = APP_DELEGATE;

    if (![[UserManager sharedInstance] isLogin]) {
        [self showLoginAlertView];
        return NO;
    }
    if ([appDelegate isLoginOvertime]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kOnLoginOvertimeNotification object:nil];
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - getter and setter

- (AccountInfoView*)accountInfoView
{
    if (!_accountInfoView) {
        _accountInfoView = [[AccountInfoView alloc] init];
        _accountInfoView.delegate = self;
        _accountInfoView.isNeedAnimation = YES;
        _accountInfoView.isVisible = NO;
    }
    return _accountInfoView;
}




@end
