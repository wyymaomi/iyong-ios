//
//  ProviderServiceTypeTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderServiceTypeTableViewCell.h"

//static



@implementation ProviderServiceTypeTableViewCell

//- (NSArray *)vechileServiceList
//{
//    if (IsNilOrNull(_vechileServiceList)) {
//        // 加载plist中的字典数组
//        NSString *path = [[NSBundle mainBundle] pathForResource:VECHILE_SERVICE_PLIST ofType:nil];
//        _vechileServiceList = [NSArray arrayWithContentsOfFile:path];
//    }
//    return _vechileServiceList;
//}

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    static NSString *ID = @"ProviderServiceTypeTableViewCell";
    ProviderServiceTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(24, 15, 2, 17)];
        view.backgroundColor = NAVBAR_BGCOLOR;
        [cell.contentView addSubview:view];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, view.top-1, ViewWidth, view.height)];
        titleLabel.font = FONT(17);
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = @"服务类型";
        titleLabel.backgroundColor = [UIColor clearColor];
        [titleLabel sizeToFit];
        [cell.contentView addSubview:titleLabel];
        
//        _imageArray = [NSMutableArray array];
//        _typeNameArray = @[@"logo_commercial", @"logo_travel", @"logo_airport", @"logo_goods", @"logo_wedding", @"logo_chartered", @"logo_truck", @"logo_bus"];
//        
//        NSArray *nameArray = @[@"商务用车", @"旅游用车", @"机场接送", @"货物用车", @"婚庆用车", @"包车服务", @"拖车服务", @"企业班车"];
        
        NSInteger number = 8;
        NSInteger startX = 32;
        NSInteger startY = titleLabel.bottom+17;
        NSInteger imgWidth = 50, imgHeight = 42.5;
        NSInteger separatorX = (ViewWidth-startX * 2 - imgWidth * 4)/3,separatorY = 33;
        
        for (NSInteger i = 0; i < number; i++) {
            
            NSDictionary *dictionary = serviceTypeList()[i];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(startX + (imgWidth + separatorX) * (i % 4), startY + (imgHeight + separatorY) * (i / 4), imgWidth, imgHeight)];
            NSString *imgName = [NSString stringWithFormat:@"%@_gray", dictionary[@"icon"]];
            imageView.image = [UIImage imageNamed:imgName];
            NSString *hightLightName = [NSString stringWithFormat:@"%@_blue", dictionary[@"icon"]];
            imageView.highlightedImage = [UIImage imageNamed:hightLightName];
            [cell.contentView addSubview:imageView];
            [imageView setHighlighted:NO];
            imageView.tag = i+1;
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(startX+(imgWidth+separatorX)*(i%4), imageView.bottom+5, imgWidth, 20)];
            label.font = FONT(11);
            label.textAlignment = UITextAlignmentCenter;
            label.text = dictionary[@"title"];//nameArray[i];
            label.highlightedTextColor = NAVBAR_BGCOLOR;
            label.textColor = UIColorFromRGB(0xBEBEBE);
            label.tag = (i+1)*1000;
            [cell.contentView addSubview:label];
            [label setHighlighted:NO];
        }
        
    }
    return cell;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    self.lineView.frame
    
    self.lineView.frame = CGRectMake(12, self.height-SEPARATOR_LINE_HEIGHT, ViewWidth-24,SEPARATOR_LINE_HEIGHT);
    
//    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(24, self.height-0.5, ViewWidth-48, 0.5)];
//    line.tag = 1000;
//    line.backgroundColor = UIColorFromRGB(0xEBEBEB);
//    [self.contentView addSubview:line];
    
//    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(24, self.height-0.5, ViewWidth-48, 0.5)];
//    line.tag = 1000;
//    line.backgroundColor = UIColorFromRGB(0x9a9a9a);
//    [self.contentView addSubview:line];
}

- (UIView*)lineView
{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = UIColorFromRGB(0xEBEBEB);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

+(CGFloat)getCellHeight
{
    return 200;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
