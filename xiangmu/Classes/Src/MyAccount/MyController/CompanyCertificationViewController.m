//
//  CompanyCertificationViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CompanyCertificationViewController.h"
#import "CommonEditTableViewCell.h"
#import "UploadImageManage.h"
#import "GTMBase64.h"
#import "CompanyCertifcationHttpMessage.h"
#import "CompanyCertificationModel.h"
#import "InputHelperViewController.h"
#import "ActionSheetCityPicker.h"
#import "CameraPhotoService.h"
#import "AliyunOSSManager.h"

// 未认证：可修改
// 待认证：不可修改
// 已认证：可修改经营地址
// 已拒绝：可修改

@interface CompanyCertificationViewController ()</*UploadImageManageDelegate,*/InputHelperDelegate>

//@property (nonatomic, strong) UploadImageManage *uploadImageManager;

@property (nonatomic, strong) CameraPhotoService *cameraPhotoService;

@property (nonatomic, strong) CompanyCertifcationHttpMessage *httpMessage;
@property (nonatomic, strong) CompanyCertificationModel *model;

@property (nonatomic, strong) NSData *imageData;//


@end

@implementation CompanyCertificationViewController


- (CameraPhotoService *)cameraPhotoService
{
    if (!_cameraPhotoService) {
        WeakSelf
        _cameraPhotoService = [[CameraPhotoService alloc] initWithViewController:self.navigationController type:CameraPhotoLarge completeBlock:^(NSInteger tag){
            StrongSelf
            [strongSelf.plainTableView reloadData];
        } onGetImageData:^(NSData *imageData, NSInteger tag) {
            StrongSelf
            strongSelf.imageData = imageData;
            [strongSelf.plainTableView reloadData];
//            strongSelf.httpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imageData] encoding:NSUTF8StringEncoding];
        }];
    }
    return _cameraPhotoService;
}

-(CompanyCertifcationHttpMessage*)httpMessage
{
    if (nil == _httpMessage) {
        _httpMessage = [[CompanyCertifcationHttpMessage alloc] init];
    }
    
    return _httpMessage;
}

-(CompanyCertificationViewModel*)viewModel
{
    if (_viewModel == nil) {
        _viewModel = [CompanyCertificationViewModel new];
    }
    return _viewModel;
}


#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"企业认证";
    
    NSInteger status = [[UserManager sharedInstance].userModel.companyCertification integerValue];
    if (status == CompanyCertificationStatusInit || status == CompanyCertificationStatusRejected) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveCompanyCertifcation)];
    }
    else if (status == CompanyCertificationStatusFinished) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(updateCompanyCertification)];
    }
    
    [self.view addSubview:self.plainTableView];
    
    [self getCompanyCertficationInfo];
    
}

-(void)pressedBackButton
{
    BOOL isCompanyCertification = [UserManager sharedInstance].userModel.isCompanyCertified;
    if (!isCompanyCertification) {
        BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"是否放弃企业认证" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定"];
        [alertView setConfirmBlock:^{
            [super pressedBackButton];
        }];
        [alertView show];
    }
    else {
        [super pressedBackButton];
    }
}

- (void)dealloc
{
    _imageData = nil;
//    self.httpMessage.fileContent = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - business methods

- (void)getCompanyCertficationInfo
{
    WeakSelf
    [self.viewModel getCompanyCertificationInfo:^(id responseData) {
        StrongSelf
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *dictionary = responseData[@"data"];
            strongSelf.model = [[CompanyCertificationModel alloc] initWithDictionary:dictionary error:nil];
            strongSelf.httpMessage.address = strongSelf.model.address;
            strongSelf.httpMessage.areaName = strongSelf.model.areaName;
            strongSelf.httpMessage.areaCode = strongSelf.model.areaCode;
            strongSelf.httpMessage.name = strongSelf.model.name;
            [strongSelf.plainTableView reloadData];
            
            NSString *businessLicenceUrl = strongSelf.model.businessLicenceUrl;
            [strongSelf getLicenseUrl:businessLicenceUrl];
        }
    } failure:^(NSString *errorDesc) {
        
    }];
}

- (void)getLicenseUrl:(NSString*)businessLicenceUrl
{
    if (!IsStrEmpty(businessLicenceUrl)) {
        WeakSelf
//        [[NetworkManager sharedInstance] downloadImage:businessLicenceUrl success:^(id responseData) {
//            StrongSelf
//            if ([responseData isKindOfClass:[NSData class]]) {
//                NSData *imageData = responseData;
//                if (imageData != nil && imageData.length > 0) {
//                    strongSelf.imageData = imageData;
////                    strongSelf.httpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imageData] encoding:NSUTF8StringEncoding];
//                    [strongSelf.plainTableView reloadData];
//                }
//            }
//        } andFailure:^(NSString *errorDesc) {
//            
//        }];
        
        // 如果本地已经缓存图片
//        NSString *convertImgUrl = [businessLicenceUrl replaceAll:@"/" with:@"-"];
//        if (isThumbnail) {
//            convertImgUrl = [convertImgUrl substringToIndex:[convertImgUrl indexOfString:@"."]];
//            convertImgUrl = [convertImgUrl stringByAppendingString:@"-thumbnail.png"];
//        }
        
        // 如果本地已经缓存图片
        NSString *convertImgUrl = [businessLicenceUrl replaceAll:@"/" with:@"-"];
//        if (isThumbnail) {
//            convertImgUrl = [convertImgUrl substringToIndex:[convertImgUrl indexOfString:@"."]];
//            convertImgUrl = [convertImgUrl stringByAppendingString:@"-thumbnail.png"];
//        }
        
        NSData *imageData = [[FileCache defaultCache] dataForKey:convertImgUrl];
        if (imageData != nil && imageData.length > 0) {
            self.imageData = imageData;
            [self.plainTableView reloadData];
            return;
        }

        [[AliyunOSSManager sharedInstance] downloadImage:businessLicenceUrl isThumbnail:NO complete:^(DownloadImageState state, NSData *data) {
           
            if (state == DownloadImageSuccess) {
                weakSelf.imageData = data;
                [[FileCache defaultCache] saveData:data forKey:convertImgUrl];
                [weakSelf.plainTableView reloadData];
            }
            
        }];
    }
}

- (void)updateCompanyCertification
{
    NSInteger status = [[UserManager sharedInstance].userModel.companyCertification integerValue];
    if (status != CompanyCertificationStatusFinished) {
        return;
    }
    if (_imageData == nil || _imageData.length == 0) {
        [MsgToolBox showToast:@"请上传营业执照"];
        return;
    }
    
    [self saveNextAction:@selector(updateCompanyCertification) object1:nil object2:nil];
    
    NSArray *array = @[_imageData];
    [self showHUDIndicatorViewAtCenter:@"正在更新企业认证信息，请稍候"];
    WeakSelf
    [[AliyunOSSManager sharedInstance] uploadImages:array isAsync:YES folderName:kAliyunOSSPrivateImagePath([UserManager sharedInstance].userModel.companyId) complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        StrongSelf
        
        if (state == UploadImageSuccess) {
            strongSelf.httpMessage.businessLicenceUrl = names[0];
            
            NSString *params = [NSString stringWithFormat:@"address=%@", self.httpMessage.address];
            [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(COMPANY_CERTIFICATION_API) requestMethod:POST params:params success:^(id responseData) {
                
                [strongSelf hideHUDIndicatorViewAtCenter];
                
                [strongSelf resetAction];
                
                NSInteger code_status = [responseData[@"code"] integerValue];
                if (code_status == STATUS_OK) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"企业认证信息更新成功" delegate:nil cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
                    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                        if (buttonIndex == 0) {
                            [strongSelf.navigationController popViewControllerAnimated:YES];
                        }
                    }];
                }
                else {
                    [MsgToolBox showToast:getErrorMsg(code_status)];
                }
                
            } andFailure:^(NSString *errorDesc) {
                
                StrongSelf
                [strongSelf resetAction];
                [strongSelf hideHUDIndicatorViewAtCenter];
                [strongSelf showAlertView:errorDesc];
                
            }];
            
        }
        else {
            
        }
    }];
    
//    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, UPDATE_COMPANY_CERTIFICATION_API];
//    NSString *params = [NSString stringWithFormat:@"address=%@", self.httpMessage.address];
//    [self showHUDIndicatorViewAtCenter:@"正在更新企业认证信息，请稍候"];
//    WeakSelf
//    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
//        StrongSelf
//        [strongSelf hideHUDIndicatorViewAtCenter];
//        [strongSelf resetAction];
//        
//        NSInteger code_status = [responseData[@"code"] integerValue];
//        if (code_status == STATUS_OK) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"企业认证信息更新成功" delegate:nil cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
//            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//                if (buttonIndex == 0) {
//                    [strongSelf.navigationController popViewControllerAnimated:YES];
//                }
//            }];
//        }
//        else {
//            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
//        }
//    } andFailure:^(NSString *errorDesc) {
//        StrongSelf
//        [strongSelf hideHUDIndicatorViewAtCenter];
//        [strongSelf showAlertView:errorDesc];
//        [strongSelf resetAction];
//    }];
}

- (void)saveCompanyCertifcation
{
    
    if(!_imageData || _imageData.length == 0) {
        [MsgToolBox showToast:@"请上传营业执照"];
        return;
    }
    
    NSString *errorMsg;
    if (![self.viewModel validateCompanyCertificationInfo:self.httpMessage errorMsg:&errorMsg]) {
        [self showAlertView:errorMsg];
        return;
    }
    

//    if (_imageData && _imageData.length == 0) {
//        [MsgToolBox showToast:@"请上传营业执照"];
//        return;
//    }
    
    [self saveNextAction:@selector(saveCompanyCertifcation) object1:nil object2:nil];
    
    NSArray *array = @[_imageData];
    [self showHUDIndicatorViewAtCenter:@"正在上传中，请稍候"];
    WeakSelf
    [[AliyunOSSManager sharedInstance] uploadImages:array isAsync:YES folderName:kAliyunOSSPrivateImagePath([UserManager sharedInstance].userModel.companyId) complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        StrongSelf
        
        if (state == UploadImageSuccess) {
            
//            strongSelf.userInfoHttpMessage.logoUrl = names[0];
            
            strongSelf.httpMessage.businessLicenceUrl = names[0];
            
//            [AliyunOSSManager sharedInstance]
            [[AliyunOSSManager sharedInstance] pubObjectACL:names[0] isPrivate:YES];
            
            [weakSelf uploadCompanyCertification];
            
        }
        else {
            [MsgToolBox showToast:@"上传营业执照失败，请重新上传"];
        }
        
        
    }];
    
//    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMPANY_CERTIFICATION_API];
//    NSString *params = self.httpMessage.description;
//    [self showHUDIndicatorViewAtCenter:@"正在上传中，请稍候"];
//    WeakSelf
//    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
//        StrongSelf
//        [strongSelf hideHUDIndicatorViewAtCenter];
//        [strongSelf resetAction];
//        
//        NSInteger code_status = [responseData[@"code"] integerValue];
//        if (code_status == STATUS_OK) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"上传成功，24小时内审核所提交信息" delegate:nil cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
//            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//                if (buttonIndex == 0) {
//                    if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(onCompanyCertification)]) {
//                        [strongSelf.delegate onCompanyCertification];
//                    }
//                    [strongSelf.navigationController popViewControllerAnimated:YES];
//                }
//            }];
//        }
//        else {
//            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
//        }
//    } andFailure:^(NSString *errorDesc) {
//        StrongSelf
//        [strongSelf resetAction];
//        [strongSelf hideHUDIndicatorViewAtCenter];
//        [strongSelf showAlertView:errorDesc];
//    }];
    
}

-(void)uploadCompanyCertification
{
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(COMPANY_CERTIFICATION_API) requestMethod:POST params:self.httpMessage.description success:^(id responseData) {
        
//        StrongSelf
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"上传成功，24小时内审核所提交信息" delegate:nil cancelButtonTitle:STR_OK otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    
                    if (self.enter_type == ENTER_TYPE_PUSH) {
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }
                    else {
                        [weakSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
                    }
//                    [self pressedBackButton];
//                    [[NSNotificationCenter defaultCenter] add]
//                    [weakSelf.navigationController popViewControllerAnimated:YES];
                    
                    if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(onCompanyCertification)]) {
                        [weakSelf.delegate onCompanyCertification];
                    }
                }
            }];
        }
        else {
            [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
//            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

- (void)onPickerImageView
{
    [self.cameraPhotoService show];
//    [self.uploadImageManager showActionSheet];
}

#pragma mark - uploadImageManage delegate method

//- (void)didFinishPicker:(UIImage*)image actionSheet:(UIActionSheet *)actionSheet
//{
//    NSData *data = UIImageJPEGRepresentation(image, 1);
//    
//    DLog(@"data.size = %lu", (unsigned long)data.length);
//    
//    self.httpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:data] encoding:NSUTF8StringEncoding];
//    
//    [self.plainTableView reloadData];
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return 44;
    return DefaultTableRowHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 210*Scale;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *bgView = [UIView new];
    bgView.backgroundColor = UIColorFromRGB(0xEBEBEB);
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 200*Scale)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(32, 22, (ViewWidth - 64),((ViewWidth-64)*0.75 - 22 * 2))];
    imageView.userInteractionEnabled = YES;
    imageView.image = [UIImage imageNamed:@"img_BusinessLicense"];
    imageView.frame = CGRectMake(30*Scale, 12.5*Scale, ViewWidth-60*Scale, 175*Scale);
    
    if (self.viewModel.canEditPhoto) {
        [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPickerImageView)]];
    }
    
    if (self.imageData && self.imageData.length > 0) {
        imageView.image = [UIImage imageWithData:self.imageData];
    }
    else {
        imageView.image = [UIImage imageNamed:@"img_BusinessLicense"];
//        imageView.image = [UIImage imageWith]
    }
//    if (IsStrEmpty(self.httpMessage.fileContent)) {
//        if (self.imageData != nil && self.imageData.length > 0) {
//            imageView.image = [UIImage imageWithData:self.imageData];
//        }
//    }
//    else {
//        NSData *decodeImageData = [GTMBase64 decodeString:self.httpMessage.fileContent];
//        imageView.image = [UIImage imageWithData:decodeImageData];
//    }
    
    [headerView addSubview:imageView];
    
    [bgView addSubview:headerView];
    
    return bgView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CommonEditTableViewCell *cell = [CommonEditTableViewCell cellWithTableView:tableView indexPath:indexPath];
    if (indexPath.row == CompanyCertificationRowArea) {
        cell.titleLabel.text = @"所在地区";
        cell.detailTextField.placeholder = @"请选择公司所在地区";
        cell.detailTextField.text = self.httpMessage.areaName;
    }
    if (indexPath.row == CompanyCertificationRowCompanyName) {
        cell.titleLabel.text = @"公司名称";
        cell.detailTextField.placeholder = @"请输入公司名称";
        cell.detailTextField.text = self.httpMessage.name;
    }
    if (indexPath.row == CompanyCertificationRowAddress) {
        cell.titleLabel.text = @"公司经营地址";
        cell.detailTextField.placeholder = @"请输入公司经营地址";
        cell.detailTextField.text = self.httpMessage.address;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CommonEditTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section == 0 /*&& indexPath.row == 0*/) {
        if (indexPath.row == CompanyCertificationRowArea) {
            if (self.viewModel.canEditAreaName) {
                [self selectCity:cell.detailTextField];
                return;
            }
        }
        if (indexPath.row == CompanyCertificationRowCompanyName) {
            if (!self.viewModel.canEditCompanyName) {
                return;
            }
            InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
            viewController.text = self.httpMessage.name;
            viewController.tag = MyCompanyTextField;
            viewController.delegate = self;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if (indexPath.row == CompanyCertificationRowAddress) {
            if (!self.viewModel.canEditCompanyAddress) {
                return;
            }
            InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
            viewController.text = self.httpMessage.address;
            viewController.tag = CompanyTextFieldBusinessAddress;
            viewController.delegate = self;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

#pragma mark - InputHelperViewController delegate method

-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
{
    if (tag == CompanyTextFieldBusinessAddress) {
        self.httpMessage.address = text.trim;
        [self.plainTableView reloadData];
    }
    if (tag == MyCompanyTextField) {
        self.httpMessage.name = text.trim;
        [self.plainTableView reloadData];
    }
}

#pragma  mark - 城市选择

-(void)selectCity:(id)sender
{
    DLog(@"doSelectBankCity");
    WeakSelf
    [ActionSheetCityPicker showPickerWithTitle:@""
                                     doneBlock:^(ActionSheetCityPicker *picker, AreaInfoModel *province, AreaInfoModel *city, AreaInfoModel *district) {
                                         StrongSelf
                                         if ([city.name isEqualToString:@"北京"] || [city.name isEqualToString:@"上海"] ||
                                             [city.name isEqualToString:@"天津"] || [city.name isEqualToString:@"重庆"]) {
                                             strongSelf.httpMessage.areaName = city.name;
                                         }
                                         else {
                                             strongSelf.httpMessage.areaName = [NSString stringWithFormat:@"%@%@", province.name, city.name];
                                         }
                                         strongSelf.httpMessage.areaCode = city.id;
                                         [strongSelf.plainTableView reloadData];
                                         
                                     } cancelBlock:^(ActionSheetCityPicker *picker) {
                                         
                                         
                                     } origin:sender];

}



@end
