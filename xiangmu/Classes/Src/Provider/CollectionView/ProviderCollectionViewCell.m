//
//  ProviderCollectionViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderCollectionViewCell.h"

@implementation ProviderCollectionViewCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.height = self.imageView.width;
    
    self.avatarImageView.frame = CGRectMake(10*Scale, self.imageView.bottom+5*Scale, 25*Scale, 25*Scale);
    
    self.avatarImageView.cornerRadius = self.avatarImageView.width/2;
    
    self.companyLabel.left = self.avatarImageView.right + 8*Scale;
    self.companyLabel.width = self.frame.size.width-self.companyLabel.left-5*Scale;
    
    self.starView.left = self.companyLabel.left;
    
    self.commentNumberLabel.left = self.starView.right+4*Scale;
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
