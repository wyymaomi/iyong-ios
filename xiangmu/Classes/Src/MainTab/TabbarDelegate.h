//
//  TabbarDelegate.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#ifndef TabbarDelegate_h
#define TabbarDelegate_h

#import <UserNotifications/UserNotifications.h>

@protocol  TabbarDelegate <NSObject>

-(NSArray*)viewControllers;

// 登录操作处理

-(void)backFromLogin;

-(BOOL)canLogin;

- (void)onLoginSuccess;

-(void)onGestureLoginSuccess;

- (void)onLogout;


// 消息聊天处理

- (void)jumpToChatList;

- (void)setupUntreatedApplyCount;

- (void)setupUnreadMessageCount;

- (void)networkChanged:(EMConnectionState)connectionState;

- (void)didReceiveLocalNotification:(UILocalNotification *)notification;

- (void)didReceiveUserNotification:(UNNotification *)notification;

- (void)playSoundAndVibration;

- (void)showNotificationWithMessage:(EMMessage *)message;


@end

#endif /* TabbarDelegate_h */
