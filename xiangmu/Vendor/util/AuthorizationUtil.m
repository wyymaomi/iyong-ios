//
//  AuthorizationUtil.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AuthorizationUtil.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import <ContactsUI/ContactsUI.h>
#import <AddressBook/AddressBook.h>
//#import <AddressBook/AddressBook.h>

@implementation AuthorizationUtil

/**
 *  check if user allow local notification of system setting
 *  @return YES-allowed,otherwise,NO.
 */
+ (BOOL)isAllowPushNotification
{
    if (IOS8_OR_LATER) {
        UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
        if (UIUserNotificationTypeNone != setting.types) {
            return YES;
        }
    }
    else {
        UIRemoteNotificationType type = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (UIRemoteNotificationTypeNone != type) {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL)isAllowAlbumn;
{
    if (IOS8_OR_LATER) {
        PHAuthorizationStatus photoAuthStatus = [PHPhotoLibrary authorizationStatus];
        if (photoAuthStatus == PHAuthorizationStatusDenied) {
            return NO;
        }
        
    }
    else {
        ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
        if (authStatus == ALAuthorizationStatusDenied) {
            return NO;
        }
    }
    
    return YES;

}

+(BOOL)isAllowVideo
{
    if (IOS7_OR_LATER) {
        AVAuthorizationStatus videoAuthStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (videoAuthStatus == AVAuthorizationStatusDenied) {
            return NO;
        }
    }
    
    return YES;

}

+(BOOL)isAllowContact
{
    if (IOS9_OR_LATER) {
        CNAuthorizationStatus authStatus = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
        if (authStatus == CNAuthorizationStatusDenied) {
            return NO;
        }
    }
    else {
        ABAuthorizationStatus authStatus = ABAddressBookGetAuthorizationStatus();
        if (authStatus == kABAuthorizationStatusDenied) {
            return NO;
        }
    }
    
    return YES;
}

@end
