//
//  RechargeAmountTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/23.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RechargeAmountTableViewCell.h"

@implementation RechargeAmountTableViewCell

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    static NSString *ID = @"RechargeAmountTableViewCell";
    RechargeAmountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
//        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
        cell = [super initWithStyle:style reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        cell.userInteractionEnabled = YES;
        //        cell.contentView.userInteractionEnabled = YES;
        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(24, 15, 2, 17)];
//        view.backgroundColor = NAVBAR_BGCOLOR;
//        [cell.contentView addSubview:view];
        
//        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, view.top-1, ViewWidth, view.height)];
//        titleLabel.font = FONT(17);
//        titleLabel.textColor = [UIColor blackColor];
//        titleLabel.text = @"服务类型";
//        titleLabel.backgroundColor = [UIColor clearColor];
//        [titleLabel sizeToFit];
//        [cell.contentView addSubview:titleLabel];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 20, ViewWidth, 20)];
        titleLabel.text = @"选择金额";
        titleLabel.font = BOLD_FONT(18);
        titleLabel.textColor = UIColorFromRGB(0xCC6666);
        [cell.contentView addSubview:titleLabel];
        
        UIButton *rechargeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rechargeButton.frame = CGRectMake(55*Scale, 250, ViewWidth - 110, 40*Scale);
        [rechargeButton setTitle:@"确认充值" forState:UIControlStateNormal];
        [rechargeButton blueStyle];
        [cell.contentView addSubview:rechargeButton];
        _rechargeButton = rechargeButton;
        
        
        cell.amountList = [NSMutableArray new];
        
//        cell.imageArray = [NSMutableArray new];
//        cell.labelArray = [NSMutableArray new];
        
        //        NSArray *typeNameArray = @[@"logo_commercial", @"logo_travel", @"logo_airport", @"logo_goods", @"logo_wedding", @"logo_chartered", @"logo_truck", @"logo_bus"];
        //
        //        NSArray *nameArray = @[@"商务用车", @"旅游用车", @"机场接送", @"货物用车", @"婚庆用车", @"包车服务", @"拖车服务", @"企业班车"];
        
        // edited by wyy 2017/09/25
        // 充值金额从1K开始
        NSInteger number = 3;//6;
        NSInteger startX = 15;
        NSInteger startY = titleLabel.bottom+20;
        NSInteger imgWidth = 105*Scale, imgHeight = 60*Scale;
        NSInteger separatorX = (ViewWidth-startX * 2 - imgWidth * 3)/2,separatorY = 15;
        
//        NSArray<NSNumber*> *list = @[@100,@200,@500,@1000,@2000,@5000];
        NSArray<NSNumber*> *list = @[@1000,@2000,@5000];
        
        for (NSInteger i = 0; i < number; i++) {
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.userInteractionEnabled = YES;
            button.enabled = YES;
            button.frame = CGRectMake(startX+(imgWidth+separatorX)*(i%3), startY+(imgHeight+separatorY)*(i/3), imgWidth, imgHeight);
            NSString *imgName = [NSString stringWithFormat:@"icon_%@", [list[i] stringValue]];
            NSString *selImgName = [NSString stringWithFormat:@"icon_%@_selected", [list[i] stringValue]];
            [button setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:selImgName] forState:UIControlStateSelected];
            button.tag = [list[i] integerValue];
            [cell.contentView addSubview:button];
            [cell.amountList addObject:button];
            [button addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
            
            if (i==0) {
                self.lastButton = button;
                button.selected = YES;
            }
            
            if (i == number-1) {
                _rechargeButton.top = button.bottom + 50*Scale;
            }
            
        }
    }
    return cell;
}

-(void)onClick:(UIButton*)button
{
    if (_lastButton != button) {
        
        //        CustomRadioButton *customRadioButton = self.customButtonArray[
        _lastButton.selected = NO;
        button.selected = YES;
        
//        NSInteger lastIndex = _lastButton.tag;
//        NSInteger currentIndex = button.tag;
        
//        CustomRadioButton *lastRadioButton = self.customButtonArray[lastIndex];
//        lastRadioButton.isSelected = NO;
        
//        CustomRadioButton *curRadioButton = self.customButtonArray[currentIndex];
//        curRadioButton.isSelected = YES;
        
        
        _lastButton = button;
        
//        if (self.delegate /*&& [self.delegate respondsToSelector:@selector(onClickButton:)]*/) {
//            [self.delegate onClickButton:button.tag];
//        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(onSelectItem:)]) {
            [self.delegate onSelectItem:button.tag];
        }
        
        
    }
}

+(CGFloat)getCellHeight
{
    return 335;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
