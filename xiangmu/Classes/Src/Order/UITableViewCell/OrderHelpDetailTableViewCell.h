//
//  OrderHelpDetailTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/7/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderHelpDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
