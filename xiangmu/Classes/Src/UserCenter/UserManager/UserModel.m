//
//  UserModel.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

- (NSString*)displayChatName;
{
    if (!IsStrEmpty(self.companyName)) {
        return self.companyName;
    }
    
    if (!IsStrEmpty(self.nickname)) {
        return self.nickname;
    }
    
    return [self.username replaceStringWithAsterisk:3 length:4];
}

- (NSString*)displayNickname;
{
    if (!IsStrEmpty(self.nickname)) {
        return self.nickname;
    }
    
    return [self.username replaceStringWithAsterisk:3 length:4];
}


- (BOOL)isRealnameCertified;
{
    return [self.certification boolValue];
}

- (BOOL)isCompanyCertified;
{
    return ([self.companyCertification integerValue] == CompanyCertificationStatusFinished);
}

@end
