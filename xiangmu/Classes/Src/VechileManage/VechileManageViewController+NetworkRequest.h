//
//  VechileManageViewController+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileManageViewController.h"
#import "BindDriverModel.h"

@interface VechileManageViewController (NetworkRequest)

-(void)fetchDriverList:(NSInteger)status createTime:(NSString*)createTime;

-(void)unbindDriver:(id)sender;

@end
