//
//  ProviderDetailViewController+Network.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderDetailViewController+Network.h"

@implementation ProviderDetailViewController (Network)

- (void)cancelFavorite
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SERVICER_CANCEL_FAVORITE_API];
    NSString *params = [NSString stringWithFormat:@"dstCompanyId=%@", self.companyId];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            self.viewModel.detailModel.collection = @0;
            [MsgToolBox showToast:@"取消收藏成功"];
//            favoriteView.image = [UIImage imageNamed:@"icon_gray_star"];
//            viewModel.serviceModel.collection = @0;
            [strongSelf setNavigationView];
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

- (void)addFavorite
{
    
//    self.nextAction = @selector(addFavorite);
//    self.object1 = nil;
//    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SERVICER_FAVORITE_API];
    NSString *params = [NSString stringWithFormat:@"dstCompanyId=%@", self.companyId];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        //        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            strongSelf.viewModel.detailModel.collection = @1;
            [MsgToolBox showToast:@"收藏成功"];
            [strongSelf setNavigationView];
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
}

//- (void)onClickTelButton:(id)

// 点赞
- (void)addLikeNumber
{
    self.nextAction = @selector(addLikeNumber);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SERVICER_LIKE_API];
    NSString *params = [NSString stringWithFormat:@"dstCompanyId=%@", /*viewModel.serviceModel.companyId*/self.companyId];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            NSInteger likeNumbers = [self.viewModel.detailModel.thumbsQuantity integerValue];
            likeNumbers++;
            strongSelf.viewModel.detailModel.thumbsUp = @1;
            strongSelf.viewModel.detailModel.thumbsQuantity = [NSString stringWithFormat:@"%ld", (long)likeNumbers];
            
            [strongSelf.tableView reloadData];
            
            [MsgToolBox showToast:kMsgLike];
            
            //            likeLabel.text = [NSString stringWithFormat:@"%ld", (long)likeNumbers];
            //            likeImageView.image = [UIImage imageNamed:@"icon_heart"];
            //            viewModel.serviceModel.thumbsUp = @1;
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

- (void)doQueryProviderDetail
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_SERVICE_DETAIL_API];
    NSDictionary *params = @{@"companyId": self.companyId};
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        StrongSelf
        
        if (code_status == STATUS_OK) {
            
            ProviderDetailModel *detailModel = [[ProviderDetailModel alloc] initWithDictionary:result[@"data"] error:nil];
//            [strongSelf.viewModel setDetailModel:detailModel];
            strongSelf.viewModel.detailModel = detailModel;
            [strongSelf downloadImagesFromAliyun];
            [strongSelf.tableView reloadData];
            [strongSelf setNavigationView];
//            [strongSelf.shareImageView downloadImageFromAliyunOSS:strongSelf.viewModel.detailModel.imgs[0] isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
            
            [strongSelf.shareImageView yy_setImageWithObjectKey:strongSelf.viewModel.detailModel.imgs[0] placeholder:DefaultPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                
                
            } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
                
                
            }];
            
//            strongSelf set
            //            [strongSelf.tableView endHeaderRefresh];
            //            [strongSelf.tableView endFooterRefresh];
            
        }
        else if (code_status == NETWORK_FAILED) {
            //            [strongSelf.tableView endHeaderRefresh];
            //            [strongSelf.tableView endFooterRefresh];
        }
        else {
            //            [strongSelf.tableView endHeaderRefresh];
            //            [strongSelf.tableView endFooterRefresh];
        }
        
    }];
}

- (void)downloadImagesFromAliyun
{
    
    
    
    
    //    for (NSInteger i = 0; i < images.count; i++) {
    //        UIImage *image = [UIImage imageNamed:images[i]];
    //        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.width * i, 0, self.scrollView.width, self.scrollView.height)];
    //        imageView.image = image;
    //        [self.scrollView addSubview:imageView];
    //    }
    
#if 0
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(5);
    
    __block NSMutableArray *errorIndexs = [NSMutableArray array];
    __block NSMutableArray *successIndexs = [NSMutableArray array];
    
    WeakSelf
    
    for (NSInteger i = 0; i < images.count; i++) {
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        
        dispatch_group_async(group, queue, ^{
            
            DLog(@"i = %d", i);
            AdvModel *advModel = images[i];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.width * i, 0, self.scrollView.width, self.scrollView.height)];
            [imageView downloadImageFromAliyunOSS:advModel.imageUrl isThumbnail:YES placeholderImage:nil success:^(id responseData) {
                
                StrongSelf
                
                imageView.image = [UIImage imageWithData:responseData];
                [strongSelf.scrollView addSubview:imageView];
                DLog(@"第%d张下载成功", i);
                [successIndexs addObject:[NSNumber numberWithInteger:i]];
                dispatch_semaphore_signal(semaphore);
                //                dispatch_group_leave(group);
                
            } andFailure:^(NSString *errorDesc) {
                
                //                dispatch_group_leave(group);
                DLog(@"第%d张下载失败，errorDesc=%@", i, errorDesc);
                [errorIndexs addObject:[NSNumber numberWithInteger:i]];
                dispatch_semaphore_signal(semaphore);
                
            }];
            
        });
        
    }
    
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    
    if (successIndexs.count == 0) {
        [self dismiss];
        return;
    }
    
    [self.maskWindow makeKeyAndVisible];
    self.maskView.userInteractionEnabled = YES;
    UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAdvDismiss:)];
    sigleTapRecognizer.numberOfTapsRequired = 1;
    [self.maskView addGestureRecognizer:sigleTapRecognizer];
#endif
    
    //    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
    //
    //        DLog(@"下载成功后通知更新UI");
    //
    //        StrongSelf
    //
    //        if (errorIndexs.count > 0) {
    //            [strongSelf dismiss];
    //            return;
    //        }
    //
    //        strongSelf.scrollView.contentSize = CGSizeMake(0.8*Screen_width * images.count, 1.46*0.825*Screen_width);
    //        [strongSelf.bgView addSubview:self.scrollView];
    //
    //        [strongSelf.maskWindow addSubview:self.pageControl];
    //        strongSelf.pageControl.numberOfPages = images.count;
    //        strongSelf.pageControl.currentPage = 0;
    //        strongSelf.pageControl.top = _bgView.bottom + 10;
    //
    //        if (color) {
    //            strongSelf.bgView.layer.borderColor = color.CGColor;
    //        }
    //
    //    });
    
    
#if 1
    dispatch_group_t group = dispatch_group_create();
    
    //    __block NSMutableArray<UIImage*>
    
    //    for (NSInteger i = 0; i < self.viewModel.detailModel.imgs.count; i++) {
    //
    //        dispatch_group_enter(group);
    //
    //        //网络请求
    //        AdvModel *advModel = images[i];
    //        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.width * i, 0, self.scrollView.width, self.scrollView.height)];
    //        [imageView downloadImageFromAliyunOSS:advModel.imageUrl isThumbnail:NO placeholderImage:nil success:^(id responseData) {
    //
    ////            imageView.image = [UIImage imageWithData:responseData];
    ////            [self.scrollView addSubview:imageView];
    ////            DLog(@"第%d张下载成功", i+1);
    //
    //            dispatch_group_leave(group);
    //
    //        } andFailure:^(NSString *errorDesc) {
    //
    //            DLog(@"第%d张下载失败，errorDesc=%@", i+1, errorDesc);
    //            dispatch_group_leave(group);
    //
    //        }];
    //
    //    }
    
    //    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
    //
    //        DLog(@"下载成功后通知更新UI");
    
    //        [self.maskWindow makeKeyAndVisible];
    //        self.scrollView.contentSize = CGSizeMake(0.8*Screen_width * images.count, 1.46*0.825*Screen_width);
    //        [_bgView addSubview:self.scrollView];
    //
    //        [_maskWindow addSubview:self.pageControl];
    //        self.pageControl.numberOfPages = images.count;
    //        self.pageControl.currentPage = 0;
    //        self.pageControl.top = _bgView.bottom + 10;
    //
    //        if (color) {
    //            _bgView.layer.borderColor = color.CGColor;
    //        }
    //
    //        self.maskView.userInteractionEnabled = YES;
    //        UITapGestureRecognizer *sigleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAdvDismiss:)];
    //        sigleTapRecognizer.numberOfTapsRequired = 1;
    //        [self.maskView addGestureRecognizer:sigleTapRecognizer];
    
    //
    //    });
#endif
    
    
#if 0
    for (NSInteger i = 0 ; i < images.count; i++) {
        AdvModel *advModel = images[i];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.width * i, 0, self.scrollView.width, self.scrollView.height)];
        NSString *imgUrl = [NSString stringWithFormat:@"%@%@%@", HOST_NAME, @"/iyong-web/ad/download?url=", advModel.imageUrl];
        DLog(@"imgUrl = %@", imgUrl);
        [imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        
        [self.scrollView addSubview:imageView];
    }
    
    self.scrollView.contentSize = CGSizeMake(0.8*Screen_width * images.count, 1.46*0.825*Screen_width);
    [_bgView addSubview:self.scrollView];
    
    [_maskWindow addSubview:self.pageControl];
    self.pageControl.numberOfPages = images.count;
    self.pageControl.currentPage = 0;
    self.pageControl.top = _bgView.bottom + 10;
    
    if (color) {
        _bgView.layer.borderColor = color.CGColor;
    }
#endif
}


- (void)fetchCommentList:(NSString*)time
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PROVIDER_COMMENT_LIST_API];
    NSDictionary *params = @{@"companyId": self.companyId,
                             @"time": time};
    
    if (IsStrEmpty(time)) {
        [self.commentList removeAllObjects];
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        if (code_status == STATUS_OK) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                ProviderCommentResponseData *responseData = [[ProviderCommentResponseData alloc] initWithDictionary:result error:nil];
                for (NSInteger i = 0; i < responseData.data.count; i++) {
                    ProviderCommentModel *model = responseData.data[i];
                    ProviderCommentViewModel *viewModel = [[ProviderCommentViewModel alloc] init];
                    viewModel.commentModel = model;
                    [strongSelf.commentList addObject:viewModel];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [strongSelf.tableView reloadData];
                    
                    [strongSelf.tableView endHeaderRefresh];
                    [strongSelf.tableView endFooterRefresh];
                    
                    //                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:ProviderCellCompanyDiscuss];
                    //                    [strongSelf.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                    
                });
                
                
            });
            
            
        }
        else {
            
            [strongSelf.tableView endHeaderRefresh];
            [strongSelf.tableView endFooterRefresh];
            
        }
    }];
}

- (void)uploadComment:(NSString*)content
{
    //    NSString *errorMsg;
    //    if (![CommunityHelper isValidEnquire:self.httpMessage errorMsg:&errorMsg]) {
    //        [MsgToolBox showToast:errorMsg];
    //        return;
    //    }
    
    
    //    self.nextAction = @selector(onInquire:);
    //    self.object1 = sender;
    //    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, SAVE_PROVIDER_COMMENT_API];
    NSString *params = [NSString stringWithFormat:@"topicId=%@&content=%@", /*self.experienceViewModel.serviceModel.id*/self.companyId, content];
    
    [self showHUDIndicatorViewAtCenter:@"正在发布评论，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"发布成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    //                    [strongSelf.navigationController popViewControllerAnimated:YES];
                    //                    strongSelf.tableView
                    [strongSelf fetchCommentList:@""];
                }
                
            }];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
    
}



@end
