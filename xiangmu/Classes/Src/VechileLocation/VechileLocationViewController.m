//
//  VechileLocationViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/11.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "VechileLocationViewController.h"
#import "BaiduTraceSDK/BaiduTraceSDK.h"

@interface VechileLocationViewController ()<BTKTraceDelegate,BTKEntityDelegate>

@end

@implementation VechileLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 使用SDK的任何功能前，都需要先调用initInfo:方法设置基础信息。
    BTKServiceOption *sop = [[BTKServiceOption alloc] initWithAK:AK mcode:mcode serviceID:serviceID keepAlive:1];
    [[BTKAction sharedInstance] initInfo:sop];
    
    [self startService];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)startService
{
    
    if ([self needLogin]) {
        return;
    }
    
    //    self.entityName = [UserManager sharedInstance].userModel.id;
    //    self.entityName = [StringUtil createUDID];
    //    DLog(@"self.entityName = %@", self.entityName);
//    self.entityName = self.driverId;
    
    BTKStartServiceOption *op = [[BTKStartServiceOption alloc] initWithEntityName:self.entityName];
    [[BTKAction sharedInstance] startService:op delegate:self];
    
}

/**
 开启轨迹服务的回调方法
 
 @param error 开启轨迹服务的结果
 */
-(void)onStartService:(BTKServiceErrorCode) error;
{
    // 设置过滤条件
    BTKQueryEntityFilterOption *filterOption = [[BTKQueryEntityFilterOption alloc] init];
    filterOption.activeTime = [[NSDate date] timeIntervalSince1970] - 7 * 24 * 3600;
    // 设置排序条件，返回的多个entity按照，定位时间'loc_time'的倒序排列
    BTKSearchEntitySortByOption * sortbyOption = [[BTKSearchEntitySortByOption alloc] init];
    sortbyOption.fieldName = @"loc_time";
    sortbyOption.sortType = BTK_ENTITY_SORT_TYPE_DESC;
    // 构造请求对象
    BTKSearchEntityRequest *request = [[BTKSearchEntityRequest alloc] initWithQueryKeyword:self.entityName filter:filterOption sortby:sortbyOption outputCoordType:BTK_COORDTYPE_BD09LL pageIndex:1 pageSize:10 ServiceID:serviceID tag:34];
    // 发起检索请求
    [[BTKEntityAction sharedInstance] searchEntityWith:request delegate:self];
    
}

/**
 查询终端实体的回调方法
 
 @param response 查询结果
 */
-(void)onQueryEntity:(NSData *)response;
{
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
    DLog(@"track history response: %@", dict);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
