//
//  YYStatusView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YYStatusViewDelegate <NSObject>

- (void)statusViewSelectIndex:(NSInteger)index;

@end

@interface YYStatusView : UIView

@property (nonatomic, strong) NSMutableArray *buttonArray;

@property (nonatomic, weak) id<YYStatusViewDelegate> delegate;

// 横线
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UIView *bottomLineView;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, assign) BOOL  isScroll;

//界面书初始化 titleArray状态值,normalColor正常标题颜色，selectedColor选中的颜色，lineColor下面线条颜色如果等于nil就没有线条
- (void)initWithTitles:(NSArray *)titleArray NormalColor:(UIColor *)normalColor SelectedColor:(UIColor *)selectedColor LineColor:(UIColor *)lineColor;

-(void)changeTag:(NSInteger)tag;

- (void)changeCurrenIndex:(NSInteger)index;


@end
