//
//  MainTabBarController+AccountInfo.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MainTabBarController.h"

@interface MainTabBarController (AccountInfo)

- (void)onClickAccount;

@end
