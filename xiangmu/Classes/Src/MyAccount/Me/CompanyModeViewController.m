//
//  CompanyModeViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CompanyModeViewController.h"
#import "MeTableViewCell.h"
#import "PostExperienceViewController.h"
#import "BaiduMapTraceViewController.h"
#import "AccountBalanceViewController.h"

@interface CompanyModeViewController ()

@end

@implementation CompanyModeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    DLog(@"WYY CompanyModeViewController viewDidLoad");
//    self.title = @"企业模式";
    if (self.source_from == SOURCE_FROM_TAB_CONTROL) {
        self.title = @"商户";
    }
    else {
        self.title = @"企业模式";
    }
    [self.view addSubview:self.plainTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return 5;
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55*Scale;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MeTableViewCell *cell = [MeTableViewCell cellWithTableView:tableView];
    
    if (indexPath.row == CompanyRowPublish) {
        cell.titleLabel.text = @"商户发布";
        cell.iconImageView.image = [UIImage imageNamed:@"icon_company_info"];
//        cell.iconImageView.image = [UIImage imageNamed:@"]
    }
    if (indexPath.row == CompanyRowCompanyInfo) {
        cell.titleLabel.text = @"商户信息";
        cell.iconImageView.image = [UIImage imageNamed:@"icon_company_publish"];
    }
    if (indexPath.row == CompanyRowPromotion) {
        cell.titleLabel.text = @"推广专栏";
        cell.iconImageView.image = [UIImage imageNamed:@"icon_company_promotion"];
    }
    if (indexPath.row == CompanyRowVechileManage) {
        cell.titleLabel.text = @"车辆管理";
        cell.iconImageView.image = [UIImage imageNamed:@"icon_driver_manage"];
    }
//    if (indexPath.row == CompanyRowCarRent) {
//        cell.titleLabel.text = @"租车";
////        cell.icon
//    }
    if (indexPath.row == CompanyRowIncome) {
        cell.titleLabel.text = @"商户收入";
        cell.iconImageView.image = [UIImage imageNamed:@"icon_company_income"];
    }
//    if (indexPath.row == CompanyRowPath) {
//        cell.titleLabel.text = @"路径规划";
//    }
//    cell.titleLabel.text = @"我的账户";
//    cell.iconImageView.image = [UIImage imageNamed:@"icon_my_account"];
    
    //        OrderStatusTableViewCell *cell = [OrderStatusTableViewCell cellWithTableView:tableView];
    //        cell.backgroundColor = [UIColor clearColor];
    //        cell.iconImageView.image = [UIImage imageNamed:@"icon_my_account"];
    //        cell.titleLabel.text = @"我的帐户";
    //        cell.titleLabel.font = FONT(14);
    //        cell.titleLabel.textColor = [UIColor darkTextColor];
    //        cell.badgeButton.hidden = YES;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == CompanyRowCompanyInfo) {
        [self openProviderInfo];
    }
    if (indexPath.row == CompanyRowPublish) {
        [self openCompanyPublish];
    }
    if (indexPath.row == CompanyRowPromotion) {
        [self openAdvPurchase];
    }
    if (indexPath.row == CompanyRowVechileManage) {
        [self openVechileManage];
    }
    if (indexPath.row == CompanyRowIncome) {
        [self openCompanyIncome];
    }
//    if (indexPath.row == CompanyRowCarRent) {
//        [self openMapView];
//    }

//    if (indexPath.row == CompanyRowPath) {
//        BaiduMapTraceViewController *viewController = [[BaiduMapTraceViewController alloc] init];
//        viewController.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:viewController animated:YES];
//    }
}

- (void)openCompanyPublish
{
    PostExperienceViewController *viewController = [[PostExperienceViewController alloc] init];
    viewController.type = TableTypeCompanyBusiness;
//    viewController.enter_type = ENTER_TYPE_PRESENT;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)openAdvPurchase
{
    [self gotoPage:@"AdPurchaseViewController"];
}

- (void)openProviderInfo
{
    [self gotoPage:@"ProviderInfoViewController"];
}

-(void)openVechileManage
{
    [self gotoPage:@"VechileManageViewController"];
}

-(void)openMapView
{
    [self gotoPage:@"MapViewController"];
}

-(void)openCompanyIncome
{
    AccountBalanceViewController *viewController = [[AccountBalanceViewController alloc] init];
    viewController.mode = 1;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
