//
//  YYTaskManager.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/16.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YYTask.h"


/**
 *  托管Task的类，保证Task能够完全执行
 */
@interface YYTaskManager : NSObject

@property (readonly, strong) NSOperationQueue *taskOperationQueue;

+ (instancetype)sharedManager;

- (void)addTask:(YYTask*)task;
- (void)removeTask:(YYTask*)task;



@end
