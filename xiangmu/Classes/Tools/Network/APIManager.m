//
//  APIManager.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "APIManager.h"

#import "AppDelegate.h"
//#import "NetworkReach.h"
#import "UserManager.h"
#import "EncryptUtil.h"
#import "NSString+Encrypt.h"
#import "ApiProxy.h"


@implementation APIManager

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        
//        if ([self conformsToProtocol:@protocol(Request)]) {
//            self.request = (id<Request>)self;
//        } else {
//            NSException *exception = [[NSException alloc] init];
//            @throw exception;
//        }
        
    }
    
    return self;
}

- (RequestType)requestType;
{
    return RequestTypePost;
}

- (NSString *)baseUrl;
{
    return HTTP_BASE_URL;
}

- (NSString *)apiUrl;
{
    return @"";
}

//- (NSString *)requestParams;
//{
//    return @"";
//}

- (BOOL) isEncryptRequest;
{
    return YES;
}

- (NSString*)url
{
    if ([self conformsToProtocol:@protocol(Request)]) {
        return [NSString stringWithFormat:@"%@/%@", [self baseUrl], [self apiUrl]];
    }
    return @"";
}

- (void)fetchDataWithRequest:(NSString*)params;
{
    if ([self conformsToProtocol:@protocol(Request)]) {
        if ([self isEncryptRequest]) {
            [self startEncryptRequest:params];
        }
        else {
//            [self startRequest];
        }
    }
//    if (self.isEncryptRequest) {
//        [self startEncryptRequest:params];
//    }
//    else {
//        [self startRequest];
//    }
}

- (void)startEncryptRequest:(NSString*)params
{
    

    
    if (self.requestType == RequestTypePost) {
        
        DLog(@"url = %@", [NSString stringWithFormat:@"%@/%@", self.baseUrl, self.apiUrl]);
        
        // 判断是否已经登录
        if (![[UserManager sharedInstance] token]) {
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(failedWithRequest:status_code:errorMsg:)]) {
                [self.delegate failedWithRequest:self.apiUrl status_code:LOGIN_TIMEOUT errorMsg:MSG_LOGIN_TIP];
            }
            return;
        }
        
//        NSString *url;
//        RequestType requestType;
//        NSDictionary *dict =
//        if ([self conformsToProtocol:@protocol(Request)]) {
////            url = [NSString stringWithFormat:@"%@/%@", base
//            
////            url = [NSString stringWith]
//            
//            
//        }
        
        NSString *localParams;
        if (IsStrEmpty(params)) {
            localParams = [NSString stringWithFormat:@"token=%@", [[UserManager sharedInstance] token]];
        }
        else {
            localParams = [NSString stringWithFormat:@"%@&token=%@", params, [[UserManager sharedInstance] token]];
        }
        
        DLog(@"param = %@", localParams);
        NSString *desKey = [[UserManager sharedInstance] desKey];
        NSString *desEncryptParam = [localParams desEncrypt:desKey];
        if (IsStrEmpty(desEncryptParam)) {
            return;
        }
        NSDictionary *encryptParams = @{@"a" : desEncryptParam};
        
        //        DLog(@"desEncryptParams = %@", desEncryptParam);
        __weak __typeof(self)weakSelf = self;
        [[ApiProxy sharedInstance] callApiWithParams:[self url] params:encryptParams requestType:self.requestType success:^(id responseObject) {
            
            __strong __typeof(self)strongSelf = weakSelf;
            
            if (IsNilOrNull(responseObject) || ![responseObject isKindOfClass:[NSData class]]) {
                return;
            }
            
            // 返回加密报文
            NSString *responseCipher = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            if (IsStrEmpty(responseCipher)) {
                return;
            }
            
            // 解密返回的报文
            NSString *decryptResponse = [responseCipher decryptUseDES:desKey];
            
            if (IsStrEmpty(decryptResponse)) {
                
                //                if (failure) {
                //                    failure(MSG_DECRYPT_FAILURE);
                //                }
                return;
            }
            
            // 转换成NSDictioanry
            NSData *decryptData = [decryptResponse dataUsingEncoding:NSUTF8StringEncoding];
            if (IsNilOrNull(decryptData) || ![decryptData isKindOfClass:[NSData class]]) {
                return;
            }
            
            //////
            NSError *error;
            id result = [NSJSONSerialization JSONObjectWithData:decryptData options:NSJSONReadingMutableContainers error:&error];
            
            if (![result isKindOfClass:[NSDictionary class]] || error != nil) {
                return;
            }
            
            DLog(@"responseObject = %@", result);
            
            NSDictionary *responseDict = result;
            
            if ([[responseDict allKeys] containsObject:@"code"] && responseDict[@"code"] != nil && ![responseDict[@"code"] isEqualToString:@""] ) {
                
                // 登录成功后重新计算时间
                AppDelegate *appDelegate = APP_DELEGATE;
                [appDelegate initResignTime];
                
                NSInteger code_status = [responseDict[@"code"] integerValue];
                if (code_status == STATUS_OK) {
                    if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(successWithRequest:responseObject:)]) {
                        [strongSelf.delegate successWithRequest:strongSelf.apiUrl responseObject:responseObject];
                    }
                    
                    return;
                }
                else {
                    if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(failedWithRequest:status_code:errorMsg:)]) {
                        [strongSelf.delegate failedWithRequest:strongSelf.apiUrl status_code:code_status errorMsg:nil];
                    }
                }
            }
        } fail:^(NSError *error) {
            
            DLog(@"error = %@", error.description);
            DLog(@"error.code = %ld", (long)error.code);
            
            if ([error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == AFNetworkErrorType_Unauthorized) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizeNotification object:nil];
                return;
            }
            else{
                
                if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(failedWithRequest:status_code:errorMsg:)]) {
                    [weakSelf.delegate failedWithRequest:[weakSelf apiUrl] status_code:NETWORK_FAILED errorMsg:MSG_NETWORK_FAILED];
                }
            }
            
        }];

        
    }
    else {
        
    }

        

}

- (void)startRequest
{
    
}




@end
