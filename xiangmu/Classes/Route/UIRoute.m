//
//  UIRoute.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UIRoute.h"

@implementation UIRoute

+ (void)openSystemSetting:(NSString*)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
        
        if (buttonIndex == 1) {
            NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
                
            }
        }
    }];
    
}

@end
