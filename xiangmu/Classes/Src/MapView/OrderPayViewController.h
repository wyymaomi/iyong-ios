//
//  OrderPayViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "BottomBarView.h"

@interface OrderPayViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *startDateView;
@property (weak, nonatomic) IBOutlet UILabel *startDateTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *vechileTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *vechileNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *vechileTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverMobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *startAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *endAddressLabel;
//@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UIView *driverInfoView;
@property (weak, nonatomic) IBOutlet UIView *travelInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *startAddressIconView;
@property (weak, nonatomic) IBOutlet UIImageView *endAddressIconView;
@property (weak, nonatomic) IBOutlet UILabel *unitPriceLabel;
@property (weak, nonatomic) IBOutlet UIView *travelInfoLineView;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@end
