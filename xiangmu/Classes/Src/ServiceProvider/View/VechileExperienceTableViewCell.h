//
//  VechileExperienceTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/21.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserExpericenViewModel.h"

@interface VechileExperienceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *thumbupButton;
@property (weak, nonatomic) IBOutlet UILabel *thumbupNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet UILabel *browseNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIImageView *imageView2;
@property (weak, nonatomic) IBOutlet UIImageView *imageView3;

//@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UIView *bottomView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

- (id)initWithStyle:(UITableView*)tableView;

+(CGFloat)getCellHeight;

@property (nonatomic, strong) UserExpericenViewModel *viewModel;

@end
