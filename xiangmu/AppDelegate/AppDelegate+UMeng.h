//
//  AppDelegate+UMeng.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/1.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AppDelegate.h"
#import <UMSocialCore/UMSocialCore.h>

@interface AppDelegate (UMeng)

- (void)setupUmeng;

@end
