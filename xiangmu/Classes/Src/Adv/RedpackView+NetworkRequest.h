//
//  RedpackView+NetworkRequest.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RedpackView.h"

@interface RedpackView (NetworkRequest)

-(void)getRedPackage;

@end
