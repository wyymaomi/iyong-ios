//
//  NewsAdvTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"

@interface NewsAdvTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIImageView *imgView;

+(CGFloat)getCellHeight;

-(void)initData:(NewsModel*)data;

@end
