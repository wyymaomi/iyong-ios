//
//  CommunityHeaderView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/9.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarView.h"
#import "CertifiedView.h"

@interface CommunityHeaderView : UIView

@property (weak, nonatomic) IBOutlet StarView *starView;
@property (weak, nonatomic) IBOutlet CertifiedView *certifiedView;
@property (weak, nonatomic) IBOutlet UIImageView *avartarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;

-(void)initData;

@end
