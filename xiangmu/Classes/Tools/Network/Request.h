//
//  Request.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RequestType)
{
    RequestTypeGet,
    RequestTypePost
};


@protocol  Request <NSObject>

- (RequestType)requestType;

- (NSString *)baseUrl;

- (NSString *)apiUrl;

//- (NSString *)requestParams;

- (BOOL) isEncryptRequest;

@end

//@interface Request : NSObject
//
//@property (nonatomic, assign) RequestMethod method;
//
//@property (nonatomic, strong) NSString *baseUrl;
//
//@property (nonatomic, strong) NSString *apiUrl;
//
//@property (nonatomic, strong) NSString *params;
//
//@property (nonatomic, assign) BOOL isEncryptRequest; // 是否加密请求
//
//@end
