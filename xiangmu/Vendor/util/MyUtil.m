//
//  MyUtil.m
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MyUtil.h"
//#import "GYZCityGroup.h"
#import "GYZCity.h"

@implementation MyUtil
+(UILabel *)createLabelFrame:(CGRect)frame title:(NSString *)title font:(UIFont *)font textAlignment:(NSTextAlignment)alignment numberOfLines:(NSInteger)numberOfLines textColor:(UIColor *)textColor
{
    UILabel *label=[[UILabel alloc]initWithFrame:frame];
    if (title) {
        label.text=title;
    }
    if (font) {
        label.font=font;
    }
    label.textAlignment=alignment;
    label.numberOfLines=numberOfLines;
    if (textColor) {
        label.textColor=textColor;
    }
    return label;
}

+(UIButton *)createBtnFrame:(CGRect)frame title:(NSString *)title bgImageName:(NSString *)bgImageName target:(id)target action:(SEL)action
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=frame;
    if (title) {
        [btn setTitle:title forState:UIControlStateNormal];
    }
    if (target && action) {
        [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    if (bgImageName) {
        [btn setBackgroundImage:[UIImage imageNamed:bgImageName] forState:UIControlStateNormal];
    }
    return btn;
}

+(UIImageView *)createImageViewFrame:(CGRect)frame image:(NSString *)image
{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    if (image) {
        imageView.image = [UIImage imageNamed:image];
    }
    return imageView;
}

+(NSString*)getCityShortnameFromCityCode:(NSString*)cityCode
{
    @autoreleasepool {
        NSArray *array = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CityData" ofType:@"plist"]];
        for (NSDictionary *groupDic in array) {
            @autoreleasepool {
                for (NSDictionary *dic in groupDic[@"citys"]) {
                    @autoreleasepool {
                        if ([cityCode isEqualToString:dic[@"city_key"]]) {
                            return dic[@"short_name"];
                        }
                    }
                    
                }
            }
        }
    }
    return @"";
}

+(NSString*)getCityCodeFromCityName:(NSString*)cityName
{
//    NSMutableArray *cityList = [
    @autoreleasepool {
        NSArray *array = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CityData" ofType:@"plist"]];
        for (NSDictionary *groupDic in array) {
            @autoreleasepool {
                for (NSDictionary *dic in groupDic[@"citys"]) {
                    @autoreleasepool {
                        if ([cityName isEqualToString:dic[@"city_name"]] || [cityName isEqualToString:@"short_name"]) {
                            return dic[@"city_key"];
                        }
                    }
                    
                }
            }

        }
    }
    return @"";
}

+(NSMutableArray *) cityList{
//    if (_cityDatas == nil) {
        NSMutableArray *cityList = [NSMutableArray new];
    
    @autoreleasepool {
        NSArray *array = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CityData" ofType:@"plist"]];
        for (NSDictionary *groupDic in array) {
            @autoreleasepool{
                for (NSDictionary *dic in [groupDic objectForKey:@"citys"]) {
                    @autoreleasepool {
                        [cityList addObject:dic];
                    }
                }
            }
        }
    }


//    }
//    return _cityDatas;
    return cityList;
}

+(NSString*)homeDisplayCityName:(NSString*)cityName
{
    return [NSString stringWithFormat:@"%@服务商", cityName];
}

@end
