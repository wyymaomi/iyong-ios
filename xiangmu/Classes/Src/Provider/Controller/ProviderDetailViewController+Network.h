//
//  ProviderDetailViewController+Network.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderDetailViewController.h"
#import "ProviderDetailModel.h"

@interface ProviderDetailViewController (Network)

- (void)cancelFavorite;

- (void)addFavorite;

- (void)addLikeNumber;

- (void)doQueryProviderDetail;

- (void)downloadImagesFromAliyun;

- (void)fetchCommentList:(NSString*)time;

- (void)uploadComment:(NSString*)content;


@end
