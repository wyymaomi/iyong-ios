//
//  FirstCollectionViewCell.h
//  xiangmu
//
//  Created by David kim on 16/3/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong)UIImageView *topImageView;
@property (nonatomic,strong)UILabel *detailLabel;
@property (nonatomic, strong) UIButton *badgeValueBtn;

-(void)showBadgeValue:(NSInteger)numberBadge;
-(void)hideBadge;

@end
