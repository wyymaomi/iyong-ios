//
//  OrderCopyResponseModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/24.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "JSONModel.h"

@protocol OrderCopyResponseModel
@end

@protocol CarInfo
@end




@interface CarInfo : JSONModel

@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *driver;
@property (nonatomic, strong) NSString *mobile;

@end

@interface OrderCopyResponseModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *carTime;
@property (nonatomic, strong) NSString<Optional> *location;
@property (nonatomic, strong) NSString<Optional> *itinerary;
@property (nonatomic, strong) NSString<Optional> *customerName;
@property (nonatomic, strong) NSString<Optional> *customerMobile;
@property (nonatomic, strong) NSString<Optional> *remark;
@property (nonatomic, strong) CarInfo<Optional> *carInfo;

@end

@interface OrderCopyResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) OrderCopyResponseModel<Optional> *data;

@end


