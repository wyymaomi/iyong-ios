//
//  InvoiceInfoViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "InvoiceinfoHttpMessage.h"

typedef NS_ENUM(NSUInteger, InvoiceInfoMode)
{
    InvoiceInfoModeSubmit=0, 
    InvoiceInfoModeViewDetail
};

@interface InvoiceInfoViewController : BaseViewController

@property (nonatomic, strong) InvoiceinfoHttpMessage *invoiceInfoHttpMessage;
@property (nonatomic, strong) NSString *requestParams;

@property (nonatomic, assign) NSUInteger mode;

@end
