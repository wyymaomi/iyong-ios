//
//  CpasswordViewController.m
//  xiangmu
//
//  Created by David kim on 16/4/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "MyUtil.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "UserHelper.h"
#import "EncryptUtil.h"
#import "NSString+Encrypt.h"
#import "UserHelper.h"
//#import "RequestData.h"
#import "ForgetPasswordView.h"
#import "BaseViewController+HandleLoginSuccess.h"

@interface ForgetPasswordViewController ()

@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *mobileTextField;
@property (nonatomic, strong) UITextField *verifyCodeTextField;
@property (nonatomic, strong) UITextField *pwdTextField;
@property (nonatomic, strong) UITextField *reNewPwdTextField;
@property (nonatomic, strong) UIButton *verifyButton;

@property (nonatomic, strong) ForgetPasswordView *forgetPasswordView;

@end

@implementation ForgetPasswordViewController

- (ForgetPasswordView*)forgetPasswordView
{
    if (!_forgetPasswordView) {
        _forgetPasswordView = [ForgetPasswordView customView];
    }
    return _forgetPasswordView;
}

-(id)init
{
    if (self = [super init]) {
        
        self.title = @"忘记密码";
        
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    bPopupLoginView = NO;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;//隐藏为YES，显示为NO
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    bPopupLoginView = YES;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
    [self.view addSubview:self.forgetPasswordView];
    
    self.mobileTextField = self.forgetPasswordView.accountTextField;
    self.verifyCodeTextField = self.forgetPasswordView.verifyCodeTextField;
    self.pwdTextField = self.forgetPasswordView.passwordTextField;
    self.reNewPwdTextField = self.forgetPasswordView.repasswordTextField;
    self.verifyButton = self.forgetPasswordView.verifyCodeButton;
    [self.verifyButton addTarget:self action:@selector(doGetVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    
    [self.forgetPasswordView.findPwdButton addTarget:self action:@selector(doResetPwd) forControlEvents:UIControlEventTouchUpInside];
    
    [self.forgetPasswordView.backButton addTarget:self action:@selector(clickBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
//    self.view.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
//    [self createMyNav];
    
//    [self createInterface];
    // Do any additional setup after loading the view.
}

- (void)clickBackButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


//-(void)createInterface
//{
//    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake((ViewWidth-253)/2, 50, 253, 66)];
//    imageView.image=[UIImage imageNamed:@"广告语"];
//    [self.scrollview addSubview:imageView];
//    
//    NSArray *nameArray=@[@"请输入您的手机号",@"请输入验证码",@"新密码",@"确认新密码"];
//    for (int i=0; i<4; i++) {
//        UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(0, 170+50*i, ViewWidth, 50)];
//        if (i==0) {
//            
//            UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(ViewWidth-130, 0, 70, 30)];
//            //rightView.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:193.0f/255.0f blue:65.0f/255.0f alpha:1.0f];
//            sendYanZheng=[UIButton buttonWithType:UIButtonTypeCustom];
//            [sendYanZheng setFrame:CGRectMake(0,0,70,30)];
//            [sendYanZheng addTarget:self action:@selector(doGetVerifyCode) forControlEvents:UIControlEventTouchUpInside];
//            //[sendYanZheng setImage:[UIImage imageNamed:@"send.png"] forState:UIControlStateNormal];
//            [sendYanZheng setTitle:@"获取验证码" forState:UIControlStateNormal];
//            sendYanZheng.titleLabel.font=[UIFont systemFontOfSize:13];
//            sendYanZheng.titleLabel.textAlignment=NSTextAlignmentCenter;
////            sendYanZheng.titleLabel.textColor=[UIColor colorWithRed:71.0f/255.0f green:107.0f/255.0f blue:154.0f/255.0f alpha:1.0f];
////            sendYanZheng.backgroundColor=[UIColor whiteColor];
////            [sendYanZheng setTitleColor:[UIColor colorWithRed:71.0f/255.0f green:107.0f/255.0f blue:154.0f/255.0f alpha:1.0f] forState:UIControlStateNormal ];
//            sendYanZheng.backgroundColor = UIColorFromRGB(0x27558c);
//            [sendYanZheng setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            sendYanZheng.cornerRadius = 5;
//            [sendYanZheng setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
//            [sendYanZheng setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x27558c)] forState:UIControlStateNormal];
//            self.verifyButton = sendYanZheng;
//            [rightView addSubview:sendYanZheng];
//            textField.rightView = rightView;
//            textField.rightViewMode = UITextFieldViewModeAlways;
//        }
//        textField.placeholder=nameArray[i];
//        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 30*i, 20, 30)];
//        textField.leftView=view;
//        textField.font=[UIFont systemFontOfSize:13];
//        textField.leftViewMode=UITextFieldViewModeAlways;
//        textField.layer.borderWidth=0.5f;
//        textField.layer.borderColor=[[UIColor colorWithRed:212.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]CGColor];
//        textField.backgroundColor=[UIColor whiteColor];
//        textField.tag = i;
//        if (i == TextFieldTypeMobile) {
//            self.mobileTextField = textField;
//            self.mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
//        }
//        else if (i == TextFieldTypeVerifyCode) {
//            self.verifyCodeTextField = textField;
//            self.verifyCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
//        }
//        else if (i == TextFieldTypeNewPassword) {
//            self.pwdTextField = textField;
//            self.pwdTextField.keyboardType = UIKeyboardTypeDefault;
//        }
//        else if (i == TextFieldTypeReNewPassword) {
//            self.reNewPwdTextField = textField;
//            self.pwdTextField.keyboardType = UIKeyboardTypeDefault;
//        }
//        [self.scrollview addSubview:textField];
//    }
//    
//    UIButton *btn=[MyUtil createBtnFrame:CGRectMake(20, 194+50*4+30, ViewWidth-40, 40) title:@"确  认" bgImageName:nil target:self action:@selector(doResetPwd)];
//    btn.layer.cornerRadius=20.0f;
//    btn.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:150.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
//    
//    [self.scrollview addSubview:btn];
//    
//    self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(btn.frame) + 10);
//}

#pragma mark - 获取验证码
-(void)doGetVerifyCode
{
    
    NSString *mobileText = self.mobileTextField.text.trim;
    
    NSString *error;
    if (![UserHelper validatePhoneNum:mobileText error:&error]) {
        [self showAlertView:error];
        return;
    }
    
    NSString *mobile = self.mobileTextField.text.trim;
//    NSString *params = [NSString stringWithFormat:@"mobile=%@&osType=1", mobile];
    [self showHUDIndicatorViewAtCenter:MSG_GET_VERIFY_CODING];
    WeakSelf
    [[UserManager sharedInstance] doGetVerifyCode:mobile success:^(NSInteger code_status) {
//        StrongSelf
        
        [NSThread sleepForTimeInterval:2];
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        if (code_status == STATUS_OK) {
            [weakSelf.verifyButton countDownTime:60 countDownBlock:^(NSUInteger timer) {
                weakSelf.verifyButton.enabled = NO;
                [weakSelf.verifyButton setTitle:[NSString stringWithFormat:@"%lu秒后重发", (unsigned long)timer] forState:UIControlStateNormal];
//                strongSelf.verifyButton.titleLabel.textColor = [UIColor whiteColor];
            } outTimeBlock:^{
                weakSelf.verifyButton.enabled = YES;
                [weakSelf.verifyButton setTitle:@"获取验证码" forState:UIControlStateNormal];
            }];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
        
    } failure:^(NSInteger code_status) {
        
//        StrongSelf
        [weakSelf hideHUDIndicatorViewAtCenter];
        if (code_status == NETWORK_FAILED) {
            [weakSelf showAlertView:MSG_GET_VERIFY_FAILURE];
        }
        else{
            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
        
    }];
}

#pragma mark - 重设密码
-(void)doResetPwd
{
    if (IsStrEmpty(self.mobileTextField.text.trim)) {
        [self showAlertView:MSG_MOBILE_NULL];
        return;
    }
    if (IsStrEmpty(self.verifyCodeTextField.text.trim)) {
        [self showAlertView:MSG_VERIFY_CODE_NULL];
        return;
    }
    if (IsStrEmpty(self.pwdTextField.text.trim)) {
        [self showAlertView:MSG_NEW_PWD_NULL];
        return;
    }
    if (IsStrEmpty(self.reNewPwdTextField.text.trim)) {
        [self showAlertView:MSG_RE_NEW_PWD_NULL];
        return;
    }
    
    NSString *error;
    if (![UserHelper validatePassWord:self.pwdTextField.text.trim error:&error]) {
        [self showAlertView:error];
        return;
    }
    
    if (![self.pwdTextField.text.trim isEqualToString:self.reNewPwdTextField.text.trim]) {
        [self showAlertView:MSG_TWICE_PWD_NOT_SAME];
        return;
    }
    
    [self showHUDIndicatorViewAtCenter:MSG_RESET_PWDING];
    NSString *params = [NSString stringWithFormat:@"username=%@&password=%@&captcha=%@", self.mobileTextField.text.trim, self.pwdTextField.text.trim, self.verifyCodeTextField.text.trim];
    [self showHUDIndicatorViewAtCenter:MSG_RESET_PWDING];
    WeakSelf
    [[UserManager sharedInstance] doResetPwd:params success:^(NSInteger code_status) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        if (code_status == STATUS_OK) {

            [[UserManager sharedInstance] setStorePassword:strongSelf.pwdTextField.text.trim];
            
            [strongSelf handleResetPwdSuccess];
            
//            [strongSelf handleLoginSuccess];
            
#if 1
//            [MsgToolBox showToast:@"重设密码成功"];
            
//            [strongSelf dismissToRootViewController:YES completion:nil];
            
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"重设密码成功" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
//            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//                
//                if (buttonIndex == 0) {
// 
//                    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
//                    
//                    [strongSelf dismissViewControllerAnimated:YES completion:nil];
//                    
//                }
//                
//            }];
#endif
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } failure:^(NSInteger code_status) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        [weakSelf showAlertView:MSG_RESET_PWD_FAILURE];
    }];
}

-(void)gotoSure
{
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
