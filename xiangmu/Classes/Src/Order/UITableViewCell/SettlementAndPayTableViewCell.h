//
//  SettlementAndPayTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarginAndPayResponseData.h"
#import "CustomRadioButton.h"
#import "CustomRadioButtonGroup.h"

@interface SettlementAndPayEditView : UIView

@property (weak, nonatomic) IBOutlet UILabel *companyInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *srcPriceLabel;
@property (weak, nonatomic) IBOutlet UITextField *settlementPriceTextField;
@property (weak, nonatomic) IBOutlet UIView *settlementPriceView;



@end

@protocol SettlementAndPayTableViewCellDelegate<NSObject>

-(void)doGetSettlementPrice;
-(void)doRefreshSettlementAndPayModule:(NSInteger)status;
-(void)onClickRate:(NSInteger)index;


@end

typedef NS_ENUM(NSInteger, CompleteStatus)
{
    CompleteStatusWaitSettle = 0,
    CompleteStatusFinishSettle,
    CompleteStatusCheckPay,
    CompleteStatusWaitPay,
    CompleteStatusHavePay
};

@interface SettlementAndPayTableViewCell : UITableViewCell<CustomRadioButtonGroupDelegate>

@property (weak, nonatomic) IBOutlet UIView *bottomLine;// 下划线

@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (weak, nonatomic) IBOutlet UIView *paymentTermView;// 付款方式View 用来加入radio button
@property (weak, nonatomic) IBOutlet UIView *paymentView;
@property (weak, nonatomic) IBOutlet UILabel *partnerLabel;//
@property (weak, nonatomic) IBOutlet UILabel *estimatePriceLabel;
@property (weak, nonatomic) IBOutlet UITextField *settlementPriceTextField;// 输入框
@property (weak, nonatomic) IBOutlet UIButton *settlementButton;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@property (weak, nonatomic) IBOutlet UILabel *settlementStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *payStatusLabel;
@property (nonatomic, assign) NSInteger height;
@property (nonatomic, assign) NSInteger paymentHeight;
@property (nonatomic, assign) CompleteStatus status;// 0:待结算；1:确认支付；2:支付；3:已结算
@property (weak, nonatomic) IBOutlet UIView *bargainView;
@property (weak, nonatomic) IBOutlet UIView *browseInfoView;// 浏览信息视图

@property (weak, nonatomic) IBOutlet UIView *payInfoView;
@property (weak, nonatomic) IBOutlet UILabel *payTermLabel;
@property (weak, nonatomic) IBOutlet UILabel *payPriceLabel;

@property (nonatomic, strong) SettlementAndPayEditView *settlementEditView;// 编辑视图

@property (weak, nonatomic) id<SettlementAndPayTableViewCellDelegate> refreshDelegate;


+ (instancetype)cellWithTableView:(UITableView *)tableView;

- (void)initData:(BarginAndPayResponseModel*)model;

@property (nonatomic, strong) BarginAndPayResponseModel *model;

@end
