//
//  BaseViewController+PaypwdCheck.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController (PaypwdCheck)

-(void)checkHasPayPwd;

@end
