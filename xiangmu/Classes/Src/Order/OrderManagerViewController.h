//
//  DispatchOrderViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
//#import "TPKeyboardAvoidingScrollView.h"
#import "TPKeyboardAvoidingTableView.h"

#import "OrderBaseInfoResponseModel.h"
#import "DispatchPriceResponseModel.h"
#import "VechileArrangeResponseModel.h"
#import "WayBillResponseModel.h"
#import "BarginAndPayResponseData.h"
#import "OrderCopyResponseModel.h"
#import "PartnerModel.h"

#import "NewOrderHttpMessage.h"
#import "DispatchOrderHttpMessage.h"
#import "VechileArrangeHttpMessage.h"
#import "WayBillHttpMessage.h"
#import "SettlementHttpMessage.h"
#import "PayHttpMessage.h"
#import "AgreepayHttpMessage.h"

typedef NS_ENUM(NSUInteger, ActionSheetTag) {
    ActionSheetTagCustomerMobile = 1000,
    ActionSheetTagDriverMobile
};

@interface OrderManagerViewController : BaseViewController

@property (strong, nonatomic) TPKeyboardAvoidingTableView *orderTableView;
//@property (strong, nonatomic) OrderTableView *orderTableView;
//@property (nonatomic, strong) DispatchObjectTableView *dispatchObjectTableView;// 派单对象tableView

@property (nonatomic, assign) NSInteger orderMode;// 模式，0:订单管理；1:订单详情
@property (assign, nonatomic) NSInteger orderStatus;
@property (nonatomic, strong) NSArray *headerStatuses;// 订单属性列表
@property (strong, nonatomic) NSString *orderId;// 订单号
@property (strong, nonatomic) OrderBaseInfoResponseModel *orderBaseInfoModel;
@property (strong, nonatomic) DispatchPriceResponseModel *dispatchPriceModel;
@property (strong, nonatomic) VechileArrangeResponseModel *vechileArrangeModel;
@property (strong, nonatomic) WayBillResponseModel *wayBillModel;
@property (strong, nonatomic) BarginAndPayResponseModel *barginAndPayModel;// 结算和支付返回信息

@property (strong, nonatomic) NewOrderHttpMessage *orderHttpMessage;
@property (strong, nonatomic) DispatchOrderHttpMessage *dispatchOrderRequestMsg;
@property (strong, nonatomic) VechileArrangeHttpMessage *vechileArrangeReqeustMsg;
@property (strong, nonatomic) WayBillHttpMessage *wayBillRequestMsg;
@property (strong, nonatomic) SettlementHttpMessage *settleHttpMsg;
@property (strong, nonatomic) AgreepayHttpMessage *agreePayHttpMsg;
@property (strong, nonatomic) PayHttpMessage *payHttpMessage;

-(void)downloadCarImage:(NSString*)imgUrl;

//@property (nonatomic, strong) OrderCopyResponseModel *copyOrderModel;

//@property (strong, nonatomic) NSData *vechileImageData;

@property (nonatomic, strong) NSString *pasteOrderString;

//@property (nonatomic, strong) NSString *copy;

@end
