//
//  AddDiaoViewController.m
//  xiangmu
//
//  Created by David kim on 16/3/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AddDiaoViewController.h"
#import "MyUtil.h"
#import "NetworkManager.h"
#import "HttpConstants.h"
#import "UserManager.h"
#import "DiaoduViewController.h"
#import "AddCarTableViewCell.h"
#import "AddDiaoDetailViewController.h"

@interface AddDiaoViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSIndexPath *sIndexPath;
}
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UITableView *diaoTabelView;
@end

@implementation AddDiaoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加调度员";
    //    self.view.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    _diaoTabelView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, ViewHeight) style:UITableViewStylePlain];
    _diaoTabelView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    _diaoTabelView.delegate=self;
    _diaoTabelView.dataSource=self;
    _diaoTabelView.showsVerticalScrollIndicator=NO;
    _diaoTabelView.tableFooterView=[[UIView alloc]init];
    [self.view addSubview:_diaoTabelView];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(gotoSave)];
    
    //    [self createMyNav];
    // Do any additional setup after loading the view.
}

-(void)getRow:(NSInteger)row WithTitle:(NSString *)title
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.diaoTabelView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=title;
}

#pragma UITableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    AddCarTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[AddCarTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    NSArray *nameArray=@[@"姓名",@"电话"];
    NSArray *placeholderArray=@[@"请填写姓名",@"请填写电话号码"];
    cell.textLabel.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
    cell.textLabel.font=[UIFont systemFontOfSize:15];
    cell.textLabel.text=nameArray[indexPath.row];
    cell.rightLabel.text=placeholderArray[indexPath.row];
    //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddDiaoDetailViewController *addDiaoDetailVC=[[AddDiaoDetailViewController alloc]init];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    AddCarTableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    addDiaoDetailVC.name=[NSString stringWithFormat:@"%@",cell.textLabel.text];
    addDiaoDetailVC.rightname=[NSString stringWithFormat:@"%@",cell.rightLabel.text];
    addDiaoDetailVC.type=DiaoTypeAdd;
    addDiaoDetailVC.row=indexPath.row;
    addDiaoDetailVC.delegate=self;
    [self.navigationController pushViewController:addDiaoDetailVC animated:YES];
}

-(void)gotoBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([self.diaoTabelView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.diaoTabelView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.diaoTabelView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.diaoTabelView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}
-(void)gotoSave
{
    for (int i=0; i<2; i++)
    {
        sIndexPath=[NSIndexPath indexPathForRow:i inSection:0];
    }
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.diaoTabelView cellForRowAtIndexPath:sIndexPath];
    if ([cell.rightLabel.text isEqualToString:@"请填写姓名"]||[cell.rightLabel.text isEqualToString:@"请填写电话号码"])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"必填信息没有添加完整" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
        NSLog(@"%@",cell.rightLabel.text);
        NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:0 inSection:0];
        AddCarTableViewCell *cell1=(AddCarTableViewCell *)[self.diaoTabelView cellForRowAtIndexPath:indexPath1];
        
        
        NSIndexPath *indexPath2=[NSIndexPath indexPathForRow:1 inSection:0];
        AddCarTableViewCell *cell2=(AddCarTableViewCell *)[self.diaoTabelView cellForRowAtIndexPath:indexPath2];
        
        
        self.nextAction = @selector(gotoSave);
        self.object1 = nil;
        self.object2 = nil;
        
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Diaodu_Add_API];
        NSString *parmas = [NSString stringWithFormat:@"nickname=%@&username=%@", cell1.rightLabel.text,cell2.rightLabel.text];
        
        [self showHUDIndicatorViewAtCenter:@"正在添加调度员，请稍候"];
        WeakSelf
        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
            
            StrongSelf
            [strongSelf resetAction];
            [strongSelf hideHUDIndicatorViewAtCenter];
            
            NSLog(@"response success");
            NSLog(@"result = %@", responseData);
            NSInteger code_status = [responseData[@"code"] integerValue];
            if(code_status == STATUS_OK)
            {
                if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(diaodu)]) {
                    [strongSelf.delegate diaodu];
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }
            else {
                [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
            }
            
        } andFailure:^(NSString *errorDesc) {
            
            NSLog(@"response failure");
            
            StrongSelf
            [strongSelf resetAction];
            [strongSelf hideHUDIndicatorViewAtCenter];
            
            [strongSelf showAlertView:errorDesc];
            
//            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
            
        }];
        
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
