//
//  CurrentAdvRevenue.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol AdvRevenueDetail <NSObject>

@end

@interface AdvRevenueDetail : JSONModel

@property (nonatomic, strong) NSString<Optional> *startHour;
@property (nonatomic, strong) NSString<Optional> *endHour;
@property (nonatomic, strong) NSNumber<Optional> *viewCount;
@property (nonatomic, strong) NSNumber<Optional> *thumbsUpCount;
@property (nonatomic, strong) NSNumber<Optional> *costs;

@end

@interface CurrentAdvRevenue : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *amountTotal;//总收益
@property (nonatomic, strong) NSNumber<Optional> *thumbsCountTotal;// 点击数
@property (nonatomic, strong) NSNumber<Optional> *viewCountTotal;// 查看数
@property (nonatomic, strong) NSMutableArray<Optional,AdvRevenueDetail> *list;//

@end
