//
//  LocationSearchBar.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LocationSearchBarDelegate <NSObject>

@optional

- (BOOL)searchFieldShouldBeginEditing:(UITextField *)textField;
- (void)searchBar:(UITextField *)searchField textDidChange:(NSString *)searchText;
- (BOOL)searchFieldShouldEndEditing:(UITextField *)textField;
- (void)searchFieldSearchButtonClicked:(UITextField *)searchField;

@end

@interface LocationSearchBar : UIView<UITextFieldDelegate>

@property (weak, nonatomic) id<LocationSearchBarDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *cityButton;
@property (weak, nonatomic) IBOutlet UIButton *speechButton;

@property (strong, nonatomic) NSString *lastSearchKey;
@property (strong, nonatomic) NSString *searchKey;

@end
