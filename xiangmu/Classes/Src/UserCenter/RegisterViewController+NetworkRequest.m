//
//  RegisterViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "RegisterViewController+NetworkRequest.h"
#import "BaseViewController+HandleLoginSuccess.h"

@implementation RegisterViewController (NetworkRequest)

- (void)doRegisterThirdparty
{
    if (self.thirdPartyLoginHttpMessage == nil) {
        return;
    }
    
    NSString *mobileText = self.thirdpartyRegisterView.accountTextField.text.trim;
    NSString *verifyCodeText = self.thirdpartyRegisterView.verifyCodeTextField.text.trim;
    NSString *referralCode = [StringUtil getSafeString:self.thirdpartyRegisterView.recommendCodeTextField.text.trim];
    
    NSString *error;
    
    if (![UserHelper validatePhoneNum:mobileText error:&error]) {
        [self showAlertView:error];
        return;
    }
    
    if (![UserHelper validateCodeString:verifyCodeText error:&error]) {
        [self showAlertView:error];
        return;
    }
    
    NSString *params = [NSString stringWithFormat:@"username=%@&captcha=%@&%@&referralCode=%@", mobileText, verifyCodeText,self.thirdPartyLoginHttpMessage.description, referralCode];
    
    [self showHUDIndicatorViewAtCenter:MSG_IS_BIND_ACCOUNT];
    WeakSelf
    [[UserManager sharedInstance] doThirdPartyRegister:params mobile:mobileText thirdPartyId:self.thirdPartyId success:^(NSInteger code_status) {
        StrongSelf
        [strongSelf dismissToRootViewController:YES completion:nil];
    } failure:^(NSInteger code_status) {
        [weakSelf hideHUDIndicatorViewAtCenter];
        [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
    }];
    
}

-(void)doRegister
{
    NSString *mobileText = self.registerView.accountTextField.text.trim;
    NSString *verifyCodeText = self.registerView.verifyCodeTextField.text.trim;
    NSString *passwordText = self.registerView.pwdTextField.text.trim;
    NSString *referralCodeText = [StringUtil getSafeString:self.registerView.recommendCodeTextField.text.trim];
    
    NSString *error;
    
    if (![UserHelper validatePhoneNum:mobileText error:&error]) {
        [self showAlertView:error];
        return;
    }
    
    if (![UserHelper validateCodeString:verifyCodeText error:&error]) {
        [self showAlertView:error];
        return;
    }
    
    if (![UserHelper validatePassWord:passwordText error:&error]) {
        [self showAlertView:error];
        return;
    }
    
    NSString *params = [NSString stringWithFormat:@"captcha=%@&referralCode=%@", verifyCodeText, referralCodeText];
    
    [self showHUDIndicatorViewAtCenter:MSG_IS_REGISTERING];
    WeakSelf
    [[UserManager sharedInstance] doRegister:params userName:mobileText password:passwordText
                                     success:^(NSInteger code_status) {
                                         StrongSelf
                                         [strongSelf doLogin:mobileText password:passwordText];
                                     } failure:^(NSInteger code_status) {
                                         [weakSelf hideHUDIndicatorViewAtCenter];
                                         [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
                                     }];
    
}

-(void)doLogin:(NSString*)userName password:(NSString*)paswordPlain
{
    // 先登录
    //    [self showHUDIndicatorViewAtCenter:MSG_IS_LOGING];
    WeakSelf
    [[UserManager sharedInstance] doLogin:userName password:paswordPlain success:^(NSInteger code_status) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        [strongSelf handleLoginSuccess];
        
        //        if (code_status == STATUS_OK) {
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
//        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:MSG_REGISTER_SUCCESS delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        
//        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//            
//            if (buttonIndex == 0) {
//                
//                if (strongSelf.registerType == 0) {
//                    
//                    [strongSelf handleLoginSuccess];
//                    
////                    strong S elf
////                    [strongSelf gotoNextPage];
//                
//                }
//
//            }
//            
//        }];
        
    } failure:^(NSInteger code_status) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        //        [strongSelf showAlertView:MSG_REGISTER_FAILURE];
        [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
    }];
}

#pragma mark - 获取验证码
-(void)doGetVerifyCode
{
    
    NSString *mobile = self.mobileTextField.text.trim;
    
    NSString *error;
    if (![UserHelper validatePhoneNum:mobile error:&error]) {
        [self showAlertView:error];
        return;
    }
    
    [self showHUDIndicatorViewAtCenter:MSG_GET_VERIFY_CODING];
    
    WeakSelf
    [[UserManager sharedInstance] doGetRegisterVerifyCode:mobile success:^(NSInteger code_status) {
        
        [NSThread sleepForTimeInterval:2];
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        if (code_status == STATUS_OK) {
            
            [weakSelf.verifyCodeBtn countDownTime:60 countDownBlock:^(NSUInteger timer) {
                weakSelf.verifyCodeBtn.enabled = NO;
                [weakSelf.verifyCodeBtn setTitle:[NSString stringWithFormat:@"%lu秒后重发", (unsigned long)timer] forState:UIControlStateNormal];
            } outTimeBlock:^{
                weakSelf.verifyCodeBtn.enabled = YES;
                [weakSelf.verifyCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
            }];
            
        }
    } failure:^(NSInteger code_status) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        if (code_status == NETWORK_FAILED) {
            [weakSelf showAlertView:MSG_GET_VERIFY_FAILURE];
        }
        else {
            [weakSelf showAlertView:[weakSelf getErrorMsg:code_status]];
        }
        
    }];
}


@end
