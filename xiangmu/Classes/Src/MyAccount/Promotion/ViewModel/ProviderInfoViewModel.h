//
//  ProviderInfoViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewModel.h"
#import "CurrentAdvRevenue.h"
#import "ProviderRankModel.h"
#import "ProviderOrderModel.h"
#import "ProviderUtil.h"

@interface ProviderInfoViewModel : BaseViewModel

@property (nonatomic, strong) NSMutableArray *todayDetailList;// 今日详情
//@property (nonatomic, strong) NSMutableArray *advList; // 广告列表
@property (nonatomic, strong) CurrentAdvRevenue *currentAdvRevenue; // 当前广告收益
@property (nonatomic, strong) ProviderRankSumModel *rankSumModel;
@property (nonatomic, strong) NSMutableArray<ProviderOrderModel*> *orderList; // 购买的广告订单列表

@property (nonatomic, strong) NSArray *sectionRows;
@property (nonatomic, assign) NSInteger sectionNum;
@property (nonatomic, strong) NSArray *rowHeightList;

@end
