//
//  AdPurchaseDetailTableView.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdPurchaseDetailTableView.h"

@implementation AdPurchaseDetailTableView

-(id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    
    if (self) {
        
        self.dataSource = self;
        
        self.delegate = self;
        
    }
    
    return self;
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    return 1;
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return 4;
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == AdvPurchaseDetailRowTItle) {
//        return 140;
//    }
//    if (indexPath.section == AdvPurchaseDetailRowCalendar) {
//        return 315;
//    }
//    if (indexPath.section == AdvPurchaseDetailRowTime) {
//        return 350;
//    }
//    if (indexPath.section == AdvPurchaseDetailRowSelection) {
//        return 115;
//    }
    
    return 0.0f;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if (section > AdvPurchaseDetailRowTItle) {
//        return 45;
//    }
    
    return 0.0001f;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    if (section > AdvPurchaseDetailRowTItle) {
//        //        return [HeaderView customView];
//        HeaderView *headerView = [HeaderView customView];
//        if (section == AdvPurchaseDetailRowCalendar) {
//            headerView.titleLabel.text = @"选择日期";
//        }
//        else if (section == AdvPurchaseDetailRowSelection) {
//            headerView.titleLabel.text = @"已选时间";
//        }
//        else if (section == AdvPurchaseDetailRowTItle) {
//            headerView.titleLabel.text = @"选择时间";
//        }
//        return headerView;
//    }
    return [UIView new];
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%lu%lu", (long)indexPath.section, (long)indexPath.row];
    // 广告条
//    if (indexPath.section == AdvPurchaseDetailRowTItle) {
//        
//        static NSString *CellIdentifier = @"TItleCellID";
//        
//        AdvPurchaseInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        
//        if (!cell) {
//            cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AdvPurchaseInfoTableViewCell class]) owner:nil options:nil] lastObject];
//            //            cell = [[AdvPurchaseInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//            cell.positionTItleLabel.text = [NSString stringWithFormat:@"第%ld版位", (long)_position];
//            cell.selectTextLabel.text = @"推广专区";
//        }
//        
//        return cell;
//        
//    }
//    
//    else {
    
        static NSString *CellIdentifier = @"CellIdentifier";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        return cell;
//    }
    //    else {
    //        //    if (indexPath.row == AdvPurchaseRowOnlienProvider) {
    //
    //        static NSString *CellIdentifier = @"AdvPurchaseTableViewCell";
    //
    //        AdvPurchaseTableViewCell *cell = (AdvPurchaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    //        if (!cell) {
    //            cell = [[AdvPurchaseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //            cell.collectionView.customDelegate = self;
    //            cell.collectionView.tag = indexPath.row;
    //        }
    //
    //        if (indexPath.row == AdvPurchaseRowOnlienProvider) {
    //            cell.headerView.titleLabel.text = @"在线商户";
    //        }
    //        if (indexPath.row == AdvPurchaseRowLocalArea) {
    //            cell.headerView.titleLabel.text = @"所属专区";
    //        }
    //
    //        
    //        
    //        return cell;
    //        
    //    }
    
    return nil;
    
    
}


@end
