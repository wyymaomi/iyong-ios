//
//  ContactListService.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ContactListService.h"
#import "AuthorizationUtil.h"
#import "UIRoute.h"


@interface ContactListService ()

@property (nonatomic, weak) UIViewController *viewController;

@end

@implementation ContactListService

-(id)initWithViewController:(UIViewController*)viewController;
{
    self = [super init];
    if (self) {
        _viewController = viewController;
        
    }
    
    return self;
}



- (void)showContactList;
{
    
    // 判断是否有推送通知权限
    if (![AuthorizationUtil isAllowContact]) {
        [UIRoute openSystemSetting:kMsgOpenSystemPushnotification];
        return;
    }
    
    if (IOS9_OR_LATER) {
        
        CNContactRootViewController *contactRootViewController = [CNContactRootViewController new];
//        contactRootViewController.delegate = contactRootViewController;
        contactRootViewController.customDelegate = _viewController;
        [_viewController presentViewController:contactRootViewController animated:YES completion:nil];
//        [self presentViewController:contactRootViewController animated:YES completion:nil];
        
    }
    else {
        ABRootViewController *abRootViewController = [[ABRootViewController alloc] init];
        abRootViewController.customDelegate = _viewController;
        abRootViewController.peoplePickerDelegate = abRootViewController;
        if(IOS8_OR_LATER){
            abRootViewController.predicateForSelectionOfPerson = [NSPredicate predicateWithValue:false];
        }
        [_viewController presentViewController:abRootViewController animated:YES completion:nil];
//        [self presentViewController:abRootViewController animated:YES completion:nil];
    }
}

//-(void)onGetContactMobile:(NSString*)phone;
//{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(onGetMobile:)]) {
//        [self.delegate onGetMobile:phone];
//    }
//}
//
//-(void)onGetContactPhone:(NSString*)phone;
//{
//    [self onGetContactMobile:phone];
////    if (self.delegate && [self.delegate respondsToSelector:@selector(onGetMobile:)]) {
////        [self.delegate onGetMobile:phone];
////    }
//}

@end

#pragma mark - ABRootViewController

@implementation ABRootViewController


#pragma mark - life cycle



#pragma mark - IOS8_OR_LATER

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    
    if (IsStrEmpty(phoneNO)) {
        return;
    }
    
    if ([phoneNO hasPrefix:@"+"]) {
        phoneNO = [phoneNO substringFromIndex:3];
    }
    if ([phoneNO startsWith:@"86"]) {
        phoneNO = [phoneNO substringFromIndex:2];
    }
    
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"%@", phoneNO);
    
//    CFRelease(phone);
    
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(onGetMobile:)]) {
        [self.customDelegate onGetMobile:phoneNO];
    }
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person
{
    ABPersonViewController *personViewController = [[ABPersonViewController alloc] init];
    personViewController.displayedPerson = person;
    [peoplePicker pushViewController:personViewController animated:YES];
}



#pragma mark - IOS7_OR_BEFORE

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
    long index = ABMultiValueGetIndexForIdentifier(phone,identifier);
    NSString *phoneNO = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phone, index);
    
    if ([phoneNO hasPrefix:@"+"]) {
        phoneNO = [phoneNO substringFromIndex:3];
    }
    if ([phoneNO startsWith:@"86"]) {
        phoneNO = [phoneNO substringFromIndex:2];
    }
    
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"%@", phoneNO);
    
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(onGetMobile:)]) {
        [self.customDelegate onGetMobile:phoneNO];
    }
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
    return NO;
}

//#pragma mark -
//#pragma mark ABPeoplePickerNavigationControllerDelegate
//- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
//{
//    return NO;
//}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:nil];;
}

#pragma mark -
#pragma mark ABPersonViewControllerDelegate methods
// Does not allow users to perform default actions such as dialing a phone number, when they select a contact property.
- (BOOL)personViewController:(ABPersonViewController *)personViewController shouldPerformDefaultActionForPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifierForValue
{
    return NO;
}


@end

@implementation CNContactRootViewController

//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];

//    [self.navigationController.navigationBar setBackgroundColor:NAVBAR_BGCOLOR];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:NAVBAR_BGCOLOR]
//                                                  forBarMetrics:UIBarMetricsDefault];

//}

-(id)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
        self.navigationController.delegate = self;
    }
    return self;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    UINavigationBar *navigationBar = navigationController.navigationBar;
    [navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    navigationBar.tintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

#pragma mark - 代理方法
// 控制器点击取消的时候调用
- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker;
{
    NSLog(@"点击了取消");
}

// 点击了联系人的时候调用, 如果实现了这个方法, 就无法进入联系人详情界面
//- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact {
//
//    // contact属性就是联系人的信息
//    NSLog(@"%@---%@", contact.namePrefix, contact.familyName);
//
//    // 获取联系人的电话号码
//    NSArray<CNLabeledValue<CNPhoneNumber*>*> *phoneNumbers = contact.phoneNumbers;
//
//    // 注意, 由于这个数组规定了泛型, 所以要使用遍历器来取出每一个特定类型的对象, 才能取到里面的属性
//    [phoneNumbers enumerateObjectsUsingBlock:^(CNLabeledValue<CNPhoneNumber*> * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//
//        NSLog(@"%@--%@", obj.label, obj.value.stringValue);
//
//        if ([obj.label contains:@"Mobile"]) {
//
//            NSString *phoneNO = obj.value.stringValue;
//
//            if (IsStrEmpty(phoneNO)) {
//                return;
//            }
//
//            if ([phoneNO hasPrefix:@"+"]) {
//                phoneNO = [phoneNO substringFromIndex:3];
//            }
//            if ([phoneNO startsWith:@"86"]) {
//                phoneNO = [phoneNO substringFromIndex:2];
//            }
//
//            phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
//            phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"(" withString:@""];
//            phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@")" withString:@""];
//            phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@" " withString:@""];
//            NSLog(@"%@", phoneNO);
//
//            if (self.controllerDelegate && [self.controllerDelegate respondsToSelector:@selector(onGetContactMobile:)]) {
//                [self.controllerDelegate onGetContactMobile:phoneNO];
//            }
//        }
//
//    }];
//}

// 点击了联系人的确切属性的时候调用, 注意, 这两个方法只能实现一个
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty {
    DLog(@"%@---%@", contactProperty.key, contactProperty.value);
    
    // 获取手机号
    if ([CNContactPhoneNumbersKey isEqualToString:contactProperty.key]) {
        CNPhoneNumber *phoneNumber = contactProperty.value;
        NSString *phoneNO = phoneNumber.stringValue;
        
        if (IsStrEmpty(phoneNO)) {
            return;
        }
        
        if ([phoneNO hasPrefix:@"+"]) {
            phoneNO = [phoneNO substringFromIndex:3];
        }
        if ([phoneNO startsWith:@"86"]) {
            phoneNO = [phoneNO substringFromIndex:2];
        }
        
        phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"-" withString:@""];
        phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@"(" withString:@""];
        phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@")" withString:@""];
        phoneNO = [phoneNO stringByReplacingOccurrencesOfString:@" " withString:@""];
        DLog(@"%@", phoneNO);
        
        if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(onGetMobile:)]) {
            [self.customDelegate onGetMobile:phoneNO];
        }
    }
}

@end


