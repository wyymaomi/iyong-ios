//
//  NewOrder3ViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "OrderBaseInfoResponseModel.h"
//#import "<#header#>"

@protocol OrderViewControllerDelegate <NSObject>

-(void)onUpdateOrderInfo:(NSDictionary*)dict;

@end

typedef NS_ENUM(NSUInteger, OrderTableViewSection)
{
    OrderTableViewSectionOne,
    OrderTableViewSectionTwo,
    OrderTableViewSectionThree,
    OrderTableViewSectionFour
};

typedef NS_ENUM(NSUInteger, OrderTableViewFirstSection) {
    OrderTableViewRowCarDate=0,
    OrderTableViewRowCarModel,
    OrderTableViewRowLocation,
};

typedef NS_ENUM(NSUInteger, OrderTableViewSecondSection) {
    OrderTableViewRowCustomer = 0,
    OrderTableViewRowMobile
};

typedef NS_ENUM(NSUInteger, OrderTableViewThirdSection) {
    OrderTableViewRowRoughTravel = 0,
    OrderTableViewRowRemark
};

typedef NS_ENUM(NSUInteger, OrderTableViewFourSection) {
    OrderTableViewRowReceivePrice = 0
};

@interface NewOrderViewController : BaseViewController

@property (nonatomic, assign) NSInteger mode;// 0:add; 1:edit
@property (nonatomic, strong) NSString *orderId;// 订单号
@property (nonatomic, strong) OrderBaseInfoResponseModel *orderBaseModel;
@property (nonatomic, weak) id<OrderViewControllerDelegate> delegate;

//@property (nonatomic, strong) BidModel *bidModel;
//@property (nonatomic, strong) EnquireModel *enquireModel;


@end
