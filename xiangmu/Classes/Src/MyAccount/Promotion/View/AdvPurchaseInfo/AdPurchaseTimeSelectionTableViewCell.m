//
//  AdPurchaseTimeSelectionTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/12.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdPurchaseTimeSelectionTableViewCell.h"


@implementation AdPurchaseTimeSelectionTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSInteger columnNum = 4;
        NSInteger startX = 14;
        NSInteger startY = 5;
        CGFloat imgWidth = 75*Scale;
        CGFloat imgHeight = 36*Scale;
        CGFloat separatorX = (ViewWidth-startX * 2 - columnNum*imgWidth)/3;
        CGFloat separatorY = 14*Scale;
        
        
        _buttonArray = [NSMutableArray new];
        
        for (NSUInteger i = 0; i < 24; i++) {
            TimeSegmentView *view = [[TimeSegmentView alloc]initWithFrame:CGRectMake(startX+(imgWidth+separatorX)*(i%columnNum), startY+(imgHeight+separatorY)*(i/columnNum), imgWidth, imgHeight)];
            if (i < 10) {
                view.startTimeLabel.text = [NSString stringWithFormat:@"0%ld:00~", (unsigned long)i];
            }
            else {
                view.startTimeLabel.text = [NSString stringWithFormat:@"%lu:00~",(unsigned long)i];
            }
            
            if (i+1<10) {
                view.endTimeLabel.text = [NSString stringWithFormat:@"~0%u:00", (unsigned)(i+1)];
            }
            else {
                view.endTimeLabel.text = [NSString stringWithFormat:@"~%u:00", (unsigned)(i+1)];
            }
            
            
            view.priceLabel.text = @"";
            view.tag = i;
            
            [self.contentView addSubview:view];
            [_buttonArray addObject:view];
            
            view.button.tag = i;
            
            view.delegate = self;
            
//            [view.button addTarget:self action:@selector(onClickButton:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    
    return self;
}

+(CGFloat)getCellHeight;
{
    return 300*Scale;
}

- (void)setViewModel:(AdvPurchaseViewModel *)viewModel
{
    if (!viewModel || viewModel.saleAdvDetailList == nil || viewModel.saleAdvDetailList.count == 0) {
        return;
    }
    
    _viewModel = viewModel;
    
    
    NSArray *list = self.viewModel.saleAdvDetailList;
    
    if (list == nil) {
        return;
    }
    
    for (NSInteger i = 0; i < list.count; i++) {
        AdvDetailModel *model = list[i];
        TimeSegmentView *view = self.buttonArray[i];
//        if (/*[model.price integerValue] > 0 && model.status == [NSNumber numberWithInteger:AdPurchaseStatusNotBuy]*/[model.purchaseFlag integerValue] == AdPurchaseFlagCanbuy && [model.status integerValue] == AdPurchaseStatusNotBuy) {
        if ([model canBuy]) {
            view.status = TimeSegmentViewStatusUnSelected;
            view.priceLabel.text = [NSString stringWithFormat:@"¥%ld", (long)[model.price integerValue]];
        }
        else {
            view.status = TimeSegmentViewStatusDisabled;
        }
    }
    
    
    // 刷新当前选中的日期的时间段
    NSString *currentDate = self.viewModel.currentSelectDate;
    [self.viewModel.advOrderList enumerateObjectsUsingBlock:^(AdPurchaseOrder * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.date isEqualToString:currentDate]) {
            TimeSegmentView *view = self.buttonArray[[obj.timeIndex integerValue]];
            view.status = TimeSegmentViewStatusSelected;
        }
        
    }];

    
    
}

-(void)onClickTimeView:(UIButton*)button;
//- (void)onClickButton:(id)sender
{
//    UIButton *button = (UIButton*)sender;
    
    BOOL isSelected = button.isSelected;
    BOOL isEnabled = button.isEnabled;
    
    NSUInteger index = button.tag;
    
    TimeSegmentView *timeSegmentView = self.buttonArray[index];
    
    NSString *startTime = [timeSegmentView.startTimeLabel.text stringByReplacingOccurrencesOfString:@"~" withString:@""];
    NSString *endTime = timeSegmentView.endTimeLabel.text;
    NSString *timeSegmentText = [NSString stringWithFormat:@"%@%@", startTime, endTime];
    
    NSString *advId = self.viewModel.saleAdvDetailList[index].id;
    NSNumber *price = self.viewModel.saleAdvDetailList[index].price;
    
    double endTimeStamp = [self.viewModel.saleAdvDetailList[index].endTime doubleValue];
    
    // 判断时间是否过期
    double currentTimeStamp = [[NSDate getTimeStamp] doubleValue];
    if (currentTimeStamp > endTimeStamp) {
//        timeSegmentView.status = TimeSegmentViewStatusDisabled;
        [MsgToolBox showToast:@"选择的时间段已过期，请重新选择"];
        return;
    }
    
    if (timeSegmentView.status == TimeSegmentViewStatusSelected) {
        if ( _cellDelegate && [_cellDelegate respondsToSelector:@selector(shouldSelectTimeSegmentView)]) {
            BOOL result = [_cellDelegate shouldSelectTimeSegmentView];
            if (!result) {
                timeSegmentView.status = TimeSegmentViewStatusUnSelected;
                return;
            }
        }
    }
    

    if (_cellDelegate && [_cellDelegate respondsToSelector:@selector(onClickButton:timeSegmentText:adId:price:endTime:isSelected:)]) {
        [_cellDelegate onClickButton:index timeSegmentText:timeSegmentText adId:advId price:price endTime:endTimeStamp isSelected:isSelected];
    }

    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
