//
//  ProviderUtil.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProviderUtil : NSObject

+(NSString*)displayAdvTitle:(NSUInteger)advType;

+(NSString*)displayAdOrderStatusText:(NSUInteger)status;

+(NSString*)getAdOrderDate:(NSString*)timemillions;

+(NSString*)getAdOrderTimeSegment:(NSString*)startTime endTime:(NSString*)endTime;

@end
