//
//  ServiceProviderViewModel.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "ServiceProviderViewModel.h"
#import "OnlineServiceModel.h"
#import "ResourceConstants.h"
//#import "UIView+Category.h"

@implementation ServiceProviderViewModel

- (void)setServiceModel:(OnlineServiceModel *)serviceModel
{
    _serviceModel = serviceModel;
//    _moment = moment;
    //计算主体Frame
//    [self setMomentsBodyFrames];
//    //计算工具条Frame
//    [self setMomentsToolBarFrames];
//    //计算CellHeight
//    [self setCellHeight];
}

//计算Code圈主体Frame
- (void)setMomentsBodyFrames{
    
    // 公司基本信息
//    CGFloat companyX = 0;
//    CGFloat companyY = 0;
//    CGFloat companyWidth = ViewWidth;
//    CGFloat companyHeight = 50;
//    self.bodyCompanyFrame = CGRectMake(companyX, companyY, companyWidth, companyHeight);
    
    //头像
    CGFloat iconX = circleCellMargin;
    CGFloat iconY = circleCellMargin;
    CGFloat iconW = circleCelliconWH;
    CGFloat iconH = circleCelliconWH;
    self.bodyIconFrame = CGRectMake(iconX, iconY, iconW, iconH);
    
    //昵称
    CGFloat nameX = circleCelliconWH + circleCellMargin * 2;
    CGFloat nameY = iconY;
    //    CGFloat nameW = 120;
    CGSize nameSize = [self.serviceModel.nickname sizeWithAttributes:circleCellNameattributes];
    self.bodyNameFrame = (CGRect){{nameX,nameY},nameSize};
    
    //时间
    CGFloat timeX = nameX;
    CGFloat timeY = CGRectGetMaxY(self.bodyNameFrame) + 5;
    CGSize timeSize = [self.serviceModel.companyName boundingRectWithSize:CGSizeMake(ViewWidth-timeX-circleCellMargin, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:circleCellTimeattributes context:nil].size;
    self.bodyTimeFrame = (CGRect){{timeX,timeY},timeSize};
    
    
    //正文
    CGFloat textX = circleCellMargin;
    CGFloat textY = CGRectGetMaxY(self.bodyIconFrame) + 5;
//    if (IsStrEmpty(self.serviceModel)) {
//        self.bodyTextFrame = (CGRect){{textX, CGRectGetMaxY(self.bodyIconFrame) + 1},{0,0}};
//    }
//    else {
//        CGFloat textW = circleCellWidth - circleCellMargin * 2;
//        CGSize textSize = [self.serviceModel.business boundingRectWithSize:CGSizeMake(textW, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:circleCellTextattributes context:nil].size;
//        self.bodyTextFrame = (CGRect){{textX,textY},textSize};
//    }

    
    //图片 (判断是否有图片)
    if ([self.serviceModel hasPhoto]) {
        CGFloat photosX = 15;
        CGFloat photosY = CGRectGetMaxY(self.bodyTextFrame) + 5;
        CGFloat photosW = (ViewWidth -  15 * 2);
        CGFloat photosH = 60;
        self.bodyPhotoFrame = CGRectMake(photosX, photosY, photosW, photosH);
        //主体frame
        CGFloat bodyH = CGRectGetMaxY(self.bodyPhotoFrame) + circleCellMargin;
        self.momentsBodyFrame = CGRectMake(0, 0, circleCellWidth, bodyH);
    }else {
        //主体frame
        CGFloat bodyH = CGRectGetMaxY(self.bodyTextFrame) + circleCellMargin;
        self.momentsBodyFrame = CGRectMake(0, 0, ViewWidth, bodyH);
    }
}

//计算Code圈工具条Frame
- (void)setMomentsToolBarFrames
{
    //工具条frame
    CGFloat toolBarX = 0;
    CGFloat toolBarY = CGRectGetMaxY(self.momentsBodyFrame)+1;
    CGFloat toolBarW = ViewWidth;
    CGFloat toolBarH = circleCellToolBarHeight;
    self.momentsToolBarFrame = CGRectMake(toolBarX, toolBarY, toolBarW, toolBarH);
    
    // 通话
    CGFloat telX = 0;
    CGFloat telY = 0;
    CGFloat telW = toolBarW/3;
    CGFloat telH = circleCellToolBarHeight;
    self.toolTelFrame = CGRectMake(telX, telY, telW, telH);
    
    //收藏
    CGFloat commentX = telX + telW + 1;
    CGFloat commentY = 0;
    CGFloat commentW = telW;
    CGFloat commentH = telH;
    self.toolFavoriteFrame = CGRectMake(commentX, commentY, commentW, commentH);
    
    //点赞
    CGFloat likeX = commentX + commentW + 1;
    CGFloat likeY = 0;
    CGFloat likeW = toolBarW / 3;
    CGFloat likeH = circleCellToolBarHeight;
    self.toolLikeFrame = CGRectMake(likeX, likeY, likeW, likeH);
}

- (void)setCellHeight{
    self.cellHeight = CGRectGetMaxY(self.momentsToolBarFrame);
}

@end
