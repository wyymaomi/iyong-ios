//
//  AvatarViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/8.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AvatarViewController+NetworkRequest.h"
#import "AliyunOSSManager.h"

@implementation AvatarViewController (NetworkRequest)

-(void)saveUserInfo
{
    self.nextAction = @selector(saveUserInfo);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    //    NSData *avatarImageData =
    NSArray *array = @[self.avatarImageData];
    [self showHUDIndicatorViewAtCenter:@"正在保存头像，请稍候..."];
    WeakSelf
    [[AliyunOSSManager sharedInstance] uploadImages:array isAsync:YES folderName:kAliyunOSSPublichImagePath([UserManager sharedInstance].userModel.companyId) complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        StrongSelf
        
        if (state == UploadImageSuccess) {
            
//            strongSelf.userInfoHttpMessage.logoUrl = names[0];
            
            NSString *avatarImgUrl = names[0];
            
            NSString *params = [NSString stringWithFormat:@"logoUrl=%@", avatarImgUrl];
            
            [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(UserInfo_Update_API) requestMethod:POST params:params success:^(id responseData) {
                
                [strongSelf hideHUDIndicatorViewAtCenter];
                
                NSInteger code_status = [responseData[@"code"] integerValue];
                
                if (code_status == STATUS_OK) {
    
                    if (strongSelf.changeAvatarImage) {
                        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(onGetAvatarImage:)]) {
                            [strongSelf.delegate onGetAvatarImage:self.avatarImageData];
                        }
                    }
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"头像更新成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                       
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                        
                    }];
//                    [strongSelf.navigationController popViewControllerAnimated:YES];
                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:kOnUserinfoUpdateNotification object:nil userInfo:nil];
                    
//                    [strongSelf.navigationController popViewControllerAnimated:YES];
                    
//                    UserModel *userModel = [[UserModel alloc] initWithDictionary:responseData[@"data"] error:nil];
//                    
//                    [UserManager sharedInstance].userModel = userModel;//[userModel mutableCopy];
                    
                }
                
            } andFailure:^(NSString *errorDesc) {
                
                //                StrongSelf
                [strongSelf hideHUDIndicatorViewAtCenter];
                [strongSelf showAlertView:errorDesc];
                [strongSelf resetAction];
                
            }];
            
        }
        else {
            [strongSelf hideHUDIndicatorViewAtCenter];
            [strongSelf resetAction];
            [MsgToolBox showAlert:@"" content:@"上传头像失败，请重新上传"];
        }
        
        
    }];
    
}

//-(void)updateUserInfo:(NSInteger)tag text:(NSString*)text;
-(void)updateUserAvatar:(NSString*)avatarUrl
{
//    NSString *params;
//    if (tag == MyNicknameTextField) {
//        params = [NSString stringWithFormat:@"nickname=%@", text];
//    }
//    parrams = [NSString stringWithFormat:@"logoUrl=%@", avatarUrl];
    
    NSString *params = [NSString stringWithFormat:@"logoUrl=%@", avatarUrl];
    
    [self showHUDIndicatorViewAtCenter:@"正在设置头像，请稍候"];
    //    __weak __typeof(self) weakSelf = self;
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(UserInfo_Update_API) requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        //        __strong __typeof(self) strongSelf = weakSelf;
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"更新昵称成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == STATUS_OK) {
                    
                    if (strongSelf.changeAvatarImage) {
                        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(onGetAvatarImage:)]) {
                            [strongSelf.delegate onGetAvatarImage:self.avatarImageData];
                        }
                    }
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                    
                }
                
            }];
        }
        else {
            [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [MsgToolBox showAlert:@"" content:errorDesc];
        
    }];
}


@end
