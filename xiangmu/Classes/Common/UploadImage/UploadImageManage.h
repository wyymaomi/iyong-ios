//
//  UploadImageManage.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/1.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UploadImageManageDelegate <NSObject>

- (void)didFinishPicker:(UIImage*)image actionSheet:(UIActionSheet*)actionSheet;

@end


@interface UploadImageManage : NSObject

//AS_SINGLETON(UploadImageManage)

@property (nonatomic, strong) UIViewController *viewController;

@property (nonatomic, weak) id<UploadImageManageDelegate> delegate;

- (instancetype)initWithViewController:(UIViewController*)viewController;

- (void)showActionSheet:(NSInteger)tag;

- (void)showActionSheet;


@end
