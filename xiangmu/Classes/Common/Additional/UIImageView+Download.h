//
//  UIImageView+Download.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SuccessBlock)(id responseData);
typedef void(^FailureBlock)(NSString *errorDesc);

@interface UIImageView (Download)


- (void)downloadImageFromAliyunOSS:(NSString*)imageUrl
                       isThumbnail:(BOOL)isThumbnail
                  placeholderImage:(UIImage*)placeholderImage
                           success:(SuccessBlock)success
                        andFailure:(FailureBlock)failure;

//-(void)downloadAvatarImageView:(NSString*)imageUrl
//    placeholderImage:(UIImage*)placeholderImage
//             success:(SuccessBlock)success
//          andFailure:(FailureBlock)failure;

//- (void)downloadImage:(NSString*)imageUrl
//     placeholderImage:(UIImage*)placeholderImage
//              success:(SuccessBlock)success
//           andFailure:(FailureBlock)failure;



@end
