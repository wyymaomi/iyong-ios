//
//  HomeExperienceTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomeExperienceTableViewCell.h"

@implementation HomeExperienceTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier model:model];
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(130*Scale, 165*Scale);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        self.collectionView = [[HomeExperienceCollectionView alloc] initWithFrame:CGRectMake(12, 40, ViewWidth-12, 175*Scale) collectionViewLayout:flowLayout];
        self.collectionView.scrollEnabled = YES;
        self.collectionView.backgroundColor = [UIColor whiteColor];
        self.collectionView.showsHorizontalScrollIndicator = NO;
        
        [self.contentView addSubview:self.collectionView];
        
//        _nameLabel = [[UILabel alloc] init];
//        _nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
//        _nameLabel.backgroundColor = [UIColor clearColor];
//        _nameLabel.font = _messageNameFont;
//        _nameLabel.textColor = _messageNameColor;
//        _nameLabel.hidden = YES;
//        [self.contentView addSubview:_nameLabel];
//        
//        [self configureLayoutConstraintsWithModel:model];
//        
//        if ([UIDevice currentDevice].systemVersion.floatValue == 7.0) {
//            self.messageNameHeight = 15;
//        }
    }
    
    return self;
}

+(NSString*)CellIdetnfier
{
    return @"HomeExpereicneTableViewCell";
}

//- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    static NSString *CellID = @"HomeExperienceTableViewCell";
//    HomeExperienceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID];
//    if (!cell) {
//        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
//        cell.backgroundColor = [UIColor whiteColor];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        
//        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
//        flowLayout.itemSize = CGSizeMake(130*Scale, 165*Scale);
//        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//        
//        cell.collectionView = [[HomeExperienceCollectionView alloc] initWithFrame:CGRectMake(12, 40, ViewWidth-12, 175*Scale) collectionViewLayout:flowLayout];
//        cell.collectionView.scrollEnabled = YES;
//        cell.collectionView.backgroundColor = [UIColor whiteColor];
//        cell.collectionView.showsHorizontalScrollIndicator = NO;
//        
//        [cell.contentView addSubview:cell.collectionView];
//        
//    }
//    return cell;
//}

- (void)setDataList:(NSMutableArray *)dataList
{
    if (dataList.count == 0) {
        self.hidden = YES;
    }
    
    self.collectionView.dataList = dataList;
    [self.collectionView reloadData];
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat buttonWidth = 50*Scale;
    CGFloat buttonHeight = 17*Scale;
    self.plusButton.frame = CGRectMake(ViewWidth-buttonWidth-12*Scale, self.headerView.top, buttonWidth, buttonHeight);

}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
