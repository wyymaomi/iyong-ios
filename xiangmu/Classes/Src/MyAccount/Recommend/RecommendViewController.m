//
//  TuijianViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RecommendViewController.h"
#import "NetworkManager.h"
#import "MyUtil.h"
#import "HttpConstants.h"
#import "UserManager.h"
#import "TuijianDetailViewController.h"
#import "MJRefresh.h"

@interface RecommendViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *dataArray;
    
    
}
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UITableView *tuijiantableView;
@end

@implementation RecommendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"推荐人管理";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"添加" style:UIBarButtonItemStyleDone target:self action:@selector(gotoAdd)];
    
    dataArray=[NSMutableArray array];
    
    [self.tuijiantableView reloadData];
    
    [self setupHeaderView];
   // [self setupFooterView];
    [self.tuijiantableView.mj_header beginRefreshing];
    
}

-(void)tuijian
{
    [self.tuijiantableView reloadData];
    [self setupHeaderView];
    [self.tuijiantableView.mj_header beginRefreshing];
}

-(UITableView *)tuijiantableView
{
    if (_tuijiantableView==nil) {
        _tuijiantableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 10, ViewWidth, ViewHeight) style:UITableViewStylePlain];
        self.tuijiantableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
        self.tuijiantableView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
        self.tuijiantableView.delegate=self;
        self.tuijiantableView.dataSource=self;
        self.tuijiantableView.bounces=YES;
        self.tuijiantableView.scrollEnabled=YES;
        self.tuijiantableView.showsVerticalScrollIndicator=NO;
        //隐藏tableview的线
       // self.tuijiantableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        self.tuijiantableView.autoresizesSubviews=YES;
        self.tuijiantableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_tuijiantableView];
        
    }
    return _tuijiantableView;
}
-(void)setupHeaderView
{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefreshing)];
    self.tuijiantableView.mj_header = header;
}
-(void)setupFooterView
{
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    //当底部控件出现多少时就自动刷新(默认为1.0,也就是底部控件完全出现的时候,才会自动刷新)
    footer.triggerAutomaticallyRefreshPercent=1;
    footer.automaticallyHidden = YES;
    self.tuijiantableView.mj_footer = footer;
}
-(void)headerRefreshing
{
    self.nextAction = @selector(headerRefreshing);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Company_Inviter_List];
    NSString *parmas = [NSString stringWithFormat:@"time=%@",@""];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf.tuijiantableView.mj_header endRefreshing];
        NSInteger code_status = [responseData[@"code"] integerValue];
        NSLog(@"response success");
        
        NSLog(@"result = %@", responseData);
        
        NSLog(@"添加成功");
        if (code_status==STATUS_OK) {
             [self resetAction];
            
            dataArray  = [responseData valueForKey:@"data"];
            [strongSelf.tuijiantableView reloadData];
            self.tuijiantableView.mj_footer.hidden = self.tuijiantableView.frame.size.height < ViewHeight ? YES : NO;
        }
        
        NSLog(@"dataArray = %@",dataArray);
      
    } andFailure:^(NSString *errorDesc) {
        
        NSLog(@"response failure");
        [self.tuijiantableView.mj_header endRefreshing];
        
    }];

}
-(void)loadMoreData
{
    
}

-(void)gotoAdd
{
    TuijianDetailViewController *tuijiandetailVC=[[TuijianDetailViewController alloc]init];
    tuijiandetailVC.delegate = self;
    [self.navigationController pushViewController:tuijiandetailVC animated:YES];
}
//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([self.tuijiantableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tuijiantableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([self.tuijiantableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tuijiantableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}



#pragma mark---UICollectionView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
        UIImageView *leftImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-15, 18, 23/3, 42/3)];
        leftImageView.image = [UIImage imageNamed:@"icon_disclosure"];
        [cell.contentView addSubview:leftImageView];
        
        UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 20, 20)];
        imageView.image=[UIImage imageNamed:@"icon_contact_dial"];
        [cell.contentView addSubview:imageView];
        
        UILabel *titleLabel=[MyUtil createLabelFrame:CGRectMake(45, 15, 100, 20) title:nil font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f]];
        titleLabel.tag=200;
      [cell.contentView addSubview:titleLabel];
    }
    UILabel *mobileLabel=(UILabel *)[cell.contentView viewWithTag:200];
    mobileLabel.text=[dataArray[indexPath.row] valueForKey:@"mobile"];
    
    
    
    cell.backgroundColor=[UIColor whiteColor];
   // cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}
@end
