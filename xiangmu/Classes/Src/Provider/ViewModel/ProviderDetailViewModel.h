//
//  ProviderDetailViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProviderDetailModel.h"

@class ProviderDetailModel;


@interface ProviderDetailViewModel : NSObject

-(void)initCompanyInfoFrame;
-(void)initCompanyDescription;
//-(void)init

@property (nonatomic, strong) ProviderDetailModel *detailModel;

-(void)initCompanyInfoFrame:(ProviderDetailModel*)detailModel;

/**
 *  title frame
 */
@property (nonatomic, assign) CGRect companyNameFrame;
@property (nonatomic, assign) CGRect avatarImageFrame;

//browse/comment/thumbup
@property (nonatomic, assign) CGRect browseNumImageFrame;
@property (nonatomic, assign) CGRect browseNumTextFrame;
@property (nonatomic, assign) CGRect commentNumImageFrame;
@property (nonatomic, assign) CGRect commentNumTextFrame;
@property (nonatomic, assign) CGRect thumbUpNumImageFrame;
@property (nonatomic, assign) CGRect thumbUpNumTextFrame;

@property (nonatomic, assign) CGFloat companyInfoSectionHeight;

@property (nonatomic, strong) UIFont *browsesFont;
@property (nonatomic, strong) UIFont *commentsFont;
@property (nonatomic, strong) UIFont *likesFont;


/**
 *  title Frame
 */
@property (nonatomic ,assign) CGRect titleFrame;

/**
 * 公司简介 body frame
 */
@property (nonatomic, assign) CGRect bodyFrame;

@property (nonatomic, assign) CGFloat companyDescriptionSectionHeight;

//@property (nonatomic, strong) CGFloat company
//@property (nonatomic, strong) NSString *descriptionText;

//@property (nonatomic, strong) 

@end
