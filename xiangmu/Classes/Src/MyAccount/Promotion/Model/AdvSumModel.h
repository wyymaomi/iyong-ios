//
//  AdvSumModel.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/13.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol AdvSumModel <NSObject>

@end

@interface AdvSumModel : JSONModel

@property (nonatomic, strong) NSString<Optional> *date;
@property (nonatomic, strong) NSString<Optional> *day;
@property (nonatomic, strong) NSNumber<Optional> *count;

@end



@interface AdvSumResponseData : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *code;
@property (nonatomic, strong) NSMutableArray<Optional, AdvSumModel> *data;

@end
