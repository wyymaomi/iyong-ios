//
//  BillPayTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BillModel.h"


@protocol BillPayTableViewCellDelegate <NSObject>

-(void)onCheckButton:(NSUInteger)tag checked:(BOOL)checked;
-(void)onSelectRowAtIndex:(NSUInteger)rowIndex;

@end

@interface BillPayTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *upCompanyLabel;

@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *payDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *payMoney;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *startLocation;
@property (weak, nonatomic) IBOutlet UILabel *endLocation;
@property (weak, nonatomic) IBOutlet UILabel *customerLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;


@property (weak, nonatomic) id<BillPayTableViewCellDelegate> delegate;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+ (CGFloat)getCellHeight;

-(void)initData:(BillRepayModel*)data;

@end
