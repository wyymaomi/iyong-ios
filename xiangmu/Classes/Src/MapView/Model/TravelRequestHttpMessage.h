//
//  TravelRequestHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface TravelRequestHttpMessage : BaseHttpMessage

//startTime 开始时间(格式:yyyy-MM-dd HH:mm)
//startLocation 开始地点
//endLocation 结束地点
//carType 车辆类型(1=舒适型，2=商务型，3=豪华型)
//areaCode 地区编码
//mileage 总里程(公里，向上取整)
//amount 总金额(向上取整)

@property (nonatomic, strong) NSNumber *useCarMode; // 用车模式（1=实时用车,2=预约用车）
@property (nonatomic, strong) NSString *startTime;//开始时间(格式:yyyy-MM-dd HH:mm)
@property (nonatomic, strong) NSString *startLocation;//startLocation 开始地点
@property (nonatomic, strong) NSString *endLocation;//endLocation 结束地点
@property (nonatomic, strong) NSNumber *carType;//carType 车辆类型(1=舒适型，2=商务型，3=豪华型)
@property (nonatomic, strong) NSString *areaCode;//areaCode 地区编码
@property (nonatomic, strong) NSNumber *mileage;//mileage 总里程(公里，向上取整)
@property (nonatomic, strong) NSNumber *amount;//amount 总金额(向上取整)
@property (nonatomic, strong) NSString *startCoordinate;//上车地点坐标（格式:x,y：经纬度坐标）
@property (nonatomic, strong) NSString *endCoordinate;//下车地点坐标
@property (nonatomic, strong) NSString *passengerMobile;

@end
