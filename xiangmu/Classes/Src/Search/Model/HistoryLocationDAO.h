//
//  HistoryLocationDAO.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DAO.h"
#import "SearchModel.h"

@interface HistoryLocationDAO : DAO

-(void)getLatestTwentyKeywordsWithCompletionBlock:(void (^)(NSArray *))completionBlock;
-(BOOL)insertHistoyrLocation:(LocationSearchModel*)location time:(NSNumber*)time;

@end
