//
//  StartOrderViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "VechileOrderHttpMessage.h"

typedef NS_ENUM(NSUInteger, ReceiveOrderStatus)
{
    ReceiveOrderStatusWaiting,
    ReceiveOrderStatusComplete
};

@interface StartOrderViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *rotationBgImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rotationImageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIView *cancelOrderView;
@property (weak, nonatomic) IBOutlet UIButton *cancelOrderButton;
@property (weak, nonatomic) IBOutlet UIView *goPayView;
@property (weak, nonatomic) IBOutlet UIButton *goPayButton;
@property (weak, nonatomic) IBOutlet UIView *waitingReceiveOrderView;
@property (weak, nonatomic) IBOutlet UIView *completeReceiveOrderView;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *carModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *carNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic, strong) VechileOrderHttpMessage *vechileOrderHttpMessage;

//@property (nonatomic, strong) NSString *receiveCutoffTime; // 接单截止时间

@property (nonatomic, strong) NSTimer *driverReceiveTime;

@property (nonatomic, assign) ReceiveOrderStatus receiveOrderStatus;

-(void)startBooking;

-(void)orderReceived:(NSDictionary*)infoDic;


@end
