//
//  CheckUpdateService.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckUpdateService : NSObject

//@property (nonatomic, assign) BOOL isReviewing;
@property (nonatomic, assign) BOOL isForceUpgrade; // 是否强制升级标志
@property (nonatomic, strong) NSString *updateUrl; // 下载地址
@property (nonatomic, strong) NSString *updateInfo; // 升级文字内容
//@property (nonatomic, retain) NSMutableArray *thirdPayModels;

-(void)checkVersion;
- (void)updateAppVersion:(NSDictionary *)result;
- (void)popupUpgradeAlert;

@end
