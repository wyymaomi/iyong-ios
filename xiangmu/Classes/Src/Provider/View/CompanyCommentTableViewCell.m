//
//  CompanyCommentTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CompanyCommentTableViewCell.h"

@implementation CompanyCommentTableViewCell

+ (instancetype)momentsTableViewCellWithTableView:(UITableView *)tableView;
{
    static NSString *reuseID = @"CompanyCommentTableViewCell";
    CompanyCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[CompanyCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseID];
        
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
//        self.backgroundColor = UIColorFromRGB(0xDEDEDE);
        //        self.contentView.backgroundColor = UIColorFromRGB(0xEDEDED);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.bodyView];
        //        [self addSubview:self.toolView];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (_viewModel) {
        self.bodyView.frame = _viewModel.momentsBodyFrame;
        //        self.toolView.frame = _viewModel.momentsToolBarFrame;
    }
}


- (void)setViewModel:(ProviderCommentViewModel *)viewModel
{
    _viewModel = viewModel;
    
    // 设置子控件的frame
    self.bodyView.frame = _viewModel.momentsBodyFrame;
    self.bodyView.viewModel = viewModel;
    //    self.toolView.frame = _viewModel.momentsToolBarFrame;
    //    self.toolView.viewModel = viewModel;
    
}


- (CompanyCommentBodyView*)bodyView
{
    if (_bodyView == nil) {
        _bodyView = [CompanyCommentBodyView new];
        //        _bodyView.cell = self;
        
    }
    
    return _bodyView;
}

@end
