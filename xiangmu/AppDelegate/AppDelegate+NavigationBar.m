//
//  AppDelegate+NavigationBar.m
//  xiangmu
//
//  Created by 湛思科技 on 16/12/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AppDelegate+NavigationBar.h"

@implementation AppDelegate (NavigationBar)

#pragma mark - 设置导航栏
-(void)setNavigationBarAppearance
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [[UITextField appearance] setTintColor:NAVBAR_BGCOLOR];
    [[UITextView appearance] setTintColor:NAVBAR_BGCOLOR];
    
    // 更改全局UINavigationBar的标题颜色
    if (isIOS10) {
        [[UINavigationBar appearance] setTintColor:NAVBAR_TITLE_TEXT_COLOR];
        [[UINavigationBar appearance] setBarTintColor:NAVBAR_TITLE_TEXT_COLOR];
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:BOLD_FONT(18),
                                                               NSForegroundColorAttributeName:NAVBAR_TITLE_TEXT_COLOR}];
    }
    else {
        // 更改全局UINavigationBar的背景色
        if (IOS7_OR_LATER) {
            [[UINavigationBar appearance] setBarTintColor:NAVBAR_TITLE_TEXT_COLOR];
            [[UINavigationBar appearance] setTintColor:NAVBAR_TITLE_TEXT_COLOR];
        }
        else if (IOS7_BEFORE) {
            [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
        }
        if(IOS8_OR_LATER && [UINavigationBar conformsToProtocol:@protocol(UIAppearanceContainer)]) {
            [[UINavigationBar appearance] setTranslucent:NO];
        }
        if (IOS7_OR_LATER) {
            [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:BOLD_FONT(18),
                                                                   NSForegroundColorAttributeName:NAVBAR_TITLE_TEXT_COLOR}];
        }
    }
    
//     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    
}

@end
