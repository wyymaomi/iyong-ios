//
//  OrderPayViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "OrderPayViewController.h"
#import "VechileOrderManager.h"

@interface OrderPayViewController ()

@property (nonatomic, strong) VechileOrderHttpMessage *vechileOrderHttpMessage;

@end

@implementation OrderPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"订单支付";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initData];
    
}

-(void)initData
{
    
    _vechileOrderHttpMessage = [VechileOrderManager sharedInstance].vechileOrderHttpMessage;
    
    NSArray *carLargeImages = @[@"img_vechile_comfortable",
                                @"img_vechile_commercial",
                                @"img_vechile_luxure",
                                @"img_vechile_economical"];
    self.vechileTypeImageView.image = [UIImage imageNamed:carLargeImages[_vechileOrderHttpMessage.carType-1]];
    
    self.startDateTimeLabel.text = _vechileOrderHttpMessage.carDateTime;
    
    // 司机相关信息
    self.vechileNumberLabel.text = _vechileOrderHttpMessage.carNumber;
    self.vechileTypeLabel.text = _vechileOrderHttpMessage.carModel;
    self.driverMobileLabel.text = _vechileOrderHttpMessage.driverMobile;
    self.driverNameLabel.text = _vechileOrderHttpMessage.driverName;

    
    self.startAddressLabel.text = _vechileOrderHttpMessage.startLocation;
    
    self.endAddressLabel.text = _vechileOrderHttpMessage.endLocation;
    
    self.unitPriceLabel.text = [NSString stringWithFormat:@"起步公里数：5km    每公里%ld元", (long)_vechileOrderHttpMessage.carUnitPrice];
    
    self.resultLabel.text = [NSString stringWithFormat:@"%.fkm    *    %ld元/km    =    %.f元", _vechileOrderHttpMessage.distance, (long)_vechileOrderHttpMessage.carUnitPrice, _vechileOrderHttpMessage.carPrice];
    
    NSString *priceText = [NSString stringWithFormat:@"%.f元", _vechileOrderHttpMessage.carPrice];
    NSString *distanceText = [NSString stringWithFormat:@"(%.f公里)", _vechileOrderHttpMessage.distance];
    self.priceLabel.text = [NSString stringWithFormat:@"%@ %@", priceText, distanceText];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.priceLabel.text];
    [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xe06430),
                                      NSFontAttributeName:FONT(12)}
                              range:[priceText rangeOfString:@"元"]];
    [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0x999999),
                                      NSFontAttributeName:FONT(16)}
                              range:[self.priceLabel.text rangeOfString:distanceText]];
    self.priceLabel.attributedText = attributedString;
    
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.vechileTypeImageView.frame = CGRectMake((ViewWidth-290*Scale)/2, 47, 290*Scale, 218*Scale);
    
    self.driverInfoView.top = self.vechileTypeImageView.bottom + 17*Scale;
    
    self.travelInfoView.top = self.driverInfoView.bottom + 10 * Scale;
    
    self.scrollView.contentSize = CGSizeMake(ViewWidth, self.travelInfoView.bottom);
    
    if (ViewWidth <= 320) {
 
        self.startDateTimeLabel.font = FONT(17*Scale);
        self.vechileNumberLabel.font = FONT(17*Scale);
        self.vechileNumberLabel.frame = CGRectMake(0, 0, ViewWidth, 17*Scale);
        self.vechileTypeLabel.font = FONT(17*Scale);
        self.vechileTypeLabel.frame = CGRectMake(0, self.vechileNumberLabel.bottom+8, ViewWidth, 17*Scale);
        self.driverNameLabel.font = FONT(17*Scale);
        self.driverNameLabel.frame = CGRectMake(0, self.vechileTypeLabel.bottom+8, ViewWidth, 17*Scale);
        self.driverMobileLabel.font = FONT(17*Scale);
        self.driverMobileLabel.frame = CGRectMake(130, self.driverNameLabel.top, ViewWidth-130, 17*Scale);
        self.driverInfoView.height = CGRectGetMaxY(self.driverNameLabel.frame);
        
        self.startAddressIconView.frame = CGRectMake(60*Scale, 7*Scale, 18*Scale, 26*Scale);
        self.startAddressLabel.frame = CGRectMake(95*Scale, 0, ViewWidth-115*Scale, 40);
        self.endAddressIconView.frame = CGRectMake(60*Scale, 48, 18*Scale, 26*Scale);
        self.endAddressLabel.frame = CGRectMake(95*Scale, self.startAddressLabel.bottom, ViewWidth-115*Scale, 40);
        self.unitPriceLabel.frame = CGRectMake(0, self.endAddressLabel.bottom+12*Scale, ViewWidth, 17*Scale);
        self.unitPriceLabel.font = FONT(15*Scale);
        self.travelInfoLineView.frame = CGRectMake(0, self.unitPriceLabel.bottom+7*Scale, ViewWidth, 0.5*Scale);
        self.resultLabel.font = FONT(16*Scale);
        self.resultLabel.frame = CGRectMake(0, self.travelInfoLineView.bottom, ViewWidth, 50*Scale);
        
        
        self.startDateView.height = 40*Scale;
        self.vechileTypeImageView.top = self.startDateView.bottom;
        self.travelInfoView.top = self.driverInfoView.bottom +10*Scale;
        self.scrollView.contentSize = CGSizeMake(ViewWidth, self.travelInfoView.bottom);
  
    }
    
//    CGSize priceSize = [self.priceLabel.text textSize:CGSizeMake(ViewWidth, 50*Scale) font:self.priceLabel.font];
//    self.priceLabel.width = priceSize.width;
    
//    CGSize distanceSize = [self.distanceLabel.text textSize:CGSizeMake(ViewWidth, 50*Scale) font:self.distanceLabel.font];
//    self.distanceLabel.frame = CGRectMake(self.bottomPriceLabel.right+10*Scale, 19*Scale, distanceSize.width, 16*Scale);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onClickOKButton:(id)sender {
    
    if ([[VechileOrderManager sharedInstance] isPendingPayOvertime]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"支付超时，请重新下单" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        WeakSelf
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
//                [weakSelf pressedBackButton];
            }
        }];
        
        return;
    }
    
    [self gotoPage:@"VechileOrderPayViewController"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
