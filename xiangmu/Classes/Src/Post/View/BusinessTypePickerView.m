//
//  BusinessTypePickerView.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BusinessTypePickerView.h"

@implementation BusinessTypePickerView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.closeButton addTarget:self action:@selector(onClickCloseButton:) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.commercialImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickBusinessType:)]];
//    [self.travelImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickBusinessType:)]];
    
//    [self.
//    ]
    
//    self.commercialImageView
    
//    self.commercialImageView.tag =
    
//    self.commercialImageView.tag = ProviderTypeCommercial;
//    self.travelImageView.tag = ProviderTypeTravel;
//    self.airportImageView.tag = ProviderTypeAirport;
//    self.weddingImageView.tag = ProviderTypeWedding;
//    self.charteredImageView.tag = ProviderTypeCharter;
//    self.truckImageView.tag = ProviderTypeTruck;
//    self.busImageView.tag = ProviderTypeBus;
//    self.goodsImageView.tag = ProviderTypeGoods;
    
//    [self. addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    
//    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)];
    [self.commercialImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.travelImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.airportImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.weddingImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.charteredImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.truckImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.busImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    [self.goodsImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickImageView:)]];
    
    _businessTypeArray = [NSMutableArray array];
    
}

- (void)onClickCloseButton:(id)sender
{
    self.hidden = YES;
}

- (void)onClickImageView:(id)sender
{
    UITapGestureRecognizer *tap = (UITapGestureRecognizer*)sender;
    
    UIImageView *imageView = (UIImageView*) tap.view;
    
    DLog(@"tag = %d", imageView.tag);
    
//    NSLog(@"image name is %@", [imageView.image accessibilityIdentifier]);
    
    if (!imageView.isHighlighted) {
        [imageView setHighlighted:YES];
    }
    else {
        [imageView setHighlighted:NO];
    }
    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(onGetBusinessType:isHighLighted:)]) {
//        [self.delegate onGetBusinessType:imageView.tag isHighLighted:imageView.isHighlighted];
//    }
    
    NSString *tagStr = [NSString stringWithFormat:@"%d", imageView.tag];
    BOOL isHighlighted = imageView.isHighlighted;
    if (isHighlighted) {
        if (![self.businessTypeArray containsObject:tagStr]) {
            [self.businessTypeArray addObject:tagStr];
        }
    }
    else {
        if ([self.businessTypeArray containsObject:tagStr]) {
            [self.businessTypeArray removeObject:tagStr];
        }
    }
    
    DLog(@"businessTypeArray = %@", self.businessTypeArray);
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
