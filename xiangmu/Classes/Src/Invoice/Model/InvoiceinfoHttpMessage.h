//
//  InvoiceinfoHttpMessage.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

//header 发票抬头
//taxpayerNumber  纳税人识别号
//registerAddress 注册地址
//openingBank 开户银行
//bankAccount 银行帐号
//contact 联系人
//tel 联系电话
//areaCode 地区编码
//areaName 所在区域
//address 详细地址
//remark 客户备注

@interface InvoiceSettingHttpMessage : BaseHttpMessage

//@property (nonatomic, strong) NSNumber *totalAmount;
//@property (nonatomic, strong) NSNumber *totalQuantity;
//@property (nonatomic, strong) NSNumber *type;
//@property (nonatomic, strong) NSString *content;

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *header;
@property (nonatomic, strong) NSString *taxpayerNumber;
@property (nonatomic, strong) NSString *registerAddress;
@property (nonatomic, strong) NSString *openingBank;
@property (nonatomic, strong) NSString *bankAccount;
@property (nonatomic, strong) NSString *contact;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *areaCode;
@property (nonatomic, strong) NSString *areaName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *remark;

@end

//totalAmount *发票总金额
//totalQuantity *发票总数量
//header *发票抬头
//type *发票类型(1=纸质发票)
//content *发票内容
//taxpayerNumber 纳税人识别号
//registerAddress 注册地址
//openingBank 开户银行
//bankAccount 银行帐号
//contact *联系人
//tel *联系电话
//areaCode *地区编码
//areaName *地区名称（XX省XX市XX县(区)）
//address *街道详细地址(XX路XX号)
//remark 备注

@interface InvoiceinfoHttpMessage : BaseHttpMessage

@property (nonatomic, strong) NSNumber *createTime;
@property (nonatomic, strong) NSNumber *totalAmount;
@property (nonatomic, strong) NSNumber *totalQuantity;
@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSString *content;
//@property (nonatomic, strong) InvoiceSettingHttpMessage *settingHttpMessage;
@property (nonatomic, strong) NSString *header;
@property (nonatomic, strong) NSString *taxpayerNumber;
@property (nonatomic, strong) NSString *registerAddress;
@property (nonatomic, strong) NSString *openingBank;
@property (nonatomic, strong) NSString *bankAccount;
@property (nonatomic, strong) NSString *contact;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *areaCode;
@property (nonatomic, strong) NSString *areaName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *remark;



@end
