//
//  YYImageDownloader.h
//  xiangmu
//
//  Created by 湛思科技 on 17/1/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+ImageContentType.h"

//typedef void (^YYImageDonwloadCompleteBlock)(UIImage *image, NSError *error);
typedef void(^YYImageDonwloadCompleteBlock)(NSData *imageData, NSError *error);

@interface YYImageDownloader : NSObject

- (void)downloadImageWithAvatarUrl:(NSString*)imageUrl
                       isThumbnail:(BOOL)isThumbnail
                         completed:(YYImageDonwloadCompleteBlock)completeBlock;

- (void)downloadImageWithImageUrl:(NSString*)imageUrl
                      isThumbnail:(BOOL)isThumbnail
                        completed:(YYImageDonwloadCompleteBlock)completeBlock;

+(YYImageDownloader*)sharedDownloader;

@property (nonatomic, strong) NSMutableDictionary *runningOperations;

@property (nonatomic, strong) NSMutableArray *downloadImageUrls;

/**所有图像的的缓存*/
@property(nonatomic,strong)NSMutableDictionary *imageCache;

@end
