//
//  CommunityTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityTableViewCell.h"
#import "CommunityPublishModel.h"
#import "DateUtil.h"

@implementation CommunityTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    
    static NSString *ID =  @"CommunityTableViewCell";
    CommunityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

-(void)initData:(CommunityPublishModel*)communityPublishModel
{
    self.publishLabel.text = [NSString stringWithFormat:@"%@-%@", communityPublishModel.nickname, communityPublishModel.companyName];
    self.startTimeLabel.text = [DateUtil formatDateTime:communityPublishModel.startTime dateFomatter:@"yyyy-MM-dd HH:mm"];
    self.endTimeLabel.text = [DateUtil formatDateTime:communityPublishModel.endTime dateFomatter:@"yyyy-MM-dd HH:mm"];
    self.locationLabel.text = communityPublishModel.seat;
    self.elapseTimeLabel.text = [DateUtil timerAgo:communityPublishModel.createTime];
    self.driverNameLabel.text = communityPublishModel.carDriver;
    self.vechileNumberLabel.text = communityPublishModel.carNumber;
    self.driverMobileLabel.text = communityPublishModel.carMobile;
    
//    [self.vechileImageView downloadImage:communityPublishModel.carImgUrl placeholderImage:[UIImage imageNamed:@"img_vechile_default"] success:^(id responseData) {
////        cell.avartarImageView.cornerRadius = cell.avartarImageView.frame.size.width/2;
//    } andFailure:^(NSString *errorDesc) {
////        cell.avartarImageView.cornerRadius = 5;
//        
//    }];
    
//    WeakSelf
//    [self.avatarImageView downloadImage:communityPublishModel.logoUrl placeholderImage:[UIImage imageNamed:@"icon_avataro"] success:^(id responseData) {
//        weakSelf.avatarImageView.cornerRadius = weakSelf.avatarImageView.frame.size.width/2;
//    } andFailure:^(NSString *errorDesc) {
//        weakSelf.avatarImageView.cornerRadius =5;
//    }];

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
