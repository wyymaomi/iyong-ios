//
//  PayModelAdapterProtocol.h
//  xiangmu
//
//  Created by 湛思科技 on 16/12/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

static const NSString *kRechargeAmount = @"kRechargeAmount";
static const NSString *kTradeNo = @"kTradeNo";
static const NSString *kPayTime = @"kPayTime";
static const NSString *kBankcardNo = @"kBankcardNo";

@protocol PayModelAdapterProtocol <NSObject>

@required
- (NSString*)generateOrderString:(NSDictionary*)dict;

//- (void)getBillNo:(NSDictionary*)dict;

@optional
- (NSString*)getAppScheme;

- (NSString*)getAppId;

- (NSString*)getAppPrivateKey;


@end
