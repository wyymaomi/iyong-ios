//
//  DrvierPathViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "TrackHistoryModel.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>

@class SportAnnotationView;


@interface DrvierPathViewController : BaseViewController
{
    BMKMapView *_mapView;
    
    BMKPolyline *_pathPolyLine;
    
    BMKPolygon *_pathPloygon;
    BMKPointAnnotation *_sportAnnotation;
    SportAnnotationView *_sportAnnotationView;
    
    
    NSInteger _sportNodeNum;//轨迹点数
    NSInteger _currentIndex;//当前结点
    
    BMKPointAnnotation * pointAnnotation;
}

//@property (strong, nonatomic) BMKMapView *mapView;

//@property (nonatomic, strong) NSMutableArray *sportDataList;

@property (nonatomic, strong) NSTimer *timer;


//@property (nonatomic, strong) NSDictionary *trackHistoryDict;

@property (nonatomic, strong) TrackHistoryModel *trackHistoryModel;

@property (nonatomic, strong) NSMutableArray *sportNodes;

@property (nonatomic, strong) NSMutableArray *trackHistoryList;


@end
