//
//  FirstCollectionViewCell.m
//  xiangmu
//
//  Created by David kim on 16/3/25.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "FirstCollectionViewCell.h"

@implementation FirstCollectionViewCell

- (UIButton *)badgeValueBtn
{
    if (!_badgeValueBtn)
    {
        _badgeValueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_badgeValueBtn setBackgroundImage:[UIImage imageNamed:@"icon_badge"] forState:UIControlStateNormal];
        [_badgeValueBtn setBackgroundImage:[UIImage imageNamed:@"icon_msg_circle"] forState:UIControlStateNormal];
//        _badgeValueBtn.backgroundColor = [UIColor redColor];
        [_badgeValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _badgeValueBtn.alpha = 0;
        _badgeValueBtn.titleLabel.font = FONT(14);//[UIFont boldSystemFontOfSize:12];
        CGFloat btnWidth = 22;//30;
        CGFloat btnHeight = 22;//25;
        _badgeValueBtn.frame = CGRectMake(self.frame.size.width - btnWidth - 5, 5, btnWidth, btnHeight);
        _badgeValueBtn.left = self.topImageView.right;
        [_badgeValueBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        _badgeValueBtn.hidden = YES;
        _badgeValueBtn.alpha = 0;
        //        _badgeValueBtn.layer.zPosition = 1;
        [_badgeValueBtn setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [_badgeValueBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
//        _badgeValueBtn.cornerRadius = _badgeValueBtn.width/2;
    }
    return _badgeValueBtn;
}

-(void)hideBadge
{
    self.badgeValueBtn.hidden = YES;
    self.badgeValueBtn.alpha = 0;
}

-(void)showBadgeValue:(NSInteger)numberBadge
{
    self.badgeValueBtn.hidden = NO;
    self.badgeValueBtn.alpha = 1;
    
    if (numberBadge == 0)
    {
        self.badgeValueBtn.alpha = 0;
        self.badgeValueBtn.hidden = YES;
        return;
    }
    
    self.badgeValueBtn.hidden = NO;
    
    NSString *number = [NSString stringWithFormat:@"%ld", (long)numberBadge];
    
    NSUInteger length = [number length];
    
    CGSize size = [number textSize:CGSizeMake(64, 20) font:FONT(14)];
    
    CGRect rect = self.badgeValueBtn.frame;
//    rect.size.width = size.width * (length + 0.5)>22?size.width*(length+0.5):22;
//    rect.size.height = rect.size.width;
    
    self.badgeValueBtn.frame = rect;
    [self.badgeValueBtn setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [self.badgeValueBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    self.badgeValueBtn.alpha = 1.0;
    [self.badgeValueBtn setTitle:number forState:UIControlStateNormal];
    [self.badgeValueBtn setTitle:number forState:UIControlStateDisabled];
    //    }
}

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        //self.frame = CGRectMake(0, 0, (ViewWidth-40)/3, 90);
        
        self.topImageView=[[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-45)/2, 25, 50/1.3, 40/1.3)];
        [self addSubview:self.topImageView];
        
        self.detailLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 60, self.frame.size.width, 15)];
        self.detailLabel.textColor=[UIColor blackColor];
        self.detailLabel.font=[UIFont systemFontOfSize:14];
        self.detailLabel.textAlignment = UITextAlignmentCenter;
        [self addSubview:self.detailLabel];
        
        [self addSubview:self.badgeValueBtn];
        
    }
    return self;
}


@end
