//
//  CarTableViewCell.m
//  xiangmu
//
//  Created by David kim on 16/4/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CarTableViewCell.h"
#import "MyUtil.h"
#define ViewWidth [[UIScreen mainScreen]bounds].size.width

@implementation CarTableViewCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];

    if (self)
    {
        self.frame = CGRectMake(0, 0, ViewWidth, 100);
        UIImageView *imgV3=[[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 120, 90)];
        self.imageView.layer.masksToBounds=YES;
        //设置圆角
        self.imageView.layer.cornerRadius=20;
        [self addSubview:imgV3];
        
        UIImageView *imgV2 = [[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth/2, 12, 15, 15)];
        imgV2.image = [UIImage imageNamed:@"icon_vechile_number"];
        [self addSubview:imgV2];
        
        UIImageView *imgV4 = [MyUtil createImageViewFrame:CGRectMake(ViewWidth/2, 42, 15, 15) image:@"icon_driver"];
        [self addSubview:imgV4];
        
        UIImageView *imgV5 = [MyUtil createImageViewFrame:CGRectMake(ViewWidth/2, 72, 15, 15) image:@"电话"];
        [self addSubview:imgV5];
        
        UILabel *lineLabel=[[UILabel alloc]initWithFrame:CGRectMake(ViewWidth/2-20, 10, 1, 100-20)];
        lineLabel.backgroundColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        [self addSubview:lineLabel];
        
        self.carNumLbl = [MyUtil createLabelFrame:CGRectMake(ViewWidth/2+35, 10, ViewWidth/2-40, 20) title:@" " font:[UIFont systemFontOfSize:14] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor blackColor]];
//        self.carNumLbl.layer.borderColor = [[UIColor colorWithRed:148.0f/255.0f green:26.0f/255.0f blue:250.0/255.0f alpha:1.0f] CGColor];
        self.carNumLbl.textAlignment=NSTextAlignmentLeft;
        self.carNumLbl.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
//        self.carNumLbl.layer.borderWidth = 1;
//        self.carNumLbl.layer.cornerRadius = 1;
        //self.carNumLbl.font=[UIFont boldSystemFontOfSize:9];
        [self addSubview:self.carNumLbl];
        
        
        
        self.driverLbl = [MyUtil createLabelFrame:CGRectMake(ViewWidth/2+35, 40, ViewWidth/2-40, 20) title:@" " font:[UIFont systemFontOfSize:14] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor blackColor]];
//        self.driverLbl.layer.borderColor = [[UIColor colorWithRed:148.0f/255.0f green:26.0f/255.0f blue:250.0/255.0f alpha:1.0f] CGColor];
        self.driverLbl.textAlignment=NSTextAlignmentLeft;
        self.driverLbl.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
//        self.driverLbl.layer.borderWidth = 1;
//        self.driverLbl.layer.cornerRadius = 1;
        //self.driverLbl.font=[UIFont boldSystemFontOfSize:9];
        [self addSubview:self.driverLbl];
        
        
        self.telLbl = [MyUtil createLabelFrame:CGRectMake(ViewWidth/2+35, 70, ViewWidth/2-40, 20) title:@" " font:[UIFont systemFontOfSize:14] textAlignment:NSTextAlignmentLeft numberOfLines:1 textColor:[UIColor blackColor]];
//        self.telLbl.layer.borderColor = [[UIColor colorWithRed:148.0f/255.0f green:26.0f/255.0f blue:250.0f/255.0f alpha:1.0f] CGColor];
        self.telLbl.textAlignment=NSTextAlignmentLeft;
        self.telLbl.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
//        self.telLbl.layer.borderWidth = 1;
//        self.telLbl.layer.cornerRadius = 1;
        //self.telLbl.font=[UIFont boldSystemFontOfSize:9];
        [self addSubview:self.telLbl];
        

    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
