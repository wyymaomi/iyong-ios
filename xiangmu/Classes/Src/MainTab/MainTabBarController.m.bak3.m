//
//  MainTabBarController.m
//  xiangmu
//
//  Created by David kim on 16/3/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "MainTabBarController.h"
#import "BaseViewController.h"
#import "UserManager.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "GestureLoginViewController.h"
#import "CommunityListViewController.h"
#import "CustomTabBar.h"


@interface MainTabBarController ()<CustomTabBarDelegate, UITabBarControllerDelegate, LoginViewControllerDelegate, GesturePasswordControllerDelegate>

@property (nonatomic, retain) UINavigationController *orderNavigation;
@property (nonatomic, retain) UINavigationController *billNavigation;
@property (nonatomic, retain) UINavigationController *meNavigation;
@property (nonatomic, retain) UINavigationController *communityNavigation;

@property (nonatomic, assign) NSInteger tabIndex;

@end

@implementation MainTabBarController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createControllers];
    self.delegate = self;
    // Do any additional setup after loading the view.
    
    CustomTabBar *tabbar = [[CustomTabBar alloc] init];
    tabbar.delegate = self;
    [self setValue:tabbar forKeyPath:@"tabBar"];
    
    [self initTabBar];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //当应用进入后台时间超过10分钟，重新进入弹出登录界面
    //    AppDelegate *appDelegate = APP_DELEGATE;
    //    [appDelegate handleCurrentTime];
}



-(void)initTabBar
{
    UITabBar *tabBar = [self tabBar];
    self.selectedIndex = 0;
    
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, tabBar.frame.size.height)];
    bgView.backgroundColor = NAVBAR_BGCOLOR;
    [tabBar insertSubview:bgView atIndex:0];
    
    //设置tabbar的颜色
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica" size:12.0f],NSFontAttributeName,nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor yellowColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica" size:12.0f],NSFontAttributeName,nil] forState:UIControlStateSelected];
    
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    // 设置社区按钮和文字为默认状态
    CustomTabBar *tabBar = (CustomTabBar*)[self tabBar];
    tabBar.titleLabel.textColor = [UIColor whiteColor];
    tabBar.centerButton.selected = NO;
    
    if (viewController == _orderNavigation || viewController == _billNavigation || viewController == _meNavigation /*|| viewController == _communityNavigation*/)
    {
        if (viewController == _orderNavigation) {
            _tabIndex = 1;
        }
        if (viewController == _billNavigation) {
            _tabIndex = 2;
            //            _tabIndex = 3;
        }
        if (viewController == _meNavigation) {
            _tabIndex = 3;
            //            _tabIndex = 4;
        }
        //        if (viewController == _communityNavigation) {
        //            _tabIndex = 2;
        //            tabBar.titleLabel.textColor = [UIColor yellowColor];
        //            tabBar.centerButton.selected = YES;
        //        }
        
        //当应用进入后台时间超过10分钟，重新进入弹出登录界面
        AppDelegate *appDelegate = APP_DELEGATE;
        appDelegate.currentTime = CFDateGetAbsoluteTime((CFDateRef)[NSDate date]);
        if (appDelegate.resignTime != 0 && appDelegate.currentTime - appDelegate.resignTime > LOGIN_OVER_TIME)
        {
            [[UserManager sharedInstance] doLogout];
        }
        
        if (![[UserManager sharedInstance] isLogin])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请先登录" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    if ([[UserManager sharedInstance] isGesturePwdLogin]) {
                        GestureLoginViewController *gesturePwdController = [[GestureLoginViewController alloc] init];
                        gesturePwdController.enter_type = ENTER_TYPE_PRESENT;
//                        gesturePwdController.loginDelegate = self;
                        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:gesturePwdController];
                        [self presentViewController:nav animated:YES completion:nil];
                    }
                    else {
                        LoginViewController *loginViewController = [[LoginViewController alloc] init];
                        loginViewController.enter_type = ENTER_TYPE_PRESENT;
                        loginViewController.loginDelegate = self;
                        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginViewController];
                        [self presentViewController:nav animated:YES completion:nil];
                    }
                }
            }];
            
            return NO;
        }
    }
    return YES;
}

-(void)onGestureLoginSuccess;
{
    self.selectedIndex = _tabIndex;
}

-(void)onLoginSuccess
{
    self.selectedIndex = _tabIndex;
}

-(void)createControllers
{
//    NSArray *ctrlArray=@[@"HomepageViewController",@"OrderStatusListViewController",/*@"CommunityListViewController",*/@"BillManageViewController",@"MyViewController"];
    ////////////////////
//    NSArray *nameArray=@[@"首页",@"订单",/*@"",*/@"账单",@"我"];
//    NSArray *imageArray=@[@"icon_tab_home",@"icon_tab_order",/*@"",*/ @"icon_tab_bill",@"icon_tab_me"];
//    NSArray *selectedArray=@[@"icon_tab_home_sel",@"icon_tab_order_sel",/*@"",*/@"icon_tab_bill_sel",@"icon_tab_me_sel"];
    
    NSArray *ctrlArray=@[@"ServiceProviderRootViewController",
                         @"InquireViewController",
                         /*@"",*/
                         /*@"CommunityListViewController",@"CommunityCategoryViewController",*/
                         /*@"BillManageViewController",*/@"BusinessCircleListViewController",
                         @"MyViewController"];
    ////////////////////
    NSArray *nameArray=@[@"首页",@"询价",/*@"发布",*/@"报价",@"我"];
    NSArray *imageArray=@[@"icon_tab_home",@"icon_tab_enquiry",@"",/*@"icon_tab_plus",*/ @"icon_tab_offer",@"icon_tab_me"];
    NSArray *selectedArray=@[@"icon_tab_home_focused",@"icon_tab_enquiry_focused",@"",/*@"icon_tab_plus",*/@"icon_tab_offer_focused",@"icon_tab_me_focused"];
    
    NSMutableArray *navArray=[[NSMutableArray alloc]init];
    for (int i=0; i<ctrlArray.count; i++) {
        NSString *className=ctrlArray[i];
        Class cls=NSClassFromString(className);
        BaseViewController *vc=[[cls alloc]init];
        vc.source_from = SOURCE_FROM_TAB_CONTROL;
        NSString *imageName=imageArray[i];
        NSString *selectedimageName=selectedArray[i];
        vc.hasLeftBackButton = NO;
        vc.tabBarItem.image=[[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage=[[UIImage imageNamed:selectedimageName]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        NSString *title=nameArray[i];
        vc.tabBarController.tabBar.barTintColor = NAVBAR_BGCOLOR;
        
        vc.tabBarItem.title=title;
        [vc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(2, -5)];
        
        
        UINavigationController *navCtrl=[[UINavigationController alloc] initWithRootViewController:vc];
        if (i == 1) {
            _orderNavigation = navCtrl;
        }
        //        if (i == 2) {
        //            _communityNavigation = navCtrl;
        //        }
        if (i == 2) {
            _billNavigation = navCtrl;
        }
        if (i == 3) {
            _meNavigation = navCtrl;
        }
        
        [self addChildViewController:navCtrl];
    }
    //    self.viewControllers = navArray;
}


#pragma mark - CustomTabBarDelegate
-(void)tabBarCenterButtonClick:(CustomTabBar*)tabBar;
{
    
    if ([self canLogin]) {
        
        CommunityListViewController *viewController = [[CommunityListViewController alloc] init];
        viewController.enter_type = ENTER_TYPE_PRESENT;
        UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:viewController];
        [self presentViewController:navVc animated:YES completion:nil];
        
    }
    
    
    //    self.selectedIndex = 2;
    //    [self tabBarController:self shouldSelectViewController:_communityNavigation];
    
    //    [self presentViewController:_communityNavigation animated:YES completion:nil];
    
    //    LBpostViewController *plusVC = [[LBpostViewController alloc] init];
    //    plusVC.view.backgroundColor = [self randomColor];
    //
    //    LBNavigationController *navVc = [[LBNavigationController alloc] initWithRootViewController:plusVC];
    //
    //    [self presentViewController:navVc animated:YES completion:nil];
    
    
    
}

-(BOOL)canLogin
{
    //当应用进入后台时间超过10分钟，重新进入弹出登录界面
    AppDelegate *appDelegate = APP_DELEGATE;
    appDelegate.currentTime = CFDateGetAbsoluteTime((CFDateRef)[NSDate date]);
    if (appDelegate.resignTime != 0 && appDelegate.currentTime - appDelegate.resignTime > LOGIN_OVER_TIME)
    {
        [[UserManager sharedInstance] doLogout];
    }
    
    if (![[UserManager sharedInstance] isLogin])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请先登录" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                if ([[UserManager sharedInstance] isGesturePwdLogin]) {
                    GestureLoginViewController *gesturePwdController = [[GestureLoginViewController alloc] init];
                    gesturePwdController.enter_type = ENTER_TYPE_PRESENT;
//                    gesturePwdController.loginDelegate = self;
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:gesturePwdController];
                    [self presentViewController:nav animated:YES completion:nil];
                }
                else {
                    LoginViewController *loginViewController = [[LoginViewController alloc] init];
                    loginViewController.enter_type = ENTER_TYPE_PRESENT;
                    loginViewController.loginDelegate = self;
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginViewController];
                    [self presentViewController:nav animated:YES completion:nil];
                }
            }
        }];
        
        return NO;
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
