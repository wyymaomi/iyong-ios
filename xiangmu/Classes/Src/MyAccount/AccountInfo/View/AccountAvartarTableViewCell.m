//
//  AccountAvartarTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AccountAvartarTableViewCell.h"

@implementation AccountAvartarTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self.contentView addSubview:self.avatarImageView];
        [self.contentView addSubview:self.nickNameLabel];
        [self.contentView addSubview:self.mobileLabel];
        [self.contentView addSubview:self.companyLabel];
        
        
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.avatarImageView.top = 28;
    self.nickNameLabel.top = self.avatarImageView.bottom + 10;
    self.mobileLabel.top = self.nickNameLabel.bottom;
    self.companyLabel.top = self.mobileLabel.top;
    
}

- (UIImageView*)avatarImageView
{
    if (_avatarImageView == nil) {
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake((ViewWidth-68)/2, 28, 68, 68)];
        _avatarImageView.image = [UIImage imageNamed:@"icon_avatar"];
    }
    
    return _avatarImageView;
}

-(UILabel*)nickNameLabel
{
    if (_nickNameLabel == nil) {
        _nickNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 105, ViewWidth, 20)];
        _nickNameLabel.font = FONT(12);
        _nickNameLabel.textColor = [UIColor darkTextColor];
        _nickNameLabel.textAlignment = UITextAlignmentCenter;
    }
    return _nickNameLabel;
}

- (UILabel*)mobileLabel
{
    if (_mobileLabel == nil) {
        _mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 125, ViewWidth, 20)];
        _mobileLabel.font = FONT(12);
        _mobileLabel.textColor = [UIColor darkTextColor];
        _mobileLabel.textAlignment = UITextAlignmentCenter;
    }
    return _mobileLabel;
}

-(UILabel*)companyLabel
{
    if (_companyLabel == nil) {
        _companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 145, ViewWidth, 20)];
        _companyLabel.font = FONT(12);
        _companyLabel.textColor = [UIColor darkTextColor];
        _companyLabel.textAlignment = UITextAlignmentCenter;
    }
    return _companyLabel;
}

@end
