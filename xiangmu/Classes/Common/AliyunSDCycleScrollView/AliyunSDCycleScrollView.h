//
//  AliyunSDCycleScrollView.h
//  xiangmu
//
//  Created by 湛思科技 on 17/2/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <SDCycleScrollView/SDCycleScrollView.h>
#import <SDCycleScrollView/SDCollectionViewCell.h>

@interface AliyunSDCycleScrollView : SDCycleScrollView

@property (nonatomic, strong) NSMutableArray *imageNames;

@end
