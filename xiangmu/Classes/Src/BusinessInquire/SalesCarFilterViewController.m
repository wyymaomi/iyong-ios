//
//  SalesCarFilterViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "SalesCarFilterViewController.h"
#import "HeaderPullRefreshTableView.h"
#import "SalesFilterQueryHeaderView.h"
#import "SalesDispatchTableViewCell.h"
#import "VechileTableViewCell.h"
#import "CarModel.h"
#import "AppConfig.h"
#import "ImageUtil.h"
#import "CarDAO.h"

@interface SalesCarFilterViewController ()<UITableViewDelegate, UITableViewDataSource, PullRefreshTableViewDelegate>

@property (nonatomic, strong) HeaderPullRefreshTableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) UIButton *lastButton;

@property (assign, nonatomic) BOOL isSelfDispatch;
@property (strong, nonatomic) CarModel *carData;

@property (nonatomic, strong) NSMutableDictionary *imageDictionary;
@property (nonatomic, strong) CarDAO *carDAO;
@property (nonatomic, assign) NSInteger selectedIndex;

@end

@implementation SalesCarFilterViewController

-(CarDAO*)carDAO
{
    if (_carDAO == nil) {
        _carDAO = [[CarDAO alloc] init];
    }
    return _carDAO;
}

-(NSMutableDictionary*)imageDictionary
{
    if (_imageDictionary == nil) {
        _imageDictionary = [NSMutableDictionary new];
    }
    return _imageDictionary;
}

-(HeaderPullRefreshTableView*)tableView
{
    if (_tableView == nil) {
        _tableView = [[HeaderPullRefreshTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height  - 58) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.pullRefreshDelegate = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.tableHeaderView = [[UIView alloc] init];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"车辆选择";
    self.selectedIndex = -1;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.okButton blueStyle];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self refreshData];
    [self.tableView beginHeaderRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - business logic
- (IBAction)onOKButtonClick:(id)sender {
//    NSString *carId;
    CarModel *carData;
    BOOL isExpatriate;// 是否外派
    switch (self.selectedIndex) {
        case -1:
            [self showAlertView:@"请选择车辆"];
            return;
        case 0:
            isExpatriate = YES;
            break;
        case 1:
            isExpatriate = NO;
            break;
        default:
            isExpatriate = NO;
            NSInteger index = self.selectedIndex - 2;
            carData = self.dataList[index];
            break;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onGetCarData:carData:)]) {
        [self.delegate onGetCarData:isExpatriate carData:carData];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(double)getMaxUpdateTime:(NSArray*)dataList
{
    double maxUpdateTime = 0L;
    for (CarModel *car in dataList) {
        if ([car.updateTime doubleValue] > maxUpdateTime) {
            maxUpdateTime = [car.updateTime doubleValue];
        }
    }
    return maxUpdateTime;
}

-(void)onHeaderRefresh
{
    [self refreshData];
}


-(void)refreshData
{
    
    self.nextAction = @selector(refreshData);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *companyId = [[UserManager sharedInstance].userModel companyId];
    self.dataList = [self.carDAO getAllCar:companyId];
    [self.tableView reloadData];
    for (NSInteger i = 0; i < self.dataList.count; i++) {
        CarModel *carModel = self.dataList[i];
        if (!IsStrEmpty(carModel.imgUrl)) {
            [self downloadCarImage:carModel.imgUrl];
        }
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_LIST_API];
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:[AppConfig currentConfig].carLastUpdateStr]];
    
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf.tableView endHeaderRefresh];
        
        CarListModel *carList = [[CarListModel alloc] initWithDictionary:responseData error:nil];
        NSInteger code_status = [carList.code integerValue];
        if (code_status == STATUS_OK) {
            
            if (!IsArrEmpty(carList.data)) {
                // 增量更新
                // 本地数据库更新
                [strongSelf.carDAO updateCarList:carList.data];
                strongSelf.dataList = [strongSelf.carDAO getAllCar:companyId];
                // 本地缓存最新更新时间，取列表中最大的updateTime
                double maxUpdateTime = [strongSelf getMaxUpdateTime:carList.data];
                [AppConfig currentConfig].carLastUpdateStr = [NSString stringWithFormat:@"%.f", (double)maxUpdateTime];
                [strongSelf.tableView reloadData];
                
                for (NSInteger i = 0; i < carList.data.count; i++) {
                    CarModel *carModel = carList.data[i];
                    if (!IsStrEmpty(carModel.imgUrl) && [carModel.valid boolValue]) {
                        [strongSelf downloadCarImage:carModel.imgUrl];
                    }
                }
            }
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    }andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf.tableView endHeaderRefresh];
        [strongSelf resetAction];
    }];
}

-(void)downloadCarImage:(NSString*)imgUrl
{
    NSString *convertImgUrl = [imgUrl replaceAll:@"/" with:@"-"];
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 0) {
            [strongSelf.imageDictionary setValue:imageData forKey:convertImgUrl];
            [strongSelf.tableView reloadData];
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataList.count + 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < 2) {
        return 50;
    }
    return 94;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.dataList.count > 0 && section == 2) {
        return 10;
    }
    return 0.0000001f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section < 2) {
        return 1;
    }
    
//    if (self.dataList > 0 && section == self.dataList.count + 2 - 1) {
//        return 0.0001f;
//    }
    return 10;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    if (indexPath.section < 2) {
        SalesDispatchTableViewCell *cell = [SalesDispatchTableViewCell cellWithTableView:tableView indexPath:indexPath];
        return cell;
    }
    else {
        VechileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"VechileTableViewCell" owner:nil options:nil] lastObject];
            cell.backgroundColor = [UIColor whiteColor];
        }
        
        CarModel *model = self.dataList[indexPath.section - 2];
        cell.carNumberLbl.text = model.number;
        cell.driverNameLbl.text = model.driver;
        cell.driverMobile.text = model.mobile;
        
        if (IsStrEmpty(model.imgUrl)) {
            cell.vechileImageView.image = [UIImage imageNamed:@"img_vechile_default"];
        }
        else {
            NSString *key = [model.imgUrl replaceAll:@"/" with:@"-"];
            if (self.imageDictionary[key] == nil) {
                cell.vechileImageView.image = [UIImage imageNamed:@"img_vechile_default"];
            }
            else {
                NSData *data = self.imageDictionary[key];
                model.imgData = data;
                UIImage *image = [UIImage imageWithData:data];
                cell.vechileImageView.image = image;
            }
        }
        
        return cell;
    }
    
    return nil;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.selectedIndex = indexPath.section;

}





@end
