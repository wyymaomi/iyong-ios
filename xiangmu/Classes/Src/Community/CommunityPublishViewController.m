//
//  CommunityPublishViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityPublishViewController.h"
#import "CommunityPublishTableViewCell.h"
#import "CommunityCarTableViewCell.h"
#import "CommunityPublishHttpMessage.h"
#import "CustomActionSheetDatePicker.h"
#import "TextInputHelperViewController.h"
#import "CarTableViewController.h"
#import "LocationSearchViewController.h"
#import "OrderFooterView.h"
#import "DateUtil.h"

@interface CommunityPublishViewController ()<InputHelperDelegate, CarTableViewControllDelegate, UIActionSheetDelegate,LocationSearchDelegate>

@property (nonatomic, strong) CommunityPublishHttpMessage *communityPublishModel;
@property (nonatomic, strong) CarModel *carModel;

@end

@implementation CommunityPublishViewController

-(CommunityPublishHttpMessage*)communityPublishModel
{
    if (_communityPublishModel == nil) {
        _communityPublishModel = [[CommunityPublishHttpMessage alloc] init];
    }
    return _communityPublishModel;
}

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"社区信息发布";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 

-(void)onGetCarData:(CarModel*)carModel;
{
    self.carModel = carModel;
    self.communityPublishModel.carId = carModel.id;
    [self.tableView reloadData];
}

#pragma makr - 发布
-(void)doPublish
{
    if (IsStrEmpty(self.communityPublishModel.startTime) || IsStrEmpty(self.communityPublishModel.endTime)) {
        [self showAlertView:@"请输入开始日期和结束日期"];
        return;
    }
    
    NSDate *date1 = [NSDate dateFromString:self.communityPublishModel.startTime dateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date2 = [NSDate dateFromString:self.communityPublishModel.endTime dateFormat:@"yyyy-MM-dd HH:mm"];
    NSInteger compareResult = [NSDate dateCompare:date1 secondDate:date2];
    if (compareResult >=0) {
        [self showAlertView:@"结束时间必须大于开始时间"];
        return;
    }
    
    if (IsStrEmpty(self.communityPublishModel.seat)) {
        [self showAlertView:@"请输入车辆当前所在位置"];
        return;
    }
    if (IsStrEmpty(self.communityPublishModel.carId)) {
        [self showAlertView:@"请选择车辆"];
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_PUBLISH_API];
    NSString *params = [NSString stringWithFormat:@"carId=%@&startTime=%@&endTime=%@&seat=%@", self.communityPublishModel.carId, self.communityPublishModel.startTime, self.communityPublishModel.endTime, self.communityPublishModel.seat];
    

    [self showHUDIndicatorViewAtCenter:@"正在发布，请稍候..."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"发布成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
               
                if (buttonIndex == 0) {
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
                
            }];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];

}

#pragma mark - 拨打司机电话

-(void)callMobile
{
    NSString *mobile = self.carModel.mobile.trim;
    if (!IsStrEmpty(mobile)) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"取消"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:mobile,nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
//        actionSheet.tag = ActionSheetTagDriverMobile;
        [actionSheet showInView:self.view];
    }
    
}

#pragma mark - UIActionSheet Delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if (buttonIndex == 0) {
        NSString *mobile = self.carModel.mobile.trim;
            NSString *num = [NSString stringWithFormat:@"tel://%@", mobile];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]];
    }
}

#pragma mark - 选择开始时间
#pragma mark - business logic
- (void)selectStartTime
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@""
                                           doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
                                               StrongSelf
                                               strongSelf.communityPublishModel.startTime = selectDateTime;
                                               [strongSelf.tableView reloadData];
                                           } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                               
                                           } origin:self.view];
}

-(void)selectEndTime
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@""
                                           doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
                                               StrongSelf
                                               strongSelf.communityPublishModel.endTime = selectDateTime;
                                               [strongSelf.tableView reloadData];
                                               
                                               NSDate *date1 = [NSDate dateFromString:strongSelf.communityPublishModel.startTime dateFormat:@"yyyy-MM-dd HH:mm"];
                                               NSDate *date2 = [NSDate dateFromString:strongSelf.communityPublishModel.endTime dateFormat:@"yyyy-MM-dd HH:mm"];
                                               NSInteger compareResult = [NSDate dateCompare:date1 secondDate:date2];
                                               if (compareResult >=0) {
                                                   [strongSelf showAlertView:@"结束时间必须大于开始时间"];
                                               }
                                               
                                           } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                               
                                               
                                           } origin:self.view];
}

#pragma mark - InputHelperDelegate
-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
{
    self.communityPublishModel.seat = text;
    [self.tableView reloadData];
}

#pragma mark - Location Delegate method

- (void)didSelectLocation:(NSString*)location;
{

    [self onTextDidChanged:0 text:location];
    
    [self.tableView reloadData];
}

#pragma mark - UITableView Delegate Method
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return 60;
    }
    return 0.001f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width,10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == [tableView numberOfSections] - 1) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(15, 10, tableView.frame.size.width-15*2, 40);
        [button setTitle:@"发    布" forState:UIControlStateNormal];
        [button blueStyle];
        [footerView addSubview:button];
        [button addTarget:self action:@selector(doPublish) forControlEvents:UIControlEventTouchUpInside];
        return footerView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 44;
    }
    return 90;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        CommunityPublishTableViewCell *cell = [CommunityPublishTableViewCell cellWithTableView:tableView indexPath:indexPath];
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"开始时间";
            cell.iconImageView.image = [UIImage imageNamed:@"icon_car_time"];
            cell.textField.text = self.communityPublishModel.startTime;
            cell.textField.placeholder = @"请选择车辆开始闲置时间";
        }
        if (indexPath.row == 1) {
            cell.titleLabel.text = @"结束时间";
            cell.iconImageView.image = [UIImage imageNamed:@"icon_end_time"];
            cell.textField.text = self.communityPublishModel.endTime;
            cell.textField.placeholder = @"请选择车辆结束闲置时间";
        }
        if (indexPath.row == 2) {
            cell.titleLabel.text = @"闲置位置";
            cell.iconImageView.image = [UIImage imageNamed:@"icon_location"];
            cell.iconImageView.width = 20;
            cell.iconImageView.height = 18;
            cell.textField.text = self.communityPublishModel.seat;
            cell.textField.placeholder = @"请输入车辆当前所在位置";
        }
        
        return cell;
        
    }
    else {
        
        CommunityCarTableViewCell *cell = [CommunityCarTableViewCell cellWithTableView:tableView indexPath:indexPath];

        if (self.carModel == nil || IsStrEmpty(self.carModel.id)) {
            cell.vechileSelectView.hidden = NO;
            cell.driverInfoVIew.hidden = YES;
        }
        else {
            cell.vechileSelectView.hidden = YES;
            cell.driverInfoVIew.hidden = NO;

            cell.carNumberLabel.text = self.carModel.number.trim;
            cell.driverNameLabel.text = self.carModel.driver.trim;
            [cell.driverMobileButton setTitle:self.carModel.mobile.trim forState:UIControlStateNormal];
            [cell.driverMobileButton addTarget:self action:@selector(callMobile) forControlEvents:UIControlEventTouchUpInside];

            if (self.carModel.imgData != nil && self.carModel.imgData > 0) {
                cell.carImageView.image = [UIImage imageWithData:self.carModel.imgData];
            }
            else {
//                [cell.carImageView downloadImageFromAliyunOSS:self.carModel.imgUrl isThumbnail:NO placeholderImage:[UIImage imageNamed:@"img_vechile_default"] success:nil andFailure:nil];
//                cell.carImageView yy_setImageWithObjectKey:self.carModel.imgUrl placeholder:DefaultVechi manager:<#(nullable YYWebImageManager *)#> progress:<#^(NSInteger receivedSize, NSInteger expectedSize)progress#> completion:<#^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error)completion#>
//                [cell.carImageView downloadImage:self.carModel.imgUrl placeholderImage:[UIImage imageNamed:@"img_vechile_default"] success:nil andFailure:nil];
            }

        }
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        return cell;
        
        
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self selectStartTime];
        }
        if (indexPath.row == 1) {
            [self selectEndTime];
        }
        if (indexPath.row == 2) {
            
            LocationSearchViewController *viewController = [[LocationSearchViewController alloc] init];
            viewController.delegate = self;
            [self.navigationController pushViewController:viewController animated:YES];

        }
    }
    else {
        
        CarTableViewController *viewController = [[CarTableViewController alloc] init];
        viewController.listType = VechileListTypecertified;
        viewController.carDelegate = self;
        viewController.source_from = SOURCE_FROM_COMMUNITY_PUBLISH;
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    

}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}


@end
