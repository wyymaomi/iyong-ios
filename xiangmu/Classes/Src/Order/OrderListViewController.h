//
//  OrderListViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

//#import "BaseViewController.h"
#import "BaseTableViewController.h"
#import "OrderListResponseModel.h"

@interface OrderListViewController : BaseViewController

@property (nonatomic, assign) NSUInteger orderStatus;
@property (nonatomic, strong) NSString *requestUrlStr;

@end
