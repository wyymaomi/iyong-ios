//
//  DispatchOrderViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/4/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderManagerViewController.h"

#import "BBAlertView.h"
#import "CustomActionSheetDatePicker.h"
#import "ActionSheetDatePicker.h"
#import "SWActionSheet.h"
#import "MsgView.h"
#import "DateUtil.h"
#import "PasswordBuild.h"
#import "EncryptUtil.h"
#import "UserHelper.h"
#import "OrderHelper.h"

#import "CarTableViewController.h"
#import "NewOrderViewController.h"
#import "InputHelperViewController.h"
#import "TextInputHelperViewController.h"
#import "PasswordSetViewController.h"
#import "CreditLineViewController.h"
#import "LocationSearchViewController.h"

#import "OrderBaseInfoTableViewCell.h"
#import "DispatchOrderTableViewCell.h"
#import "VechileArrangeTableViewCell.h"
#import "WayBillTableViewCell.h"
#import "SettlementAndPayTableViewCell.h"

#import "OrderStatusHeader.h"
#import "OrderHeaderSectionView.h"
#import "OrderFooterView.h"




#import "CarModel.h"

#import "MyPartnerListViewController.h"

@interface OrderManagerViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, OrderHeaderSectionViewDelegate, MyPartnerDelegate, DispatchOrderTableViewCellDelegate,SettlementAndPayTableViewCellDelegate, CarTableViewControllDelegate, InputHelperDelegate, WayBillTableViewCellDelegate, OrderViewControllerDelegate, TTMessageDelegate, UIActionSheetDelegate,LocationSearchDelegate>

@property (nonatomic, strong) NSString *carDateStr;
@property (nonatomic, strong) UIButton *resendSMSButton;
@property (nonatomic, strong) UIButton *sendSMSButton;
@property (nonatomic, strong) NSTimer *countDownTimer;// 倒计时定时器
@property (nonatomic, strong) NSData *carImageData;// 车辆图片下载数据

@end

@implementation OrderManagerViewController

-(NewOrderHttpMessage*)orderHttpMessage
{
    if (_orderHttpMessage == nil) {
        _orderHttpMessage = [NewOrderHttpMessage new];
    }
    return _orderHttpMessage;
}

-(DispatchOrderHttpMessage*)dispatchOrderRequestMsg
{
    if (_dispatchOrderRequestMsg == nil) {
        _dispatchOrderRequestMsg = [DispatchOrderHttpMessage new];
    }
    return _dispatchOrderRequestMsg;
}

-(VechileArrangeHttpMessage*)vechileArrangeReqeustMsg
{
    if (_vechileArrangeReqeustMsg == nil) {
        _vechileArrangeReqeustMsg = [VechileArrangeHttpMessage new];
    }
    return _vechileArrangeReqeustMsg;
}

-(WayBillHttpMessage*)wayBillRequestMsg
{
    if (IsNilOrNull(_wayBillRequestMsg)) {
        _wayBillRequestMsg = [WayBillHttpMessage new];
    }
    return _wayBillRequestMsg;
}

-(AgreepayHttpMessage*)agreePayHttpMsg
{
    if (IsNilOrNull(_agreePayHttpMsg)) {
        _agreePayHttpMsg = [AgreepayHttpMessage new];
    }
    return _agreePayHttpMsg;
}

-(SettlementHttpMessage*)settleHttpMsg
{
    if (IsNilOrNull(_settleHttpMsg)) {
        _settleHttpMsg = [SettlementHttpMessage new];
    }
    return _settleHttpMsg;
}

-(UITableView*)orderTableView
{
    if (_orderTableView == nil) {
        _orderTableView = [[TPKeyboardAvoidingTableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        _orderTableView.backgroundColor = UIColorFromRGB(0xDEDEDE);
        _orderTableView.delegate = self;
        _orderTableView.dataSource = self;
        _orderTableView.userInteractionEnabled = YES;
        _orderTableView.bounces = YES;
        _orderTableView.scrollEnabled = YES;
        _orderTableView.showsHorizontalScrollIndicator = NO;
        _orderTableView.showsVerticalScrollIndicator = YES;
        _orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _orderTableView.autoresizesSubviews = YES;
        _orderTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_orderTableView];
    }
    return _orderTableView;
}

- (NSArray *)headerStatuses
{
    if (IsNilOrNull(_headerStatuses)) {
        // 加载plist中的字典数组
        NSString *path = [[NSBundle mainBundle] pathForResource:ORDER_STATUS_HEADER_PLIST ofType:nil];
        NSArray *dictArray = [NSArray arrayWithContentsOfFile:path];
        
        // 字典数组 -> 模型数组
        NSMutableArray *statusArray = [NSMutableArray array];
        for (NSDictionary *dict in dictArray) {
            OrderStatusHeader *orderStatusHeader = [[OrderStatusHeader alloc] initWithDict:dict];
            [statusArray addObject:orderStatusHeader];
        }
        _headerStatuses = statusArray;
        if (self.orderMode == OrderModeDetail) {
            for (NSInteger section = 0; section < _headerStatuses.count; section++) {
                OrderStatusHeader *header = _headerStatuses[section];
                
                switch (section) {
                    case OrderSectionNew:
                        header.isOpen = [self.orderBaseInfoModel.type integerValue] > 0;
                        break;
                    case OrderSectionDispatch:
                        header.isOpen = [self.dispatchPriceModel.type integerValue] > 0;
                        break;
                    case OrderSectionVechileArrange:
                        header.isOpen = [self.vechileArrangeModel.type integerValue] > 0;
                        break;
                    case OrderSectionWayBill:
                        header.isOpen = [self.wayBillModel.type integerValue] > 0;
                        break;
                    case OrderSectionPay:
                        header.isOpen = YES;
                        break;
                    default:
                        break;
                }
            }
        }
    }
    return _headerStatuses;
}

-(PayHttpMessage*)payHttpMessage
{
    if (IsNilOrNull(_payHttpMessage)) {
        _payHttpMessage = [PayHttpMessage new];
    }
    return _payHttpMessage;
}

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //    [self refreshView];
    
}

-(void)initRightBarButtonItem
{
    // 复制按钮，在选择车辆后才呈现
    if ((self.orderStatus > OrderStatusVechileArrange && self.orderStatus < OrderStatusBarginAndPay) || (self.orderStatus == OrderStatusVechileArrange && !IsStrEmpty(self.vechileArrangeModel.carId))){
        if (!self.navigationItem.rightBarButtonItem) {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"复制" style:UIBarButtonItemStyleDone target:self action:@selector(pressedCopyButton)];
        }
    }
    else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self refreshView];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)pressedBackButton
{
    if (self.enter_type == ENTER_TYPE_PUSH) {
        if (self.source_from == SOURCE_FROM_ORDER_LIST) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if(self.source_from == SOURCE_FROM_NEW_ORDER)
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}




#pragma mark - UITableView delegate method

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderStatusHeader *header = self.headerStatuses[indexPath.section];
    if (indexPath.section == OrderSectionVechileArrange) {
        return [VechileArrangeTableViewCell getHeight:self.vechileArrangeModel];
    }
    if (indexPath.section == OrderSectionDispatch) {
        return [DispatchOrderTableViewCell getHeight:self.dispatchPriceModel];
    }
    if (indexPath.section == OrderSectionPay) {
        SettlementAndPayTableViewCell *cell = (SettlementAndPayTableViewCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        if (cell != nil) {
            return cell.height;
        }
    }
    return header.cell_height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.orderStatus;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    OrderStatusHeader *header = self.headerStatuses[section];
    
    if (header.isOpen) {
        return 1;
    }
    return 0;
}

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    OrderStatusHeader *header = self.headerStatuses[section];
    OrderHeaderSectionView *headerView = [OrderHeaderSectionView customView];
    headerView.delegate = self;
    [headerView initDataWithHeaderStatus:section headerStatus:header];
    return headerView;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == OrderSectionNew) {
        OrderBaseInfoTableViewCell *cell = [OrderBaseInfoTableViewCell cellWithTableView:tableView];
        [cell initData:self.orderBaseInfoModel];
        // 用车日期
        cell.carDateLabel.userInteractionEnabled = YES;
        cell.carDateLabel.enabled = YES;
        if (cell.carDateLabel.isUserInteractionEnabled) {
//            [cell.carDateLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doSetCarDate:)]];
        }
        cell.itineraryTextView.delegate = self;
        cell.memoTextView.delegate = self;
        [cell.telephoneButton addTarget:self action:@selector(callCustomer:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.editButton addTarget:self action:@selector(onBaseInfoUpdateBtnClik) forControlEvents:UIControlEventTouchUpInside];
        cell.editButton.tag = OrderSectionNew;
        return cell;
    }
    if (indexPath.section == OrderSectionDispatch) {
        DispatchOrderTableViewCell *cell = [DispatchOrderTableViewCell cellWithTableView:tableView dispatchPriceModel:self.dispatchPriceModel];
        [cell initData:self.dispatchPriceModel];
        cell.delegate = self;
        
//        [cell.editButton addTarget:self action:@selector(onDispatchRollbackBtnClick) forControlEvents:UIControlEventTouchUpInside];
        cell.editButton.tag = OrderSectionDispatch;
        
        return cell;
        
    }
    if (indexPath.section == OrderSectionVechileArrange) {
        VechileArrangeTableViewCell *cell = [VechileArrangeTableViewCell cellWithTableView:tableView];
        [cell initData:self.vechileArrangeModel];
        if (IsStrEmpty(self.vechileArrangeModel.imgUrl)) {
            cell.vechileImageView.image = [UIImage imageNamed:IMG_VECHILE_PLACE_HOLDER];
        }
        else {
            if (self.carImageData != nil && self.carImageData.length > 0) {
                cell.vechileImageView.image = [UIImage imageWithData:self.carImageData];
            }
            else {
                cell.vechileImageView.image = [UIImage imageNamed:IMG_VECHILE_PLACE_HOLDER];
            }
        }
        
        [cell.carSelView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addCar)]];
        [cell.driverMobileButton addTarget:self action:@selector(callMobile) forControlEvents:UIControlEventTouchUpInside];
        cell.changeButton.tag = OrderSectionVechileArrange;
        [cell.changeButton addTarget:self action:@selector(onCarRollbackBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [cell.smsButton addTarget:self action:@selector(sendSms) forControlEvents:UIControlEventTouchUpInside];
        [cell.wechatButton addTarget:self action:@selector(sendWechatMessaage) forControlEvents:UIControlEventTouchUpInside];
        [cell.telephoneButton addTarget:self action:@selector(callMobile) forControlEvents:UIControlEventTouchUpInside];
        self.resendSMSButton = cell.resendButton;
        self.sendSMSButton = cell.smsButton;
        
        
        return cell;
    }
    if (indexPath.section == OrderSectionWayBill) {
        WayBillTableViewCell *cell = [WayBillTableViewCell cellWithTableView:tableView wayBillModel:self.wayBillModel];
        [cell initData:self.wayBillModel];
        //        [cell.editButton addTarget:self action:@selector(onWayBillRollBack) forControlEvents:UIControlEventTouchUpInside];
        cell.delegate = self;
        return cell;
    }
    if (indexPath.section == OrderSectionPay) {
        SettlementAndPayTableViewCell *cell = [SettlementAndPayTableViewCell cellWithTableView:tableView];
        [cell initData:self.barginAndPayModel];
        cell.refreshDelegate = self;
        cell.settlementPriceTextField.tag = OrderTextFieldSettlementPrice;
        [cell.changeButton addTarget:self action:@selector(onBarginChanged) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == [tableView numberOfSections]-1) {
        if (self.orderStatus == OrderStatusNew && NotNilAndNull(self.orderBaseInfoModel) &&  [self.orderBaseInfoModel.type integerValue] == OrderInfoTypeNew) {
            return 60;
        }
        if (self.orderStatus == OrderStatusDispatch && NotNilAndNull(self.dispatchPriceModel) && [self.dispatchPriceModel.type integerValue] == OrderDispatchTypeFirst && [self.dispatchPriceModel.rollbackStatus integerValue] == OrderRollbackStatusInit) {
            return 60;
        }
        if (self.orderStatus == OrderStatusVechileArrange && NotNilAndNull(self.vechileArrangeModel) ) {
            if ([self.vechileArrangeModel.type integerValue] != OrderVechilearrangeTypeFirst && [self.vechileArrangeModel.confirm integerValue] == 0) {
                // 司机未确认，需要提交司机已确认
                return 60;
            }
        }
        if (self.orderStatus == OrderStatusWayBill && NotNilAndNull(self.wayBillModel) && [self.wayBillModel.type integerValue] == OrderWaybillTypeFirst) {
            return 60;
        }
        if (self.orderStatus == OrderStatusBarginAndPay) {
            if (NotNilAndNull(self.barginAndPayModel)) {
                if ([self.barginAndPayModel.selected isEqualToString:@"bargain"]) {
                    if (NotNilAndNull(self.barginAndPayModel) && [self.barginAndPayModel.bargain.type integerValue] == OrderPayTypeFirst) {
                        return 60;
                    }
                }
                if ([self.barginAndPayModel.selected isEqualToString:@"pay"]) {
                    if (/*NotNilAndNull(self.barginAndPayModel) &&*/ [self.barginAndPayModel.pay.type integerValue] == OrderPayTypeFirstPay || [self.barginAndPayModel.pay.type integerValue] == OrderPayTypeFirst) {
                        return 60;
                    }
                }
            }
            
        }
    }
    return 0.0001f;
}

//-(NSString*)getFooterButtonTitle
//{
//    return nil;
//}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    if (section == [tableView numberOfSections] - 1) {
        
        // 如果为支付，显示充值和支付两个按钮
        if (_orderStatus == OrderStatusBarginAndPay) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:OrderSectionPay];
            SettlementAndPayTableViewCell *cell = (SettlementAndPayTableViewCell*)[self tableView:self.orderTableView cellForRowAtIndexPath:indexPath];
            if (cell) {
                if (cell.status == CompleteStatusWaitPay) {
                    UIView *view = [UIView new];
                    return view;
                }
                 else if (cell.status == CompleteStatusCheckPay) {
                     OrderFooterView *footerView = [[OrderFooterView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 60)];

                     footerView.rechargeButton.hidden = NO;
                     footerView.rechargeButton.left = 10;
                     footerView.rechargeButton.width = footerView.frame.size.width/2-15;
                     [footerView.rechargeButton setTitle:@"去充值" forState:UIControlStateNormal];
                     [footerView.rechargeButton addTarget:self action:@selector(gotoRechargePage:) forControlEvents:UIControlEventTouchUpInside];
                     
                     footerView.okButton.hidden = NO;
                     footerView.okButton.left = footerView.frame.size.width/2+5;
//                     footerView.okButton.width = ViewWidth/2-20;
                     footerView.okButton.width = footerView.rechargeButton.width;
                     [footerView.okButton setTitle:@"确定" forState:UIControlStateNormal];
                     [footerView.okButton addTarget:self action:@selector(doEnvelopHttpRequestMsg) forControlEvents:UIControlEventTouchUpInside];
                     
                     return footerView;
                     
                 }
                else if (cell.status == CompleteStatusHavePay) {
                    return [UIView new];
                }
                 else {
                     OrderFooterView *footerView = [[OrderFooterView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 60)];
                     [footerView.okButton setTitle:@"确定" forState:UIControlStateNormal];
                     [footerView.okButton addTarget:self action:@selector(doEnvelopHttpRequestMsg) forControlEvents:UIControlEventTouchUpInside];
                     return footerView;
                 }
            }
        }
        else {
            CGFloat footerHeight = [self tableView:tableView heightForFooterInSection:section];
            if (footerHeight > 0.0001f) {
                OrderFooterView *footerView = [[OrderFooterView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, 60)];
                //            OrderFooterView *footerView = [OrderFooterView customView];
                [footerView.okButton setTitle:@"确定" forState:UIControlStateNormal];
                [footerView.okButton addTarget:self action:@selector(doEnvelopHttpRequestMsg) forControlEvents:UIControlEventTouchUpInside];
                return footerView;
            }
            else {
                return [UIView new];
            }

        }

    }
    return [UIView new];
}

#pragma mark - OrderHeaderSectionViewDelegate method
-(void)sectionHeaderView:(OrderHeaderSectionView*)sectionHeaderView sectionClosed:(NSInteger)section;
{
    OrderStatusHeader *header = self.headerStatuses[section];
    header.isOpen = NO;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
    NSArray *array = @[indexPath];
    NSInteger countOfRowsToDelete = [self.orderTableView numberOfRowsInSection:section];
    if (countOfRowsToDelete > 0) {
        [self.orderTableView deleteRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationTop];
    }
}

-(void)sectionHeaderView:(OrderHeaderSectionView*)sectionHeaderView sectionOpened:(NSInteger)section;
{
    if (self.orderMode == 0) {
        OrderStatusHeader *header = self.headerStatuses[section];
        header.isOpen = YES;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
        NSArray *array = @[indexPath];
        [self.orderTableView insertRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationBottom];
    }
    else if (self.orderMode == 1) {
        [self onQueryDetail:[NSNumber numberWithInteger:section]];
    }
    
}

#pragma mark - 获取编辑输入的值
-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
{
    switch (tag) {
        case OrderTextFieldDispatchPrice:
            self.dispatchPriceModel.editDstPrice = text.trim;
            break;
        case OrderTextFieldArriveLocation:
            self.wayBillModel.editLocation = text.trim;
            break;
        case OrderTextFieldDistance:
            self.wayBillModel.editDistance = text.trim;
            break;
        case OrderTextFieldRoadRemark:
            self.wayBillModel.editRemark = text.trim;
            break;
        case OrderTextFieldSettlementPrice:
            self.barginAndPayModel.bargain.editDstPrice = text.trim;
            break;
        default:
            break;
    }
    [self refreshView];
}

#pragma mark - SearchViewController Delegate method
- (void)didSelectLocation:(NSString*)location;
{
    self.wayBillModel.editLocation = location;
}

#pragma mark - 发送短信 微信 电话

-(void)sendSms
{
    WeakSelf
    [UserHelper verificationCode:^{
        weakSelf.resendSMSButton.hidden = YES;
        weakSelf.sendSMSButton.hidden = NO;
    } blockNo:^(id time) {
        if ([time isKindOfClass:[NSString class]]) {
            weakSelf.resendSMSButton.hidden = NO;
            weakSelf.sendSMSButton.hidden = YES;
            NSString *resendSMSTitle = [NSString stringWithFormat:@"%@秒后重发", time];
            weakSelf.resendSMSButton.titleLabel.text = resendSMSTitle;
        }
    }];
    
    self.nextAction = @selector(sendSms);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_SMS_API];
    NSString *orderId = _orderMode == 1 ? self.orderId : self.orderBaseInfoModel.id;
    NSString *mobile = self.vechileArrangeModel.mobile;
    NSString *param = [NSString stringWithFormat:@"orderId=%@&mobile=%@", orderId, mobile];
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
//    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                // 发送短信成功
                [strongSelf showAlertView:MSG_SEND_SMS_SUCCESS];
            }
            else if([data integerValue] == 0){
                // 发送短信失败
                [strongSelf showAlertView:MSG_SEND_SMS_FAILURE];
            }
        }
        else {
            [MsgToolBox showAlert:@"" content:getErrorMsg(code_status)];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];
    
}

-(void)sendWechatMessaage
{
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_COPY_API];
    NSString *orderId = _orderMode == 1 ? self.orderId : self.orderBaseInfoModel.id;
    NSString *param = [NSString stringWithFormat:@"orderId=%@", orderId];
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSMutableString *copyString = [NSMutableString new];
            NSString *carDateTime = [[NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([strongSelf.orderBaseInfoModel.carTime doubleValue] / 1000.0f)] stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [copyString appendFormat:@"用车日期：%@", carDateTime];
            [copyString appendFormat:@"出发地点：%@\n", strongSelf.orderBaseInfoModel.location.trim];
            [copyString appendFormat:@"大致行程：%@\n", strongSelf.orderBaseInfoModel.itinerary.trim];
            [copyString appendFormat:@"用车人：%@\n", strongSelf.orderBaseInfoModel.customerName.trim];
            [copyString appendFormat:@"电话：%@\n", strongSelf.orderBaseInfoModel.customerMobile.trim];
            [copyString appendFormat:@"备注：%@\n", strongSelf.orderBaseInfoModel.remark.trim];
            [copyString appendFormat:@"车辆安排：%@ %@ %@", strongSelf.vechileArrangeModel.number.trim, strongSelf.vechileArrangeModel.driver.trim, strongSelf.vechileArrangeModel.mobile.trim];
        }
        else {
            
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];
    
}

#pragma mark - 拨打客户电话
-(void)callCustomer:(UIButton*)button
{
    NSString *tel = button.titleLabel.text;
    if (!IsStrEmpty(tel)) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"取消"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:tel, nil];
        actionSheet.tag = ActionSheetTagCustomerMobile;
        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
        [actionSheet showInView:self.view];
    }
}

#pragma mark - 拨打司机电话
-(void)callMobile
{
    NSString *mobile = self.vechileArrangeModel.mobile.trim;
    if (!IsStrEmpty(mobile)) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"取消"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:mobile,nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
        actionSheet.tag = ActionSheetTagDriverMobile;
        [actionSheet showInView:self.view];
    }
    
}

#pragma mark - UIActionSheet Delegate method
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == ActionSheetTagCustomerMobile) {
        if (buttonIndex == 0) {
            NSString *mobile = self.orderBaseInfoModel.customerMobile.trim;
            NSString *num = [NSString stringWithFormat:@"tel://%@", mobile];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]];
        }
    }
    else if (actionSheet.tag == ActionSheetTagDriverMobile) {
        if (buttonIndex == 0) {
            NSString *mobile = self.vechileArrangeModel.mobile.trim;
            NSString *num = [[NSString alloc] initWithFormat:@"tel://%@",mobile];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]];
        }
    }
    
}

#pragma mark - 复制信息
- (void)pressedCopyButton
{
    [self doGetOrderDetail];
}

-(void)doGetOrderDetail
{
    
    self.nextAction = @selector(doGetOrderDetail);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_COPY_API];
    NSString *orderId = _orderMode == 1 ? self.orderId : self.orderBaseInfoModel.id;
    NSString *param = [NSString stringWithFormat:@"orderId=%@", orderId];
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            OrderCopyResponseModel *data = [[OrderCopyResponseModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            strongSelf.pasteOrderString = [strongSelf generateCopyOrderStr:data];
            TTMessage *message = [[TTMessage alloc] init];
            message.delegate = strongSelf;
            [message showMessageViewWithTitle:data];
        }
        else {
            
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];
}

-(NSString*)generateCopyOrderStr:(OrderCopyResponseModel*)orderCopyData
{
    NSMutableString *copyString = [NSMutableString new];
    NSString *carDateTime = [[NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([orderCopyData.carTime doubleValue] / 1000.0f)] stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [copyString appendFormat:@"用车日期：%@", carDateTime];
    [copyString appendFormat:@"出发地点：%@\n", orderCopyData.location.trim];
    [copyString appendFormat:@"大致行程：%@\n", orderCopyData.itinerary.trim];
    [copyString appendFormat:@"用车人：%@\n", orderCopyData.customerName.trim];
    [copyString appendFormat:@"电话：%@\n", orderCopyData.customerMobile.trim];
    [copyString appendFormat:@"备注：%@\n", orderCopyData.remark.trim];
    [copyString appendFormat:@"车辆安排：%@ %@ %@", orderCopyData.carInfo.number.trim, orderCopyData.carInfo.driver.trim, orderCopyData.carInfo.mobile.trim];
    return copyString;
}

-(void)onClickOKButton;
{
    if (IsStrEmpty(self.pasteOrderString)) {
        return;
    }
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.pasteOrderString;
}

-(void)onClickCancelButton;
{
    //    [self showAlertView:@"Cancelled"];
}

#pragma mark - 车辆模块相关业务逻辑

-(void)doSetCarDate:(id)sender
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@"请选择日期和时间"
                                           doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
                                               weakSelf.orderBaseInfoModel.editCarTime = selectDateTime;
                                               [weakSelf.orderTableView reloadData];
                                           } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                               
                                           } origin:sender];
}

-(void)onGetVechileData:(NSDictionary*)carVo;
{
    self.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:carVo error:nil];
    if (!IsStrEmpty(self.vechileArrangeModel.imgUrl)) {
        [self downloadCarImage:self.vechileArrangeModel.imgUrl];
    }
    [self refreshView];
}


-(void)addCar
{
    CarTableViewController *vc = [[CarTableViewController alloc] init];
    vc.carDelegate = self;
    vc.source_from = SOURCE_FROM_ORDER_MANAGER;
    vc.listType = VechileListTypecertified;
    // 订单号
    NSString *orderId = _orderMode == 1 ? self.orderId : self.orderBaseInfoModel.id;
    vc.orderId = orderId;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)downloadCarImage:(NSString*)imgUrl
{
    if (IsStrEmpty(imgUrl)) {
        return;
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] downloadImage:imgUrl success:^(id responseData) {
        StrongSelf
        NSData *imageData = responseData;
        if (imageData != nil && imageData.length > 23) {
            strongSelf.carImageData = imageData;
            [strongSelf refreshView];
        }
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}


#pragma mark - 派单模块相关业务逻辑

-(void)onGetDispatchPrice;
{
    InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
    viewController.text = self.dispatchPriceModel.editDstPrice.trim;
    viewController.tag = OrderTextFieldDispatchPrice;
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
    
}

-(void)onGetDispatchObject;
{
    [self doGetDispatchCompany];
}

-(void)doGetDispatchCompany
{
    self.nextAction = @selector(doGetDispatchCompany);
    self.object1 = nil;
    self.object2 = nil;
    
    MyPartnerListViewController *vc = [[MyPartnerListViewController alloc] init];
    vc.source_from = SOURCE_FROM_ORDER_MANAGER;
    vc.companyDelegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

//-(void)onGetDispatchCompany:(NSDictionary *)dict
-(void)onGetDispatchCompany:(PartnerModel*)partnerModel
{
    //    self.dispatchPriceModel.companyInfo = [NSString stringWithFormat:@"%@(%@)", dict[@"nickname"], dict[@"companyName"]];
    self.dispatchPriceModel.companyInfo = [NSString stringWithFormat:@"%@(%@)", partnerModel.nickname, partnerModel.companyName];
    self.dispatchPriceModel.dstCompanyId = partnerModel.partnerCompanyId;//dict[@"partnerCompanyId"];
    self.dispatchOrderRequestMsg.dstCompanyId = partnerModel.partnerCompanyId;//dict[@"partnerCompanyId"];
    //    [self.orderTableView reloadData];
    [self refreshView];
}

-(void)onSelfDispatch
{
    self.dispatchPriceModel.companyInfo = @"自派";
    self.dispatchPriceModel.dstCompanyId = [[UserManager sharedInstance].userModel companyId];
    self.dispatchOrderRequestMsg.dstCompanyId = [[UserManager sharedInstance].userModel companyId];
    //    [self.orderTableView reloadData];
    [self refreshView];
}


#pragma mark - 路单模块相关业务逻辑

-(void)doGetArriveLocation;
{
    
    LocationSearchViewController *viewController = [[LocationSearchViewController alloc] init];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
    
//    InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
//    viewController.text = self.wayBillModel.editLocation.trim;
//    viewController.tag = OrderTextFieldArriveLocation;
//    viewController.delegate = self;
//    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)doGetEndTime;
{
    [self doSetEndtime];
}

-(void)doGetDistance;
{
    InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
    viewController.text = self.wayBillModel.editDistance.trim;
    viewController.tag = OrderTextFieldDistance;
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)doGetRemark;
{
    TextInputHelperViewController *viewController = [[TextInputHelperViewController alloc] init];
    viewController.title = @"路单备注";
    viewController.text = self.wayBillModel.editRemark.trim;
    viewController.tag = OrderTextFieldRoadRemark;
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
    
}

#pragma mark - 选择结束时间
-(void)doSetEndtime
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:OrderSectionWayBill];
    WayBillTableViewCell *cell = [self.orderTableView cellForRowAtIndexPath:indexPath];
    NSDate *carDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)([self.orderBaseInfoModel.carTime doubleValue] / 1000.0f)];
    NSDate *endDateTime = [DateUtil getTenDayAfterDateTime:carDate];
    WeakSelf
    [ActionSheetDatePicker showPickerWithTitle:@""
                                datePickerMode:UIDatePickerModeDateAndTime
                                  selectedDate:endDateTime//[NSDate date]
                                   minimumDate:endDateTime
                                   maximumDate:nil
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                                         if ([selectedDate isKindOfClass:[NSDate class]]) {
                                             StrongSelf
                                             NSDate *date = selectedDate;
                                             strongSelf.wayBillModel.editEndTime = [date stringWithDateFormat:@"yyyy-MM-dd HH:mm"];
                                             [strongSelf.orderTableView reloadData];
                                         }
                                     } cancelBlock:^(ActionSheetDatePicker *picker) {
                                         
                                         
                                     } origin:cell.editEndTimeTextField];
}

#pragma mark - SettlementAndPayTableViewCellDelegate method
-(void)doGetSettlementPrice
{
    InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
    viewController.text = self.barginAndPayModel.bargain.editDstPrice;
    viewController.tag = OrderTextFieldSettlementPrice;
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
    
}

-(void)doRefreshSettlementAndPayModule:(NSInteger)status;
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:OrderSectionPay];
    NSArray *array = @[indexPath];
    [self.orderTableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
}

-(void)onClickRate:(NSInteger)index;
{
    
    RateModel *rateModel = self.barginAndPayModel.pay.rates[index];
    self.payHttpMessage.rateId = rateModel.id;
    self.payHttpMessage.payPrice = rateModel.price;
    self.payHttpMessage.payType = rateModel.payType;
    
    //    if (rateModel == nil) {
    //        return;
    //    }
    //    if ([rateModel.payType integerValue] == 2) {
    //        self.payHttpMessage.rateId = rateModel.id;
    //        self.payHttpMessage.payPrice = rateModel.price;
    //    }
    
}

#pragma mark -
-(void)onWayBillUpdate
{
    
}

#pragma mark - 检查是否有足够的信用余额或者帐户余额

- (void)checkAccountBalance:(NSInteger)dstPrice dstCompanyId:(NSString*)dstCompanyId
{

    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    WeakSelf
    [[NetworkManager sharedInstance] getFinanceInfo:^(NSDictionary *responseData) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSDictionary *data = responseData[@"data"];
            
            NSInteger creditAmount = [data[@"creditAmount"] integerValue];
            NSInteger accountAmount = [data[@"accountAmount"] integerValue];
//            NSInteger dstPrice = [self.dispatchPriceModel.editDstPrice integerValue];//[bidModel.price integerValue];
            // 如果信用余额足够，提示“您的信用余额为XX元，派单后将扣除XX元”
            if (dstPrice <= creditAmount) {
                NSString *alertMsg = [NSString stringWithFormat:@"您的信用余额为%ld元，派单后将扣除%ld元", (long)creditAmount, (long)dstPrice];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:alertMsg delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 1) {
                        [strongSelf onSubmit:strongSelf.dispatchOrderRequestMsg];
                    }
                    
                }];
                
            }
            else if (dstPrice <= accountAmount) {
                NSString *alertMsg = [NSString stringWithFormat:@"您的帐户余额为%ld元，派单后将扣除%ld元", (long)accountAmount, (long)dstPrice];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:alertMsg delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 1) {
                        [strongSelf onSubmit:strongSelf.dispatchOrderRequestMsg];
                    }
                    
                }];
                
            }
            else {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"账户余额不足，请充值" delegate:nil cancelButtonTitle:@"充值" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        [strongSelf gotoPage:@"NewRechargeViewController"];
                    }
                    
                }];
                
            }
        }
        
    } andFailue:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}



#pragma mark - 提交

-(void)doEnvelopHttpRequestMsg
{
    self.nextAction = @selector(doEnvelopHttpRequestMsg);
    self.object1 = nil;
    self.object2 = nil;
    
    // 订单号
    NSString *orderId = _orderMode == 1 ? self.orderId : self.orderBaseInfoModel.id;
    
    if (_orderStatus == OrderStatusDispatch) {
        if (IsNilOrNull(self.dispatchPriceModel)) {
            return;
        }
        if (IsStrEmpty(self.dispatchOrderRequestMsg.dstCompanyId)) {
            [self showAlertView:ALERT_MSG_CHOOSE_DISPATCH_OBJECT];
            return;
        }
        if (IsStrEmpty(self.dispatchPriceModel.editDstPrice) &&
            ![[UserManager sharedInstance] isSelfDispatch:self.dispatchOrderRequestMsg.dstCompanyId]) {
            [self showAlertView:ALERT_MSG_DISPATCH_PRICE_NULL];
            return;
        }
        
        self.dispatchOrderRequestMsg.orderId = orderId;
        self.dispatchOrderRequestMsg.dstPrice = self.dispatchPriceModel.editDstPrice.trim;
//        [self onSubmit:self.dispatchOrderRequestMsg];
//        ［self checkAccountBalance:self. dstCompanyId:<#(NSString *)#>
        
        // 如果是自派 不显示扣费提示
        if ([[UserManager sharedInstance] isSelfDispatch:self.dispatchOrderRequestMsg.dstCompanyId]){
            [self onSubmit:self.dispatchOrderRequestMsg];
        }
        else {
            [self checkAccountBalance:[self.dispatchOrderRequestMsg.dstPrice integerValue] dstCompanyId:self.dispatchOrderRequestMsg.dstCompanyId];
        }
        
    }
    else if (_orderStatus == OrderStatusVechileArrange) {
        self.vechileArrangeReqeustMsg.orderId = orderId;
        [self onSubmit:self.vechileArrangeReqeustMsg];
    }
    else if (_orderStatus == OrderStatusWayBill) {
        if (IsNilOrNull(self.wayBillModel)) {
            return;
        }
        if (IsStrEmpty(self.wayBillModel.editLocation)) {
            [self showAlertView:ALERT_MSG_LOCATION_NULL];
            return;
        }
        if (IsStrEmpty(self.wayBillModel.editDistance)) {
            [self showAlertView:ALERT_MSG_DISTANCE_NULL];
            return;
        }
        if (IsStrEmpty(self.wayBillModel.editEndTime)) {
            [self showAlertView:ALERT_MSG_ENDTIME_NULL];
            return;
        }
        //        if (IsStrEmpty(self.wayBillModel.editRemark)) {
        //            [self showAlertView:ALERT_MSG_REMARK_NULL];
        //            return;
        //        }
        
        self.wayBillRequestMsg.orderId = orderId;
        self.wayBillRequestMsg.location = self.wayBillModel.editLocation;
        self.wayBillRequestMsg.endTime = self.wayBillModel.editEndTime;
        self.wayBillRequestMsg.distance = self.wayBillModel.editDistance;
        self.wayBillRequestMsg.remark = self.wayBillModel.editRemark;
        
        [self onSubmit:self.wayBillRequestMsg];
    }
    else if (_orderStatus == OrderStatusBarginAndPay) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:OrderSectionPay];
        SettlementAndPayTableViewCell *cell = (SettlementAndPayTableViewCell*)[self tableView:self.orderTableView cellForRowAtIndexPath:indexPath];
        if (cell != nil) {
            if (cell.status == 0) {
                // 结算
                if (IsNilOrNull(self.barginAndPayModel) || IsStrEmpty( self.barginAndPayModel.bargain.editDstPrice)) {
                    [self showAlertView:ALERT_MSG_SETTLEMENT_PRICE_NULL];
                    return;
                }
                self.settleHttpMsg.price = self.barginAndPayModel.bargain.editDstPrice;
                self.settleHttpMsg.orderId = orderId;
                [self onSubmit:self.settleHttpMsg];
            }
            if (cell.status == 1) {
                // 同意付款
                self.agreePayHttpMsg.orderId = orderId;
                self.agreePayHttpMsg.price = [self.barginAndPayModel.pay.dstPrice stringValue];
                [self onSubmit:self.agreePayHttpMsg];
            }
            if (cell.status == 2) {
                // 付款前判断是否已经设置了支付密码，
                // 如果未设置支付，先设置支付密码，
                // 如果已经设置了支付密码，则继续发送支付请求
                if (self.payHttpMessage.rateId == nil) {
                    [self showAlertView:ALERT_MSG_RATE_ID_NULL];
                    return;
                }
                
                if([self.payHttpMessage.payType integerValue] == 1){
                    if (![[UserManager sharedInstance].userModel.empower boolValue]) {
                        BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"您的信用额度未授权\n无法使用信用付款" customView:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"申请额度"];
                        WeakSelf
                        [alertView setConfirmBlock:^{
                            CreditLineViewController *viewController = [[CreditLineViewController alloc] init];
                            viewController.hidesBottomBarWhenPushed = YES;
                            viewController.creditLine = @0;
                            [weakSelf.navigationController  pushViewController:viewController animated:YES];
                        }];
                        [alertView show];
                        return;
                    }
                }
                
                [self checkHasPayPwd];
                
            }
        }
    }
}
-(void)onSubmit:(id)requestObj
{
    if ([requestObj isKindOfClass:[NewOrderHttpMessage class]]) {
        NewOrderHttpMessage *httpMsg = (NewOrderHttpMessage*)requestObj;
        NSString *orderId = _orderMode == 1 ? self.orderId : self.orderBaseInfoModel.id;
        NSString *params = [NSString stringWithFormat:@"%@&id=%@", httpMsg.description, orderId];
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_UPDATE_ORDER];
        [self doSubmitRequest:params url:url];
    }
    else if ([requestObj isKindOfClass:[DispatchOrderHttpMessage class]]) {
        DispatchOrderHttpMessage *httpMsg = (DispatchOrderHttpMessage*)requestObj;
        NSString *params = httpMsg.description;
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_DISPATCH_API];
        [self doSubmitRequest:params url:url];
    }
    else if ([requestObj isKindOfClass:[VechileArrangeHttpMessage class]]) {
        VechileArrangeHttpMessage *httpMsg = (VechileArrangeHttpMessage*)requestObj;
        NSString *params = httpMsg.description;
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_ARRANGE_VECHILE_API];
        [self doSubmitRequest:params url:url];
    }
    else if ([requestObj isKindOfClass:[WayBillHttpMessage class]]) {
        
        // 判断是否为自建自派订单，如果是 判断当前订单是否填写了接单价格
        WeakSelf
        [self showHUDIndicatorViewAtCenter:MSG_DISPATCH_ORDER];
        NSString *orderId = _orderMode == 1 ? self.orderId : self.orderBaseInfoModel.id;
        [OrderHelper checkIsSelfDispatchOrder:orderId success:^(NSDictionary* responseData) {
            
            StrongSelf
            NSInteger code_status = [responseData[@"code"] integerValue];
            if (code_status == STATUS_OK) {
                
                NSInteger data = [responseData[@"data"] integerValue];
                if (data == 1) {
                    if (IsStrEmpty(strongSelf.orderBaseInfoModel.price) || [strongSelf.orderBaseInfoModel.price integerValue] == -1) {
                        
                        [strongSelf hideHUDIndicatorViewAtCenter];
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入接单价" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                        [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
                        UITextField *priceField = [alertView textFieldAtIndex:0];
                        priceField.placeholder = @"请输入接单价";
                        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                            if (buttonIndex == 1) {
                                UITextField *textField = [alertView textFieldAtIndex:0];
                                if (textField.text.trim > 0) {
                                    
                                    NewOrderHttpMessage *orderHttpMessage = [OrderHelper getOrderHttpMessage:strongSelf.orderBaseInfoModel price:textField.text.trim];
                                    [strongSelf onSubmit:orderHttpMessage];
                                    
                                    WayBillHttpMessage *httpMsg = (WayBillHttpMessage*)requestObj;
                                    NSString *params = httpMsg.description;
                                    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_SAVE_WAYBILL_API];
                                    [strongSelf doSubmitRequest:params url:url];
                                    
                                }
                                else{
                                    
                                    WayBillHttpMessage *httpMsg = (WayBillHttpMessage*)requestObj;
                                    NSString *params = httpMsg.description;
                                    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_SAVE_WAYBILL_API];
                                    [strongSelf doSubmitRequest:params url:url];
                                }
                            }
                        }];
                        
                    }
                    else {
                        WayBillHttpMessage *httpMsg = (WayBillHttpMessage*)requestObj;
                        NSString *params = httpMsg.description;
                        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_SAVE_WAYBILL_API];
                        [strongSelf doSubmitRequest:params url:url];
                    }
                }
                else {
                    WayBillHttpMessage *httpMsg = (WayBillHttpMessage*)requestObj;
                    NSString *params = httpMsg.description;
                    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_SAVE_WAYBILL_API];
                    [strongSelf doSubmitRequest:params url:url];
                }
            }
            else {
                [strongSelf hideHUDIndicatorViewAtCenter];
                [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
            }
            
            
        } andFailure:^(NSString *errorDesc) {
            WeakSelf
            [weakSelf showAlertView:errorDesc];
            [weakSelf hideHUDIndicatorViewAtCenter];
            
        }];
    }
    else if ([requestObj isKindOfClass:[SettlementHttpMessage class]]) {
        SettlementHttpMessage *httpMsg = (SettlementHttpMessage*)requestObj;
        NSString *params = httpMsg.description;
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_BARGAIN_API];
        [self doSubmitRequest:params url:url];
    }
    else if ([requestObj isKindOfClass:[AgreepayHttpMessage class]]) {
        AgreepayHttpMessage *httpMsg = (AgreepayHttpMessage*)requestObj;
        NSString *params = httpMsg.description;
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_AGREE_PAY_API];
        [self doSubmitRequest:params url:url];
    }
    else if ([requestObj isKindOfClass:[PayHttpMessage class]]) {
        PayHttpMessage *httpMsg = (PayHttpMessage*)requestObj;
        NSString *params = httpMsg.description;
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_PAY_API];
        [self doSubmitRequest:params url:url];
    }
}



-(void)doSubmitRequest:(NSString*)params url:(NSString*)url
{
    WeakSelf
    
    [self showHUDIndicatorViewAtCenter:MSG_DISPATCH_ORDER];
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (![responseData isKindOfClass:[NSDictionary class]]) {
            return;
        }
        
        id codeStatus = responseData[@"code"];
        if (IsNilOrNull(codeStatus)) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            id responseObject = responseData[@"data"];
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                NSDictionary *data = responseObject;
                
                if ([data[@"status"] integerValue] == 5) {
                    NSDictionary *vo = data[@"vo"];
                    strongSelf.barginAndPayModel = [[BarginAndPayResponseModel alloc] initWithDictionary:vo error:nil];
                    strongSelf.orderStatus = 5;
                    [strongSelf refreshView];
                }
                return;
            }
            
            NSArray *array = responseData[@"data"];
            
            NSInteger orderStatus;
            
            if (!IsArrEmpty(array)) {
                
                for (NSDictionary *dict in array) {
                    
                    NSUInteger status = [dict[@"status"] integerValue];
                    NSDictionary *vo = dict[@"vo"];
                    
                    if (status == 1) {
                        strongSelf.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusNew;
                    }
                    if (status == 2) {
                        strongSelf.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusDispatch;
                    }
                    if (status == 3) {
                        strongSelf.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:vo error:nil];
                        if (!IsStrEmpty(strongSelf.vechileArrangeModel.imgUrl)) {
                            [strongSelf downloadCarImage:self.vechileArrangeModel.imgUrl];
                        }
                        orderStatus = OrderStatusVechileArrange;
                    }
                    if (status == 4) {
                        strongSelf.wayBillModel = [[WayBillResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusWayBill;
                    }
                    if (status == 5) {
                        strongSelf.barginAndPayModel = [[BarginAndPayResponseModel alloc] initWithDictionary:vo error:nil];
                        orderStatus = OrderStatusBarginAndPay;
                    }
                }
            }
            strongSelf.headerStatuses = nil;
            strongSelf.orderStatus = orderStatus;
            [strongSelf refreshView];
        }
        else {
            if (code_status == 1485) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入接单价" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
                UITextField *priceField = [alertView textFieldAtIndex:0];
                priceField.placeholder = @"请输入接单价";
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 1) {
                        UITextField *textField = [alertView textFieldAtIndex:0];
                        if (textField.text.trim > 0) {
                            strongSelf.agreePayHttpMsg.dstPrice = textField.text.trim;
                            [strongSelf doEnvelopHttpRequestMsg];
                        }
                        else {
                            [strongSelf showAlertView:@"请输入接单价"];
                        }
                    }
                }];
            }
            else {
                [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
            }
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
    
}

#pragma mark - 查询是否有支付密码
-(void)checkHasPayPwd
{
    
    self.nextAction = @selector(checkHasPayPwd);
    self.object1 = nil;
    self.object2 = nil;
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_HAS_API];
    [self showHUDIndicatorViewAtCenter:@"正在加载中"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            //            if (!IsStrEmpty(responseData[@"data"])) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                // 已经设置支付密码发送支付请求 弹出支付密码输入窗口验证 验证OK再进行支付
                [self reCreatePassword];
            }
            else if([data integerValue] == 0){
                // 未设置支付密码
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请设置支付密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                WeakSelf
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        PasswordSetViewController *viewController = [[PasswordSetViewController alloc] init];
                        viewController.type = MimaTypeJiaoyi;
                        [weakSelf.navigationController pushViewController:viewController animated:YES];
                    }
                }];
            }
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];
}

#pragma mark - 弹出支付密码窗口
- (void)reCreatePassword {
    PasswordBuild * pwdCreate = [[PasswordBuild alloc] init];
    pwdCreate.pwdCount = 6;
    pwdCreate.pwdOperationType = PwdOperationTypeCreate;
    [pwdCreate show];
    
    __weak typeof(self)weakSelf = self;
    pwdCreate.PwdInit = ^(NSString *pwd){
        [weakSelf checkPayPwd:pwd];
    };
}

#pragma mark - 验证支付是否正确
-(void)checkPayPwd:(NSString*)pwd
{
    self.nextAction = @selector(checkPayPwd:);
    self.object1 = pwd;
    self.object2 = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, PAY_PWD_VALID_API];
    NSString *param = [NSString stringWithFormat:@"password=%@", [EncryptUtil getRSAEncryptPassword:pwd]];
    [self showHUDIndicatorViewAtCenter:@"正在校验中"];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSString *data = responseData[@"data"];
            if ([data integerValue] == 1) {
                if (strongSelf.payHttpMessage.rateId == nil) {
                    [strongSelf showAlertView:ALERT_MSG_RATE_ID_NULL];
                    return;
                }
                NSString *orderId = _orderMode == 1 ? strongSelf.orderId : strongSelf.orderBaseInfoModel.id;
                strongSelf.payHttpMessage.orderId = orderId;
                strongSelf.payHttpMessage.bargainId = strongSelf.barginAndPayModel.pay.id;
                [strongSelf onSubmit:strongSelf.payHttpMessage];
            }
            else if([data integerValue] == 0){
                // 校验支付密码失败
                [strongSelf showAlertView:@"校验支付密码失败"];
            }
            //            }
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
    }];
}

#pragma mark - 查询
- (void)onQueryDetail:(NSNumber*)section;
{
    
    self.nextAction = @selector(onQueryDetail:);
    self.object1 = section;//[NSNumber numberWithInteger:section];
    self.object2 = nil;
    
    NSString *params = [NSString stringWithFormat:@"orderId=%@", self.orderId];
    NSArray *urls = @[ORDER_DETAIL_BASE_API, ORDER_DETAIL_CHAIN_API, ORDER_DETAIL_CARINFO_API, ORDER_DETAIL_WAYBILL_API, ORDER_DETAIL_BARGIN_API];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, urls[[section integerValue]]];
    
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSDictionary *dict = responseData[@"data"];
            NSInteger status = [dict[@"status"] integerValue];
            NSDictionary *vo = dict[@"vo"];
            
            if (status == 1) {
                strongSelf.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:vo error:nil];
            }
            if (status == 2) {
                strongSelf.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
            }
            if (status == 3) {
                strongSelf.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:vo error:nil];
                if (!IsStrEmpty(self.vechileArrangeModel.imgUrl)) {
                    [strongSelf downloadCarImage:strongSelf.vechileArrangeModel.imgUrl];
                }
            }
            if (status == 4) {
                strongSelf.wayBillModel = [[WayBillResponseModel alloc] initWithDictionary:vo error:nil];
            }
            if (status == 5) {
                strongSelf.barginAndPayModel = [[BarginAndPayResponseModel alloc] initWithDictionary:vo error:nil];
            }
            OrderStatusHeader *header = strongSelf.headerStatuses[[section integerValue]];
            header.isOpen = YES;
            [strongSelf refreshView];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

#pragma mark - 修改订单基本信息
-(void)onBaseInfoUpdateBtnClik
{
    NewOrderViewController *viewController = [[NewOrderViewController alloc] init];
    viewController.mode = 1;
    viewController.orderId = self.orderBaseInfoModel.id.trim;
    viewController.delegate = self;
    viewController.orderBaseModel = self.orderBaseInfoModel;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)onUpdateOrderInfo:(NSDictionary*)dict;
{
    self.orderBaseInfoModel = [[OrderBaseInfoResponseModel alloc] initWithDictionary:dict error:nil];
    //    if (self.dispatchPriceModel != nil) {
    //        self.dispatchPriceModel.srcPrice = self.orderBaseInfoModel.price.trim;
    //    }
    [self refreshView];
    //    [self.orderTableView reloadData];
}

#pragma mark - 同意改派订单

- (void)onAgreeRollback
{
    self.nextAction = @selector(onAgreeRollback);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *orderId = (self.orderBaseInfoModel.id == nil)?self.orderId:self.orderBaseInfoModel.id;
    NSString *param = [NSString stringWithFormat:@"orderId=%@", orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_AGREE_ROLLBACK_CHAIN_API];
    
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(id responseData) {
        
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"同意改派订单成功!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
                
            }];
            
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
    
}

#pragma mark - 派单单位改派
-(void)onDispatchRollbackBtnClick
{
    
    self.nextAction = @selector(onDispatchRollbackBtnClick);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *orderId = (self.orderBaseInfoModel.id == nil) ? self.orderId : self.orderBaseInfoModel.id;
    NSString *param = [NSString stringWithFormat:@"orderId=%@", orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_ROLLBACK_CHAIN];
    
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            NSInteger status = [data[@"status"] integerValue];
            NSDictionary *vo = data[@"vo"];
            if (status == 2)
                strongSelf.dispatchPriceModel = [[DispatchPriceResponseModel alloc] initWithDictionary:vo error:nil];
            strongSelf.orderStatus = OrderStatusDispatch;
            [strongSelf refreshView];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
    
}

#pragma mark - 车辆改派
-(void)onCarRollbackBtnClick
{
    
    self.nextAction = @selector(onCarRollbackBtnClick);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *orderId = (self.orderBaseInfoModel.id == nil) ? self.orderId : self.orderBaseInfoModel.id;
    NSString *param = [NSString stringWithFormat:@"orderId=%@&id=%@", orderId, self.vechileArrangeModel.id];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_ROLLBACK_CARINFO];
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(id responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            NSInteger status = [data[@"status"] integerValue];
            NSDictionary *vo = data[@"vo"];
            if (status == 3)
            {
                strongSelf.vechileArrangeModel = [[VechileArrangeResponseModel alloc] initWithDictionary:vo error:nil];
                if (!IsStrEmpty(strongSelf.vechileArrangeModel.imgUrl)) {
                    [strongSelf downloadCarImage:strongSelf.vechileArrangeModel.imgUrl];
                }
                strongSelf.orderStatus = OrderStatusVechileArrange;
            }
            
            [strongSelf refreshView];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

#pragma mark - 修改路单

-(void)onWayBillRollback
{
    
}

#pragma mark -
-(void)onBarginChanged
{
    self.nextAction = @selector(onBarginChanged);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *orderId = (self.orderBaseInfoModel.id == nil) ? self.orderId : self.orderBaseInfoModel.id;
    NSString *param = [NSString stringWithFormat:@"orderId=%@", orderId];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, ORDER_ROLLBACK_BARGIN];
    
    [self showHUDIndicatorViewAtCenter:MSG_LOADING];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:param success:^(NSDictionary *responseData) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (IsNilOrNull(responseData[@"code"])) {
            return;
        }
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
            NSDictionary *vo = data[@"vo"];
            strongSelf.barginAndPayModel.bargain = [[BarginModel alloc] initWithDictionary:vo error:nil];
            strongSelf.orderStatus = OrderStatusBarginAndPay;
            [strongSelf refreshView];
        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
}

#pragma mark - goto 充值界面

- (void)gotoRechargePage:(id)sender
{
    [self gotoPage:@"NewRechargeViewController"];
    
}

#pragma mark - 刷新视图
-(void)refreshView
{
    [self initRightBarButtonItem];
    [self.orderTableView reloadData];
}


#pragma mark - receive memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
