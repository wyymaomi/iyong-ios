//
//  BindDriverViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface BindDriverViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@end
