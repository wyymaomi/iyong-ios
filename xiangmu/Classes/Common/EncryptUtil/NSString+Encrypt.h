//
//  NSString+Encrypt.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encrypt)

- (NSString*) sha1;
- (NSString*) md5;
- (NSString*) desEncrypt:(NSString *)key;
- (NSString *) decryptUseDES:(NSString*)key;

@end
