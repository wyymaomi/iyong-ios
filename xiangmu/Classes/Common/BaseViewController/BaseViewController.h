//
//  BaseViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRefreshTableView.h"
#import "HeaderPullRefreshTableView.h"
#import "TPKeyboardAvoidingTableView.h"
#import "UIScrollView+EmptyDataSet.h"
//#import "YYFPSLabel.h"


typedef NS_OPTIONS(NSUInteger, ENTER_TYPE) {
    ENTER_TYPE_PRESENT = 0,
    ENTER_TYPE_PUSH = 1
};

typedef NS_ENUM(NSUInteger, LoadStatus) {
    LoadStatusInit = 0,
    LoadStatusLoading,
    LoadStatusFinish,
    LoadStatusNetworkError
};

@protocol EmptyDataDelegate <NSObject>
@optional
- (UIImage*)emptyIconImage;
- (NSString*)emptyTitleText;
- (NSString*)emptyDetailText;
- (void)onButtonClick;


@end

@protocol PayDelegate <NSObject>

-(void)onPaySuccess;

@end

@protocol LoginDelegate <NSObject>

-(void)onLoginSuccess;
@optional
-(void)onGestureLoginSuccss;

@end

typedef NS_ENUM(NSUInteger, AdvDisplayType)
{
    AdvDisplayTypeInit, // 初始化
    AdvDisplayTypePause, // 广告暂停显示
    AdvDisplayTypeInDisplay// 广告显示中
};



@interface BaseViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, PullRefreshTableViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (nonatomic, weak) id<LoginDelegate> loginDelegate;
@property (nonatomic, weak) id<PayDelegate> payDelegate;

//@property (nonatomic, assign) BOOL showAdv; // 是否显示广告界面
@property (nonatomic, assign) AdvDisplayType advDisplayType; 

@property (nonatomic, strong) NSMutableArray *dataList;
@property (nonatomic, strong) TPKeyboardAvoidingTableView *tpkeyboardAvoidingPlainTableView;
@property (nonatomic, strong) TPKeyboardAvoidingTableView *tpkeyboardAvoidingGroupTableView;
@property (nonatomic, strong) PullRefreshTableView *pullRefreshTableView;
@property (nonatomic, strong) HeaderPullRefreshTableView *headerPullRefreshTableView;
@property (nonatomic, strong) UITableView *plainTableView;
@property (nonatomic, strong) UITableView *groupTableView;
@property (nonatomic, assign) NSInteger loadStatus;// 加载状态
@property (nonatomic, weak) id<EmptyDataDelegate> emptyDataDelegate;

@property (nonatomic, assign) NSInteger source_from;
@property (nonatomic, assign) BOOL hasLeftBackButton;
@property (assign, nonatomic) ENTER_TYPE enter_type;// 进入方式
@property (nonatomic, assign) BOOL flag;
//@property (nonatomic, assign) NSInteger receiveAuthMessageCount;// 收到登录消息次数
//@property (nonatomic, assign) BOOL bPopupLoginView;

@property (nonatomic, strong) NSArray *codeMsgList;

//@property (nonatomic, strong) GestureLoginViewController *gestureLoginViewController;
//@property (nonatomic, strong) LoginViewController *loginViewController;

-(NSString*)getErrorMsg:(NSInteger)code;

-(void)pressedBackButton;

//- (void)showLoginAlertView;
-(BOOL)needLogin;
-(void)onLoginSuccess;

-(void)onRecevieLoginOvertime;
-(void)onReceiveAuthNotification;

@property (nonatomic, strong) UIViewController *nextController;// 登录后要跳转的页面
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, assign) SEL nextAction;
@property (nonatomic, strong) id object1;
@property (nonatomic, strong) id object2;

- (void)saveNextAction:(SEL)selector object1:(id)object1 object2:(id)object2;
-(void)resetAction;

-(void)setRightNavigationItemWithTitle:(NSString*)title selMethod:(SEL)selMethod;
-(void)setRightNavigationItemWithImage:(UIImage*)image selMehtod:(SEL)selMethod;

//@property (nonatomic, strong) YYFPSLabel *fpsLabel;

@end
