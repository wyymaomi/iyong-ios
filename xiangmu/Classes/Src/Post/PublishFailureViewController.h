//
//  PublishFailureViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/7.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface PublishFailureViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end
