//
//  ProviderInfoViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/18.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "ProviderInfoViewController+NetworkRequest.h"
#import "AdPayViewController.h"

#import "AlipayOrder.h"
#import "SDKManager.h"
#import "SDKRegisterService.h"
#import "SDKPayService.h"
#import "WXApiObject.h"
#import "WXApi.h"

@implementation ProviderInfoViewController (ProviderInfoNetworkRequest)

-(void)getFinanceInfo
{
    self.nextAction = @selector(getFinanceInfo);
    self.object1 = nil;
    self.object2 = nil;
    
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_FINANCE_API];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:@"" success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf resetAction];
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSDictionary *data = responseData[@"data"];
//            strongSelf.promotionAccountMoney = responseData[@"]
            strongSelf.myAccountMoney = data[@"accountAmount"];
            strongSelf.promotionAccountMoney = data[@"extensionAmount"];
            
            [strongSelf.groupTableView reloadData];
            
            
        }
        else {
        }
        
    } andFailure:^(NSString *errorDesc) {
        [weakSelf resetAction];
        DLog(@"response failure");
    }];
    
}

-(void)fetchTodayBalance
{
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(SUM_CURRENT_PAYMENT_API) requestMethod:POST params:nil success:^(id responseData) {
        StrongSelf
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            NSNumber *balance = responseData[@"data"];
            strongSelf.currentPomotionAccountMoney = balance;
            [strongSelf.groupTableView reloadData];
//            [strongSelf.groupTableView reloadSections:[NSIndexSet indexSetWithIndex:ProviderInfoSectionBalance] withRowAnimation:UITableViewRowAnimationNone];
        }
        else {
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

- (void)fetchProviderRankList
{
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(COMPANY_RAKING_API) requestMethod:POST params:nil success:^(id responseData) {
        
        StrongSelf
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            strongSelf.viewModel.rankSumModel = [[ProviderRankSumModel alloc] initWithDictionary:responseData[@"data"] error:nil];
            [strongSelf.groupTableView reloadData];
        }
        else {
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
        
    }];
}

- (void)fetchProviderCurrentAdvRevenue
{
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_AD_CURRENT_API) requestMethod:POST params:nil success:^(id responseData) {
        
        StrongSelf
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            CurrentAdvRevenue *advRevenue = [[CurrentAdvRevenue alloc] initWithDictionary:responseData[@"data"] error:nil];
            strongSelf.viewModel.currentAdvRevenue = advRevenue;
            [strongSelf.groupTableView reloadData];
        
        }
        else {
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
}

#pragma mark - fetchProviderAdOrderListWithDetail
- (void)fetchProviderAdOrderListWithDetail
{

    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_AD_ORDER_LIST_API) requestMethod:POST params:nil success:^(id responseData) {
        
        StrongSelf
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [strongSelf.viewModel.orderList removeAllObjects];
                
                NSArray *data = responseData[@"data"];
                
                for (NSDictionary *dict in data) {
                    
                    NSError *error;
                    ProviderOrderModel *order = [[ProviderOrderModel alloc] initWithDictionary:dict error:&error];
                    if (!error) {
                        [strongSelf.viewModel.orderList addObject:order];
                    }
                    
                }
                
                dispatch_main_async_safe(^{
                    
                    [strongSelf.groupTableView reloadData];
                    
                });
                
            });
            
            
        }
        else {
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
        
    }];
}

-(void)continuePayOrder:(UIButton*)button
{
    ProviderOrderModel *model = self.viewModel.orderList[button.tag];
    [self submitPay:model.id payMode:[model.payMode integerValue]];
#if 0
    ProviderOrderModel *model = self.viewModel.orderList[button.tag];
    AdPayViewController *viewController = [[AdPayViewController alloc] init];
    viewController.httpMessage.price = [NSNumber numberWithInteger:[model.price integerValue]];
//    viewController.httpMessage.adMainIds =
    NSMutableArray *adIdList = [NSMutableArray new];
    for (ProviderOrderDetailModel *detailModel in model.details) {
        [adIdList addObject:detailModel.adMainId];
    }
    viewController.httpMessage.adMainIds = [adIdList componentsJoinedByString:@","];
    viewController.adOrderId = model.id;
    
    [self.navigationController pushViewController:viewController animated:YES];
#endif
}

-(void)cancelOrder:(UIButton*)button
{
    ProviderOrderModel *model = self.viewModel.orderList[button.tag];
    [self requestCancelOrder:model.id];
}

-(void)deleteOrder:(id)sender
{
    UIButton *button = (UIButton*)sender;
    ProviderOrderModel *model = self.viewModel.orderList[button.tag];
    [self requestDeleteOrder:model.id];
}

-(void)requestDeleteOrder:(NSString*)adOrderId
{
    NSString *params = [NSString stringWithFormat:@"adOrderId=%@", adOrderId];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_DELETE_AD_ORDER_API) requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        
        NSUInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            [MsgToolBox showToast:@"删除订单成功"];
            
            [strongSelf fetchProviderAdOrderListWithDetail];
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
        
    }];
}

- (void)requestCancelOrder:(NSString*)adOrderId
{
    NSString *params = [NSString stringWithFormat:@"adOrderId=%@", adOrderId];
    [self showHUDIndicatorViewAtCenter:@"正在取消订单，请稍候...."];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_CANCEL_AD_ORDER_API) requestMethod:POST params:params success:^(id responseData) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
       
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            [MsgToolBox showToast:@"取消订单成功"];
            
            [strongSelf fetchProviderAdOrderListWithDetail];
            
        }
        else {
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [MsgToolBox showToast:errorDesc];
        
    }];
}

- (void)submitPay:(NSString*)adOrderId payMode:(NSUInteger)payMode
{
    NSString *params = [NSString stringWithFormat:@"orderId=%@", adOrderId];
    //    NSString *params = [NSString stringWithFormat:@"%@&orderId=%@", self.httpMessage.description, adOrderId];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PROVIDER_PAY_ORDER_API) requestMethod:POST params:params success:^(id responseData) {
        StrongSelf
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            
            if (payMode == AdPayTypeAlipay) {
                NSString *data = responseData[@"data"];
                if (!IsStrEmpty(data)) {
                    [strongSelf doAlipayPay:data];
                }
                
            }
            else if (payMode == AdPayTypeWechat) {
                
                NSDictionary *data = responseData[@"data"];
                if (data != nil) {
                    [strongSelf bizPay:data];
                }
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"支付成功，请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                    
                    if (buttonIndex == 0) {
                        [strongSelf fetchProviderAdOrderListWithDetail];
//                        [strongSelf.navigationController popToRootViewControllerAnimated:YES];
                    }
                    
                }];
            }
            
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        
        DLog(@"errorDesc = %@", errorDesc);
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        [strongSelf resetAction];
        
    }];
}

//
//选中商品调用支付宝极简支付
//
- (void)doAlipayPay:(NSString*)encryptOrderString
{
    
    //    NSDictionary *dict = @{kTradeNo:@"1234567890",//[self generateTradeNO],
    //                           kRechargeAmount:self.transAmountTextField.text.trim,
    //                           kPayTime:[[NSDate date] stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"]};
    //    id<PayModelAdapterProtocol> alipay = [[AlipayOrder alloc] init];
    
    [[SDKManager getPayService:SDKPlatformAlipay] payOrder:/*[alipay generateOrderString:dict]*/encryptOrderString callBack:^(NSString *signString, NSError *error) {
        DLog(@"signString = %@", signString);
        //        NSInteger resultStatus = [signString integerValue];
        //        if (resultStatus == 6001) {
        //            [MsgToolBox showAlert:@"" content:@"用户取消"];
        ////                        [self showTipMessage:@"用户取消"];
        //            //            [self ]
        //        }
        //        else if (resultStatus == 6002){
        //            [MsgToolBox showAlert:@"" content:@"网络连接错误"];
        ////                        [self showTipMessage:@"网络连接错误"];
        //        }
        //        else if (resultStatus == 4000){
        //            [MsgToolBox showAlert:@"" content:@"订单支付失败"];
        //            //            [self showTipMessage:@"订单支付失败"];
        //        }
        //        else if (resultStatus == 8000){
        //            [MsgToolBox showAlert:@"" content:@"正在处理中"];
        //            //            [self showTipMessage:@"正在处理中"];
        //        }
        //        else if (resultStatus == 9000){
        //            [MsgToolBox showAlert:@"" content:@"订单支付成功"];
        //            //            [self showTipMessage:@"订单支付成功"];
        //        }
        
    }];
    
}

#pragma mark - 微信支付

- (void)bizPay:(NSDictionary*)dict
{
    //    NSString *res = [WXApiRequestHandler jumpToBizPay];
    //    if( ![@"" isEqual:res] ){
    //        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"支付失败" message:res delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //
    //        [alter show];
    ////        [alter release];
    //    }
    
    NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
    
    //调起微信支付
    PayReq *req             = [[PayReq alloc] init];
    req.partnerId           = dict[@"partnerid"];
    req.prepayId            = [dict objectForKey:@"prepayid"];
    req.nonceStr            = [dict objectForKey:@"noncestr"];
    req.timeStamp           = stamp.intValue;
    req.package             = [dict objectForKey:@"package"];
    req.sign                = [dict objectForKey:@"sign"];
    [WXApi sendReq:req];
    
}




@end
