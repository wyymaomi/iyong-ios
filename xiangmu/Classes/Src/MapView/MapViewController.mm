//
//  MapViewViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MapViewController.h"
#import "AddressTableView.h"
#import "BottomBarView.h"
#import "CustomActionSheetDatePicker.h"
#import "LocationSearchViewController.h"
#import "VechileOrderHttpMessage.h"
#import "VechileTypeSelectViewController.h"
#import "MyUtil.h"
#import "DVSwitch.h"
#import "InputHelperViewController.h"
#import "BaiduTraceSDK/BaiduTraceSDK.h"



typedef NS_ENUM(NSUInteger, MapMode)
{
    MapModeInit = 0,
    MapModeDriverPath
};



@interface MapViewController ()<BMKMapViewDelegate,BMKLocationServiceDelegate,BMKGeoCodeSearchDelegate,AddressTableViewDelegate,VechileTypeSelectDelgate,BMKRouteSearchDelegate,InputHelperDelegate,BTKTraceDelegate,BTKEntityDelegate>
{
    BMKMapView *_mapView;
    BMKPointAnnotation *_annotation;
//    LocationAnnotationView *_locationAnnotationView;
    BMKLocationService *_locService;
    BMKGeoCodeSearch *_geocodeSearch;
    CLLocationCoordinate2D _currentCoordinate;
    
    // 第一次定位标记
    BOOL isFirstLocated;
    
    // 地图中心点标记
    UIImageView *_centerMaker;
    UIImage *_imageLocated;
    UIImage *_imageNotLocate;
    
    BMKRouteSearch* _routesearch;
    NSString *_startCityText;
    NSString *_startAddrText;
    NSString *_endCityText;
    NSString *_endAddrText;
    
}

@property (nonatomic, strong) AddressTableView *tableView;
@property (nonatomic, strong) BottomBarView *bottomBarView;
@property (nonatomic, strong) UILabel *startLocationTipLabel; // 我在这里上车
@property (nonatomic, strong) VechileOrderHttpMessage *vechileOrderHttpMessage;
@property (nonatomic, assign) MapMode mapMode;

@property (nonatomic, strong) DVSwitch *switchControl;

//@property (nonatomic, assign) NSUInteger timeType;//用车时间类型



@end

@implementation MapViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"爱佣出行";

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"价格说明" style:(UIBarButtonItemStylePlain) target:self action:@selector(gotoPriceExplainWebpage)];
    
    [self initMapView];
    [self initLocationService];
    [self initCenterMarker];
//    [self initLocationService];
//    [self initGeocodeSearch];
    [self initTableView];
    [self initBottomBar];

    
    [self initRouteSearch];
    
    self.vechileOrderHttpMessage = self.tableView.vechileOrderHttpMessage;
    
    [self addObserver:self forKeyPath:@"mapMode" options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"vechileOrderHttpMessage.useCarMode" options:NSKeyValueObservingOptionNew context:NULL];
//    [self addObserver:self forKeyPath:@"vechileOrderHttpMessage.carType" options:NSKeyValueObservingOptionNew context:NULL]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveCancelOrderMessage:) name:kOnCancelCarOrderSuccessNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initTimer) name:kUpdateUserLocationNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopTimer) name:kStopRefreshTimerNotification object:nil];
    
//    [self initTimer];
    
//    [self initData];

}

-(void)initTimer
{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:TIMER_INTERVAL target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
}

-(void)onTimer
{
    if (self.mapMode == MapModeInit) {
        self.pageIndex = 1;
        [self.carList removeAllObjects];
//        [_mapView removeAnnotations:_mapView.annotations];
//        [_mapView removeOverlays:_mapView.overlays];
        [self searchNearbyCar];
    }
}

-(void)stopTimer
{
    if (_timer) {
        [_timer invalidate];
//        [[NSRunLoop currentRunLoop] cancelTimer];
        _timer = nil;
    }
}

-(void)refreshBottombarStatus
{
    
    NSString *carDateTime = self.vechileOrderHttpMessage.carDateTime;
    NSString *startAddress = self.vechileOrderHttpMessage.startLocation;
    NSString *endAddress = self.vechileOrderHttpMessage.endLocation;
 

    
    // 计算用车价格
    // 车型价格接口新增起步价和起步里程，订单金额=起步价+(超出起步里程*单价)
    NSInteger carTravelDistance = self.vechileOrderHttpMessage.distance;
    NSInteger carUnitPrice = self.tableView.vechileOrderHttpMessage.carUnitPrice;
    NSInteger carStartPrice = self.tableView.vechileOrderHttpMessage.carStartPrice;
    NSInteger carStartDistance = self.tableView.vechileOrderHttpMessage.carStartDistance;
    NSInteger distanceAfterStart = carTravelDistance - carStartDistance;
    if (carTravelDistance == 0) {
        self.vechileOrderHttpMessage.carPrice = 0;
    }
    else {
        if (distanceAfterStart <= 0) {
            self.vechileOrderHttpMessage.carPrice = carStartPrice;
        }
        else {
            self.vechileOrderHttpMessage.carPrice = carStartPrice + carUnitPrice * distanceAfterStart;
        }
    }
    
    
    if (self.vechileOrderHttpMessage.carPrice > 0) {
        NSString *priceText = [NSString stringWithFormat:@"%.f元", _vechileOrderHttpMessage.carPrice];
        NSString *distanceText = [NSString stringWithFormat:@"(%.f公里)", _vechileOrderHttpMessage.distance];
        //    self.priceLabel.text = [NSString stringWithFormat:@"%@ %@", priceText, distanceText];
        NSString *priceLabelText = [NSString stringWithFormat:@"%@ %@", priceText, distanceText];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:priceLabelText];
        [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0xe06430),
                                          NSFontAttributeName:FONT(12)}
                                  range:[priceLabelText rangeOfString:@"元"]];
        [attributedString addAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(0x999999),
                                          NSFontAttributeName:FONT(16)}
                                  range:[priceLabelText rangeOfString:distanceText]];
        self.bottomBarView.priceLabel.attributedText = attributedString;
    }
    else {
        self.bottomBarView.priceLabel.text = @"";
    }

    if (self.vechileOrderHttpMessage.useCarMode == CarModeImmediate) {
        if (IsStrEmpty(startAddress) || IsStrEmpty(endAddress)) {
            [self.bottomBarView setEnabled:NO];
        }
        else if (self.vechileOrderHttpMessage.carPrice > 0) {
            [self.bottomBarView setEnabled:YES];
        }
        else {
            [self.bottomBarView setEnabled:NO];
        }
    }
    else if (self.vechileOrderHttpMessage.useCarMode == CarModeBooking){
        if (IsStrEmpty(carDateTime) || IsStrEmpty(startAddress) || IsStrEmpty(endAddress)) {
            [self.bottomBarView setEnabled:NO];
        }
        else if (self.vechileOrderHttpMessage.carPrice > 0) {
            [self.bottomBarView setEnabled:YES];
        }
        else {
            [self.bottomBarView setEnabled:NO];
        }
    }
    
    [self.bottomBarView setNeedsLayout];
    

    
}

-(void)pressedBackButton
{
    if (self.mapMode == MapModeDriverPath) {
        self.mapMode = MapModeInit;
    }
    else {
        [super pressedBackButton];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    _bottomBarView.frame = CGRectMake(0, self.view.frame.size.height-50*Scale, ViewWidth, 50*Scale);
    _bottomBarView.orderButton.frame = CGRectMake(ViewWidth-120*Scale, 0, 120*Scale, 50*Scale);
    
    if (self.vechileOrderHttpMessage.useCarMode == CarModeImmediate) {
        self.tableView.frame = CGRectMake(12*Scale, self.view.height-225*Scale, ViewWidth-24*Scale, 160*Scale);
    }
    else {
        self.tableView.frame = CGRectMake(12*Scale, self.view.height-265*Scale, ViewWidth-24*Scale, 200*Scale);
    }
    
    _switchControl.frame = CGRectMake(12*Scale, self.tableView.top-35*Scale, 95*Scale, 30*Scale);
    
    
    _centerMaker.frame = CGRectMake((ViewWidth-14*Scale)/2, (ViewHeight-25*Scale)/2-25*Scale-44*Scale, 14*Scale, 25*Scale);
    self.startLocationTipLabel.frame = CGRectMake((ViewWidth-120*Scale)/2, _centerMaker.top-35*Scale, 120*Scale, 35*Scale);
    
    _mapView.frame = CGRectMake(0, 0, ViewWidth, self.view.height-_bottomBarView.height);
    if (self.mapMode == MapModeDriverPath) {
        _mapView.frame = CGRectMake(0, -44*Scale-20*Scale, ViewWidth, self.view.height-_bottomBarView.height);
    }
    DLog(@"mapView.frame = %.f,%.f,%.f,%.f", _mapView.frame.origin.x, _mapView.frame.origin.y, _mapView.frame.size.width, _mapView.frame.size.height);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _mapView.delegate = self;
    
    _routesearch.delegate = self;
    
    if (self.mapMode == MapModeInit) {
        [self initTimer];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    _mapView.delegate = nil;
    
    _routesearch.delegate = nil;
    
    [self stopTimer];
    
//    self removeObserver:self forKeyPath:@"vechile"
}

-(void)dealloc
{
//    [self stopTimer];
    
    if (_mapView) {
        _mapView = nil;
    }
    [self removeObserver:self forKeyPath:@"mapMode"];
    [self removeObserver:self forKeyPath:@"vechileOrderHttpMessage.useCarMode"];
    
//    if (_timer) {
//        [_timer invalidate];
//        _timer = nil;
//    }
    

    
}

#pragma mark - KVO register method

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"vechileOrderHttpMessage.useCarMode"]) {
        
        [self getCurrentCarDetail:self.vechileOrderHttpMessage.useCarMode carType:self.vechileOrderHttpMessage.carType];
        [self startCarRouteSearch];
        [self.tableView reloadData];
        [self.view setNeedsLayout];
        
        return;
    }
    
    if ([keyPath isEqualToString:@"mapMode"]) {
        
        switch (self.mapMode) {
            case MapModeInit:
            {
                self.title = @"爱佣出行";
                
                NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
                [_mapView removeAnnotations:array];
                array = [NSArray arrayWithArray:_mapView.overlays];
                [_mapView removeOverlays:array];
                
                _mapView.top = 0;
                [_mapView setCenterCoordinate:_currentCoordinate];
                _mapView.zoomLevel = 19;
                
                _centerMaker.hidden = NO;
                self.startLocationTipLabel.hidden = NO;
                
            }
                
                break;
            case MapModeDriverPath:
                self.title = @"确认预约";
                _centerMaker.hidden = YES;
                self.startLocationTipLabel.hidden = YES;
                [self.view setNeedsLayout];
                
            default:
                break;
        }
    }
}




#pragma mark - 

-(void)initBottomBar
{
    _bottomBarView = [BottomBarView customView];
    [self.view addSubview:_bottomBarView];
    
    [_bottomBarView setEnabled:NO];
    
    _bottomBarView.priceLabel.text = nil;
    
    [_bottomBarView.orderButton addTarget:self action:@selector(onBooking) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITableView

-(void)initTableView
{
    _tableView = [[AddressTableView alloc] initWithFrame:CGRectMake(12, self.view.frame.size.height-225, ViewWidth-24, 160) style:UITableViewStylePlain];
    _tableView.cornerRadius = 10;
    _tableView.customDelegate = self;
    [self.view addSubview:_tableView];
    
    // 预约类型选择组件 用车预约类型：现在／预约
    _switchControl = [[DVSwitch alloc] initWithStringsArray:@[@"现在", @"预约"]];
    _switchControl.frame = CGRectMake(12, 0, 95, 30);
    _switchControl.cornerRadius = 18;
    _switchControl.backgroundColor = [UIColor whiteColor];
    _switchControl.font = FONT(15);
    _switchControl.sliderColor = [UIColor clearColor];
    _switchControl.labelTextColorInsideSlider = UIColorFromRGB(0x313131);
    _switchControl.labelTextColorOutsideSlider = UIColorFromRGB(0x999999);
    _switchControl.sliderBorderColor = UIColorFromRGB(0xEBEBEB);
    _switchControl.sliderBorderWidth = 0.5f;
    [self.view addSubview:_switchControl];
    WeakSelf
    [_switchControl setPressedHandler:^(NSUInteger index) {
        if (index == 0) {
            weakSelf.vechileOrderHttpMessage.useCarMode = CarModeImmediate;
        }
        else if (index == 1) {
            weakSelf.vechileOrderHttpMessage.useCarMode = CarModeBooking;
        }
        DLog(@"Did switch to index: %lu", (unsigned long)index);
        
    }];
}

#pragma mark - MapView 初始化
-(void)initMapView
{
    _mapView = [[BMKMapView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_mapView];
    
    _mapView.delegate = self;
    _mapView.userTrackingMode = BMKUserTrackingModeFollow;
    _mapView.showsUserLocation = YES;
    _mapView.zoomLevel = 19;
}

#pragma mark - 初始化标注
-(void)initCenterMarker
{
    UIImage *image = [UIImage imageNamed:@"icon_center_marker"];
    _centerMaker = [[UIImageView alloc] initWithImage:image];
    _centerMaker.frame = CGRectMake(self.view.frame.size.width/2-image.size.width/2, ViewHeight/2-image.size.height, image.size.width, image.size.height);
    _centerMaker.center = CGPointMake(self.view.frame.size.width / 2, (ViewHeight -  _centerMaker.frame.size.height) * 0.5);
    [self.view addSubview:_centerMaker];
    
    [self.view addSubview:self.startLocationTipLabel];
    
}

-(UILabel*)startLocationTipLabel
{
    if (!_startLocationTipLabel) {
        _startLocationTipLabel = [[UILabel alloc] init];
        _startLocationTipLabel.backgroundColor = [UIColor whiteColor];
        _startLocationTipLabel.text = @"我在这里上车";
        _startLocationTipLabel.textColor = UIColorFromRGB(0x999999);
        _startLocationTipLabel.textAlignment = UITextAlignmentCenter;
        _startLocationTipLabel.font = FONT(14);
        _startLocationTipLabel.cornerRadius = 18;
    }
    return _startLocationTipLabel;
}

#pragma mark - 初始化定位

-(void)initLocationService
{
    //初始化BMKLocationService
    //    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    _locService = [[BMKLocationService alloc]init];
    //    _locService.delegate = self;
//    [_locService setAllowsBackgroundLocationUpdates:YES];
    //启动LocationService
    [_locService startUserLocationService];
    //    });
    _locService.delegate = self;
}

#pragma mark - 初始化搜索

-(void)initGeocodeSearch
{
    _geocodeSearch = [[BMKGeoCodeSearch alloc]init];
    BMKGeoCodeSearchOption *geoCodeSearchOption = [[BMKGeoCodeSearchOption alloc] init];
    geoCodeSearchOption.city = @"北京";
    geoCodeSearchOption.address = @"海淀区上地10街10号";
    BOOL flag = [_geocodeSearch geoCode:geoCodeSearchOption];
    if (flag) {
        
    }
    else {
        
    }
}



#pragma mark - 根据当前的经纬度请求地址
-(void)queryCurrentAddress
{
    if (!_geocodeSearch) {
        _geocodeSearch = [[BMKGeoCodeSearch alloc] init];
    }
    BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
//    reverseGeocodeSearchOption.reverseGeoPoint = _currentCoordinate;
    reverseGeocodeSearchOption.reverseGeoPoint = _mapView.centerCoordinate;
    BOOL flag = [_geocodeSearch reverseGeoCode:reverseGeocodeSearchOption];
    _geocodeSearch.delegate = self;
    if(flag)
    {
//        self.queryAddressType = queryAddressType;
        //#if DEBUG
        //        [MsgToolBox showToast:@"反geo检索发送成功"];
        //#endif
        DLog(@"反geo检索发送成功");
    }
    else
    {
#if DEBUG
        [MsgToolBox showToast:@"反geo检索发送失败"];
#endif
        DLog(@"反geo检索发送失败");
    }
    
}

#pragma mark - 接收反向地理编码结果

-(void) onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result
                        errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        //      在此处理正常结果
        if (result) {
            
            NSString *startAddress = result.address;
            
            BMKPoiInfo *point;
            if (result.poiList.count > 0) {
                point = result.poiList[0];
                
                if (!IsStrEmpty(point.address.trim)) {
                    startAddress = point.address;
                }
            }
            
#if DEBUG
            DLog(@"address = %@, sematicDescription = %@", result.address, result.sematicDescription);
            DLog(@"point.name = %@,point.address=%@", point.name,point.address);
            DLog(@"cityCode = %@", result.addressDetail.city);
#endif

            self.tableView.vechileOrderHttpMessage.startLocation = point.name;
            self.tableView.vechileOrderHttpMessage.startAddress = startAddress;
            self.tableView.vechileOrderHttpMessage.startCoordiante = point.pt;
            self.tableView.vechileOrderHttpMessage.startCity = point.city;
            self.tableView.vechileOrderHttpMessage.startCoordiante = result.location;
            
            NSString *city = result.addressDetail.city;
            self.tableView.vechileOrderHttpMessage.startCityCode = [MyUtil getCityCodeFromCityName:city];
            
            [self initCarTypeList];
            
            [self startCarRouteSearch];
            
            [self.tableView reloadData];

            
        }
    }
    else {
        
        DLog(@"抱歉，未找到结果");
    }
}

#pragma mark - AddressTableViewDelegate method

-(void)didSelectItemOnTag:(NSUInteger)tag;
{
    if (tag == AddressTableViewRowTypeTime) {
        [self setCarDate];
    }
    else if (tag == AddressTableViewRowTypeCarType) {
        [self selectCarType];
    }
    else if (tag == AddressTableViewRowTypeStartAddress) {
        [self onSettingAddress:YES];
    }
    else if (tag == AddressTAbleViewRowTypeEndAddress) {
        [self onSettingAddress:NO];
    }
    else if (tag == AddressTAbleViewRowTypeTelephone) {
        InputHelperViewController *viewController = [[InputHelperViewController alloc] init];
        viewController.text = self.vechileOrderHttpMessage.telephone;
        viewController.tag = UseCarTelephone;
        viewController.delegate = self;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

#pragma makr - InputHelper Delegate

-(void)onTextDidChanged:(NSInteger)tag text:(NSString*)text;
{
    
    if (tag == UseCarTelephone) {
        self.vechileOrderHttpMessage.telephone = text;
        [self.tableView reloadData];
    }
    
}

#pragma mark - 预约

-(void)onBooking
{
    
    if (!self.bottomBarView.isEnabled) {
        return;
    }
    
    BOOL isRealnameCertification = [UserManager sharedInstance].userModel.isRealnameCertified;
    if (!isRealnameCertification) {
        BBAlertView *alertView = [[BBAlertView alloc] initWithTitle:@"" style:BBAlertViewStyleLogin message:@"还未实名认证" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"前往"];
        WeakSelf
        [alertView setConfirmBlock:^{
            [weakSelf gotoRealnameCertificationPage];
        }];
        
        [alertView show];
        return;
    }
    
    // 预约时间是否比当前时间晚一个小时
    if (self.vechileOrderHttpMessage.useCarMode == CarModeBooking) {
        if (self.vechileOrderHttpMessage.carDateTime == nil) {
            [MsgToolBox showToast:@"请输入预约时间"];
            return;
        }
        NSInteger currentDateTimestamp = [NSDate unixTimestamp];
        NSDate *orderDate = [NSDate dateFromString:self.vechileOrderHttpMessage.carDateTime dateFormat:@"yyyy-MM-dd HH:mm"];
        NSInteger orderDateTimestamp = [orderDate timeIntervalSince1970];
        DLog(@"currentDateTimestamp = %ld, orderDateTimestamp = %ld", (long)currentDateTimestamp, (long)orderDateTimestamp);
        if ((orderDateTimestamp-currentDateTimestamp) < 3500) {
            [MsgToolBox showToast:@"预约时间必须比当前时间晚一个小时，请重新输入"];
            return;
        }
    }


#if 1
    

    self.travelOrderHttpMessage.startTime = self.vechileOrderHttpMessage.carDateTime;
    
    self.travelOrderHttpMessage.startLocation = [NSString stringWithFormat:@"%@(%@)", self.vechileOrderHttpMessage.startAddress, self.vechileOrderHttpMessage.startLocation];
    self.travelOrderHttpMessage.endLocation = [NSString stringWithFormat:@"%@(%@)", self.vechileOrderHttpMessage.endAddress, self.vechileOrderHttpMessage.endLocation];
    self.travelOrderHttpMessage.areaCode = self.vechileOrderHttpMessage.startCityCode;
    self.travelOrderHttpMessage.carType = [NSNumber numberWithInteger:self.vechileOrderHttpMessage.carType];
    self.travelOrderHttpMessage.mileage = [NSNumber numberWithInteger:self.vechileOrderHttpMessage.distance];
    self.travelOrderHttpMessage.amount = [NSNumber numberWithInteger:self.vechileOrderHttpMessage.carPrice];
    self.travelOrderHttpMessage.startCoordinate = [NSString stringWithFormat:@"%f,%f", self.vechileOrderHttpMessage.startCoordiante.latitude,self.vechileOrderHttpMessage.startCoordiante.longitude];
    self.travelOrderHttpMessage.endCoordinate = [NSString stringWithFormat:@"%f,%f", self.vechileOrderHttpMessage.endCoordiante.latitude, self.vechileOrderHttpMessage.endCoordiante.longitude];
    self.travelOrderHttpMessage.useCarMode = [NSNumber numberWithInteger:self.vechileOrderHttpMessage.useCarMode];
    self.travelOrderHttpMessage.passengerMobile = self.vechileOrderHttpMessage.telephone;

    [self showHUDIndicatorViewAtCenter:@""];
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PUBLISH_TRAVEL_API) requestMethod:POST params:self.travelOrderHttpMessage.description success:^(id responseData) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSDictionary *data = responseData[@"data"];
            
            [VechileOrderManager sharedInstance].vechileOrderHttpMessage = weakSelf.tableView.vechileOrderHttpMessage;
            weakSelf.tableView.vechileOrderHttpMessage.orderId = data[@"id"];

            [VechileOrderManager sharedInstance].vechileOrderHttpMessage = weakSelf.tableView.vechileOrderHttpMessage;
            StartOrderViewController *viewController = [[StartOrderViewController alloc] init];
            [weakSelf.navigationController pushViewController:viewController animated:YES];
            
        }
        else {
            
            [MsgToolBox showToast:getErrorMsg(code_status)];
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        [MsgToolBox showToast:errorDesc];
        
    }];
    
#endif

}

#pragma mark - 用车类型

-(void)initCarTypeList
{
    WeakSelf
    
    NSString *params = [NSString stringWithFormat:@"areaCode=%@", self.vechileOrderHttpMessage.startCityCode];
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(CAR_TYPE_LIST_API) requestMethod:POST params:params success:^(id responseData) {
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSMutableArray *carTypeList = [NSMutableArray new];
            
            NSArray *list = responseData[@"data"];
            
            for (NSDictionary *dict in list) {
                
                CarTypeModel *model = [[CarTypeModel alloc] initWithDictionary:dict error:nil];
                [carTypeList addObject:model];
                
            }
            
            weakSelf.carTypeList = carTypeList;
            
            weakSelf.tableView.carTypeList = carTypeList;
            
            
            if (carTypeList.count > 0) {
                
                CarTypeModel *model = carTypeList[0];
                weakSelf.vechileOrderHttpMessage.carType = [model.carType integerValue];
                [weakSelf getCurrentCarDetail:weakSelf.vechileOrderHttpMessage.useCarMode carType:weakSelf.vechileOrderHttpMessage.carType];
            }
            else {
                weakSelf.vechileOrderHttpMessage.carType = 0;
                weakSelf.vechileOrderHttpMessage.carUnitPrice = 0;
                weakSelf.vechileOrderHttpMessage.carStartPrice = 0;
                weakSelf.vechileOrderHttpMessage.carStartDistance = 0;
            }
            
            
            
            [weakSelf refreshBottombarStatus];
            
            [weakSelf.tableView reloadData];

            
        }
        else {
            
        }
        
    } andFailure:^(NSString *errorDesc) {
        
    }];
}

-(void)getCurrentCarDetail:(NSUInteger)useCarMode carType:(NSUInteger)carType
{
    
    for (CarTypeModel *obj in self.carTypeList) {
        if ([obj.carType integerValue] == self.vechileOrderHttpMessage.carType) {
            self.vechileOrderHttpMessage.carUnitPrice = [obj.unitPrice integerValue];
            self.vechileOrderHttpMessage.carStartPrice = [obj.startingPrice integerValue];
            self.vechileOrderHttpMessage.carStartDistance = [obj.startingMileage integerValue];
            
            // 获取当前用车类型的路线规划方案
            for (RouteModel *routeType in obj.carTypeRoutes) {
                if (self.vechileOrderHttpMessage.useCarMode == [routeType.useCarMode integerValue]) {
                    self.vechileOrderHttpMessage.carRouteType =  [routeType.route integerValue];
                    break;
                }
            }
        }
    }
}

-(void)selectCarType
{
    
    //    VechileType
    VechileTypeSelectViewController *viewController = [VechileTypeSelectViewController new];
    viewController.delegate = self;
    viewController.carTypeList = self.carTypeList;
    viewController.carType = self.vechileOrderHttpMessage.carType;
    [self.navigationController pushViewController:viewController animated:YES];

}

-(void)onSelectVechile:(NSUInteger)tag;
{
    CarTypeModel *carModel = self.carTypeList[tag];
    
    self.tableView.vechileOrderHttpMessage.carType = [carModel.carType integerValue];
    self.tableView.vechileOrderHttpMessage.carUnitPrice = [carModel.unitPrice integerValue];
    self.tableView.vechileOrderHttpMessage.carStartPrice = [carModel.startingPrice integerValue];
    self.tableView.vechileOrderHttpMessage.carStartDistance = [carModel.startingMileage integerValue];
    [self getCurrentCarDetail:self.vechileOrderHttpMessage.useCarMode carType:[carModel.carType integerValue]];
    
    [self startCarRouteSearch];
    [self.tableView reloadData];
    
}

#pragma mark - business logic
//- (void)setCarDate:(id)sender
-(void)setCarDate
{
    WeakSelf
    [CustomActionSheetDatePicker showPickerWithTitle:@"请选择日期和时间"
                                           doneBlock:^(CustomActionSheetDatePicker *picker, NSString *selectDateTime) {
//                                               weakSelf.orderHttpMessage.carTime = selectDateTime;
                                               weakSelf.tableView.vechileOrderHttpMessage.carDateTime = selectDateTime;
                                               [weakSelf.tableView reloadData];
                                               [weakSelf refreshBottombarStatus];
//                                               [weakSelf refreshBottomBar];
                                           } cancelBlock:^(CustomActionSheetDatePicker *picker) {
                                               
                                           } origin:self.view];
    
    
}

-(void)onSettingAddress:(BOOL)isStartAddress
{
    LocationSearchViewController *vc = [[LocationSearchViewController alloc] init];
    vc.title = @"出发地点";
    WeakSelf
    vc.selectAddressHandle = ^(NSString *location,  NSString *address, NSString *cityName, NSString *areaCode, CLLocationCoordinate2D coordinate){//^(NSString *text, NSString *areaCode, CLLocationCoordinate2D coordinate) {
        
        weakSelf.mapMode = MapModeDriverPath;
        
        if (isStartAddress) {
            
            if (![areaCode isEqualToString:weakSelf.vechileOrderHttpMessage.startCityCode]) {
                weakSelf.tableView.vechileOrderHttpMessage.startCityCode = areaCode;
                [weakSelf initCarTypeList];
            }
            
            weakSelf.tableView.vechileOrderHttpMessage.startLocation = location;
            weakSelf.tableView.vechileOrderHttpMessage.startAddress = address;
            
            weakSelf.tableView.vechileOrderHttpMessage.startCity = cityName;
            weakSelf.tableView.vechileOrderHttpMessage.startCoordiante = coordinate;
//            [weakSelf initCarTypeList];
            [weakSelf.tableView reloadData];
            
            [weakSelf startCarRouteSearch];
        }
        else {
            weakSelf.tableView.vechileOrderHttpMessage.endLocation = location;
            weakSelf.tableView.vechileOrderHttpMessage.endAddress = address;
            weakSelf.tableView.vechileOrderHttpMessage.endCity = cityName;
            weakSelf.tableView.vechileOrderHttpMessage.endCityCode = areaCode;
            weakSelf.tableView.vechileOrderHttpMessage.endCoordiante = coordinate;
            [weakSelf.tableView reloadData];
            
            [weakSelf startCarRouteSearch];
        }
    };
    
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - 计算距离

-(NSString *)Rounding:(float)price afterPoint:(int)position{
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:position raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *ouncesDecimal;
    NSDecimalNumber *roundedOunces;
    
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:price];
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
//    [ouncesDecimal release];
    return [NSString stringWithFormat:@"%@",roundedOunces];
}

-(void)calculateDistance
{
    
    NSString *startAddress = self.tableView.vechileOrderHttpMessage.startLocation;
    NSString *endAddress = self.tableView.vechileOrderHttpMessage.endLocation;
    
    if (IsStrEmpty(startAddress) || IsStrEmpty(endAddress)) {
        return;
    }
    
    CLLocationCoordinate2D startCoordinate = self.tableView.vechileOrderHttpMessage.startCoordiante;
    CLLocationCoordinate2D endCoordiante = self.tableView.vechileOrderHttpMessage.endCoordiante;
    
    NSString *url = [NSString stringWithFormat:@"%@?output=json&origins=%.2f,%.2f&destinations=%.2f,%.2f&ak=%@", BAIDU_ROUTE_API, startCoordinate.latitude, startCoordinate.longitude, endCoordiante.latitude, endCoordiante.longitude, Baidu_Web_AK];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    WeakSelf
    [[NetworkManager sharedInstance] startHttpGet:url params:nil withBlock:^(NSInteger code_status, NSDictionary *responseData, NSString *error) {
        
        StrongSelf
        
        [strongSelf hideHUDIndicatorViewAtCenter];
        
        if (code_status == 0) {
            
            NSArray *result = responseData[@"result"];
            if (result.count > 0) {
                NSDictionary *dict = result[0];
                NSDictionary *distance = dict[@"distance"];
                
                // 根据车辆类型单价、距离 计算 价格
//                NSString *distanceText = [strongSelf Rounding:[distance[@"value"] doubleValue]/1000.0f afterPoint:0];
                CGFloat distanceFloat = ceilf([distance[@"value"] floatValue]/1000.0f);
                
                if (distanceFloat < strongSelf.vechileOrderHttpMessage.carStartDistance) {
                    distanceFloat = strongSelf.vechileOrderHttpMessage.carStartDistance;
                }
                strongSelf.vechileOrderHttpMessage.distance = distanceFloat;
                
                [strongSelf refreshBottombarStatus];
                
            }
            
        }
        else if (code_status == NETWORK_FAILED) {
            //            [weakSelf hideHUDIndicatorViewAtCenter];
//            [MsgToolBox showToast:error];
        }
        else {
//            NSString *msg = result[@"message"];
//            [MsgToolBox showToast:msg];
        }
        
        
    }];
}



#pragma mark - 实现BMKLocationServiceDelegate 处理位置信息更新
//处理方向变更信息
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation
{
    //    NSLog(@"heading is %@",userLocation.heading);
}
//处理位置坐标更新
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    
//    DLog(@"latitude = %f, longitude = %f", userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
    
    double currentLatitude = userLocation.location.coordinate.latitude;
    double currentLongitude = userLocation.location.coordinate.longitude;
    
    if (currentLatitude == _currentCoordinate.latitude && currentLongitude == _currentCoordinate.longitude) {
        return;
    }
    
    _currentCoordinate = CLLocationCoordinate2DMake(userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
    
    
    
    // 首次定位
    if (userLocation.isUpdating && !isFirstLocated) {
        [_mapView setCenterCoordinate:_currentCoordinate animated:YES];
        
        [self onTimer];
        
        [self queryCurrentAddress];
//        [self searchNearbyCar];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateUserLocationNotification object:nil];

        isFirstLocated = YES;
    }
    
}

#pragma mark - BMKMapView Delegate method
/**
 *地图区域改变完成后会调用此接口
 *@param mapview 地图View
 *@param animated 是否动画
 */
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated;
{
    if (isFirstLocated && self.mapMode == MapModeInit) {
//        self.tableView.vechileOrderHttpMessage.startCoordiante = _mapView.centerCoordinate;
        [self queryCurrentAddress];
        
        self.pageIndex = 1;
        [self.carList removeAllObjects];
        [self.carEntities removeAllObjects];

        [_mapView removeAnnotations:_mapView.annotations];

        [self.carEntities removeAllObjects];
        
//        for (NSString *key in self.carEntities.allKeys) {
//            id object = self.carEntities[key];
//            if ([object isKindOfClass:[MapbusAnnotation class]]) {
//                MapbusAnnotation *annotation = object;
//                annotation.movingOver = YES;
//            }
//        }
        [self searchNearbyCar];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kStopRefreshTimerNotification object:nil];
        
        
    }
}

#pragma mark - 查询附近车辆位置

-(NSMutableArray *)carList
{
    if (!_carList) {
        _carList = [NSMutableArray array];
    }
    return _carList;
}

-(void)searchNearbyCar
{
    // 设置圆形的圆心
    CLLocationCoordinate2D center = _mapView.centerCoordinate;//CLLocationCoordinate2DMake(40.047772, 116.312964);
    // 设置检索的过滤条件
    BTKQueryEntityFilterOption *filterOption = [[BTKQueryEntityFilterOption alloc] init];
    filterOption.activeTime = [[NSDate date] timeIntervalSince1970]-5*60;//7 * 24 * 3600;
    // 设置检索结果的排序方式
    BTKSearchEntitySortByOption * sortbyOption = [[BTKSearchEntitySortByOption alloc] init];
    sortbyOption.fieldName = @"loc_time";
    sortbyOption.sortType = BTK_ENTITY_SORT_TYPE_DESC;
    NSUInteger radius = 5000;
    // 构造检索请求对象
    BTKAroundSearchEntityRequest *request = [[BTKAroundSearchEntityRequest alloc] initWithCenter:center inputCoordType:BTK_COORDTYPE_BD09LL radius:radius filter:filterOption sortby:sortbyOption outputCoordType:BTK_COORDTYPE_BD09LL pageIndex:self.pageIndex pageSize:MapbusPageSize ServiceID:serviceID tag:55];
    // 发起检索请求
    [[BTKEntityAction sharedInstance] aroundSearchEntityWith:request delegate:self];
    
    

}

/**
 圆形区域检索entity的回调方法
 
 @param response 检索结果
 */
-(void)onEntityAroundSearch:(NSData *)response;
{
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
    DLog(@"onEntityAroundSearch response: %@", dict);
    
    NSUInteger status = [dict[@"status"] integerValue];
    
    NSUInteger tag = [dict[@"tag"] integerValue];
    
    // track history test
    
    if (tag == 55) {
    
        if (status == 0) {
        
            NSUInteger total = [dict[@"total"] integerValue];
            
            if (total == 0) {
                [_carEntities removeAllObjects];
                [_mapView removeAnnotations:_mapView.annotations];
                return;
            }
            
            self.totalPages = total/MapbusPageSize;
            if (total % MapbusPageSize != 0) {
                self.totalPages++;
            }
            
            if (self.pageIndex < self.totalPages) {
                
                NSArray *entities = dict[@"entities"];
                if(entities.count > 0) {
//                    [self.carList addObjectsFromArray:points];
                    
                    for (NSDictionary *entity in entities) {
                        
                        MapbusAnnotation *annotation = [[MapbusAnnotation alloc]init];
                        annotation.entityName = entity[@"entity_name"];
                        NSDictionary *latest_location = entity[@"latest_location"];
                        annotation.speed = [latest_location[@"speed"] floatValue];
                        annotation.direction = [latest_location[@"direction"] floatValue];
                        annotation.coordinate = CLLocationCoordinate2DMake([latest_location[@"latitude"] doubleValue], [latest_location[@"longitude"] doubleValue]);
                        [self.carList addObject:annotation];
                        
                    }
                    
                }
                
                self.pageIndex++;
                
                [self searchNearbyCar];
                
            }
            else {
                

                
                NSArray *entities = dict[@"entities"];
                
                if(entities.count > 0) {
                    
                    // 返回的数据对比
                    
                    for (NSDictionary *entity in entities) {
                        
                        NSString *entityName = entity[@"entity_name"];
                        // 如果不存在这个entityName
                        if (self.carEntities[entityName] == nil) {
                            MapbusAnnotation *annotation = [[MapbusAnnotation alloc] init];
                            annotation.movingOver = YES;
                            annotation.entityName = entity[@"entity_name"];
                            NSDictionary *latest_location = entity[@"latest_location"];
                            annotation.speed = [latest_location[@"speed"] floatValue];
                            annotation.direction = [latest_location[@"direction"] floatValue];
                            annotation.coordinate = CLLocationCoordinate2DMake([latest_location[@"latitude"] doubleValue], [latest_location[@"longitude"] doubleValue]);
                            annotation.height = [latest_location[@"height"] floatValue];
                            annotation.radius = [latest_location[@"radius"] floatValue];
                            
                            [self.carEntities setObject:annotation forKey:entityName];
                            [_mapView addAnnotation:annotation];
                            
                            
                            
                        }
                        // 如果存在这个entityName
                        else {
                            
                            MapbusAnnotation *annotation = self.carEntities[entityName];
                            NSDictionary *latest_location = entity[@"latest_location"];
                            annotation.speed = [latest_location[@"speed"] floatValue];
                            annotation.direction = [latest_location[@"direction"] floatValue];

                            annotation.height = [latest_location[@"height"] floatValue];
                            annotation.radius = [latest_location[@"radius"] floatValue];
                            
                            [annotation addLocation:CLLocationCoordinate2DMake([latest_location[@"latitude"] doubleValue], [latest_location[@"longitude"] doubleValue])];
                            
                            [annotation startAnimation];
                            
                        }
                        
                        
                        
//                        MapbusAnnotation *annotation = [[MapbusAnnotation alloc]init];
//                        annotation.entityName = entity[@"entity_name"];
//                        NSDictionary *latest_location = entity[@"latest_location"];
//                        annotation.speed = [latest_location[@"speed"] floatValue];
//                        annotation.degree = [latest_location[@"direction"] floatValue];
//                        annotation.coordinate = CLLocationCoordinate2DMake([latest_location[@"latitude"] doubleValue], [latest_location[@"longitude"] doubleValue]);
//                        [self.carList addObject:annotation];
                        
                    }
                    
//                    [_mapView removeAnnotations:_mapView.annotations];
//                    [_mapView addAnnotations:self.carList];
                    
                    
                }
                
            }
        }
    
        else {
            
            if (status == 3003) {
                [_mapView removeAnnotations:_mapView.annotations];
                [self.carEntities removeAllObjects];
            }
            

            
        }
    }

}



- (NSMutableDictionary*)carEntities
{
    if (!_carEntities) {
        _carEntities = [NSMutableDictionary new];
    }
    return _carEntities;
}

#pragma mark - getter and setter

-(TravelRequestHttpMessage*)travelOrderHttpMessage
{
    if (!_travelOrderHttpMessage) {
        _travelOrderHttpMessage = [TravelRequestHttpMessage new];
    }
    return _travelOrderHttpMessage;
}

-(void)onReceiveCancelOrderMessage:(id)sender
{
    
    [self initData];
    
}

#pragma mark - 初始化数据

-(void)initData
{
    
    self.vechileOrderHttpMessage.startLocation = nil;
    self.vechileOrderHttpMessage.startAddress = nil;
    self.vechileOrderHttpMessage.endLocation = nil;
    self.vechileOrderHttpMessage.endAddress = nil;
    self.vechileOrderHttpMessage.telephone = nil;
    
    self.bottomBarView.priceLabel.text = nil;
    //    self.bottomBarView.distanceLabel.text = nil;
    [self.bottomBarView setEnabled:NO];
    
    self.mapMode = MapModeInit;
    
    isFirstLocated = YES;
    
    [self.switchControl forceSelectedIndex:0 animated:YES];
    
    [self.tableView reloadData];
    
    self.vechileOrderHttpMessage.useCarMode = CarModeImmediate;
    self.vechileOrderHttpMessage.carDateTime = nil;
    self.vechileOrderHttpMessage.carType = DEFAULT_CAR_TYPE;

    [self getCurrentCarDetail:self.vechileOrderHttpMessage.useCarMode carType:self.vechileOrderHttpMessage.carType];

}


#pragma mark - Route Search

-(void)initRouteSearch
{
    _routesearch = [[BMKRouteSearch alloc]init];
    _routesearch.delegate = self;

}

-(void)startCarRouteSearch
{
    if (IsStrEmpty(self.vechileOrderHttpMessage.startLocation) || IsStrEmpty(self.vechileOrderHttpMessage.endLocation)) {
//        self.mapMode = MapModeInit;
        return;
    }

    
    BMKPlanNode* start = [[BMKPlanNode alloc]init];
    start.pt = self.vechileOrderHttpMessage.startCoordiante;
//    start.name =  self.vechileOrderHttpMessage.startAddress;//_startAddrText;
    start.cityName =  self.vechileOrderHttpMessage.startCity; //_startCityText;
    BMKPlanNode* end = [[BMKPlanNode alloc]init];
    end.pt = self.vechileOrderHttpMessage.endCoordiante;
//    end.name = self.vechileOrderHttpMessage.endAddress;//_endAddrText;
    end.cityName = self.vechileOrderHttpMessage.endCity; //_endCityText;
    
//    DLog(@"startAddress = %@, endAddress = %@", start.name, end.name);
//    DLog(@"startAddress = %@, endAddress = %@", )
    
    BMKDrivingRoutePlanOption *drivingRouteSearchOption = [[BMKDrivingRoutePlanOption alloc]init];
    drivingRouteSearchOption.from = start;
    drivingRouteSearchOption.to = end;
    // 根据后台指定的路线方案设置
    if (self.vechileOrderHttpMessage.carRouteType == RouteTypeTimeFirst) {
        drivingRouteSearchOption.drivingPolicy = BMK_DRIVING_TIME_FIRST;
    }
    else if (self.vechileOrderHttpMessage.carRouteType == RouteTypeDistanceFist) {
        drivingRouteSearchOption.drivingPolicy = BMK_DRIVING_DIS_FIRST;
    }
    else if (self.vechileOrderHttpMessage.carRouteType == RouteTypeAvoidTrafficJams) {
        drivingRouteSearchOption.drivingPolicy = BMK_DRIVING_BLK_FIRST;
    }
    // 道路和路况
//    drivingRouteSearchOption.drivingRequestTrafficType = BMK_DRIVING_REQUEST_TRAFFICE_TYPE_NONE;//不获取路况信息
    BOOL flag = [_routesearch drivingSearch:drivingRouteSearchOption];
    if(flag)
    {
        DLog(@"car检索发送成功");
    }
    else
    {
        DLog(@"car检索发送失败");
    }
    
}


/**
 *返回驾乘搜索结果
 *@param searcher 搜索对象
 *@param result 搜索结果，类型为BMKDrivingRouteResult
 *@param error 错误号，@see BMKSearchErrorCode
 */
- (void)onGetDrivingRouteResult:(BMKRouteSearch*)searcher result:(BMKDrivingRouteResult*)result errorCode:(BMKSearchErrorCode)error;
{
    NSLog(@"onGetDrivingRouteResult error:%d", (int)error);
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
    array = [NSArray arrayWithArray:_mapView.overlays];
    [_mapView removeOverlays:array];
    if (error == BMK_SEARCH_NO_ERROR) {
        BMKDrivingRouteLine* plan = (BMKDrivingRouteLine*)[result.routes objectAtIndex:0];
        NSLog(@"disatnce = %d",plan.distance);
        
        CGFloat distanceFloat = ceilf(plan.distance/1000.0f);
        
        if (distanceFloat < self.vechileOrderHttpMessage.carStartDistance) {
            distanceFloat = self.vechileOrderHttpMessage.carStartDistance;
        }
        self.vechileOrderHttpMessage.distance = distanceFloat;
        [self refreshBottombarStatus];
        
        if (self.mapMode == MapModeInit) {
            return;
        }
        
        // 计算路线方案中的路段数目
        NSInteger size = [plan.steps count];
        int planPointCounts = 0;
        for (int i = 0; i < size; i++) {
            BMKDrivingStep* transitStep = [plan.steps objectAtIndex:i];
            if(i==0){
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item.coordinate = plan.starting.location;
                item.title = @"起点";
                item.type = 0;
                [_mapView addAnnotation:item]; // 添加起点标注
                
            }
            if(i==size-1){
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item.coordinate = plan.terminal.location;
                item.title = @"终点";
                item.type = 1;
                [_mapView addAnnotation:item]; // 添加起点标注
            }
            //添加annotation节点
            RouteAnnotation* item = [[RouteAnnotation alloc]init];
            item.coordinate = transitStep.entrace.location;
            item.title = transitStep.entraceInstruction;
            item.degree = transitStep.direction * 30;
            item.type = 4;
            [_mapView addAnnotation:item];
            
            //轨迹点总数累计
            planPointCounts += transitStep.pointsCount;
        }
        // 添加途经点
        if (plan.wayPoints) {
            for (BMKPlanNode* tempNode in plan.wayPoints) {
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item = [[RouteAnnotation alloc]init];
                item.coordinate = tempNode.pt;
                item.type = 5;
                item.title = tempNode.name;
                [_mapView addAnnotation:item];
            }
        }
        //轨迹点
//        NSMutableArray *temppoints = [NSMutableArray arrayWithCapacity:planPointCounts];
        
        BMKMapPoint * temppoints = new BMKMapPoint[planPointCounts];
        int i = 0;
        for (int j = 0; j < size; j++) {
            BMKDrivingStep* transitStep = [plan.steps objectAtIndex:j];
            int k=0;
            for(k=0;k<transitStep.pointsCount;k++) {
                temppoints[i].x = transitStep.points[k].x;
                temppoints[i].y = transitStep.points[k].y;
                i++;
            }
            
        }
        // 通过points构建BMKPolyline
        BMKPolyline* polyLine = [BMKPolyline polylineWithPoints:temppoints count:planPointCounts];
        [_mapView addOverlay:polyLine]; // 添加路线overlay
        delete []temppoints;
        [self mapViewFitPolyLine:polyLine];
    } else if (error == BMK_SEARCH_AMBIGUOUS_ROURE_ADDR) {
        //检索地址有歧义,返回起点或终点的地址信息结果：BMKSuggestAddrInfo，获取到推荐的poi列表
        DLog(@"检索地址有岐义，请重新输入。");
//        [self showGuide];
    }

}

#pragma mark - 私有

//根据polyline设置地图范围
- (void)mapViewFitPolyLine:(BMKPolyline *) polyLine {
    CGFloat ltX, ltY, rbX, rbY;
    if (polyLine.pointCount < 1) {
        return;
    }
    BMKMapPoint pt = polyLine.points[0];
    ltX = pt.x, ltY = pt.y;
    rbX = pt.x, rbY = pt.y;
    for (int i = 1; i < polyLine.pointCount; i++) {
        BMKMapPoint pt = polyLine.points[i];
//        DLog(@"pt.x=%.f, pt.y=%.f", pt.x, pt.y);
        if (pt.x < ltX) {
            ltX = pt.x;
        }
        if (pt.x > rbX) {
            rbX = pt.x;
        }
        if (pt.y > ltY) {
            ltY = pt.y;
        }
        if (pt.y < rbY) {
            rbY = pt.y;
        }
    }
    BMKMapRect rect;
    rect.origin = BMKMapPointMake(ltX , ltY);
    rect.size = BMKMapSizeMake(rbX - ltX, rbY - ltY);
    
    [_mapView setVisibleMapRect:rect];

    _mapView.zoomLevel = _mapView.zoomLevel - 1.35;
    
    
}

#pragma mark - BMKMapViewDelegate

- (BMKAnnotationView *)mapView:(BMKMapView *)view viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if (self.mapMode == MapModeDriverPath) {
        if ([annotation isKindOfClass:[RouteAnnotation class]]) {
            return [(RouteAnnotation*)annotation getRouteAnnotationView:view];
        }
    }
    else if (self.mapMode == MapModeInit) {
        if ([annotation isKindOfClass:[MapbusAnnotation class]]) {
            return [(MapbusAnnotation*)annotation getRouteAnnotationView:view];
        }
    }

    return nil;
}

- (BMKOverlayView*)mapView:(BMKMapView *)map viewForOverlay:(id<BMKOverlay>)overlay
{
    if (self.mapMode == MapModeDriverPath && [overlay isKindOfClass:[BMKPolyline class]]) {
        BMKPolylineView* polylineView = [[BMKPolylineView alloc] initWithOverlay:overlay];
        polylineView.fillColor = [UIColor greenColor];
        polylineView.strokeColor = [UIColor greenColor];
        polylineView.lineWidth = 2;
        return polylineView;
    }
    return nil;
}

-(void)gotoPriceExplainWebpage
{
    NSString *url = [NSString stringWithFormat:@"%@/%@?area_code=%@", HTTP_BASE_URL, PRICE_EXPLAIN_HTML, self.vechileOrderHttpMessage.startCityCode];
//    NSString *url = [NSString stringWithFormat:@"%@%@?area_code=%@", RESOURSE_WEB_PAGE, PRICE_EXPLAIN_HTML,self.vechileOrderHttpMessage.startCityCode];
    [self openWebView:url];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
