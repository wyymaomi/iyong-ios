//
//  UserExperienceListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/1/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "UserExperienceListViewController.h"
#import "VechileExperienceTableViewCell.h"
//#import "OnlineServiceTableViewCell.h"
//#import "OnlineServiceModel.h"
//#import "ServiceProviderViewModel.h"
#import "UserExpericeModel.h"
#import "UserExpericenViewModel.h"
#import "UserExperienceTableViewCell.h"
#import "CommentToolBarView.h"
#import "BBAlertView.h"
#import "AppConfig.h"
#import "ExperienceDetailViewController.h"
//#import "CommentDetailViewController.h"
#import "PageUtil.h"
#import "UserExperienceListViewController+NetworkRequest.h"
#import <UMSocialCore/UMSocialCore.h>
#import "PostExperienceViewController.h"

@interface UserExperienceListViewController ()<UIActionSheetDelegate,UserExperienceCellDelegate>



@end

@implementation UserExperienceListViewController

- (PageUtil*)commonPage
{
    if (_commonPage == nil) {
        _commonPage = [PageUtil new];
        _commonPage.pageIndex = 1;
    }
    return _commonPage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.pullRefreshTableView];
//    self.title = @"体验";
    [self.navigationItem setTitle:@"用户体验"];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(gotoPublish)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@sele
//                                              ]
    
    [self.pullRefreshTableView beginHeaderRefresh];
    
//#if DEBUG
//    [self.view addSubview:self.fpsLabel];
//    self.fpsLabel.top = self.view.height-30*Scale;
//#endif
    
//    self.pullRefreshTableView.separatorStyle = UITable;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(onPublish)];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
//    self.fpsLabel.top = self.view.frame.size.height-22;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //    self.statusView.frame = CGRectMake(0, 0, ViewWidth, 50);
    //    [self.statusView initWithTitles:self.titleArray NormalColor:[UIColor blackColor] SelectedColor:[UIColor blackColor] LineColor:[UIColor blueColor]];
    //
    self.pullRefreshTableView.frame = CGRectMake(0, 0, ViewWidth, self.view.frame.size.height);
    
//    [self.pullRefreshTableView beginHeaderRefresh];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PullRefresh delegate method

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
//    [self refreshData:nil time:nil isHeaderRefresh:YES];
    self.commonPage.pageIndex = 1;
    [self fetchExperienceListByPageIndex:self.commonPage.pageIndex];
    
}

-(void)onFooterRefresh
{
    
    if (self.dataList.count == 0) {
        //        [self.pullRefreshTableView endFooterRefresh];
        //        self.pullRefreshTableView onH
        [self onHeaderRefresh];
        return;
    }
    
    if (self.dataList.count > 0) {
        self.nextAction = @selector(onFooterRefresh);
        self.object1 = nil;
        self.object2 = nil;
        
        [self fetchExperienceListByPageIndex:self.commonPage.pageIndex];
        
    }

    
}

#pragma mark - 

- (void)gotoPublish
{
    
    if ([self needLogin]) {
        return;
    }
    
    PostExperienceViewController *viewController = [[PostExperienceViewController alloc] init];
    viewController.type = TableTypeUserExperience;
    viewController.enter_type = ENTER_TYPE_PRESENT;
//    self.navigationController
//    [self.navigationController pushViewController:viewController animated:YES];
    //    viewController.enter_type = ENTER_TYPE_PRESENT;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navigationController animated:YES completion:nil];
    
//    PublishViewController *viewController = [[PublishViewController alloc] init];
//    viewController.
//    self gotoPage:@"PublishView"
//    PublishViewController *publishViewController = [[PublishViewController alloc] init];
//    [self presentViewController:publishViewController animated:YES completion:nil];
}

#pragma mark -

//-(void)refreshData:(NSString*)quantity time:(NSString*)time isHeaderRefresh:(BOOL)isHeaderRefresh
//{
//    
////    if ([self.title isEqualToString:@"我的收藏"]) {
////        [self fetchMyFavoriteList:quantity time:time isHeaderRefresh:isHeaderRefresh];
////    }
////    else {
//        [self fetchServiceList:quantity time:time isHeaderRefresh:isHeaderRefresh];
////    }
//    
//}



#pragma  mark - 按点赞数量排序获取数据


#if 0
- (void)fetchServiceList:(NSString*)quantity time:(NSString*)time isHeaderRefresh:(BOOL)isHeaderRefresh
{
    NSDictionary *params = @{/*@"quantity":[StringUtil getSafeString:quantity],*/
                             @"areaCode":[AppConfig currentConfig].gpsCityCode,
                             @"time":[StringUtil getSafeString:time],
                             @"token":[StringUtil getSafeString:[UserManager sharedInstance].token]};
    
    NSString *url;
    url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_LIST_API];
//    if ([self.title isEqualToString:@"我的收藏"]) {
//        url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, MY_FAVORITE_SERVICER_API];
//    }
    
    if (isHeaderRefresh) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    self.loadStatus = LoadStatusLoading;
    WeakSelf
    [[NetworkManager sharedInstance] startHttpPost:url params:params withBlock:^(NSInteger code_status, NSDictionary *result, NSString *error) {
        
        StrongSelf
        
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        if (code_status == STATUS_OK) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
                if (error) {
                    return;
                }
                
                //                if ([responseData.code integerValue] == STATUS_OK) {
                
                for (NSInteger i = 0 ; i < responseData.data.count; i++) {
                    UserExpericeModel *model = responseData.data[i];
                    UserExpericenViewModel *viewModel = [[UserExpericenViewModel alloc] init];
                    viewModel.serviceModel = model;
                    [strongSelf.dataList addObject:viewModel];
                }

                
                //                    [strongSelf.pullRefreshTableView reloadData];
                //                }
                //                else {
                //                    [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
                //                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    strongSelf.loadStatus = LoadStatusFinish;
                    [strongSelf.pullRefreshTableView reloadData];
                    //                    strongSelf.pullRefreshTableView.footerHidden = NO;
                });
                
            });

            
//            strongSelf.loadStatus = LoadStatusFinish;
//
//            NSError *error;
//            UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
//            if (error) {
//                return;
//            }
//            
//            if ([responseData.code integerValue] == STATUS_OK) {
//                
//                for (NSInteger i = 0 ; i < responseData.data.count; i++) {
//                    UserExpericeModel *model = responseData.data[i];
//                    UserExpericenViewModel *viewModel = [[UserExpericenViewModel alloc] init];
//                    viewModel.serviceModel = model;
//                    [strongSelf.dataList addObject:viewModel];
//                }
//                
//                [strongSelf.pullRefreshTableView reloadData];
//            }
//            else {
//                [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
//            }
            
        }
        else if (code_status == NETWORK_FAILED) {
            strongSelf.loadStatus = LoadStatusNetworkError;
        }
        else {
            strongSelf.loadStatus = LoadStatusFinish;
            NSError *error;
            UserExpericeResponseData *responseData = [[UserExpericeResponseData alloc] initWithDictionary:result error:&error];
            if (error) {
                return;
            }
            [strongSelf showAlertView:getErrorMsg([responseData.code integerValue])];
        }
        
    }];
}
#endif

#pragma mark - 打电话


#pragma mark - 分享

- (void)onClickShareButton:(id)sender
{
    if ([self needLogin]) {
        return;
    }
    
//    [UMSocialManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
//        // 根据获取的platformType确定所选平台进行下一步操作
//    }];
    
//    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer*)sender;
//    UIView *view = [singleTap view];
//    NSInteger index = view.tag;
//    
//    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该功能暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//    [alertView show];
    
    
}

#pragma mark - 评论

- (void)onClickCommentButton:(id)sender
{
    if ([self needLogin]) {
        return;
    }
    
    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer*)sender;
    UIView *view = [singleTap view];
    NSInteger index = view.tag;
    
    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleDefault Title:nil message:@"该功能暂未开放,敬请期待" customView:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];

}

#pragma mark - 点赞服务商

- (void)onClickLikeButton:(id)sender
{
    self.nextAction = @selector(onClickLikeButton:);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    UIButton *button = (UIButton*)sender;
    NSInteger index = button.tag;
    DLog(@"index = %ld", (long)index);
    UserExpericenViewModel *viewModel = self.dataList[index];
    [self addLikeNumber:viewModel];
    
    
    
//    if ([sender isKindOfClass:[UIButton class]]) {
    
//    }
//    else {
//        UITapGestureRecognizer *singleTap = (UITapGestureRecognizer*)sender;
//        UIView *view = [singleTap view];
//        NSInteger index = view.tag;
//        UILabel *likeLabel = [view viewWithTag:2000];
//        UIImageView *likeImageView = [view viewWithTag:2001];
//        
//        UserExpericenViewModel *viewModel = self.dataList[index];
//        
//        [self addLikeNumber:viewModel likeLabel:likeLabel likeImageView:likeImageView];
//    }
    

    
    
}


#pragma mark - 收藏服务商



#pragma mark - UITableViewCell delegate method


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    DLog(@"-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView");
    return self.dataList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    DLog(@"tableView:(UITableView*)tableView numberOfRowsInSection:(NSIndexPath*)indexPath");
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    DLog(@"-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath");
    
    if (self.dataList.count == 0) {
        return 0;
    }

//    UserExpericenViewModel *viewModel = self.dataList[indexPath.section];
//    return viewModel.cellHeight;
    
    return [VechileExperienceTableViewCell getCellHeight];
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    //    DLog(@"- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section");
//    return 10;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    //    DLog(@"- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section");
    return [UIView new];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    VechileExperienceTableViewCell *cell = [[VechileExperienceTableViewCell alloc] initWithStyle:tableView];
    
    VechileExperienceTableViewCell *cell = [VechileExperienceTableViewCell cellWithTableView:tableView indexPath:indexPath];
    UserExpericenViewModel *viewModel = self.dataList[indexPath.section];
    UserExpericeModel *data = viewModel.serviceModel;
    // 下载头像
//    cell.avatarImageView.image = AvatarPlaceholderImage;
//    [cell.avatarImageView downloadImage:data.logoUrl placeholderImage:AvatarPlaceholderImage success:^(id responseData) {
//        cell.avatarImageView.cornerRadius = cell.avatarImageView.width/2;
//    } andFailure:^(NSString *errorDesc) {
    
//    }];
//    NSString *img1, *img2, *img3;
    
//    [cell.avatarImageView downloadImageFromAliyunOSS:data.logoUrl isThumbnail:NO placeholderImage:AvatarPlaceholderImage success:nil andFailure:nil];
    
    [cell.avatarImageView yy_setImageWithObjectKey:data.logoUrl placeholder:AvatarPlaceholderImage manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        
        
    }];
    
    cell.imageView1.hidden = YES;
    cell.imageView2.hidden = YES;
    cell.imageView3.hidden = YES;
    if (data.imgs.count >= 1){
        cell.imageView1.hidden = NO;
#if DONWLOAD_SWITCH
        [cell.imageView1 downloadImageFromAliyunOSS:data.imgs[0] isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
#else
        [cell.imageView1 yy_setImageWithObjectKey:data.imgs[0] placeholder:DefaultPlaceholderImage imageSize:DefaultImageSize manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
        }];
#endif
    }
    if (data.imgs.count >=2) {
        cell.imageView2.hidden = NO;
#if DONWLOAD_SWITCH
        [cell.imageView2 downloadImageFromAliyunOSS:data.imgs[1] isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
#else
        [cell.imageView2 yy_setImageWithObjectKey:data.imgs[1] placeholder:DefaultPlaceholderImage imageSize:DefaultImageSize manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
        }];
#endif
    }
    if (data.imgs.count >= 3) {
        cell.imageView3.hidden = NO;
#if DONWLOAD_SWITCH
        [cell.imageView3 downloadImageFromAliyunOSS:data.imgs[2] isThumbnail:YES placeholderImage:DefaultPlaceholderImage success:nil andFailure:nil];
#else
        [cell.imageView3 yy_setImageWithObjectKey:data.imgs[2] placeholder:DefaultPlaceholderImage imageSize:DefaultImageSize manager:[YYWebImageManager sharedManager] progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        } completion:^(UIImage * _Nullable image, NSString * _Nonnull objectKey, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
        }];
#endif
    }
    cell.contentLabel.text = data.content;
    cell.userNameLabel.text = [StringUtil getSafeString:data.nickname];//[StringUtil getSafeString:data.companyName];//data.companyName;
    
    cell.thumbupButton.enabled = YES;
    cell.thumbupButton.tag = indexPath.section;
    cell.thumbupNumberLabel.text = data.thumbsQuantity;
//    [cell.thumbupButton addTarget:self action:@selector(onClickLikeButton:) forControlEvents:UIControlEventTouchUpInside];
    
//    NSString *thumbupImageName = [data.thumbsUp boolValue]?@"icon_thumup_highlighted":@"icon_thumbup_gray";
//    [cell.thumbupButton setImage:[UIImage imageNamed:thumbupImageName] forState:UIControlStateNormal];
    
    cell.browseNumberLabel.text = data.browseQuantity;
    cell.commentNumberLabel.text = data.commentQuantity;
    
    return cell;

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserExpericenViewModel *viewModel = self.dataList[indexPath.section];
//    UserExpericeModel *data = viewModel.serviceModel;
    ExperienceDetailViewController *viewController = [[ExperienceDetailViewController alloc] init];
    viewController.experienceViewModel = viewModel;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
    
}


@end
