//
//  PublishHeaderTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/7.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "PublishHeaderTableViewCell.h"

@implementation PublishHeaderTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;
{
    static NSString *ID = @"PostContentTableViewCell";
    PublishHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
    }
    return cell;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.headerView.frame = CGRectMake(0, 0, ViewWidth, self.height);
    self.lineView.frame = CGRectMake(24, self.height-SEPARATOR_LINE_HEIGHT, ViewWidth-48, SEPARATOR_LINE_HEIGHT);
}

- (UIView*)lineView
{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor=UIColorFromRGB(0xebebeb);
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}

- (HeaderView*)headerView
{
    if (_headerView == nil) {
        _headerView = [HeaderView customView];
        _headerView.titleLabel.text = @"图片上传";
        [self.contentView addSubview:_headerView];
    }
    return _headerView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
