//
//  HyperlinksButton.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HyperlinksButton : UIButton

@property (strong, nonatomic) UIColor *lineColor;

@end
