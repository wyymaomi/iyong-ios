//
//  StartOrderViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/24.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "StartOrderViewController.h"
#import "StartOrderViewController+Business.h"
#import "VechileOrderManager.h"
#import "TravelRequestHttpMessage.h"



@interface StartOrderViewController ()

@property (nonatomic, strong) TravelRequestHttpMessage *travelOrderHttpMessage;

@end

@implementation StartOrderViewController

-(TravelRequestHttpMessage*)travelOrderHttpMessage
{
    if (!_travelOrderHttpMessage) {
        _travelOrderHttpMessage = [TravelRequestHttpMessage new];
    }
    return _travelOrderHttpMessage;
}

-(id)init
{
    self = [super init];
    
    if (self) {
        
        _vechileOrderHttpMessage = [VechileOrderManager sharedInstance].vechileOrderHttpMessage;
        
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    _vechileOrderHttpMessage = [VechileOrderManager sharedInstance].vechileOrderHttpMessage;
    
    [self initViews];
    
    [self initData];
    
    [self addObserver:self forKeyPath:@"receiveOrderStatus" options:NSKeyValueObservingOptionNew context:NULL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveCancelOrderMessage:) name:kOnCancelCarOrderSuccessNotification object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onOrderReceived:) name:kGetOrderReceivedNotification object:nil];
    
//    WeakSelf

//    [[NSRunLoop currentRunLoop] addTimer:self.driverReceiveTime forMode:NSRunLoopCommonModes];
    
}

-(void)initData
{
    self.distanceLabel.text = [NSString stringWithFormat:@"行程：%.fkm", _vechileOrderHttpMessage.distance];
    self.priceLabel.text = [NSString stringWithFormat:@"%.f", _vechileOrderHttpMessage.carPrice];
}

-(void)initViews
{
    self.view.backgroundColor = UIColorFromRGB(0x3E3E3E);
    
    self.rotationBgImageView.frame = CGRectMake((ViewWidth-230*Scale)/2, 65*Scale, 230*Scale, 230*Scale);
    
    self.rotationImageView.frame = CGRectMake((ViewWidth-239*Scale)/2, 63*Scale, 239*Scale, 239*Scale);
    
    
    self.receiveOrderStatus = ReceiveOrderStatusWaiting;
    
    self.cancelOrderView.hidden = NO;
    self.goPayView.hidden = YES;
    self.waitingReceiveOrderView.hidden = NO;
    self.completeReceiveOrderView.hidden = YES;
    
    [self.backButton addTarget:self action:@selector(pressedBackButton) forControlEvents:UIControlEventTouchUpInside];
}

//-(void)pressedBackButton
//{
//    [[NSNotificationCenter defaultCenter] postNotificationName:kOnCancelCarOrderSuccessNotification object:nil];
//    
//    [super pressedBackButton];
//}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self repeateRotation];
    
    if (!_driverReceiveTime && self.receiveOrderStatus == ReceiveOrderStatusWaiting) {
        _driverReceiveTime = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(getOrderDetail) userInfo:nil repeats:YES];
    }
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [self stopRotation];
    
    if (_driverReceiveTime) {
        [_driverReceiveTime invalidate];
        _driverReceiveTime = nil;
    }
    
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.rotationBgImageView.frame = CGRectMake((ViewWidth-270*Scale)/2, 36*Scale, 270*Scale, 270*Scale);
    
    self.rotationImageView.frame = CGRectMake((ViewWidth-239*Scale)/2, 48*Scale, 239*Scale, 239*Scale);
    
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kOnCancelCarOrderSuccessNotification object:nil];
    [self removeObserver:self forKeyPath:@"receiveOrderStatus"];
//    if (self.driverReceiveTime) {
//        [self.driverReceiveTime invalidate];
//        self.driverReceiveTime = nil;
//    }

}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    
    if ([keyPath isEqualToString:@"receiveOrderStatus"]) {
        
        switch (self.receiveOrderStatus) {
            case ReceiveOrderStatusWaiting:
                self.cancelOrderView.hidden = NO;
                self.goPayView.hidden = YES;
                self.waitingReceiveOrderView.hidden = NO;
                self.completeReceiveOrderView.hidden = YES;
                break;
            case ReceiveOrderStatusComplete:
                self.cancelOrderView.hidden = NO;
                self.goPayView.hidden = NO;
                self.waitingReceiveOrderView.hidden = YES;
                self.completeReceiveOrderView.hidden = NO;
                self.carModelLabel.text = [NSString stringWithFormat:@"车型：%@", self.vechileOrderHttpMessage.carModel];
                self.carNumberLabel.text = [NSString stringWithFormat:@"车牌：%@", self.vechileOrderHttpMessage.carNumber];
                self.companyLabel.text = self.vechileOrderHttpMessage.companyName;
                
            default:
                break;
        }
    }
}


#pragma mark - Rotation

-(void)repeateRotation
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = 2;
    rotationAnimation.repeatCount = HUGE_VALF;
    rotationAnimation.cumulative = NO;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    [self.rotationImageView.layer addAnimation:rotationAnimation forKey:@"Rotation"];
//    [self.indicator.layer addAnimation:rotationAnimation forKey:@"Rotation"];
}

-(void)stopRotation
{
    [self.rotationImageView.layer removeAllAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelOrder:(id)sender {
    
    NSString *orderId = [VechileOrderManager sharedInstance].vechileOrderHttpMessage.orderId;
    NSInteger carMode = [VechileOrderManager sharedInstance].vechileOrderHttpMessage.useCarMode;
//    [[VechileOrderManager sharedInstance] cancelOrder:orderId viewController:self];
//    [[VechileOrderManager sharedInstance] cancelOrder:orderId viewController:self isCompleteFinish:NO];
    [[VechileOrderManager sharedInstance] cancelOrder:orderId carOrderMode:carMode viewController:self isCompletePay:NO];
    
}

- (IBAction)gotoPay:(id)sender {
//    [MsgToolBox showToast:@"前往支付"];
    
    if ([[VechileOrderManager sharedInstance] isPendingPayOvertime]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"支付超时，请重新下单" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        WeakSelf
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                [weakSelf pressedBackButton];
            }
        }];
        
        return;
    }
    
    [self gotoPage:@"OrderPayViewController"];
}

#pragma mark - 预约
-(void)startBooking
{
    self.travelOrderHttpMessage.startTime = self.vechileOrderHttpMessage.carDateTime;
    self.travelOrderHttpMessage.startLocation = [NSString stringWithFormat:@"%@(%@)", self.vechileOrderHttpMessage.startAddress, self.vechileOrderHttpMessage.startLocation];
    self.travelOrderHttpMessage.endLocation = [NSString stringWithFormat:@"%@(%@)", self.vechileOrderHttpMessage.endAddress, self.vechileOrderHttpMessage.endLocation];
    self.travelOrderHttpMessage.areaCode = self.vechileOrderHttpMessage.startCityCode;
    self.travelOrderHttpMessage.carType = [NSNumber numberWithInteger:self.vechileOrderHttpMessage.carType];
    self.travelOrderHttpMessage.mileage = [NSNumber numberWithInteger:self.vechileOrderHttpMessage.distance];
    self.travelOrderHttpMessage.amount = [NSNumber numberWithInteger:self.vechileOrderHttpMessage.carPrice];
    self.travelOrderHttpMessage.startCoordinate = [NSString stringWithFormat:@"%f,%f", self.vechileOrderHttpMessage.startCoordiante.latitude,self.vechileOrderHttpMessage.startCoordiante.longitude];
    self.travelOrderHttpMessage.endCoordinate = [NSString stringWithFormat:@"%f,%f", self.vechileOrderHttpMessage.endCoordiante.latitude, self.vechileOrderHttpMessage.endCoordiante.longitude];
    
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(PUBLISH_TRAVEL_API) requestMethod:POST params:self.travelOrderHttpMessage.description success:^(id responseData) {
        
//        [weakSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSDictionary *data = responseData[@"data"];
            
            weakSelf.vechileOrderHttpMessage.orderId = data[@"id"];
            weakSelf.vechileOrderHttpMessage.receiveCutoffTime = [data[@"receiveCutoffTime"] doubleValue];
            
            
        }
        else {
            
            [MsgToolBox showToast:getErrorMsg(code_status)];
            
            [weakSelf pressedBackButton];
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [MsgToolBox showToast:errorDesc];
        
        [weakSelf pressedBackButton];
        
    }];
}

-(void)orderReceived:(NSDictionary*)infoDic
{
    NSString *orderId = infoDic[@"orderId"];
    DLog(@"orderId=%@", orderId);
    
    if (![self.vechileOrderHttpMessage.orderId isEqualToString:orderId]) {
        self.vechileOrderHttpMessage.orderId = infoDic[@"orderId"];
    }
//    self.vechileOrderHttpMessage.orderId = infoDic[@"orderId"];
    [self stopDriverTimer];
    [self getOrderDetail];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
