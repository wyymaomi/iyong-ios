//
//  CommunityCategoryViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

//const NSInteger CommunitySection = 3;
//const NSInteger CommunityFirstSectionRows = 1;
//const NSInteger CommunitySecondSectionRows = 2;
//const NSInteger CommunityThirdSectionRows = 2;

#define ITERATIONS (1024*10)

static double then, now;

@interface CommunityCategoryViewController : BaseViewController

@property (nonatomic, assign) BOOL isFirstIn;

//- (void)showAdv;

@property (nonatomic, strong) NSTimeZone *timeZone;
@property (nonatomic, strong) NSString *dateAsString;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;



@end
