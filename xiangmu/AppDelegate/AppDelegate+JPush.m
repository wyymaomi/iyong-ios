//
//  AppDelegate+JPush.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/5.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AppDelegate+JPush.h"

//#import "RootViewController.h"
#import <AdSupport/AdSupport.h>
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
#import "StartOrderViewController.h"

@implementation AppDelegate (JPush)

//-(void)setupAPNs
//{
//
//}

- (void)setupJPush:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
    //Required
    //notice: 3.0.0及以后版本注册可以这样写，也可以继续用之前的注册方式
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        // 可以添加自定义categories
        // NSSet<UNNotificationCategory *> *categories for iOS10 or later
        // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    // Optional
    // 获取IDFA
    // 如需使用IDFA功能请添加此代码并在初始化方法的advertisingIdentifier参数中填写对应值
    NSString *advertisingId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
#ifdef RELEASE
    BOOL isProduction = YES;
#else
    BOOL isProduction = NO;
#endif
    
    NSString *channel = @"APP_STORE";
    
    // Required
    // init Push
    // notice: 2.1.5版本的SDK新增的注册方法，改成可上报IDFA，如果没有使用IDFA直接传nil
    // 如需继续使用pushConfig.plist文件声明appKey等配置内容，请依旧使用[JPUSHService setupWithOption:launchOptions]方式初始化。
    [JPUSHService setupWithOption:launchOptions appKey:JPUSH_APPKEY
                          channel:channel
                 apsForProduction:isProduction
            advertisingIdentifier:advertisingId];
    
    //2.1.9版本新增获取registration id block接口。
    [JPUSHService registrationIDCompletionHandler:^(int resCode, NSString *registrationID) {
        if(resCode == 0){
            NSLog(@"registrationID获取成功：%@",registrationID);
            
            [AppConfig currentConfig].deviceId = registrationID;//[CloudPushSDK getDeviceId];
             
             // 如果用户token为空，则不上传
             
             if ([[UserManager sharedInstance] isLogin] /*&& !IsStrEmpty(loginRsaBody) && !IsStrEmpty(loginShaBody)*/) {
                 [[UserManager sharedInstance] doAutoLogin:^(NSInteger code_status) {
                     if (code_status == STATUS_OK) {
                         [self.window makeKeyAndVisible];
                     }
                 } failure:^(NSInteger code_status) {
                     [self.window makeKeyAndVisible];
                 }];
             }
            
        }
        else{
            NSLog(@"registrationID获取失败，code：%d",resCode);
        }
    }];
}

//#pragma mark- JPUSHRegisterDelegate
//
//// iOS 10 Support
//- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
//    // Required
//    NSDictionary * userInfo = notification.request.content.userInfo;
//    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//        [JPUSHService handleRemoteNotification:userInfo];
//    }
//    completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
//}
//
//// iOS 10 Support
//- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
//    // Required
//    NSDictionary * userInfo = response.notification.request.content.userInfo;
//    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//        [JPUSHService handleRemoteNotification:userInfo];
//    }
//    completionHandler();  // 系统要求执行这个方法
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    DLog(@"iOS7及以上系统，收到通知:%@", userInfo);
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    
    [self handleRemoteMessage:UIApplicationStateActive userInfo:userInfo];
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    // Required,For systems with less than or equal to iOS6
    [JPUSHService handleRemoteNotification:userInfo];
    
    [self handleRemoteMessage:UIApplicationStateActive userInfo:userInfo];
    
    DLog(@"iOS6以下系统，收到通知:%@", userInfo);
}


#pragma mark - 注册APNS服务器成功

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    DLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
    [JPUSHService showLocalNotificationAtFront:notification identifierKey:nil];
}

//#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#pragma mark- JPUSHRegisterDelegate
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    NSDictionary * userInfo = notification.request.content.userInfo;
    
    UNNotificationRequest *request = notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        DLog(@"iOS10 前台收到推送通知: %@", userInfo);
//        NSLog(@"iOS10 前台收到远程通知:%@", [self logDic:userInfo]);
        
//        [rootViewController addNotificationCount];
        [self handleRemoteMessage:UIApplicationStateActive userInfo:userInfo];
        
    }
    else {
        // 判断为本地通知
        NSLog(@"iOS10 前台收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
    }
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置
}

- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    UNNotificationRequest *request = response.notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        DLog(@"iOS10.收到远程通知：%@", userInfo);
//        NSLog(@"iOS10 收到远程通知:%@", [self logDic:userInfo]);
//        [rootViewController addNotificationCount];
        
        [self handleRemoteMessage:UIApplicationStateActive userInfo:userInfo];
        
    }
    else {
        // 判断为本地通知
        NSLog(@"iOS10 收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
    }
    
    completionHandler();  // 系统要求执行这个方法
}

#pragma mark - 自定义处理推送通知

#define kReceiveOrderNotificationCode @"1002"

-(void)handleRemoteMessage:(NSUInteger)applicationStatus userInfo:(NSDictionary*)userInfo
{
    
    //    NSUInteger applicationStatus = application.applicationState;
    
    
#if 1
    NSDictionary *apns = userInfo[@"aps"];
    NSString *message = apns[@"alert"];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [JPUSHService resetBadge];
    
    
    NSString *code = userInfo[@"code"];
    

    NSString *dataString = userInfo[@"data"];
    NSDictionary *tempDataDict;
    if (!IsStrEmpty(dataString)) {
        NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
        tempDataDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }

    if (applicationStatus == UIApplicationStateActive /*|| applicationStatus == UIApplicationStateInactive*/) {
        
        if ([code isEqualToString:kReceiveOrderNotificationCode]) {
            
            [self loadOrderView:tempDataDict message:message];
  
        }
        else {
            
            if (!IsStrEmpty(message)) {
                [MsgToolBox showToast:message];
            }
            
        }
        
    }
    else {
        
        if ([code isEqualToString:kReceiveOrderNotificationCode]) {
            
            [self loadOrderView:tempDataDict message:message];
            
        }
        
        else {
            
            if (!IsStrEmpty(message)) {
                [MsgToolBox showToast:message];
            }
            
        }
        
    }
    
#endif
}

- (void)loadOrderView:(NSDictionary*)data message:(NSString*)message
{
    
    UITabBarController*tab = (UITabBarController*)self.window.rootViewController;
    
    UINavigationController *nvc = tab.selectedViewController;
    
    UIViewController *vc = nvc.visibleViewController;
    
    //防止同一界面多次push
    if ([vc isMemberOfClass:[StartOrderViewController class]]) {
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:kGetOrderReceivedNotification object:data];
        
        StartOrderViewController *viewController = (StartOrderViewController*)vc;
        [viewController orderReceived:data];
        
        
    }
    else {
        
        NSString *alertMsg;
        if (IsStrEmpty(message)) {
            alertMsg = @"您有一笔订单已被司机接单，请在15分钟内支付";
        }
        else {
            alertMsg = message;
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"消息" message:alertMsg delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"查看", nil];
        [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                
                StartOrderViewController *orderViewController = [[StartOrderViewController alloc] init];
                orderViewController.hidesBottomBarWhenPushed = YES;
                [orderViewController orderReceived:data];
                [vc.navigationController pushViewController:orderViewController animated:YES];
                
            }
            
        }];
        
    }
    
}



@end
