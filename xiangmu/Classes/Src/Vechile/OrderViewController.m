//
//  OrderViewController.m
//  xiangmu
//
//  Created by David kim on 16/4/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "OrderViewController.h"
#import "AddCarViewController.h"
#import "EditCarViewController.h"
#import "UserManager.h"
#import "NetworkManager.h"
#import "CarTableViewCell.h"
#import "HttpConstants.h"
#import "MJRefresh.h"
#import "MyUtil.h"
#import "LoginViewController.h"
#import "UserManager.h"
#import "OrderListResponseModel.h"
#import "UIImageView+WebCache.h"


@interface OrderViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    NSMutableArray *dataArray;
    NSString *dayStr;
    NSString *timeStr;
    NSString *uid;
    CarTableViewCell *cellView;
}
@end

@implementation OrderViewController

- (void)viewDidLoad {
     self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    [super viewDidLoad];
    self.carTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    self.tabBarController.tabBar.hidden=YES;
    self.view.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    [self createMyNav];
  
    [self.carTableView reloadData];
    [self setupHeaderView];
    [self setupFooterView];
    [self.carTableView.mj_header beginRefreshing];
   
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(NSMutableArray *)dataArray
{
    if (dataArray == nil) {
        dataArray=[NSMutableArray new];
    }
    return dataArray;
}
-(UITableView *)carTableView
{
    if (_carTableView == nil) {
        _carTableView=[[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        _carTableView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
        _carTableView.delegate=self;
        _carTableView.dataSource=self;
        _carTableView.userInteractionEnabled=YES;
        _carTableView.bounces=YES;
        _carTableView.scrollEnabled=YES;
        _carTableView.showsVerticalScrollIndicator=NO;
        _carTableView.showsHorizontalScrollIndicator=NO;
        //去掉分割线
        _carTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //
        _carTableView.autoresizesSubviews=YES;
        //
        _carTableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_carTableView];
    }
    return _carTableView;
}
-(void)gotoShuaxin
{
    [self setupHeaderView];
    [self.carTableView.mj_header beginRefreshing];
    [self.carTableView reloadData];
}
//-(void)Success:(NSNotification *)noti
//{
//    
//    [self setupHeaderView];
//    [self.carTableView.mj_header beginRefreshing];
//    [self.carTableView reloadData];
//}
-(void)cheliang
{
    [self setupHeaderView];
    [self.carTableView.mj_header beginRefreshing];
    [self.carTableView reloadData];
}
//-(void)editSuccess:(NSNotification *)notification
//{
//    [self.carTableView reloadData];
//    [self setupHeaderView];
//    [self.carTableView.mj_header beginRefreshing];
//   
//   
//    
//}

-(void)createMyNav
{
    UILabel *titleLabel=[self addNavTitle:CGRectMake(0, 0, 335, 49) title:@"车辆管理"];
    titleLabel.textColor=[UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:35.0f/255.0f green:70.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
    
    UIButton *leftBtn=[self addNavBtn:CGRectMake(0, 0, 9, 15) title:nil target:self action:@selector(gotoBack) isLeft:YES];
    [leftBtn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    
    UIButton *rightBtn=[self addNavBtn:CGRectMake(0, 0, 40,20) title:@"添加" target:self action:@selector(gotoAdd) isLeft:NO];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


-(void)gotoBack
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.tabBarController.tabBar.hidden=NO;
   
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:35.0f/255.0f green:70.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
}
-(void)gotoAdd
{
    AddCarViewController  *addCarVC=[[AddCarViewController alloc]init];
    addCarVC.delegate = self;
    [self.navigationController pushViewController:addCarVC animated:YES];
}

-(void)setupHeaderView
{
    MJRefreshNormalHeader *header=[MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefreshing)];
    self.carTableView.mj_header=header;
}
-(void)headerRefreshing
{
    self.nextAction = @selector(headerRefreshing);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *params=[NSString stringWithFormat:@"time=%@",@""];
     NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_LIST_API];
    WEAKSELF
    [[NetworkManager sharedInstance]startEncryptRequest:url requestMethod:POST params:params success:^(id responseData) {
        STRONGSELF
        [strongSelf.carTableView.mj_header endRefreshing];
        id code = responseData[@"code"];
        if (IsNilOrNull(code)) {
            return;
        }
        NSInteger code_status = [responseData[@"code"] integerValue];
        if (code_status == STATUS_OK) {
            [self resetAction];
            
            dataArray=[responseData valueForKey:@"data"];
            NSLog(@"%@",dataArray);
            [strongSelf.carTableView reloadData];
            self.carTableView.mj_footer.hidden=self.carTableView.frame.size.height<ViewHeight?YES:NO;
        }
        else
        {
            
        }
    } andFailure:^(NSString *errorDesc) {
        [self.carTableView.mj_header endRefreshing];
    }];
}
-(void)setupFooterView
{
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    footer.triggerAutomaticallyRefreshPercent = 1;
    footer.automaticallyHidden = YES;
    self.carTableView.mj_footer = footer;
}

-(void)loadMoreData
{
    
}
#pragma mark ----UITableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    CarTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    cell.backgroundColor=[UIColor whiteColor];
    if (cell==nil) {
        cell=[[CarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    if ([[dataArray[indexPath.section] valueForKey:@"inspectionDate"] isKindOfClass:[NSNull class]]) {
        timeStr=@" ";
    }
    else
    {
        NSDate *confrom =[NSDate dateWithTimeIntervalSince1970:[[dataArray[indexPath.section] valueForKey:@"inspectionDate"] integerValue]/1000];
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        timeStr =[dateFormatter stringFromDate:confrom];
        
    }
    if ([[dataArray[indexPath.section] valueForKey:@"insuranceDate"] isKindOfClass:[NSNull class]]) {
        dayStr=@" ";
        
    }
    else
    {
        NSDate *day=[NSDate dateWithTimeIntervalSince1970:[[dataArray[indexPath.section] valueForKey:@"insuranceDate"] integerValue]/1000];
        NSDateFormatter *dateFor=[[NSDateFormatter alloc]init];
        [dateFor setDateFormat:@"yyyy-MM-dd"];
        dayStr=[dateFor stringFromDate:day];
        
    }
    uid=[dataArray[indexPath.section] valueForKey:@"id"];
    //cell.carModelLbl.text = [dataArray[indexPath.section] valueForKey:@"model"];
    cell.driverLbl.text=[dataArray[indexPath.section] valueForKey:@"driver"];
    cell.carNumLbl.text = [dataArray[indexPath.section] valueForKey:@"number"];
    cell.telLbl.text = [dataArray[indexPath.section]valueForKey:@"mobile"];

    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}
//删除操作
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //获取到相应cell
    cellView=(CarTableViewCell *)[self.carTableView cellForRowAtIndexPath:indexPath];
    
        if (cellView.editingStyle==UITableViewCellEditingStyleDelete)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"确定删除" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
            [alert show];
            uid=[dataArray[indexPath.section] valueForKey:@"id"];
      //  }
       
    }
}
//设置分区头视图
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectZero];
    headerView.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
    return headerView;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        NSLog(@"确定");

                // 汽车信息获取
                NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_Delete_API];
                
                NSString *parmas = [NSString stringWithFormat:@"id=%@", uid];
                NSLog(@"%@&&&&&&&&&&",uid);
                
                [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
                    
                    NSLog(@"response success");
                    [self.carTableView reloadData];
                    [self setupHeaderView];
                    [self.carTableView.mj_header beginRefreshing];
                    
                    NSLog(@"result = %@", responseData);
                    
                } andFailure:^(NSString *errorDesc) {
                    
                    NSLog(@"response failure");

                }];
                
            }
            
    else
    {
        NSLog(@"取消");

        }
 
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    EditCarViewController *editVC=[[EditCarViewController alloc]init];
    if ([[dataArray[indexPath.section] valueForKey:@"inspectionDate"] isKindOfClass:[NSNull class]]) {
        timeStr=@" ";
    }
    else
    {
        NSDate *confrom =[NSDate dateWithTimeIntervalSince1970:[[dataArray[indexPath.section] valueForKey:@"inspectionDate"] integerValue]/1000];
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        timeStr =[dateFormatter stringFromDate:confrom];
        
    }
    if ([[dataArray[indexPath.section] valueForKey:@"insuranceDate"] isKindOfClass:[NSNull class]]) {
        dayStr=@" ";
        
    }
    else
    {
        NSDate *day=[NSDate dateWithTimeIntervalSince1970:[[dataArray[indexPath.section] valueForKey:@"insuranceDate"] integerValue]/1000];
        NSDateFormatter *dateFor=[[NSDateFormatter alloc]init];
        [dateFor setDateFormat:@"yyyy-MM-dd"];
        dayStr=[dateFor stringFromDate:day];
        
    }
    uid=[dataArray[indexPath.section] valueForKey:@"id"];
    editVC.delegate = self;
    editVC.uid=uid;
    //    NSLog(@"%@",uid);
    
    [self.navigationController pushViewController:editVC animated:YES];

}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *title=[MyUtil createLabelFrame:CGRectMake(0, 0, 20, 30) title:@"删除" font:[UIFont systemFontOfSize:8] textAlignment:NSTextAlignmentCenter numberOfLines:0 textColor:[UIColor whiteColor]];
    return title.text;
}


@end
