//
//  CustomRadioButtonExt.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/6.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CustomRadioButtonGroup.h"
#import "CustomRadioButton.h"
#import "BarginAndPayResponseData.h"



@implementation CustomRadioButtonGroup

-(id)initWithFrame:(CGRect)frame data:(NSArray*)data;
{
    self = [super initWithFrame:frame];
    if (self) {
        NSInteger count = data.count;
        NSInteger row = count%ROW_COUNT==0?count/ROW_COUNT:count/ROW_COUNT+1;// 多少行;
        
        NSInteger marginRow = 20;// 行间距
        NSInteger startX = 20;//(ViewWidth - BUTTON_WIDTH * ROW_COUNT - marginRow)/2;
        NSInteger buttonWidth = (ViewWidth - startX * 2 - marginRow)/2;
        
        for (NSInteger i = 0; i < row; i++) {
            for (NSInteger j = 0; j < ROW_COUNT; j++) {
                NSInteger index = i * ROW_COUNT + j;
                if (index < data.count) {
                    CustomRadioButton *button = [CustomRadioButton customView];
                    button.frame = CGRectMake(startX + j * (buttonWidth + marginRow), START_Y + i * (BUTTON_HEIGHT + MARGIN_COLUMN), buttonWidth, BUTTON_HEIGHT);
                    RateModel *item = data[i * ROW_COUNT + j];
                    button.textLabel.text = [NSString stringWithFormat:@"¥%@ %@", item.price, item.name];
                    button.clickButton.tag = i * ROW_COUNT + j;
                    [button.clickButton addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
                    button.tag = i * ROW_COUNT + j;
                    [self.customButtonArray addObject:button];
                    [self addSubview:button];
                }
            }
        }
    }
    return self;
}

-(NSMutableArray*)customButtonArray
{
    if (_customButtonArray == nil) {
        _customButtonArray = [NSMutableArray new];
    }
    return _customButtonArray;
}

-(void)onClick:(UIButton*)button
{
    if (_lastButton != button) {
        
//        CustomRadioButton *customRadioButton = self.customButtonArray[
        _lastButton.selected = NO;
        button.selected = YES;
        
        NSInteger lastIndex = _lastButton.tag;
        NSInteger currentIndex = button.tag;
        
        CustomRadioButton *lastRadioButton = self.customButtonArray[lastIndex];
        lastRadioButton.isSelected = NO;
        
        CustomRadioButton *curRadioButton = self.customButtonArray[currentIndex];
        curRadioButton.isSelected = YES;
        
        
        _lastButton = button;
        
        if (self.delegate /*&& [self.delegate respondsToSelector:@selector(onClickButton:)]*/) {
            [self.delegate onClickButton:button.tag];
        }
        

    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
