//
//  YYAliyunOSSImageOperation.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AliyunOSSManager.h"

#if __has_include(<YYWebImage/YYWebImage.h>)
#import <YYWebImage/YYImageCache.h>
#import <YYWebImage/YYWebImageManager.h>
#else
#import "YYImageCache.h"
#import "YYWebImageManager.h"
#endif

NS_ASSUME_NONNULL_BEGIN


@interface YYAliyunOSSImageOperation : NSOperation

@property (nonatomic, strong) OSSGetObjectRequest * _Nullable getObjectRequest;

@property (nonatomic, strong) NSString * _Nullable objectKey;

@property (nullable, nonatomic, strong, readonly) YYImageCache *cache;

@property (nullable, nonatomic, strong) NSString *cacheKey;

@property (nonatomic, assign) CGSize imageSize;

-(instancetype _Nullable )initWithRequestUrl:(NSString*_Nullable)objectKey
//                                     options:(YYWebImageOptions)options
                                     imgSize:(CGSize)imgSize
                                       cache:(YYImageCache*_Nullable)cache
                                    cacheKey:(NSString*_Nullable)cacheKey
                                    progress:(YYWebImageProgressBlock _Nullable )progress
                                  completion:(YYAliyunOSSImageCompletionBlock _Nullable )completion;



@end

NS_ASSUME_NONNULL_END
