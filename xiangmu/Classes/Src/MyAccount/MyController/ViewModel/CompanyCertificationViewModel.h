//
//  CompanyCertificationViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 16/10/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyCertifcationHttpMessage.h"

@interface CompanyCertificationViewModel : NSObject

@property (nonatomic, assign) BOOL canEditAreaName; // 是否可以修改公司所在地区
@property (nonatomic, assign) BOOL canEditCompanyName; // 是否可以修改公司名称
@property (nonatomic, assign) BOOL canEditCompanyAddress; // 是否可以修改经营地址
@property (nonatomic, assign) BOOL canEditPhoto; // 是否可以修改营业执照
@property (nonatomic, assign) BOOL isInit;// 第一次提交认证信息

- (void)getCompanyCertificationInfo:(SuccessBlock)success failure:(FailureBlock)failure;

- (BOOL)validateCompanyCertificationInfo:(CompanyCertifcationHttpMessage*)httpMessage errorMsg:(NSString**)errorMsg;

@end
