//
//  MyDetailTableViewController.h
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AvatarViewController.h"
#import "RegisterUserInfoViewController.h"

//@protocol MyDetailViewControllerDelegate <NSObject>
//
//@optional
//
////刷新个人信息
//- (void)refreshMyInfo:(NSIndexPath *)indexpath;
//
//@end


typedef NS_ENUM(NSInteger, UserInfoRow){
    UserInfoRowAvatar = 0,
    UserInfoRowNickName,
    UseerInfoRowMobile,
    UserInfoRowCompanyName,
    UsrInfoBindEmail
//    UserInfoRowIdCertification,
//    UserInfoRowCompanyCertification
};

typedef NS_ENUM(NSInteger, MeCertificationRow) {
    MeCertificationRowLevel = 0,                // 等级
    MeCertificationRowCertificationInfo,    // 认证信息
    MeCertificationRowRealname,// 实名认证
    MeCertificationRowCompany,  // 企业认证
    MeCertificationRowVechile,   // 车辆认证
    MeCertificationServiceProvier   // 服务商认证
};

#define UserInfoRowNumber 5
#define MeCertificationNumber 4


@interface UserInfoViewController : BaseViewController<AvatarDelegate>
{
    //头像
    UIImageView *avatarImg;
}

@property (nonatomic,weak) id<MyDetailViewControllerDelegate> personDelegate;

@property (nonatomic,strong) NSIndexPath *indexpath;

@property (nonatomic, strong) NSData *avatarImageData;

//@property (nonatomic, strong) NSString *imageFileContents;// BASE64编码后的图片数据


@end
