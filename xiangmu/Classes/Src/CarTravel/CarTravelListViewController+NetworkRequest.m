//
//  CarTravelListViewController+NetworkRequest.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/27.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "CarTravelListViewController+NetworkRequest.h"

@implementation CarTravelListViewController (NetworkRequest)

-(void)fetchOrderList:(NSUInteger)listStatus time:(NSString*)time;
{
    if (IsStrEmpty(time)) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    NSString *params = [NSString stringWithFormat:@"listStatus=%lu&time=%@", (unsigned long)listStatus, [StringUtil getSafeString:time]];
    
    self.loadStatus = LoadStatusInit;
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(MY_TRAVEL_ORDER_LIST_API) requestMethod:POST params:params success:^(NSDictionary * responseData) {
        
        [weakSelf.pullRefreshTableView endHeaderRefresh];
        [weakSelf.pullRefreshTableView endFooterRefresh];
        [weakSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            NSArray *dataList = responseData[@"data"];
            for (NSDictionary *dictionary in dataList) {
                
//                GrabOrderModel *model = [[GrabOrderModel alloc] initWithDictionary:dictionary error:nil];
//                [weakSelf.dataList addObject:model];
                
                TravelOrderModel *model = [[TravelOrderModel alloc] initWithDictionary:dictionary error:nil];
                [weakSelf.dataList addObject:model];
                
            }
            
            [weakSelf.pullRefreshTableView reloadData];
            
            weakSelf.loadStatus = LoadStatusFinish;
            
            
        }
        else {
            
            weakSelf.loadStatus = LoadStatusFinish;
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf resetAction];
        weakSelf.loadStatus = LoadStatusNetworkError;
        
        
    }];
}


@end
