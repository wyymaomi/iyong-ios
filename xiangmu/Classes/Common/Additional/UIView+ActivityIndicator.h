//
//  UIView+ActivityIndicator.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/21.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

#define hudViewTag                     0x98751235

@interface UIView (ActivityIndicator)

- (void)showHUDIndicatorViewAtCenter:(NSString *)indiTitle;

- (void)hideHUDIndicatorViewAtCenter;

@end
