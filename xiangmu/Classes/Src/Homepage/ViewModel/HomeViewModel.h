//
//  HomeViewModel.h
//  xiangmu
//
//  Created by 湛思科技 on 17/3/3.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewModel.h"
#import "OnlineServiceModel.h"

@interface HomeViewModel : BaseViewModel

@property (nonatomic, strong) NSMutableArray *advList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *recommendList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *providerList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *experienceList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *beijingList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *shanghaiList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *hangzhouList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *shenzhenList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *gpsCityList;// GPS定位城市数据

@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *weddingList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *travelList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *commercialList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *airportList;
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *busServiceList; // 企业班车
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *charteredList; // 包车服务
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *goodsList; // 货运用车
@property (nonatomic, strong) NSMutableArray<OnlineServiceModel*> *truckList; // 拖车服务

@property (nonatomic, strong) NSMutableDictionary *providerListDict;

@end
