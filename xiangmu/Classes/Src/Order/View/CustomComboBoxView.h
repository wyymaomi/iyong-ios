//
//  CustomComboBoxView.h
//  xiangmu
//
//  Created by 湛思科技 on 16/5/8.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomComboBoxView : UIView

@property (strong, nonatomic) UILabel *textLabel;
@property (strong, nonatomic) UIImageView *imageView;

@end
