//
//  RealnameCertificationViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/6/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "RealnameCertificationViewController.h"
#import "BindBankcardViewController.h"
#import "UserHelper.h"

@interface RealnameCertificationViewController ()

@property (weak, nonatomic) IBOutlet UITextField *realNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *idcardTextField;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) UIButton *pointBtn;

@end

@implementation RealnameCertificationViewController

-(UIButton*)pointBtn
{
    if (!_pointBtn) {
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        _pointBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _pointBtn.frame = CGRectMake(0, screenHeight, 106, 53);
        //        _pointBtn.frame = CGRectMake(0, screenHeight-53, 106, 53);
        _pointBtn.backgroundColor = [UIColor clearColor];
        //        _pointBtn.borderWidth = 0.5;
        //        _pointBtn.borderColor = UIColorFromRGB(0xCCCCCC);
        _pointBtn.titleLabel.font = FONT(18);
        [_pointBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_pointBtn setTitle:@"X" forState:UIControlStateNormal];
        [_pointBtn addTarget:self action:@selector(pointAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _pointBtn;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
//    self.navigationItem.rightBarButtonItem = UINavigatio
    
    //注册通知
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (self.type == RealnameModeEdit) {
        if ([self.realNameTextField canBecomeFirstResponder]) {
            [self.realNameTextField becomeFirstResponder];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"实名认证";
    
    
    
    //    self.idcardTextField.text = [StringUtil encodeBindIdcardNo:@"31011020160702222X"];
    //    self.idcardTextField.delegate = self;
    //    [self.idcardTextField addTarget:self action:@selector(editingDidBegin:) forControlEvents:UIControlEventEditingDidBegin];
    
    
//    
    
    if (self.type == RealnameModeNoEdit) {
        self.realNameTextField.enabled = NO;
        self.idcardTextField.enabled = NO;
        
//        self.okButton.hidden = YES;
        
        NSString *idCardNo = [[UserManager sharedInstance].userModel idNumber];
        self.idcardTextField.text = [StringUtil encodeBindIdcardNo:idCardNo];
        self.realNameTextField.text = [[UserManager sharedInstance].userModel realName];
        
    }
    else {
        self.idcardTextField.text = @"";
        self.realNameTextField.text = @"";
        
        self.realNameTextField.enabled = YES;
        self.idcardTextField.enabled = YES;
//        self.okButton.hidden  = NO;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(onClickOKButton)];
        
        
    }
    
//    [self.okButton blueStyle];
//    [self.okButton addTarget:self action:@selector(onClickOKButton) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setCancelBlock:(BBBasicBlock)block
{
    _cancelBlock = [block copy];
}

- (void)setConfirmBlock:(BBBasicBlock)block
{
    _confirmBlock = [block copy];
}

-(void)onClickOKButton
{
    self.nextAction = @selector(onClickOKButton);
    self.object1 = nil;
    self.object2 = nil;
    
    NSString *errorMsg;
    
    if (IsStrEmpty(self.realNameTextField.text.trim)) {
        [self showAlertView:@"请输入姓名"];
        return;
    }
    
    if (![UserHelper validateIdcard:self.idcardTextField.text.trim error:&errorMsg]) {
        [self showAlertView:errorMsg];
        return;
    }
    
    NSString *realNameText = self.realNameTextField.text.trim;
    NSString *idCardText = self.idcardTextField.text.trim;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, IDCARD_CERTIFICATION_API];
    NSString *params = [NSString stringWithFormat:@"name=%@&idNumber=%@", realNameText, idCardText];
    
    [self showHUDIndicatorViewAtCenter:MSG_IDCARD_CERTIFICATIONING];
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseData) {
        StrongSelf
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf resetAction];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        
        
        if (code_status == STATUS_OK) {
            
            // 更新身份证信息
            [[NetworkManager sharedInstance] getUserInfo:nil andFailue:nil];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"实名认证成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    
//                    [UserManager sharedInstance].realName = self.realNameTextField.text.trim;
//                    [UserManager sharedInstance].idcardNo = self.idcardTextField.text.trim;
//                    [UserManager sharedInstance].isIdcardCertificationed = YES;
                    
                    [UserManager sharedInstance].userModel.certification = [NSNumber numberWithBool:YES];
                    [UserManager sharedInstance].userModel.idNumber = idCardText;//self.idcardTextField.text.trim;
                    [UserManager sharedInstance].userModel.realName = realNameText;//self.realNameTextField.text.trim;
                    
                    if (strongSelf.type == 2) {
                        
                        BindBankcardViewController *viewController = [[BindBankcardViewController alloc] init];
                        viewController.bankcardName = realNameText;
                        [strongSelf.navigationController pushViewController:viewController animated:YES];
                        
                    }
                    else {

                        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(onRealnameCertificationResult:realName:idcardNo:)]) {
                            [strongSelf.delegate onRealnameCertificationResult:YES realName:strongSelf.realNameTextField.text.trim idcardNo:strongSelf.idcardTextField.text.trim];
                        }
                        
                        [strongSelf pressedBackButton];
                        
                    }

                }
                
            }];
        }
        else if (code_status ==STATUS_FAIL) {
            NSString *msg = responseData[@"data"];
            if (IsStrEmpty(msg)) {
                msg = @"实名认证失败，请重新输入";
            }
            [strongSelf showAlertView:msg];
        }
        else {
            [MsgToolBox showToast:getErrorMsg(code_status)];
//            [strongSelf showAlertView:[strongSelf getErrorMsg:code_status]];
        }
    } andFailure:^(NSString *errorDesc) {
        StrongSelf
        [strongSelf resetAction];
        [strongSelf hideHUDIndicatorViewAtCenter];
        [strongSelf showAlertView:errorDesc];
        
    }];
    
}


#pragma mark -
- (void)pointAction {
    //    if (![_textF.text isEqualToString:@""] && ![_textF.text containsString:@"."]) {
    //        [_textF insertText:_pointBtn.titleLabel.text];
    //    }
    //    if (![self.idcardTextField.text isEqualToString:@""]) {
    [self.idcardTextField insertText:self.pointBtn.titleLabel.text];
    //    }
}

#pragma mark - 处理TextField响应事件
- (void)editingDidBegin:(UITextField *)textField
{
    if (textField == self.idcardTextField && self.idcardTextField.keyboardType == UIKeyboardTypeNumberPad) {
        UIWindow *keyboardWindow = nil;
        NSArray *windows = [[UIApplication sharedApplication] windows];
        BOOL version_iOS_9 = floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_3;
        
        for (UIWindow *window in windows) {
            if (version_iOS_9) {
                if ([NSStringFromClass([window class]) isEqualToString:@"UIRemoteKeyboardWindow"]) {
                    keyboardWindow = window;
                    [keyboardWindow addSubview:self.pointBtn];
                    break;
                }
            }
            else {
                if ([NSStringFromClass([window class]) isEqualToString:@"UITextEffectsWindow"]) {
                    keyboardWindow = window;
                    [keyboardWindow addSubview:self.pointBtn];
                    break;
                }
            }
        }
    }
    
    
}

#pragma mark - 处理键盘通知

- (void)keyboardWillShow:(NSNotification *)notification {
    if ([self.realNameTextField isFirstResponder]) {
        if (_pointBtn) {
            if (_pointBtn.superview) {
                [_pointBtn removeFromSuperview];
                _pointBtn = nil;
            }
        }
    }
    
    if ([self.idcardTextField isFirstResponder] && self.idcardTextField.keyboardType == UIKeyboardTypeNumberPad) {
        
        NSDictionary *userInfo = [notification userInfo];
        //通知的userInfo中包含了键盘的动画时间和动画类型
        CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        NSInteger animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
        
        if (_pointBtn == nil) {
            UIWindow *keyboardWindow = nil;
            NSArray *windows = [[UIApplication sharedApplication] windows];
            BOOL version_iOS_9 = floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_3;
            
            for (UIWindow *window in windows) {
                if (version_iOS_9) {
                    if ([NSStringFromClass([window class]) isEqualToString:@"UIRemoteKeyboardWindow"]) {
                        keyboardWindow = window;
                        [keyboardWindow addSubview:self.pointBtn];
                        break;
                    }
                }
                else {
                    if ([NSStringFromClass([window class]) isEqualToString:@"UITextEffectsWindow"]) {
                        keyboardWindow = window;
                        [keyboardWindow addSubview:self.pointBtn];
                        break;
                    }
                }
            }
        }
        
        
        if (_pointBtn) {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:animationDuration];
            [UIView setAnimationCurve:animationCurve];
            _pointBtn.transform = CGAffineTransformTranslate(_pointBtn.transform, 0, -53);
            [UIView commitAnimations];
        }
        
    }
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    if ([self.idcardTextField isFirstResponder]) {
        
        NSDictionary *userInfo = [notification userInfo];
        CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        
        if (_pointBtn.superview) {
            [UIView animateWithDuration:animationDuration animations:^{
                _pointBtn.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                [_pointBtn removeFromSuperview];
                _pointBtn = nil;
            }];
        }
        
    }
    
}

#pragma mark - 处理视图响应事件
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
////    [_textF resignFirstResponder];
//    if ([self.idcardTextField isFirstResponder] && [self.idcardTextField canResignFirstResponder]) {
//        [self.idcardTextField resignFirstResponder];
//    }
//    if ([self.realNameTextField isFirstResponder] && [self.realNameTextField canResignFirstResponder]) {
//        [self.realNameTextField resignFirstResponder];
//    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
