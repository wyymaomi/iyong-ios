//
//  EditCarViewController.m
//  xiangmu
//
//  Created by David kim on 16/3/30.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "EditCarViewController.h"
#import "MyUtil.h"
#import "NetworkManager.h"
#import "AFNetworking.h"
#import "OrderViewController.h"
#import "UserManager.h"
#import "HttpConstants.h"
#import "AddCarDetailViewController.h"
#import "AddCarTableViewCell.h"

@interface EditCarViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArray;
    NSString *dateStr;
    NSString *dayStr;
    NSString *timeStr;
    //表示选中的是哪个textField
    NSInteger _index;
    NSIndexPath *sIndexPath;
}
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *button;
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation EditCarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    dateStr=[formatter stringFromDate:[NSDate date]];
    
    
    [self createMyNav];
    [self down];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addSuccess:) name:@"addSuccess" object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ageSuccess:) name:@"ageSuccess" object:nil];
    [self.view addSubview:self.tableView];
    //[self downloadData];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
//-(void)addSuccess:(NSNotification *)noti
//{
//    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:4 inSection:0];
//    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//    cell.rightLabel.text=noti.object;
//}
//-(void)ageSuccess:(NSNotification *)noti
//{
//    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:5 inSection:0];
//    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//    cell.rightLabel.text=noti.object;
//}
-(void)AgeRow:(NSInteger)row WithTitle:(NSString *)title
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:5 inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=title;
}
-(void)ChangeRow:(NSInteger)row WithTitle:(NSString *)title
{
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:4 inSection:0];
        AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        cell.rightLabel.text=title;
}
-(void)getRow:(NSInteger)row WithTitle:(NSString *)title
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=title;
    if (title.length==0) {
        if (row==1) {
            cell.rightLabel.text=@"请填写车";
        }
        else if (row==2)
        {
            cell.rightLabel.text=@"请填写车牌号";
        }
        else if (row==3)
        {
            cell.rightLabel.text=@"请填姓名";
        }
        else if (row==6)
        {
            cell.rightLabel.text=@"请填写手机号";
        }
    }
}
-(void)ChangeRow:(NSInteger)row WithSex:(NSString *)sex
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=sex;
}
-(void)KnowRow:(NSInteger)row WithDate:(NSString *)date
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:row inSection:1];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.rightLabel.text=date;
}
-(void)createMyNav
{
    self.titleLabel=[self addNavTitle:CGRectMake(0, 0, 335, 49) title:@"编辑车辆"];
    self.titleLabel.textColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:35.0f/255.0f green:70.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.translucent=NO;
    UIButton *btn=[self addNavBtn:CGRectMake(0, 0, 9, 15) title:nil target:self action:@selector(gotoBack) isLeft:YES];
    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    self.button=[self addNavBtn:CGRectMake(0, 0, 40, 20) title:@"保存" target:self action:@selector(gotoSave) isLeft:NO];
    
    [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
-(UITableView *)tableView
{
    if (_tableView == nil) {
        self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, ViewHeight) style:UITableViewStyleGrouped];
        //560
        self.tableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        self.tableView.backgroundColor=[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
        self.tableView.scrollEnabled=YES;
        self.tableView.delegate=self;
        self.tableView.dataSource=self;
        self.tableView.showsVerticalScrollIndicator=NO;
        self.tableView.tableFooterView=[[UIView alloc]init];
    }
    return _tableView;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


//使直线没有前后空隙
-(void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark----UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 7;
    }
    else
    {
        return 2;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return 80;
        }
        return 50;
    }
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        return 20;
    }
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellid=@"cellid";
    AddCarTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    cell.backgroundColor=[UIColor whiteColor];
    if (cell==nil) {
        cell=[[AddCarTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    if (indexPath.section==0) {
        NSArray *nameArray=@[@"头   像",@"车   型",@"车   牌",@"姓   名",@"年   龄",@"性   别",@"电   话"];
        cell.textLabel.textColor=[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
        cell.textLabel.font=[UIFont systemFontOfSize:13];
        cell.textLabel.text=nameArray[indexPath.row];
        if (indexPath.row==0) {
            avatarImg=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-150, 10, 100, 60)];
            
            UITapGestureRecognizer *showGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showAlert:)];
            [showGestureRecognizer setNumberOfTapsRequired:1];
            [avatarImg addGestureRecognizer:showGestureRecognizer];
            
            avatarImg.userInteractionEnabled=YES;
            if ([[NSUserDefaults standardUserDefaults]objectForKey:@"imagePath"])
            {
                NSString *imagePath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"] stringByAppendingPathComponent:@"nowImage.png"];
                
                NSLog(@"%@",[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"] stringByAppendingPathComponent:@"nowImage.png"]);
                
                avatarImg.image = [UIImage imageWithContentsOfFile:imagePath];
                avatarImg.layer.masksToBounds=YES;
                // self.imageView.backgroundColor=[UIColor whiteColor];
                //设置圆角
                avatarImg.layer.cornerRadius=15;
            }
            else
            {
                avatarImg.image=[UIImage imageNamed:@"帅帅的车.jpg"];
                avatarImg.layer.masksToBounds=YES;
                // self.imageView.backgroundColor=[UIColor whiteColor];
                //设置圆角
                avatarImg.layer.cornerRadius=15;
            }
            
            [cell addSubview:avatarImg];
        }
        else
        {
            [self down];
        }
        
    }
    
    else
    {
        NSArray *nameArray=@[@"年检日期",@"保险日期"];
        cell.textLabel.textColor=[UIColor colorWithRed:100.0f/255.0f green:98.0f/255.0f blue:99.0f/255.0f alpha:1.0f];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
        cell.textLabel.text=nameArray[indexPath.row];
    }
    //显示最右边的箭头
   // cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
-(void)showAlert:(UITapGestureRecognizer *)tap
{
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"提示" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
    UIAlertAction *fromPhotoAction=[UIAlertAction actionWithTitle:@"从手机相册中选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIImagePickerController *imagePicker=[[UIImagePickerController alloc]init];
        imagePicker.delegate=self;
        imagePicker.allowsEditing=YES;
        imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    UIAlertAction *fromCameraAction=[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIImagePickerController *imagePicker=[[UIImagePickerController alloc]init];
        imagePicker.delegate=self;
        imagePicker.allowsEditing=NO;
        imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:fromCameraAction];
    [alertController addAction:fromPhotoAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0&&indexPath.row==0) {
       
        
    }
    else
    {
        AddCarTableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
        AddCarDetailViewController *Addcardetail=[[AddCarDetailViewController alloc]init];
        Addcardetail.name=[NSString stringWithFormat:@"%@",cell.textLabel.text];
        Addcardetail.rightname=[NSString stringWithFormat:@"%@",cell.rightLabel.text];
        Addcardetail.type=SearchTypeEdit;
        NSLog(@"%@",Addcardetail.name);
        Addcardetail.delegate=self;
        Addcardetail.section=indexPath.section;
        Addcardetail.row=indexPath.row;
        [self.navigationController pushViewController:Addcardetail animated:YES];
    }
}
//选好图片之后
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    //在选择之后,先退出图片选择
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image =[info objectForKey:UIImagePickerControllerOriginalImage];
    [self saveImage:image withName:@"帅帅的车.jpg"];
    NSString *fullpath1 =[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/"] stringByAppendingPathComponent:@"帅帅的车.jpg"];
    [avatarImg setImage:[[UIImage alloc]initWithContentsOfFile:fullpath1]];
    
    NSUserDefaults *ud1 =[NSUserDefaults standardUserDefaults];
    [ud1 setObject:fullpath1 forKey:@"imagePath"];
    [ud1 synchronize];
    
    //    //这里的token是要和个人相关联的
    //    NSString *a2=(NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)token, (CFStringRef)@"!*'();:@&=+$,/?%#[]", (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    //
    //    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    NSString *s1=[NSString stringWithFormat:@"http://mgjt.hzgames.cn:8080/horse_tvpartner/account2/avatar?token=%@",a2];
    //    [manager POST:s1 parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    //     {
    //         [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 1)name:@"avatar" fileName:@"image" mimeType:@"image/jpeg"];
    //
    //     }success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //         NSLog(@"Success: %@", responseObject);
    //     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //         NSLog(@"Error: %@", error);
    //     }];
    
    [self saveImage:image withName:@"nowImage.png"];
    NSString *fullpath=[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"]stringByAppendingPathComponent:@"nowImage.png"];
    [avatarImg setImage:[[UIImage alloc]initWithContentsOfFile:fullpath]];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:fullpath forKey:@"imagePath"];
    [ud synchronize];
    NSLog(@"图片路径是：%@",fullpath);
}

//存储图片
- (void) saveImage:(UIImage *)currentImage withName:(NSString *)imageName
{
    NSData *imageData =UIImageJPEGRepresentation(currentImage, 0.5);
    //获取沙盒目录
    NSString *fullPath =[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"] stringByAppendingPathComponent:imageName];
    //将图片写入文件
    [imageData writeToFile:fullPath atomically:YES];
}

-(void)down
{
            NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_Query_API];
            NSString *parmas = [NSString stringWithFormat:@"id=%@", self.uid];
            
            [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
                
                NSLog(@"response success");
                dataArray  = [responseData valueForKey:@"data"];
                
                NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:1 inSection:0];
                AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath1];
                cell.rightLabel.text=[dataArray valueForKey:@"model"];
//                if ([dataArray valueForKey:@"model"]==nil) {
//                    cell.rightLabel.text=@"1";
//                }
                
                NSIndexPath *indexPath2=[NSIndexPath indexPathForRow:2 inSection:0];
                AddCarTableViewCell *cell1=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath2];
                cell1.rightLabel.text=[dataArray valueForKey:@"number"];
                
                NSIndexPath *indexPath3=[NSIndexPath indexPathForRow:3 inSection:0];
                AddCarTableViewCell *cell2=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath3];
                cell2.rightLabel.text=[dataArray valueForKey:@"driver"];
                
                NSIndexPath *indexPath7=[NSIndexPath indexPathForRow:4 inSection:0];
                AddCarTableViewCell *cell7=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath7];
                cell7.rightLabel.text=[dataArray valueForKey:@"age"];
                
                NSIndexPath *indexPath8=[NSIndexPath indexPathForRow:5 inSection:0];
                AddCarTableViewCell *cell8=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath8];
                if ([[dataArray valueForKey:@"sex"]integerValue]==1) {
                    cell8.rightLabel.text=@"男";
                }
                else
                {
                    cell8.rightLabel.text=@"女";
                }
                
                
                NSIndexPath *indexPath4=[NSIndexPath indexPathForRow:6 inSection:0];
                AddCarTableViewCell *cell3=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath4];
                cell3.rightLabel.text=[dataArray valueForKey:@"mobile"];
                
                NSIndexPath *indexPath5=[NSIndexPath indexPathForRow:0 inSection:1];
                AddCarTableViewCell *cell4=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath5];
                
                                if ([[dataArray valueForKey:@"inspectionDate"] isKindOfClass:[NSNull class]]) {
                                    cell4.rightLabel.text=@"选择日期";
                                }
                                else
                                {
                                    NSDate *confrom =[NSDate dateWithTimeIntervalSince1970:[[dataArray valueForKey:@"inspectionDate"] integerValue]/1000];
                                    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
                                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                                    NSString *timeStr =[dateFormatter stringFromDate:confrom];
                                    cell4.rightLabel.text=timeStr;
                                }
                NSIndexPath *indexPath6=[NSIndexPath indexPathForRow:1 inSection:1];
                AddCarTableViewCell *cell5=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath6];
                                if ([[dataArray valueForKey:@"insuranceDate"] isKindOfClass:[NSNull class]]) {
                                    cell5.rightLabel.text=@"选择日期";
                                }
                                else
                                {
                                    NSDate *day=[NSDate dateWithTimeIntervalSince1970:[[dataArray valueForKey:@"insuranceDate"] integerValue]/1000];
                                    NSDateFormatter *dateFor=[[NSDateFormatter alloc]init];
                                    [dateFor setDateFormat:@"yyyy-MM-dd"];
                                    NSString *dayStr=[dateFor stringFromDate:day];
                                    cell5.rightLabel.text=dayStr;
                                }

                
                NSLog(@"result = %@", responseData);
                
            } andFailure:^(NSString *errorDesc) {
                
                NSLog(@"response failure");
            }];
            
        }
-(void)downloadData
{
    NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:1 inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath1];
    
    
    NSIndexPath *indexPath2=[NSIndexPath indexPathForRow:2 inSection:0];
    AddCarTableViewCell *cell1=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath2];
    
    
    NSIndexPath *indexPath3=[NSIndexPath indexPathForRow:3 inSection:0];
    AddCarTableViewCell *cell2=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath3];
    
    NSIndexPath *indexPath7=[NSIndexPath indexPathForRow:4 inSection:0];
    AddCarTableViewCell *cell7=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath7];
    
    NSIndexPath *indexPath8=[NSIndexPath indexPathForRow:5 inSection:0];
    AddCarTableViewCell *cell6=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath8];
    if ([cell6.rightLabel.text isEqualToString:@"男"]) {
        cell6.rightLabel.text=@"1";
    }
    else
    {
        cell6.rightLabel.text=@"0";
    }
    
    
    NSIndexPath *indexPath4=[NSIndexPath indexPathForRow:6 inSection:0];
    AddCarTableViewCell *cell3=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath4];
    
    NSIndexPath *indexPath5=[NSIndexPath indexPathForRow:0 inSection:1];
    AddCarTableViewCell *cell4=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath5];
    
    NSIndexPath *indexPath6=[NSIndexPath indexPathForRow:1 inSection:1];
    AddCarTableViewCell *cell5=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath6];
    
    if ([cell4.rightLabel.text isEqualToString:@"选择日期"]) {
        cell4.rightLabel.text=@" ";
        
    }
    if ([cell5.rightLabel.text isEqualToString:@"选择日期"]) {
        cell5.rightLabel.text=@" ";
    }
//    else
//    {
//        
//    }
    

 // 汽车信息获取
            NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, CAR_Update_API];
            NSString *parmas = [NSString stringWithFormat:@"model=%@&number=%@&driver=%@&age=%@&sex=%d&mobile=%@&inspectionDate=%@&insuranceDate=%@&id=%@",cell.rightLabel.text,cell1.rightLabel.text ,cell2.rightLabel.text,cell7.rightLabel.text,[cell6.rightLabel.text integerValue],cell3.rightLabel.text,cell4.rightLabel.text,cell5.rightLabel.text,self.uid];
            
            [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
                
                NSLog(@"response success");
               
                if ([cell.rightLabel.text isEqualToString:@"请填写车"]||[cell1.rightLabel.text isEqualToString:@"请填写车牌号"]||[cell2.rightLabel.text isEqualToString:@"请填姓名"]||[cell6.rightLabel.text isEqualToString:@"请填写手机号"])
                {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"必填信息没有添加完整" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
                    [alert show];
                    return;
                }

                else
                {
                    
                }
                NSInteger code_status = [responseData[@"code"] integerValue];
                if (code_status == STATUS_OK) {
//                    [[NSNotificationCenter defaultCenter]postNotificationName:@"editSuccess" object:nil];
                    if ([_delegate respondsToSelector:@selector(cheliang)]) {
                        [_delegate cheliang];
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    
//                    [self.navigationController popViewControllerAnimated:YES];
                }
                //添加成功,发送通知
                
                NSLog(@"result = %@", responseData);
                
            } andFailure:^(NSString *errorDesc) {
                
                NSLog(@"response failure");
            }];
            
        }

-(void)gotoBack
{
    NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:1 inSection:0];
    AddCarTableViewCell *cell=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath1];
    
    
    NSIndexPath *indexPath2=[NSIndexPath indexPathForRow:2 inSection:0];
    AddCarTableViewCell *cell1=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath2];
    
    
    NSIndexPath *indexPath3=[NSIndexPath indexPathForRow:3 inSection:0];
    AddCarTableViewCell *cell2=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath3];
    
    NSIndexPath *indexPath7=[NSIndexPath indexPathForRow:4 inSection:0];
    AddCarTableViewCell *cell7=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath7];
    
    NSIndexPath *indexPath8=[NSIndexPath indexPathForRow:5 inSection:0];
    AddCarTableViewCell *cell6=(AddCarTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath8];
    if ([cell.rightLabel.text isEqualToString:@"请填写车"]||[cell1.rightLabel.text isEqualToString:@"请填写车牌号"]||[cell2.rightLabel.text isEqualToString:@"请填姓名"]||[cell6.rightLabel.text isEqualToString:@"请填写手机号"])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"必填信息没有添加完整" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)gotoSave
{
    [self downloadData];
    //[self.navigationController popViewControllerAnimated:YES];
}







@end
