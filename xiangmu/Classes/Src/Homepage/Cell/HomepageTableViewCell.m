//
//  HomepageTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/2.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "HomepageTableViewCell.h"

@implementation HomepageTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(130*Scale, 165*Scale);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        self.collectionView = [[HomepageCollectionView alloc] initWithFrame:CGRectMake(12*Scale, 40/**Scale*/, ViewWidth-12*Scale, 175*Scale) collectionViewLayout:flowLayout];
        self.collectionView.scrollEnabled = YES;
        self.collectionView.backgroundColor = [UIColor whiteColor];
        self.collectionView.showsHorizontalScrollIndicator = NO;
        
        [self.contentView addSubview:self.collectionView];
    }
    
    return self;
}



//- (NSMutableArray*)dataList
//{
//    if (!_dataList) {
//        _dataList = [NSMutableArray new];
//    }
//    return _dataList;
//}

- (id)initWithStyle:(UITableView*)tableView style:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    static NSString *CellID = @"HomepageTableViewCell";
    HomepageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(130*Scale, 165*Scale);
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        cell.collectionView = [[HomepageCollectionView alloc] initWithFrame:CGRectMake(12*Scale, 40/**Scale*/, ViewWidth-12*Scale, 175*Scale) collectionViewLayout:flowLayout];
        cell.collectionView.scrollEnabled = YES;
        cell.collectionView.backgroundColor = [UIColor whiteColor];
        cell.collectionView.showsHorizontalScrollIndicator = NO;
        
        [cell.contentView addSubview:cell.collectionView];
        
    }
    return cell;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    self.disclosureButton.frame = CGRectMake(ViewWidth-20*Scale, 0, <#CGFloat width#>, <#CGFloat height#>)
    
//    CGFloat buttonWidth = 50*Scale;
//    CGFloat buttonHeight = 17*Scale;
//    self.plusButton.frame = CGRectMake(ViewWidth-buttonWidth-12*Scale, self.headerView.top, buttonWidth, buttonHeight);
    
}

-(void)initDataList:(NSInteger)row dataList:(NSMutableArray *)dataList
{
    
    if (dataList.count == 0) {
        self.hidden = YES;
        return;
    }
    
    [self initTitleView:row];
    
    self.collectionView.dataList = dataList;
    [self.collectionView reloadData];
    
//    cell.dataList = self.viewModel.providerList;
}

- (void)initTitleView:(NSInteger)row
{
    self.plusButton.hidden = NO;
    self.disclosureButton.hidden = NO;
    
    NSString *titleText;
    
    switch (row) {
        case HomeTableRowRecommend:
            titleText = @"热门推荐";
            self.plusButton.hidden = YES;
            self.disclosureButton.hidden = YES;
            break;
        case HomeTableRowServicer:
            titleText = @"全国服务商";
            break;
        case HomeTableRowGpsCity:
//            titleText =
            break;
        case HomeTableRowWedding:
            titleText = @"婚庆用车";
            break;
        case HomeTableRowTravel:
            titleText = @"旅游用车";
            break;
        case HomeTableRowCommercial:
            titleText = @"商务用车";
            break;
        case HomeTableRowAirport:
            titleText = @"机场接送";
            break;
        case HomeTableRowBus:
            titleText = @"企业班车";
            break;
        case HomeTableRowCharter:
            titleText = @"包车服务";
            break;
        case HomeTableRowGoods:
            titleText = @"货运用车";
            break;
        case HomeTableRowTruck:
            titleText = @"拖车服务";
            break;
//        case HomeTableRowExperience:
//            titleText = @"用户体验";
//            break;
//        case HomeTableRowBeijing:
////            titleText = @"北京专区";
//            titleText = @"北京地区服务商";
//            break;
//        case HomeTableRowShanghai:
////            titleText = @"上海专区";
//            titleText = @"上海地区服务商";
//            break;
//        case HomeTableRowHangzhou:
////            titleText = @"杭州专区";
//            titleText = @"杭州地区服务商";
//            break;
//        case HomeTableRowShenzhen:
////            titleText = @"深圳专区";
//            titleText = @"深圳地区服务商";
//            break;
            
        default:
            break;
    }
    self.titleLabel.text = titleText;
    
}

- (void)setDataList:(NSMutableArray *)dataList
{
    if (dataList.count == 0) {
        self.hidden = YES;
    }
    
    self.collectionView.dataList = dataList;
    [self.collectionView reloadData];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
