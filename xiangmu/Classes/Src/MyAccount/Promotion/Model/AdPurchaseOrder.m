//
//  AdPurchaseOrder.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/17.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "AdPurchaseOrder.h"

@implementation AdPurchaseOrder

-(NSString*)displayDate
{
    return [self.date substringFromIndex:[self.date indexOfString:@"-"]+1];
}

@end
