//
//  HomepageViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/7.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

//typedef NS_ENUM(NSUInteger, AdvDisplayType)
//{
//    AdvDisplayTypeInit, // 初始化
//    AdvDisplayTypePause, // 广告暂停显示
//    AdvDisplayTypeInDisplay// 广告显示中
//};


@interface HomepageViewController : BaseViewController

//@property (nonatomic, assign) AdvDisplayType showAdvStatus; // 0:初始化；1:暂停显示广告，重新进入时继续显示广告；2：广告显示
@property (nonatomic, assign) BOOL showAdv; // 是否显示广告界面
@property (nonatomic, assign) BOOL isAccountInfoShow;// 是否显示帐户页面
//@property (nonatomic, assign) BOOL isFirstIn;// 是否第一次进入广告界面
//@property (weak, nonatomic) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionView *collectionView;

-(void)setNavTitleView;

@end
