//
//  DaitgCollectionViewCell.m
//  xiangmu
//
//  Created by David kim on 16/3/24.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "DaitgCollectionViewCell.h"

@implementation DaitgCollectionViewCell
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        UIImageView *imageView1=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 40, 40)];
        imageView1.image=[UIImage imageNamed:@"icon_iYong"];
        [self addSubview:imageView1];
        
        UILabel *lineLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 10, 1, 40)];
        lineLabel.backgroundColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        [self addSubview:lineLabel];
        
        self.company=[[UILabel alloc]initWithFrame:CGRectMake(65, 10, 300, 20)];
        self.company.font=[UIFont systemFontOfSize:14];
        self.company.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        [self addSubview:self.company];
        
        self.boss=[[UILabel alloc]initWithFrame:CGRectMake(65, 35, 300, 20)];
        self.boss.font=[UIFont systemFontOfSize:14];
        self.boss.textColor=[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0f/255.0f alpha:1.0f];
        [self addSubview:self.boss];
        
        
    }
    return self;
}
@end
