//
//  CommentView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/31.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommentView.h"

#define TopMargin 10
#define BottomMargin 6

#define CommentLabelHeight 21
#define CommentTopMargin 10
#define CommentLeftMargin 10



@interface CommentView ()

@property (nonatomic, strong) UIImageView *likeCmtBg;

@property (strong, nonatomic) UIImageView *likeIconView;

//@property (strong, nonatomic) MLLinkLabel *likeLabel;

//@property (strong, nonatomic) UIView *divider;


@property (strong, nonatomic) NSMutableArray *commentLabels;

@end

@implementation CommentView


-(UIImageView*)likeCmtBg;
{
    if (_likeCmtBg == nil) {
        
        _likeCmtBg = [[UIImageView alloc] initWithFrame:self.frame];
        UIImage *image = [UIImage imageNamed:@"img_comment_bg"];
        image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(20, 30, 10, 10) resizingMode:UIImageResizingModeStretch];
        _likeCmtBg.image = image;

    }
    
    return _likeCmtBg;
    
}

-(NSMutableArray*)commentLabels
{
    if (_commentLabels == nil) {
        
        _commentLabels = [NSMutableArray new];
    }
    
    return _commentLabels;
    
}

//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        
//        _commentLabels = [NSMutableArray array];
//        
//        [self addSubview:self.likeCmtBg];
//        
//    }
//    return self;
//}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        
        _commentLabels = [NSMutableArray new];
        
        [self addSubview:self.likeCmtBg];
        
    }
    
    return self;
}

- (void)layoutSubviews
{
    self.likeCmtBg.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}


- (void)updateWithItem:(BusinessCircleModel *)item;
{
    
    for (UIView *view in self.commentLabels) {
        
        [view removeFromSuperview];
    
    }
    
    if (item.comments.count > 0) {
        
        CGFloat sumHeiht = TopMargin;
        
        for (NSInteger i = 0; i < item.comments.count; i++) {
            
            Comment *comment = item.comments[i];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CommentLeftMargin, CommentTopMargin + i * CommentLabelHeight, self.frame.size.width-CommentLeftMargin*2, CommentLabelHeight)];
            
            label.text = [NSString stringWithFormat:@"%@：¥%@", comment.companyName, comment.price];
            
            label.font = FONT(12);
            
            label.textColor = [UIColor darkGrayColor];
            
            [self.commentLabels addObject:label];
            
            [self addSubview:label];
            
            sumHeiht += CommentLabelHeight;
            
        }
        
    }
    
}

+ (CGFloat)getHeight:(BusinessCircleModel *)item maxWidth:(CGFloat)maxWidth
{
    return CommentTopMargin * 2 + CommentLabelHeight * item.comments.count;
}



@end
