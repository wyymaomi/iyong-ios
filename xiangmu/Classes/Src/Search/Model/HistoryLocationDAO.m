//
//  HistoryLocationDAO.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/23.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "HistoryLocationDAO.h"

@implementation HistoryLocationDAO

-(void)getLatestTwentyKeywordsWithCompletionBlock:(void (^)(NSArray *))completionBlock;
{
    __block NSMutableArray *array = nil;
    
    [self.databaseQueue inDatabase:^(FMDatabase *db){
        NSString *sql = [NSString stringWithFormat:@"select * from search_history order by time desc"];
        
        FMResultSet *rs = [db executeQuery:sql];
        if(!rs){
            [rs close];
            return;
        }
        array = [[NSMutableArray alloc] initWithCapacity:20];
        while ([rs next]) {
            
            LocationSearchModel *data = [[LocationSearchModel alloc] init];
            data.uid = [rs stringForColumn:@"id"];
            data.name = [rs stringForColumn:@"title"];
            data.address = [rs stringForColumn:@"address"];
            data.areaCode = [rs stringForColumn:@"cityName"];
            data.location = @{@"lat": [NSNumber numberWithDouble:[rs doubleForColumn:@"latitude"]],
                              @"lng": [NSNumber numberWithDouble:[rs doubleForColumn:@"longitude"]]};
            
            [array addObject:data];
            
            if ([array count] >= 10)
            {
                break;
            }
        }
        [rs close];
        
    }];
    
    if (completionBlock) {
        completionBlock(array);
    }

}


-(BOOL)insertHistoyrLocation:(LocationSearchModel*)location time:(NSNumber*)time
{
    if (!location) {
        return NO;
    }
    
    __block BOOL isSuccess = NO;
    
//    {
//        location = {
//            lat = 31.051489,
//            lng = 121.655259
//        },
//        uid = eb4572bf10b19f243a13fd9d,
//        city = 上海市,
//        district = 浦东新区,
//        cityid = 289,
//        business = ,
//        name = 新场-地铁站
//    }
//    )
    
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        NSString *sql = @"replace into search_history(id, title, address, time, cityName, latitude, longitude) values(?,?,?,?,?,?,?)";
        
        isSuccess = [db executeUpdate:sql, location.uid, location.name, location.address, time, location.areaCode, location.location[@"lat"], location.location[@"lng"]];
//        isSuccess = [db executeUpdate:sql, location.id, location.title, location.address, time, location.city, ];
        
    }];
    
    return isSuccess;
}

@end
