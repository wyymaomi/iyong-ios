//
//  GesturePasswordController.m
//  GesturePassword
//
//  Created by hb on 14-8-23.
//  Copyright (c) 2014年 黑と白の印记. All rights reserved.
//

#import <Security/Security.h>
#import <CoreFoundation/CoreFoundation.h>
#import "UserDefaults.h"
#import "NSString+Encrypt.h"

#import "GesturePasswordController.h"
#import "NetworkManager.h"

@interface GesturePasswordController ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) GesturePasswordView * gesturePasswordView;
@property (nonatomic, strong) UIButton *okButton;

@end

@implementation GesturePasswordController {
    NSString * previousString;
    NSString * password;
}

//@synthesize gesturePasswordView;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

-(UIButton*)okButton
{
    if (IsNilOrNull(_okButton)) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(20, self.view.frame.size.height - 120, ViewWidth - 40, 34);
        button.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:150.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
        button.layer.cornerRadius = 15;
        [button setTitle:@"完成" forState:UIControlStateNormal];
        [button blueStyle];
        _okButton = button;
    }
    return _okButton;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    if (IOS7_OR_LATER) {
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = YES;
            self.navigationController.interactivePopGestureRecognizer.delegate = self;
        }
//    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(doSave)];

//     Do any additional setup after loading the view.
    previousString = [NSString string];
    password = [[UserDefaults sharedInstance] objectForKey:KEY_GESTURE_PWD];
    if ([[UserManager sharedInstance] gesturePasswordIsExit]) {
        self.title = @"重设手势密码";
        [self verify];
    }
    else {
        self.title = @"设置手势密码";
        [self reset];
    }
    
}

-(void)doSave
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(GesturePasswordView*)gesturePasswordView
{
    if (_gesturePasswordView == nil) {
        _gesturePasswordView = [[GesturePasswordView alloc]initWithFrame:[UIScreen mainScreen].bounds];
//        [_gesturePasswordView.tentacleView setRerificationDelegate:self];

    }
    return _gesturePasswordView;
}


#pragma mark - 验证手势密码
- (void)verify{
    [self.gesturePasswordView.tentacleView setRerificationDelegate:self];
    [self.gesturePasswordView.tentacleView setStyle:1];
    [self.gesturePasswordView setGesturePasswordDelegate:self];
    self.gesturePasswordView.state.text = @"请验证当前手势密码";
    [self.view addSubview:self.gesturePasswordView];
    
//    [self.view addSubview:self.okButton];
    
}

#pragma mark - 重置手势密码
- (void)reset{
    [self.gesturePasswordView.tentacleView setStyle:2];
    [self.gesturePasswordView.tentacleView setResetDelegate:self];
//    [self.gesturePasswordView.imgView setHidden:YES];
    [self.gesturePasswordView.forgetButton setHidden:YES];
    [self.gesturePasswordView.changeButton setHidden:YES];
    [self.view addSubview:self.gesturePasswordView];
    
//    [self.view addSubview:self.okButton];

}

#pragma mark - 判断是否已存在手势密码
- (BOOL)exist{
//    KeychainItemWrapper * keychin = [[KeychainItemWrapper alloc]initWithIdentifier:@"Gesture" accessGroup:nil];
//    password = [keychin objectForKey:(__bridge id)kSecValueData];
    password = [[UserDefaults sharedInstance] objectForKey:KEY_GESTURE_PWD];
    if ([password isEqualToString:@""])return NO;
    return YES;
}

#pragma mark - 清空记录
- (void)clear{
//    KeychainItemWrapper * keychin = [[KeychainItemWrapper alloc]initWithIdentifier:@"Gesture" accessGroup:nil];
//    [keychin resetKeychainItem];
    [[UserDefaults sharedInstance] removeObjectForKey:KEY_GESTURE_PWD];
    
}

#pragma mark - 改变手势密码
- (void)change{
}

#pragma mark - 忘记手势密码
- (void)forget{
}

- (BOOL)verification:(NSString *)result{
    if ([[result sha1] isEqualToString:password]) {
//        [_gesturePasswordView.state setTextColor:[UIColor colorWithRed:2/255.f green:174/255.f blue:240/255.f alpha:1]];
//        [_gesturePasswordView.state setText:@"输入正确"];
//        [self presentViewController:(UIViewController) animated:YES completion:nil];
//        [self reset];
        
        [self.gesturePasswordView.tentacleView setStyle:2];
        [self.gesturePasswordView.tentacleView setResetDelegate:self];
        [self.gesturePasswordView.forgetButton setHidden:YES];
        [self.gesturePasswordView.changeButton setHidden:YES];
        [_gesturePasswordView.tentacleView enterArgin];
        [_gesturePasswordView.state setTextColor:[UIColor colorWithRed:2/255.f green:174/255.f blue:240/255.f alpha:1]];
        [_gesturePasswordView.state setText:@"请绘制新的手势密码"];
        
        return YES;
    }
    [_gesturePasswordView.state setTextColor:[UIColor redColor]];
    [_gesturePasswordView.state setText:@"手势密码错误"];
    [_gesturePasswordView.tentacleView enterArgin];
    return NO;
}

- (BOOL)resetPassword:(NSString *)result{
    if ([previousString isEqualToString:@""]) {
        previousString=result;
        [_gesturePasswordView.tentacleView enterArgin];
        [_gesturePasswordView.state setTextColor:NAVBAR_BGCOLOR];
//        [_gesturePasswordView.state setTextColor:[UIColor colorWithRed:2/255.f green:174/255.f blue:240/255.f alpha:1]];
        [_gesturePasswordView.state setText:@"请验证输入密码"];
        return YES;
    }
    else {
        if ([result isEqualToString:previousString]) {
//            KeychainItemWrapper * keychin = [[KeychainItemWrapper alloc]initWithIdentifier:@"Gesture" accessGroup:nil];
//            [keychin setObject:@"<帐号>" forKey:(__bridge id)kSecAttrAccount];
//            [keychin setObject:result forKey:(__bridge id)kSecValueData];
            //[self presentViewController:(UIViewController) animated:YES completion:nil];
//            [[UserDefaults sharedInstance] setObject:result forKey:KEY_GESTURE_PWD];
            [self saveGesturePwd:[result sha1]];
            [_gesturePasswordView.state setTextColor:NAVBAR_BGCOLOR];
//            [_gesturePasswordView.state setTextColor:[UIColor colorWithRed:2/255.f green:174/255.f blue:240/255.f alpha:1]];
            [_gesturePasswordView.state setText:@"已保存手势密码"];
            return YES;
        }
        else{
            previousString =@"";
            [_gesturePasswordView.state setTextColor:[UIColor redColor]];
            [_gesturePasswordView.state setText:@"两次密码不一致，请重新输入"];
            [self.gesturePasswordView.tentacleView enterArgin];
            return NO;
        }
    }
}

-(void)saveGesturePwd:(NSString*)shaPwd
{
    [[UserDefaults sharedInstance] setObject:shaPwd forKey:KEY_GESTURE_PWD];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"设置手势密码成功, 请返回" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    WeakSelf
    [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
        StrongSelf
        if (buttonIndex == 0) {
            if (strongSelf.source_from == SOURCE_FROM_LOGIN) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessNotification object:nil];
                [strongSelf dismissToRootViewController:YES completion:nil];
                
                
                
            }
            else {
                [strongSelf pressedBackButton];
            }
        }
    }];

}


#pragma mark -
#pragma mark UIGestureRecognizerDelegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer;
{
    return NO;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
