//
//  AdvModel.m
//  xiangmu
//
//  Created by 湛思科技 on 16/10/13.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "AdvModel.h"

@implementation AdvModel

-(BOOL)isRedpack;
{
    BOOL isSpecialAdv = [self.special boolValue];
    
    NSInteger specialType = [self.specialType integerValue];
    
    return (isSpecialAdv && specialType == SpecialAdvTypeRedpack);
}

@end



@implementation AdvResponseData

@end
