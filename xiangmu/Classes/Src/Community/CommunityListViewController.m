//
//  CommunityListViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 16/7/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "CommunityListViewController.h"
#import "CommunityTableViewCell.h"
#import "CommunityHeaderTableViewCell.h"
#import "CommunityPublishViewController.h"
#import "CommunityPublishModel.h"

@interface CommunityListViewController ()

@end

@implementation CommunityListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"社区";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(publish)];
    [self.view addSubview:self.pullRefreshTableView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.pullRefreshTableView beginHeaderRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 发布
-(void)publish
{
    CommunityPublishViewController *viewController = [[CommunityPublishViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - PullRefresh delegate method

-(void)onHeaderRefresh
{
    self.nextAction = @selector(onHeaderRefresh);
    self.object1 = nil;
    self.object2 = nil;
    
    [self refreshData:nil isHeaderRefresh:YES];
    
}

-(void)onFooterRefresh
{
    if (self.dataList.count == 0) {
        [self.pullRefreshTableView endFooterRefresh];
        return;
    }

    self.nextAction = @selector(onFooterRefresh);
    self.object1 = nil;
    self.object2 = nil;
 
        CommunityPublishModel *model = [self.dataList lastObject];
    [self refreshData:model.createTime isHeaderRefresh:NO];
    
}

-(void)refreshData:(NSString*)createTime isHeaderRefresh:(BOOL)isHeaderRefresh
{
    NSString *params = [NSString stringWithFormat:@"time=%@", [StringUtil getSafeString:createTime]];
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, COMMUNITY_PARTNER_IDLECAR_LIST_API];
    
    if (isHeaderRefresh) {
        self.pullRefreshTableView.footerHidden = YES;
        [self.dataList removeAllObjects];
    }
    
    WeakSelf
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:params success:^(NSDictionary* responseObject) {
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        strongSelf.pullRefreshTableView.footerHidden = NO;
        
        CommunityResponseData *responseData = [[CommunityResponseData alloc] initWithDictionary:responseObject error:nil];
        if ([responseData.code integerValue] == STATUS_OK) {
            
            [strongSelf.dataList addObjectsFromArray:responseData.data];
            [strongSelf.pullRefreshTableView reloadData];
            

        }
        else {
            [strongSelf showAlertView:[strongSelf getErrorMsg:[responseData.code integerValue]]];
        }
        
    } andFailure:^(NSString *errorDesc) {
        
        StrongSelf
        [strongSelf.pullRefreshTableView endHeaderRefresh];
        [strongSelf.pullRefreshTableView endFooterRefresh];
        [strongSelf resetAction];
        [strongSelf showAlertView:errorDesc];
        
    }];
}


#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 226;
    }
    return 300;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        CommunityHeaderTableViewCell *cell = [CommunityHeaderTableViewCell cellWithTableView:tableView];
        
        return cell;
    }
    else {
        CommunityTableViewCell *cell = [CommunityTableViewCell cellWithTableView:tableView indexPath:indexPath];
        if (self.dataList.count > 0) {
            CommunityPublishModel *model = self.dataList[indexPath.row-1];
            [cell initData:model];
        }

        return cell;
    }
    
    return nil;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    //    [self gotoOrderDetailPage:indexPath.section];
//    
//}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//}


@end
