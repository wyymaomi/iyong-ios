//
//  APIManager.h
//  xiangmu
//
//  Created by 湛思科技 on 16/11/29.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Request.h"



@protocol APIManagerDelegate <NSObject>

- (void)failedWithRequest:(NSString*)apiUrl status_code:(NSInteger)status_code errorMsg:(NSString*)errorMsg;

- (void)successWithRequest:(NSString*)apiUrl responseObject:(NSDictionary*)responseObject;

@end


@interface APIManager : NSObject<Request>

@property (nonatomic, weak) NSObject<Request> *request;

@property (nonatomic, weak) id<APIManagerDelegate> delegate;

- (void)fetchDataWithRequest:(NSString*)params;

@end
