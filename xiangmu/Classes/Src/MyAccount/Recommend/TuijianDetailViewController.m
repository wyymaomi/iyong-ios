//
//  TuijianDetailViewController.m
//  xiangmu
//
//  Created by David kim on 16/5/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "TuijianDetailViewController.h"
#import "HttpConstants.h"
#import "UserManager.h"
#import "NetworkManager.h"
#import "RecommendViewController.h"
//#import "ABRootViewController.h"
//#import "CNContactRootViewController.h"

@interface TuijianDetailViewController ()/*<ABPeoplePickerNavigationControllerDelegate, ABRootViewControllerDelegate, CNContactRootViewControllerDelegate>*/
{
    UITextField *textField;
}
@end

@implementation TuijianDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"手机号（推荐人）";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(gotoAdd)];
//    self.view.backgroundColor=[UIColor colorWithRed:247.0f/255.0f green:247.0f/255.0f blue:247.0f/255.0f alpha:1.0f];
//    [self createMyNav];
    [self createInterface];
    // Do any additional setup after loading the view.
}
//-(void)createMyNav
//{
//    UILabel *titleLabel=[self addNavTitle:CGRectMake(self.view.frame.size.width/2-30, 0, 60, 49) title:@"手机号(推荐人)"];
//    titleLabel.textColor=[UIColor whiteColor];
//    UIButton *btn=[self addNavBtn:CGRectMake(0, 0, 9, 15) title:nil target:self action:@selector(gotoBack) isLeft:YES];
//    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
//    
//    UIButton *btn1=[self addNavBtn:CGRectMake(0, 0, 40, 20) title:@"保存" target:self action:@selector(gotoAdd) isLeft:NO];
//    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    
//    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:35.0f/255.0f green:70.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
//    self.navigationController.navigationBar.translucent=NO;
//}
//-(void)gotoBack
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}

-(void)gotoAdd
{
    UITextField *textField = [self.view viewWithTag:800];
    
    
    NSString *searchText = textField.text;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^1[3|4|5|7|8][0-9]\\d{8}$" options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *result = [regex firstMatchInString:searchText options:0 range:NSMakeRange(0, [searchText length])];
    if (result) {
        NSLog(@"手机号正确");
        NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, Company_Add_Inviter];
        NSString *parmas = [NSString stringWithFormat:@"mobile=%@",textField.text];
        
        [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:parmas success:^(NSDictionary* responseData) {
            NSInteger code_status = [responseData[@"code"] integerValue];
            NSLog(@"response success");
            
            NSLog(@"result = %@", responseData);
            
            NSLog(@"添加成功");
            
            if(code_status == STATUS_OK)
            {
                //添加成功,发送通知
//                [[NSNotificationCenter defaultCenter]postNotificationName:@"addChenggong" object:nil];
                if ([_delegate respondsToSelector:@selector(tuijian)]) {
                    [_delegate tuijian];
                }
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            
        } andFailure:^(NSString *errorDesc) {
            
            NSLog(@"response failure");
            //                                    ViewController *viewController=[[ViewController alloc]init];
            //                                    [self.navigationController pushViewController:viewController animated:YES];
            //                                    return;
            
            
        }];

    }
    else
    {
        NSLog(@"手机号错误");
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入11位手机号"preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertView addAction:alertAction];
        [self presentViewController:alertView animated:YES completion:nil];
    }
    
    
}

-(void)createInterface
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 10, ViewWidth, 40)];
    view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:view];
    
    textField=[[UITextField alloc]initWithFrame:CGRectMake(15, 10, ViewWidth-80, 20)];
    textField.placeholder=@"请输入推荐人手机号";
    textField.tag = 800;
    textField.keyboardType=UIKeyboardTypeNumberPad;
    textField.font=[UIFont systemFontOfSize:14];
    [view addSubview:textField];
    
    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 60, ViewWidth, 40)];
    view2.backgroundColor = [UIColor whiteColor];
    view2.userInteractionEnabled = YES;
    [view2 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onGetPhoneContact)]];
//    [view2 addGestureRecognizer:[[UIGestureRecognizer alloc] initWithTarget:self action:@selector(onGetPhoneContact:)]];
    [self.view addSubview:view2];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 21, 26)];
    iconImageView.image = [UIImage imageNamed:@"icon_contact"];
    [view2 addSubview:iconImageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, ViewWidth - 80, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"添加通讯录";
    label.font = FONT(14);
    label.textColor = [UIColor darkGrayColor];
    [view2 addSubview:label];
    
    UIImageView *leftImageView=[[UIImageView alloc]initWithFrame:CGRectMake(ViewWidth-15, 18, 23/3, 42/3)];
    leftImageView.image = [UIImage imageNamed:@"icon_disclosure"];
    [view2 addSubview:leftImageView];
    
   
}

-(void)onGetPhoneContact
{
//    if (isIOS10 || isIOS9) {
//        
//        CNContactRootViewController *contactRootViewController = [CNContactRootViewController new];
//        contactRootViewController.delegate = contactRootViewController;
//        contactRootViewController.controllerDelegate = self;
//        [self presentViewController:contactRootViewController animated:YES completion:nil];
//    }
//    else {
//        ABRootViewController *abRootViewController = [[ABRootViewController alloc] init];
//        abRootViewController.controllerDelegate = self;
//        abRootViewController.peoplePickerDelegate = abRootViewController;
//        if(IOS8_OR_LATER){
//            abRootViewController.predicateForSelectionOfPerson = [NSPredicate predicateWithValue:false];
//        }
//        [self presentViewController:abRootViewController animated:YES completion:nil];
//    }
}

-(void)onGetContactMobile:(NSString*)phone;
{
//    UITextField *textField = [self.view viewWithTag:800]
    [self onGetContactPhone:phone];
}

-(void)onGetContactPhone:(NSString*)phone;
{
    UITextField *textField = [self.view viewWithTag:800];
    textField.text = phone.trim;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
