//
//  NewOrderDTO.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/22.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseHttpMessage.h"

@interface NewOrderHttpMessage : BaseHttpMessage

//@property (nonatomic, strong) NSNumber *type;
//@property (nonatomic, strong) NSString *status;// 状态码
@property (nonatomic, strong) NSString *carTime;//用车时间
@property (nonatomic, strong) NSString *model; // 车型
@property (nonatomic, strong) NSString *location;//出发地点
@property (nonatomic, strong) NSString *itinerary;//大致行程
@property (nonatomic, strong) NSString *customerName;//用车联系人
@property (nonatomic, strong) NSString *customerMobile;//用车人电话
@property (nonatomic, strong) NSString *remark;//备注
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *areaCode;

@end
