//
//  PostExperienceViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 17/2/22.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "PostExperienceViewController.h"
#import "TZImagePickerController.h"
#import "CollectionViewCell.h"
#import "PostContentTableViewCell.h"
#import "AliyunOSSManager.h"
#import "BusinessHttpMessage.h"
#import "AppConfig.h"
#import "BusinessInfoModel.h"
//#import "BusinessTypePickerView.h"
#import "ProviderDetailModel.h"
#import "HeaderView.h"
#import "BusinessTypeTableViewCell.h"
#import "PublishSuccessViewController.h"
#import "PublishHeaderTableViewCell.h"
#import "PublishFailureViewController.h"
#import "CameraPhotoService.h"


@interface PostExperienceViewController ()<TZImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>
{
    NSInteger _itemWidth;
    CGFloat _margin;
}

@property (nonatomic, strong) BusinessTypeTableViewCell *typeTableViewCell;

@property (nonatomic, strong) BusinessHttpMessage *businessHttpMessage;
@property (nonatomic, strong) ExperienceHttpMessage *experienceHttpMessage;
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) NSMutableArray<UIImage*> *photosArray;
//@property (nonatomic ,strong) NSMutableArray *assestArray;
@property BOOL isSelectOriginalPhoto;

//@property (nonatomic, strong) BusinessTypePickerView *businessTypePickerView;// 企业服务类型
@property (nonatomic, strong) NSMutableArray *businessTypeArray; // 企业类型

@property (nonatomic, strong) ProviderDetailModel *providerData; // 在线服务商详情数据

@property (nonatomic, strong) CameraPhotoService *cameraPhotoService;
@property (weak, nonatomic) IBOutlet UIButton *postButton;

@property (nonatomic, strong) UILabel *titleLabel; // 标题

@end

@implementation PostExperienceViewController

- (UILabel*)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = NAVBAR_BGCOLOR;
        _titleLabel.font = FONT(16);
    }
    return _titleLabel;
}

- (CameraPhotoService *)cameraPhotoService
{
    if (!_cameraPhotoService) {
        WeakSelf
        _cameraPhotoService = [[CameraPhotoService alloc] initWithViewController:self type:CameraPhotoLarge completeBlock:^(NSInteger tag){
            StrongSelf
//            [strongSelf.plainTableView reloadData];
            [strongSelf.collectionView reloadData];
        } onGetImageData:^(NSData *imageData, NSInteger tag) {
            StrongSelf
            UIImage *image = [UIImage imageWithData:imageData];
            [strongSelf.photosArray insertObject:image atIndex:0];
//            [strongSelf.photosArray addObject:image];
//            [strongSelf.collectionView reloadData];
//            strongSelf.httpMessage.fileContent = [[NSString alloc] initWithData:[GTMBase64 encodeData:imageData] encoding:NSUTF8StringEncoding];
        }];
    }
    return _cameraPhotoService;
}

- (NSMutableArray*)businessTypeArray
{
    if (_businessTypeArray == nil) {
        _businessTypeArray = [NSMutableArray array];
    }
    return _businessTypeArray;
}

- (BusinessHttpMessage*)businessHttpMessage
{
    if (_businessHttpMessage == nil) {
        _businessHttpMessage = [BusinessHttpMessage new];
    }
    return _businessHttpMessage;
}

- (ExperienceHttpMessage*)experienceHttpMessage
{
    if (!_experienceHttpMessage) {
        _experienceHttpMessage = [ExperienceHttpMessage new];
        _experienceHttpMessage.areaCode = [AppConfig currentConfig].gpsCityCode;
    }
    return _experienceHttpMessage;
}

-(void)pressedBackButton
{
    [self onClickCloseButton];
}

//- (IBAction)onClickCloseButton:(id)sender{
//    [self dismissViewControllerAnimated:YES completion:nil];
-(void)onClickCloseButton {
    BBAlertView *alertView = [[BBAlertView alloc] initWithStyle:BBAlertViewStyleLogin Title:nil message:@"还未发布信息" customView:nil delegate:nil cancelButtonTitle:@"继续" otherButtonTitles:@"退出"];
//    [alertView setCancelBlock::^{
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }];
    [alertView setConfirmBlock:^{
//        [super pressedBackButton];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertView show];
}

- (NSMutableArray *)photosArray{
    if (!_photosArray) {
        self.photosArray = [NSMutableArray array];
    }
    return _photosArray;
}

//- (NSMutableArray *)assestArray{
//    if (!_assestArray) {
//        self.assestArray = [NSMutableArray array];
//    }
//    return _assestArray;
//}

-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        _margin = 4;
        _itemWidth = (ViewWidth-48-30)/3;
        UICollectionViewFlowLayout *flowLayOut = [[UICollectionViewFlowLayout alloc] init];
        flowLayOut.itemSize = CGSizeMake(_itemWidth, _itemWidth);
        flowLayOut.sectionInset = UIEdgeInsetsMake(10, 5, 5, 5);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(24, 15, ViewWidth-48, 240) collectionViewLayout:flowLayOut];
        _collectionView.backgroundColor = [UIColor whiteColor];
//        _collectionView.backgroundColor = [UIColor cyanColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _tpkeyboardAvoidingTableView.delegate = self;
    _tpkeyboardAvoidingTableView.dataSource = self;
    _tpkeyboardAvoidingTableView.tableFooterView = [UIView new];
    _tpkeyboardAvoidingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tpkeyboardAvoidingTableView.tag = _type;
    
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(onClickPublishButton:)];
    
    if (self.type == TableTypeCompanyBusiness) {
        [self getBusinessInfo];
//        [self.postButton setBackgroundImage:[UIImage imageNamed:@"btn_post_provider_normal"] forState:UIControlStateNormal];
//        [self.postButton setBackgroundImage:[UIImage imageNamed:@"btn_post_provider_highlighted"] forState:UIControlStateHighlighted];
//        self.titleLabel.text = @"商户信息";
        self.title = @"发布商户";
        
    }
    else {
//        [self.postButton setBackgroundImage:[UIImage imageNamed:@"btn_post_userexperience_normal"] forState:UIControlStateNormal];
//        [self.postButton setBackgroundImage:[UIImage imageNamed:@"btn_post_userexperience_highlighted"] forState:UIControlStateHighlighted];
//        self.titleLabel.text = @"用车体验";
        self.title = @"发布体验";
    }
    

    


}

//- (BOOL)prefersStatusBarHidden
//{
//    return YES;//隐藏为YES，显示为NO
//}

- (IBAction)closePage:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - 点击箭头按钮弹出业务选择框

- (void)onClickUpDownButton:(id)sender
{
//    if (self.businessTypePickerView.isHidden) {
//        self.businessTypePickerView.hidden = NO;
//    }
}

#pragma mark - 点击发布按钮

- (IBAction)onClickPublishButton:(id)sender {
    if (_type == TableTypeUserExperience) {
        [self publishUserExperience];
    }
    else if (_type == TableTypeCompanyBusiness) {
        [self publishCompanyBusiness];
    }
}

#pragma mark - 查询公司信息
- (void)getBusinessInfo
{
    if (![[UserManager sharedInstance] isLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, GET_PROVIDER_DETAIL_API];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:nil success:^(NSDictionary * responseData) {
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        NSDictionary *data = responseData[@"data"];
        
        if (code_status == STATUS_OK) {
            
//            BusinessInfoModel *businessInfoModel = [[BusinessInfoModel alloc] initWithDictionary:data error:nil];
            
            _providerData = [[ProviderDetailModel alloc] initWithDictionary:data error:nil];
            
//            dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
                StrongSelf
                
                [[AliyunOSSManager sharedInstance] downloadImageArray:_providerData.imgs complete:^(NSArray<UIImage *> *images, DownloadImageState state) {
                    
                    if (state == DownloadImageSuccess) {
                        strongSelf.photosArray = [images mutableCopy];
//                        strongSelf.assestArray = [images mutableCopy];
                        [strongSelf.collectionView reloadData];
                    }
                }];
                
                strongSelf.businessHttpMessage.blurb = _providerData.blurb;
                strongSelf.businessTypeArray = [_providerData.business mutableCopy];
            
            
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    //回调或者说是通知主线程刷新
//                     [weakSelf.collectionView reloadData];
//                });
            
//            });
            

//            weakSelf.contentTextView.text = _providerData.blurb;
            
            [strongSelf.tpkeyboardAvoidingTableView reloadData];
            
            
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        
    }];
    
    
}


#pragma mark - 发布公司信息

- (void)publishCompanyBusiness
{
    NSString *content = self.businessHttpMessage.blurb;//self.contentTextView.text;
    
    if (self.photosArray.count == 0) {
        [MsgToolBox showToast:@"至少上传一张图片"];
        return;
    }
    
    if (content.length == 0) {
        [MsgToolBox showToast:@"请输入公司简介"];
        return;
    }
    
    self.nextAction = @selector(publishCompanyBusiness);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, UPLOAD_PROVIDER_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在上传中"];
    
    WeakSelf
    
    [[AliyunOSSManager sharedInstance] uploadImageArray:self.photosArray isAsync:YES folderName:kAliyunOSSPublichImagePath([UserManager sharedInstance].userModel.companyId) complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        if (state == UploadImageSuccess) {
            
            StrongSelf

            strongSelf.businessHttpMessage.imgs = [names componentsJoinedByString:@","];
            strongSelf.businessHttpMessage.business = [strongSelf.businessTypeArray componentsJoinedByString:@","];
            
            [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:strongSelf.businessHttpMessage.description success:^(id responseData) {
                
                [strongSelf hideHUDIndicatorViewAtCenter];
                [strongSelf resetAction];
                
                NSInteger code_status = [responseData[@"code"] integerValue];
                
                if (code_status == STATUS_OK) {
                    
                    PublishSuccessViewController *viewController = [[PublishSuccessViewController alloc] init];
                    [strongSelf presentViewController:viewController animated:YES completion:nil];
                    
                }
                else {
                    PublishFailureViewController *viewController = [[PublishFailureViewController alloc] init];
                    [strongSelf presentViewController:viewController animated:YES completion:nil];
                    
                }
                
            } andFailure:^(NSString *errorDesc) {
                
                [weakSelf resetAction];
                [weakSelf hideHUDIndicatorViewAtCenter];
                PublishFailureViewController *viewController = [[PublishFailureViewController alloc] init];
                [strongSelf presentViewController:viewController animated:YES completion:nil];
                
                
            }];
        }
        
    }];
    
}

#pragma mark - 发布用车体验

- (void)publishUserExperience
{
    
    if (self.photosArray.count == 0) {
        [MsgToolBox showToast:@"至少上传一张图片"];
        return;
    }
    
    if (self.experienceHttpMessage.content.length == 0) {
        [MsgToolBox showToast:@"请输入用车体验"];
        return;
    }
    
    self.nextAction = @selector(publishUserExperience);
    self.object1 = nil;
    self.object2 = nil;
    
    if ([self needLogin]) {
        return;
    }
    
    
    NSString *url = [NSString stringWithFormat:@"%@/%@", HTTP_BASE_URL, EXPERIENCE_SAVE_API];
    
    [self showHUDIndicatorViewAtCenter:@"正在上传中"];
    
    WeakSelf
    
    //    NSMutableArray *imageDataArray = [self imageDataArray];
    [[AliyunOSSManager sharedInstance] uploadImageArray:self.photosArray isAsync:YES folderName:kAliyunOSSPublichImagePath([UserManager sharedInstance].userModel.companyId) complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        if (state == UploadImageSuccess) {
            
            StrongSelf
            
            strongSelf.experienceHttpMessage.imgs = [names componentsJoinedByString:@","];
            strongSelf.experienceHttpMessage.areaCode = [AppConfig currentConfig].gpsCityCode;
            
            
            [[NetworkManager sharedInstance] startEncryptRequest:url requestMethod:POST params:strongSelf.experienceHttpMessage.description success:^(id responseData) {
                
                [strongSelf hideHUDIndicatorViewAtCenter];
                [strongSelf resetAction];
                
                NSInteger code_status = [responseData[@"code"] integerValue];
                
                if (code_status == STATUS_OK) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kOnRefreshHomeExperienceListNotification object:nil];
                    
                    PublishSuccessViewController *viewController = [[PublishSuccessViewController alloc] init];
                    [strongSelf presentViewController:viewController animated:YES completion:nil];
                    
                }
                else {
                    
                    PublishFailureViewController *viewController = [[PublishFailureViewController alloc] init];
                    [strongSelf presentViewController:viewController animated:YES completion:nil];
                }
                
            } andFailure:^(NSString *errorDesc) {
                
                [weakSelf resetAction];
                [weakSelf hideHUDIndicatorViewAtCenter];
                
                PublishFailureViewController *viewController = [[PublishFailureViewController alloc] init];
                [strongSelf presentViewController:viewController animated:YES completion:nil];
            }];
        }
        
    }];
    
    
}



- (void)checkLocalPhoto{
    
    TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
    [imagePicker setSortAscendingByModificationDate:NO];
    imagePicker.isSelectOriginalPhoto = _isSelectOriginalPhoto;
//    imagePicker.selectedAssets = _assestArray;
    imagePicker.allowPickingVideo = NO;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    self.photosArray = [NSMutableArray arrayWithArray:photos];
//    [self.photosArray addObjectsFromArray:photos];
//    self.assestArray = [NSMutableArray arrayWithArray:assets];
    _isSelectOriginalPhoto = isSelectOriginalPhoto;
    [_collectionView reloadData];
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _photosArray.count) {
//        [self checkLocalPhoto];
        if (self.photosArray.count == 9) {
            [MsgToolBox showToast:@"最多只能选9张"];
            return;
        }
        [self.cameraPhotoService show];
    }
//    else{
//        if (_type == TableTypeUserExperience) {
//            TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:_assestArray selectedPhotos:_photosArray index:indexPath.row];
//            imagePickerVc.isSelectOriginalPhoto = _isSelectOriginalPhoto;
//            [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
//                _photosArray = [NSMutableArray arrayWithArray:photos];
//                _assestArray = [NSMutableArray arrayWithArray:assets];
//                _isSelectOriginalPhoto = isSelectOriginalPhoto;
//                [_collectionView reloadData];
//                _collectionView.contentSize = CGSizeMake(0, ((_photosArray.count + 2) / 3 ) * (_margin + _itemWidth));
//            }];
//            [self presentViewController:imagePickerVc animated:YES completion:nil];
//        }
//    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _photosArray.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (indexPath.row == _photosArray.count) {
        cell.imagev.image = [UIImage imageNamed:@"icon_btn_AlbumAdd"];
        cell.deleteButton.hidden = YES;
        
    }else{
        cell.imagev.image = _photosArray[indexPath.row];
        cell.deleteButton.hidden = NO;
    }
    cell.deleteButton.tag = 100 + indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(deletePhotos:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}

- (void)deletePhotos:(UIButton *)sender{
    [_photosArray removeObjectAtIndex:sender.tag - 100];
//    [_assestArray removeObjectAtIndex:sender.tag - 100];
    [_collectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag-100 inSection:0];
        [_collectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [_collectionView reloadData];
    }];
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (_type == TableTypeCompanyBusiness) {
        return 6;
    }
    else if (_type == TableTypeUserExperience) {
        return 3;
    }
    
    return 0;
    

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_type == TableTypeCompanyBusiness) {
        if (indexPath.row == CompanyTableRowPhotoExampleHeader) {
            return 48;
        }
        if (indexPath.row == CompanyTableRowPhotoExample) {
            return 310*Scale;
        }
        if (indexPath.row == CompanyTableRowPhotoHeader) {
            return 48;
        }
        if (indexPath.row == CompanyTableRowPickerPhoto) {
            return 280;
        }
        if (indexPath.row == CompanyTableRowServiceType) {
            return 210;
        }
        if (indexPath.row == CompanyTableRowCommentContent) {
            return 200;
        }
    }
    else if (_type == TableTypeUserExperience) {
        if (indexPath.row == TableViewRowPhotoHeader) {
            return 48;
        }
        if (indexPath.row == TableViewRowPickerPhoto) {
            return 280;
        }
        if (indexPath.row == TableViewRowCommentContent) {
            return 216;
        }
    }
    
    return 0;
}

- (UITableViewCell*)tableviewWithCompany:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
//    static NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%ld%ld", indexPath.section, indexPath.row];
    // 图片上传示例
    if (indexPath.row == CompanyTableRowPhotoExampleHeader) {
        PublishHeaderTableViewCell *cell = [PublishHeaderTableViewCell cellWithTableView:tableView indexPath:indexPath];
        cell.headerView.titleLabel.text = @"图片示例";
        return cell;
    }
    if (indexPath.row == CompanyTableRowPhotoExample) {
        NSString *CellIdentifier = @"CompanyTableRowPhotoExampleCellId";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(22*Scale, 5, ViewWidth-44*Scale, 253*Scale)];
            imageView.image = [UIImage imageNamed:@"img_example"];
            
            [cell.contentView addSubview:imageView];
            
            UILabel *msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(22*Scale, imageView.bottom+20, ViewWidth-44*Scale, 20*Scale)];
            msgLabel.font = FONT(15);
            msgLabel.textAlignment = UITextAlignmentCenter;
            msgLabel.textColor = NAVBAR_BGCOLOR;
            msgLabel.text = @"①为首页展示图，②③④为您详情页图片展示";
            [cell.contentView addSubview:msgLabel];
            
        }
        return cell;
    }
    
    //图片上传头部
    if (indexPath.row == CompanyTableRowPhotoHeader) {
        
        PublishHeaderTableViewCell *cell = [PublishHeaderTableViewCell cellWithTableView:tableView indexPath:indexPath];
        
        return cell;
    }
    // 选择图片
    if (indexPath.row == CompanyTableRowPickerPhoto) {
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%lu%lu", (long)indexPath.section, (long)indexPath.row];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 260, ViewWidth-20, 15)];
            label.text = @"(限9张图片)";
            label.textColor = UIColorFromRGB(0x888888);
            label.font = FONT(12);
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = UITextAlignmentRight;
            
            [cell.contentView addSubview:self.collectionView];
            [cell.contentView addSubview:label];
            
            UIView *lineView = [UIView new];
            lineView.backgroundColor = UIColorFromRGB(0xebebeb);
            [cell.contentView addSubview:lineView];
            lineView.frame = CGRectMake(24, 280-0.5, ViewWidth-48, 0.5);
            [cell.contentView addSubview:lineView];
        }
        
        return cell;
    }
    if (indexPath.row == CompanyTableRowServiceType) {
        
        BusinessTypeTableViewCell *cell = [[BusinessTypeTableViewCell alloc] initWithStyle:tableView style:UITableViewCellStyleDefault reuseIdentifier:@""];
        _typeTableViewCell = cell;
        
        // 商户服务类型
        if (_providerData) {
            
            for (NSInteger i = 0; i < cell.imageArray.count; i++) {
                UIButton *button = cell.imageArray[i];
                UILabel *label = cell.labelArray[i];
                NSNumber *number = [NSNumber numberWithInteger:button.tag];
                if ([self.businessTypeArray containsObject:number]) {
                    [button setSelected:YES];
                    [label setHighlighted:YES];
                }
                else {
                    [button setSelected:NO];
                    [label setHighlighted:NO];
                }
            }
        }
        
        for (UIButton *button in cell.imageArray) {
            [button addTarget:self action:@selector(onClickButton:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        
        return cell;
        
    }
    if (indexPath.row == CompanyTableRowCommentContent) {
        
        PostContentTableViewCell *cell = [PostContentTableViewCell cellWithTableView:tableView indexPath:indexPath];
        cell.headerView.titleLabel.text = @"公司简介";
        cell.textView.text = self.businessHttpMessage.blurb;
        cell.textView.delegate = self;
        _contentTextView = cell.textView;
        return cell;
        
    }
    return nil;
}

- (void)textViewDidChange:(UITextView *)textView;
{
    if (_type == TableTypeUserExperience) {
        self.experienceHttpMessage.content = textView.text;
//        _contentTextView.text = 
//        _contentTextView = cell.textView;
//        [self.tpkeyboardAvoidingTableView reloadData];
    }
    else if (_type == TableTypeCompanyBusiness) {
        self.businessHttpMessage.blurb = textView.text;
//        _contentTextView = cell.textView;
//        [self.tpkeyboardAvoidingTableView reloadData];
    }

}

- (UITableViewCell*)tableViewWithExperience:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
        //
          if (indexPath.row == TableViewRowPhotoHeader) {
            
            PublishHeaderTableViewCell *cell = [PublishHeaderTableViewCell cellWithTableView:tableView indexPath:indexPath];
            
            return cell;
        }
        // 选择图片
        if (indexPath.row == TableViewRowPickerPhoto) {
            
            NSString *CellIdentifier = [NSString stringWithFormat:@"CellIdentifier%lu%lu", (long)indexPath.section, (long)indexPath.row];
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 260, ViewWidth-20, 15)];
                label.text = @"(限9张图片)";
                label.textColor = UIColorFromRGB(0x888888);
                label.font = FONT(12);
                label.backgroundColor = [UIColor clearColor];
                label.textAlignment = UITextAlignmentRight;
                
                [cell.contentView addSubview:self.collectionView];
                [cell.contentView addSubview:label];
                
                UIView *lineView = [UIView new];
                lineView.frame = CGRectMake(12, 280-0.5, ViewWidth-24, 0.5);
                lineView.backgroundColor = UIColorFromRGB(0xEBEBEB);
                [cell.contentView addSubview:lineView];
            }
            
            return cell;
        }
        if (indexPath.row == TableViewRowCommentContent) {
            
            PostContentTableViewCell *cell = [PostContentTableViewCell cellWithTableView:tableView indexPath:indexPath];
            cell.textView.text = self.experienceHttpMessage.content;
            cell.textView.delegate = self;
            _contentTextView = cell.textView;
            return cell;
            
        }
    return nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_type == TableTypeUserExperience) {
        return [self tableViewWithExperience:indexPath tableView:tableView];
    }
    
    else if (_type == TableTypeCompanyBusiness) {
        return [self tableviewWithCompany:indexPath tableView:tableView];
    }

    
    return nil;
}

- (void)setButtonStatus:(id)sender
{
    //    DLog(@"onClickButton:sender");
    UIButton *button = (UIButton*)sender;
    NSInteger index = button.tag-1;
    UILabel *label = self.typeTableViewCell.labelArray[index];

    [button setSelected:YES];
    label.highlighted = YES;
    
//    if (!button.isSelected) {
//        [button setSelected:YES];
//        label.highlighted = YES;
//        //        label.textColor = UIColorFromRGB(0xBEBEBE);
//    }
//    else {
//        [button setSelected:NO];
//        label.highlighted = NO;
//        //        label.textColor = NAVBAR_BGCOLOR;
//    }
}

- (void)onClickButton:(id)sender
{
//    DLog(@"onClickButton:sender");
    UIButton *button = (UIButton*)sender;
    NSInteger index = button.tag-1;
    UILabel *label = self.typeTableViewCell.labelArray[index];
    
    if (!button.isSelected) {
        [button setSelected:YES];
        label.highlighted = YES;
//        label.textColor = UIColorFromRGB(0xBEBEBE);
    }
    else {
        [button setSelected:NO];
        label.highlighted = NO;
//        label.textColor = NAVBAR_BGCOLOR;
    }
    
    NSNumber *tagNumber = [NSNumber numberWithLong:(long)button.tag];
//    NSString *tagStr = [NSString stringWithFormat:@"%ld", (long)button.tag];
    if (button.isSelected) {
        if (![self.businessTypeArray containsObject:tagNumber]) {
            [self.businessTypeArray addObject:tagNumber];
        }
    }
    else {
        if ([self.businessTypeArray containsObject:tagNumber]) {
            [self.businessTypeArray removeObject:tagNumber];
        }
    }
    
    DLog(@"self.busienssTypeArray = %@", self.businessTypeArray);
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
