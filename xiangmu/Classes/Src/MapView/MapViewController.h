//
//  MapViewViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/7/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
//#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import "CarTypeModel.h"
#import "TravelRequestHttpMessage.h"
#import "StartOrderViewController.h"
#import "VechileOrderManager.h"
#import "RouteAnnotation.h"
#import "MapbusAnnotation.h"

#define TIMER_INTERVAL 3

@interface MapViewController : BaseViewController

@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) NSMutableArray<CarTypeModel*> *carTypeList;
@property (nonatomic, strong) TravelRequestHttpMessage *travelOrderHttpMessage;

@property (nonatomic, assign) NSUInteger totalPages;
@property (nonatomic, assign) NSUInteger pageIndex;
@property (nonatomic, strong) NSMutableArray<MapbusAnnotation*> *carList;
@property (nonatomic, strong) NSMutableArray<NSDictionary*> *responseEntityList; // 返回的车辆数据
@property (nonatomic, strong) NSMutableDictionary *carEntities;
//@property (nonatomic, )

@end
