//
//  BaiduMapTraceViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/19.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaiduMapTraceViewController.h"
#import "DrvierPathViewController.h"
#import "StringUtil.h"

//static NSUInteger serviceID = 143874;
//static NSString *AK = @"N0mHEkunzGiWn0TfbQHfGhjcd5zVWT6U";
//static NSString *mcode = @"com.chinaiyong.iyong";
////static NSString *entityName = @"entityB";
//static NSUInteger serverFenceID = 0;
//static NSUInteger localFenceID = 0;

@interface BaiduMapTraceViewController ()

@property (nonatomic, strong) NSString *entityName;
@property (nonatomic, assign) NSUInteger startTime;

@end

@implementation BaiduMapTraceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"开始" forState:UIControlStateNormal];
    button.frame = CGRectMake((ViewWidth-80)/2, 10, 80, 80);
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:button];
    [button addTarget:self action:@selector(startGather) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *stopButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [stopButton setTitle:@"暂停" forState:UIControlStateNormal];
    [stopButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    stopButton.frame = CGRectMake(((ViewWidth-80)/2), 100, 80, 80);
    [stopButton addTarget:self action:@selector(stopGather) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:stopButton];
    
    UIButton *queryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [queryButton setTitle:@"查询" forState:UIControlStateNormal];
    queryButton.frame = CGRectMake((ViewWidth-80)/2, 200, 80, 80);
    [queryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:queryButton];
    [queryButton addTarget:self action:@selector(queryTrackHistory) forControlEvents:UIControlEventTouchUpInside];
    
    
    // 使用SDK的任何功能前，都需要先调用initInfo:方法设置基础信息。
//    BTKServiceOption *sop = [[BTKServiceOption alloc] initWithAK:AK mcode:mcode serviceID:serviceID keepAlive:1];
//    [[BTKAction sharedInstance] initInfo:sop];
    
    [self startService];
    
    
}

-(void)pressedBackButton
{
    [self stopGather];
    [self stopService];
    
    [super pressedBackButton];
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [self stopGather];
//    [self stopService];
}

-(void)dealloc
{
//    [self stopGather];
//    [self stopService];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - service轨迹服务 请求

-(void)startService
{
    self.entityName = [StringUtil createUDID];
    
    BTKStartServiceOption *op = [[BTKStartServiceOption alloc] initWithEntityName:self.entityName];
    [[BTKAction sharedInstance] startService:op delegate:self];
}

-(void)stopService
{
    [[BTKAction sharedInstance] stopService:self];
}

-(void)startGather
{
    [[BTKAction sharedInstance] startGather:self];
}

-(void)stopGather
{
    [[BTKAction sharedInstance] stopGather:self];
}

-(void)changeInterval
{
    [[BTKAction sharedInstance] setLocationAttributeWithActivityType:CLActivityTypeAutomotiveNavigation desiredAccuracy:kCLLocationAccuracyBestForNavigation distanceFilter:3];
    
    NSInteger gatherInterval = 3;
    NSInteger packInterval = 30;
//    int gatherInterval = self.gatherIntervalTextField.text ? [self.gatherIntervalTextField.text intValue] : arc4random() % 3 + 2; // 2,3,4
//    int packInterval = self.packIntervalTextField.text ? [self.packIntervalTextField.text intValue] : (arc4random() % 2 + 2) * gatherInterval; // 2倍或3倍的采集周期
    [[BTKAction sharedInstance] changeGatherAndPackIntervals:gatherInterval packInterval:packInterval delegate:self];
}

#pragma mark - service轨迹服务 回调
-(void)onStartService:(BTKServiceErrorCode)error {
    NSLog(@"start service response: %lu", (unsigned long)error);
    if (error == 0) {
        
    }
    
    
}

-(void)onStopService:(BTKServiceErrorCode)error {
    NSLog(@"stop service response: %lu", (unsigned long)error);
    
}

-(void)onStartGather:(BTKGatherErrorCode)error {
    NSLog(@"start gather response: %lu", (unsigned long)error);
    if (self.startTime == 0) {
        self.startTime = [[NSDate date] timeIntervalSince1970];
    }
    
}

-(void)onStopGather:(BTKGatherErrorCode)error {
    NSLog(@"stop gather response: %lu", (unsigned long)error);
}

-(void)onChangeGatherAndPackIntervals:(BTKChangeIntervalErrorCode)error {
    NSLog(@"change gather and pack intervals response: %lu", (unsigned long)error);
}

#pragma mark - API track - 请求

-(void)queryTrackHistory
{
    
//    DrvierPathViewController *viewController = [[DrvierPathViewController alloc] init];
////    viewController.trackHistoryDict = dict;
//    //    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
//    //    [self presentViewController:viewController animated:YES completion:nil];
//    [self.navigationController pushViewController:viewController animated:YES];
//    
//    return;
    
    NSUInteger endTime = [[NSDate date] timeIntervalSince1970];
    BTKQueryTrackProcessOption *option = [[BTKQueryTrackProcessOption alloc] init];
    option.denoise = 1;
    option.vacuate = 0;//抽稀属性只有查询历史轨迹时才有作用
    option.mapMatch = 0;
    option.radiusThreshold = 55;
    
    BTKQueryHistoryTrackRequest *request = [[BTKQueryHistoryTrackRequest alloc] initWithEntityName:self.entityName startTime:self.startTime endTime:endTime isProcessed:TRUE processOption:option supplementMode:BTK_TRACK_PROCESS_OPTION_SUPPLEMENT_MODE_DRIVING outputCoordType:BTK_COORDTYPE_BD09LL sortType:BTK_TRACK_SORT_TYPE_ASC pageIndex:1 pageSize:100 serviceID:serviceID tag:13];
    [[BTKTrackAction sharedInstance] queryHistoryTrackWith:request delegate:self];

}

#pragma mark - API track - 回调

-(void)onQueryHistoryTrack:(NSData *)response {
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"track history response: %@", dict);
    
    if ([dict[@"status"] integerValue] == 0) {
        
        dispatch_main_sync_safe(^{
            
            DrvierPathViewController *viewController = [[DrvierPathViewController alloc] init];
            viewController.trackHistoryDict = dict;
            //    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
            //    [self presentViewController:viewController animated:YES completion:nil];
            [self.navigationController pushViewController:viewController animated:YES];
            
        });
        
    }
    

    

    
    
}


@end
