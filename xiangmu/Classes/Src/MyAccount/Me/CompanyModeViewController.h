//
//  CompanyModeViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/10.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger, CompanyRow)
{
    CompanyRowPublish,
    CompanyRowCompanyInfo,
    CompanyRowPromotion,
    CompanyRowVechileManage,
//    CompanyRowCarRent,
    CompanyRowIncome // 商户收入
//    CompanyRowPath
};

@interface CompanyModeViewController : BaseViewController

@end
