//
//  AdPayViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/4/14.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"
#import "AdPurchaseHttpMessage.h"

typedef NS_ENUM(NSUInteger , AdPayAccountRow)
{
    AdPayRowPromotionAccount,
    AdPayRowMyAccount
};

typedef NS_ENUM(NSUInteger, AdThirdpayRow)
{
    AdThirdPayRowAlipay,
    AdThirdPayRowWechat
};

typedef NS_ENUM(NSUInteger, PayMode)
{
    PayModeAdv=0,
    PayModeTravel
};

@interface AdPayViewController : BaseViewController<PayDelegate>

@property (nonatomic, strong) AdPurchaseHttpMessage *httpMessage;
@property (nonatomic, strong) NSString *adOrderId;
@property (nonatomic, strong) NSNumber *promotionAccountMoney;
@property (nonatomic, strong) NSNumber *myAccountMoney;

@property (nonatomic, assign) PayMode mode; // 0:广告支付模式；1:支付车费模式

@end
