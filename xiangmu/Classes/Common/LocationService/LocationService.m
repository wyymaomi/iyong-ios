//
//  LocationService.m
//  xiangmu
//
//  Created by 湛思科技 on 16/8/24.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "LocationService.h"
#import "AppConfig.h"


@implementation LocationService

DEF_SINGLETON(LocationService)

-(void)startLocation{
    
    if (![CLLocationManager locationServicesEnabled]) {
        
        self.responseStatus = LocateCityServiceUnable;
        
        DLog(@"位置服务不可用");
        
        if ([AppConfig currentConfig].isFirstLocateCity.boolValue) {
            
            [AppConfig currentConfig].isFirstLocateCity = @NO;
   
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(onLocateCityServiceUnable)]) {
            
            [self.delegate onLocateCityServiceUnable];
            
        }
        
    }
    
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        
        self.responseStatus = LocateCityUserDenied;
        
        DLog(@"用户不允许使用位置");
        
        if ([AppConfig currentConfig].isFirstLocateCity.boolValue) {
            
            [AppConfig currentConfig].isFirstLocateCity = @NO;
            
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(onLocateCityUserDenied)]) {
            
            [self.delegate onLocateCityServiceUnable];
            
        }
        
        
    }
    
    else {
        
//        [dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            
//            [self startUpdatingLocation];
//            
//        }];
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
            [self startUpdatingLocation];
            
//        });
        
        
        
    }

    
}

- (void)startUpdatingLocation
{
    
    if (!manager) {
        
        manager = [[CLLocationManager alloc] init];
        
        manager.delegate = self;
    }
    
    if ( [manager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [manager requestWhenInUseAuthorization];
    }
    
//    if (IOS8_OR_LATER) {
//        
//#if __IPHONE_8_0
//        [manager requestWhenInUseAuthorization];
//#endif
//        
//    }
    
    [manager startUpdatingLocation];
    DLog(@"startUpdatingLocation");
    
    //time out
//    [self performSelector:@selector(stopUpdatingLocation)
//               withObject:nil
//               afterDelay:self.timeOutDefault > 0 ? self.timeOutDefault : 60.0f];
    
}

- (void)stopUpdatingLocation
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(stopUpdatingLocation)
                                               object:nil];
    [manager stopUpdatingLocation];
}

#pragma mark -
#pragma mark delegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    self.coordinate = newLocation.coordinate;
    
//    if (IOS5_OR_LATER)
    {
        if (!geocoder)
        {
            geocoder = [[CLGeocoder alloc] init];
        }
        
        WeakSelf
        [geocoder reverseGeocodeLocation:newLocation
                       completionHandler:^(NSArray *placemarks,NSError *error)
         {
             if (error && [placemarks count])
             {
                 weakSelf.responseStatus = LocateCityAddressFail;
                 if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(onLocateCityAddressFail)]) {
                     [weakSelf.delegate onLocateCityAddressFail];
                 }
             }
             else
             {
                 for(CLPlacemark *placemark in placemarks)
                 {
                     weakSelf.addressInfoDic = placemark.addressDictionary;
                     NSString *currentCity = [placemark.addressDictionary objectForKey:@"City"];
                     if (!currentCity.length) {
                         currentCity = [placemark.addressDictionary objectForKey:@"State"];
                     }
                     weakSelf.cityName = currentCity;
                     DLog(@"locate city is : %@", currentCity);
                     
                     if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(onLocateCitySuccess:)]) {
                         [weakSelf.delegate onLocateCitySuccess:weakSelf.cityName];
                     }
                     
                     break;
                 }
             }
             
//             [self done];
         }];
    }
    
    [self stopUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DLog(@"location error : %@", [error localizedDescription]);
    
    [self stopUpdatingLocation];
    
    self.responseStatus = LocateCityFail;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(onLocateCityFail)]) {
        
        [self.delegate onLocateCityFail];
        
    }
    
}

+ (void)saveCityToConfig:(NSString *)cityName
{
    [AppConfig currentConfig].locationCity = cityName;
}


@end
