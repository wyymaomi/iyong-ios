//
//  IdleShareTableViewCell.h
//  xiangmu
//
//  Created by 湛思科技 on 16/9/2.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IdleShareTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *vechileModelLabel;
@property (weak, nonatomic) IBOutlet UIButton *addPartnerButton;
@property (weak, nonatomic) IBOutlet UILabel *endDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *idleLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeEclapseLabel;
@property (weak, nonatomic) IBOutlet UIImageView *vechileImageView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath*)indexPath;

+ (CGFloat)getCellHeight;

@end
