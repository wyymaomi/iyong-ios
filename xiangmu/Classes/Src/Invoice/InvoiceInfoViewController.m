//
//  InvoiceInfoViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/8/31.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "InvoiceInfoViewController.h"
#import "InvoiceinfoTableView.h"
#import "MyInvoiceViewController.h"

@interface InvoiceInfoViewController ()<InvoiceinfoTableViewDelegate>

@property (nonatomic, strong) InvoiceinfoTableView *tableView;

@end

@implementation InvoiceInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.mode == InvoiceInfoModeSubmit  ? @"确认开票" : @"发票详情";
    
    _tableView = [[InvoiceinfoTableView alloc] initWithFrame:CGRectMake(0, 0, ViewWidth, self.view.frame.size.height) style:UITableViewStyleGrouped];
    _tableView.invoiceInfoHttpMessage = self.invoiceInfoHttpMessage;
    _tableView.mode = self.mode;
    _tableView.customDelegate = self;
    [self.view addSubview:_tableView];
    
    [self.tableView reloadData];
}

-(void)onClickSubmitButton;
{
//    NSString *parmas = self.requestParams;
    
    NSString *errorMsg;
    BOOL isVerifyed = [InvoiceManager validateInvoiceInfo:self.invoiceInfoHttpMessage error:&errorMsg];
    if (!isVerifyed) {
        [MsgToolBox showToast:errorMsg];
        return;
    }
    
    [self showHUDIndicatorViewAtCenter:@"正在提交，请稍候..."];
    
    WeakSelf
    
    [[NetworkManager sharedInstance] startEncryptRequest:APIWithBaseUrl(ADD_INVOICE_API) requestMethod:POST params:self.requestParams success:^(id responseData) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        NSInteger code_status = [responseData[@"code"] integerValue];
        
        if (code_status == STATUS_OK) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"发票已提交" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                [weakSelf.navigationController popToViewController:[InvoiceManager sharedInstance].myInvoiceController animated:YES];
                
//                [weakSelf.navigationController popToViewController:[MyInvoiceViewController class] animated:YES];
                
            }];
            
        }
        else {
            
            [MsgToolBox showToast:getErrorMsg(code_status)];
        
        }
        
        
    } andFailure:^(NSString *errorDesc) {
        
        [weakSelf hideHUDIndicatorViewAtCenter];
        
        [MsgToolBox showToast:errorDesc];
        
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
