//
//  EditDiaoduViewController.h
//  xiangmu
//
//  Created by David kim on 16/5/11.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFNavController.h"
#import "AddDiaoDetailViewController.h"

@protocol EditDiaodu <NSObject>

-(void)editdiaodu;

@end

@interface EditDiaoduViewController : LFNavController<DiaoDu>
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,weak) id <EditDiaodu> delegate;
@end
