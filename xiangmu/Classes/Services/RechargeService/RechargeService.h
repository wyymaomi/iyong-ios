//
//  RechargeService.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/9/25.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RechargeService : NSObject

+ (instancetype)sharedInstance;

- (void)doAlipayPay:(NSString*)encryptOrderString;

- (void)bizPay:(NSDictionary*)dict;

@end
