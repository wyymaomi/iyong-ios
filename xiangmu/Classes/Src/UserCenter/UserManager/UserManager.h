//
//  UserManager.h
//  xiangmu
//
//  Created by 湛思科技 on 16/4/14.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"
#import "StringUtil.h"

@interface UserManager : NSObject

//@property (nonatomic, strong) NSString *session;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *desKey;

@property (nonatomic, strong) NSString *thirdPartyLoginParams; // 第三方登录信息
@property (nonatomic, strong) NSString *thirdPartyId; // 第三方登录平台ID

@property (nonatomic, strong) NSString *storeUserName;
@property (nonatomic, strong) NSString *storePassword;

@property (nonatomic, strong) UserModel *userModel;
@property (nonatomic, assign) BOOL isBindMailbox;

+ (instancetype)sharedInstance;

-(void)doLogin:(NSString*)userName
      password:(NSString*)passwordPlain
       success:(void(^)(NSInteger code_status))withSuccess
       failure:(void(^)(NSInteger code_status))withFailure;

-(void)doAutoLogin:(void(^)(NSInteger code_status))withSuccess
           failure:(void(^)(NSInteger code_status))withFailure;

-(void)doRegister:(NSString*)params
         userName:(NSString*)userName
         password:(NSString*)password
          success:(void(^)(NSInteger code_status))withSuccess
          failure:(void(^)(NSInteger code_status))withFailure;

- (void)doGetRegisterVerifyCode:(NSString*)mobile
                        success:(void(^)(NSInteger code_status))withSuccess
                        failure:(void(^)(NSInteger code_status))withFailure;

-(void)doGetVerifyCode:(NSString*)params
               success:(void(^)(NSInteger code_status))withSuccess
               failure:(void(^)(NSInteger code_status))withFailure;

-(void)doResetPwd:(NSString*)params
          success:(void(^)(NSInteger code_status))withSuccess
          failure:(void(^)(NSInteger code_status))withFailure;

//-(void)doGesturePwdValidate:(NSString*)params
//                    success:(void(^)(NSInteger code_status))withSuccess
//                    failure:(void(^)(NSInteger code_status))withFailure;

-(BOOL)isLogin;
-(BOOL)isGesturePwdLogin;// 是否手势密码登录
-(void)isGesturePwdLogin:(void(^)(NSInteger hasSetGesturePwd))withBlock;

-(BOOL)gesturePasswordIsExit;// 是否缓存手势密码

-(void)doLogout;
-(void)doRemoveAll;
-(void)doRemoveGesturePwd;

-(BOOL)isSelfDispatch:(NSString*)companyId;// 区分是否自派外派
-(BOOL)isBindMailbox;

-(NSInteger)userLevel;//

- (void)doThirdPartyLogin:(NSString*)loginParams
             thirdPartyId:(NSUInteger)thirdPartyId
                  success:(void(^)(NSInteger code_status))withSuccess
                  failure:(void(^)(NSInteger code_status))withFailure;

- (void)doThirdPartyRegister:(NSString*)registerParam
                      mobile:(NSString*)mobile
                thirdPartyId:(NSUInteger)thirdPartyId
                     success:(void(^)(NSInteger code_status))withSuccess
                     failure:(void(^)(NSInteger code_status))withFailure;

- (void)getAccountInfo:(void(^)(NSInteger code_status))block;

@end
