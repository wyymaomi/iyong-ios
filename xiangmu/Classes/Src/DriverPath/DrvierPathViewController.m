//
//  DrvierPathViewController.m
//  xiangmu
//
//  Created by 湛思科技 on 2017/6/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "DrvierPathViewController.h"
//#import "SportAnnotationView.h"

// 运动结点信息类
@interface BMKSportNode : NSObject

//经纬度
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
//方向（角度）
@property (nonatomic, assign) CGFloat angle;
//距离
@property (nonatomic, assign) CGFloat distance;
//速度
@property (nonatomic, assign) CGFloat speed;



@end

@implementation BMKSportNode

@end


@interface SportAnnotationView : BMKAnnotationView

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation SportAnnotationView

//@synthesize imageView = _imageView;

- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBounds:CGRectMake(0.f, 0.f, 22.f, 22.f)];
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 22.f, 22.f)];
//        _imageView.image = [UIImage imageNamed:@"sportarrow.png"];
        _imageView.image = [UIImage imageNamed:@"icon_blue_start"];
        [self addSubview:_imageView];
    }
    return self;
}

@end

#pragma mark - DriverPathViewController

@interface DrvierPathViewController ()<BMKMapViewDelegate>

@property (nonatomic, strong) UIButton *playButton;

// 是否已经结束轨迹动画
@property (nonatomic, assign) NSUInteger animationStatus;// 0: 未开始动画；1:动画已经开始

@end

@implementation DrvierPathViewController

-(NSMutableArray*)trackHistoryList
{
    if (!_trackHistoryList) {
        _trackHistoryList = [NSMutableArray new];
    }
    return _trackHistoryList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"行程轨迹";
    
    self.animationStatus = 0;
    // Do any additional setup after loading the view.
    
    //适配ios7
//    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0)) {
//        self.navigationController.navigationBar.translucent = NO;
//    }
    
    _mapView = [[BMKMapView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_mapView];
    
    _mapView.zoomLevel = 19;
//    _mapView.centerCoordinate = CLLocationCoordinate2DMake(31.161320789839, 121.4452211652);
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
    
    //初始化轨迹点
    [self initSportNodes];
    
    
    
}

//根据polyline设置地图范围
- (void)mapViewFitPolyLine:(BMKPolyline *) polyLine {
    CGFloat ltX, ltY, rbX, rbY;
    if (polyLine.pointCount < 1) {
        return;
    }
    BMKMapPoint pt = polyLine.points[0];
    ltX = pt.x, ltY = pt.y;
    rbX = pt.x, rbY = pt.y;
    for (int i = 1; i < polyLine.pointCount; i++) {
        BMKMapPoint pt = polyLine.points[i];
        if (pt.x < ltX) {
            ltX = pt.x;
        }
        if (pt.x > rbX) {
            rbX = pt.x;
        }
        if (pt.y > ltY) {
            ltY = pt.y;
        }
        if (pt.y < rbY) {
            rbY = pt.y;
        }
    }
    BMKMapRect rect;
    rect.origin = (BMKMapPoint){ltX, ltY};//BMKMapPointMake(ltX, ltY);
//    rect.origin = BMKMapPointMake(ltX , ltY);
    rect.size = (BMKMapSize){rbX-ltX, rbY-ltY};//BMKMapSizeMake(rbX - ltX, rbY - ltY);
    [_mapView setVisibleMapRect:rect];
    DLog(@"_mapView.zoomLevel = %.2f", _mapView.zoomLevel);
    _mapView.zoomLevel = _mapView.zoomLevel - 0.5;
}

- (void)zoomToMapPoints:(BMKMapView*)mapView annotations:(NSArray*)annotations
{
    double minLat = 360.0f, maxLat = -360.0f;
    double minLon = 360.0f, maxLon = -360.0f;
    for (BMKSportNode *node in self.sportNodes) {
        if ( node.coordinate.latitude  < minLat ) minLat = node.coordinate.latitude;
        if ( node.coordinate.latitude  > maxLat ) maxLat = node.coordinate.latitude;
        if ( node.coordinate.longitude < minLon ) minLon = node.coordinate.longitude;
        if ( node.coordinate.longitude > maxLon ) maxLon = node.coordinate.longitude;
    }
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((maxLat + minLat) / 2.0, (maxLon + minLon) / 2.0);
//    MKCoordinateSpan span = MKCoordinateSpanMake(maxLat - minLat, maxLon - minLon);
//    MKCoordinateRegion region = MKCoordinateRegionMake(center, span);
//    [mapView setRegion:region animated:YES];
    BMKCoordinateSpan span = (BMKCoordinateSpan){maxLat-minLat, maxLon-minLon};
    BMKCoordinateRegion region =  (BMKCoordinateRegion){center, span};
    // 设定的该范围可能会被调整为适合地图窗口显示的范围
    [mapView setRegion:region];
    
}

-(UIButton*)playButton
{
    if (!_playButton) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x333333)] forState:UIControlStateNormal];
//        button.frame = CGRectMake((ViewWidth-218*Scale)/2, (self.view.frame.size.height-75*Scale), 218*Scale, 53.5*Scale);
//        [button setTitle:@"回放" forState:UIControlStateNormal];
//        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        button.titleLabel.font = FONT(25);
        button.frame = CGRectMake(145*Scale, self.view.height-118.5*Scale, 86*Scale, 86*Scale);
        [button setBackgroundImage:[UIImage imageNamed:@"btn_path_playback"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(startPathAnimation) forControlEvents:UIControlEventTouchUpInside];
        _playButton = button;
        [self.view addSubview:button];
    }
    return _playButton;
}

-(void)viewWillAppear:(BOOL)animated
{
    [_mapView viewWillAppear];
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
    
    self.playButton.frame = CGRectMake(145*Scale, self.view.height-118.5*Scale, 86*Scale, 86*Scale);
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [_mapView viewWillDisappear];
    _mapView.delegate = nil; // 不用时，置nil
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 初始化轨迹点

- (void)initSportNodes {
    
    if (!_trackHistoryList) {
        return;
    }
    
//    NSError *error;
//    _trackHistoryModel = [[TrackHistoryModel alloc] initWithDictionary:_trackHistoryDict error:&error];
//    if (error) {
//        DLog(@"error.description = %@", error.description);
//    }
    
    _sportNodes = [[NSMutableArray alloc] init];
    
    
    
    for (NSDictionary *dict in self.trackHistoryList) {
        
        BMKSportNode *sportNode = [[BMKSportNode alloc] init];
        sportNode.coordinate = CLLocationCoordinate2DMake([dict[@"latitude"] doubleValue], [dict[@"longitude"] doubleValue]);
        sportNode.angle = [dict[@"direction"] doubleValue];
        sportNode.speed = [dict[@"speed"] doubleValue];
        [_sportNodes addObject:sportNode];
        
    }
    
    BMKSportNode *sportNode = _sportNodes[0];
//    _mapView.centerCoordinate = sportNode.coordinate;
    
    _sportNodeNum = _sportNodes.count;
    
    DLog(@"sportNodeNum = %ld", (long)_sportNodeNum);
    
    
    
}

#pragma mark - 开始

- (void)start {
    CLLocationCoordinate2D paths[_sportNodeNum];
    for (NSInteger i = 0; i < _sportNodeNum; i++) {
        BMKSportNode *node = _sportNodes[i];
        paths[i] = node.coordinate;
    }
    
    _pathPolyLine = [BMKPolyline polylineWithCoordinates:paths count:_sportNodeNum];
    [_mapView addOverlay:_pathPolyLine];
    
//    _pathPloygon = [BMKPolygon polygonWithCoordinates:paths count:_sportNodeNum];
//    [_mapView addOverlay:_pathPloygon];
    
    _sportAnnotation = [[BMKPointAnnotation alloc]init];
    _sportAnnotation.coordinate = paths[0];
    _sportAnnotation.title = @"test";
    [_mapView addAnnotation:_sportAnnotation];
    _currentIndex = 0;
    
//    _mapView.centerCoordinate =
    BMKSportNode *node = _sportNodes[0];
//    _mapView.centerCoordinate = node.coordinate;
    
//    [self zoomToMapPoints:_mapView annotations:_sportNodes];
    
    [self mapViewFitPolyLine:_pathPolyLine];
    
}


-(void)startPathAnimation
{
    if (self.animationStatus == 0) {
        self.animationStatus = 1;
    }
    else {
        self.animationStatus = 0;
    }
//    self.animationStatus = self.animationStatus == 0 ? self.animationStatus = 1 : self.animationStatus = 0;
    
//    UIImage *backgroundImage = (self.animationStatus == 0) ? [UIImage imageNamed:@"btn_path_playback"] : [UIImage imageNamed:@"btn_path_endplay"]:;
    
    UIImage *backgroundImage;
    if (self.animationStatus == 0) {
        backgroundImage = [UIImage  imageNamed:@"btn_path_playback"];
        [self endAnimation];
    }
    else {
        backgroundImage = [UIImage imageNamed:@"btn_path_endplay"];
        [self running];
    }
    
    [self.playButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
//    if (self.animationStatus == 0) {
//        self.playButton setBackgroundImage:[UIImage imageNamed:@"] forState:<#(UIControlState)#>
//    }
//    self.playButton setBackgroundImage:[UIImage imageNamed:@"] forState:<#(UIControlState)#>
}

//runing
- (void)running {
    
    if (self.animationStatus == 0) {
        [self endAnimation];
        return;
    }
    
    if (_currentIndex >= _sportNodeNum) {
        return;
    }
    
    
    
    BMKSportNode *node = _sportNodes[_currentIndex];//[_sportNodes objectAtIndex:_currentIndex/];/%_sportNodeNum];
    WeakSelf
//    DLog(@"node.distance = %.2f, node.speed = %.2f, animationDuration = %.2f", node.distance, node.speed, node.distance/node.speed);
    
    [UIView animateWithDuration:1 animations:^{
        _currentIndex++;
        if (_currentIndex >= _sportNodeNum) {
            return;
        }
        BMKSportNode *node = _sportNodes[_currentIndex];//[_sportNodes objectAtIndex:_currentIndex%_sportNodeNum];
        _sportAnnotation.coordinate = node.coordinate;
//        _mapView.centerCoordinate = _sportAnnotation.coordinate;
    } completion:^(BOOL finished) {
        
        if (_currentIndex >= _sportNodeNum) {
//            UIImage *backgroundImage = [UIImage  imageNamed:@"btn_path_playback"];
//            [self.playButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
//            self.animationStatus = 0;
            [self endAnimation];
            return;
        }
        
        [weakSelf running];
    }];
    
//   _timer =[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(playTrack) userInfo:nil repeats:YES];
    

    
}

- (void)endAnimation{
//    [_recordImageView.layer removeAllAnimations];//会结束动画，使finished变量返回Null
//    [self.view.layer removeAllAnimations];
//    [_mapView.layer removeAllAnimations];
    self.animationStatus = 0;
    UIImage *backgroundImage = [UIImage imageNamed:@"btn_path_playback"];
    [self.playButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    _currentIndex = 0;
    BMKSportNode *node = _sportNodes[_currentIndex];
    _sportAnnotation.coordinate = node.coordinate;
    
}

-(void)playTrack
{
    if (_currentIndex<_sportNodes.count) {
        
//        CLLocationCoordinate2D start_coor;
        
        BMKSportNode *node = self.sportNodes[_currentIndex];
        
//        start_coor.latitude = node.coordinate.lati
        
//        start_coor.latitude = [[[steparray objectAtIndex:timeCount]objectForKey:@"lat"] floatValue];
//        start_coor.longitude = [[[steparray objectAtIndex:timeCount]objectForKey:@"lon"] floatValue];
        
//        timeCount = timeCount++;
        _currentIndex++;
        
//        pointAnnotation.title = @"轨迹";
//        pointAnnotation.coordinate = start_coor;
        
        
        [_mapView setCenterCoordinate:node.coordinate];
    }
    else
    {
        [_timer invalidate];
        
        [MsgToolBox showToast:@"播放结束"];
        
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"btn_path_playback"] forState:UIControlStateNormal];
        
//        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"播放结束" delegate:self cancelButtonTitle:@"我知道了" otherButtonTitles:nil, nil nil]show];
    }
}

#pragma mark - BMKMapViewDelegate

- (void)mapViewDidFinishLoading:(BMKMapView *)mapView {
    [self start];
    
    self.playButton.frame = CGRectMake(145*Scale, self.view.height-118.5*Scale, 86*Scale, 86*Scale);
    
}

//根据overlay生成对应的View
- (BMKOverlayView *)mapView:(BMKMapView *)mapView viewForOverlay:(id <BMKOverlay>)overlay
{
    if ([overlay isKindOfClass:[BMKPolygon class]])
    {
        BMKPolygonView* polygonView = [[BMKPolygonView alloc] initWithOverlay:overlay];
        polygonView.strokeColor = [[UIColor alloc] initWithRed:0.0 green:0.5 blue:0.0 alpha:0.6];
        polygonView.lineWidth = 3.0;
        return polygonView;
    }
    if ([overlay isKindOfClass:[BMKPolyline class]]) {
        BMKPolylineView *polyLineView = [[BMKPolylineView alloc] initWithOverlay:overlay];
        polyLineView.strokeColor = [[UIColor alloc] initWithRed:0.0 green:0.5 blue:0.0 alpha:0.6];
        polyLineView.lineWidth = 3.0;
        return polyLineView;
    }
    return nil;
}


// 根据anntation生成对应的View
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if (_sportAnnotationView == nil) {
        _sportAnnotationView = [[SportAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"sportsAnnotation"];
        _sportAnnotationView.draggable = NO;
        BMKSportNode *node = [_sportNodes firstObject];
        _sportAnnotationView.imageView.transform = CGAffineTransformMakeRotation(node.angle);
        
    }
    return _sportAnnotationView;
}

//-(void)mapView:(BMKMapView*)mapView didAddOverlayViews:(NSArray *)overlayViews
//{
//    
//}

- (void)mapView:(BMKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
//    [self running];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
