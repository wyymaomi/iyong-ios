//
//  MyAccountTableViewCell.m
//  xiangmu
//
//  Created by 湛思科技 on 17/3/20.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import "MyAccountTableViewCell.h"

@implementation MyAccountTableViewCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.name.frame = CGRectMake(50*Scale, 0, ViewWidth-40, self.frame.size.height);
    
    self.name.font = FONT(18);
    
    self.detailLabel.frame = CGRectMake(0, 0, ViewWidth-35, self.frame.size.height);
    
    self.detailLabel.font = FONT(18);
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
