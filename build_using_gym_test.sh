#!/bin/bash

#计时
SECONDS=0

#假设脚本放置在与项目相同的路径下
cur_project_path=$(pwd)
#取当前时间字符串添加到文件结尾
now=$(date +"%Y_%m_%d_%H_%M_%S")

#指定项目的scheme名称
scheme="xiangmu_test"
#指定要打包的配置名
configuration="Adhoc"
#指定打包所使用的输出方式，目前支持app-store, package, ad-hoc, enterprise, development, 和developer-id，即xcodebuild的method参数
export_method='ad-hoc'

#指定项目地址
workspace_path="$cur_project_path/xiangmu.xcworkspace"
#project_path="$cur_project_path/xiangmu.xcworkspace"
#指定输出路径
output_path="$cur_project_path/build"
#指定输出归档文件地址
archive_path="$output_path/xiangmu_test_${now}.xcarchive"
#指定输出ipa地址
ipa_path="$output_path/xiangmu_test_${now}.ipa"
#指定输出ipa名称
ipa_name="xiangmu_test_${now}.ipa"
#获取执行命令时的commit message
commit_msg="$1"

#输出设定的变量值
echo "===workspace path: ${workspace_path}==="
#echo "===project path: ${project_path}===="
echo "===archive path: ${archive_path}==="
echo "===ipa path: ${ipa_path}==="
echo "===export method: ${export_method}==="
echo "===commit msg: $1==="

#先清空前一次build
gym --workspace ${workspace_path} --scheme ${scheme} --clean --configuration ${configuration} --archive_path ${archive_path} --export_method ${export_method} --output_directory ${output_path} --output_name ${ipa_name}
#gym --project ${project_path} --scheme ${scheme} --clean --configuration ${configuration} --archive_path ${archive_path} --export_method ${export_method} --output_directory ${output_path} --output_name ${ipa_name}

#上传到fir
#fir publish ${ipa_path} -T fir_token -c "${commit_msg}"

#上传到蒲公英平台
#蒲公英应用上传地址
#url="http://www.pgyer.com/apiv1/app/upload"
url="https://qiniu-storage.pgyer.com/apiv1/app/upload"
#蒲公英提供的 用户Key
uKey="5923b6c7dd5829014e834269484672d8"
#上传文件的文件名（这个可随便取，但一定要以 ipa 结尾）
#file_name = 'xxxx.ipa'
#蒲公英提供的 API Key
_api_key="be29a81f175461f1cccb546036a80b8a"
curl -F "file=@${ipa_path}" \
-F "uKey=${uKey}" \
-F "_api_key=${_api_key}" \
https://qiniu-storage.pgyer.com/apiv1/app/upload
#输出总用时
echo "===Finished. Total time: ${SECONDS}s==="


