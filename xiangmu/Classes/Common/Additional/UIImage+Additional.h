//
//  UIImage+Additional.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additional)

+ (UIImage *)imageWithColor:(UIColor *)color;

- (UIImage *)maskImage:(UIColor *)maskColor;

/*!
 @abstract      从资源文件中获取的图像,功能类似于UIImage
 @discussion    返回的是alloc对象，需要在外部release
 @param         filename  文件名称
 @result        retainCount为1的UIImage
 */
+ (UIImage *)newImageFromResource:(NSString *)filename;

///设置图片旋转角度
- (UIImage*)zj_imageRotatedByAngle:(CGFloat)Angle;

@end

