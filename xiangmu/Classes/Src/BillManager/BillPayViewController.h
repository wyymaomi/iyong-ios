//
//  BillPayViewController.h
//  xiangmu
//
//  Created by 湛思科技 on 16/8/16.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "BaseViewController.h"

@interface BillPayViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *totalRepayLabel;// 总还款金额
@property (weak, nonatomic) IBOutlet UIButton *selectAllButton;//全选按钮
@property (weak, nonatomic) IBOutlet UIButton *payButton;// 还款按钮
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;


@end
