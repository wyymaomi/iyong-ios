//
//  NewMainTabBarController.h
//  xiangmu
//
//  Created by 湛思科技 on 2017/5/11.
//  Copyright © 2017年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabbarDelegate.h"
#import "CustomTabBarViewController.h"
//#import "UITabBarController+EMConnection.h"

@interface NewMainTabBarController : CustomTabBarViewController<TabbarDelegate>
//{
//    EMConnectionState _connectionState;
//}

- (void)setCenterButtonSelected:(BOOL)selected;

//@property (nonatomic, strong) AccountInfoView *accountInfoView;
@property (nonatomic, assign) BOOL isAccountInfoShow;// 是否显示帐户页面
@property (nonatomic, assign) SEL nextAction;



//@property (nonatomic, strong) UINavigationController *homeNavigation;
//@property (nonatomic, retain) UINavigationController *orderNavigation;
//@property (nonatomic, retain) UINavigationController *billNavigation;
//@property (nonatomic, retain) UINavigationController *meNavigation;
//@property (nonatomic, retain) UINavigationController *communityNavigation;
//@property (nonatomic, strong) MeViewController       *meViewController;



@end
