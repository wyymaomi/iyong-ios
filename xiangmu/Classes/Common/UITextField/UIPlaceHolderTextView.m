//
//  UIPlaceHolderTextView.m
//  xiangmu
//
//  Created by 湛思科技 on 16/5/17.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import "UIPlaceHolderTextView.h"

@interface UIPlaceHolderTextView ()



@end

@implementation UIPlaceHolderTextView

CGFloat const UI_PLACEHOLDER_TEXT_CHANGED_ANIMATION_DURATION = 0.25;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if __has_feature(objc_arc)
#else
    [_placeHolderLabel release]; _placeHolderLabel = nil;
    [_placeholderColor release]; _placeholderColor = nil;
    [_placeholder release]; _placeholder = nil;
    [super dealloc];
#endif
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Use Interface Builder User Defined Runtime Attributes to set
    // placeholder and placeholderColor in Interface Builder.
    if (!self.placeholder) {
        [self setPlaceholder:@""];
    }
    
    if (!self.placeholderColor) {
        [self setPlaceholderColor:[UIColor lightGrayColor]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
}

-(void)setPlaceholder:(NSString *)placeholder
{
    if (placeholder.trim.length > 0) {
        self.placeHolderLabel.text = placeholder;
        self.placeHolderLabel.alpha = 1;
    }
    else {
        self.placeHolderLabel.alpha = 0;
    }
}

-(void)setPlaceholderColor:(UIColor *)placeholderColor
{
    self.placeHolderLabel.textColor = placeholderColor;
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {

        
//        [self setPlaceholder:@""];
        [self setPlaceholderColor:[UIColor lightGrayColor]];
        
        [self addSubview:self.placeHolderLabel];
        self.placeHolderLabel.frame = CGRectMake(8, 8, self.bounds.size.width-16, 30);
        self.placeHolderLabel.textColor = [UIColor lightGrayColor];
        [self.placeHolderLabel sizeToFit];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)textChanged:(NSNotification *)notification
{
//    if([[self placeholder] length] == 0)
//    {
//        return;
//    }
    
    WeakSelf
    [UIView animateWithDuration:UI_PLACEHOLDER_TEXT_CHANGED_ANIMATION_DURATION animations:^{
        StrongSelf
        if([[strongSelf text] length] == 0)
        {
            [[strongSelf viewWithTag:999] setAlpha:1];
        }
        else
        {
            [[strongSelf viewWithTag:999] setAlpha:0];
        }
    }];
}

//- (void)setText:(NSString *)text {
//    [super setText:text];
//    [self textChanged:nil];
//}

-(UILabel*)placeHolderLabel
{
    if (_placeHolderLabel == nil )
    {
        _placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(8,8,self.bounds.size.width - 16,0)];
        _placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _placeHolderLabel.numberOfLines = 0;
        _placeHolderLabel.font = self.font;
        _placeHolderLabel.backgroundColor = [UIColor clearColor];
        _placeHolderLabel.textColor = self.placeholderColor;
        _placeHolderLabel.alpha = 0;
        _placeHolderLabel.tag = 999;
        [self addSubview:_placeHolderLabel];
    }
    return _placeHolderLabel;
}

//- (void)drawRect:(CGRect)rect
//{
//    if( [[self placeholder] length] > 0 )
//    {
//        
//        
//        _placeHolderLabel.text = self.placeholder;
//        [_placeHolderLabel sizeToFit];
//        [self sendSubviewToBack:_placeHolderLabel];
//    }
//    
//    if( [[self text] length] == 0 && [[self placeholder] length] > 0 )
//    {
//        [[self viewWithTag:999] setAlpha:1];
//    }
//    
//    
//    
//    [super drawRect:rect];
//}

@end
