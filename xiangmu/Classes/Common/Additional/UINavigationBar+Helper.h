//
//  UINavigationBar+Helper.h
//  xiangmu
//
//  Created by 湛思科技 on 16/6/28.
//  Copyright © 2016年 湛思科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Helper)

- (void)setBottomBorderColor:(UIColor *)color height:(CGFloat)height;

@end
